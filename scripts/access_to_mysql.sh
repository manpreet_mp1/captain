#!/bin/bash
#==============================================================================================================================
# FILE        : script.sh
# DESCRIPTION : Convert MS Access to MySQL. Saves it to local.
#               After it is saved to local, export it to .sql and import it via application
#==============================================================================================================================
set -o nounset      # help avoid bugs
shopt -s extglob
PATH=/bin:/usr/bin:/sbin # for consistency

### verify the first parameter/argument
if [[ $# -lt 1 ]]; then
   echo "$(date +%Y%m%d_%H%M) ${0##*/} requires the first parameter as path to access file"
   exit 500
fi

# mysql credentials
DB_NAME=ipeds_testing
DB_UN=root

# set login path to suppress warnings
mysql_config_editor remove --login-path=local
mysql_config_editor set --login-path=local --host=localhost --user=$DB_UN --password

# get all the tables
TABLES=$(mdb-tables -1 $1)

# drop tables if exits
echo "$(date +%Y%m%d_%H%M) DROPPING TABLE IF EXISTS"
for t in $TABLES
do
    mysql --login-path=local $DB_NAME -Bse "DROP TABLE IF EXISTS $t;"
done

rm -rf meta.sql
mdb-schema $1 mysql > meta.sql

echo "$(date +%Y%m%d_%H%M) CREATING TABLES;"
mysql --login-path=local $DB_NAME < meta.sql
rm -rf meta.sql

echo "$(date +%Y%m%d_%H%M) IMPORTING DATA INTO TABLES;"
for t in $TABLES
do
    echo "$(date +%Y%m%d_%H%M) IMPORTING DATA FOR $t;"
    rm -rf $t.csv
    mdb-export -D '%Y-%m-%d %H:%M:%S' $1 $t > $t.csv
    mysqlimport --login-path=local --ignore-lines=1 --fields-terminated-by=, --fields-optionally-enclosed-by='"' --local $DB_NAME $t.csv
    mysql --login-path=local $DB_NAME -Bse "ALTER TABLE $t CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;"
    rm -rf $t.csv
done

echo "$(date +%Y%m%d_%H%M) EXPORTING TO .SQL FOR IMPORT;"
mysqldump --login-path=local $DB_NAME --result-file=ipeds.sql
rm -rf ./../storage/sqlDump/ipeds.sql
mv ipeds.sql ./../storage/sqlDump/ipeds.sql

echo "$(date +%Y%m%d_%H%M) COMPLETED;"