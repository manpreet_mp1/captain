<?php
namespace Tests\Unit\Repositories\FinancialCoachAssessmentRepository;

use App\Repositories\Misc\FinancialCoachAssessmentRepository;
use Illuminate\Http\Request;
use Tests\TestCase;

class GetQualifiedOrNotQualifiedTest extends TestCase
{
    /** @test */
    public function not_qualified_no_parent_two_income()
    {
        $request = new Request();

        $request->replace([
            "parentOneIncome" => "Less than $75,000",
            "parentTwoIncome" => null,
            "assetBank" => "$51,000 to $100,000",
            "collegeSaving" => "$51,000 to $100,000",
            "investment" => "$51,000 to $100,000",
            "retirement" => "$51,000 to $100,000",
        ]);

        $this->assertEquals((new FinancialCoachAssessmentRepository())->getQualifiedOrNotQualified($request), "notQualified");
    }

    /** @test */
    public function not_qualified_parent_two_income_incorrect_format()
    {
        $request = new Request();

        $request->replace([
            "parentOneIncome" => "Less than $75,000",
            "parentTwoIncome" => "_____",
            "assetBank" => "$51,000 to $100,000",
            "collegeSaving" => "$51,000 to $100,000",
            "investment" => "$51,000 to $100,000",
            "retirement" => "$51,000 to $100,000",
        ]);

        $this->assertEquals((new FinancialCoachAssessmentRepository())->getQualifiedOrNotQualified($request), "notQualified");
    }

    /** @test */
    public function not_qualified_income_met_assets_not_met()
    {
        $request = new Request();

        $request->replace([
            "parentOneIncome" => "$76,000 to $125,000",
            "parentTwoIncome" => "$76,000 to $125,000",
            "assetBank" => "Less than $50,000",
            "collegeSaving" => "Less than $50,000",
            "investment" => "Less than $50,000",
            "retirement" => "Less than $50,000",
        ]);

        $this->assertEquals((new FinancialCoachAssessmentRepository())->getQualifiedOrNotQualified($request), "notQualified");
    }

    /** @test */
    public function all_qualified()
    {
        $request = new Request();

        $request->replace([
            "parentOneIncome" => "$76,000 to $125,000",
            "parentTwoIncome" => "$76,000 to $125,000",
            "assetBank" => "$51,000 to $100,000",
            "collegeSaving" => "$51,000 to $100,000",
            "investment" => "$51,000 to $100,000",
            "retirement" => "$51,000 to $100,000",
        ]);

        $this->assertEquals((new FinancialCoachAssessmentRepository())->getQualifiedOrNotQualified($request), "qualified");
    }
}
