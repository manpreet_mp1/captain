<?php

namespace Tests\Unit\Controllers;

use Tests\TestCase;
use function config;
use function route;

class FinancialCoachAssessmentRedirectControllerTest extends TestCase
{
    /** @test */
    public function it_should_redirect_to_ncsa_mvp()
    {
        $response = $this->get(route('scripts.financialCoachAssessmentRedirect') . "?source=ncsa-mvp");

        $response->assertRedirect(config('scripts.financialCoachAssessment.ncsa-mvp.notQualifiedUrl'));
    }

    /** @test */
    public function it_should_redirect_to_ncsa_mvp_chris_campbell()
    {
        $response = $this->get(route('scripts.financialCoachAssessmentRedirect') . "?source=ncsa-mvp-chris-campbell");

        $response->assertRedirect(config('scripts.financialCoachAssessment.ncsa-mvp-chris-campbell.notQualifiedUrl'));
    }
}
