<?php
namespace Tests\Unit;

use Tests\TestCase;

/**
 * Class CodeStyleLinterTest
 * @package Tests\Unit\Core
 */
class CodeStyleLinterTest extends TestCase
{
    /** @test */
    public function psr2()
    {
        $phpCsFixerPath = base_path('/vendor/bin/php-cs-fixer');

        // Let's check PSR-2 compliance for our code, our tests and our index.php
        // Add any other pass you want to test to this array.
        foreach (['app', 'config', 'public/index.php', 'tests', 'database'] as $path) {
            // Run linter in dry-run mode so it changes nothing.
            $fulllPath = base_path($path);
            exec(
                $phpCsFixerPath . " fix {$fulllPath} --dry-run --allow-risky=no",
                $output,
                $return_var
            );

            // If we've got output, pop its first item ("Fixed all files...")
            // and trim whitespace from the rest so the below makes sense.
            if ($output) {
                array_pop($output);
                $output = array_map('trim', $output);
            }

            // Check shell return code: if nonzero, report the output as a failure.
            $this->assertEquals(
                0,
                $return_var,
                "PSR-2 linter reported errors in $path/: " . implode('; ', $output)
            );
        }
    }
}
