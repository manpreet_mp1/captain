# Laravel Native Functions
composer install
composer dumpautoload
php artisan cache:clear
php artisan config:clear
php artisan queue:restart

#!/bin/bash
# Set access and ownership of folders.
chown www-data:www-data -R *
echo "Ownership updated!"
chmod 775 -R *
echo "Access updated!"
chmod 777 -R storage