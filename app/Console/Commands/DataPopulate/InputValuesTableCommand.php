<?php

namespace App\Console\Commands\DataPopulate;

use App\Models\Dropdown\InputValue;
use App\Repositories\Misc\HelperRepository;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use function now;
use function storage_path;

class InputValuesTableCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DataPopulate:InputValuesTableCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate dropdown table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Model::unguard();

        $this->info(now() . ": started: DataPopulate:InputValuesTableCommand");

        $this->info(now() . ": Delete All Current Records");

        InputValue::truncate();

        $this->info(now() . ": Inserting Records");

        $records = (new HelperRepository())->createArrayFromCsvFile(storage_path('dataPopulate') . "/input_values.csv");

        InputValue::insert($records);

        InputValue::whereNotNull('id')->update([
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $this->info(now() . ": completed: DataPopulate:InputValuesTableCommand");

        Model::unguard(false);

        return true;
    }
}
