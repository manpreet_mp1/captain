<?php

namespace App\Console\Commands\DataPopulate;

use function config;
use function exec;
use Illuminate\Console\Command;
use function now;
use function storage_path;
use ZipArchive;

class DumpIpedsDataFromSqlFileCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DataPopulate:DumpIpedsDataFromSqlFileCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dump IPEDS data from .sql file to IPEDS Connection';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(now() . ': started: DataPopulate:DumpIpedsDataFromSqlFileCommand');

        $host = config('database.connections.ipeds.host');
        $port = config('database.connections.ipeds.port');
        $database = config('database.connections.ipeds.database');
        $username = config('database.connections.ipeds.username');
        $password = config('database.connections.ipeds.password');

        $zip = new ZipArchive;
        $zip->open(storage_path('sqlDump') . '/ipeds.sql.zip');
        $zip->extractTo(storage_path('sqlDump'));
        $zip->close();

        $command = "mysql --database={$database} --user={$username} --host={$host} --port={$port} -p{$password} < " . storage_path('sqlDump') . '/ipeds.sql';

        exec($command);
        
        $this->info(now() . ': completed: DataPopulate:DumpIpedsDataFromSqlFileCommand');

        return true;
    }
}
