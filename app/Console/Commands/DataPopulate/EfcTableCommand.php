<?php

namespace App\Console\Commands\DataPopulate;

use App\Models\Data\Efc\EfcTableA1;
use App\Models\Data\Efc\EfcTableA2;
use App\Models\Data\Efc\EfcTableA3;
use App\Models\Data\Efc\EfcTableA35;
use App\Models\Data\Efc\EfcTableA4;
use App\Models\Data\Efc\EfcTableA5;
use App\Models\Data\Efc\EfcTableA6;
use App\Models\Data\Efc\EfcTableA7;
use App\Repositories\Misc\HelperRepository;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use function now;
use function storage_path;

class EfcTableCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DataPopulate:EfcTableCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate EFC Tables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Model::unguard();

        $this->info(now() . ': started: DataPopulate:EfcTableCommand');

        $this->info(now() . ': EFC Table A1');
        EfcTableA1::truncate();
        $records = (new HelperRepository())->createArrayFromCsvFile(storage_path('dataPopulate') . "/efc_table_a1.csv");
        EfcTableA1::insert($records);
        EfcTableA1::whereNotNull('id')->update([
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $this->info(now() . ': EFC Table A2');
        EfcTableA2::truncate();
        $records = (new HelperRepository())->createArrayFromCsvFile(storage_path('dataPopulate') . "/efc_table_a2.csv");
        EfcTableA2::insert($records);
        EfcTableA2::whereNotNull('id')->update([
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $this->info(now() . ': EFC Table A3');
        EfcTableA3::truncate();
        $records = (new HelperRepository())->createArrayFromCsvFile(storage_path('dataPopulate') . "/efc_table_a3.csv");
        EfcTableA3::insert($records);
        EfcTableA3::whereNotNull('id')->update([
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $this->info(now() . ': EFC Table A35');
        EfcTableA35::truncate();
        $records = (new HelperRepository())->createArrayFromCsvFile(storage_path('dataPopulate') . "/efc_table_a35.csv");
        EfcTableA35::insert($records);
        EfcTableA35::whereNotNull('id')->update([
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $this->info(now() . ': EFC Table A4');
        EfcTableA4::truncate();
        $records = (new HelperRepository())->createArrayFromCsvFile(storage_path('dataPopulate') . "/efc_table_a4.csv");
        EfcTableA4::insert($records);
        EfcTableA4::whereNotNull('id')->update([
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $this->info(now() . ': EFC Table A5');
        EfcTableA5::truncate();
        $records = (new HelperRepository())->createArrayFromCsvFile(storage_path('dataPopulate') . "/efc_table_a5.csv");
        EfcTableA5::insert($records);
        EfcTableA5::whereNotNull('id')->update([
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $this->info(now() . ': EFC Table A6');
        EfcTableA6::truncate();
        $records = (new HelperRepository())->createArrayFromCsvFile(storage_path('dataPopulate') . "/efc_table_a6.csv");
        EfcTableA6::insert($records);
        EfcTableA6::whereNotNull('id')->update([
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $this->info(now() . ': EFC Table A7');
        EfcTableA7::truncate();
        $records = (new HelperRepository())->createArrayFromCsvFile(storage_path('dataPopulate') . "/efc_table_a7.csv");
        EfcTableA7::insert($records);
        EfcTableA7::whereNotNull('id')->update([
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $this->info(now() . ': completed: DataPopulate:EfcTableCommand');

        Model::unguard(false);

        return true;
    }
}
