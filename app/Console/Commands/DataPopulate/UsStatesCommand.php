<?php

namespace App\Console\Commands\DataPopulate;

use App\Models\Dropdown\State;
use App\Repositories\Misc\HelperRepository;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use function now;
use function storage_path;

class UsStatesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DataPopulate:UsStatesCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate US States to DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Model::unguard();

        $this->info(now() . ": started: DataPopulate:UsStatesCommand");

        $this->info(now() . ": Delete All Current Records");

        State::truncate();

        $this->info(now() . ": Inserting Records");

        $records = (new HelperRepository())->createArrayFromCsvFile(storage_path('dataPopulate') . "/states.csv");

        State::insert($records);

        State::whereNotNull('id')->update([
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $this->info(now() . ": completed: DataPopulate:UsStatesCommand");

        Model::unguard(false);

        return true;
    }
}
