<?php

namespace App\Console\Commands\DataPopulate;

use App\Models\Data\Scholarship\ScholarshipMerit;
use App\Repositories\Misc\HelperRepository;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use function now;
use function storage_path;

class ScholarshipMeritTableCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DataPopulate:ScholarshipMeritTableCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate scholarship merit table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Model::unguard();

        $this->info(now() . ": started: DataPopulate:ScholarshipMeritTableCommand");

        $this->info(now() . ": Delete All Current Records");

        ScholarshipMerit::truncate();

        $this->info(now() . ": Inserting Records");

        $records = (new HelperRepository())->createArrayFromCsvFile(storage_path('dataPopulate') . "/scholarship_merits.csv");

        $recordChunks = array_chunk($records, 1000);

        foreach ($recordChunks as $chunk) {
            ScholarshipMerit::insert($chunk);
        }
        
        ScholarshipMerit::whereNotNull('id')->update([
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $this->info(now() . ": completed: DataPopulate:ScholarshipMeritTableCommand");

        Model::unguard(false);

        return true;
    }
}
