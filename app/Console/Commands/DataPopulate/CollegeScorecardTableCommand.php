<?php

namespace App\Console\Commands\DataPopulate;

use App\Models\Data\College\CollegeTreasury;
use App\Repositories\Misc\HelperRepository;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use function array_map;
use function now;

class CollegeScorecardTableCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DataPopulate:CollegeScorecardTableCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert selected data from college scorecard';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Model::unguard();

        $this->info(now() . ': started: DataPopulate:CollegeScorecardTableCommand');

        $this->info(now() . ": Delete All Current Records");

        CollegeTreasury::truncate();

        $this->info(now() . ": Inserting Records");

        $records = (new HelperRepository())->createArrayFromCsvFile(storage_path('dataPopulate') . "/college_treasuries.csv");

        // special code for handing nulls in ron'd data
        foreach ($records as $key => $array) {
            $records[$key] = array_map(function ($value) {
                return $value === "" ? null : $value;
            }, $array);
        }

        CollegeTreasury::insert($records);

        CollegeTreasury::whereNotNull('id')->update([
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $this->info(now() . ': completed: DataPopulate:CollegeScorecardTableCommand');

        Model::unguard(false);

        return true;
    }
}
