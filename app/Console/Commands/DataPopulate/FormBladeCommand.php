<?php

namespace App\Console\Commands\DataPopulate;

use App\Models\Core\FormBlade;
use App\Repositories\Misc\HelperRepository;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use function now;
use function storage_path;

class FormBladeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DataPopulate:FormBladeCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate Form Blade to DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Model::unguard();

        $this->info(now() . ': started: DataPopulate:FormBladeCommand');

        $this->info(now() . ': Delete All Current Records');

        FormBlade::truncate();

        $this->info(now() . ': Inserting Records');

        $records = (new HelperRepository())->createArrayFromCsvFile(storage_path('dataPopulate') . "/form_blades.csv");

        FormBlade::insert($records);

        FormBlade::whereNotNull('id')->update([
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $this->info(now() . ': completed: DataPopulate:FormBladeCommand');

        Model::unguard(false);

        return true;
    }
}
