<?php

namespace App\Console\Commands\DataPopulate;

use App\Models\Data\College\CustomRonCollege;
use App\Repositories\Misc\HelperRepository;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use function now;
use function storage_path;

class CustomRonCollegesTableCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DataPopulate:CustomRonCollegesTableCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate CFG Calculator Values';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Model::unguard();

        $this->info(now() . ': started: DataPopulate:CustomRonCollegesTableCommand');

        $this->info(now() . ': Delete All Current Records');

        CustomRonCollege::truncate();

        $this->info(now() . ': Inserting Records');

        $records = (new HelperRepository())->createArrayFromCsvFile(storage_path('dataPopulate') . "/custom_ron_colleges.csv");

        // special code for handing nulls in ron'd data
        foreach ($records as $key => $array) {
            $records[$key] = array_map(function ($value) {
                return $value === "" ? null : $value;
            }, $array);
        }

        CustomRonCollege::insert($records);

        CustomRonCollege::whereNotNull('id')->update([
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $this->info(now() . ': completed: DataPopulate:CustomRonCollegesTableCommand');

        Model::unguard(false);

        return true;
    }
}
