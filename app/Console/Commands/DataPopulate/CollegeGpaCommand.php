<?php

namespace App\Console\Commands\DataPopulate;

use App\Models\Data\College\CollegeGpa;
use App\Repositories\Misc\HelperRepository;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use function array_map;
use function now;
use function storage_path;

class CollegeGpaCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DataPopulate:CollegeGpaCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate College GPA';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Model::unguard();

        $this->info(now() . ': started: DataPopulate:CollegeGpaCommand');

        $this->info(now() . ': Delete All Current Records');

        CollegeGpa::truncate();

        $this->info(now() . ': Inserting Records');

        $records = (new HelperRepository())->createArrayFromCsvFile(storage_path('dataPopulate') . "/college_gpa.csv");

        // special code for handing nulls
        foreach ($records as $key => $array) {
            $records[$key] = array_map(function ($value) {
                return $value === "" ? null : $value;
            }, $array);
        }

        CollegeGpa::insert($records);

        CollegeGpa::whereNotNull('id')->update([
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $this->info(now() . ': completed: DataPopulate:CollegeGpaCommand');

        Model::unguard(false);

        return true;
    }
}
