<?php

namespace App\Console\Commands\DataPopulate;

use App\Models\Data\FiveTwoNine\FiveTwoNineBasic;
use App\Models\Data\FiveTwoNine\FiveTwoNineBenefit;
use App\Models\Data\FiveTwoNine\FiveTwoNineContact;
use App\Models\Data\FiveTwoNine\FiveTwoNineDropdownType;
use App\Models\Data\FiveTwoNine\FiveTwoNineInvestment;
use App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail;
use App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentSecondDetail;
use App\Models\Data\FiveTwoNine\FiveTwoNineManagement;
use App\Models\Data\FiveTwoNine\FiveTwoNineMaster;
use App\Models\Data\FiveTwoNine\FiveTwoNinePrepaid;
use App\Models\Data\FiveTwoNine\FiveTwoNineStateDescription;
use App\Repositories\Misc\HelperRepository;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use function now;
use function storage_path;

class FiveTwoNineTablesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DataPopulate:FiveTwoNineTablesCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate 529 Tables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(now() . ': started: DataPopulate:FiveTwoNineTablesCommand');

        Model::unguard();

        $this->info(now() . ': five_two_nine_basics');
        FiveTwoNineBasic::truncate();
        $records = (new HelperRepository())->createArrayFromCsvFile(storage_path('dataPopulate') . "/five_two_nine_basics.csv");
        FiveTwoNineBasic::insert($records);
        FiveTwoNineBasic::whereNotNull('id')->update([
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $this->info(now() . ': five_two_nine_contacts');
        FiveTwoNineContact::truncate();
        $records = (new HelperRepository())->createArrayFromCsvFile(storage_path('dataPopulate') . "/five_two_nine_contacts.csv");
        FiveTwoNineContact::insert($records);
        FiveTwoNineContact::whereNotNull('id')->update([
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $this->info(now() . ': five_two_nine_investments');
        FiveTwoNineInvestment::truncate();
        $records = (new HelperRepository())->createArrayFromCsvFile(storage_path('dataPopulate') . "/five_two_nine_investments.csv");
        FiveTwoNineInvestment::insert($records);
        FiveTwoNineInvestment::whereNotNull('id')->update([
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $this->info(now() . ': five_two_nine_investment_first_details');
        FiveTwoNineInvestmentFirstDetail::truncate();
        $records = (new HelperRepository())->createArrayFromCsvFile(storage_path('dataPopulate') . "/five_two_nine_investment_first_details.csv");
        FiveTwoNineInvestmentFirstDetail::insert($records);
        FiveTwoNineInvestmentFirstDetail::whereNotNull('id')->update([
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $this->info(now() . ': five_two_nine_investment_second_details');
        FiveTwoNineInvestmentSecondDetail::truncate();
        $records = (new HelperRepository())->createArrayFromCsvFile(storage_path('dataPopulate') . "/five_two_nine_investment_second_details.csv");
        FiveTwoNineInvestmentSecondDetail::insert($records);
        FiveTwoNineInvestmentSecondDetail::whereNotNull('id')->update([
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $this->info(now() . ': five_two_nine_managements');
        FiveTwoNineManagement::truncate();
        $records = (new HelperRepository())->createArrayFromCsvFile(storage_path('dataPopulate') . "/five_two_nine_managements.csv");
        FiveTwoNineManagement::insert($records);
        FiveTwoNineManagement::whereNotNull('id')->update([
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $this->info(now() . ': five_two_nine_benefits');
        FiveTwoNineBenefit::truncate();
        $records = (new HelperRepository())->createArrayFromCsvFile(storage_path('dataPopulate') . "/five_two_nine_benefits.csv");
        FiveTwoNineBenefit::insert($records);
        FiveTwoNineBenefit::whereNotNull('id')->update([
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $this->info(now() . ': five_two_nine_prepaids');
        FiveTwoNinePrepaid::truncate();
        $records = (new HelperRepository())->createArrayFromCsvFile(storage_path('dataPopulate') . "/five_two_nine_prepaids.csv");
        FiveTwoNinePrepaid::insert($records);
        FiveTwoNinePrepaid::whereNotNull('id')->update([
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $this->info(now() . ': five_two_nine_state_descriptions');
        FiveTwoNineStateDescription::truncate();
        $records = (new HelperRepository())->createArrayFromCsvFile(storage_path('dataPopulate') . "/five_two_nine_state_descriptions.csv");
        FiveTwoNineStateDescription::insert($records);
        FiveTwoNineStateDescription::whereNotNull('id')->update([
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $this->info(now() . ': five_two_nine_dropdown_types');
        FiveTwoNineDropdownType::truncate();
        $records = (new HelperRepository())->createArrayFromCsvFile(storage_path('dataPopulate') . "/five_two_nine_dropdown_types.csv");
        FiveTwoNineDropdownType::insert($records);
        FiveTwoNineDropdownType::whereNotNull('id')->update([
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $this->info(now() . ': five_two_nine_masters');
        FiveTwoNineMaster::truncate();
        $records = (new HelperRepository())->createArrayFromCsvFile(storage_path('dataPopulate') . "/five_two_nine_masters.csv");
        FiveTwoNineMaster::insert($records);
        FiveTwoNineMaster::whereNotNull('id')->update([
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        Model::unguard(false);

        $this->info(now() . ': completed: DataPopulate:FiveTwoNineTablesCommand');

        return true;
    }
}
