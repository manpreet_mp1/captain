<?php

namespace App\Console\Commands\DataPopulate;

use App\Models\Data\College\College;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use ZipArchive;
use function config;
use function ini_set;
use function now;
use function storage_path;

class CollegeDataUsaCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DataPopulate:CollegeDataUsaCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert data usa data to college';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '-1');

        $this->info(now() . ': started: DataPopulate:CollegeDataUsaCommand');

        $bar = $this->output->createProgressBar(College::count());

        $zip = new ZipArchive;
        $zip->open(storage_path('sqlDump') . '/college_datausa.sql.zip');
        $zip->extractTo(storage_path('sqlDump'));
        $zip->close();

        $path = storage_path('sqlDump') . "/college_datausa.sql";

        $lines = file($path);

        foreach ($lines as $line) {
            DB::connection(config('database.data'))->unprepared($line);

            $bar->advance();
        }

        $bar->finish();

        $this->info(now() . ': completed: DataPopulate:CollegeScorecardTableCommand');

        return true;
    }
}
