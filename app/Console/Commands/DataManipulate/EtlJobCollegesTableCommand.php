<?php

namespace App\Console\Commands\DataManipulate;

use App\Models\Data\College\College;
use function config;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use function now;

class EtlJobCollegesTableCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DataManipulate:EtlJobCollegesTableCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'ETL Job for colleges, add from multiple IPEDS table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(now() . ': started: DataManipulate:EtlJobCollegesTableCommand');

        $ipedsDbName = config('database.connections.ipeds.database');
        $dataDbName = config('database.connections.data.database');

        College::truncate();

        DB::connection('ipeds')->statement("
            INSERT INTO {$dataDbName}.colleges
            SELECT
                InstitutionalCharacteristics_DirectoryInformation.unitid
              , InstitutionalCharacteristics_DirectoryInformation.instnm as name
              , null as slug             
              , if (ControlValueSet . valueLabel LIKE \"%private%\", \"Private\" , \"Public\") AS institute_type
              , InstitutionalCharacteristics_DirectoryInformation.ADDR as address
              , InstitutionalCharacteristics_DirectoryInformation.city
              , InstitutionalCharacteristics_DirectoryInformation.stabbr as state_abbr
              , InstitutionalCharacteristics_DirectoryInformation.zip
              , CONCAT(InstitutionalCharacteristics_DirectoryInformation.instnm,\" (\", InstitutionalCharacteristics_DirectoryInformation.city, \")\") AS inst_nm_city
              , InstitutionalCharacteristics_DirectoryInformation.WEBADDR as web_addr
              , InstitutionalCharacteristics_DirectoryInformation.NPRICURL as net_price_addr
              , LocaleValueSet.valueLabel as locale
              , InstSizeValueSet.valueLabel as inst_size
              , CalSysValueSet.valueLabel as cal_sys
              , InstitutionalCharacteristics_DirectoryInformation.opeid
              , InstitutionalCharacteristics_EducationalOfferings.APPLFEEU as ug_application_fee
              , Admissions_AdmissionConsiderations.APPLCN as applicants_total
              , Admissions_AdmissionConsiderations.ADMSSN as admissions_total
              , Admissions_Frequent.DVADM01 as percent_admitted
              , Admissions_AdmissionConsiderations.ENRLT as enrolled_total
              , Admissions_Frequent.DVADM07 as admissions_yield
              , FallEnrollment_Frequent.EFUG as undergraduate_enrollment
              , Admissions_AdmissionConsiderations.ACTCM25 as act_comp_25
              , Admissions_AdmissionConsiderations.ACTCM75 as act_comp_75
              , Admissions_AdmissionConsiderations.ACTEN25 as act_eng_25
              , Admissions_AdmissionConsiderations.ACTEN75 as act_eng_75
              , Admissions_AdmissionConsiderations.ACTMT25 as act_math_25
              , Admissions_AdmissionConsiderations.ACTMT75 as act_math_75
              , Admissions_AdmissionConsiderations.ACTNUM as act_submitted_total
              , Admissions_AdmissionConsiderations.ACTPCT as act_submitted_percent
              , Admissions_AdmissionConsiderations.SATVR25 as sat_read_25
              , Admissions_AdmissionConsiderations.SATVR75 as sat_read_75
              , Admissions_AdmissionConsiderations.SATMT25 as sat_math_25
              , Admissions_AdmissionConsiderations.SATMT75 as sat_math_75
              , Admissions_AdmissionConsiderations.SATNUM as sat_submitted_total
              , Admissions_AdmissionConsiderations.SATPCT as sat_submitted_percent
              , admissionReq1.valueLabel as admission_req_gpa
              , admissionReq2.valueLabel as admission_req_rank
              , admissionReq3.valueLabel as admission_req_record
              , admissionReq4.valueLabel as admission_req_comp_prep_prog
              , admissionReq5.valueLabel as admission_req_rec
              , admissionReq6.valueLabel as admission_req_comp
              , admissionReq7.valueLabel as admission_req_test_score
              , admissionReq8.valueLabel as admission_req_toefl
              , admissionReq9.valueLabel as admission_req_other_test
              , ifnull(nullif(InstitutionalCharacteristics_Frequent.cinson,\"\"),ifnull(nullif(InstitutionalCharacteristics_Frequent.cinsoff,\"\"),nullif(InstitutionalCharacteristics_Frequent.cinsfam,\"\"))) AS original_in_state_cost
              , ifnull(nullif(InstitutionalCharacteristics_Frequent.cotson,\"\"),ifnull(nullif(InstitutionalCharacteristics_Frequent.cotsoff,\"\"),nullif(InstitutionalCharacteristics_Frequent.cotsfam,\"\"))) AS original_out_state_cost
              , ifnull(nullif(InstitutionalCharacteristics_Frequent.cindon,\"\"),ifnull(nullif(InstitutionalCharacteristics_Frequent.cindoff,\"\"),nullif(InstitutionalCharacteristics_Frequent.cindfam,\"\"))) AS original_private_cost
              , InstitutionalCharacteristics_StudentCharges.chg5ay3 AS on_camp_room_board
              , InstitutionalCharacteristics_StudentCharges.chg6ay3 AS on_camp_other
              , InstitutionalCharacteristics_StudentCharges.chg7ay3 AS off_camp_not_family_room_board
              , InstitutionalCharacteristics_StudentCharges.chg8ay3 AS off_camp_not_family_other
              , InstitutionalCharacteristics_StudentCharges.chg9ay3 AS off_camp_with_family_other
              , InstitutionalCharacteristics_StudentCharges.chg4ay3 AS books_supplies
              , InstitutionalCharacteristics_EducationalOfferings.TUITPL1 as tuition_guaranteed_plan
              , InstitutionalCharacteristics_EducationalOfferings.TUITPL2 as prepaid_tuition_plan
              , InstitutionalCharacteristics_EducationalOfferings.TUITPL3 as tuition_payment_plan
              , InstitutionalCharacteristics_EducationalOfferings.TUITPL4 as other_alternative_tuition_plan
              , IFNULL(nullif(StudentFinancialAid_StudentFinancialAid.npist2,\"\"), StudentFinancialAid_StudentFinancialAid.npgrn2) as net_price
              , IF(StudentFinancialAid_StudentFinancialAid.NPIS412 = 0, StudentFinancialAid_StudentFinancialAid.NPT412, StudentFinancialAid_StudentFinancialAid.NPIS412) as net_price_0_30k
              , IF(StudentFinancialAid_StudentFinancialAid.NPIS422 = 0, StudentFinancialAid_StudentFinancialAid.NPT422, StudentFinancialAid_StudentFinancialAid.NPIS422) as net_price_30k_48k
              , IF(StudentFinancialAid_StudentFinancialAid.NPIS432 = 0, StudentFinancialAid_StudentFinancialAid.NPT432, StudentFinancialAid_StudentFinancialAid.NPIS432) as net_price_48k_75k
              , IF(StudentFinancialAid_StudentFinancialAid.NPIS442 = 0, StudentFinancialAid_StudentFinancialAid.NPT442, StudentFinancialAid_StudentFinancialAid.NPIS442) as net_price_75k_110k
              , IF(StudentFinancialAid_StudentFinancialAid.NPIS452 = 0, StudentFinancialAid_StudentFinancialAid.NPT452, StudentFinancialAid_StudentFinancialAid.NPIS452) as net_price_110k_plus
              , CAST((StudentFinancialAid_StudentFinancialAid.NPIS442 + StudentFinancialAid_StudentFinancialAid.NPIS452 + StudentFinancialAid_StudentFinancialAid.NPT442 + StudentFinancialAid_StudentFinancialAid.NPT452)/2 AS UNSIGNED) as net_price_75k_plus
              , StudentFinancialAid_StudentFinancial.AGRNT_N as full_first_ug_any_aid_total
              , StudentFinancialAid_StudentFinancial.AGRNT_P as full_first_ug_any_aid_percent
              , StudentFinancialAid_StudentFinancial.AGRNT_A as full_first_ug_any_aid_amount
              , StudentFinancialAid_StudentFinancial.FGRNT_N as full_first_ug_fed_aid_total
              , StudentFinancialAid_StudentFinancial.FGRNT_P as full_first_ug_fed_aid_percent
              , StudentFinancialAid_StudentFinancial.FGRNT_A as full_first_ug_fed_aid_amount
              , StudentFinancialAid_StudentFinancial.SGRNT_N as full_first_ug_state_aid_total
              , StudentFinancialAid_StudentFinancial.SGRNT_P as full_first_ug_state_aid_percent
              , StudentFinancialAid_StudentFinancial.SGRNT_A as full_first_ug_state_aid_amount
              , StudentFinancialAid_StudentFinancial.IGRNT_N as full_first_ug_inst_aid_total
              , StudentFinancialAid_StudentFinancial.IGRNT_P as full_first_ug_inst_aid_percent
              , StudentFinancialAid_StudentFinancial.IGRNT_A as full_first_ug_inst_aid_amount
              , StudentFinancialAid_StudentFinancial.PGRNT_N as full_first_ug_pell_grant_total
              , StudentFinancialAid_StudentFinancial.PGRNT_P as full_first_ug_pell_grant_percent
              , StudentFinancialAid_StudentFinancial.PGRNT_A as full_first_ug_pell_grant_amount
              , StudentFinancialAid_StudentFinancial.LOAN_N as full_first_ug_student_loan_total
              , StudentFinancialAid_StudentFinancial.LOAN_P as full_first_ug_student_loan_percent
              , StudentFinancialAid_StudentFinancial.LOAN_A as full_first_ug_student_loan_amount
              , GraduationRates_Frequent.GRRTTOT AS graduation_rate_total
              , FallEnrollment_TotalEnteringClass.RET_PCF as full_time_retention_rate
              , FallEnrollment_TotalEnteringClass.RET_PCP as part_time_retention_rate
              , FallEnrollment_TotalEnteringClass.STUFACR as student_to_faculty
              , InstitutionalCharacteristics_EducationalOfferings.SLO7 as weekend_eve_college
              , InstitutionalCharacteristics_EducationalOfferings.SLO8 as teacher_certification
              , InstitutionalCharacteristics_EducationalOfferings.DISTCRS as distance_education
              , InstitutionalCharacteristics_EducationalOfferings.SLO6 as study_abroad
              , InstitutionalCharacteristics_StudentChargesByProgram.CIPCODE1 as major_1
              , InstitutionalCharacteristics_StudentChargesByProgram.CIPCODE2 as major_2
              , InstitutionalCharacteristics_StudentChargesByProgram.CIPCODE3 as major_3
              , InstitutionalCharacteristics_StudentChargesByProgram.CIPCODE4 as major_4
              , InstitutionalCharacteristics_StudentChargesByProgram.CIPCODE5 as major_5
              , 100 - FallEnrollment_Frequent.PCUENRW as ug_men_percent
              , FallEnrollment_Frequent.PCUENRW as ug_women_percent
              , FallEnrollment_Frequent.RMINSTTP as ug_in_state_percent
              , FallEnrollment_Frequent.RMOUSTTP as ug_out_state_percent
              , FallEnrollment_Frequent.RMFRGNCP as ug_foreign_percent
              , FallEnrollment_Frequent.RMUNKNWP as ug_unknown_percent
              , StudentFinancialAid_StudentFinancialAid.GIS4N12 as ug_30k_total
              , StudentFinancialAid_StudentFinancialAid.GIS4N22 as ug_30k_48k_total
              , StudentFinancialAid_StudentFinancialAid.GIS4N32 as ug_48k_75k_total
              , StudentFinancialAid_StudentFinancialAid.GIS4N42 as ug_75k_110k_total
              , FallEnrollment_Frequent.PCUENRBK as ug_african_percent
              , FallEnrollment_Frequent.PCUENRAS as ug_asian_percent
              , FallEnrollment_Frequent.PCUENRHS as ug_hispanic_percent
              , FallEnrollment_Frequent.PCUENRNR as ug_non_resident_percent
              , FallEnrollment_Frequent.PCUENR2M as ug_multiracial_percent
              , FallEnrollment_Frequent.PCUENRAN as ug_native_percent
              , FallEnrollment_Frequent.PCUENRNH as ug_pacific_percent
              , FallEnrollment_Frequent.PCUENRUN as ug_unknown_race_percent
              , FallEnrollment_Frequent.PCUENRWH as ug_white_percent
              , 2017 as year
              , year(curdate()) - 2017 as data_year_difference
              , CustomRonColleges.avg_graduation_year
              , CustomRonColleges.need_met
              , CustomRonColleges.gift_aid
              , CustomRonColleges.self_help
              , CustomRonColleges.inflation
              , ifnull(CustomRonColleges.inflation_year_1,CustomRonColleges.inflation) as inflation_year_1
              , ifnull(CustomRonColleges.inflation_year_2,CustomRonColleges.inflation) as inflation_year_2
              , ifnull(CustomRonColleges.inflation_year_3,CustomRonColleges.inflation) as inflation_year_3
              , ifnull(CustomRonColleges.inflation_year_4,CustomRonColleges.inflation) as inflation_year_4
              , ifnull(CustomRonColleges.inflation_year_5,CustomRonColleges.inflation) as inflation_year_5
              , (year(curdate()) + 0) as year_1
              , (year(curdate()) + 1) as year_2
              , (year(curdate()) + 2) as year_3
              , (year(curdate()) + 3) as year_4
              , (year(curdate()) + 4) as year_5
              , (ifnull(InstitutionalCharacteristics_Frequent.cindon, ifnull(InstitutionalCharacteristics_Frequent.cindoff, InstitutionalCharacteristics_Frequent.cindfam)) * pow((1 + ifnull(CustomRonColleges.inflation_year_1, CustomRonColleges.inflation)),(year(CURDATE()) + 0) - 2017)) as private_cost_year_1
              , (ifnull(InstitutionalCharacteristics_Frequent.cindon, ifnull(InstitutionalCharacteristics_Frequent.cindoff, InstitutionalCharacteristics_Frequent.cindfam)) * pow((1 + ifnull(CustomRonColleges.inflation_year_2, CustomRonColleges.inflation)),(year(CURDATE()) + 1) - 2017)) as private_cost_year_2
              , (ifnull(InstitutionalCharacteristics_Frequent.cindon, ifnull(InstitutionalCharacteristics_Frequent.cindoff, InstitutionalCharacteristics_Frequent.cindfam)) * pow((1 + ifnull(CustomRonColleges.inflation_year_3, CustomRonColleges.inflation)),(year(CURDATE()) + 2) - 2017)) as private_cost_year_3
              , (ifnull(InstitutionalCharacteristics_Frequent.cindon, ifnull(InstitutionalCharacteristics_Frequent.cindoff, InstitutionalCharacteristics_Frequent.cindfam)) * pow((1 + ifnull(CustomRonColleges.inflation_year_4, CustomRonColleges.inflation)),(year(CURDATE()) + 3) - 2017)) as private_cost_year_4
              , (ifnull(InstitutionalCharacteristics_Frequent.cindon, ifnull(InstitutionalCharacteristics_Frequent.cindoff, InstitutionalCharacteristics_Frequent.cindfam)) * pow((1 + ifnull(CustomRonColleges.inflation_year_5, CustomRonColleges.inflation)),(year(CURDATE()) + 4) - 2017)) as private_cost_year_5
              , (ifnull(InstitutionalCharacteristics_Frequent.cinson, ifnull(InstitutionalCharacteristics_Frequent.cinsoff, InstitutionalCharacteristics_Frequent.cinsfam)) * pow((1 + ifnull(CustomRonColleges.inflation_year_1, CustomRonColleges.inflation)),(year(CURDATE()) + 0) - 2017)) as instate_cost_year_1
              , (ifnull(InstitutionalCharacteristics_Frequent.cinson, ifnull(InstitutionalCharacteristics_Frequent.cinsoff, InstitutionalCharacteristics_Frequent.cinsfam)) * pow((1 + ifnull(CustomRonColleges.inflation_year_2, CustomRonColleges.inflation)),(year(CURDATE()) + 1) - 2017)) as instate_cost_year_2
              , (ifnull(InstitutionalCharacteristics_Frequent.cinson, ifnull(InstitutionalCharacteristics_Frequent.cinsoff, InstitutionalCharacteristics_Frequent.cinsfam)) * pow((1 + ifnull(CustomRonColleges.inflation_year_3, CustomRonColleges.inflation)),(year(CURDATE()) + 2) - 2017)) as instate_cost_year_3
              , (ifnull(InstitutionalCharacteristics_Frequent.cinson, ifnull(InstitutionalCharacteristics_Frequent.cinsoff, InstitutionalCharacteristics_Frequent.cinsfam)) * pow((1 + ifnull(CustomRonColleges.inflation_year_4, CustomRonColleges.inflation)),(year(CURDATE()) + 3) - 2017)) as instate_cost_year_4
              , (ifnull(InstitutionalCharacteristics_Frequent.cinson, ifnull(InstitutionalCharacteristics_Frequent.cinsoff, InstitutionalCharacteristics_Frequent.cinsfam)) * pow((1 + ifnull(CustomRonColleges.inflation_year_5, CustomRonColleges.inflation)),(year(CURDATE()) + 4) - 2017)) as instate_cost_year_5
              , (ifnull(InstitutionalCharacteristics_Frequent.cotson, ifnull(InstitutionalCharacteristics_Frequent.cotsoff, InstitutionalCharacteristics_Frequent.cotsfam)) * pow((1 + ifnull(CustomRonColleges.inflation_year_1, CustomRonColleges.inflation)),(year(CURDATE()) + 0) - 2017)) as outstate_cost_year_1
              , (ifnull(InstitutionalCharacteristics_Frequent.cotson, ifnull(InstitutionalCharacteristics_Frequent.cotsoff, InstitutionalCharacteristics_Frequent.cotsfam)) * pow((1 + ifnull(CustomRonColleges.inflation_year_2, CustomRonColleges.inflation)),(year(CURDATE()) + 1) - 2017)) as outstate_cost_year_2
              , (ifnull(InstitutionalCharacteristics_Frequent.cotson, ifnull(InstitutionalCharacteristics_Frequent.cotsoff, InstitutionalCharacteristics_Frequent.cotsfam)) * pow((1 + ifnull(CustomRonColleges.inflation_year_3, CustomRonColleges.inflation)),(year(CURDATE()) + 2) - 2017)) as outstate_cost_year_3
              , (ifnull(InstitutionalCharacteristics_Frequent.cotson, ifnull(InstitutionalCharacteristics_Frequent.cotsoff, InstitutionalCharacteristics_Frequent.cotsfam)) * pow((1 + ifnull(CustomRonColleges.inflation_year_4, CustomRonColleges.inflation)),(year(CURDATE()) + 3) - 2017)) as outstate_cost_year_4
              , (ifnull(InstitutionalCharacteristics_Frequent.cotson, ifnull(InstitutionalCharacteristics_Frequent.cotsoff, InstitutionalCharacteristics_Frequent.cotsfam)) * pow((1 + ifnull(CustomRonColleges.inflation_year_5, CustomRonColleges.inflation)),(year(CURDATE()) + 4) - 2017)) as outstate_cost_year_5
              , CollegeScorecardTreasuries.md_earn_wne_p10
              , CollegeScorecardTreasuries.mn_earn_wne_p10
              , CollegeScorecardTreasuries.md_earn_wne_p6
              , CollegeScorecardTreasuries.mn_earn_wne_p6
              , (CollegeScorecardTreasuries.mn_earn_wne_p6 + CollegeScorecardTreasuries.md_earn_wne_p6)/2 as avg_earn_wne_p6
              , (CollegeScorecardTreasuries.mn_earn_wne_p10 + CollegeScorecardTreasuries.md_earn_wne_p10)/2 as avg_earn_wne_p10
              , CustomRonColleges.is_college_im as is_college_im
              , if(ControlValueSet.valueLabel LIKE \"%private%\", 1, 0) as is_private
              , now() as created_at
              , now() as updated_at
              , null as deleted_at
            FROM
              {$dataDbName}.custom_ron_colleges CustomRonColleges
              JOIN {$ipedsDbName}.HD2017 InstitutionalCharacteristics_DirectoryInformation ON CustomRonColleges.unitid = InstitutionalCharacteristics_DirectoryInformation.UNITID
              LEFT JOIN {$ipedsDbName}.valuesets17 LocaleValueSet ON InstitutionalCharacteristics_DirectoryInformation.locale = LocaleValueSet.Codevalue AND LocaleValueSet.varName = \"locale\"
              LEFT JOIN {$ipedsDbName}.valuesets17 InstSizeValueSet ON InstitutionalCharacteristics_DirectoryInformation.instsize = InstSizeValueSet.Codevalue AND InstSizeValueSet.varName = \"instsize\"
              LEFT JOIN {$ipedsDbName}.IC2017 InstitutionalCharacteristics_EducationalOfferings ON CustomRonColleges.unitid = InstitutionalCharacteristics_EducationalOfferings.UNITID
              LEFT JOIN {$ipedsDbName}.valuesets17 CalSysValueSet ON InstitutionalCharacteristics_EducationalOfferings.calsys = CalSysValueSet.Codevalue AND CalSysValueSet.varName = \"calsys\"
              LEFT JOIN {$ipedsDbName}.ADM2017 Admissions_AdmissionConsiderations ON CustomRonColleges.unitid = Admissions_AdmissionConsiderations.UNITID
              LEFT JOIN {$ipedsDbName}.DRVADM2017 Admissions_Frequent ON CustomRonColleges.unitid = Admissions_Frequent.UNITID
              LEFT JOIN {$ipedsDbName}.DRVEF2017 FallEnrollment_Frequent ON CustomRonColleges.unitid = FallEnrollment_Frequent.UNITID
              LEFT JOIN {$ipedsDbName}.valuesets17 admissionReq1 ON Admissions_AdmissionConsiderations.ADMCON1 = admissionReq1.Codevalue AND admissionReq1.varName = \"ADMCON1\"
              LEFT JOIN {$ipedsDbName}.valuesets17 admissionReq2 ON Admissions_AdmissionConsiderations.ADMCON2 = admissionReq2.Codevalue AND admissionReq2.varName = \"ADMCON2\"
              LEFT JOIN {$ipedsDbName}.valuesets17 admissionReq3 ON Admissions_AdmissionConsiderations.ADMCON3 = admissionReq3.Codevalue AND admissionReq3.varName = \"ADMCON3\"
              LEFT JOIN {$ipedsDbName}.valuesets17 admissionReq4 ON Admissions_AdmissionConsiderations.ADMCON4 = admissionReq4.Codevalue AND admissionReq4.varName = \"ADMCON4\"
              LEFT JOIN {$ipedsDbName}.valuesets17 admissionReq5 ON Admissions_AdmissionConsiderations.ADMCON5 = admissionReq5.Codevalue AND admissionReq5.varName = \"ADMCON5\"
              LEFT JOIN {$ipedsDbName}.valuesets17 admissionReq6 ON Admissions_AdmissionConsiderations.ADMCON6 = admissionReq6.Codevalue AND admissionReq6.varName = \"ADMCON6\"
              LEFT JOIN {$ipedsDbName}.valuesets17 admissionReq7 ON Admissions_AdmissionConsiderations.ADMCON7 = admissionReq7.Codevalue AND admissionReq7.varName = \"ADMCON7\"
              LEFT JOIN {$ipedsDbName}.valuesets17 admissionReq8 ON Admissions_AdmissionConsiderations.ADMCON8 = admissionReq8.Codevalue AND admissionReq8.varName = \"ADMCON8\"
              LEFT JOIN {$ipedsDbName}.valuesets17 admissionReq9 ON Admissions_AdmissionConsiderations.ADMCON9 = admissionReq9.Codevalue AND admissionReq9.varName = \"ADMCON9\"
              LEFT JOIN {$ipedsDbName}.DRVIC2017 InstitutionalCharacteristics_Frequent ON CustomRonColleges.unitid = InstitutionalCharacteristics_Frequent.UNITID
              LEFT JOIN {$ipedsDbName}.IC2017_AY InstitutionalCharacteristics_StudentCharges ON CustomRonColleges.unitid = InstitutionalCharacteristics_StudentCharges.UNITID
              LEFT JOIN {$ipedsDbName}.SFA1617_P2 StudentFinancialAid_StudentFinancialAid ON CustomRonColleges.unitid = StudentFinancialAid_StudentFinancialAid.UNITID
              LEFT JOIN {$ipedsDbName}.SFA1617_P1 StudentFinancialAid_StudentFinancial ON CustomRonColleges.unitid = StudentFinancialAid_StudentFinancial.UNITID
              LEFT JOIN {$ipedsDbName}.DRVGR2017 GraduationRates_Frequent ON CustomRonColleges.unitid = GraduationRates_Frequent.UNITID
              LEFT JOIN {$ipedsDbName}.EF2017D FallEnrollment_TotalEnteringClass ON CustomRonColleges.unitid = FallEnrollment_TotalEnteringClass.UNITID
              LEFT JOIN {$ipedsDbName}.IC2017_PY InstitutionalCharacteristics_StudentChargesByProgram ON CustomRonColleges.unitid = InstitutionalCharacteristics_StudentChargesByProgram.UNITID
              LEFT JOIN {$dataDbName}.college_treasuries CollegeScorecardTreasuries ON CustomRonColleges.unitid = CollegeScorecardTreasuries.UNITID
              LEFT JOIN {$ipedsDbName}.valuesets17 ControlValueSet ON InstitutionalCharacteristics_DirectoryInformation.CONTROL = ControlValueSet.Codevalue AND ControlValueSet.varName = \"CONTROL\";
        ");

        // generate slug
        foreach (College::all() as $college) {
            $college->update([
                'slug' => SlugService::createSlug(College::class, 'slug', $college->name)
            ]);
        }

        $this->info(now() . ': completed: DataManipulate:EtlJobCollegesTableCommand');

        return true;
    }
}
