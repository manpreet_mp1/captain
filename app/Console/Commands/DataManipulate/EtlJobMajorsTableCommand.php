<?php

namespace App\Console\Commands\DataManipulate;

use App\Models\Data\Major\Major;
use App\Repositories\Misc\HelperRepository;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use function config;
use function now;
use function strlen;

class EtlJobMajorsTableCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DataManipulate:EtlJobMajorsTableCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'ETL Job for majors';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(now() . ': started: DataManipulate:EtlJobMajorsTableCommand');

        Model::unguard();

        $this->insertMajorDataFromCsv();

        $this->insertMajorStats();

        $this->insertMajorDegreeTypeStats();

        $this->insertMajorCollegeStats();

        $this->info(now() . ': completed: DataManipulate:EtlJobMajorsTableCommand');

        Model::unguard(false);

        return true;
    }

    private function insertMajorCollegeStats()
    {
        $ipedsDbName = config('database.connections.ipeds.database');
        $dataDbName = config('database.connections.data.database');

        DB::connection('ipeds')->select("
        INSERT into {$dataDbName}.major_colleges (code, unitid, name, slug, size_of_college, degree_count, associate_count, bachelor_count, certificate_count, doctor_count, master_count, other_count, created_at, updated_at, deleted_at) 
        SELECT
              tmp.code,
              tmp.UNITID,
              colleges.name,
              colleges.slug,
              colleges.inst_size,
              SUM(tmp.total_count) AS total_count,
              SUM(tmp.associate_count) as associate_count,
              SUM(tmp.bachelor_count) as bachelor_count,
              SUM(tmp.certificate_count) as certificate_count,
              SUM(tmp.doctor_count) as doctor_count,
              SUM(tmp.master_count) as master_count,
              SUM(tmp.other_count) as other_count,
              now() as created_at,
              now() as updated_at,
              null as deleted_at
            FROM (
                SELECT
                  m.code,
                  c.UNITID,
                  SUM(c.CTOTALT) as total_count,
                  CASE WHEN c.AWLEVEL = 3 THEN SUM(c.CTOTALT) END AS associate_count,
                  CASE WHEN c.AWLEVEL = 5 THEN SUM(c.CTOTALT) END AS bachelor_count,
                  CASE WHEN c.AWLEVEL IN (6, 8) THEN SUM(c.CTOTALT) END AS certificate_count,
                  CASE WHEN c.AWLEVEL IN (17, 18) THEN SUM(c.CTOTALT) END AS doctor_count,
                  CASE WHEN c.AWLEVEL IN (19,7) THEN SUM(c.CTOTALT) END AS master_count,
                  CASE WHEN c.AWLEVEL NOT IN (3,5,6,8,17,18,19,7) THEN SUM(c.CTOTALT) END AS other_count
                  FROM
                     {$dataDbName}.majors m
                     JOIN {$ipedsDbName}.C2017_A c ON c.CIPCODE = m.code
                  GROUP BY
                   m.code, c.AWLEVEL, c.UNITID
                ) tmp
            JOIN {$dataDbName}.colleges on colleges.unitid = tmp.UNITID
            GROUP BY
               tmp.code, tmp.UNITID
            HAVING total_count > 0;");
    }

    /**
     * Insert degree stats
     */
    private function insertMajorDegreeTypeStats()
    {
        $ipedsDbName = config('database.connections.ipeds.database');
        $dataDbName = config('database.connections.data.database');

        DB::connection('ipeds')->select("
        UPDATE
          {$dataDbName}.majors as m,
          (
          SELECT
              tmp.code,
              SUM(total_count) AS total_count,
              SUM(tmp.associate_count) as associate_count,
              SUM(tmp.bachelor_count) as bachelor_count,
              SUM(tmp.certificate_count) as certificate_count,
              SUM(tmp.doctor_count) as doctor_count,
              SUM(tmp.master_count) as master_count,
              SUM(tmp.other_count) as other_count
            FROM (
                SELECT
                  m.code,
                  SUM(c.CTOTALT) as total_count,
                  CASE WHEN c.AWLEVEL = 3 THEN SUM(c.CTOTALT) END AS associate_count,
                  CASE WHEN c.AWLEVEL = 5 THEN SUM(c.CTOTALT) END AS bachelor_count,
                  CASE WHEN c.AWLEVEL IN (6, 8) THEN SUM(c.CTOTALT) END AS certificate_count,
                  CASE WHEN c.AWLEVEL IN (17, 18, 19) THEN SUM(c.CTOTALT) END AS doctor_count,
                  CASE WHEN c.AWLEVEL IN (7) THEN SUM(c.CTOTALT) END AS master_count,
                  CASE WHEN c.AWLEVEL NOT IN (3,5,6,8,17,18,19,7) THEN SUM(c.CTOTALT) END AS other_count
                  FROM
                     {$dataDbName}.majors m
                     JOIN {$ipedsDbName}.C2017_A c ON c.CIPCODE = m.code
                  GROUP BY
                   m.code, c.AWLEVEL
                ) tmp
            GROUP BY
               tmp.code
            HAVING total_count > 0
          ) tmp2
        SET
            m.associate_count = tmp2.associate_count,
            m.associate_percent = ROUND((tmp2.associate_count/tmp2.total_count)*100),
            m.bachelor_count = tmp2.bachelor_count,
            m.bachelor_percent = ROUND((tmp2.bachelor_count/tmp2.total_count)*100),
            m.certificate_count = tmp2.certificate_count,
            m.certificate_percent = ROUND((tmp2.certificate_count/tmp2.total_count)*100),
            m.doctor_count = tmp2.doctor_count,
            m.doctor_percent = ROUND((tmp2.doctor_count/tmp2.total_count)*100),
            m.master_count = tmp2.master_count,
            m.master_percent = ROUND((tmp2.master_count/tmp2.total_count)*100),
            m.other_count = tmp2.other_count,
            m.other_percent = ROUND((tmp2.other_count/tmp2.total_count)*100)
        WHERE m.code = tmp2.code;");
    }

    /**
     * Insert all stats related to majors
     */
    private function insertMajorStats()
    {
        $ipedsDbName = config('database.connections.ipeds.database');
        $dataDbName = config('database.connections.data.database');

        DB::connection('ipeds')->select("
        UPDATE
          {$dataDbName}.majors as m,
          (SELECT
             m.code,
             SUM(c.CTOTALM) as male_count,
             ROUND(SUM(c.CTOTALM)/SUM(CTOTALT)*100) as male_percent,
             SUM(c.CTOTALW) as female_count,
             ROUND(SUM(c.CTOTALW)/SUM(CTOTALT)*100) as female_percent,
             SUM(c.CWHITT) as white_count,
             ROUND(SUM(c.CWHITT)/SUM(CTOTALT)*100) as white_percent,
             SUM(c.CAIANT) as indian_count,
             ROUND(SUM(c.CAIANT)/SUM(CTOTALT)*100) as indian_percent,
             SUM(c.CASIAT) as asian_count,
             ROUND(SUM(c.CASIAT)/SUM(CTOTALT)*100) as asian_percent,
             SUM(c.CBKAAT) as black_count,
             ROUND(SUM(c.CBKAAT)/SUM(CTOTALT)*100) as black_percent,
             SUM(c.CHISPT) as hispanic_count,
             ROUND(SUM(c.CHISPT)/SUM(CTOTALT)*100) as hispanic_percent,
             SUM(c.CNHPIT) as native_count,
             ROUND(SUM(c.CNHPIT)/SUM(CTOTALT)*100) as native_percent,
             SUM(c.C2MORT) as two_or_more_count,
             ROUND(SUM(c.C2MORT)/SUM(CTOTALT)*100) as two_or_more_percent,
             SUM(c.CNRALT) as non_resident_count,
             ROUND(SUM(c.CNRALT)/SUM(CTOTALT)*100) as non_resident_percent,
             SUM(c.CUNKNT) as unknown_count,
             ROUND(SUM(c.CUNKNT)/SUM(CTOTALT)*100) as unknown_percent
          FROM
             {$dataDbName}.majors m
             JOIN {$ipedsDbName}.C2017_A c ON c.CIPCODE = m.code
          GROUP BY m.code
           HAVING sum(CTOTALT) > 0) tmp
          SET
            m.male_count = tmp.male_count,
            m.male_percent = tmp.male_percent,
            m.female_count = tmp.female_count,
            m.female_percent = tmp.female_percent,
            m.white_count = tmp.white_count,
            m.white_percent = tmp.white_percent,
            m.indian_count = tmp.indian_count,
            m.indian_percent = tmp.indian_percent,
            m.asian_count = tmp.asian_count,
            m.asian_percent = tmp.asian_percent,
            m.black_count = tmp.black_count,
            m.black_percent = tmp.black_percent,
            m.hispanic_count = tmp.hispanic_count,
            m.hispanic_percent = tmp.hispanic_percent,
            m.native_count = tmp.native_count,
            m.native_percent = tmp.native_percent,
            m.two_or_more_count = tmp.two_or_more_count,
            m.two_or_more_percent = tmp.two_or_more_percent,
            m.non_resident_count = tmp.non_resident_count,
            m.non_resident_percent = tmp.non_resident_percent,
            m.unknown_count = tmp.unknown_count,
            m.unknown_percent = tmp.unknown_percent
        WHERE m.code = tmp.code;");
    }

    /**
     * Insert majors data from csv
     */
    private function insertMajorDataFromCsv()
    {
        Major::truncate();

        $records = (new HelperRepository())->createArrayFromCsvFile(storage_path('dataPopulate') . "/majors.csv");

        foreach ($records as $key => $value) {
            if (strlen($value['code']) === 2) {
                $type = 'main';
            }

            if (strlen($value['code']) === 5) {
                $type = 'heading';
            }

            if (strlen($value['code']) === 7) {
                $type = 'item';
            }

            $records[$key]['type'] = $type;
        }

        $recordChunks = array_chunk($records, 1000);

        foreach ($recordChunks as $chunk) {
            Major::insert($chunk);
        }

        Major::whereNotNull('id')->update([
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        // generate slug
        foreach (Major::all() as $major) {
            $major->update([
                'slug' => SlugService::createSlug(Major::class, 'slug', $major->title)
            ]);
        }
    }
}
