<?php

namespace App\Console\Commands\DataManipulate;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use function now;

class AddIndexToIpedsTableCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DataManipulate:AddIndexToIpedsTableCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add unitid index to all these tables for faster process';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(now() . ': started: DataManipulate:AddIndexToIpedsTableCommand');

        foreach ($this->getUnitIdTables() as $table) {
            $this->info(now() . ": Adding index to {$table}");
            DB::connection('ipeds')->select("ALTER TABLE {$table} ADD INDEX (UNITID);");
        }

        DB::connection('ipeds')->select("ALTER TABLE valuesets17 ADD INDEX (Codevalue);");
        DB::connection('ipeds')->select("ALTER TABLE valuesets17 ADD INDEX (varName);");

        $this->info(now() . ': completed: DataManipulate:AddIndexToIpedsTableCommand');

        return true;
    }

    /**
     * Get all the table for which we need to add the unit id index
     *
     * @return array
     */
    private function getUnitIdTables()
    {
        return [
            'ADM2017',
            'AL2017',
            'C2017_A',
            'C2017_B',
            'C2017_C',
            'C2017DEP',
            'CUSTOMCGIDS2017',
            'DRVADM2017',
            'DRVAL2017',
            'DRVC2017',
            'DRVEF122017',
            'DRVEF2017',
            'DRVF2017',
            'DRVGR2017',
            'DRVHR2017',
            'DRVIC2017',
            'DRVOM2017',
            'EAP2017',
            'EF2017',
            'EF2017A',
            'EF2017A_DIST',
            'EF2017B',
            'EF2017C',
            'EF2017D',
            'EFFY2017',
            'EFIA2017',
            'F1617_F1A',
            'F1617_F2',
            'F1617_F3',
            'FLAGS2017',
            'GR200_17',
            'GR2017',
            'GR2017_L2',
            'GR2017_PELL_SSL',
            'HD2017',
            'IC2017',
            'IC2017_AY',
            'IC2017_PY',
            'IC2017Mission',
            'OM2017',
            'S2017_IS',
            'S2017_NH',
            'S2017_OC',
            'S2017_SIS',
            'SAL2017_IS',
            'SAL2017_NIS',
            'SFA1617_P1',
            'SFA1617_P2',
            'SFAV1617',
        ];
    }
}
