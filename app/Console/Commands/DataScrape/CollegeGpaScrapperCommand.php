<?php

namespace App\Console\Commands\DataScrape;

use App\Models\Data\College\CollegeGpa;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use function now;

class CollegeGpaScrapperCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DataScrape:CollegeGpaScrapperCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrape the college GPA';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Model::unguard();

        $this->info(now() . ': started: DataScrape:CollegeGpaScrapperCommand');

        $this->scrapeAndSave();
        
        $this->mapAndUpdate();

        $this->info(now() . ': completed: DataScrape:CollegeGpaScrapperCommand');

        Model::unguard(false);

        return true;
    }

    private function mapAndUpdate()
    {
        DB::connection('data')->select("
            UPDATE
                college_gpas AS cg
                JOIN colleges AS c ON c.name = CONCAT(\"%\",cg.school,\"%\")
             SET
                cg.unitid = c.unitid;
        ");
    }

    /**
     * Scrape and save data to database
     */
    private function scrapeAndSave()
    {
        CollegeGpa::truncate();

        $client = new Client([
            'base_uri' => 'https://gpacalculator.net/wp-admin/admin-ajax.php'
        ]);

        $this->info(now() . ': Getting result from API');

        $bar = $this->output->createProgressBar(77);

        $bar->start();

        for ($i=1; $i<78; $i++) {
            $contentLength = $i < 10 ? 57 : 58;

            $response = $client->post('', [
                'headers' => [
                    ':authority' => 'gpacalculator.net',
                    ':method' => 'POST',
                    ':path' => '/wp-admin/admin-ajax.php',
                    ':scheme' => 'https',
                    'accept' => 'application/json, text/plain, */*',
                    'accept-encoding' => 'gzip, deflate, br',
                    'accept-language' => 'en-GB,en-US;q=0.9,en;q=0.8',
                    'content-length' => $contentLength,
                    'content-type' => 'application/x-www-form-urlencoded',
                    'cookie' => '_ga=GA1.2.61046960.1563392344; _gid=GA1.2.1969923992.1563392344; _gat=1',
                    'origin' => 'https://gpacalculator.net',
                    'referer' => 'https://gpacalculator.net/admission/',
                    'user-agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/75.0.3770.90 Chrome/75.0.3770.90 Safari/537.36',
                ],
                'form_params' => [
                    'action' => 'cdb_search',
                    'state' => 'All',
                    'score_type' => 'GPA',
                    'score' => '5',
                    'page' => $i
                ]
            ]);

            foreach (json_decode($response->getBody()->getContents())->rows as $row) {
                CollegeGpa::create([
                    'school' => $row->school,
                    'state' => $row->state,
                    'sat25' => $row->sat25,
                    'sat75' => $row->sat75,
                    'act25' => $row->act25,
                    'act75' => $row->act75,
                    'gpa' => $row->gpa,
                    'acceptance_rate' => $row->acceptance_rate,
                ]);
            }

            $bar->advance();
        }

        $bar->finish();
    }
}
