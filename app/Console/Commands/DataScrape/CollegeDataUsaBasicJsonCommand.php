<?php

namespace App\Console\Commands\DataScrape;

use App\Models\Data\College\College;
use Illuminate\Console\Command;
use function file_get_contents;
use function now;

class CollegeDataUsaBasicJsonCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DataScrape:CollegeDataUsaBasicJsonCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download basic json from datausa.io to college';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(now() . ': started: DataScrape:CollegeDataUsaBasicJsonCommand');

        $bar = $this->output->createProgressBar(College::whereNull('data_usa')->count());

        foreach (College::whereNull('data_usa')->get() as $college) {
            $bar->advance();

            $id = $college->unitid;

            $url = "https://graphite.datausa.io/api/profile/university/{$id}";

            $contents = file_get_contents($url);

            $college->update([
                'data_usa' => $contents
            ]);
        }

        $bar->finish();

        $this->info(now() . ': completed: DataScrape:CollegeDataUsaBasicJsonCommand');

        return true;
    }
}
