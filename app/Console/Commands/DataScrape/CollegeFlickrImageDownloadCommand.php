<?php

namespace App\Console\Commands\DataScrape;

use App\Models\Data\College\College;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use function file_get_contents;
use function now;

class CollegeFlickrImageDownloadCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DataScrape:CollegeFlickrImageDownloadCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download college flickr images to local. Need to be manually uploaded to s3';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(now() . ': started: DataScrape:CollegeFlickrImageDownloadCommand');

        $bar = $this->output->createProgressBar(College::count());

        foreach (College::all() as $college) {
            $bar->advance();

            $id = $college->unitid;

            $path = 'splash/' . $id . '.jpeg';

            $url = "https://datausa.io/api/profile/university/{$id}/splash";

            $contents = file_get_contents($url);

            Storage::put($path, $contents);
        }

        $bar->finish();

        $this->info(now() . ': completed: DataScrape:CollegeFlickrImageDownloadCommand');

        return true;
    }
}
