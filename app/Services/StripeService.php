<?php

namespace App\Services;

use App\Models\Family\Family;
use Exception;
use Stripe\Coupon;
use Stripe\Customer;
use Stripe\Order;
use Stripe\Plan;
use Stripe\SKU;
use Stripe\Stripe;
use Stripe\Subscription;
use function array_merge;
use function config;
use function now;

class StripeService
{
    /**
     * Set the Stripe API key
     */
    public function __construct()
    {
        Stripe::setApiKey(config('services.stripe.secret'));
    }

    /**
     * Retrieves the details of an existing SKU. Supply the unique SKU identifier
     * from either a SKU creation request or from the product, and
     * Stripe will return the corresponding SKU information.
     *
     * @param $sku [The identifier of the SKU to be retrieved.]
     * @return SKU
     */
    public function retrieveASku($sku)
    {
        return SKU::retrieve($sku);
    }

    /**
     * Retrieves the plan with the given ID.
     *
     * @param $id
     * @return Plan
     */
    public function retrieveAPlan($id)
    {
        return Plan::retrieve($id);
    }

    /**
     * Retrieves the coupon with the given ID.
     *
     * @param $coupon
     * @return mixed
     */
    public function retrieveACoupon($coupon)
    {
        try {
            return Coupon::retrieve($coupon);
        } catch (Exception $exception) {
            return false;
        }
    }

    /**
     * Create a customer
     *
     * @param Family $family
     * @return Customer
     */
    public function createACustomer(Family $family)
    {
        $parenOne = $family->parentOne()->user;

        $customer = Customer::create([
            'name' => $parenOne->full_name,
            'email' => $parenOne->email,
            "description" => "Customer for {$parenOne->email}",
            'metadata' => [
                'family_id' => $family->id,
                'family_name' => $family->name_text,
            ]
        ]);

        $family->update([
            'payment_customer_id' => $customer->id
        ]);

        return $customer;
    }

    /**
     * When you create a new credit card, you must specify a customer or recipient on which to create it.
     *
     * @param Family $family
     * @param $stripeToken
     * @return bool
     */
    public function createACard(Family $family, $stripeToken)
    {
        Customer::createSource(
            $family->payment_customer_id,
            [
                'source' => $stripeToken
            ]
        );

        return true;
    }

    /**
     * Create and pay a order
     *
     * @param Family $family
     * @param $sku
     * @return array
     */
    public function createAndPayAOrder(Family $family, $sku)
    {
        // create an order
        $initialOrder = [
            "items" => [
                [
                    "type" => "sku",
                    "parent" => $sku
                ]
            ],
            "currency" => "usd",
        ];

        // has coupon code
        if ($family->coupon_code) {
            $initialOrder = array_merge($initialOrder, [
                'coupon' => $family->coupon_code
            ]);
        }

        try {
            $order = Order::create($initialOrder);

            $order->pay([
                'customer' => $family->payment_customer_id
            ]);

            return [
                'success' => true
            ];
        } catch (Exception $exception) {
            return [
                'success' => false,
                'error' => $exception->getMessage()
            ];
        }
    }

    /**
     * Create a subscription
     *
     * @param Family $family
     * @param $planId
     * @param null $months
     * @return array
     */
    public function createASubscription(Family $family, $planId, $months = null)
    {
        $initialOrder = [
            'customer' => $family->payment_customer_id,
            "items" => [
                [
                    "plan" => $planId,
                ],
            ],
            'prorate' => false,
        ];

        // has coupon code
        if ($family->coupon_code) {
            $initialOrder = array_merge($initialOrder, [
                'coupon' => $family->coupon_code
            ]);
        }

        // if months are set
        if ($months) {
            $initialOrder = array_merge($initialOrder, [
                'cancel_at' => now()->addMonthsNoOverflow($months - 1)->addDay()->timestamp
            ]);
        }

        try {
            Subscription::create($initialOrder);

            return [
                'success' => true
            ];
        } catch (Exception $exception) {
            return [
                'success' => false,
                'error' => $exception->getMessage()
            ];
        }
    }
}
