<?php
namespace App\Services;

use AcuityScheduling;
use App\Models\Core\User;
use App\Models\Family\Appointment;
use function base64_encode;
use function config;
use function route;

class AcuityService
{
    private $service;

    /**
     * AcuityService constructor.
     */
    public function __construct()
    {
        $this->service = new AcuityScheduling([
            'userId' => config('services.acuity.userId'),
            'apiKey' => config('services.acuity.apiKey'),
        ]);
    }

    /**
     * Get a single appointment by ID.
     *
     * @param $appointmentID
     * @return array|bool|mixed|string
     */
    public function getASingleAppointmentById($appointmentID)
    {
        return $this->service->request("/appointments/{$appointmentID}");
    }

    /**
     * Update an appointment details from a white-list of updatable attributes.
     *
     * @param $appointmentID
     * @param $formData
     * @return array|bool|mixed|string
     */
    public function updateAnAppointmentById($appointmentID, $formData)
    {
        return $this->service->request("/appointments/{$appointmentID}", [
            'method' => 'PUT',
            'data' => $formData
        ]);
    }

    /**
     * Update NCSA Internal Form Data with hash of email
     *
     * @param $appointmentId
     * @return void
     */
    public function updateNCSAInternalDataForm($appointmentId)
    {
        // Get the appointment
        $appointment = $this->getASingleAppointmentById($appointmentId);

        // Update the appointment from
        $formData  = [
            'fields' => [
                [
                    'id' => config('scripts.acuity.financialCoachAssessmentResponseFieldId'),
                    'value' => route('scripts.financialCoachAssessmentResponse', ['record' => base64_encode($appointment['email'])]),
                ],
            ],
        ];

        $this->updateAnAppointmentById($appointmentId, $formData);
    }

    /**
     * Return dates with availability for a month and an appointment type.
     *
     * @param $month
     * @param $appointmentTypeID
     * @param $timezone
     * @return array|bool|mixed|string
     */
    public function getAvailabilityDatesForAppointmentType($month, $appointmentTypeID, $timezone)
    {
        return $this->service->request("/availability/dates", [
            'method' => 'GET',
            'query' => [
                'month' => $month,
                'appointmentTypeID' => $appointmentTypeID,
                'timezone' => $timezone,
            ]
        ]);
    }

    /**
     * Return dates with availability for a month and an appointment type.
     *
     * @param $date
     * @param $appointmentTypeID
     * @param $timezone
     * @return array|bool|mixed|string
     */
    public function getAvailabilityTimesForAppointmentType($date, $appointmentTypeID, $timezone)
    {
        return $this->service->request("/availability/times", [
            'method' => 'GET',
            'query' => [
                'date' => $date,
                'appointmentTypeID' => $appointmentTypeID,
                'timezone' => $timezone,
            ]
        ]);
    }

    /**
     * Create an appointment
     *
     * @param $datetime
     * @param $appointmentTypeID
     * @param $timezone
     * @param User $user
     * @return array|bool|mixed|string]
     */
    public function createAnAppointment($datetime, $appointmentTypeID, $timezone, User $user)
    {
        $appointment = $this->service->request("/appointments", [
            'method' => 'POST',
            'data' => [
                'datetime' => $datetime,
                'appointmentTypeID' => $appointmentTypeID,
                'timezone' => $timezone,
                'firstName' => $user->first_name,
                'lastName' => $user->last_name,
                'email' => $user->email,
                'phone' => $user->mobile,
                'fields' => [
                    ['id' =>  6610393, 'value' => $user->family_id],
                    ['id' =>  6610402, 'value' => $user->family->name_text],
                ]
            ]
        ]);

        (new Appointment())->create([
            'id' => $appointment['id'],
            'name' => $appointment['type'],
            'family_id' => $user->family_id,
            'acuity_calendar_id' => $appointment['calendarID'],
            'appointment_date' => $appointment['date'],
            'appointment_start_time' => $appointment['time'],
            'appointment_end_time' => $appointment['endTime'],
            'appointment_datetime' => $appointment['datetime'],
            'client_timezone' => $appointment['timezone'],
            'duration_minutes' => $appointment['duration'],
            'is_canceled' => (int)$appointment['canceled'],
        ]);

        return $appointment;
    }
}
