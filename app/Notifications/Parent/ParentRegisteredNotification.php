<?php
namespace App\Notifications\Parent;

use App\Models\Core\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;

class ParentRegisteredNotification extends Notification implements ShouldQueue
{
    use Queueable;

    protected $user;

    /**
     * Create a new notification instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            'slack',
        ];
    }
    
    /**
     * Get the mail representation of the notification.
     *
     * @param $notifiable
     * @return SlackMessage
     */
    public function toSlack($notifiable)
    {
        $user = $this->user;

        return (new SlackMessage())
            ->to(config('slack.notification_channel'))
            ->success()
            ->content('New Parent Registered!')
            ->attachment(function ($attachment) use ($user) {
                $attachment->title('Parent Details')
                    ->fields([
                        'id' => $user->id,
                        'name' => $user->full_name,
                        'email' => $user->email,
                        'mobile' => $user->formatted_mobile,
                    ]);
            });
    }
}
