<?php

namespace App\Models\Wordpress;

use function array_key_exists;
use Corcel\Model\Post as PostAlias;
use Illuminate\Database\Query\Builder;
use function strip_tags;
use function substr;

/**
 * App\Models\Wordpress\Post
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Corcel\Model\Post[] $attachment
 * @property-read \Corcel\Model\User $author
 * @property-read \Illuminate\Database\Eloquent\Collection|\Corcel\Model\Post[] $children
 * @property-read \Illuminate\Database\Eloquent\Collection|\Corcel\Model\Comment[] $comments
 * @property-read \Corcel\Model\Collection\MetaCollection|\Corcel\Model\Meta\PostMeta[] $fields
 * @property-read \AdvancedCustomFields $acf
 * @property-read string $content
 * @property-read string $excerpt
 * @property-read string $image
 * @property-read array $keywords
 * @property-read string $keywords_str
 * @property-read string $main_category
 * @property-read string $strategy_intro_text
 * @property-read array $terms
 * @property-read \Corcel\Model\Collection\MetaCollection|\Corcel\Model\Meta\PostMeta[] $meta
 * @property-read \Corcel\Model\Post $parent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Corcel\Model\Post[] $revision
 * @property-read \Illuminate\Database\Eloquent\Collection|\Corcel\Model\Taxonomy[] $taxonomies
 * @property-read \Corcel\Model\Meta\ThumbnailMeta $thumbnail
 * @method static \Illuminate\Database\Eloquent\Builder|\Corcel\Model\Post hasMeta($meta, $value = null, $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\Corcel\Model\Post hasMetaLike($meta, $value = null)
 * @method static \App\Models\Wordpress\PostBuilder|\App\Models\Wordpress\Post newModelQuery()
 * @method static \App\Models\Wordpress\PostBuilder|\App\Models\Wordpress\Post newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Corcel\Model\Post newest()
 * @method static \Illuminate\Database\Eloquent\Builder|\Corcel\Model\Post oldest()
 * @method static \App\Models\Wordpress\PostBuilder|\App\Models\Wordpress\Post query()
 * @mixin \Eloquent
 * @property int $ID
 * @property int $post_author
 * @property \Illuminate\Support\Carbon $post_date
 * @property \Illuminate\Support\Carbon $post_date_gmt
 * @property string $post_content
 * @property string $post_title
 * @property string $post_excerpt
 * @property string $post_status
 * @property string $comment_status
 * @property string $ping_status
 * @property string $post_password
 * @property string $post_name
 * @property string $to_ping
 * @property string $pinged
 * @property \Illuminate\Support\Carbon $post_modified
 * @property \Illuminate\Support\Carbon $post_modified_gmt
 * @property string $post_content_filtered
 * @property int $post_parent
 * @property string $guid
 * @property int $menu_order
 * @property string $post_type
 * @property string $post_mime_type
 * @property int $comment_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wordpress\Post whereCommentCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wordpress\Post whereCommentStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wordpress\Post whereGuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wordpress\Post whereID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wordpress\Post whereMenuOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wordpress\Post wherePingStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wordpress\Post wherePinged($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wordpress\Post wherePostAuthor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wordpress\Post wherePostContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wordpress\Post wherePostContentFiltered($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wordpress\Post wherePostDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wordpress\Post wherePostDateGmt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wordpress\Post wherePostExcerpt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wordpress\Post wherePostMimeType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wordpress\Post wherePostModified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wordpress\Post wherePostModifiedGmt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wordpress\Post wherePostName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wordpress\Post wherePostParent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wordpress\Post wherePostPassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wordpress\Post wherePostStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wordpress\Post wherePostTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wordpress\Post wherePostType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wordpress\Post whereToPing($value)
 */
class Post extends PostAlias
{
    /**
     * @param Builder $query
     * @return PostBuilder
     */
    public function newEloquentBuilder($query)
    {
        return new PostBuilder($query);
    }

    /**
     * Strategy intro text for view
     *
     * @return string
     */
    public function getStrategyIntroTextAttribute()
    {
        return strip_tags(substr($this->attributes['post_content'], 0, 300) . '[ ]...');
    }

    /**
     * Get a strategy rating div
     *
     * @param $categories
     * @return string
     */
    public function getStrategyRating($categories)
    {
        if (array_key_exists("strategies-rating-5", $categories)) {
            return '<i class="fa fa-fw fa-star text-warning"></i> <i class="fa fa-fw fa-star text-warning"></i> <i class="fa fa-fw fa-star text-warning"></i> <i class="fa fa-fw fa-star text-warning"></i> <i class="fa fa-fw fa-star text-warning"></i>';
        }

        if (array_key_exists("strategies-rating-4", $categories)) {
            return '<i class="fa fa-fw fa-star text-warning"></i> <i class="fa fa-fw fa-star text-warning"></i> <i class="fa fa-fw fa-star text-warning"></i> <i class="fa fa-fw fa-star text-warning"></i>';
        }

        if (array_key_exists("strategies-rating-3", $categories)) {
            return '<i class="fa fa-fw fa-star text-warning"></i> <i class="fa fa-fw fa-star text-warning"></i> <i class="fa fa-fw fa-star text-warning"></i>';
        }

        if (array_key_exists("strategies-rating-2", $categories)) {
            return '<i class="fa fa-fw fa-star text-warning"></i> <i class="fa fa-fw fa-star text-warning"></i>';
        }

        if (array_key_exists("strategies-rating-1", $categories)) {
            return '<i class="fa fa-fw fa-star text-warning"></i>';
        }
    }
}
