<?php

namespace App\Models\Wordpress;

use function collect;
use Corcel\Model\Taxonomy;
use Corcel\Model\Term;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class Category extends Taxonomy
{
    /**
     * Used to set the post's type
     */
    protected $taxonomy = 'category';

    /**
     * Get all the sub category for the main category
     *
     * @return Collection
     */
    public function scopeSubCategories()
    {
        $termId = $this->attributes['term_id'];
        $subCategories = collect(Arr::pluck(self::where('parent', $termId)->get()->ToArray(), 'term'));

        // get the slug of this category from term
        $term = Term::select('slug')->where('term_id', $termId)->first();

        // for rating we need to sort by Desc
        if ($term['slug'] == "strategies-rating") {
            $subCategories = $subCategories->sortByDesc('name');
        } else {
            $subCategories = $subCategories->sortBy('name');
        }

        return $subCategories;
    }

    /**
     * Scope to filter subcategories via slug
     *
     * @return Collection
     */
    public function scopeSubCategoriesSlug()
    {
        $termId  = $this->attributes['term_id'];
        $subCategories = collect(Arr::pluck(self::where('parent', $termId)->get()->ToArray(), 'term.slug'));
        $subCategories = $subCategories->sortBy('name');

        return $subCategories;
    }

    /**
     * Get rating start system
     *
     * @param $name
     * @param $slug
     * @return string
     */
    public static function getRatingCategoryStars($name, $slug)
    {
        if ($slug =="strategies-rating-5") {
            return '<i class="fa fa-fw fa-star text-warning"></i> <i class="fa fa-fw fa-star text-warning"></i> <i class="fa fa-fw fa-star text-warning"></i> <i class="fa fa-fw fa-star text-warning"></i> <i class="fa fa-fw fa-star text-warning"></i>';
        }
        if ($slug =="strategies-rating-4") {
            return '<i class="fa fa-fw fa-star text-warning"></i> <i class="fa fa-fw fa-star text-warning"></i> <i class="fa fa-fw fa-star text-warning"></i> <i class="fa fa-fw fa-star text-warning"></i>';
        }
        if ($slug =="strategies-rating-3") {
            return '<i class="fa fa-fw fa-star text-warning"></i> <i class="fa fa-fw fa-star text-warning"></i> <i class="fa fa-fw fa-star text-warning"></i>';
        }
        if ($slug =="strategies-rating-2") {
            return '<i class="fa fa-fw fa-star text-warning"></i> <i class="fa fa-fw fa-star text-warning"></i>';
        }
        if ($slug =="strategies-rating-1") {
            return '<i class="fa fa-fw fa-star text-warning"> </i>';
        }

        return $name;
    }
}
