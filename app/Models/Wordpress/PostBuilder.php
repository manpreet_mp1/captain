<?php

namespace App\Models\Wordpress;

use Corcel\Model\Builder\PostBuilder as PostBuilderAlias;
use Illuminate\Database\Eloquent\Builder;

class PostBuilder extends PostBuilderAlias
{
    /**
     * Filter post by multiple categories
     *
     * @param $slug
     * @return PostBuilder|Builder
     */
    public function catSlug(array $slug)
    {
        return $this->whereHas('taxonomies', function ($query) use ($slug) {
            return $query->where('taxonomy', 'category')->whereHas('term', function ($query) use ($slug) {
                return $query->whereIn('slug', $slug);
            });
        });
    }
}
