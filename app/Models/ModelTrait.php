<?php

namespace App\Models;

use App\Repositories\Misc\HelperRepository;
use function number_format;

trait ModelTrait
{
    /**
     * Get formatted columns in percent
     * This is done to avoid numerous getters
     *
     * @param $column
     * @param string $default
     * @return string
     */
    public function getPercent($column, $default = '-')
    {
        return $this->$column ? $this->$column . '%' : $default;
    }

    /**
     * Get formatted columns in dollar
     * This is done to avoid numerous getters
     *
     * @param $column
     * @param string $default
     * @return string
     */
    public function getDollar($column, $default = "-")
    {
        return $this->$column ? '$' . number_format($this->$column, 0) : $default;
    }

    /**
     * Get formatted columns in number format
     * This is done to avoid numerous getters
     *
     * @param $column
     * @param string $default
     * @return string
     */
    public function getNumber($column, $default = "-")
    {
        return $this->$column ? number_format($this->$column, 0) : $default;
    }

    /**
     * Get Yes/No Value from integer
     *
     * @param $column
     * @param string $default
     * @return string
     */
    public function getYesNo($column, $default = "-")
    {
        if ($this->$column) {
            return $this->$column === 1 ? "Yes" : "No";
        }

        return $default;
    }

    /**
     * Get external url
     *
     * @param $column
     * @param string $default
     * @return string
     */
    public function getUrl($column, $default="#")
    {
        if ($this->$column) {
            return (new HelperRepository())->addHttp($this->$column);
        }

        return $default;
    }
}
