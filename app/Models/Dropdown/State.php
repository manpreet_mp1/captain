<?php

namespace App\Models\Dropdown;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\Dropdown\State
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dropdown\State newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dropdown\State newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dropdown\State onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dropdown\State query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dropdown\State withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dropdown\State withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $abbreviation
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dropdown\State whereAbbreviation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dropdown\State whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dropdown\State whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dropdown\State whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dropdown\State whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dropdown\State whereUpdatedAt($value)
 */
class State extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'abbreviation',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
