<?php

namespace App\Models\Dropdown;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use function now;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\Dropdown\InputValue
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dropdown\InputValue additionalIncomeSources()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dropdown\InputValue employmentStatus()
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dropdown\InputValue gradeLevel()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dropdown\InputValue householdRole()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dropdown\InputValue maritalStatus()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dropdown\InputValue newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dropdown\InputValue newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dropdown\InputValue onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dropdown\InputValue query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dropdown\InputValue relationshipToParent()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dropdown\InputValue withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dropdown\InputValue withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string $type
 * @property string $label
 * @property string $value
 * @property string|null $image
 * @property int $order_seq
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dropdown\InputValue whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dropdown\InputValue whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dropdown\InputValue whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dropdown\InputValue whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dropdown\InputValue whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dropdown\InputValue whereOrderSeq($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dropdown\InputValue whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dropdown\InputValue whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dropdown\InputValue whereValue($value)
 */
class InputValue extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'label',
        'value',
        'image',
        'order_seq',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * Get all values related to employment status
     *
     * @param $query
     * @return mixed
     */
    public function scopeEmploymentStatus($query)
    {
        return $query->where('type', 'EMPLOYMENT_STATUS');
    }

    /**
     * Get all values related to grade level
     *
     * @param $query
     * @return mixed
     */
    public function scopeGradeLevel($query)
    {
        return $query->where('type', 'GRADE_LEVEL');
    }

    /**
     * Get all values related to relationship to parents
     *
     * @param $query
     * @return mixed
     */
    public function scopeRelationshipToParent($query)
    {
        return $query->where('type', 'RELATIONSHIP_TO_PARENT');
    }

    /**
     * Get all values related to household roles
     *
     * @param $query
     * @return mixed
     */
    public function scopeHouseholdRole($query)
    {
        return $query->where('type', 'HOUSEHOLD_ROLE');
    }

    /**
     * Get all values related to marital status
     *
     * @param $query
     * @return mixed
     */
    public function scopeMaritalStatus($query)
    {
        return $query->where('type', 'MARITAL_STATUS');
    }

    /**
     * Get all values related to additional income sources
     *
     * @param $query
     * @return mixed
     */
    public function scopeAdditionalIncomeSources($query)
    {
        return $query->where('type', 'ADDITIONAL_INCOME_SOURCES');
    }

    /**
     * Get all values related to timezone
     *
     * @param $query
     * @return mixed
     */
    public function scopeTimezones($query)
    {
        return $query->where('type', 'TIMEZONE');
    }

    /**
     * Get number dropdown
     *
     * @param int $start
     * @param int $end
     * @return array
     */
    public static function getDropdownNumber($start = 1, $end = 10)
    {
        $numberDropdown = [];

        for ($i = $start; $i <= $end; $i++) {
            $numberDropdown[$i] = $i;
        }

        return $numberDropdown;
    }

    /**
     * Get efc year dropdown
     *
     * @return array
     */
    public static function getEfcYearDropdown()
    {
        $currentYear = now()->year;
        $currentYearPlusOne = $currentYear + 1;
        $currentYearPlusTwo = $currentYear + 2;
        $currentYearPlusThree = $currentYear + 3;
        $currentYearPlusFour = $currentYear + 4;

        return [
            "$currentYear - $currentYearPlusOne" => "$currentYear - $currentYearPlusOne",
            "$currentYearPlusOne - $currentYearPlusTwo" => "$currentYearPlusOne - $currentYearPlusTwo",
            "$currentYearPlusTwo - $currentYearPlusThree" => "$currentYearPlusTwo - $currentYearPlusThree",
            "$currentYearPlusThree - $currentYearPlusFour" => "$currentYearPlusThree - $currentYearPlusFour",
        ];
    }
}
