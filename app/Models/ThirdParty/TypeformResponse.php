<?php
namespace App\Models\ThirdParty;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\ThirdParty\TypeformResponse
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdParty\TypeformResponse byEmail($email)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdParty\TypeformResponse financialCoachAssessmentForm()
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdParty\TypeformResponse newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdParty\TypeformResponse newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ThirdParty\TypeformResponse onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdParty\TypeformResponse query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ThirdParty\TypeformResponse withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ThirdParty\TypeformResponse withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string $form_id
 * @property string|null $email
 * @property array|null $data
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdParty\TypeformResponse whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdParty\TypeformResponse whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdParty\TypeformResponse whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdParty\TypeformResponse whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdParty\TypeformResponse whereFormId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdParty\TypeformResponse whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdParty\TypeformResponse whereUpdatedAt($value)
 */
class TypeformResponse extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'form_id',
        'email',
        'data',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'data' => 'json',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * Filter the records to Financial Coach Assessment
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeFinancialCoachAssessmentForm($query)
    {
        return $query->where('form_id', 'l6T6Wf');
    }

    /**
     * Filter the records By Email
     *
     * @param Builder $query
     * @param $email
     * @return Builder
     */
    public function scopeByEmail($query, $email)
    {
        return $query->where('email', $email);
    }
}
