<?php

namespace App\Models\Data\Efc;

use function config;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\Data\Efc\EfcTableA1
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA1 newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA1 newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\Efc\EfcTableA1 onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA1 query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\Efc\EfcTableA1 withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\Efc\EfcTableA1 withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property int $state_id
 * @property string $state_name
 * @property string $state_abbr
 * @property float $state_tax_allowance_14
 * @property float $state_tax_allowance_15
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA1 whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA1 whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA1 whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA1 whereStateAbbr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA1 whereStateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA1 whereStateName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA1 whereStateTaxAllowance14($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA1 whereStateTaxAllowance15($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA1 whereUpdatedAt($value)
 */
class EfcTableA1 extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $table = 'efc_table_a1';

    /**
     * Specify the connection, since this implements multitenant solution
     * Called via constructor to facilitate testing
     *
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->setConnection(config('database.data'));
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'state_id',
        'state_name',
        'state_abbr',
        'state_tax_allowance_14',
        'state_tax_allowance_15',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
