<?php

namespace App\Models\Data\Efc;

use function config;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\Data\Efc\EfcTableA2
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA2 newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA2 newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\Efc\EfcTableA2 onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA2 query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\Efc\EfcTableA2 withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\Efc\EfcTableA2 withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property int $income_work
 * @property string $description
 * @property float $min_value
 * @property float $max_value
 * @property float $percent
 * @property float $fixed
 * @property string $exceed
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA2 whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA2 whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA2 whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA2 whereExceed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA2 whereFixed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA2 whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA2 whereIncomeWork($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA2 whereMaxValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA2 whereMinValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA2 wherePercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA2 whereUpdatedAt($value)
 */
class EfcTableA2 extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $table = 'efc_table_a2';

    /**
     * Specify the connection, since this implements multitenant solution
     * Called via constructor to facilitate testing
     *
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->setConnection(config('database.data'));
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'income_work',
        'description',
        'min_value',
        'max_value',
        'percent',
        'fixed',
        'exceed',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
