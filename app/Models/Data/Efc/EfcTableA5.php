<?php

namespace App\Models\Data\Efc;

use function config;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\Data\Efc\EfcTableA5
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA5 newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA5 newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\Efc\EfcTableA5 onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA5 query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\Efc\EfcTableA5 withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\Efc\EfcTableA5 withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property int $age_older_parent
 * @property float $two_parent_allowance
 * @property float $one_parent_allowance
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA5 whereAgeOlderParent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA5 whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA5 whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA5 whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA5 whereOneParentAllowance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA5 whereTwoParentAllowance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Efc\EfcTableA5 whereUpdatedAt($value)
 */
class EfcTableA5 extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $table = 'efc_table_a5';

    /**
     * Specify the connection, since this implements multitenant solution
     * Called via constructor to facilitate testing
     *
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->setConnection(config('database.data'));
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'age_older_parent',
        'two_parent_allowance_14',
        'one_parent_allowance_15',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
