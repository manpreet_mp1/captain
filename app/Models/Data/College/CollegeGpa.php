<?php

namespace App\Models\Data\College;

use function config;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\Data\College\CollegeGpa
 *
 * @property int $id
 * @property int|null $unitid
 * @property string $school
 * @property string $state
 * @property string|null $sat25
 * @property string|null $sat75
 * @property string|null $act25
 * @property string|null $act75
 * @property string|null $gpa
 * @property string|null $acceptance_rate
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeGpa newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeGpa newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\College\CollegeGpa onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeGpa query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeGpa whereAcceptanceRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeGpa whereAct25($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeGpa whereAct75($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeGpa whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeGpa whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeGpa whereGpa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeGpa whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeGpa whereSat25($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeGpa whereSat75($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeGpa whereSchool($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeGpa whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeGpa whereUnitid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeGpa whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\College\CollegeGpa withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\College\CollegeGpa withoutTrashed()
 * @mixin \Eloquent
 */
class CollegeGpa extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    /**
     * Specify the connection, since this implements multitenant solution
     * Called via constructor to facilitate testing
     *
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->setConnection(config('database.data'));
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'unitid',
        'school',
        'state',
        'sat25',
        'sat75',
        'act25',
        'act75',
        'gpa',
        'acceptance_rate',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
