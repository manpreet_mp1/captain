<?php

namespace App\Models\Data\College;

use App\Models\ModelTrait;
use App\Repositories\Misc\HelperRepository;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use OwenIt\Auditing\Contracts\Auditable;
use function config;
use function explode;
use function json_decode;
use function number_format;
use function optional;
use function round;
use function strip_tags;

/**
 * App\Models\Data\College\College
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read mixed $avg_earn_wne_p10_text
 * @property-read mixed $custom_act_score_range
 * @property-read mixed $custom_cal_sys
 * @property-read mixed $custom_inst_size
 * @property-read mixed $custom_locale
 * @property-read mixed $custom_sat_score_range
 * @property-read string $custom_web_addr
 * @property-read string $graduation_rate_total_text
 * @property-read string $logo
 * @property-read string $original_in_state_cost_text
 * @property-read string $percent_admitted_text
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College findSimilarSlugs($attribute, $config, $slug)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\College\College onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College search(\Illuminate\Http\Request $request)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereSlug($slug)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\College\College withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\College\College withoutTrashed()
 * @mixin \Eloquent
 * @property int $unitid
 * @property string $name
 * @property string|null $slug
 * @property string $institute_type
 * @property string $address
 * @property string $city
 * @property string $state_abbr
 * @property string $zip
 * @property string $inst_nm_city institute name and city combined
 * @property string|null $web_addr
 * @property string|null $net_price_addr Net price calculator web address
 * @property string|null $locale locale - City, Rural, Suburb, Town
 * @property string|null $inst_size Size of the college
 * @property string|null $cal_sys Academic calendar system
 * @property string $opeid Federal School Code or OPEID
 * @property int|null $ug_application_fee Undergraduate application fee
 * @property int|null $applicants_total Applicants total
 * @property int|null $admissions_total Admissions total
 * @property float|null $percent_admitted Percent admitted - total, Acceptance Rate
 * @property int|null $enrolled_total Enrolled total
 * @property float|null $admissions_yield Admissions yield - full time
 * @property int|null $undergraduate_enrollment Undergraduate enrollment
 * @property int|null $act_comp_25 ACT Composite 25th percentile score
 * @property int|null $act_comp_75 ACT Composite 75th percentile score
 * @property int|null $act_eng_25 ACT English 25th percentile score
 * @property int|null $act_eng_75 ACT English 75th percentile score
 * @property int|null $act_math_25 ACT Math 25th percentile score
 * @property int|null $act_math_75 ACT Math 75th percentile score
 * @property int|null $act_submitted_total Number of first-time degree/certificate-seeking students submitting ACT scores
 * @property int|null $act_submitted_percent Percent of first-time degree/certificate-seeking students submitting ACT scores
 * @property int|null $sat_read_25 SAT Critical Reading 25th percentile score
 * @property int|null $sat_read_75 SAT Critical Reading 75th percentile score
 * @property int|null $sat_math_25 SAT Math 25th percentile score
 * @property int|null $sat_math_75 SAT Math 75th percentile score
 * @property int|null $sat_submitted_total Number of first-time degree/certificate-seeking students submitting SAT scores
 * @property int|null $sat_submitted_percent Percent of first-time degree/certificate-seeking students submitting SAT scores
 * @property string|null $admission_req_gpa Secondary school GPA
 * @property string|null $admission_req_rank Secondary school rank
 * @property string|null $admission_req_record Secondary school record
 * @property string|null $admission_req_comp_prep_prog Completion of college-preparatory program
 * @property string|null $admission_req_rec Recommendations
 * @property string|null $admission_req_comp Formal demonstration of competencies
 * @property string|null $admission_req_test_score Admission test scores
 * @property string|null $admission_req_toefl TOEFL (Test of English as a Foreign Language
 * @property string|null $admission_req_other_test Other Test (Wonderlic, WISC-III, etc.)
 * @property int|null $original_in_state_cost Total price for in-state students living on campus/off campus/with parents if earlier is null
 * @property int|null $original_out_state_cost Total price for out-state-district students living on campus/off campus/with parents if earlier is null
 * @property int|null $original_private_cost Total price for private-state students living on campus/off campus/with parents if earlier is null
 * @property int|null $on_camp_room_board On campus, room and board
 * @property int|null $on_camp_other On campus, other expenses
 * @property int|null $off_camp_not_family_room_board Off campus (not with family), room and board
 * @property int|null $off_camp_not_family_other Off campus (not with family), other expenses
 * @property int|null $off_camp_with_family_other Off campus (with family), other expenses
 * @property int|null $books_supplies Books and supplies
 * @property int|null $tuition_guaranteed_plan Tuition guaranteed plan
 * @property int|null $prepaid_tuition_plan Prepaid tuition plan
 * @property int|null $tuition_payment_plan Tuition payment plan
 * @property int|null $other_alternative_tuition_plan Other alternative tuition plan
 * @property int|null $net_price Average net price-students receiving grant or scholarship aid, paying the in-state or in-district tuition rate
 * @property int|null $net_price_0_30k Average net price (income 0-30,000)-students awarded Title IV federal financial aid
 * @property int|null $net_price_30k_48k Average net price (income 30,001-48,000)-students awarded Title IV federal financial aid
 * @property int|null $net_price_48k_75k Average net price (income 48,001-75,000)-students awarded Title IV federal financial aid
 * @property int|null $net_price_75k_110k Average net price (income 75,001-110,000)-students awarded Title IV federal financial aid
 * @property int|null $net_price_110k_plus Average net price (income over 110,000)-students awarded Title IV federal financial aid
 * @property int|null $net_price_75k_plus Custom: Average net price income more than 75001
 * @property int|null $full_first_ug_any_aid_total Number of full-time first-time undergraduates awarded federal, state, local or institutional grant aid
 * @property int|null $full_first_ug_any_aid_percent Percent of full-time first-time undergraduates awarded federal, state, local or institutional grant aid
 * @property int|null $full_first_ug_any_aid_amount Average amount of federal, state, local or institutional grant aid awarded
 * @property int|null $full_first_ug_fed_aid_total Number of full-time first-time undergraduates awarded federal grant aid
 * @property int|null $full_first_ug_fed_aid_percent Percent of full-time first-time undergraduates awarded federal grant aid
 * @property int|null $full_first_ug_fed_aid_amount Average amount of federal grant aid awarded to full-time first-time undergraduates
 * @property int|null $full_first_ug_state_aid_total Number of full-time first-time undergraduates awarded state/local grant aid
 * @property int|null $full_first_ug_state_aid_percent Percent of full-time first-time undergraduates awarded state/local grant aid
 * @property int|null $full_first_ug_state_aid_amount Average amount of state/local grant aid awarded to full-time first-time undergraduates
 * @property int|null $full_first_ug_inst_aid_total Number of full-time first-time undergraduates awarded  institutional grant aid
 * @property int|null $full_first_ug_inst_aid_percent Percent of full-time first-time undergraduates awarded institutional grant aid
 * @property int|null $full_first_ug_inst_aid_amount Average amount of institutional grant aid awarded to full-time first-time undergraduates
 * @property int|null $full_first_ug_pell_grant_total Number of undergraduate students awarded Pell grants
 * @property int|null $full_first_ug_pell_grant_percent Percent of full-time first-time undergraduates awarded Pell grants
 * @property int|null $full_first_ug_pell_grant_amount Average amount of Pell grant aid awarded to full-time first-time undergraduates
 * @property int|null $full_first_ug_student_loan_total Number of full-time first-time undergraduates awarded student loans
 * @property int|null $full_first_ug_student_loan_percent Percent of full-time first-time undergraduates awarded student loans
 * @property int|null $full_first_ug_student_loan_amount Average amount of student loans awarded to full-time first-time undergraduates
 * @property int|null $graduation_rate_total Graduation rate, total cohort
 * @property int|null $full_time_retention_rate Full-time retention rate
 * @property int|null $part_time_retention_rate Part-time retention rate
 * @property int|null $student_to_faculty Student-to-faculty ratio
 * @property int|null $weekend_eve_college Weekend/evening college
 * @property int|null $teacher_certification Teacher certification (below the postsecondary level)
 * @property int|null $distance_education Distance education courses offered
 * @property int|null $study_abroad Study abroad
 * @property string|null $major_1 CIP code of largest program
 * @property string|null $major_2 CIP code of 2nd largest program
 * @property string|null $major_3 CIP code of 3rd largest program
 * @property string|null $major_4 CIP code of 4th largest program
 * @property string|null $major_5 CIP code of 5th largest program
 * @property int|null $ug_men_percent Percent of undergraduate enrollment that are men (100 - women)
 * @property int|null $ug_women_percent Percent of undergraduate enrollment that are women
 * @property int|null $ug_in_state_percent Percent of first-time undergraduates - in-state
 * @property int|null $ug_out_state_percent Percent of first-time undergraduates - out-of-state
 * @property int|null $ug_foreign_percent Percent of first-time undergraduates - foreign countries
 * @property int|null $ug_unknown_percent Percent of first-time undergraduates - residence unknown
 * @property int|null $ug_30k_total Number in income level (0-30,000)
 * @property int|null $ug_30k_48k_total Number in income level (30,001-48,000)
 * @property int|null $ug_48k_75k_total Number in income level (48,001-75,000)
 * @property int|null $ug_75k_110k_total Number in income level (75,001-110,000)
 * @property int|null $ug_african_percent Percent of undergraduate enrollment that are Black or African American
 * @property int|null $ug_asian_percent Percent of undergraduate enrollment that are Asian
 * @property int|null $ug_hispanic_percent Percent of undergraduate enrollment that are Hispanic/Latino
 * @property int|null $ug_non_resident_percent Percent of undergraduate enrollment that are Nonresident Alien
 * @property int|null $ug_multiracial_percent Percent of undergraduate enrollment that are two or more races
 * @property int|null $ug_native_percent Percent of undergraduate enrollment that are American Indian or Alaska Native
 * @property int|null $ug_pacific_percent Percent of undergraduate enrollment that are Native Hawaiian or Other Pacific Islander
 * @property int|null $ug_unknown_race_percent Percent of undergraduate enrollment that are Race/ethnicity unknown
 * @property int|null $ug_white_percent Percent of undergraduate enrollment that are White
 * @property int $year Which year IPEDS Data do we have, this is only related to price
 * @property int $data_year_difference Year in IPEDS Data and current year
 * @property float $avg_graduation_year
 * @property float $need_met
 * @property float $gift_aid
 * @property float $self_help
 * @property float $inflation
 * @property float|null $inflation_year_1
 * @property float|null $inflation_year_2
 * @property float|null $inflation_year_3
 * @property float|null $inflation_year_4
 * @property float|null $inflation_year_5
 * @property int $year_1
 * @property int $year_2
 * @property int $year_3
 * @property int $year_4
 * @property int $year_5
 * @property int|null $private_cost_year_1
 * @property int|null $private_cost_year_2
 * @property int|null $private_cost_year_3
 * @property int|null $private_cost_year_4
 * @property int|null $private_cost_year_5
 * @property int|null $instate_cost_year_1
 * @property int|null $instate_cost_year_2
 * @property int|null $instate_cost_year_3
 * @property int|null $instate_cost_year_4
 * @property int|null $instate_cost_year_5
 * @property int|null $out_state_cost_year_1
 * @property int|null $out_state_cost_year_2
 * @property int|null $out_state_cost_year_3
 * @property int|null $out_state_cost_year_4
 * @property int|null $out_state_cost_year_5
 * @property int|null $md_earn_wne_p10 Median earnings of students working and not enrolled 10 years after entry
 * @property int|null $mn_earn_wne_p10 Mean earnings of students working and not enrolled 10 years after entry
 * @property int|null $md_earn_wne_p6 Median earnings of students working and not enrolled 6 years after entry
 * @property int|null $mn_earn_wne_p6 Mean earnings of students working and not enrolled 6 years after entry
 * @property int|null $avg_earn_wne_p6 Average earnings of students working and not enrolled 6 years after entry
 * @property int|null $avg_earn_wne_p10 Average earnings of students working and not enrolled 10 years after entry
 * @property int $is_college_im Does colleges uses IM or FM Methodology
 * @property int $is_private Is college a private college
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereActComp25($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereActComp75($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereActEng25($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereActEng75($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereActMath25($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereActMath75($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereActSubmittedPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereActSubmittedTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereAdmissionReqComp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereAdmissionReqCompPrepProg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereAdmissionReqGpa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereAdmissionReqOtherTest($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereAdmissionReqRank($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereAdmissionReqRec($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereAdmissionReqRecord($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereAdmissionReqTestScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereAdmissionReqToefl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereAdmissionsTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereAdmissionsYield($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereApplicantsTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereAvgEarnWneP10($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereAvgEarnWneP6($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereAvgGraduationYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereBooksSupplies($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereCalSys($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereDataYearDifference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereDistanceEducation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereEnrolledTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereFullFirstUgAnyAidAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereFullFirstUgAnyAidPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereFullFirstUgAnyAidTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereFullFirstUgFedAidAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereFullFirstUgFedAidPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereFullFirstUgFedAidTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereFullFirstUgInstAidAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereFullFirstUgInstAidPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereFullFirstUgInstAidTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereFullFirstUgPellGrantAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereFullFirstUgPellGrantPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereFullFirstUgPellGrantTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereFullFirstUgStateAidAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereFullFirstUgStateAidPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereFullFirstUgStateAidTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereFullFirstUgStudentLoanAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereFullFirstUgStudentLoanPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereFullFirstUgStudentLoanTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereFullTimeRetentionRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereGiftAid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereGraduationRateTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereInflation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereInflationYear1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereInflationYear2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereInflationYear3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereInflationYear4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereInflationYear5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereInstNmCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereInstSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereInstateCostYear1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereInstateCostYear2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereInstateCostYear3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereInstateCostYear4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereInstateCostYear5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereInstituteType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereIsCollegeIm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereIsPrivate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereMajor1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereMajor2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereMajor3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereMajor4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereMajor5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereMdEarnWneP10($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereMdEarnWneP6($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereMnEarnWneP10($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereMnEarnWneP6($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereNeedMet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereNetPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereNetPrice030k($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereNetPrice110kPlus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereNetPrice30k48k($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereNetPrice48k75k($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereNetPrice75k110k($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereNetPrice75kPlus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereNetPriceAddr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereOffCampNotFamilyOther($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereOffCampNotFamilyRoomBoard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereOffCampWithFamilyOther($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereOnCampOther($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereOnCampRoomBoard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereOpeid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereOriginalInStateCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereOriginalOutStateCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereOriginalPrivateCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereOtherAlternativeTuitionPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereOutStateCostYear1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereOutStateCostYear2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereOutStateCostYear3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereOutStateCostYear4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereOutStateCostYear5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College wherePartTimeRetentionRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College wherePercentAdmitted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College wherePrepaidTuitionPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College wherePrivateCostYear1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College wherePrivateCostYear2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College wherePrivateCostYear3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College wherePrivateCostYear4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College wherePrivateCostYear5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereSatMath25($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereSatMath75($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereSatRead25($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereSatRead75($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereSatSubmittedPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereSatSubmittedTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereSelfHelp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereStateAbbr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereStudentToFaculty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereStudyAbroad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereTeacherCertification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereTuitionGuaranteedPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereTuitionPaymentPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereUg30k48kTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereUg30kTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereUg48k75kTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereUg75k110kTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereUgAfricanPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereUgApplicationFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereUgAsianPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereUgForeignPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereUgHispanicPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereUgInStatePercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereUgMenPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereUgMultiracialPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereUgNativePercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereUgNonResidentPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereUgOutStatePercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereUgPacificPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereUgUnknownPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereUgUnknownRacePercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereUgWhitePercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereUgWomenPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereUndergraduateEnrollment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereUnitid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereWebAddr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereWeekendEveCollege($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereYear1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereYear2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereYear3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereYear4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereYear5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College whereZip($value)
 * @property-read string $custom_net_price_addr
 * @property-read mixed $ug110k_plus_percent
 * @property-read mixed $ug30k48k_percent
 * @property-read mixed $ug30k_percent
 * @property-read mixed $ug48k75k_percent
 * @property-read mixed $ug75k110k_percent
 * @property mixed data_usa
 * @property mixed optional_data_usa
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\College enrollmentSize($size)
 */
class College extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;
    use Sluggable, SluggableScopeHelpers;
    use ModelTrait;

    protected $primaryKey = "unitid";

    /**
     * Specify the connection, since this implements multitenant solution
     * Called via constructor to facilitate testing
     *
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->setConnection(config('database.data'));
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => [
                    'name',
                ],
            ]
        ];
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug',
        'data_usa',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'data_usa' => 'array',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * Get logo url
     *
     * @return string
     */
    public function getLogoAttribute()
    {
        return "https://spikecdn.com/answers4college/colleges/logos/" . $this->unitid . ".png";
    }

    /**
     * Get splash url
     *
     * @return string
     */
    public function getSplashAttribute()
    {
        return "https://spikecdn.com/answers4college/colleges/splash/" . $this->unitid . ".jpeg";
    }

    public function getOptionalDataUsaAttribute()
    {
        if ($this->data_usa) {
            return optional(json_decode($this->data_usa));
        }

        return null;
    }

    /**
     * Get splash url
     *
     * @return string
     */
    public function getAboutOneAttribute()
    {
        if ($this->data_usa && $this->optional_data_usa->descriptions) {
            foreach ($this->optional_data_usa->descriptions as $description) {
                if ($description->id == 38) {
                    return  strip_tags($description->description);
                }
            }
        }

        return "N/A";
    }

    /**
     * Get splash url
     *
     * @return string
     */
    public function getAboutTwoAttribute()
    {
        if ($this->data_usa && $this->optional_data_usa->descriptions) {
            foreach ($this->optional_data_usa->descriptions as $description) {
                if ($description->id == 39) {
                    return  strip_tags($description->description);
                }
            }
        }

        return "N/A";
    }

    /**
     * Get splash url
     *
     * @return string
     */
    public function getAboutThreeAttribute()
    {
        if ($this->data_usa && $this->optional_data_usa->descriptions) {
            foreach ($this->optional_data_usa->descriptions as $description) {
                if ($description->id == 40) {
                    return  strip_tags($description->description);
                }
            }
        }

        return "N/A";
    }

    public function getPhotoMetaAttribute()
    {
        if ($this->data_usa && $this->optional_data_usa->image) {
            return "
                About the photo: {$this->optional_data_usa->image->meta}
                        <br/>
                        <i class=\"fa fa-camera kt-padding-r-5\"></i>
                        <a href=\"\"> Photo By: {$this->optional_data_usa->image->author}</a>
            ";
        }

        return "N/A";
    }

    /**
     * Get web address
     *
     * @return string
     * @return string
     *
     */
    public function getCustomWebAddrAttribute()
    {
        if ($this->web_addr) {
            return (new HelperRepository())->addHttp($this->web_addr);
        }

        return "#";
    }

    /**
     * Get net price calculate address
     *
     * @return string
     */
    public function getCustomNetPriceAddrAttribute()
    {
        if ($this->net_price_addr) {
            return (new HelperRepository())->addHttp($this->net_price_addr);
        }

        return "#";
    }

    /**
     * Get custom sat range
     *
     * @return string
     */
    public function getCustomSatScoreRangeAttribute()
    {
        if ($this->sat_read_25 && $this->sat_read_75 && $this->sat_math_25 && $this->sat_math_75) {
            $first = $this->sat_read_25 + $this->sat_math_25;
            $second = $this->sat_read_75 + $this->sat_math_75;

            return $first . " - " . $second;
        }

        return "-";
    }

    /** Student Body - Household Income Levels */
    public function getUg30kPercentAttribute()
    {
        if ($this->ug_30k_total > 0 && $this->enrolled_total > 0) {
            return number_format(($this->ug_30k_total/$this->enrolled_total) * 100, 0) . '%';
        }

        return '-';
    }
    public function getUg30k48kPercentAttribute()
    {
        if ($this->ug_30k_48k_total > 0 && $this->enrolled_total > 0) {
            return number_format(($this->ug_30k_48k_total/$this->enrolled_total) * 100, 0) . '%';
        }

        return '-';
    }
    public function getUg48k75kPercentAttribute()
    {
        if ($this->ug_48k_75k_total > 0 && $this->enrolled_total > 0) {
            return number_format(($this->ug_48k_75k_total/$this->enrolled_total) * 100, 0) . '%';
        }

        return '-';
    }
    public function getUg75k110kPercentAttribute()
    {
        if ($this->ug_75k_110k_total > 0 && $this->enrolled_total > 0) {
            return number_format(($this->ug_75k_110k_total/$this->enrolled_total) * 100, 0) . '%';
        }

        return '-';
    }
    public function getUg110kPlusPercentAttribute()
    {
        if ($this->enrolled_total > 0) {
            $otherSum = $this->ug_30k_total + $this->ug_30k_48k_total + $this->ug_48k_75k_total + $this->ug_75k_110k_total;
            $result = (($this->enrolled_total - $otherSum)/$this->enrolled_total) * 100;

            return $result > 0 ? number_format($result, 0) . '%' : "-";
        }

        return '-';
    }

    /**
     * Get custom act range
     *
     * @return string
     */
    public function getCustomActScoreRangeAttribute()
    {
        if ($this->act_comp_25 && $this->act_comp_75) {
            return $this->act_comp_25 . " - " . $this->act_comp_75;
        }

        return "-";
    }

    /**
     * Get custom locale value
     *
     * @return mixed
     */
    public function getCustomLocaleAttribute()
    {
        $locale = explode(":", $this->locale);

        $class = "blueprintone-city33";

        if ($locale[0] == "Rural") {
            $class = "blueprintone-rural33";
        }
        if ($locale[0] == "Suburb") {
            $class = "blueprintone-rural30";
        }
        if ($locale[0] == "Town") {
            $class = "blueprintone-building90";
        }

        $output['class'] = $class;
        $output['data']  = $locale[0];

        return $output;
    }

    /**
     * get custom institute size
     *
     * @return mixed
     */
    public function getCustomInstSizeAttribute()
    {
        $original = $this->inst_size;

        $class = "blueprintone-crowd";
        $data = "Large";

        if ($original == "Under 1,000" or $original == "1,000 - 4,999") {
            $class = "blueprintone-users33";
            $data = "Small";
        }
        if ($original == "5,000 - 9,999" or $original == "10,000 - 19,999") {
            $class = "blueprintone-users6";
            $data = "Medium";
        }
        if ($original == "20,000 and above") {
            $class = "blueprintone-crowd";
            $data = "Large";
        }
        if ($original == "Not reported") {
            $class = "blueprintone-crowd";
            $data = "N/A";
        }

        $output['class'] = $class;
        $output['data']  = $data;

        return $output;
    }

    /**
     * get custom calendar system
     *
     * @return mixed
     */
    public function getCustomCalSysAttribute()
    {
        $original = $this->cal_sys;

        $data = $original;

        if ($original == "Four-one-four plan") {
            $data = "4-1-4";
        }
        if ($original == "Other academic year") {
            $data = "Other";
        }

        $output['data']  = $data;

        return $output;
    }

    /**
     * Get college generosity
     *
     * @return float|int
     */
    public function getGenerosityNumberAttribute()
    {
        $minGenerosity = 10;
        $maxGenerosity = 85;

        $collegeGenerosity = round((($this->need_met * 100) + ($this->gift_aid * 100))/2, 0);
        $collegeGenerosity = $collegeGenerosity < $minGenerosity ? $minGenerosity : $collegeGenerosity;
        $collegeGenerosity = $collegeGenerosity > $maxGenerosity ? $maxGenerosity : $collegeGenerosity;

        return $collegeGenerosity;
    }

    /**
     * Get college generosity degree for map
     *
     * @return float|int
     */
    public function getGenerosityDegreeAttribute()
    {
        $maxGenerosityDegree = 55;

        $collegeGenerosityNumber = round((($this->need_met * 100) + ($this->gift_aid * 100))/2, 0);

        $collegeGenerosityDegree =  (1.8 * $collegeGenerosityNumber) - 90;

        return $collegeGenerosityDegree < $maxGenerosityDegree ? $collegeGenerosityDegree : $maxGenerosityDegree;
    }

    /**
     * Scope to filter records
     *
     * @param $query
     * @param Request $request
     * @return mixed
     */
    public function scopeSearch($query, Request $request)
    {
        return $query
            ->when($request->get('name'), function ($query, $filter) {
                $query->where('name', 'like', "%" . $filter . "%");
            })
            ->when($request->get('state_abbr'), function ($query, $filter) {
                $query->where('state_abbr', $filter);
            })
            ->when($request->get('enrollment_size'), function ($query, $filter) {
                $query->enrollmentSize($filter);
            })
            ->when($request->get('institute_type'), function ($query, $filter) {
                $query->where('institute_type', $filter);
            })
            ->when($request->get('locale'), function ($query, $filter) {
                $query->where('locale', $filter);
            });
    }

    /**
     * Scope to filter enrollment size
     *
     * @param $query
     * @param $size
     * @return mixed
     */
    public function scopeEnrollmentSize($query, $size)
    {
        switch ($size) {
            case 1:
                return $query->where('enrolled_total', '<', 1000);
            case  2:
                return $query->where('enrolled_total', '>=', 1000)
                    ->where('enrolled_total', '<', 2000);
            case  3:
                return $query->where('enrolled_total', '>=', 2000)
                    ->where('enrolled_total', '<', 4000);
            case  4:
                return $query->where('enrolled_total', '>=', 4000)
                    ->where('enrolled_total', '<', 6000);
            case  5:
                return $query->where('enrolled_total', '>=', 6000)
                    ->where('enrolled_total', '<', 10000);
            case  6:
                return $query->where('enrolled_total', '>=', 10000)
                    ->where('enrolled_total', '<', 15000);
            case  7:
                return $query->where('enrolled_total', '>=', 15000)
                    ->where('enrolled_total', '<', 20000);
            case  8:
                return $query->where('enrolled_total', '>=', 20000)
                    ->where('enrolled_total', '<', 30000);
            case  9:
                return $query->where('enrolled_total', '>=', 30000);
        }
    }
}
