<?php

namespace App\Models\Data\College;

use function config;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\Data\College\CollegeTreasury
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeTreasury newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeTreasury newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\College\CollegeTreasury onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeTreasury query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\College\CollegeTreasury withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\College\CollegeTreasury withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property int $unitid
 * @property int|null $mn_earn_wne_p6
 * @property int|null $md_earn_wne_p6
 * @property int|null $mn_earn_wne_p10
 * @property int|null $md_earn_wne_p10
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeTreasury whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeTreasury whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeTreasury whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeTreasury whereMdEarnWneP10($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeTreasury whereMdEarnWneP6($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeTreasury whereMnEarnWneP10($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeTreasury whereMnEarnWneP6($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeTreasury whereUnitid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CollegeTreasury whereUpdatedAt($value)
 */
class CollegeTreasury extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    /**
     * Specify the connection, since this implements multitenant solution
     * Called via constructor to facilitate testing
     *
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->setConnection(config('database.data'));
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'unitid',
        'mn_earn_wne_p6',
        'md_earn_wne_p6',
        'mn_earn_wne_p10',
        'md_earn_wne_p10',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
