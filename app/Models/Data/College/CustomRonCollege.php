<?php

namespace App\Models\Data\College;

use function config;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\Data\College\CustomRonCollege
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CustomRonCollege newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CustomRonCollege newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\College\CustomRonCollege onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CustomRonCollege query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\College\CustomRonCollege withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\College\CustomRonCollege withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property int $unitid
 * @property string $instnm
 * @property string|null $webaddr
 * @property string|null $stabbr
 * @property string|null $city
 * @property float $need_met
 * @property float $gift_aid
 * @property float $self_help
 * @property int $is_college_im
 * @property float $avg_graduation_year
 * @property float $inflation
 * @property float|null $inflation_year_1
 * @property float|null $inflation_year_2
 * @property float|null $inflation_year_3
 * @property float|null $inflation_year_4
 * @property float|null $inflation_year_5
 * @property string|null $merit_link
 * @property string|null $common_data_link
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CustomRonCollege whereAvgGraduationYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CustomRonCollege whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CustomRonCollege whereCommonDataLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CustomRonCollege whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CustomRonCollege whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CustomRonCollege whereGiftAid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CustomRonCollege whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CustomRonCollege whereInflation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CustomRonCollege whereInflationYear1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CustomRonCollege whereInflationYear2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CustomRonCollege whereInflationYear3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CustomRonCollege whereInflationYear4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CustomRonCollege whereInflationYear5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CustomRonCollege whereInstnm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CustomRonCollege whereIsCollegeIm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CustomRonCollege whereMeritLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CustomRonCollege whereNeedMet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CustomRonCollege whereSelfHelp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CustomRonCollege whereStabbr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CustomRonCollege whereUnitid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CustomRonCollege whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\College\CustomRonCollege whereWebaddr($value)
 */
class CustomRonCollege extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    /**
     * Specify the connection, since this implements multitenant solution
     * Called via constructor to facilitate testing
     *
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->setConnection(config('database.data'));
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'unitid',
        'instnm',
        'webaddr',
        'stabbr',
        'city',
        'need_met',
        'gift_aid',
        'self_help',
        'is_college_im',
        'avg_graduation_year',
        'inflation',
        'inflation_year_1',
        'inflation_year_2',
        'inflation_year_3',
        'inflation_year_4',
        'inflation_year_5',
        'merit_link',
        'common_data_link',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
