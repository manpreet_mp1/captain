<?php

namespace App\Models\Data\Major;

use App\Models\ModelTrait;
use function config;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\Data\Major\Major
 *
 * @property int $id
 * @property string $family
 * @property string $code
 * @property string $type
 * @property string $title
 * @property string|null $slug
 * @property string $definition
 * @property int|null $associate_count
 * @property int|null $associate_percent
 * @property int|null $bachelor_count
 * @property int|null $bachelor_percent
 * @property int|null $certificate_count
 * @property int|null $certificate_percent
 * @property int|null $doctor_count
 * @property int|null $doctor_percent
 * @property int|null $master_count
 * @property int|null $master_percent
 * @property int|null $other_count
 * @property int|null $other_percent
 * @property int|null $male_count
 * @property int|null $male_percent
 * @property int|null $female_count
 * @property int|null $female_percent
 * @property int|null $white_count
 * @property int|null $white_percent
 * @property int|null $indian_count
 * @property int|null $indian_percent
 * @property int|null $asian_count
 * @property int|null $asian_percent
 * @property int|null $black_count
 * @property int|null $black_percent
 * @property int|null $hispanic_count
 * @property int|null $hispanic_percent
 * @property int|null $native_count
 * @property int|null $native_percent
 * @property int|null $two_or_more_count
 * @property int|null $two_or_more_percent
 * @property int|null $non_resident_count
 * @property int|null $non_resident_percent
 * @property int|null $unknown_count
 * @property int|null $unknown_percent
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Data\Major\MajorCollege[] $colleges
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major findSimilarSlugs($attribute, $config, $slug)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major main()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\Major\Major onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereAsianCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereAsianPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereAssociateCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereAssociatePercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereBachelorCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereBachelorPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereBlackCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereBlackPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereCertificateCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereCertificatePercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereDefinition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereDoctorCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereDoctorPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereFamily($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereFemaleCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereFemalePercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereHispanicCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereHispanicPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereIndianCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereIndianPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereMaleCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereMalePercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereMasterCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereMasterPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereNativeCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereNativePercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereNonResidentCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereNonResidentPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereOtherCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereOtherPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereTwoOrMoreCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereTwoOrMorePercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereUnknownCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereUnknownPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereWhiteCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\Major whereWhitePercent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\Major\Major withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\Major\Major withoutTrashed()
 * @mixin \Eloquent
 */
class Major extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;
    use Sluggable, SluggableScopeHelpers;
    use ModelTrait;

    /**
     * Specify the connection, since this implements multitenant solution
     * Called via constructor to facilitate testing
     *
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->setConnection(config('database.data'));
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => [
                    'title',
                ],
            ]
        ];
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * Has many colleges in a major
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function colleges()
    {
        return $this->hasMany(MajorCollege::class, 'code', 'code');
    }

    /**
     * Filter the main records
     *
     * @param $query
     * @return mixed
     */
    public function scopeMain($query)
    {
        return $query->where('type', 'main');
    }

    /**
     * Get all the items related to a heading
     *
     * @return mixed
     */
    public function items()
    {
        return self::where('type', 'item')
            ->where('code', 'like', $this->code . '%')
            ->get();
    }
}
