<?php

namespace App\Models\Data\Major;

use App\Models\ModelTrait;
use function config;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\Data\Major\MajorCollege
 *
 * @property int $id
 * @property string $code
 * @property int $unitid
 * @property string $name
 * @property string $slug
 * @property string $size_of_college
 * @property int|null $degree_count
 * @property int|null $associate_count
 * @property int|null $bachelor_count
 * @property int|null $certificate_count
 * @property int|null $doctor_count
 * @property int|null $master_count
 * @property int|null $other_count
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\MajorCollege newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\MajorCollege newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\Major\MajorCollege onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\MajorCollege query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\MajorCollege whereAssociateCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\MajorCollege whereBachelorCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\MajorCollege whereCertificateCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\MajorCollege whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\MajorCollege whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\MajorCollege whereDegreeCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\MajorCollege whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\MajorCollege whereDoctorCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\MajorCollege whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\MajorCollege whereMasterCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\MajorCollege whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\MajorCollege whereOtherCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\MajorCollege whereSizeOfCollege($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\MajorCollege whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\MajorCollege whereUnitid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Major\MajorCollege whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\Major\MajorCollege withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\Major\MajorCollege withoutTrashed()
 * @mixin \Eloquent
 */
class MajorCollege extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;
    use ModelTrait;

    /**
     * Specify the connection, since this implements multitenant solution
     * Called via constructor to facilitate testing
     *
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->setConnection(config('database.data'));
    }
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
