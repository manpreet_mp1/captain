<?php

namespace App\Models\Data\Scholarship;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\Data\Scholarship\ScholarshipMerit
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read mixed $college_logo_url
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Scholarship\ScholarshipMerit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Scholarship\ScholarshipMerit newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\Scholarship\ScholarshipMerit onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Scholarship\ScholarshipMerit query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Scholarship\ScholarshipMerit search($request)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\Scholarship\ScholarshipMerit withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\Scholarship\ScholarshipMerit withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property int $ipeds
 * @property string $school_name
 * @property string $state_abbr
 * @property string $name
 * @property string $type
 * @property string $amount
 * @property string $required_sat
 * @property string $required_act
 * @property string $required_gpa
 * @property string $school_type
 * @property string $acceptance_rate
 * @property string $scholarship_url
 * @property string $tuition
 * @property string $eligibility_for_public_schools
 * @property string $international_students
 * @property string $mid_50_act
 * @property string $mid_50_sat
 * @property string $us_news_ranking
 * @property string $scholarship_description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Scholarship\ScholarshipMerit whereAcceptanceRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Scholarship\ScholarshipMerit whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Scholarship\ScholarshipMerit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Scholarship\ScholarshipMerit whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Scholarship\ScholarshipMerit whereEligibilityForPublicSchools($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Scholarship\ScholarshipMerit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Scholarship\ScholarshipMerit whereInternationalStudents($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Scholarship\ScholarshipMerit whereIpeds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Scholarship\ScholarshipMerit whereMid50Act($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Scholarship\ScholarshipMerit whereMid50Sat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Scholarship\ScholarshipMerit whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Scholarship\ScholarshipMerit whereRequiredAct($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Scholarship\ScholarshipMerit whereRequiredGpa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Scholarship\ScholarshipMerit whereRequiredSat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Scholarship\ScholarshipMerit whereScholarshipDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Scholarship\ScholarshipMerit whereScholarshipUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Scholarship\ScholarshipMerit whereSchoolName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Scholarship\ScholarshipMerit whereSchoolType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Scholarship\ScholarshipMerit whereStateAbbr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Scholarship\ScholarshipMerit whereTuition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Scholarship\ScholarshipMerit whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Scholarship\ScholarshipMerit whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\Scholarship\ScholarshipMerit whereUsNewsRanking($value)
 */
class ScholarshipMerit extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    /**
     * Specify the connection, since this implements multitenant solution
     * Called via constructor to facilitate testing
     *
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->setConnection(config('database.data'));
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ipeds',
        'school_name',
        'state_abbr',
        'name',
        'type',
        'amount',
        'required_sat',
        'required_act',
        'required_gpa',
        'school_type',
        'acceptance_rate',
        'scholarship_url',
        'tuition',
        'eligibility_for_public_schools',
        'international_students',
        'mid_50_act',
        'mid_50_sat',
        'us_news_ranking',
        'scholarship_description',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * Get the college logo attribute
     */
    public function getCollegeLogoUrlAttribute()
    {
        return "https://spikecdn.com/answers4college/colleges/logos/" . $this->ipeds . ".png";
    }

    /**
     * Search for records
     *
     * @param $query
     * @param $request
     * @return mixed
     */
    public function scopeSearch($query, $request)
    {
        return $query
            ->when($request->get('collegeName'), function ($query, $collegeName) {
                $query->where('school_name', 'like', "%" . $collegeName . "%");
            })
            ->when($request->get('state'), function ($query, $state) {
                $query->where('state_abbr', $state);
            })
            ->when($request->get('type'), function ($query, $type) {
                $query->where('type', $type);
            });
    }
}
