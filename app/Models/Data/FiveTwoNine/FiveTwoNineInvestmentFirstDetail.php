<?php

namespace App\Models\Data\FiveTwoNine;

use function config;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail
 *
 * @property int $id
 * @property int $plan_id
 * @property int $option_id
 * @property string|null $sub_option
 * @property string|null $fees_structure
 * @property string|null $heading_name
 * @property string|null $heading_value
 * @property string|null $estimated_underlying_fund_expenses
 * @property string|null $program_manager_fee
 * @property string|null $state_fee
 * @property string|null $misc_fees
 * @property string|null $annual_distribution_fee
 * @property string|null $total_annual_asset_based_fees
 * @property string|null $maximum_initial_sales_charge
 * @property string|null $annual_account_maintenance_fee
 * @property string|null $content_before
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail whereAnnualAccountMaintenanceFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail whereAnnualDistributionFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail whereContentBefore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail whereEstimatedUnderlyingFundExpenses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail whereFeesStructure($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail whereHeadingName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail whereHeadingValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail whereMaximumInitialSalesCharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail whereMiscFees($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail whereOptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail wherePlanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail whereProgramManagerFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail whereStateFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail whereSubOption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail whereTotalAnnualAssetBasedFees($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail withoutTrashed()
 * @mixin \Eloquent
 */
class FiveTwoNineInvestmentFirstDetail extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    /**
     * Specify the connection, since this implements multitenant solution
     * Called via constructor to facilitate testing
     *
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->setConnection(config('database.data'));
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'plan_id',
        'option_id',
        'sub_option',
        'fees_structure',
        'heading_name',
        'heading_value',
        'estimated_underlying_fund_expenses',
        'program_manager_fee',
        'state_fee',
        'misc_fees',
        'annual_distribution_fee',
        'total_annual_asset_based_fees',
        'maximum_initial_sales_charge',
        'annual_account_maintenance_fee',
        'content_before',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
