<?php

namespace App\Models\Data\FiveTwoNine;

use function config;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\Data\FiveTwoNine\FiveTwoNineInvestment
 *
 * @property int $id
 * @property int $plan_id
 * @property int $option_id
 * @property string|null $name
 * @property string|null $option_type
 * @property string|null $sub_option
 * @property string|null $manager
 * @property string|null $residency_requirements
 * @property string|null $enrollment_fee
 * @property string|null $minimum_initial_contribution
 * @property string|null $minimum_subsequent_contribution
 * @property string|null $maximum_total_contribution
 * @property string|null $performance_url
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestment newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestment onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestment query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestment whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestment whereEnrollmentFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestment whereManager($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestment whereMaximumTotalContribution($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestment whereMinimumInitialContribution($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestment whereMinimumSubsequentContribution($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestment whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestment whereOptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestment whereOptionType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestment wherePerformanceUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestment wherePlanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestment whereResidencyRequirements($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestment whereSubOption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestment withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestment withoutTrashed()
 * @mixin \Eloquent
 */
class FiveTwoNineInvestment extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    /**
     * Specify the connection, since this implements multitenant solution
     * Called via constructor to facilitate testing
     *
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->setConnection(config('database.data'));
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'plan_id',
        'option_id',
        'name',
        'option_type',
        'sub_option',
        'manager',
        'residency_requirements',
        'enrollment_fee',
        'minimum_initial_contribution',
        'minimum_subsequent_contribution',
        'maximum_total_contribution',
        'link_to_performance',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
