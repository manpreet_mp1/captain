<?php

namespace App\Models\Data\FiveTwoNine;

use function config;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentSecondDetail
 *
 * @property int $id
 * @property int $plan_id
 * @property int $option_id
 * @property string|null $sub_option
 * @property string|null $redemption
 * @property string|null $fees_structure
 * @property string|null $heading_name
 * @property string|null $heading_value
 * @property string|null $1_year
 * @property string|null $3_year
 * @property string|null $5_year
 * @property string|null $10_year
 * @property string|null $content_after
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentSecondDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentSecondDetail newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentSecondDetail onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentSecondDetail query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentSecondDetail where10Year($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentSecondDetail where1Year($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentSecondDetail where3Year($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentSecondDetail where5Year($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentSecondDetail whereContentAfter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentSecondDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentSecondDetail whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentSecondDetail whereFeesStructure($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentSecondDetail whereHeadingName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentSecondDetail whereHeadingValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentSecondDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentSecondDetail whereOptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentSecondDetail wherePlanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentSecondDetail whereRedemption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentSecondDetail whereSubOption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentSecondDetail whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentSecondDetail withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentSecondDetail withoutTrashed()
 * @mixin \Eloquent
 */
class FiveTwoNineInvestmentSecondDetail extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    /**
     * Specify the connection, since this implements multitenant solution
     * Called via constructor to facilitate testing
     *
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->setConnection(config('database.data'));
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'plan_id',
        'option_id',
        'sub_option',
        'redemption',
        'fees_structure',
        'heading_name',
        'heading_value',
        '1_year',
        '3_year',
        '5_year',
        '10_year',
        'content_after',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
