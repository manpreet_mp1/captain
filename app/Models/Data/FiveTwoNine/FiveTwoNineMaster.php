<?php

namespace App\Models\Data\FiveTwoNine;

use App\Models\ModelTrait;
use function config;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\Data\FiveTwoNine\FiveTwoNineMaster
 *
 * @property int $id
 * @property int $plan_id
 * @property int $state_id
 * @property string|null $name
 * @property string|null $slug
 * @property string|null $type_1
 * @property string|null $type_2
 * @property string|null $type_3_key
 * @property string|null $type_3_name
 * @property string|null $state_abbr
 * @property string|null $state_name
 * @property string|null $website
 * @property string|null $plan_phone
 * @property string|null $address_line_1
 * @property string|null $address_line_2
 * @property string|null $city_state_zip
 * @property string|null $phone
 * @property string|null $toll_free
 * @property string|null $fax
 * @property string|null $email
 * @property string|null $home_url
 * @property string|null $offering_material_url
 * @property string|null $enrollment_material_url
 * @property string|null $online_account_access_url
 * @property string|null $general_performance_url
 * @property string|null $residency
 * @property string|null $residency_details
 * @property string|null $pm_name
 * @property string|null $im_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestment[] $InvestmentOptions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Data\FiveTwoNine\FiveTwoNineInvestmentFirstDetail[] $InvestmentOptionsFirstDetails
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Data\FiveTwoNine\FiveTwoNineBenefit[] $benefits
 * @property-read string $offer_financial_aid_benefits
 * @property-read string $offer_reward_programs
 * @property-read string $offer_state_matching_grants
 * @property-read string $offer_state_tax_credit_or_deduction
 * @property-read string $offer_state_tax_deferred_earnings_withdrawals
 * @property-read mixed $sshot_url
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Data\FiveTwoNine\FiveTwoNineManagement[] $management
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster filter($filters)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereAddressLine1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereAddressLine2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereCityStateZip($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereEnrollmentMaterialUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereFax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereGeneralPerformanceUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereHomeUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereImName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereOfferingMaterialUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereOnlineAccountAccessUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster wherePlanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster wherePlanPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster wherePmName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereResidency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereResidencyDetails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereStateAbbr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereStateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereStateName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereTollFree($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereType1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereType2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereType3Key($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereType3Name($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster whereWebsite($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineMaster withoutTrashed()
 * @mixin \Eloquent
 */
class FiveTwoNineMaster extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;
    use ModelTrait;

    /**
     * Specify the connection, since this implements multitenant solution
     * Called via constructor to facilitate testing
     *
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->setConnection(config('database.data'));
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'plan_id',
        'state_id',
        'name',
        'slug',
        'type_1',
        'type_2',
        'type_3_key',
        'type_3_name',
        'state_abbr',
        'state_name',
        'website',
        'plan_phone',
        'address_line_1',
        'address_line_2',
        'city_state_zip',
        'phone',
        'toll_free',
        'fax',
        'email',
        'home_url',
        'offering_material_url',
        'enrollment_material_url',
        'online_account_access_url',
        'general_performance_url',
        'residency',
        'residency_details',
        'pm_name',
        'im_name',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * Has many managers
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function management()
    {
        return $this->hasMany(FiveTwoNineManagement::class, 'plan_id', 'plan_id');
    }

    /**
     * Has many benefits
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function benefits()
    {
        return $this->hasMany(FiveTwoNineBenefit::class, 'plan_id', 'plan_id');
    }

    /**
     * Has many investment options
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function InvestmentOptions()
    {
        return $this->hasMany(FiveTwoNineInvestment::class, 'plan_id', 'plan_id');
    }

    /**
     * Has many first options details
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function InvestmentOptionsFirstDetails()
    {
        return $this->hasMany(FiveTwoNineInvestmentFirstDetail::class, 'plan_id', 'plan_id');
    }
    
    /**
     * Get the college logo attribute
     */
    public function getSshotUrlAttribute()
    {
        return "https://spikecdn.com/answers4college/fiveTwoNine/sshot/" . $this->plan_id . ".jpg";
    }

    /**
     * State Tax Credit or Deduction?
     *
     * @return string
     */
    public function getOfferStateTaxCreditOrDeductionAttribute()
    {
        $count = $this->benefits()->where('benefit_heading', 'like', $this->attributes['state_name'] . ' offers state tax deductions%')
            ->orWhere('benefit_heading', 'like', $this->attributes['state_name'] . ' offers state tax deductions%')
            ->count();

        if ($count > 0) {
            return "Yes";
        }

        return "No";
    }

    /**
     * State Tax Deferred Earnings/ Withdrawals
     *
     * @return string
     */
    public function getOfferStateTaxDeferredEarningsWithdrawalsAttribute()
    {
        $count = $this->benefits()->where('benefit_heading', 'like', $this->attributes['state_name'] . ' offers state tax exemptions for earnings%')
            ->count();

        if ($count > 0) {
            return "Yes";
        }

        return "No";
    }

    /**
     * Financial Aid Benefits
     * @return string
     */
    public function getOfferFinancialAidBenefitsAttribute()
    {
        $count = $this->benefits()->where('benefit_heading', 'like', $this->attributes['state_name'] . ' offers financial aid benefits%')
            ->count();

        if ($count > 0) {
            return "Yes";
        }

        return "No";
    }

    /**
     * State Matching Grants
     *
     * @return string
     */
    public function getOfferStateMatchingGrantsAttribute()
    {
        $count = $this->benefits()->where('benefit_heading', 'like', $this->attributes['state_name'] . ' offers state matching grants%')
            ->count();

        if ($count > 0) {
            return "Yes";
        }

        return "No";
    }

    /**
     * Reward Programs
     *
     * @return string
     */
    public function getOfferRewardProgramsAttribute()
    {
        $count = $this->benefits()->where('benefit_heading', 'like', $this->attributes['state_name'] . ' offers a rewards program%')
            ->count();

        if ($count > 0) {
            return "Yes";
        }

        return "No";
    }

    /**
     * Filter scope
     *
     * @param $query
     * @param array $filters
     * @return mixed
     */
    public function scopeFilter($query, $filters)
    {
        return $query
            ->when($filters->get('name'), function ($query, $filter) {
                return $query->where('name', 'like', '%' . $filter . '%');
            })
            ->when($filters->get('state_id'), function ($query, $filter) {
                return $query->where('state_id', $filter);
            })
            ->when($filters->get('type_3_key'), function ($query, $filter) {
                return $query->where('type_3_key', $filter);
            })
            ;
    }
}
