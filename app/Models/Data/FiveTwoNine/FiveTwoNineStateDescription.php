<?php

namespace App\Models\Data\FiveTwoNine;

use function config;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\Data\FiveTwoNine\FiveTwoNineStateDescription
 *
 * @property int $id
 * @property int $state_id
 * @property string|null $name
 * @property string|null $abbr
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineStateDescription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineStateDescription newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineStateDescription onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineStateDescription query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineStateDescription whereAbbr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineStateDescription whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineStateDescription whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineStateDescription whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineStateDescription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineStateDescription whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineStateDescription whereStateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineStateDescription whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineStateDescription withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\FiveTwoNine\FiveTwoNineStateDescription withoutTrashed()
 * @mixin \Eloquent
 */
class FiveTwoNineStateDescription extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    /**
     * Specify the connection, since this implements multitenant solution
     * Called via constructor to facilitate testing
     *
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->setConnection(config('database.data'));
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'state_id',
        'name',
        'abbr',
        'description',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
