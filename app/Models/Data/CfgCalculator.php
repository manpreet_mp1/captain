<?php

namespace App\Models\Data;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\Data\CfgCalculator
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\CfgCalculator newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\CfgCalculator newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\CfgCalculator onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\CfgCalculator query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\CfgCalculator withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Data\CfgCalculator withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string $key
 * @property float $value
 * @property string $description
 * @property string $type
 * @property string $calculators
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\CfgCalculator whereCalculators($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\CfgCalculator whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\CfgCalculator whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\CfgCalculator whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\CfgCalculator whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\CfgCalculator whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\CfgCalculator whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\CfgCalculator whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Data\CfgCalculator whereValue($value)
 */
class CfgCalculator extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    /**
     * Specify the connection, since this implements multitenant solution
     * Called via constructor to facilitate testing
     *
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->setConnection(config('database.data'));
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'value',
        'description',
        'type',
        'calculators',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
