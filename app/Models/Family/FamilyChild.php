<?php

namespace App\Models\Family;

use App\Models\Core\User;
use App\Models\Dropdown\InputValue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\Family\FamilyChild
 *
 * @property int $id
 * @property int $user_id
 * @property int $family_id
 * @property int|null $grade_level_id
 * @property int|null $relationship_parent_one_id
 * @property int|null $relationship_parent_two_id
 * @property int|null $income_work_dollar
 * @property int|null $income_adjusted_gross_dollar
 * @property int|null $income_tax_dollar
 * @property int|null $income_additional_tax_credit_dollar
 * @property int|null $income_additional_child_support_paid_dollar
 * @property int|null $income_additional_work_study_dollar
 * @property int|null $income_additional_grant_scholarship_dollar
 * @property int|null $income_additional_combat_pay_dollar
 * @property int|null $income_additional_cooperative_education_dollar
 * @property int|null $income_untaxed_tax_deferred_dollar
 * @property int|null $income_untaxed_ira_deduction_dollar
 * @property int|null $income_untaxed_child_support_received_dollar
 * @property int|null $income_untaxed_tax_exempt_dollar
 * @property int|null $income_untaxed_ira_distribution_dollar
 * @property int|null $income_untaxed_pension_portion_dollar
 * @property int|null $income_untaxed_paid_to_military_dollar
 * @property int|null $income_untaxed_veteran_benefit_dollar
 * @property int|null $income_untaxed_other_dollar
 * @property int|null $income_untaxed_student_behalf_dollar
 * @property int|null $asset_non_retirement_checking_savings_dollar
 * @property int|null $asset_non_retirement_ugma_utma_dollar
 * @property int|null $asset_non_retirement_certificate_of_deposit_dollar
 * @property int|null $asset_non_retirement_t_bills_dollar
 * @property int|null $asset_non_retirement_money_market_dollar
 * @property int|null $asset_non_retirement_mutual_dollar
 * @property int|null $asset_non_retirement_stock_dollar
 * @property int|null $asset_non_retirement_bond_dollar
 * @property int|null $asset_non_retirement_trust_dollar
 * @property int|null $asset_non_retirement_other_investments_dollar
 * @property int $is_in_college
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property int|mixed $adjusted_gross_income
 * @property int|mixed $cash_checking_saving
 * @property int|mixed $investment_account
 * @property int|mixed $trust_account
 * @property-read \App\Models\Core\User $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild inCollege($inSchool = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Family\FamilyChild onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereAssetNonRetirementBondDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereAssetNonRetirementCertificateOfDepositDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereAssetNonRetirementCheckingSavingsDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereAssetNonRetirementMoneyMarketDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereAssetNonRetirementMutualDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereAssetNonRetirementOtherInvestmentsDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereAssetNonRetirementStockDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereAssetNonRetirementTBillsDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereAssetNonRetirementTrustDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereAssetNonRetirementUgmaUtmaDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereFamilyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereGradeLevelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereIncomeAdditionalChildSupportPaidDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereIncomeAdditionalCombatPayDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereIncomeAdditionalCooperativeEducationDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereIncomeAdditionalGrantScholarshipDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereIncomeAdditionalTaxCreditDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereIncomeAdditionalWorkStudyDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereIncomeAdjustedGrossDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereIncomeTaxDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereIncomeUntaxedChildSupportReceivedDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereIncomeUntaxedIraDeductionDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereIncomeUntaxedIraDistributionDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereIncomeUntaxedOtherDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereIncomeUntaxedPaidToMilitaryDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereIncomeUntaxedPensionPortionDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereIncomeUntaxedStudentBehalfDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereIncomeUntaxedTaxDeferredDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereIncomeUntaxedTaxExemptDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereIncomeUntaxedVeteranBenefitDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereIncomeWorkDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereIsInCollege($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereRelationshipParentOneId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereRelationshipParentTwoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyChild whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Family\FamilyChild withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Family\FamilyChild withoutTrashed()
 * @mixin \Eloquent
 * @property-read int|null $income_additional_dollar_total
 * @property-read int|null $income_untaxed_dollar_total
 */
class FamilyChild extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    // based in input values
    const GRADE_LEVEL_ID = [
        'COMMUNITY_COLLEGE' => 22,
        'NOT_IN_SCHOOL' => 23,
    ];

    protected $guarded = ['id'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * Child belongs to a user
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Grade Level
     *
     * @return BelongsTo
     */
    public function gradeLevel()
    {
        return $this->belongsTo(InputValue::class, 'grade_level_id', 'value')
            ->gradeLevel();
    }

    /**
     * Relationship to parent one
     *
     * @return BelongsTo
     */
    public function relationshipParentOne()
    {
        return $this->belongsTo(InputValue::class, 'relationship_parent_one_id', 'value')
            ->relationshipToParent();
    }

    /**
     * Relationship to parent two
     *
     * @return BelongsTo
     */
    public function relationshipParentTwo()
    {
        return $this->belongsTo(InputValue::class, 'relationship_parent_two_id', 'value')
            ->relationshipToParent();
    }

    /**
     * Get the sum of all additional income
     *
     * @return int|null
     */
    public function getIncomeAdditionalDollarTotalAttribute()
    {
        return $this->income_additional_tax_credit_dollar +
            $this->income_additional_child_support_paid_dollar +
            $this->income_additional_work_study_dollar +
            $this->income_additional_grant_scholarship_dollar +
            $this->income_additional_combat_pay_dollar +
            $this->income_additional_cooperative_education_dollar;
    }

    /**
     * Get the sum of all untaxed income
     *
     * @return int|null
     */
    public function getIncomeUntaxedDollarTotalAttribute()
    {
        return $this->income_untaxed_tax_deferred_dollar +
            $this->income_untaxed_ira_deduction_dollar +
            $this->income_untaxed_child_support_received_dollar +
            $this->income_untaxed_tax_exempt_dollar +
            $this->income_untaxed_ira_distribution_dollar +
            $this->income_untaxed_pension_portion_dollar +
            $this->income_untaxed_paid_to_military_dollar +
            $this->income_untaxed_veteran_benefit_dollar +
            $this->income_untaxed_other_dollar +
            $this->income_untaxed_student_behalf_dollar;
    }

    /**
     * Get the children in school
     *
     * @param $query
     * @param int $inCollege
     * @return mixed
     */
    public function scopeInCollege($query, $inCollege = 0)
    {
        if ($inCollege == 1) {
            return $query->where('is_in_college', 1);
        }

        return $query;
    }

    /**
     * Get first year and academic year for a student
     *
     * @return array
     */
    public function getKidYearInCollege()
    {
        $map = [
            1 => 13, // Preschool
            2 => 12, // Kindergarten
            3 => 11, // 1st Grade
            4 => 10, // 2nd Grade
            5 => 9, // 3rd Grade
            6 => 8, // 4th Grade
            7 => 7, // 5th Grade
            8 => 6, // 6th Grade
            9 => 5, // 7th Grade
            10 => 4, // 8th Grade
            11 => 3, // 9th Grade
            12 => 2, // 10th Grade
            13 => 1, // 11th Grade
            14 => 0, // 12th Grade
            15 => -1, // College Freshman
            16 => -2, // College Sophomore
            17 => -3, // College Junior
            18 => -4, // College Senior
            19 => -5, // Grad School Year 1
            20 => -6, // Grad School Year 2
            21 => -7, // Grad School Year 3
        ];

        $firstYearInCollege = Carbon::now()->addYears($map[$this->grade_level_id])->year;

        return [
            'firstYearInCollege' => $firstYearInCollege,
            'firstAcademicYearInCollege' => $firstYearInCollege . ' - ' . ($firstYearInCollege + 1)
        ];
    }
}
