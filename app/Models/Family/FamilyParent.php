<?php

namespace App\Models\Family;

use App\Models\Core\User;
use App\Models\Dropdown\InputValue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\Family\FamilyParent
 *
 * @property int $id
 * @property int $user_id
 * @property int $family_id
 * @property int|null $role_id
 * @property int|null $marital_status_id
 * @property int|null $employment_status_id
 * @property int|null $state_id
 * @property \Illuminate\Support\Carbon|null $birth_date
 * @property int|null $income_work_dollar
 * @property int|null $income_adjusted_gross_dollar
 * @property int|null $income_tax_dollar
 * @property int|null $income_additional_tax_credit_dollar
 * @property int|null $income_additional_child_support_paid_dollar
 * @property int|null $income_additional_work_study_dollar
 * @property int|null $income_additional_grant_scholarship_dollar
 * @property int|null $income_additional_combat_pay_dollar
 * @property int|null $income_additional_cooperative_education_dollar
 * @property int|null $income_untaxed_tax_deferred_dollar
 * @property int|null $income_untaxed_ira_deduction_dollar
 * @property int|null $income_untaxed_child_support_received_dollar
 * @property int|null $income_untaxed_tax_exempt_dollar
 * @property int|null $income_untaxed_ira_distribution_dollar
 * @property int|null $income_untaxed_pension_portion_dollar
 * @property int|null $income_untaxed_paid_to_military_dollar
 * @property int|null $income_untaxed_veteran_benefit_dollar
 * @property int|null $income_untaxed_other_dollar
 * @property int|null $asset_education_student_sibling_dollar
 * @property int|null $asset_education_five_two_nine_dollar
 * @property int|null $asset_education_coverdell_dollar
 * @property int|null $asset_non_retirement_checking_savings_dollar
 * @property int|null $asset_non_retirement_certificate_of_deposit_dollar
 * @property int|null $asset_non_retirement_t_bills_dollar
 * @property int|null $asset_non_retirement_money_market_dollar
 * @property int|null $asset_non_retirement_mutual_dollar
 * @property int|null $asset_non_retirement_stock_dollar
 * @property int|null $asset_non_retirement_bond_dollar
 * @property int|null $asset_non_retirement_trust_dollar
 * @property int|null $asset_non_retirement_other_securities_dollar
 * @property int|null $asset_non_retirement_annuities_dollar
 * @property int|null $asset_non_retirement_other_investments_dollar
 * @property int|null $asset_retirement_defined_benefit_dollar
 * @property int|null $asset_retirement_four_o_one_dollar
 * @property int|null $asset_retirement_ira_dollar
 * @property int|null $asset_retirement_roth_ira_dollar
 * @property int|null $asset_retirement_previous_employer_dollar
 * @property int|null $asset_retirement_other_dollar
 * @property int|null $insurance_permanent_cash_value_dollar
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $age
 * @property-read int|null $formatted_age
 * @property-read int $is_parent_two
 * @property-read \App\Models\Core\User $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Family\FamilyParent onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereAssetEducationCoverdellDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereAssetEducationFiveTwoNineDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereAssetEducationStudentSiblingDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereAssetNonRetirementAnnuitiesDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereAssetNonRetirementBondDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereAssetNonRetirementCertificateOfDepositDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereAssetNonRetirementCheckingSavingsDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereAssetNonRetirementMoneyMarketDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereAssetNonRetirementMutualDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereAssetNonRetirementOtherInvestmentsDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereAssetNonRetirementOtherSecuritiesDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereAssetNonRetirementStockDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereAssetNonRetirementTBillsDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereAssetNonRetirementTrustDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereAssetRetirementDefinedBenefitDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereAssetRetirementFourOOneDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereAssetRetirementIraDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereAssetRetirementOtherDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereAssetRetirementPreviousEmployerDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereAssetRetirementRothIraDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereBirthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereEmploymentStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereFamilyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereIncomeAdditionalChildSupportPaidDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereIncomeAdditionalCombatPayDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereIncomeAdditionalCooperativeEducationDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereIncomeAdditionalGrantScholarshipDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereIncomeAdditionalTaxCreditDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereIncomeAdditionalWorkStudyDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereIncomeAdjustedGrossDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereIncomeTaxDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereIncomeUntaxedChildSupportReceivedDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereIncomeUntaxedIraDeductionDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereIncomeUntaxedIraDistributionDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereIncomeUntaxedOtherDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereIncomeUntaxedPaidToMilitaryDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereIncomeUntaxedPensionPortionDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereIncomeUntaxedTaxDeferredDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereIncomeUntaxedTaxExemptDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereIncomeUntaxedVeteranBenefitDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereIncomeWorkDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereInsurancePermanentCashValueDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereMaritalStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereStateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\FamilyParent whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Family\FamilyParent withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Family\FamilyParent withoutTrashed()
 * @mixin \Eloquent
 */
class FamilyParent extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $guarded = ['id'];

    protected $fillable = [
        'user_id',
        'family_id',
        'role_id',
        'marital_status_id',
        'employment_status_id',
        'state_id',
        'birth_date',
        'income_work_dollar',
        'income_adjusted_gross_dollar',
        'income_tax_dollar',
        'income_additional_tax_credit_dollar',
        'income_additional_child_support_paid_dollar',
        'income_additional_work_study_dollar',
        'income_additional_grant_scholarship_dollar',
        'income_additional_combat_pay_dollar',
        'income_additional_cooperative_education_dollar',
        'income_untaxed_tax_deferred_dollar',
        'income_untaxed_ira_deduction_dollar',
        'income_untaxed_child_support_received_dollar',
        'income_untaxed_tax_exempt_dollar',
        'income_untaxed_ira_distribution_dollar',
        'income_untaxed_pension_portion_dollar',
        'income_untaxed_paid_to_military_dollar',
        'income_untaxed_veteran_benefit_dollar',
        'income_untaxed_other_dollar',
        'asset_education_student_sibling_dollar',
        'asset_education_five_two_nine_dollar',
        'asset_education_coverdell_dollar',
        'asset_non_retirement_checking_savings_dollar',
        'asset_non_retirement_certificate_of_deposit_dollar',
        'asset_non_retirement_t_bills_dollar',
        'asset_non_retirement_money_market_dollar',
        'asset_non_retirement_mutual_dollar',
        'asset_non_retirement_stock_dollar',
        'asset_non_retirement_bond_dollar',
        'asset_non_retirement_trust_dollar',
        'asset_non_retirement_other_securities_dollar',
        'asset_non_retirement_annuities_dollar',
        'asset_non_retirement_other_investments_dollar',
        'asset_retirement_defined_benefit_dollar',
        'asset_retirement_four_o_one_dollar',
        'asset_retirement_ira_dollar',
        'asset_retirement_roth_ira_dollar',
        'asset_retirement_previous_employer_dollar',
        'asset_retirement_other_dollar',
        'insurance_permanent_cash_value_dollar',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'birth_date' => 'date',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * Parent belongs to a user
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Parent belongs to a role
     *
     * @return BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(InputValue::class, 'role_id', 'value')
            ->householdRole();
    }

    /**
     * Get the user's date of birth
     *
     * @return string
     */
    public function getBirthDateFormattedAttribute()
    {
        if ($this->attributes['birth_date']) {
            return Carbon::parse($this->attributes['birth_date'])->format("m/d/Y");
        }

        return null;
    }

    /**
     * If there is another parent
     *
     * @return int
     */
    public function getIsParentTwoAttribute()
    {
        if ($this->marital_status_id === 2) {
            return 1;
        }

        return 0;
    }

    /**
     * Get age attribute
     *
     * @return int|null
     */
    public function getAgeAttribute()
    {
        if ($this->birth_date) {
            return Carbon::parse($this->birth_date)->age;
        }

        return null;
    }

    /**
     * Get age attribute
     *
     * @return int|null
     */
    public function getFormattedAgeAttribute()
    {
        if ($this->birth_date) {
            return Carbon::parse($this->birth_date)->age . " Years";
        }

        return null;
    }
}
