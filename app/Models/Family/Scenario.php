<?php

namespace App\Models\Family;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\Family\Scenario
 *
 * @property int $id
 * @property int $family_id
 * @property int $family_child_id
 * @property string $name
 * @property int $is_shared_with_family
 * @property string $slug
 * @property string $summary
 * @property int $college_year
 * @property int $college_id_1
 * @property int $college_id_2
 * @property int $college_id_3
 * @property int $college_id_4
 * @property int $student_income_adjusted_gross_dollar
 * @property int $student_income_work_dollar
 * @property int $student_income_tax_dollar
 * @property int $student_income_untaxed_dollar_total
 * @property int $student_income_additional_dollar_total
 * @property int $student_asset_non_retirement_checking_savings_dollar
 * @property int $student_asset_non_retirement_ugma_utma_dollar
 * @property int $student_asset_non_retirement_certificate_of_deposit_dollar
 * @property int $student_asset_non_retirement_t_bills_dollar
 * @property int $student_asset_non_retirement_money_market_dollar
 * @property int $student_asset_non_retirement_mutual_dollar
 * @property int $student_asset_non_retirement_stock_dollar
 * @property int $student_asset_non_retirement_bond_dollar
 * @property int $student_asset_non_retirement_trust_dollar
 * @property int $student_asset_non_retirement_other_investments_dollar
 * @property int $parent_income_adjusted_gross_dollar_total
 * @property int $parent_1_income_work_dollar
 * @property int $parent_2_income_work_dollar
 * @property int $parent_income_tax_dollar_total
 * @property int $parent_income_untaxed_dollar_total
 * @property int $parent_income_additional_dollar_total
 * @property int $parent_asset_non_retirement_checking_savings_dollar_total
 * @property int $parent_asset_non_retirement_certificate_of_deposit_dollar_total
 * @property int $parent_asset_non_retirement_t_bills_dollar_total
 * @property int $parent_asset_non_retirement_money_market_dollar_total
 * @property int $parent_asset_non_retirement_mutual_dollar_total
 * @property int $parent_asset_non_retirement_stock_dollar_total
 * @property int $parent_asset_non_retirement_bond_dollar_total
 * @property int $parent_asset_non_retirement_trust_dollar_total
 * @property int $parent_asset_non_retirement_other_securities_dollar_total
 * @property int $parent_asset_non_retirement_annuities_dollar_total
 * @property int $parent_asset_non_retirement_other_investments_dollar_total
 * @property int $parent_asset_education_student_sibling_dollar_total
 * @property int $parent_asset_education_five_two_nine_dollar_total
 * @property int $parent_asset_education_coverdell_dollar_total
 * @property int $parent_asset_retirement_defined_benefit_dollar_total
 * @property int $parent_asset_retirement_four_o_one_dollar_total
 * @property int $parent_asset_retirement_ira_dollar_total
 * @property int $parent_asset_retirement_roth_ira_dollar_total
 * @property int $parent_asset_retirement_other_dollar_total
 * @property int $parent_insurance_permanent_cash_value_dollar_total
 * @property int $parent_farm_net_worth_dollar
 * @property int $parent_business_net_worth_dollar
 * @property int $parent_asset_real_estate_primary_net_worth_dollar
 * @property int $parent_asset_real_estate_other_net_worth_dollar
 * @property int $savings_tax_planning_min
 * @property int $savings_tax_planning_max
 * @property int $savings_assets_management_min
 * @property int $savings_assets_management_max
 * @property int $savings_borrowing_loan_min
 * @property int $savings_borrowing_loan_max
 * @property int $savings_cashflow_improvement_min
 * @property int $savings_cashflow_improvement_max
 * @property int $savings_other_min
 * @property int $savings_other_max
 * @property int $savings_outside_scholarship_min
 * @property int $savings_outside_scholarship_max
 * @property int $savings_other_financial_other_min
 * @property int $savings_other_financial_other_max
 * @property int $planning_retirement_age
 * @property float $planning_investment_rate
 * @property int $contributions_parent_annual_income_dollar
 * @property int $contributions_parent_assets_education_dollar
 * @property int $contributions_parent_assets_non_retirement_dollar
 * @property int $contributions_parent_assets_retirement_dollar
 * @property int $contributions_parent_life_insurance_dollar
 * @property int $contributions_parent_business_farm_dollar
 * @property int $contributions_parent_real_estate_dollar
 * @property int $contributions_parent_plus_private_loans_dollar
 * @property int $contributions_parent_other_contribution_dollar
 * @property int $contributions_student_annual_income_dollar
 * @property int $contributions_student_total_asset_dollar
 * @property int $contributions_student_loans_dollar
 * @property int $contributions_student_other_contribution_dollar
 * @property string|null $comments_parent_income
 * @property string|null $comments_parent_assets_education
 * @property string|null $comments_parent_assets_non_retirement
 * @property string|null $comments_parent_assets_retirement
 * @property string|null $comments_parent_assets_insurance
 * @property string|null $comments_parent_assets_business_farm
 * @property string|null $comments_parent_assets_real_estate
 * @property string|null $comments_student_income
 * @property string|null $comments_student_asset
 * @property string|null $comments_planned_parent_contribution
 * @property string|null $comments_planned_student_contribution
 * @property string|null $comments_savings_financial
 * @property string|null $comments_savings_other
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read \App\Models\Family\FamilyChild $child
 * @property-read string $is_shared_text
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario findSimilarSlugs($attribute, $config, $slug)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Family\Scenario onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereCollegeId1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereCollegeId2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereCollegeId3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereCollegeId4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereCollegeYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereCommentsParentAssetsBusinessFarm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereCommentsParentAssetsEducation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereCommentsParentAssetsInsurance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereCommentsParentAssetsNonRetirement($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereCommentsParentAssetsRealEstate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereCommentsParentAssetsRetirement($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereCommentsParentIncome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereCommentsPlannedParentContribution($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereCommentsPlannedStudentContribution($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereCommentsSavingsFinancial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereCommentsSavingsOther($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereCommentsStudentAsset($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereCommentsStudentIncome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereContributionsParentAnnualIncomeDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereContributionsParentAssetsEducationDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereContributionsParentAssetsNonRetirementDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereContributionsParentAssetsRetirementDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereContributionsParentBusinessFarmDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereContributionsParentLifeInsuranceDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereContributionsParentOtherContributionDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereContributionsParentPlusPrivateLoansDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereContributionsParentRealEstateDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereContributionsStudentAnnualIncomeDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereContributionsStudentLoansDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereContributionsStudentOtherContributionDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereContributionsStudentTotalAssetDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereFamilyChildId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereFamilyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereIsSharedWithFamily($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParent1IncomeWorkDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParent2IncomeWorkDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentAssetEducationCoverdellDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentAssetEducationFiveTwoNineDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentAssetEducationStudentSiblingDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentAssetNonRetirementAnnuitiesDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentAssetNonRetirementBondDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentAssetNonRetirementCertificateOfDepositDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentAssetNonRetirementCheckingSavingsDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentAssetNonRetirementMoneyMarketDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentAssetNonRetirementMutualDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentAssetNonRetirementOtherInvestmentsDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentAssetNonRetirementOtherSecuritiesDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentAssetNonRetirementStockDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentAssetNonRetirementTBillsDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentAssetNonRetirementTrustDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentAssetRealEstateOtherNetWorthDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentAssetRealEstatePrimaryNetWorthDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentAssetRetirementDefinedBenefitDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentAssetRetirementFourOOneDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentAssetRetirementIraDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentAssetRetirementPreviousEmployerDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentAssetRetirementRothIraDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentBusinessNetWorthDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentFarmNetWorthDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentIncomeAdditionalDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentIncomeAdjustedGrossDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentIncomeTaxDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentIncomeUntaxedDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereParentInsurancePermanentCashValueDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario wherePlanningInvestmentRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario wherePlanningRetirementAge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereSavingsAssetsManagementMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereSavingsAssetsManagementMin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereSavingsBorrowingLoanMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereSavingsBorrowingLoanMin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereSavingsCashflowImprovementMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereSavingsCashflowImprovementMin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereSavingsOtherFinancialOtherMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereSavingsOtherFinancialOtherMin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereSavingsOtherMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereSavingsOtherMin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereSavingsOutsideScholarshipMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereSavingsOutsideScholarshipMin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereSavingsTaxPlanningMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereSavingsTaxPlanningMin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereStudentAssetNonRetirementBondDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereStudentAssetNonRetirementCertificateOfDepositDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereStudentAssetNonRetirementCheckingSavingsDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereStudentAssetNonRetirementMoneyMarketDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereStudentAssetNonRetirementMutualDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereStudentAssetNonRetirementOtherInvestmentsDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereStudentAssetNonRetirementStockDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereStudentAssetNonRetirementTBillsDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereStudentAssetNonRetirementTrustDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereStudentAssetNonRetirementUgmaUtmaDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereStudentIncomeAdditionalDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereStudentIncomeAdjustedGrossDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereStudentIncomeTaxDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereStudentIncomeUntaxedDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereStudentIncomeWorkDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereSummary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Scenario whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Family\Scenario withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Family\Scenario withoutTrashed()
 * @mixin \Eloquent
 */
class Scenario extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;
    use Sluggable, SluggableScopeHelpers;

    protected $fillable = [
        'family_id',
        'family_child_id',
        'name',
        'slug',
        'is_shared_with_family',
        'summary',
        'college_year',
        'college_id_1',
        'college_id_2',
        'college_id_3',
        'college_id_4',
        'student_income_adjusted_gross_dollar',
        'student_income_work_dollar',
        'student_income_tax_dollar',
        'student_income_untaxed_dollar_total',
        'student_income_additional_dollar_total',
        'student_asset_non_retirement_checking_savings_dollar',
        'student_asset_non_retirement_ugma_utma_dollar',
        'student_asset_non_retirement_certificate_of_deposit_dollar',
        'student_asset_non_retirement_t_bills_dollar',
        'student_asset_non_retirement_money_market_dollar',
        'student_asset_non_retirement_mutual_dollar',
        'student_asset_non_retirement_stock_dollar',
        'student_asset_non_retirement_bond_dollar',
        'student_asset_non_retirement_trust_dollar',
        'student_asset_non_retirement_other_investments_dollar',
        'parent_income_adjusted_gross_dollar_total',
        'parent_1_income_work_dollar',
        'parent_2_income_work_dollar',
        'parent_income_tax_dollar_total',
        'parent_income_untaxed_dollar_total',
        'parent_income_additional_dollar_total',
        'parent_asset_non_retirement_checking_savings_dollar_total',
        'parent_asset_non_retirement_certificate_of_deposit_dollar_total',
        'parent_asset_non_retirement_t_bills_dollar_total',
        'parent_asset_non_retirement_money_market_dollar_total',
        'parent_asset_non_retirement_mutual_dollar_total',
        'parent_asset_non_retirement_stock_dollar_total',
        'parent_asset_non_retirement_bond_dollar_total',
        'parent_asset_non_retirement_trust_dollar_total',
        'parent_asset_non_retirement_other_securities_dollar_total',
        'parent_asset_non_retirement_annuities_dollar_total',
        'parent_asset_non_retirement_other_investments_dollar_total',
        'parent_asset_education_student_sibling_dollar_total',
        'parent_asset_education_five_two_nine_dollar_total',
        'parent_asset_education_coverdell_dollar_total',
        'parent_asset_retirement_defined_benefit_dollar_total',
        'parent_asset_retirement_four_o_one_dollar_total',
        'parent_asset_retirement_ira_dollar_total',
        'parent_asset_retirement_roth_ira_dollar_total',
        'parent_asset_retirement_other_dollar_total',
        'parent_insurance_permanent_cash_value_dollar_total',
        'parent_farm_net_worth_dollar',
        'parent_business_net_worth_dollar',
        'parent_asset_real_estate_primary_net_worth_dollar',
        'parent_asset_real_estate_other_net_worth_dollar',
        'savings_tax_planning_min',
        'savings_tax_planning_max',
        'savings_assets_management_min',
        'savings_assets_management_max',
        'savings_borrowing_loan_min',
        'savings_borrowing_loan_max',
        'savings_cashflow_improvement_min',
        'savings_cashflow_improvement_max',
        'savings_other_min',
        'savings_other_max',
        'savings_outside_scholarship_min',
        'savings_outside_scholarship_max',
        'savings_other_financial_other_min',
        'savings_other_financial_other_max',
        'planning_retirement_age',
        'planning_investment_rate',
        'contributions_parent_annual_income_dollar',
        'contributions_parent_assets_education_dollar',
        'contributions_parent_assets_non_retirement_dollar',
        'contributions_parent_assets_retirement_dollar',
        'contributions_parent_life_insurance_dollar',
        'contributions_parent_business_farm_dollar',
        'contributions_parent_real_estate_dollar',
        'contributions_parent_plus_private_loans_dollar',
        'contributions_parent_other_contribution_dollar',
        'contributions_student_annual_income_dollar',
        'contributions_student_total_asset_dollar',
        'contributions_student_loans_dollar',
        'contributions_student_other_contribution_dollar',
        'comments_parent_income',
        'comments_parent_assets_education',
        'comments_parent_assets_non_retirement',
        'comments_parent_assets_retirement',
        'comments_parent_assets_insurance',
        'comments_parent_assets_business_farm',
        'comments_parent_assets_real_estate',
        'comments_student_income',
        'comments_student_asset',
        'comments_planned_parent_contribution',
        'comments_planned_student_contribution',
        'comments_savings_financial',
        'comments_savings_other',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => [
                    'name'
                ],
            ]
        ];
    }

    /**
     * Get the Is-Shared Text Attribute
     *
     * @return string
     */
    public function getIsSharedTextAttribute()
    {
        if ($this->is_shared_with_family === 1) {
            return "Yes";
        }

        return "No";
    }

    public function getParentAvailableIncomeWorkDollarAttribute()
    {
        return $this->parent_1_income_work_dollar
            + $this->parent_2_income_work_dollar;
    }

    public function getParentAvailableAssetEducationDollarAttribute()
    {
        return $this->parent_asset_education_student_sibling_dollar_total +
            $this->parent_asset_education_five_two_nine_dollar_total +
            $this->parent_asset_education_coverdell_dollar_total;
    }

    public function getParentAvailableAssetNonRetirementDollarAttribute()
    {
        return $this->parent_asset_non_retirement_checking_savings_dollar_total +
        $this->parent_asset_non_retirement_certificate_of_deposit_dollar_total +
        $this->parent_asset_non_retirement_t_bills_dollar_total +
        $this->parent_asset_non_retirement_money_market_dollar_total +
        $this->parent_asset_non_retirement_mutual_dollar_total +
        $this->parent_asset_non_retirement_stock_dollar_total +
        $this->parent_asset_non_retirement_bond_dollar_total +
        $this->parent_asset_non_retirement_trust_dollar_total +
        $this->parent_asset_non_retirement_other_securities_dollar_total +
        $this->parent_asset_non_retirement_annuities_dollar_total +
        $this->parent_asset_non_retirement_other_investments_dollar_total;
    }

    public function getParentAvailableAssetRetirementDollarAttribute()
    {
        return $this->parent_asset_retirement_defined_benefit_dollar_total +
            $this->parent_asset_retirement_four_o_one_dollar_total +
            $this->parent_asset_retirement_ira_dollar_total +
            $this->parent_asset_retirement_roth_ira_dollar_total +
            $this->parent_asset_retirement_other_dollar_total;
    }

    public function getStudentAvailableAssetDollarAttribute()
    {
        return $this->student_asset_non_retirement_checking_savings_dollar +
            $this->student_asset_non_retirement_ugma_utma_dollar +
            $this->student_asset_non_retirement_certificate_of_deposit_dollar +
            $this->student_asset_non_retirement_t_bills_dollar +
            $this->student_asset_non_retirement_money_market_dollar +
            $this->student_asset_non_retirement_mutual_dollar +
            $this->student_asset_non_retirement_stock_dollar +
            $this->student_asset_non_retirement_bond_dollar +
            $this->student_asset_non_retirement_trust_dollar +
            $this->student_asset_non_retirement_other_investments_dollar;
    }

    /**
     * Every scenario belongs to a family
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function family()
    {
        return $this->belongsTo(Family::class, 'family_id');
    }

    /**
     * Every scenario belongs to a child/student
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function child()
    {
        return $this->belongsTo(FamilyChild::class, 'family_child_id');
    }
}
