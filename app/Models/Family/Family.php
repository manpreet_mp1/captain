<?php

namespace App\Models\Family;

use App\Repositories\Family\FamilyRepository;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use function in_array;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\Family\Family
 *
 * @property int $id
 * @property int|null $advisor_id
 * @property string $name
 * @property string $slug
 * @property int|null $parent_income_work_dollar_total
 * @property int|null $parent_income_adjusted_gross_dollar
 * @property int|null $parent_income_adjusted_gross_dollar_total
 * @property int $is_income_info_breakdown_completed
 * @property int|null $parent_income_tax_dollar
 * @property int|null $parent_income_tax_dollar_total
 * @property int $is_income_tax_breakdown_completed
 * @property int|null $parent_income_additional_tax_credit_dollar
 * @property int|null $parent_income_additional_tax_credit_dollar_total
 * @property int|null $parent_income_additional_child_support_paid_dollar
 * @property int|null $parent_income_additional_child_support_paid_dollar_total
 * @property int|null $parent_income_additional_work_study_dollar
 * @property int|null $parent_income_additional_work_study_dollar_total
 * @property int|null $parent_income_additional_grant_scholarship_dollar
 * @property int|null $parent_income_additional_grant_scholarship_dollar_total
 * @property int|null $parent_income_additional_combat_pay_dollar
 * @property int|null $parent_income_additional_combat_pay_dollar_total
 * @property int|null $parent_income_additional_cooperative_education_dollar
 * @property int|null $parent_income_additional_cooperative_education_dollar_total
 * @property int $is_income_additional_breakdown_completed
 * @property int|null $parent_income_untaxed_tax_deferred_dollar
 * @property int|null $parent_income_untaxed_tax_deferred_dollar_total
 * @property int|null $parent_income_untaxed_ira_deduction_dollar
 * @property int|null $parent_income_untaxed_ira_deduction_dollar_total
 * @property int|null $parent_income_untaxed_child_support_received_dollar
 * @property int|null $parent_income_untaxed_child_support_received_dollar_total
 * @property int|null $parent_income_untaxed_tax_exempt_dollar
 * @property int|null $parent_income_untaxed_tax_exempt_dollar_total
 * @property int|null $parent_income_untaxed_ira_distribution_dollar
 * @property int|null $parent_income_untaxed_ira_distribution_dollar_total
 * @property int|null $parent_income_untaxed_pension_portion_dollar
 * @property int|null $parent_income_untaxed_pension_portion_dollar_total
 * @property int|null $parent_income_untaxed_paid_to_military_dollar
 * @property int|null $parent_income_untaxed_paid_to_military_dollar_total
 * @property int|null $parent_income_untaxed_veteran_benefit_dollar
 * @property int|null $parent_income_untaxed_veteran_benefit_dollar_total
 * @property int|null $parent_income_untaxed_other_dollar
 * @property int|null $parent_income_untaxed_other_dollar_total
 * @property int $is_income_untaxed_breakdown_completed
 * @property int|null $parent_asset_education_student_sibling_dollar
 * @property int|null $parent_asset_education_student_sibling_dollar_total
 * @property int|null $parent_asset_education_five_two_nine_dollar
 * @property int|null $parent_asset_education_five_two_nine_dollar_total
 * @property int|null $parent_asset_education_coverdell_dollar
 * @property int|null $parent_asset_education_coverdell_dollar_total
 * @property int $is_assets_eduction_breakdown_completed
 * @property int|null $parent_asset_non_retirement_checking_savings_dollar
 * @property int|null $parent_asset_non_retirement_checking_savings_dollar_total
 * @property int|null $parent_asset_non_retirement_certificate_of_deposit_dollar
 * @property int|null $parent_asset_non_retirement_certificate_of_deposit_dollar_total
 * @property int|null $parent_asset_non_retirement_t_bills_dollar
 * @property int|null $parent_asset_non_retirement_t_bills_dollar_total
 * @property int|null $parent_asset_non_retirement_money_market_dollar
 * @property int|null $parent_asset_non_retirement_money_market_dollar_total
 * @property int|null $parent_asset_non_retirement_mutual_dollar
 * @property int|null $parent_asset_non_retirement_mutual_dollar_total
 * @property int|null $parent_asset_non_retirement_stock_dollar
 * @property int|null $parent_asset_non_retirement_stock_dollar_total
 * @property int|null $parent_asset_non_retirement_bond_dollar
 * @property int|null $parent_asset_non_retirement_bond_dollar_total
 * @property int|null $parent_asset_non_retirement_trust_dollar
 * @property int|null $parent_asset_non_retirement_trust_dollar_total
 * @property int|null $parent_asset_non_retirement_other_securities_dollar
 * @property int|null $parent_asset_non_retirement_other_securities_dollar_total
 * @property int|null $parent_asset_non_retirement_annuities_dollar
 * @property int|null $parent_asset_non_retirement_annuities_dollar_total
 * @property int|null $parent_asset_non_retirement_other_investments_dollar
 * @property int|null $parent_asset_non_retirement_other_investments_dollar_total
 * @property int $is_assets_non_retirement_breakdown_completed
 * @property int|null $parent_asset_retirement_defined_benefit_dollar
 * @property int|null $parent_asset_retirement_defined_benefit_dollar_total
 * @property int|null $parent_asset_retirement_four_o_one_dollar
 * @property int|null $parent_asset_retirement_four_o_one_dollar_total
 * @property int|null $parent_asset_retirement_ira_dollar
 * @property int|null $parent_asset_retirement_ira_dollar_total
 * @property int|null $parent_asset_retirement_roth_ira_dollar
 * @property int|null $parent_asset_retirement_roth_ira_dollar_total
 * @property int|null $parent_asset_retirement_previous_employer_dollar
 * @property int|null $parent_asset_retirement_previous_employer_dollar_total
 * @property int|null $parent_asset_retirement_other_dollar
 * @property int|null $parent_asset_retirement_other_dollar_total
 * @property int $is_assets_retirement_breakdown_completed
 * @property array|null $parent_asset_real_estate_options
 * @property int|null $parent_asset_real_estate_primary_purchase_dollar
 * @property int|null $parent_asset_real_estate_primary_value_dollar
 * @property int|null $parent_asset_real_estate_primary_owned_dollar
 * @property float|null $parent_asset_real_estate_primary_interest_percent
 * @property int|null $parent_asset_real_estate_primary_payment_dollar
 * @property int|null $parent_asset_real_estate_other_purchase_dollar
 * @property int|null $parent_asset_real_estate_other_value_dollar
 * @property int|null $parent_asset_real_estate_other_owned_dollar
 * @property float|null $parent_asset_real_estate_other_interest_percent
 * @property int|null $parent_asset_real_estate_other_payment_dollar
 * @property int $is_real_estate_breakdown_completed
 * @property int|null $parent_business_value_dollar
 * @property int|null $parent_farm_value_dollar
 * @property int $is_business_breakdown_completed
 * @property int $is_insurance_breakdown_completed
 * @property int $is_completed_assessment
 * @property int $assessment_current_step_num
 * @property int $household_dependent_children_num
 * @property int $is_parent_asset_non_retirement
 * @property int|null $parent_asset_non_retirement_dollar_total
 * @property int $is_parent_asset_education
 * @property int|null $parent_asset_education_dollar_total
 * @property int $is_parent_asset_retirement_accounts
 * @property int|null $parent_asset_retirement_dollar_total
 * @property int|null $parent_opinion_stretch_household_income
 * @property int|null $parent_opinion_jeopardize_retirement
 * @property int|null $parent_opinion_ways_to_borrow
 * @property int|null $parent_opinion_cashing_investments
 * @property int|null $parent_opinion_no_clear_plan
 * @property int|null $parent_opinion_qualify_for_financial_aid
 * @property string|null $efc_school_year
 * @property int $is_other_family_members
 * @property int|null $other_family_members_num
 * @property int $is_completed_efc
 * @property string|null $source
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Family\FamilyChild[] $children
 * @property-read string $is_completed_efc_text
 * @property-read int $is_qualified
 * @property-read string $is_qualified_text
 * @property-read string $name_text
 * @property-read int|mixed $number_of_other_family_members
 * @property-read int|mixed $parent_asset_real_estate_primary_net_worth
 * @property-read int|mixed $parent_asset_real_estate_secondary_net_worth
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Family\FamilyParent[] $parents
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family findSimilarSlugs($attribute, $config, $slug)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Family\Family onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereAdvisorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereAssessmentCurrentStepNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereEfcSchoolYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereHouseholdDependentChildrenNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereIsAssetsEductionBreakdownCompleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereIsAssetsNonRetirementBreakdownCompleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereIsAssetsRetirementBreakdownCompleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereIsBusinessBreakdownCompleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereIsCompletedAssessment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereIsCompletedEfc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereIsIncomeAdditionalBreakdownCompleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereIsIncomeInfoBreakdownCompleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereIsIncomeTaxBreakdownCompleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereIsIncomeUntaxedBreakdownCompleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereIsInsuranceBreakdownCompleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereIsOtherFamilyMembers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereIsParentAssetEducation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereIsParentAssetNonRetirement($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereIsParentAssetRetirementAccounts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereIsRealEstateBreakdownCompleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereOtherFamilyMembersNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetEducationCoverdellDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetEducationCoverdellDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetEducationDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetEducationFiveTwoNineDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetEducationFiveTwoNineDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetEducationStudentSiblingDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetEducationStudentSiblingDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetNonRetirementAnnuitiesDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetNonRetirementAnnuitiesDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetNonRetirementBondDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetNonRetirementBondDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetNonRetirementCertificateOfDepositDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetNonRetirementCertificateOfDepositDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetNonRetirementCheckingSavingsDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetNonRetirementCheckingSavingsDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetNonRetirementDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetNonRetirementMoneyMarketDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetNonRetirementMoneyMarketDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetNonRetirementMutualDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetNonRetirementMutualDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetNonRetirementOtherInvestmentsDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetNonRetirementOtherInvestmentsDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetNonRetirementOtherSecuritiesDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetNonRetirementOtherSecuritiesDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetNonRetirementStockDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetNonRetirementStockDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetNonRetirementTBillsDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetNonRetirementTBillsDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetNonRetirementTrustDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetNonRetirementTrustDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetRealEstateOptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetRealEstateOtherInterestPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetRealEstateOtherOwnedDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetRealEstateOtherPaymentDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetRealEstateOtherPurchaseDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetRealEstateOtherValueDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetRealEstatePrimaryInterestPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetRealEstatePrimaryOwnedDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetRealEstatePrimaryPaymentDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetRealEstatePrimaryPurchaseDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetRealEstatePrimaryValueDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetRetirementDefinedBenefitDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetRetirementDefinedBenefitDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetRetirementDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetRetirementFourOOneDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetRetirementFourOOneDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetRetirementIraDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetRetirementIraDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetRetirementOtherDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetRetirementOtherDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetRetirementPreviousEmployerDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetRetirementPreviousEmployerDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetRetirementRothIraDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentAssetRetirementRothIraDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentBusinessValueDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentFarmValueDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeAdditionalChildSupportPaidDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeAdditionalChildSupportPaidDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeAdditionalCombatPayDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeAdditionalCombatPayDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeAdditionalCooperativeEducationDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeAdditionalCooperativeEducationDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeAdditionalGrantScholarshipDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeAdditionalGrantScholarshipDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeAdditionalTaxCreditDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeAdditionalTaxCreditDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeAdditionalWorkStudyDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeAdditionalWorkStudyDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeAdjustedGrossDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeAdjustedGrossDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeTaxDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeTaxDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeUntaxedChildSupportReceivedDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeUntaxedChildSupportReceivedDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeUntaxedIraDeductionDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeUntaxedIraDeductionDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeUntaxedIraDistributionDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeUntaxedIraDistributionDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeUntaxedOtherDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeUntaxedOtherDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeUntaxedPaidToMilitaryDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeUntaxedPaidToMilitaryDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeUntaxedPensionPortionDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeUntaxedPensionPortionDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeUntaxedTaxDeferredDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeUntaxedTaxDeferredDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeUntaxedTaxExemptDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeUntaxedTaxExemptDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeUntaxedVeteranBenefitDollar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeUntaxedVeteranBenefitDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentIncomeWorkDollarTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentOpinionCashingInvestments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentOpinionJeopardizeRetirement($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentOpinionNoClearPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentOpinionQualifyForFinancialAid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentOpinionStretchHouseholdIncome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereParentOpinionWaysToBorrow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Family\Family whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Family\Family withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Family\Family withoutTrashed()
 * @mixin \Eloquent
 * @property-read int|null $parent_income_additional_total
 * @property-read int|null $parent_income_additional_dollar_total
 * @property-read int|null $parent_income_untaxed_dollar_total
 * @property-read int|mixed $parent_asset_real_estate_other_net_worth_dollar
 * @property-read int|mixed $parent_asset_real_estate_primary_net_worth_dollar
 */
class Family extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;
    use Sluggable, SluggableScopeHelpers;

    protected $guarded = ['id'];

    const ACTIVE_SUBSCRIPTION = [
        'smarttrack',
        'smarttrack-advisor',
        'bundle',
        'smarttrack-3-payment',
        'smarttrack-advisor-3-payment',
        'bundle-3-payment',
    ];

    const SOFTWARE_BUNDLE_SUBSCRIPTION = [
        'smarttrack',
        'bundle',
        'smarttrack-3-payment',
        'bundle-3-payment',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'parent_asset_real_estate_options' => 'array',
        'child_support_options' => 'array',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => [
                    'name'
                ],
            ]
        ];
    }

    /**
     * A family can have multiple parents
     *
     * @return HasMany
     */
    public function parents()
    {
        return $this->hasMany(FamilyParent::class);
    }

    /**
     * Get parent one
     *
     * @return HasMany
     */
    public function parentOne()
    {
        return $this->parents()->first();
    }

    /**
     * Get parent two
     *
     * @return HasMany
     */
    public function parentTwo()
    {
        return $this->parents()->skip(1)->first();
    }

    /**
     * A family can have multiple students
     *
     * @return HasMany
     */
    public function children()
    {
        return $this->hasMany(FamilyChild::class);
    }

    /**
     * Get is active flag for subscription check
     *
     * @return int
     */
    public function getIsActiveAttribute()
    {
        if (in_array($this->subscription, self::ACTIVE_SUBSCRIPTION)) {
            return 1;
        }

        return 0;
    }

    /**
     * Get is active flag for subscription check
     *
     * @return int
     */
    public function getIsActiveClassAttribute()
    {
        if ($this->is_active === 1) {
            return "";
        }

        return "inactive-overlay";
    }

    /**
     * Get if the family is on software or bundle pricing
     *
     * @return int
     */
    public function getIsSoftwareOrBundleAttribute()
    {
        if (in_array($this->subscription, self::SOFTWARE_BUNDLE_SUBSCRIPTION)) {
            return 1;
        }

        return 0;
    }

    /**
     * Get the Is-Qualified Attribute
     *
     * @return int
     */
    public function getIsQualifiedAttribute()
    {
        return (new FamilyRepository())->checkIfQualifiedOrNot($this);
    }

    /**
     * Get the Is-Qualified Text Attribute
     *
     * @return string
     */
    public function getIsQualifiedTextAttribute()
    {
        if ((new FamilyRepository())->checkIfQualifiedOrNot($this) === 1) {
            return "Yes";
        }

        return "No";
    }

    /**
     * Get the Is-Completed EFC Text Attribute
     *
     * @return string
     */
    public function getIsCompletedEfcTextAttribute()
    {
        if ($this->is_completed_efc === 1) {
            return "Yes";
        }

        return "No";
    }

    /**
     * Get the Is-Completed EFC Text Attribute
     *
     * @return string
     */
    public function getNameTextAttribute()
    {
        return $this->name . ' Family';
    }

    /**
     * Get total of all income additional
     *
     * @return int|null
     */
    public function getParentIncomeAdditionalDollarTotalAttribute()
    {
        return $this->parent_income_additional_tax_credit_dollar_total +
            $this->parent_income_additional_child_support_paid_dollar_total +
            $this->parent_income_additional_work_study_dollar_total +
            $this->parent_income_additional_grant_scholarship_dollar_total +
            $this->parent_income_additional_combat_pay_dollar_total +
            $this->parent_income_additional_cooperative_education_dollar_total;
    }

    /**
     * Get total of all income additional
     *
     * @return int|null
     */
    public function getParentIncomeUntaxedDollarTotalAttribute()
    {
        return $this->parent_income_untaxed_tax_deferred_dollar_total +
            $this->parent_income_untaxed_ira_deduction_dollar_total +
            $this->parent_income_untaxed_child_support_received_dollar_total +
            $this->parent_income_untaxed_tax_exempt_dollar_total +
            $this->parent_income_untaxed_ira_distribution_dollar_total +
            $this->parent_income_untaxed_pension_portion_dollar_total +
            $this->parent_income_untaxed_paid_to_military_dollar_total +
            $this->parent_income_untaxed_veteran_benefit_dollar_total +
            $this->parent_income_untaxed_other_dollar_total;
    }

    /**
     * If breakdown complete show sum else show total
     *
     * @return int|null
     */
    public function getParentAssetNonRetirementDollarTotalAttribute()
    {
        if ($this->is_assets_non_retirement_breakdown_completed == 1) {
            return $this->parent_asset_non_retirement_checking_savings_dollar_total +
                $this->parent_asset_non_retirement_certificate_of_deposit_dollar_total +
                $this->parent_asset_non_retirement_t_bills_dollar_total +
                $this->parent_asset_non_retirement_money_market_dollar_total +
                $this->parent_asset_non_retirement_mutual_dollar_total +
                $this->parent_asset_non_retirement_stock_dollar_total +
                $this->parent_asset_non_retirement_bond_dollar_total +
                $this->parent_asset_non_retirement_trust_dollar_total +
                $this->parent_asset_non_retirement_other_securities_dollar_total +
                $this->parent_asset_non_retirement_annuities_dollar_total +
                $this->parent_asset_non_retirement_other_investments_dollar_total;
        }

        return $this->attributes['parent_asset_non_retirement_dollar_total'];
    }

    /**
     * If breakdown complete show sum else show total
     *
     * @return int|null
     */
    public function getParentAssetEducationDollarTotalAttribute()
    {
        if ($this->is_parent_asset_education == 1) {
            return $this->parent_asset_education_student_sibling_dollar_total +
                $this->parent_asset_education_five_two_nine_dollar_total +
                $this->parent_asset_education_coverdell_dollar_total;
        }

        return $this->attributes['parent_asset_education_dollar_total'];
    }

    /**
     * If breakdown complete show sum else show total
     *
     * @return int|null
     */
    public function getParentAssetRetirementDollarTotalAttribute()
    {
        if ($this->is_parent_asset_retirement_accounts == 1) {
            return $this->parent_asset_retirement_defined_benefit_dollar_total +
                $this->parent_asset_retirement_four_o_one_dollar_total +
                $this->parent_asset_retirement_ira_dollar_total +
                $this->parent_asset_retirement_roth_ira_dollar_total +
                $this->parent_asset_retirement_previous_employer_dollar_total +
                $this->parent_asset_retirement_other_dollar_total;
        }

        return $this->attributes['parent_asset_retirement_dollar_total'];
    }

    /**
     * Get value using logic
     *
     * @return int|mixed
     */
    public function getNumberOfOtherFamilyMembersAttribute()
    {
        if ($this->is_other_family_members === 1) {
            return $this->attributes['number_of_other_family_members'];
        }

        return 0;
    }

    /**
     * Get value using logic
     *
     * @return int|mixed
     */
    public function getParentAssetRealEstatePrimaryNetWorthDollarAttribute()
    {
        $value =  $this->attributes['parent_asset_real_estate_primary_value_dollar'] - $this->attributes['parent_asset_real_estate_primary_owned_dollar'];

        if ($value > 0) {
            return $value;
        }

        return 0;
    }

    /**
     * Get value using logic
     *
     * @return int|mixed
     */
    public function getParentAssetRealEstateOtherNetWorthDollarAttribute()
    {
        $value =  $this->attributes['parent_asset_real_estate_other_value_dollar'] - $this->attributes['parent_asset_real_estate_other_owned_dollar'];

        if ($value > 0) {
            return $value;
        }

        return 0;
    }
}
