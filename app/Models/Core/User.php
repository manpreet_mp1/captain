<?php

namespace App\Models\Core;

use App\Models\Family\Family;
use App\Models\Family\FamilyChild;
use App\Models\Family\FamilyParent;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notification;
use OwenIt\Auditing\Contracts\Auditable;
use function substr;

/**
 * App\Models\Core\User
 *
 * @property mixed family_id
 * @property mixed source
 * @property mixed parent
 * @property mixed id
 * @property mixed last_name
 * @property mixed first_name
 * @property mixed full_name
 * @property mixed email
 * @property mixed mobile
 * @property mixed role
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read \App\Models\Family\Family $family
 * @property-read string $first_name_initial
 * @property-read null|string $formatted_mobile
 * @property-read int $is_advisor
 * @property-read int $is_parent
 * @property-read string $notification_full_name
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\User advisors()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\User findSimilarSlugs($attribute, $config, $slug)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\User newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Core\User onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\User query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\User whereSlug($slug)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Core\User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Core\User withoutTrashed()
 * @mixin \Eloquent
 * @property string $password
 * @property string $tmp_password
 * @property int $is_account
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\User whereFamilyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\User whereIsAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\User whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\User whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\User whereTmpPassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\User students()
 */
class User extends Authenticatable implements Auditable
{
    use Notifiable;
    use Sluggable, SluggableScopeHelpers;
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'family_id',
        'first_name',
        'last_name',
        'slug',
        'email',
        'mobile',
        'role',
        'password',
        'tmp_password',
        'is_account',
        'acuity_calendar_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'family_id' => 'integer',
        'mobile' => 'integer',
        'is_account' => 'integer',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * Route notifications for the Slack channel.
     *
     * @param  Notification  $notification
     * @return string
     */
    public function routeNotificationForSlack($notification)
    {
        // @todo -remove me and use log
        return config('slack.slack_webhook_url');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => [
                    'first_name',
                    'last_name',
                ],
            ]
        ];
    }

    /** RELATIONSHIP AND METHOD RELATIONSHIP FOR FROM HERE */

    /**
     * Some users are parent
     *
     * @return HasOne
     */
    public function parent()
    {
        return $this->hasOne(FamilyParent::class);
    }

    /**
     * Some users are children
     *
     * @return HasOne
     */
    public function child()
    {
        return $this->hasOne(FamilyChild::class);
    }

    /**
     * All users belongs to family, except advisors
     *
     * @return BelongsTo
     */
    public function family()
    {
        return $this->belongsTo(Family::class);
    }

    /**
     * Get the parent two
     *
     * @return mixed
     */
    public function userTwo()
    {
        return self::where('family_id', $this->family_id)
            ->where('id', '<>', $this->id)
            ->first();
    }

    /** MUTATOR FROM HERE */

    /**
     * Get the full name
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Get the first name initial
     *
     * @return string
     */
    public function getFirstNameInitialAttribute()
    {
        return substr($this->first_name, 0, 1);
    }

    /**
     * Get the notification full name
     *
     * @return string
     */
    public function getNotificationFullNameAttribute()
    {
        return $this->full_name . " (" . $this->id . " - " . $this->email . ")";
    }

    /**
     * Get mobile formatted
     *
     * @return null|string
     */
    public function getFormattedMobileAttribute()
    {
        if ($phone = $this->mobile) {
            try {
                return '(' . substr($phone, 0, 3) . ') ' . substr($phone, 3, 3) . '-' . substr($phone, 6);
            } catch (Exception $e) {
                return null;
            }
        }

        return null;
    }

    /**
     * If the user is a parent
     *
     * @return int
     */
    public function getIsParentAttribute()
    {
        if ($this->role === "PARENT") {
            return 1;
        }

        return 0;
    }

    /**
     * If the user is a advisor
     *
     * @return int
     */
    public function getIsAdvisorAttribute()
    {
        if ($this->role === "ADVISOR") {
            return 1;
        }

        return 0;
    }

    /** SCOPE FROM HERE */

    /**
     * Filter users to only advisors
     *
     * @param  Builder  $query
     * @return Builder
     */
    public function scopeAdvisors($query)
    {
        return $query->where('role', "ADVISOR");
    }

    /**
     * Filter users to only students
     *
     * @param  Builder  $query
     * @return Builder
     */
    public function scopeStudents($query)
    {
        return $query->where('role', "STUDENT");
    }
}
