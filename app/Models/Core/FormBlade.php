<?php

namespace App\Models\Core;

use function array_merge;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\Core\FormBlade
 *
 * @property mixed active_class_blade_name
 * @property mixed blade_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read array $active_class_blade_name_array
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\FormBlade caseCreation()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\FormBlade efc()
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\FormBlade newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\FormBlade newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Core\FormBlade onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\FormBlade query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\FormBlade visibleInMenu()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Core\FormBlade withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Core\FormBlade withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string $type
 * @property string|null $menu_name
 * @property string|null $menu_sub_text
 * @property string|null $icon_class
 * @property int $is_visible_in_menu
 * @property int $order_seq
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\FormBlade whereActiveClassBladeName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\FormBlade whereBladeName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\FormBlade whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\FormBlade whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\FormBlade whereIconClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\FormBlade whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\FormBlade whereIsVisibleInMenu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\FormBlade whereMenuName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\FormBlade whereMenuSubText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\FormBlade whereOrderSeq($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\FormBlade whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Core\FormBlade whereUpdatedAt($value)
 */
class FormBlade extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'blade_name',
        'menu_name',
        'menu_sub_text',
        'is_visible_in_menu',
        'icon_class',
        'order_seq',
        'active_class_blade_name',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_visible_in_menu' => 'integer',
        'active_class_blade_name' => 'array',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * Get the active class blade array, add the blade name
     *
     * @return array
     */
    public function getActiveClassBladeNameArrayAttribute()
    {
        return array_merge($this->active_class_blade_name, [$this->blade_name]);
    }

    /**
     * Filter the case creation records
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeCaseCreation($query)
    {
        return $query->where('type', 'CASE_CREATION');
    }

    /**
     * Filter the efc records
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeEfc($query)
    {
        return $query->where('type', 'EFC');
    }

    /**
     * Filter the records that are visible in menu
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeVisibleInMenu($query)
    {
        return $query->where('is_visible_in_menu', 1);
    }
}
