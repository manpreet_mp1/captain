<?php
namespace App\Events\Parent;

use App\Models\Core\User;
use Illuminate\Queue\SerializesModels;

class ParentRegisteredEvent
{
    use SerializesModels;

    public $user;

    /**
     * Create a new event instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
