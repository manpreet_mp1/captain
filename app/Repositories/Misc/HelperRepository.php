<?php

namespace App\Repositories\Misc;

use Exception;
use function abort;
use function array_combine;
use function explode;
use function pow;
use function str_replace;

class HelperRepository
{
    /**
     * Create an array from csv with key as headers
     *
     * @param $filePath
     * @return array
     */
    public function createArrayFromCsvFile($filePath)
    {
        $data = [];
        $header = null;
        if (($handle = fopen($filePath, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000000, ',')) !== false) {
                if (!$header) {
                    $header = $row;
                } else {
                    try {
                        if (isset($row) && count($row) > 0 && $row[0]) {
                            $data[] = array_combine($header, $row);
                        }
                    } catch (Exception $exception) {
                        $message = [
                            'exception' => $exception->getMessage(),
                            'header' => $header,
                            'row' => $row,
                        ];
                        abort(422, json_encode($message));
                    }
                }
            }
            fclose($handle);
        }

        return $data;

//        Below code skips empty column
//        $records = array_map('str_getcsv', file($filePath));
//        dd($records);
//        array_walk($records, function (&$a) use ($records) {
//            $a = array_combine($records[0], $a);
//        });
//        array_shift($records);
//        return $records;
    }

    /**
     * Add http
     *
     * @param $url
     * @return string
     */
    public function addHttp($url)
    {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "http://" . $url;
        }

        return $url;
    }

    /**
     * Extract number for dollar amount
     *
     * @param $value
     * @param bool $treatNegativeAsZero
     * @return mixed
     */
    public function extractNumberFromDollar($value, $treatNegativeAsZero = true)
    {
        $value = explode(".", $value)[0];
        $value = str_replace("$", "", $value);
        $value = str_replace(",", "", $value);
        $value = str_replace(" ", "", $value);

        if ($treatNegativeAsZero && (int)$value < 0) {
            return 0;
        }

        return $value;
    }

    /**
     * Get compound interest
     *
     * @param $p
     * @param $r
     * @param $t
     * @param int $n
     * @return float|int
     */
    public function getCompoundInterest($p, $r, $t, $n = 1)
    {
        return ($p * (pow((1 + $r/$n), $t*$n))) - $p;
    }
}
