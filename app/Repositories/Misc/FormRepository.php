<?php

namespace App\Repositories\Misc;

use App\Models\Core\FormBlade;
use Illuminate\Support\Collection;
use function in_array;

class FormRepository
{
    /**
     * Get the current
     *
     * @param $scopeName
     * @param $currentStep
     * @return string
     */
    public function getCurrentFormSlug($scopeName, $currentStep)
    {
        return FormBlade::{$scopeName}()
            ->where('order_seq', $currentStep)
            ->first()
            ->blade_name;
    }

    /**
     * Get the form blade menu
     *
     * @param $scopeName
     * @param $slug
     * @return mixed
     */
    public function getFormBladeMenu($scopeName, $slug)
    {
        $output = new Collection();

        foreach (FormBlade::{$scopeName}()->visibleInMenu()->orderBy('order_seq')->get() as $menu) {
            $output->push((object)[
                'blade_name' => $menu->blade_name,
                'icon_class' => $menu->icon_class,
                'menu_name' => $menu->menu_name,
                'active_class' => in_array($slug, $menu->active_class_blade_name_array) ? "current" : "",
                'href' => "javascript:;",
                'menu_sub_text' => $menu->menu_sub_text,
            ]);
        }

        return $output;
    }
}
