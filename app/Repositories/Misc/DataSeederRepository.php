<?php

namespace App\Repositories\Misc;

use App\Models\Core\User;
use App\Models\Family\Family;
use App\Models\Family\FamilyChild;
use App\Models\Family\FamilyParent;
use function factory;

class DataSeederRepository
{
    /**
     * Create family
     *
     * @param int $numberOfParents
     * @param int $numberOfChildren
     * @param int $completedAssessment
     * @param int $completedEfc
     * @param bool $advisorId
     * @param string $subscription
     */
    public function createFamily(
        $numberOfParents = 2,
        $numberOfChildren = 4,
        $completedAssessment = 1,
        $completedEfc = 1,
        $advisorId = null,
        $subscription = 'BUNDLE'
    ) {
        // based on value passed create a advisor
        // create a family, with given number of children
        $family = factory(Family::class)->create([
            'household_dependent_children_num' => $numberOfChildren,
            'is_completed_assessment' => $completedAssessment,
            'is_completed_efc' => $completedEfc,
            'advisor_id' => $advisorId,
            'subscription' => $subscription,
        ]);

        // create parents
        $parentUsers = factory(User::class, $numberOfParents)->create([
            'family_id' => $family->id,
            'role' => 'PARENT',
        ]);
        foreach ($parentUsers as $parentUser) {
            factory(FamilyParent::class)->create([
                'user_id' => $parentUser->id,
                'family_id' => $family->id,
                'marital_status_id' => $numberOfParents,
            ]);
        }

        // create students
        $studentUsers = factory(User::class, $numberOfChildren)->create([
            'family_id' => $family->id,
            'role' => 'STUDENT',
        ]);
        foreach ($studentUsers as $studentUser) {
            factory(FamilyChild::class)->create([
                'user_id' => $studentUser->id,
                'family_id' => $family->id
            ]);
        }
    }
}
