<?php

namespace App\Repositories\Misc;

use Illuminate\Http\Request;
use function in_array;

class FinancialCoachAssessmentRepository
{
    /**
     * Check from request if a user is qualified or not
     *
     * @param Request $request
     * @return string
     */
    public function getQualifiedOrNotQualified(Request $request)
    {
        // Get parent incomes
        $parentOneIncome = $request->input("parentOneIncome") ?: "Less than $75,000";
        $parentTwoIncome = $request->input("parentTwoIncome") ?: "Less than $75,000";
        $parentTwoIncome = $parentTwoIncome !== '_____' ? $parentTwoIncome : "Less than $75,000";

        // Get all the assets
        $assetBank = $request->input("assetBank") ?: "Less than $50,000";
        $collegeSaving = $request->input("collegeSaving") ?: "We DO NOT have college savings accounts.";
        $investment = $request->input("investment") ?: "We DO NOT have any investment accounts.";
        $retirement = $request->input("retirement") ?: "We DO NOT have any retirement accounts.";

        // Both has income less than 75k - Not Qualified
        if ($parentOneIncome === "Less than $75,000" && $parentTwoIncome === "Less than $75,000") {
            return "notQualified";
        }

        // No asset more than 50K
        if ($assetBank === "Less than $50,000"
            && (in_array($collegeSaving, [
                "We DO NOT have college savings accounts.",
                "Less than $50,000"
            ]))
            && (in_array($investment, [
                "We DO NOT have any investment accounts.",
                "Less than $50,000"
            ]))
            && (in_array($retirement, [
                "We DO NOT have any retirement accounts.",
                "Less than $50,000"
            ]))
        ) {
            return "notQualified";
        }

        return "qualified";
    }

    /**
     * Check from request if a user is qualified or not
     *
     * @param array $data
     * @return array
     */
    public function getQualifiedOrNotQualifiedDb(array $data)
    {
        // transform to array
        $answers = $this->transformAnswersDbToArray($data['answers']);

        // Get parent incomes
        $parentOneIncome = isset($answers['lr6cwqzxgmqx']) ? $answers['lr6cwqzxgmqx'] : "Less than $75,000";
        $parentTwoIncome = isset($answers['aLSKs1uT4qsz']) ? $answers['aLSKs1uT4qsz'] : "Less than $75,000";

        // Get all the assets
        $assetBank = isset($answers['eQnez7vEPsrq']) ? $answers['eQnez7vEPsrq'] : "Less than $50,000";
        $collegeSaving = isset($answers['LHuoTl80XQqV']) ? $answers['LHuoTl80XQqV'] : "We DO NOT have college savings accounts.";
        $investment = isset($answers['jFHqJS03zTs5']) ? $answers['jFHqJS03zTs5'] : "We DO NOT have any investment accounts.";
        $retirement = isset($answers['pNffl2nGl6jS']) ? $answers['pNffl2nGl6jS'] : "We DO NOT have any retirement accounts.";

        $data = [
            'parentOneIncome' => $parentOneIncome,
            'parentTwoIncome' => $parentTwoIncome,
            'assetBank' => $assetBank,
            'collegeSaving' => $collegeSaving,
            'investment' => $investment,
            'retirement' => $retirement,
        ];

        // Both has income less than 75k - Not Qualified
        if ($parentOneIncome === "Less than $75,000" && $parentTwoIncome === "Less than $75,000") {
            return [
                'data' => $data,
                'status' => "notQualified"
            ];
        }

        // No asset more than 50K
        if ($assetBank === "Less than $50,000"
            && (in_array($collegeSaving, [
                "We DO NOT have college savings accounts.",
                "Less than $50,000"
            ]))
            && (in_array($investment, [
                "We DO NOT have any investment accounts.",
                "Less than $50,000"
            ]))
            && (in_array($retirement, [
                "We DO NOT have any retirement accounts.",
                "Less than $50,000"
            ]))
        ) {
            return [
                'data' => $data,
                'status' => "notQualified"
            ];
        }

        return [
            'data' => $data,
            'status' => "qualified"
        ];
    }

    /**
     * Transform the DB answers to array with key
     *
     * @param array $answers
     * @return array
     */
    private function transformAnswersDbToArray(array $answers)
    {
        $output = [];

        // only transform choice
        foreach ($answers as $answer) {
            switch ($answer['type']) {
                case "choice":
                    $output[$answer['field']['id']] = $answer['choice']['label'];

                    break;
            }
        }

        return $output;
    }
}
