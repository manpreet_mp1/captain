<?php

namespace App\Repositories\Premium;

use App\Models\Data\CfgCalculator;
use App\Models\Data\Efc\EfcTableA1;
use App\Models\Data\Efc\EfcTableA2;
use App\Models\Data\Efc\EfcTableA3;
use App\Models\Data\Efc\EfcTableA35;
use App\Models\Data\Efc\EfcTableA4;
use App\Models\Data\Efc\EfcTableA5;
use App\Models\Data\Efc\EfcTableA6;
use App\Models\Data\Efc\EfcTableA7;
use App\Models\Family\Family;
use App\Models\Family\FamilyChild;
use App\Models\Family\Scenario;
use Illuminate\Support\Carbon;
use function collect;
use function max;
use function min;
use function now;
use function optional;

class EfcCalculatorRepository
{
    private $cfgCalculator;
    private $efcTableA1;
    private $efcTableA2;
    private $efcTableA3;
    private $efcTableA35;
    private $efcTableA4;
    private $efcTableA5;
    private $efcTableA6;
    private $efcTableA7;

    public function __construct()
    {
        $this->cfgCalculator = CfgCalculator::all();
        $this->efcTableA1 = EfcTableA1::all();
        $this->efcTableA2 = EfcTableA2::all();
        $this->efcTableA3 = EfcTableA3::all();
        $this->efcTableA35 = EfcTableA35::all();
        $this->efcTableA4 = EfcTableA4::all();
        $this->efcTableA5 = EfcTableA5::all();
        $this->efcTableA6 = EfcTableA6::all();
        $this->efcTableA7 = EfcTableA7::all();
    }

    /**
     * Calculate Efc
     *
     * @param Family $family
     * @param null $childId
     * @return array
     */
    public function getEfc(Family $family, $childId = null)
    {
        // transform the family data to input data
        $efcInputData = $this->transformFamilyToData($family, $childId);

        // get efc for each student
        foreach ($efcInputData as $key => $record) {
            $efcStudentData = $this->transformArrayToInput($record);
            $data['efcResultArray'][$key] = $this->calculateEfc($efcStudentData);
        }

        $efcResult = collect($data['efcResultArray']);

        // get the average
        $data['efc_fm_final'] = $efcResult->average('efc_final_fm');
        $data['efc_im_final'] = $efcResult->average('efc_final_im');
        $data['studentInCollege'] = $family->children()->where('is_in_college', 1)->get();

        return $data;
    }

    /**
     * Get EFC from scenario
     *
     * @param Scenario $scenario
     * @return mixed
     */
    public function getEfcFromScenario(Scenario $scenario)
    {
        // transform the family data to input data
        $efcInputData = $this->transformScenarioToData($scenario);

        $efcResult = $this->calculateEfc($this->transformArrayToInput($efcInputData));

        $data['efc_fm_final'] = $efcResult['efc_final_fm'];
        $data['efc_im_final'] = $efcResult['efc_final_im'];
        $data['efcResultArray'][$scenario->family_child_id] = $efcResult;

        return $data;
    }

    /**
     * Calculate EFC from the given data
     *
     * @param $data
     * @return array
     */
    private function calculateEfc($data)
    {
        /** Parent Calculations */
        // section 1 - PARENTS’ INCOME
        $output['parent_income'] = $this->parentIncome($data);
        // section 2 - ALLOWANCES AGAINST PARENTS’ INCOME
        $output['parent_income_allowance'] = $this->parentIncomeAllowance($data, $output);
        // section 3 - AVAILABLE INCOME
        $output['parent_available_income'] =
            $output['parent_income']['parent_total_income']
            - $output['parent_income_allowance']['parent_total_allowance'];
        // section 4 - PARENTS’ CONTRIBUTION FROM ASSETS FM
        $output['parent_contribution_assets_fm'] = $this->parentContributionAsset($data, "FM");
        $output['parent_contribution_assets_im'] = $this->parentContributionAsset($data, "IM");
        // section 5 - PARENTS’ CONTRIBUTION
        $output['parent_contribution_fm'] = $this->parentContribution($data, $output, "FM");
        $output['parent_contribution_im'] = $this->parentContribution($data, $output, "IM");
        /** End of Parent Calculations */

        /** Student Contribution */
        // section 1 - STUDENT’S INCOME IN 2014
        $output['student_income'] = $this->studentIncome($data);
        // section 2 -  ALLOWANCES AGAINST STUDENT INCOME
        $output['student_income_allowance_fm'] = $this->studentIncomeAllowance($data, $output, "FM");
        $output['student_income_allowance_im'] = $this->studentIncomeAllowance($data, $output, "IM");
        // section 3 -  STUDENT’S CONTRIBUTION FROM INCOME
        $output['student_contribution_income_fm'] = $this->studentContributionIncome($output, "FM");
        $output['student_contribution_income_im'] = $this->studentContributionIncome($output, "IM");
        // section 4 -  STUDENT’S CONTRIBUTION FROM ASSETS
        $output['student_contribution_assets_fm'] = $this->studentContributionAssets($data, "FM");
        $output['student_contribution_assets_im'] = $this->studentContributionAssets($data, "IM");
        /** End of Student Contribution  */

        /** Calculate EFC */
        $output['efc_calculation_fm'] = $this->calculateEfcFromData($output, "FM");
        $output['efc_calculation_im'] = $this->calculateEfcFromData($output, "IM");
        $output['efc_special_calculation_fm'] = $this->calculateEfcSpecial($data, $output, "FM");
        $output['efc_special_calculation_im'] = $this->calculateEfcSpecial($data, $output, "IM");
        $output['efc_final_fm'] = $output['efc_special_calculation_fm']['efc'];
        $output['efc_final_im'] = $output['efc_special_calculation_im']['efc'];
        /** End of Calculate EFC  */
        
        return $output;
    }

    /**
     * Get parent income
     *
     * @param $data
     * @return mixed
     */
    private function parentIncome($data)
    {
        // line 1
        $output['parent_adjusted_gross_income'] = $data['parent_dependent_income_agi'];
        // line 2
        $output['parent_1_earned_income_work'] = $data['parent_dependent_1_income_work'];
        $output['parent_2_earned_income_work'] = $data['parent_dependent_2_income_work'];
        // line 3
        if ($output['parent_adjusted_gross_income'] > 0) {
            $output['parent_taxable_income'] = $output['parent_adjusted_gross_income'];
        } else {
            $output['parent_taxable_income'] = $output['parent_1_earned_income_work'] + $output['parent_2_earned_income_work'];
        }
        // line 4
        $output['parent_untaxed_income_benefit'] = $data['parent_dependent_income_total_untaxed_benefit'];
        // line 5
        $output['parent_taxable_untaxed_income'] = $output['parent_taxable_income'] + $output['parent_untaxed_income_benefit'];
        // line 6
        $output['parent_additional_financial_income'] = $data['parent_dependent_income_additional_financial_info'];
        // line 7
        $output['parent_total_income'] = $output['parent_taxable_untaxed_income'] - $output['parent_additional_financial_income'];

        return $output;
    }

    /**
     * Get total allowance for the income
     *
     * @param $data
     * @param $input
     * @return mixed
     */
    private function parentIncomeAllowance($data, $input)
    {
        // line 8
        $output['parent_income_tax_paid'] = $data['parent_dependent_income_taxes_paid'];
        // line 9
        // state and other tax allowance (Table A1) If negative, enter zero.
        $table1 = $this->efcTableA1->firstWhere('state_id', $data['fafsa_parent_dependent_1_state_residence']);
        $output['parent_state'] = $table1['state_name'];
        if ($input['parent_income']['parent_total_income'] < 15000) {
            $output['parent_state_tax_allowance_percent'] = $table1['state_tax_allowance_14'];
            $output['parent_state_allowance'] = $table1['state_tax_allowance_14'] * $input['parent_income']['parent_total_income'];
        } else {
            $output['parent_state_tax_allowance_percent'] = $table1['state_tax_allowance_15'];
            $output['parent_state_allowance'] = $table1['state_tax_allowance_15'] * $input['parent_income']['parent_total_income'];
        }
        if ($output['parent_state_allowance'] < 0) {
            $output['parent_state_allowance'] = 0;
        }
        // line 10
        // parent 1 (father/mother/stepparent) Social Security tax allowance (Table A2)
        $parent1Ssn = $this->calculateSsnAllowance($data['parent_dependent_1_income_work']);
        $output['parent_1_income'] = $data['parent_dependent_1_income_work'];
        $output['parent_1_ssn_desc'] = $parent1Ssn['desc'];
        $output['parent_1_ssn_allowance'] = $parent1Ssn['value'];
        // line 11
        //Parent 2 (father/mother/stepparent) Social Security tax allowance (Table A2)
        $parent2Ssn = $this->calculateSsnAllowance($data['parent_dependent_2_income_work']);
        $output['parent_2_income'] = $data['parent_dependent_2_income_work'];
        $output['parent_2_ssn_desc'] = $parent2Ssn['desc'];
        $output['parent_2_ssn_allowance'] = $parent2Ssn['value'];
        // line 12
        // income protection allowance (Table A3)
        $table3 = $this->efcTableA3->where('num_household', $data['fafsa_parent_number_family_members'])
            ->where('num_student_college', $data['fafsa_parent_number_family_members_college'])
            ->first();

        $output['household_number_family_members'] = $data['fafsa_parent_number_family_members'];
        $output['household_number_family_members_college'] = $data['fafsa_parent_number_family_members_college'];
        $output['income_protection_allowance'] = $table3['value'];
        // line 13
        // employment expense allowance:
        // parent_employment_expense_allowance
        // two working parents (Parents’ Marital Status is “married” or “unmarried and both parents living together”):
        // 35% of the lesser of the earned incomes, or $4,000, whichever is less
        if (($data['parent_dependent_1_income_work'] > 0) && ($data['parent_dependent_2_income_work'] >0)) {
            $table35 = $this->efcTableA35->firstWhere('key', 'FMW');
            $output['parent_employment_expense_allowance_desc'] = $table35['description'];
            $output['parent_employment_expense_allowance_income'] = min($data['parent_dependent_1_income_work'], $data['parent_dependent_2_income_work']);
            $output['parent_employment_expense_allowance'] = min($table35['fixed'], $output['parent_employment_expense_allowance_income'] * $table35['percent']);
        } else {
            // one-parent families: 35% of earned income, or $4,000, whichever is less
            if ($data['fafsa_parent_1_marital_status'] == 'I') {
                $table35 = $this->efcTableA35->firstWhere('key', 'OPF');
                $output['parent_employment_expense_allowance_desc'] = $table35['description'];
                $output['parent_employment_expense_allowance_income'] = max($data['parent_dependent_1_income_work'], $data['parent_dependent_2_income_work']);
                $output['parent_employment_expense_allowance'] = min($table35['fixed'], $output['parent_employment_expense_allowance_income'] * $table35['percent']);
            }
            // two-parent families, one working parent: enter zero
            else {
                $table35 = $this->efcTableA35->firstWhere('key', 'FMONW');
                $output['parent_employment_expense_allowance_desc'] = $table35['description'];
                $output['parent_employment_expense_allowance_income'] = 0;
                $output['parent_employment_expense_allowance'] = 0;
            }
        }
        // line 14
        // TOTAL ALLOWANCES
        $output['parent_total_allowance'] = $output['parent_income_tax_paid']
            + $output['parent_state_allowance']
            + $output['parent_1_ssn_allowance']
            + $output['parent_2_ssn_allowance']
            + $output['income_protection_allowance']
            + $output['parent_employment_expense_allowance'];

        return $output;
    }

    /**
     * Calculate Social Security Tax Allowance
     *
     * @param $income
     * @return array
     */
    private function calculateSsnAllowance($income)
    {
        $output = [];

        $table2 = $this->efcTableA2->where('min_value', '<=', $income)
            ->where('max_value', '>=', $income)
            ->first();

        $output['desc'] = $table2['description'];

        if ($table2['exceed'] == "NA") {
            $output['value'] = ($income * $table2['percent']) + $table2['fixed'];
        } else {
            $output['value'] =
                (
                    ($income - $table2['exceed'])
                    * $table2['percent']
                ) + $table2['fixed'];
        }

        return $output;
    }

    /**
     * Get parent asset contribution towards EFC
     *
     * @param $data
     * @param $type
     * @return array
     */
    private function parentContributionAsset($data, $type)
    {
        // line 16
        $output['parent_cash_savings_checking'] = $data['parent_dependent_asset_cash_savings_checking'];
        // line 17
        $output['parent_net_worth_of_investments'] = $data['parent_dependent_education_529']
            + $data['parent_dependent_education_ira']
            + $data['parent_dependent_non_retirement_cod']
            + $data['parent_dependent_non_retirement_tbill']
            + $data['parent_dependent_non_retirement_money_fund']
            + $data['parent_dependent_non_retirement_mutual_fund']
            + $data['parent_dependent_non_retirement_stock']
            + $data['parent_dependent_non_retirement_bond']
            + $data['parent_dependent_non_retirement_trust_fund']
            + $data['parent_dependent_non_retirement_other_security']
            + $data['parent_dependent_non_retirement_other_investment']
            + $data['parent_dependent_asset_net_worth_real_estate'];
        if ($type == "IM") {
            $output['parent_net_worth_of_investments'] = $output['parent_net_worth_of_investments']
                + $data['parent_dependent_education_siblings_assets']
                + $data['parent_dependent_non_retirement_annuities']
                + $data['parent_dependent_asset_primary_net_worth_real_estate'];
        }
        // line 18
        $output['parent_net_worth_business'] = $data['parent_dependent_asset_net_worth_business'];
        $output['parent_net_worth_farm'] = $data['parent_dependent_asset_net_worth_farm'];
        $output['parent_bf_business_employee'] = $data['parent_bf_business_employee'];
        $output['parent_bf_farm_live'] = $data['parent_bf_farm_live'];
        // line 19
        $output['parent_adjusted_net_worth_business_farm'] = $this->calculateBusinessFarmValue($output, $type);
        // line 20
        $output['parent_net_worth'] = $output['parent_cash_savings_checking']
            + $output['parent_net_worth_of_investments']
            + $output['parent_adjusted_net_worth_business_farm'];
        // line 21 Education savings and asset protection allowance (Table A5)
        $output['age_of_parent_1'] = Carbon::parse($data['fafsa_parent_1_dob'])->age;
        $output['age_of_parent_2'] = Carbon::parse($data['fafsa_parent_2_dob'])->age;
        $output['max_age_parent'] = max($output['age_of_parent_1'], $output['age_of_parent_2']);
        $output['number_parents'] = $data['fafsa_parent_number'];
        // education savings and asset protection allowance (Table A5)
        $table5 = $this->efcTableA5->firstWhere('age_older_parent', $output['max_age_parent']);
        if ($output['number_parents'] == 1) {
            $output['esa_protection_allowance']  = $table5['one_parent_allowance'];
        } else {
            $output['esa_protection_allowance']  = $table5['two_parent_allowance'];
        }
        // line 22
        // discretionary net worth (line 20 minus line 21)
        $output['parent_discretionary_net_worth'] = $output['parent_net_worth'] - $output['esa_protection_allowance'];
        // line 23
        // asset conversion rate
        $cfgCal1 = $this->cfgCalculator->firstWhere('key', 'EFC_A_PCFA_ACR_FM');
        $output['asset_conversion_rate'] = $cfgCal1['value'];
        // line 24
        // CONTRIBUTION FROM ASSETS If negative, enter zero.
        $output['parent_asset_contribution'] = $output['parent_discretionary_net_worth'] * $output['asset_conversion_rate'];
        if ($output['parent_asset_contribution'] < 0) {
            $output['parent_asset_contribution'] = 0;
        }

        return $output;
    }

    /**
     * Calculate Business Farm Value
     *
     * @param  $data
     * @param string $type
     * @return float|int
     */
    private function calculateBusinessFarmValue($data, $type="FM")
    {
        $parentNetWorthBusiness = $data['parent_net_worth_business'];
        $parentNetWorthFarm = $data['parent_net_worth_farm'];
        if ($type == "FM") {
            if ($data['parent_bf_business_employee']=="Yes") {
                $parentNetWorthBusiness = 0;
            }
            if ($data['parent_bf_farm_live']=="Yes") {
                $parentNetWorthFarm = 0;
            }
        }
        // get business value
        $table4 = $this->efcTableA4->where('min_value', '<=', $parentNetWorthBusiness)
            ->where('max_value', '>=', $parentNetWorthBusiness)
            ->first();
        if ($table4['exceed'] == "NA") {
            $business = ($parentNetWorthBusiness  * $table4['percent']) + $table4['fixed'];
        } else {
            $business =
                (
                    ($parentNetWorthBusiness  - $table4['exceed'])
                    * $table4['percent']
                ) + $table4['fixed'];
        }
        // get farm value
        $table4 = $this->efcTableA4->where('min_value', '<=', $parentNetWorthFarm)
            ->where('max_value', '>=', $parentNetWorthFarm)
            ->first();

        if ($table4['exceed'] == "NA") {
            $farm = ($parentNetWorthFarm * $table4['percent']) + $table4['fixed'];
        } else {
            $farm =
                (
                    ($parentNetWorthFarm - $table4['exceed'])
                    * $table4['percent']
                ) + $table4['fixed'];
        }

        $value = $business + $farm;

        return $value;
    }

    /**
     * Parent contribution
     *
     * @param $data
     * @param $input
     * @param $type
     * @return array
     */
    private function parentContribution($data, $input, $type)
    {
        // AVAILABLE INCOME (AI) (from line 15)
        $output['parent_available_income'] = $input['parent_available_income'];
        // CONTRIBUTION FROM ASSETS (from line 24) +
        $output['parent_asset_contribution'] = $input['parent_contribution_assets_fm']['parent_asset_contribution'];
        if ($type == "IM") {
            $output['parent_asset_contribution'] = $input['parent_contribution_assets_im']['parent_asset_contribution'];
        }
        // line 25
        // Adjusted Available Income (AAI) May be a negative number.
        $output['parent_adjusted_available_income'] = $output['parent_available_income'] + $output['parent_asset_contribution'];
        // line 26
        // Total parents’ contribution from AAI (Calculate using Table A6.) If negative, enter zero.
        $table6 = $this->efcTableA6->where('min_value', '<=', $output['parent_adjusted_available_income'])
            ->where('max_value', '>=', $output['parent_adjusted_available_income'])
            ->first();
        $output['parent_total_contribution_aai_desc'] = $table6['description'];
        if ($table6['exceed'] == "NA") {
            $output['parent_total_contribution_aai'] = ($output['parent_adjusted_available_income'] * $table6['percent']) + $table6['fixed'];
        } else {
            $output['parent_total_contribution_aai'] =
                (
                    ($output['parent_adjusted_available_income'] - $table6['exceed'])
                    * $table6['percent']
                ) + $table6['fixed'];
        }
        if ($output['parent_total_contribution_aai'] < 0) {
            $output['parent_total_contribution_aai'] = 0;
        }
        // line 27
        // Number in college in 2015–2016 (Exclude parents) (FAFSA/SAR #74)
        $output['number_family_members_college'] = $data['fafsa_parent_number_family_members_college'];
        // line 28
        // PARENTS’ CONTRIBUTION (standard contribution for nine-month enrollment)*** If negative, enter zero.
        $output['parent_contribution'] = $output['parent_total_contribution_aai']/$output['number_family_members_college'];
        if ($output['parent_contribution'] < 0) {
            $output['parent_contribution'] = 0;
        }

        return $output;
    }

    /**
     * Get student income
     *
     * @param $data
     * @return array
     */
    private function studentIncome($data)
    {
        // line 29
        // Adjusted Gross Income (FAFSA/SAR #36) If negative, enter zero.
        $output['student_agi'] = $data['student_dependent_income_agi'];
        if ($output['student_agi'] < 0) {
            $output['student_agi'] = 0;
        }
        // line 30
        // Income earned from work (FAFSA/SAR #39)
        $output['student_income_work'] = $data['student_dependent_income_work'];
        // line 31
        // Taxable Income
        if ($output['student_agi'] < 1) {
            $output['student_taxable_income'] = $output['student_income_work'];
        } else {
            $output['student_taxable_income'] = $output['student_agi'];
        }
        // line 32
        // Total untaxed income and benefits
        $output['student_untaxed_benefits'] = $data['student_dependent_income_total_untaxed_benefit'];
        // line 33
        // Taxable and untaxed income
        $output['student_taxable_untaxed_income'] = $output['student_taxable_income'] + $output['student_untaxed_benefits'];
        // line 34
        // Total additional financial information
        $output['student_additional_financial'] = $data['student_dependent_income_additional_financial_info'];
        // line 35
        // TOTAL INCOME line 33 minus line 34) = May be a negative number.
        $output['student_total_income'] = $output['student_taxable_untaxed_income'] - $output['student_additional_financial'];

        return $output;
    }

    /**
     * Calculate student income allowance
     *
     * @param $data
     * @param $input
     * @param $type
     * @return array
     */
    private function studentIncomeAllowance($data, $input, $type)
    {
        // line 36
        // 2014 U.S. income tax paid (FAFSA/SAR #37)
        $output['student_tax_paid'] = $data['student_dependent_income_taxes_paid'];
        if ($output['student_tax_paid'] < 0) {
            $output['student_tax_paid'] = 0;
        }
        // line 37
        // State and other tax allowance (Table A7) If negative, enter zero
        $table7 = $this->efcTableA7->firstWhere('state_id', $data['fafsa_student_dependent_state_residence']);
        $output['student_state'] = $table7['state_name'];
        $output['student_state_tax_allowance_percent'] = $table7['percent'];
        $output['student_state_allowance'] = $table7['percent'] * $input['student_income']['student_total_income'];
        if ($output['student_state_allowance'] < 0) {
            $output['student_state_allowance'] = 0;
        }
        // line 38
        //Social Security tax allowance (Table A2)
        $studentSsn = $this->calculateSsnAllowance($input['student_income']['student_income_work']);
        $output['student_income_work'] = $input['student_income']['student_income_work'];
        $output['student_ssn_desc'] = $studentSsn['desc'];
        $output['student_ssn_allowance'] = $studentSsn['value'];
        // line 39
        // Income protection allowance
        // EFA_A_AGSI_IPA
        $cfgCal1 = $this->cfgCalculator->firstWhere('key', 'EFA_A_AGSI_IPA');
        $output['student_income_protection_allowance_desc'] = $cfgCal1['description'];
        $output['student_income_protection_allowance'] = $cfgCal1['value'];
        // line 40
        // Allowance for parents’ negative Adjusted Available Income (If line 25 is negative, enter line 25 as a positive number in line 40.
        // If line 25 is zero or positive, enter zero in line 40.)
        $output['parent_adjusted_available_income'] = $input['parent_contribution_fm']['parent_adjusted_available_income'];
        if ($type == "IM") {
            $output['parent_adjusted_available_income'] = $input['parent_contribution_im']['parent_adjusted_available_income'];
        }
        if ($output['parent_adjusted_available_income'] < 0) {
            $output['parent_negative_adjusted_available_income'] = $output['parent_adjusted_available_income'] * -1;
        } else {
            $output['parent_negative_adjusted_available_income'] = 0;
        }
        // line 41
        // TOTAL ALLOWANCES
        $output['student_total_allowances'] = $output['student_tax_paid']
            + $output['student_state_allowance']
            + $output['student_ssn_allowance']
            + $output['student_income_protection_allowance']
            + $output['parent_negative_adjusted_available_income'];

        return $output;
    }

    /**
     * Student Income contribution
     *    * @param $input
     * @param $type
     * @return array
     */
    private function studentContributionIncome($input, $type)
    {
        $output['student_total_income'] = $input['student_income']['student_total_income'];
        $output['student_total_allowances'] = $input['student_income_allowance_fm']['student_total_allowances'];
        if ($type == "IM") {
            $output['student_total_allowances'] = $input['student_income_allowance_im']['student_total_allowances'];
        }
        // line 42
        // Available income (AI) =
        $output['student_available_income'] = $output['student_total_income'] - $output['student_total_allowances'];
        // line 43
        // Assessment of AI
        $cfgCal = $this->cfgCalculator->firstWhere('key', 'EFA_A_SCFI_AI_FM');
        if ($type == "IM") {
            $cfgCal = $this->cfgCalculator->firstWhere('key', 'EFA_A_SCFI_AI_IM');
        }
        $output['student_assessment_of_ai_percent'] = $cfgCal['value'];
        // line 44
        // STUDENT’S CONTRIBUTION FROM AI = If negative, enter zero.
        $output['student_contribution_from_ai'] = $output['student_available_income'] * $output['student_assessment_of_ai_percent'];
        if ($output['student_contribution_from_ai'] < 0) {
            $output['student_contribution_from_ai'] = 0;
        }
        // line 44 - IM
        // if(line 44 < EFA_A_SCFM_MIN_IM) then EFA_A_SCFM_MIN_IM
        if ($type == "IM") {
            $cfgCal = $this->cfgCalculator->firstWhere('key', 'EFA_A_SCFM_MIN_IM');
            $output['student_contribution_from_ai_min'] = $cfgCal['value'];
            if ($output['student_contribution_from_ai'] < $cfgCal['value']) {
                $output['student_contribution_from_ai'] = $cfgCal['value'];
            }
        }

        return $output;
    }

    /**
     * Student contribution assets
     *
     * @param $data
     * @param $type
     * @return array
     */
    private function studentContributionAssets($data, $type)
    {
        // line 45
        // Cash, savings & checking (FAFSA/SAR #41)
        $output['student_cash_savings_checking'] = $data['student_dependent_asset_cash_savings_checking'];
        // line 46
        // Net worth of investments*
        $output['student_net_worth_of_investments'] = $data['student_dependent_asset_certificate_deposit']
            + $data['student_dependent_asset_t_bills']
            + $data['student_dependent_asset_money_market_fund']
            + $data['student_dependent_asset_mutual_fund']
            + $data['student_dependent_asset_stocks']
            + $data['student_dependent_asset_bonds']
            + $data['student_dependent_asset_ugma_utma_trust']
            + $data['student_dependent_asset_trust_fund']
            + $data['student_dependent_asset_other_investment'];
        // line 47
        // Net worth of business and/or investment farm
        $output['student_net_worth_of_business_farm'] = 0;
        // line 48
        // Net worth (sum of lines 45 through 47) =
        $output['student_net_worth'] = $output['student_cash_savings_checking']
            + $output['student_net_worth_of_investments']
            + $output['student_net_worth_of_business_farm'];
        // line 49
        // Assessment rate × .20
        $cfg_cal = $this->cfgCalculator->firstWhere('key', 'EFA_A_SCFA_AR_FM');
        if ($type == "IM") {
            $cfg_cal = $this->cfgCalculator->firstWhere('key', 'EFA_A_SCFA_AR_IM');
        }
        $output['student_assessment_rate'] = $cfg_cal['value'];
        // line 50
        // STUDENT’S CONTRIBUTION FROM ASSETS =
        $output['student_contribution_from_assets'] = $output['student_net_worth'] * $output['student_assessment_rate'];

        return $output;
    }

    /**
     * Apply special conditions to Efc
     *
     * @param $data
     * @param $input
     * @param $type
     * @return array
     */
    private function calculateEfcSpecial($data, $input, $type)
    {
        $output['efc'] = $input['efc_calculation_fm']['efc'];
        if ($type=="IM") {
            $output['efc'] = $input['efc_calculation_im']['efc'];
        }

        $output['fafsa_parent_benefits'] = $data['fafsa_parent_benefits'];
        $output['fafsa_parent_filed_1040_aez'] = $data['fafsa_parent_filed_1040_aez'];
        $output['fafsa_parent_filed_1040'] = $data['fafsa_parent_filed_1040'];
        $output['fafsa_parent_no_income_tax'] = $data['fafsa_parent_no_income_tax'];
        $output['fafsa_parent_dislocated_worker'] = $data['fafsa_parent_dislocated_worker'];

        $firstConditionMatch = 0;
        if ($output['fafsa_parent_benefits'] == "Yes"
            or $output['fafsa_parent_filed_1040_aez'] == "Yes"
            or $output['fafsa_parent_filed_1040'] == "Yes"
            or $output['fafsa_parent_no_income_tax'] == "Yes"
            or $output['fafsa_parent_dislocated_worker'] == "Yes") {
            $firstConditionMatch = 1;
        }

        // Let the AGI less than values from CFG
        $cfgCalSimple = $this->cfgCalculator->firstWhere('key', 'EFC_A_B_C_SEFC_AGI');
        $cfgCalZero = $this->cfgCalculator->firstWhere('key', 'EFC_A_C_ZEFQ_AGI');

        // Will the students qualify for the simplified EFC formulas?
        if ($firstConditionMatch == 1 and $input['parent_income']['parent_adjusted_gross_income'] <= $cfgCalSimple['value']) {
            // Change in PARENTS’ CONTRIBUTION
            $output['parent_available_income'] = $input['parent_available_income'];
            // CONTRIBUTION FROM ASSETS (from line 24) +
            $output['parent_asset_contribution'] = 0;
            // Line 25
            // Adjusted Available Income (AAI) May be a negative number.
            $output['parent_adjusted_available_income'] = $output['parent_available_income'] + $output['parent_asset_contribution'];
            // Line 26
            // Total parents’ contribution from AAI (Calculate using Table A6.) If negative, enter zero.
            $table6 = $this->efcTableA6->where('min_value', '<=', $output['parent_adjusted_available_income'])
                ->where('max_value', '>=', $output['parent_adjusted_available_income'])
                ->first();
            $output['parent_total_contribution_aai_desc'] = $table6['description'];
            if ($table6['exceed'] == "NA") {
                $output['parent_total_contribution_aai'] = ($output['parent_adjusted_available_income'] * $table6['percent']) + $table6['fixed'];
            } else {
                $output['parent_total_contribution_aai'] =
                    (
                        ($output['parent_adjusted_available_income'] - $table6['exceed'])
                        * $table6['percent']
                    ) + $table6['fixed'];
            }

            if ($output['parent_total_contribution_aai'] < 0) {
                $output['parent_total_contribution_aai'] = 0;
            }
            // line 27
            // Number in college in 2015–2016 (Exclude parents) (FAFSA/SAR #74)
            $output['number_family_members_college'] = $data['fafsa_parent_number_family_members_college'];
            // line 28
            // PARENTS’ CONTRIBUTION (standard contribution for nine-month enrollment)*** If negative, enter zero.
            $output['parent_contribution'] = $output['parent_total_contribution_aai']/$output['number_family_members_college'];
            if ($output['parent_contribution'] < 0) {
                $output['parent_contribution'] = 0;
            }

            // Final EFC
            $output['efc'] = $output['parent_contribution'] + $input['student_contribution_income_fm']['student_contribution_from_ai'];
            if ($type == "IM") {
                $output['efc'] = $output['parent_contribution'] + $input['student_contribution_income_im']['student_contribution_from_ai'];
            }
        }

        // Will the  students qualify for an automatic zero EFC calculation?
        if ($firstConditionMatch == 1 and $input['parent_income']['parent_adjusted_gross_income'] <= $cfgCalZero['value']) {
            $output['efc'] = 0;
        }

        return $output;
    }

    /**
     * Get the efc value
     *
     * @param $input
     * @param $type
     * @return array
     */
    private function calculateEfcFromData($input, $type)
    {
        // line 101
        // PARENTS’ CONTRIBUTION
        $output['parent_contribution'] = $input['parent_contribution_fm']['parent_contribution'];
        if ($type=="IM") {
            $output['parent_contribution'] = $input['parent_contribution_im']['parent_contribution'];
        }
        // line 102
        // STUDENT’S CONTRIBUTION FROM AI
        $output['student_contribution_from_ai'] = $input['student_contribution_income_fm']['student_contribution_from_ai'];
        if ($type == "IM") {
            $output['student_contribution_from_ai'] = $input['student_contribution_income_im']['student_contribution_from_ai'];
        }
        // line 103
        // STUDENT’S CONTRIBUTION FROM ASSETS
        $output['student_contribution_from_assets'] = $input['student_contribution_assets_fm']['student_contribution_from_assets'];
        if ($type == "IM") {
            $output['student_contribution_from_assets'] = $input['student_contribution_assets_im']['student_contribution_from_assets'];
        }
        // Line 51
        // EXPECTED FAMILY CONTRIBUTION (standard contribution for nine-month enrollment)** If negative, enter zero.
        $output['efc'] = $output['parent_contribution']
            + $output['student_contribution_from_ai']
            + $output['student_contribution_from_assets'];
        if ($output['efc'] < 0) {
            $output['efc'] = 0;
        }

        return $output;
    }

    /**
     * Transform family db to input
     *
     * @param Family $family
     * @param null $childId
     * @return array
     */
    private function transformFamilyToData(Family $family, $childId = null)
    {
        $parent = $family->parents[0];
        $parentTwo = isset($family->parents[1]) ? $family->parents[1] : null;

        // if child is passed then we only need that child
        if ($childId) {
            $children = $family->children()->where('id', $childId)->get();
        } else {
            // get all the student going to college
            $children = $family->children()->where('is_in_college', 1)->get();
        }

        $childrenInCollegeCount = $family->children()->where('is_in_college', 1)->count();

        // Parents
        $data['parent_dependent_income_agi'] = $family->parent_income_adjusted_gross_dollar_total;
        $data['parent_dependent_1_income_work'] = $parent->income_work_dollar;
        $data['parent_dependent_2_income_work'] = optional($parentTwo)->income_work_dollar;
        $data['parent_dependent_income_taxes_paid'] = $family->parent_income_tax_dollar_total;
        $data['parent_dependent_income_additional_financial_info'] = $family->parent_income_additional_dollar_total;
        $data['parent_dependent_income_total_untaxed_benefit'] = $family->parent_income_untaxed_dollar_total;

        $data['parent_dependent_education_siblings_assets'] = $family->parent_asset_education_student_sibling_dollar_total;
        $data['parent_dependent_education_529'] = $family->parent_asset_education_five_two_nine_dollar_total;
        $data['parent_dependent_education_ira'] = $family->parent_asset_education_coverdell_dollar_total;

        $data['parent_dependent_asset_cash_savings_checking'] = $family->parent_asset_non_retirement_checking_savings_dollar_total;
        $data['parent_dependent_non_retirement_cod'] = $family->parent_asset_non_retirement_certificate_of_deposit_dollar_total;
        $data['parent_dependent_non_retirement_tbill'] = $family->parent_asset_non_retirement_t_bills_dollar_total;
        $data['parent_dependent_non_retirement_money_fund'] = $family->parent_asset_non_retirement_money_market_dollar_total;
        $data['parent_dependent_non_retirement_mutual_fund'] = $family->parent_asset_non_retirement_mutual_dollar_total;
        $data['parent_dependent_non_retirement_stock'] = $family->parent_asset_non_retirement_stock_dollar_total;
        $data['parent_dependent_non_retirement_bond'] = $family->parent_asset_non_retirement_bond_dollar_total;
        $data['parent_dependent_non_retirement_trust_fund'] = $family->parent_asset_non_retirement_trust_dollar_total;
        $data['parent_dependent_non_retirement_other_security'] = $family->parent_asset_non_retirement_other_securities_dollar_total;
        $data['parent_dependent_non_retirement_other_investment'] = $family->parent_asset_non_retirement_other_investments_dollar_total;
        $data['parent_dependent_non_retirement_annuities'] = $family->parent_asset_non_retirement_annuities_dollar_total;

        $data['parent_dependent_retirement_db_account'] = $family->parent_asset_retirement_defined_benefit_dollar_total;
        $data['parent_dependent_retirement_401k'] = $family->parent_asset_retirement_four_o_one_dollar_total;
        $data['parent_dependent_retirement_ira'] = $family->parent_asset_retirement_ira_dollar_total;
        $data['parent_dependent_retirement_roth_ira'] = $family->parent_asset_retirement_roth_ira_dollar_total;
        $data['parent_dependent_retirement_other_plan'] = $family->parent_asset_retirement_other_dollar_total;

        $data['parent_dependent_insurance_life_insurance_cash_value'] = $parent->insurance_permanent_cash_value_dollar +
            optional($parentTwo)->insurance_permanent_cash_value_dollar;

        $data['parent_dependent_asset_primary_net_worth_real_estate'] = $family->parent_asset_real_estate_primary_net_worth_dollar;
        $data['parent_dependent_asset_net_worth_real_estate'] = $family->parent_asset_real_estate_other_net_worth_dollar;
        $data['parent_dependent_asset_net_worth_business'] = $family->parent_business_value_dollar;
        $data['parent_dependent_asset_net_worth_farm'] = $family->parent_farm_value_dollar;

        // fafsa variables
        $fafsaParentNumberFamilyMembers = $family->parents()->count()
            + $family->children()->count()
            + $family->other_family_members_num;
        $fafsaParentNumberFamilyMembers = $fafsaParentNumberFamilyMembers <= 6 ? $fafsaParentNumberFamilyMembers : 6;
        $fafsaParentNumberFamilyMembersCollege = $childrenInCollegeCount >= 1 ? $childrenInCollegeCount : 1;

        $data['fafsa_student_dependent_state_residence'] = $parent->state_id;
        $data['fafsa_parent_dependent_1_state_residence'] = $parent->state_id;
        $data['fafsa_parent_dependent_2_state_residence'] = $parent->state_id;

        $data['fafsa_parent_number_family_members'] = $fafsaParentNumberFamilyMembers;
        $data['fafsa_parent_number_family_members_college'] = $fafsaParentNumberFamilyMembersCollege;
        $data['fafsa_parent_number'] = $family->parents->count() > 2 ? 2 : $family->parents->count();
        $data['fafsa_parent_1_marital_status'] = isset($parentTwo) ? "M" : "D";
        $data['fafsa_parent_1_dob'] = optional($parent->birth_date)->toDateTimeString();
        $data['fafsa_parent_2_dob'] = $parentTwo ?
            optional($parentTwo->birth_date)->toDateTimeString()
            : null;

        $records = [];

        // student variables
        foreach ($children as $child) {
            $data['student_dependent_income_agi'] = $child->income_adjusted_gross_dollar;
            $data['student_dependent_income_work'] = $child->income_work_dollar;
            $data['student_dependent_income_taxes_paid'] = $child->income_tax_dollar;

            $data['student_dependent_income_additional_financial_info'] = $child->income_additional_dollar_total;
            $data['student_dependent_income_total_untaxed_benefit'] = $child->income_untaxed_dollar_total;

            $data['student_dependent_asset_cash_savings_checking'] = $child->asset_non_retirement_checking_savings_dollar;
            $data['student_dependent_asset_ugma_utma_trust'] = $child->asset_non_retirement_ugma_utma_dollar;
            $data['student_dependent_asset_certificate_deposit'] = $child->asset_non_retirement_certificate_of_deposit_dollar;
            $data['student_dependent_asset_t_bills'] = $child->asset_non_retirement_t_bills_dollar;
            $data['student_dependent_asset_money_market_fund'] = $child->asset_non_retirement_money_market_dollar;
            $data['student_dependent_asset_mutual_fund'] = $child->asset_non_retirement_mutual_dollar;
            $data['student_dependent_asset_stocks'] = $child->asset_non_retirement_stock_dollar;
            $data['student_dependent_asset_bonds'] = $child->asset_non_retirement_bond_dollar;
            $data['student_dependent_asset_trust_fund'] = $child->asset_non_retirement_trust_dollar;
            $data['student_dependent_asset_other_investment'] = $child->asset_non_retirement_other_investments_dollar;

            $records[$child->id] = $data;
        }

        return $records;
    }

    /**
     * Transform scenario to EFC data
     *
     * @param Scenario $scenario
     * @return mixed
     */
    public function transformScenarioToData(Scenario $scenario)
    {
        $family = $scenario->family;
        $parent = $family->parents[0];
        $parentTwo = isset($family->parents[1]) ? $family->parents[1] : null;
        // get all the student going to college
        $children = FamilyChild::where('family_id', $family->id)
            ->where('is_in_college', 1)
            ->get();

        // Parents
        $data['parent_dependent_income_agi'] = $scenario->parent_income_adjusted_gross_dollar_total;
        $data['parent_dependent_1_income_work'] = $scenario->parent_1_income_work_dollar;
        $data['parent_dependent_2_income_work'] = $scenario->parent_2_income_work_dollar;
        $data['parent_dependent_income_taxes_paid'] = $scenario->parent_income_tax_dollar_total;
        $data['parent_dependent_income_additional_financial_info'] = $scenario->parent_income_additional_dollar_total;
        $data['parent_dependent_income_total_untaxed_benefit'] = $scenario->parent_income_untaxed_dollar_total;

        $data['parent_dependent_education_siblings_assets'] = $scenario->parent_asset_education_student_sibling_dollar_total;
        $data['parent_dependent_education_529'] = $scenario->parent_asset_education_five_two_nine_dollar_total;
        $data['parent_dependent_education_ira'] = $scenario->parent_asset_education_coverdell_dollar_total;

        $data['parent_dependent_asset_cash_savings_checking'] = $scenario->parent_asset_non_retirement_checking_savings_dollar_total;
        $data['parent_dependent_non_retirement_cod'] = $scenario->parent_asset_non_retirement_certificate_of_deposit_dollar_total;
        $data['parent_dependent_non_retirement_tbill'] = $scenario->parent_asset_non_retirement_t_bills_dollar_total;
        $data['parent_dependent_non_retirement_money_fund'] = $scenario->parent_asset_non_retirement_money_market_dollar_total;
        $data['parent_dependent_non_retirement_mutual_fund'] = $scenario->parent_asset_non_retirement_mutual_dollar_total;
        $data['parent_dependent_non_retirement_stock'] = $scenario->parent_asset_non_retirement_stock_dollar_total;
        $data['parent_dependent_non_retirement_bond'] = $scenario->parent_asset_non_retirement_bond_dollar_total;
        $data['parent_dependent_non_retirement_trust_fund'] = $scenario->parent_asset_non_retirement_trust_dollar_total;
        $data['parent_dependent_non_retirement_other_security'] = $scenario->parent_asset_non_retirement_other_securities_dollar_total;
        $data['parent_dependent_non_retirement_other_investment'] = $scenario->parent_asset_non_retirement_other_investments_dollar_total;
        $data['parent_dependent_non_retirement_annuities'] = $scenario->parent_asset_non_retirement_annuities_dollar_total;

        $data['parent_dependent_retirement_db_account'] = $scenario->parent_asset_retirement_defined_benefit_dollar_total;
        $data['parent_dependent_retirement_401k'] = $scenario->parent_asset_retirement_four_o_one_dollar_total;
        $data['parent_dependent_retirement_ira'] = $scenario->parent_asset_retirement_ira_dollar_total;
        $data['parent_dependent_retirement_roth_ira'] = $scenario->parent_asset_retirement_roth_ira_dollar_total;
        $data['parent_dependent_retirement_other_plan'] = $scenario->parent_asset_retirement_other_dollar_total;

        $data['parent_dependent_insurance_life_insurance_cash_value'] = $scenario->parent_insurance_permanent_cash_value_dollar_total;

        $data['parent_dependent_asset_primary_net_worth_real_estate'] = $scenario->parent_asset_real_estate_primary_net_worth_dollar;
        $data['parent_dependent_asset_net_worth_real_estate'] = $scenario->parent_asset_real_estate_other_net_worth_dollar;
        $data['parent_dependent_asset_net_worth_business'] = $scenario->parent_business_net_worth_dollar;
        $data['parent_dependent_asset_net_worth_farm'] = $scenario->parent_farm_net_worth_dollar;

        // fafsa variables
        $fafsaParentNumberFamilyMembers = $family->parents()->count()
            + $family->children()->count()
            + $family->other_family_members_num;
        $fafsaParentNumberFamilyMembers = $fafsaParentNumberFamilyMembers <= 6 ? $fafsaParentNumberFamilyMembers : 6;
        $fafsaParentNumberFamilyMembersCollege = $children->count() >= 1 ? $children->count() : 1;

        $data['fafsa_student_dependent_state_residence'] = $parent->state_id;
        $data['fafsa_parent_dependent_1_state_residence'] = $parent->state_id;
        $data['fafsa_parent_dependent_2_state_residence'] = $parent->state_id;

        $data['fafsa_parent_number_family_members'] = $fafsaParentNumberFamilyMembers;
        $data['fafsa_parent_number_family_members_college'] = $fafsaParentNumberFamilyMembersCollege;
        $data['fafsa_parent_number'] = $family->parents->count() > 2 ? 2 : $family->parents->count();
        $data['fafsa_parent_1_marital_status'] = isset($parentTwo) ? "M" : "D";
        $data['fafsa_parent_1_dob'] = optional($parent->birth_date)->toDateTimeString();
        $data['fafsa_parent_2_dob'] = $parentTwo ?
            optional($parentTwo->birth_date)->toDateTimeString()
            : null;

        $data['student_dependent_income_agi'] = $scenario->student_income_adjusted_gross_dollar;
        $data['student_dependent_income_work'] = $scenario->student_income_work_dollar;
        $data['student_dependent_income_taxes_paid'] = $scenario->student_income_tax_dollar;

        $data['student_dependent_income_additional_financial_info'] = $scenario->student_income_additional_dollar_total;
        $data['student_dependent_income_total_untaxed_benefit'] = $scenario->student_income_untaxed_dollar_total;

        $data['student_dependent_asset_cash_savings_checking'] = $scenario->student_asset_non_retirement_checking_savings_dollar;
        $data['student_dependent_asset_ugma_utma_trust'] = $scenario->student_asset_non_retirement_ugma_utma_dollar;
        $data['student_dependent_asset_certificate_deposit'] = $scenario->student_asset_non_retirement_certificate_of_deposit_dollar;
        $data['student_dependent_asset_t_bills'] = $scenario->student_asset_non_retirement_t_bills_dollar;
        $data['student_dependent_asset_money_market_fund'] = $scenario->student_asset_non_retirement_money_market_dollar;
        $data['student_dependent_asset_mutual_fund'] = $scenario->student_asset_non_retirement_mutual_dollar;
        $data['student_dependent_asset_stocks'] = $scenario->student_asset_non_retirement_stock_dollar;
        $data['student_dependent_asset_bonds'] = $scenario->student_asset_non_retirement_bond_dollar;
        $data['student_dependent_asset_trust_fund'] = $scenario->student_asset_non_retirement_trust_dollar;
        $data['student_dependent_asset_other_investment'] = $scenario->student_asset_non_retirement_other_investments_dollar;

        return $data;
    }

    /**
     * Transform array input data to efc format
     * @note - this will be used in future for API
     *
     * @param $efcInputData
     * @return array
     */
    private function transformArrayToInput($efcInputData)
    {
        // student variables
        $data['student_dependent_income_agi'] = isset($efcInputData['student_dependent_income_agi'])
            ? $efcInputData['student_dependent_income_agi']
            : 0;
        $data['student_dependent_income_work'] = isset($efcInputData['student_dependent_income_work'])
            ? $efcInputData['student_dependent_income_work']
            : 0;
        $data['student_dependent_income_additional_financial_info'] = isset($efcInputData['student_dependent_income_additional_financial_info'])
            ? $efcInputData['student_dependent_income_additional_financial_info']
            : 0;
        $data['student_dependent_income_total_untaxed_benefit'] = isset($efcInputData['student_dependent_income_total_untaxed_benefit'])
            ? $efcInputData['student_dependent_income_total_untaxed_benefit']
            : 0;
        $data['student_dependent_income_taxes_paid'] = isset($efcInputData['student_dependent_income_taxes_paid'])
            ? $efcInputData['student_dependent_income_taxes_paid']
            : 0;
        $data['student_dependent_asset_cash_savings_checking'] = isset($efcInputData['student_dependent_asset_cash_savings_checking'])
            ? $efcInputData['student_dependent_asset_cash_savings_checking']
            : 0;
        $data['student_dependent_asset_ugma_utma_trust'] = isset($efcInputData['student_dependent_asset_ugma_utma_trust'])
            ? $efcInputData['student_dependent_asset_ugma_utma_trust']
            : 0;
        $data['student_dependent_asset_certificate_deposit'] = isset($efcInputData['student_dependent_asset_certificate_deposit'])
            ? $efcInputData['student_dependent_asset_certificate_deposit']
            : 0;
        $data['student_dependent_asset_t_bills'] = isset($efcInputData['student_dependent_asset_t_bills'])
            ? $efcInputData['student_dependent_asset_t_bills']
            : 0;
        $data['student_dependent_asset_money_market_fund'] = isset($efcInputData['student_dependent_asset_money_market_fund'])
            ? $efcInputData['student_dependent_asset_money_market_fund']
            : 0;
        $data['student_dependent_asset_mutual_fund'] = isset($efcInputData['student_dependent_asset_mutual_fund'])
            ? $efcInputData['student_dependent_asset_mutual_fund']
            : 0;
        $data['student_dependent_asset_stocks'] = isset($efcInputData['student_dependent_asset_stocks'])
            ? $efcInputData['student_dependent_asset_stocks']
            : 0;
        $data['student_dependent_asset_bonds'] = isset($efcInputData['student_dependent_asset_bonds'])
            ? $efcInputData['student_dependent_asset_bonds']
            : 0;
        $data['student_dependent_asset_trust_fund'] = isset($efcInputData['student_dependent_asset_trust_fund'])
            ? $efcInputData['student_dependent_asset_trust_fund']
            : 0;
        $data['student_dependent_asset_other_investment'] = isset($efcInputData['student_dependent_asset_other_investment'])
            ? $efcInputData['student_dependent_asset_other_investment']
            : 0;

        // parent variables
        $data['parent_dependent_income_agi'] = isset($efcInputData['parent_dependent_income_agi'])
            ? $efcInputData['parent_dependent_income_agi']
            : 0;
        $data['parent_dependent_1_income_work'] = isset($efcInputData['parent_dependent_1_income_work'])
            ? $efcInputData['parent_dependent_1_income_work']
            : 0;
        $data['parent_dependent_2_income_work'] = isset($efcInputData['parent_dependent_2_income_work'])
            ? $efcInputData['parent_dependent_2_income_work']
            : 0;
        $data['parent_dependent_income_total_untaxed_benefit'] = isset($efcInputData['parent_dependent_income_total_untaxed_benefit'])
            ? $efcInputData['parent_dependent_income_total_untaxed_benefit']
            : 0;
        $data['parent_dependent_income_additional_financial_info'] = isset($efcInputData['parent_dependent_income_additional_financial_info'])
            ? $efcInputData['parent_dependent_income_additional_financial_info']
            : 0;
        $data['parent_dependent_income_taxes_paid'] = isset($efcInputData['parent_dependent_income_taxes_paid'])
            ? $efcInputData['parent_dependent_income_taxes_paid']
            : 0;
        $data['parent_dependent_asset_cash_savings_checking'] = isset($efcInputData['parent_dependent_asset_cash_savings_checking'])
            ? $efcInputData['parent_dependent_asset_cash_savings_checking']
            : 0;
        $data['parent_dependent_education_siblings_assets'] = isset($efcInputData['parent_dependent_education_siblings_assets'])
            ? $efcInputData['parent_dependent_education_siblings_assets']
            : 0;
        $data['parent_dependent_education_529'] = isset($efcInputData['parent_dependent_education_529'])
            ? $efcInputData['parent_dependent_education_529']
            : 0;
        $data['parent_dependent_education_ira'] = isset($efcInputData['parent_dependent_education_ira'])
            ? $efcInputData['parent_dependent_education_ira']
            : 0;
        $data['parent_dependent_non_retirement_cod'] = isset($efcInputData['parent_dependent_non_retirement_cod'])
            ? $efcInputData['parent_dependent_non_retirement_cod']
            : 0;
        $data['parent_dependent_non_retirement_tbill'] = isset($efcInputData['parent_dependent_non_retirement_tbill'])
            ? $efcInputData['parent_dependent_non_retirement_tbill']
            : 0;
        $data['parent_dependent_non_retirement_money_fund'] = isset($efcInputData['parent_dependent_non_retirement_money_fund'])
            ? $efcInputData['parent_dependent_non_retirement_money_fund']
            : 0;
        $data['parent_dependent_non_retirement_mutual_fund'] = isset($efcInputData['parent_dependent_non_retirement_mutual_fund'])
            ? $efcInputData['parent_dependent_non_retirement_mutual_fund']
            : 0;
        $data['parent_dependent_non_retirement_stock'] = isset($efcInputData['parent_dependent_non_retirement_stock'])
            ? $efcInputData['parent_dependent_non_retirement_stock']
            : 0;
        $data['parent_dependent_non_retirement_bond'] = isset($efcInputData['parent_dependent_non_retirement_bond'])
            ? $efcInputData['parent_dependent_non_retirement_bond']
            : 0;
        $data['parent_dependent_non_retirement_trust_fund'] = isset($efcInputData['parent_dependent_non_retirement_trust_fund'])
            ? $efcInputData['parent_dependent_non_retirement_trust_fund']
            : 0;
        $data['parent_dependent_non_retirement_other_security'] = isset($efcInputData['parent_dependent_non_retirement_other_security'])
            ? $efcInputData['parent_dependent_non_retirement_other_security']
            : 0;
        $data['parent_dependent_non_retirement_other_investment'] = isset($efcInputData['parent_dependent_non_retirement_other_investment'])
            ? $efcInputData['parent_dependent_non_retirement_other_investment']
            : 0;
        $data['parent_dependent_non_retirement_annuities'] = isset($efcInputData['parent_dependent_non_retirement_annuities'])
            ? $efcInputData['parent_dependent_non_retirement_annuities']
            : 0;
        $data['parent_dependent_retirement_db_account'] = isset($efcInputData['parent_dependent_retirement_db_account'])
            ? $efcInputData['parent_dependent_retirement_db_account']
            : 0;
        $data['parent_dependent_retirement_401k'] = isset($efcInputData['parent_dependent_retirement_401k'])
            ? $efcInputData['parent_dependent_retirement_401k']
            : 0;
        $data['parent_dependent_retirement_ira'] = isset($efcInputData['parent_dependent_retirement_ira'])
            ? $efcInputData['parent_dependent_retirement_ira']
            : 0;
        $data['parent_dependent_retirement_roth_ira'] = isset($efcInputData['parent_dependent_retirement_roth_ira'])
            ? $efcInputData['parent_dependent_retirement_roth_ira']
            : 0;
        $data['parent_dependent_retirement_other_plan'] = isset($efcInputData['parent_dependent_retirement_other_plan'])
            ? $efcInputData['parent_dependent_retirement_other_plan']
            : 0;
        $data['parent_dependent_insurance_life_insurance_cash_value'] = isset($efcInputData['parent_dependent_insurance_life_insurance_cash_value'])
            ? $efcInputData['parent_dependent_insurance_life_insurance_cash_value']
            : 0;
        $data['parent_dependent_asset_primary_net_worth_real_estate'] = isset($efcInputData['parent_dependent_asset_primary_net_worth_real_estate'])
            ? $efcInputData['parent_dependent_asset_primary_net_worth_real_estate']
            : 0;
        $data['parent_dependent_asset_net_worth_real_estate'] = isset($efcInputData['parent_dependent_asset_net_worth_real_estate'])
            ? $efcInputData['parent_dependent_asset_net_worth_real_estate']
            : 0;
        $data['parent_dependent_asset_net_worth_business'] = isset($efcInputData['parent_dependent_asset_net_worth_business'])
            ? $efcInputData['parent_dependent_asset_net_worth_business']
            : 0;
        $data['parent_dependent_asset_net_worth_farm'] = isset($efcInputData['parent_dependent_asset_net_worth_farm'])
            ? $efcInputData['parent_dependent_asset_net_worth_farm']
            : 0;

        // fafsa variables
        $data['fafsa_student_dependent_state_residence'] = isset($efcInputData['fafsa_student_dependent_state_residence'])
            ? $efcInputData['fafsa_student_dependent_state_residence']
            : 1;
        $data['fafsa_parent_dependent_1_state_residence'] = isset($efcInputData['fafsa_parent_dependent_1_state_residence'])
            ? $efcInputData['fafsa_parent_dependent_1_state_residence']
            : 1;
        $data['fafsa_parent_dependent_2_state_residence'] = isset($efcInputData['fafsa_parent_dependent_2_state_residence'])
            ? $efcInputData['fafsa_parent_dependent_2_state_residence']
            : 1;
        $data['fafsa_parent_number_family_members'] = isset($efcInputData['fafsa_parent_number_family_members'])
            ? $efcInputData['fafsa_parent_number_family_members']
            : 0;
        $data['fafsa_parent_number_family_members_college'] = isset($efcInputData['fafsa_parent_number_family_members_college'])
            ? $efcInputData['fafsa_parent_number_family_members_college']
            : 1;
        $data['fafsa_parent_number'] = isset($efcInputData['fafsa_parent_number'])
            ? $efcInputData['fafsa_parent_number']
            : 1;
        $data['fafsa_parent_1_marital_status'] = isset($efcInputData['fafsa_parent_1_marital_status'])
            ? $efcInputData['fafsa_parent_1_marital_status']
            : "M";
        $data['fafsa_parent_1_dob'] = isset($efcInputData['fafsa_parent_1_dob'])
            ? $efcInputData['fafsa_parent_1_dob']
            : now()->toDateTimeString();
        $data['fafsa_parent_2_dob'] = isset($efcInputData['fafsa_parent_2_dob'])
            ? $efcInputData['fafsa_parent_2_dob']
            : now()->toDateTimeString();
        $data['parent_bf_business_employee'] = isset($efcInputData['parent_bf_business_employee'])
            ? $efcInputData['parent_bf_business_employee']
            : "No";
        $data['parent_bf_farm_live'] = isset($efcInputData['parent_bf_farm_live'])
            ? $efcInputData['parent_bf_farm_live']
            : "No";

        $data['fafsa_parent_benefits'] = isset($efcInputData['fafsa_parent_benefits'])
            ? $efcInputData['fafsa_parent_benefits']
            : 0;
        $data['fafsa_parent_filed_1040_aez'] = isset($efcInputData['fafsa_parent_filed_1040_aez'])
            ? $efcInputData['fafsa_parent_filed_1040_aez']
            : 0;
        $data['fafsa_parent_filed_1040'] = isset($efcInputData['fafsa_parent_filed_1040'])
            ? $efcInputData['fafsa_parent_filed_1040']
            : 0;
        $data['fafsa_parent_no_income_tax'] = isset($efcInputData['fafsa_parent_no_income_tax'])
            ? $efcInputData['fafsa_parent_no_income_tax']
            : 0;
        $data['fafsa_parent_dislocated_worker'] = isset($efcInputData['fafsa_parent_dislocated_worker'])
            ? $efcInputData['fafsa_parent_dislocated_worker']
            : 0;

        return $data;
    }
}
