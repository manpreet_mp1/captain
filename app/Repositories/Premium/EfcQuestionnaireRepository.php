<?php

namespace App\Repositories\Premium;

use App\Models\Core\FormBlade;
use App\Models\Dropdown\InputValue;
use App\Models\Family\Family;
use App\Models\Family\FamilyChild;
use App\Repositories\Family\FamilyRepository;
use App\Repositories\Misc\FormRepository;

class EfcQuestionnaireRepository
{
    /**
     * Get the first blade name
     *
     * @return mixed
     */
    public function getFirstBladeName()
    {
        return FormBlade::efc()->orderBy('order_seq')->first()->blade_name;
    }

    /**
     * Get the required view data
     *
     * @param Family $family
     * @param $slug
     * @param $request
     * @return mixed
     */
    public function getViewData(Family $family, $slug, $request)
    {
        $data['schoolYear'] = InputValue::getEfcYearDropdown();
        $data['dropDownNumberTillFour'] = InputValue::getDropdownNumber(1, 4);
        $data['request'] = $request->all();
        $data['formBlade'] = FormBlade::efc()->where('blade_name', $slug)->first();
        $data['nextFormBlade'] = FormBlade::efc()->where('order_seq', $data['formBlade']->order_seq + 1)->first();
        $data['formBladeMenu'] = (new FormRepository())->getFormBladeMenu("efc", $slug);
        $data['family'] = $family;
        $data['familyChildren'] = $family->children;
        $data['familyChildrenArray'] = $data['familyChildren']->toArray();

        return $data;
    }

    public function store(Family $family, $slug, $request)
    {
        $formBlade = FormBlade::efc()->where('blade_name', $slug)->first();

        // based on the slug store the data
        switch ($slug) {
            case "student-in-college":
                // set all not in college
                FamilyChild::where('family_id', $family->id)->update([
                    'is_in_college' => 0
                ]);
                foreach ($request->input('in_college') as $childId) {
                    FamilyChild::find($childId)->update([
                        'is_in_college' => 1
                    ]);
                }

                break;
        }

        (new FamilyRepository())->updateFamilyParentStudents($family, $request);

        return FormBlade::efc()->where('order_seq', $formBlade->order_seq + 1)->first();
    }
}
