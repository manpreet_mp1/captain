<?php

namespace App\Repositories\Premium;

use App\Models\Data\CfgCalculator;
use App\Models\Data\College\College;
use App\Models\Dropdown\State;
use App\Models\Family\Family;
use App\Models\Family\Scenario;
use App\Repositories\Misc\HelperRepository;
use Illuminate\Support\Carbon;
use function min;
use function now;
use function number_format;

class QpCalculatorRepository
{
    /**
     * Get QP result
     *
     * @param Family $family
     * @param Scenario $scenario
     * @return mixed
     */
    public function getQpResultFromScenario(Family $family, Scenario $scenario)
    {
        $data['efc']['original'] = (new EfcCalculatorRepository())->getEfc($family, $scenario->family_child_id);
        $data['efc']['new'] = (new EfcCalculatorRepository())->getEfcFromScenario($scenario);
        $data['efc']['compare'] = $this->compareEfc($data['efc']);

        // get college details
        $data['collegesDetails'][1] = $this->getCollegeDetailsScenario($scenario, $scenario->college_id_1, $data['efc'], $scenario->college_year, $scenario->family->parentOne()->state_id);
        if ($scenario->college_id_2 != 0) {
            $data['collegesDetails'][2] = $this->getCollegeDetailsScenario($scenario, $scenario->college_id_2, $data['efc'], $scenario->college_year, $scenario->family->parentOne()->state_id);
        }
        if ($scenario->college_id_3 != 0) {
            $data['collegesDetails'][3] = $this->getCollegeDetailsScenario($scenario, $scenario->college_id_3, $data['efc'], $scenario->college_year, $scenario->family->parentOne()->state_id);
        }
        if ($scenario->college_id_4 != 0) {
            $data['collegesDetails'][4] = $this->getCollegeDetailsScenario($scenario, $scenario->college_id_4, $data['efc'], $scenario->college_year, $scenario->family->parentOne()->state_id);
        }

        return $data;
    }

    /**
     * Get college details for quick plan
     *
     * @param Scenario $scenario
     * @param $collegeId
     * @param $efc
     * @param int $collegeYear
     * @param null $stateId
     * @return mixed
     */
    public function getCollegeDetailsScenario(Scenario $scenario, $collegeId, $efc, $collegeYear = 1, $stateId = null)
    {
        $college = College::find($collegeId);
        $output['model'] = $college->toArray();
        $output['cost'] = $this->getCollegeCost($college, $efc['new'], $collegeYear, $stateId);

        // for quick plan only
        $output['collegeCostsImpactOnRetirement'] = $this->getCollegeCostsImpactOnRetirement($scenario, $output['cost']['graduationYearCostWithAid']);
        $output['fundingCollegeUsingIncomeAssets'] = $this->getFundingCollegeUsingIncomeAssets($scenario, $output['cost']['avgGraduationYear'], $output['cost']['graduationYearCostWithAid']);
        $output['fundingCollegeUsingEducationLoans'] = $this->getFundingCollegeUsingEducationLoans($output['fundingCollegeUsingIncomeAssets']['unfundedCostOfEducation']);
        $output['financialAidSavings'] = $this->getFinancialAidSavings($college, $efc);
        $output['financialPlanningStrategies'] = $this->getFinancialPlanningStrategies($scenario, $output['financialAidSavings']['totalFinancialAidSavings'], $output['collegeCostsImpactOnRetirement']);

        return $output;
    }

    private function getFundingCollegeUsingEducationLoans($unfundedCostOfEducation)
    {
        $output['unfundedCostOfEducation'] = $unfundedCostOfEducation;
        $studentLoanNumber = CfgCalculator::where('key', 'QP_FCFSL_WOT')->first();

        // there is short fall and is less than student loans
        if ($unfundedCostOfEducation > 0 && $unfundedCostOfEducation < $studentLoanNumber['value']) {
            $output['studentTotalLoans'] = $unfundedCostOfEducation;
        }

        // there is shortfall and is more than student loan
        if ($unfundedCostOfEducation > 0 && $unfundedCostOfEducation > $studentLoanNumber['value']) {
            $output['studentTotalLoans'] = (int)$studentLoanNumber['value'];
        }
        if ($unfundedCostOfEducation <= 0) {
            $output['studentTotalLoans'] = 0;
        }

        if ($output['studentTotalLoans'] > 0) {
            $output['studentLoanFeesPercent'] = CfgCalculator::where('key', 'QP_SL_LOR')->first()->value;
            $output['studentLoanFees'] = $output['studentLoanFeesPercent'] * $output['studentTotalLoans'];
            $output['studentLoanPrincipal'] = $output['studentLoanFees'] + $output['studentTotalLoans'];
            $output['studentLoanInterestPercent'] = CfgCalculator::where('key', 'QP_SL_IR')->first()->value;
            $output['studentMonthlyPayment'] = (($output['studentLoanInterestPercent'] / 12) * $output['studentLoanPrincipal']) /
                (1 - pow((1 + ($output['studentLoanInterestPercent'] / 12)), -120));
            $output['studentTotalLoanCost10Years'] = $output['studentMonthlyPayment'] * 120;
            $output['studentInterestPaidIncludingLoanFees'] = $output['studentTotalLoanCost10Years'] - $output['studentTotalLoans'];
        } else {
            $output['studentLoanFeesPercent'] = CfgCalculator::where('key', 'QP_SL_LOR')->first()->value;
            $output['studentLoanFees'] = 0;
            $output['studentLoanPrincipal'] = 0;
            $output['studentLoanInterestPercent'] = CfgCalculator::where('key', 'QP_SL_IR')->first()->value;
            $output['studentMonthlyPayment'] = 0;
            $output['studentTotalLoanCost10Years'] = 0;
            $output['studentInterestPaidIncludingLoanFees'] = 0;
        }

        // parent loan
        if ($output['unfundedCostOfEducation'] - $output['studentTotalLoans'] > 0) {
            $output['parentTotalLoans'] = $output['unfundedCostOfEducation'] - $output['studentTotalLoans'];
            $output['parentLoanFeesPercent'] = CfgCalculator::where('key', 'QP_PL_LOR')->first()->value;
            $output['parentLoanFees'] = $output['parentLoanFeesPercent'] * $output['parentTotalLoans'];
            $output['parentLoanPrincipal'] = $output['parentLoanFees'] + $output['parentTotalLoans'];
            $output['parentLoanInterestPercent'] = CfgCalculator::where('key', 'QP_PL_IR')->first()->value;
            $output['parentMonthlyPayment'] = (($output['parentLoanInterestPercent'] / 12) * $output['parentLoanPrincipal']) /
                (1 - pow((1 + ($output['parentLoanInterestPercent'] / 12)), -120));
            $output['parentTotalLoanCost10Years'] = $output['parentMonthlyPayment'] * 120;
            $output['parentInterestPaidIncludingLoanFees'] = $output['parentTotalLoanCost10Years'] - $output['parentTotalLoans'];
        } else {
            $output['parentTotalLoans'] = 0;
            $output['parentLoanFeesPercent'] = CfgCalculator::where('key', 'QP_PL_LOR')->first()->value;
            $output['parentLoanFees'] = 0;
            $output['parentLoanPrincipal'] = 0;
            $output['parentLoanInterestPercent'] = CfgCalculator::where('key', 'QP_PL_IR')->first()->value;
            $output['parentMonthlyPayment'] = 0;
            $output['parentTotalLoanCost10Years'] = 0;
            $output['parentInterestPaidIncludingLoanFees'] = 0;
        }

        // total all education loans
        $output['allTotalLoans'] = $output['studentTotalLoans'] + $output['parentTotalLoans'];
        $output['allLoanFees'] = $output['studentLoanFees'] + $output['parentLoanFees'];
        $output['allInterestPaidIncludingLoanFees'] = $output['studentInterestPaidIncludingLoanFees'] + $output['parentInterestPaidIncludingLoanFees'];
        $output['allMonthlyPayment'] = $output['studentMonthlyPayment'] + $output['parentMonthlyPayment'];
        $output['allTotalLoanCost10Years'] = $output['studentTotalLoanCost10Years'] + $output['parentTotalLoanCost10Years'];

        return $output;
    }

    /**
     * FINANCIAL PLANNING STRATEGIES
     *
     * @param Scenario $scenario
     * @param $totalFinancialAidSavings
     * @param $collegeCostsImpactOnRetirement
     * @return mixed
     */
    private function getFinancialPlanningStrategies(Scenario $scenario, $totalFinancialAidSavings, $collegeCostsImpactOnRetirement)
    {
        $output['financialAidSavingsTextMin'] = number_format(0, 0);
        $output['financialAidSavingsTextMax'] = number_format($totalFinancialAidSavings, 0);
        $output['financialAidSavingsTextRange'] = "$ {$output['financialAidSavingsTextMin']} - $ {$output['financialAidSavingsTextMax']}";

        $output['taxReductionPlanningTextMin'] = number_format($scenario->savings_tax_planning_min, 0);
        $output['taxReductionPlanningTextMax'] = number_format($scenario->savings_tax_planning_max, 0);
        $output['taxReductionPlanningTextRange'] = "$ {$output['taxReductionPlanningTextMin']} - $ {$output['taxReductionPlanningTextMax']}";

        $output['assetManagementTextMin'] = number_format($scenario->savings_assets_management_min, 0);
        $output['assetManagementTextMax'] = number_format($scenario->savings_assets_management_max, 0);
        $output['assetManagementTextRange'] = "$ {$output['assetManagementTextMin']} - $ {$output['assetManagementTextMax']}";

        $output['borrowingLoanStrategiesTextMin'] = number_format($scenario->savings_borrowing_loan_min, 0);
        $output['borrowingLoanStrategiesTextMax'] = number_format($scenario->savings_borrowing_loan_max, 0);
        $output['borrowingLoanStrategiesTextRange'] = "$ {$output['borrowingLoanStrategiesTextMin']} - $ {$output['borrowingLoanStrategiesTextMax']}";

        $output['cashflowImprovementsTextMin'] = number_format($scenario->savings_cashflow_improvement_min, 0);
        $output['cashflowImprovementsTextMax'] = number_format($scenario->savings_cashflow_improvement_max, 0);
        $output['cashflowImprovementsTextRange'] = "$ {$output['cashflowImprovementsTextMin']} - $ {$output['cashflowImprovementsTextMax']}";

        $output['otherTextMin'] = number_format($scenario->savings_other_min, 0);
        $output['otherTextMax'] = number_format($scenario->savings_other_max, 0);
        $output['otherTextRange'] = "$ {$output['otherTextMin']} - $ {$output['otherTextMax']}";

        $output['totalMin'] = $scenario->savings_tax_planning_min +
            $scenario->savings_assets_management_min +
            $scenario->savings_borrowing_loan_min +
            $scenario->savings_cashflow_improvement_min +
            $scenario->savings_other_min;
        $output['totalMax'] = $totalFinancialAidSavings +
            $scenario->savings_tax_planning_max +
            $scenario->savings_assets_management_max +
            $scenario->savings_borrowing_loan_max +
            $scenario->savings_cashflow_improvement_max +
            $scenario->savings_other_max;
        $output['totalTextMin'] = number_format($output['totalMin'], 0);
        $output['totalTextMax'] = number_format($output['totalMax'], 0);
        $output['totalTextRange'] = "$ {$output['totalTextMin']} - $ {$output['totalTextMax']}";

        $output['desiredAgeOfRetirement'] = $collegeCostsImpactOnRetirement['desiredAgeOfRetirement'];
        $output['yearsToRetirement'] = $collegeCostsImpactOnRetirement['yearsToRetirement'];
        $output['rateOfReturn'] = $collegeCostsImpactOnRetirement['rateOfReturn'];

        $output['impactMin'] = $output['totalMin'] + (new HelperRepository())->getCompoundInterest($output['totalMin'], $output['rateOfReturn']/100, $output['yearsToRetirement']);
        $output['impactMax'] = $output['totalMax'] + (new HelperRepository())->getCompoundInterest($output['totalMax'], $output['rateOfReturn']/100, $output['yearsToRetirement']);
        $output['impactTextMin'] = number_format($output['impactMin'], 0);
        $output['impactTextMax'] = number_format($output['impactMax'], 0);
        $output['impactTextRange'] = "$ {$output['impactTextMin']} - $ {$output['impactTextMax']}";

        return $output;
    }

    /**
     *  FUNDING COLLEGE - USING INCOME & ASSETS
     *
     * @param Scenario $scenario
     * @param $avgGraduationYear
     * @param $graduationYearCostWithAid
     * @return mixed
     */
    private function getFundingCollegeUsingIncomeAssets(Scenario $scenario, $avgGraduationYear, $graduationYearCostWithAid)
    {
        $output['studentContributionFromIncomeAtGraduation'] = $scenario->contributions_student_annual_income_dollar * $avgGraduationYear;
        $output['parentContributionFromIncomeAtGraduation'] = $scenario->contributions_parent_annual_income_dollar * $avgGraduationYear;
        $output['studentContributionFromAssetAtGraduation'] = $scenario->contributions_student_total_asset_dollar;
        $output['parentContributionFromAssetAtGraduation'] = $scenario->contributions_parent_assets_education_dollar +
            $scenario->contributions_parent_assets_non_retirement_dollar +
            $scenario->contributions_parent_assets_retirement_dollar +
            $scenario->contributions_parent_life_insurance_dollar +
            $scenario->contributions_parent_business_farm_dollar +
            $scenario->contributions_parent_real_estate_dollar;

        $output['totalPlannedContributions'] = $output['studentContributionFromIncomeAtGraduation'] +
            $output['parentContributionFromIncomeAtGraduation'] +
            $output['studentContributionFromAssetAtGraduation'] +
            $output['parentContributionFromAssetAtGraduation'];
        $output['unfundedCostOfEducation'] = $graduationYearCostWithAid - $output['totalPlannedContributions'] > 0
            ? $graduationYearCostWithAid - $output['totalPlannedContributions']
            : 0;

        return $output;
    }

    /**
     * FINANCIAL AID SAVINGS
     *
     * @param College $college
     * @param $efc
     * @return mixed
     */
    private function getFinancialAidSavings(College $college, $efc)
    {
        $output['efcBefore'] = $college->is_college_im == 1 ? $efc['original']['efc_im_final'] : $efc['original']['efc_fm_final'];
        $output['efcAfter'] = $college->is_college_im == 1 ? $efc['new']['efc_im_final'] : $efc['new']['efc_fm_final'];
        $output['needMet'] = $college->need_met * 100;
        $output['giftAid'] = $college->gift_aid * 100;
        $output['selfHelpAid'] = $college->self_help * 100;

        $output['potentialAnnualGiftAidSavings'] = ($output['efcBefore'] - $output['efcAfter']) * $college->gift_aid;
        $output['expectedYearsToGraduate'] = $college->avg_graduation_year;
        $output['totalFinancialAidSavings'] = $output['potentialAnnualGiftAidSavings'] * $output['expectedYearsToGraduate'];

        return $output;
    }

    /**
     * Get college cost impact on retirement
     *
     * @param Scenario $scenario
     * @param $graduationYearCostWithAid
     * @return mixed
     */
    private function getCollegeCostsImpactOnRetirement(Scenario $scenario, $graduationYearCostWithAid)
    {
        $output['parentOneAge'] = Carbon::parse($scenario->family->parentOne()->birth_date)->age;
        $output['parentTwoAge'] = $scenario->family->parentTwo()
            ? Carbon::parse($scenario->family->parentTwo()->birth_date)->age
            : 2000;
        $output['minAge'] = min([$output['parentOneAge'], $output['parentTwoAge']]);

        $output['desiredAgeOfRetirement'] = $scenario->planning_retirement_age;
        $output['yearsToRetirement'] = $output['desiredAgeOfRetirement'] - $output['minAge'];
        $output['rateOfReturn'] = $scenario->planning_investment_rate;
        $output['collegeCostImpactOnRetirement'] = ($graduationYearCostWithAid * $output['yearsToRetirement']) +
            (new HelperRepository())->getCompoundInterest($graduationYearCostWithAid, $output['rateOfReturn']/100, $output['yearsToRetirement']);

        return $output;
    }

    /**
     * Get college details for directory
     *
     * @param $collegeId
     * @param $efc
     * @param int $collegeYear
     * @param null $stateId
     * @return mixed
     */
    public function getCollegeDetails($collegeId, $efc, $collegeYear = 1, $stateId = null)
    {
        $college = College::find($collegeId);
        $output['model'] = $college->toArray();
        $output['cost'] = $this->getCollegeCost($college, $efc, $collegeYear, $stateId);

        return $output;
    }

    /**
     * Get college cost
     *
     * @param College $college
     * @param $efc
     * @param int $collegeYear
     * @param null $stateId
     * @return array
     */
    private function getCollegeCost(College $college, $efc, $collegeYear = 1, $stateId = null)
    {
        $column = "instate";

        $collegeStateId = State::whereAbbreviation($college->state_abbr)->first()->id;
        if ($stateId && $stateId != $collegeStateId) {
            $column = "out_state";
        }

        if ($college->is_private) {
            $column = "private";
        }

        $yearColumn = now()->year + $collegeYear - $college->year;
        $yearColumn = $yearColumn > 5 ? 5 : $yearColumn;

        $amountColumn = $column . "_cost_year_" . $yearColumn;
        $inflationColumn = "inflation_year_" . $yearColumn;

        $efc = $college->is_college_im == 1 ? $efc['efc_im_final'] : $efc['efc_fm_final'];

        $output['type'] = $column;
        $output['amountColumn'] = $amountColumn;
        $output['efc'] = $efc;
        $output['annualCost'] = $college->{$amountColumn};
        $output['inflation'] = $college->{$inflationColumn};

        if ($output['annualCost'] > $efc) {
            $output['collegeEligibleNeedCost'] = $output['annualCost'] - $output['efc'];
        } else {
            $output['collegeEligibleNeedCost'] = 0;
        }

        $output['needMet'] = $college->need_met;
        $output['giftAid'] = $college->gift_aid;
        $output['financialAidEligibility'] = $output['collegeEligibleNeedCost'] * $output['needMet'];
        $output['amountOfGiftAid'] = $output['financialAidEligibility'] * $output['giftAid'];
        $output['amountOfGiftAidFourYear'] = $output['amountOfGiftAid'] * 4;

        $output['fourYearCostWithoutAid'] = (new HelperRepository())->getCompoundInterest($output['annualCost'], $output['inflation'], 4) + 4 * $output['annualCost'];
        $output['fourYearCostWithAid'] = $output['fourYearCostWithoutAid'] - (4 * $output['amountOfGiftAid']);

        $output['avgGraduationYear'] = $college->avg_graduation_year;
        $output['graduationYearCostWithoutAid'] = (new HelperRepository())->getCompoundInterest($output['annualCost'], $output['inflation'], $output['avgGraduationYear']) + $output['avgGraduationYear'] * $output['annualCost'];
        $output['graduationYearCostWithAid'] = $output['graduationYearCostWithoutAid'] - ($output['avgGraduationYear'] * $output['amountOfGiftAid']);

        $output['avgSalary6YearsAfterEnrollment'] = $college->avg_earn_wne_p6;
        $output['avgSalary10YearsAfterEnrollment'] = $college->avg_earn_wne_p10;
        $output['educationInvestmentRecoveryRate6years'] = $output['avgSalary6YearsAfterEnrollment'] / $output['graduationYearCostWithAid'] * 100;
        $output['educationInvestmentRecoveryRate10years'] = $output['avgSalary10YearsAfterEnrollment'] / $output['graduationYearCostWithAid'] * 100;

        return $output;
    }

    /**
     * Compare old and new EFC
     *
     * @param $data
     * @return mixed
     */
    private function compareEfc($data)
    {
        $output['aid_first_year_fm'] = $data['original']['efc_fm_final'] - $data['new']['efc_fm_final'];
        $output['aid_first_year_im'] = $data['original']['efc_im_final'] - $data['new']['efc_im_final'];
        $output['aid_four_year_fm'] = $output['aid_first_year_fm'] * 4;
        $output['aid_four_year_im'] = $output['aid_first_year_im'] * 4;

        return $output;
    }
}
