<?php

namespace App\Repositories\Resources;

use App\Http\Controllers\Traits\AuthAdvisorTrait;
use App\Models\Data\College\College;
use App\Models\Dropdown\State;
use App\Models\Family\CollegeFamilyChild;
use App\Models\Family\Family;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CollegeRepository
{
    use AuthAdvisorTrait;

    /**
     * Get all the view data
     * @param null $caseSlug
     * @param null $collegeSlug
     * @return mixed
     */
    public function getViewData($collegeSlug = null, $caseSlug = null)
    {
        $data['states'] = ['' => ''] + State::pluck('name', 'abbreviation')->toArray();
        $data['enrollmentSize'] = [
            '' => '',
            '1' => '0 - 1K',
            '2' => '1K - 2K',
            '3' => '2K - 4K',
            '4' => '4K - 6K',
            '5' => '6K - 10K',
            '6' => '10K - 15K',
            '7' => '15K - 20K',
            '8' => '20K - 30K',
            '9' => '> 30K',
        ];
        $data['instituteType'] = [
            '' => '',
            'Public' => 'Public',
            'Private' => 'Private',
        ];

        $data['locale'] =  [
            '' => '',
            'City: Large' => 'Large City',
            'City: Midsize' => 'Midsize City',
            'City: Small' => 'Small City',
            'Rural: Distant' => 'Distant Rural',
            'Rural: Fringe' => 'Fringe Rural',
            'Rural: Remote' => 'Remote Rural',
            'Suburb: Large' => 'Large Suburb',
            'Suburb: Midsize' => 'Midsize Suburb',
            'Suburb: Small' => 'Small Suburb',
            'Town: Distant' => 'Distant Town',
            'Town: Fringe' => 'Fringe Town',
            'Town: Remote' => 'Remote Town',
        ];

        if ($caseSlug) {
            $data['family'] = $this->family($caseSlug);
        } else {
            $data['family'] = Auth::user()->family;
        }

        if ($collegeSlug) {
            $data['students'] = $data['family']->children;
            $data['students']->load('user');
            $college = $this->getItem($collegeSlug);

            $data['savedArray'] = CollegeFamilyChild::whereIn('family_child_id', $data['students']->pluck('id'))
                ->where('college_id', $college->unitid)
                ->pluck('family_child_id')
                ->toArray();
        }

        return $data;
    }

    /**
     * Get all items
     *
     * @param Request $request
     * @return mixed
     */
    public function getItems(Request $request)
    {
        return College::search($request)
            ->orderBy('name')
            ->paginate(5);
    }

    /**
     * Get item
     *
     * @param $slug
     * @return mixed
     */
    public function getItem($slug)
    {
        return College::where('slug', $slug)->first();
    }

    /**
     * Save a college to students
     *
     * @param Family $family
     * @param $collegeSlug
     * @param null $studentIds
     */
    public function save(Family $family, $collegeSlug, $studentIds = null)
    {
        // get the college
        $college = $this->getItem($collegeSlug);

        // get all the students
        $students = $family->children;

        // delete all records for these child for this school
        CollegeFamilyChild::whereIn('family_child_id', $students->pluck('id'))
            ->where('college_id', $college->unitid)
            ->delete();

        // if there are students
        if ($studentIds) {
            foreach ($studentIds as $studentId) {
                CollegeFamilyChild::create([
                    'college_id' => $college->unitid,
                    'family_child_id' => $studentId
                ]);
            }
        }
    }
}
