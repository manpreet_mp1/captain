<?php

namespace App\Repositories\Resources;

use App\Http\Controllers\Traits\AuthAdvisorTrait;
use App\Models\Data\Scholarship\ScholarshipMerit;
use App\Models\Dropdown\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MeritScholarshipsRepository
{
    use AuthAdvisorTrait;

    /**
     * Get the required view data
     *
     * @param null $caseSlug
     * @return mixed
     */
    public function getViewData($caseSlug = null)
    {
        $output['stateList'] = ['' => ''] + State::pluck('name', 'abbreviation')->toArray();
        $output['typeList'] = [
            '' => '',
            'Automatic' => 'Automatic',
            'Competitive' => 'Competitive',
            'Talent' => 'Talent',
            'National Merit/Achievement' => 'National Merit/Achievement',
        ];

        if ($caseSlug) {
            $output['family'] = $this->family($caseSlug);
        } else {
            $output['family'] = Auth::user()->family;
        }

        return $output;
    }

    /**
     * Get all items
     *
     * @param Request $request
     * @return mixed
     */
    public function getItems(Request $request)
    {
        return ScholarshipMerit::search($request)
            ->selectRaw('
                ipeds,
                school_name,
                COUNT(id) AS scholarship_count
            ')
            ->groupBy('ipeds', 'school_name')
            ->orderBy('school_name')
            ->get();
    }

    /**
     * Get item
     *
     * @param $ipeds
     * @return mixed
     */
    public function getItem($ipeds)
    {
        return ScholarshipMerit::where('ipeds', $ipeds)->get();
    }
}
