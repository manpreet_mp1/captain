<?php

namespace App\Repositories\Resources;

use App\Http\Controllers\Traits\AuthAdvisorTrait;
use App\Models\Data\Major\Major;
use App\Models\Family\MajorFamilyChild;
use App\Models\Family\Family;
use Illuminate\Support\Facades\Auth;

class MajorRepository
{
    use AuthAdvisorTrait;

    /**
     * Get the view data
     *
     * @param null $majorSlug
     * @param null $caseSlug
     * @return mixed
     */
    public function getViewData($majorSlug = null, $caseSlug = null)
    {
        $data['main'] = Major::main()->get();

        if ($caseSlug) {
            $data['family'] = $this->family($caseSlug);
        } else {
            $data['family'] = Auth::user()->family;
        }

        if ($majorSlug) {
            $data['students'] = $data['family']->children;
            $data['students']->load('user');
            $major = $this->getItem($majorSlug);

            $data['savedArray'] = MajorFamilyChild::whereIn('family_child_id', $data['students']->pluck('id'))
                ->where('major_code', $major->code)
                ->pluck('family_child_id')
                ->toArray();
        }

        return $data;
    }

    /**
     * Get heading items
     *
     * @param $mainSlug
     * @return mixed
     */
    public function getHeadings($mainSlug)
    {
        $data['main'] = Major::where('slug', $mainSlug)
            ->main()
            ->first();

        $data['heading'] = Major::where('family', $data['main']->family)
            ->where('type', 'heading')
            ->get();

        return $data;
    }

    /**
     * Get an item
     *
     * @param $slug
     * @return mixed
     */
    public function getItem($slug)
    {
        return Major::where('slug', $slug)->first();
    }

    /**
     * Save a Major to students
     *
     * @param Family $family
     * @param $majorSlug
     * @param null $studentIds
     */
    public function save(Family $family, $majorSlug, $studentIds = null)
    {
        // get the major
        $major = $this->getItem($majorSlug);

        // get all the students
        $students = $family->children;

        // delete all records for these child for this major
        MajorFamilyChild::whereIn('family_child_id', $students->pluck('id'))
            ->where('major_code', $major->code)
            ->delete();

        // if there are students
        if ($studentIds) {
            foreach ($studentIds as $studentId) {
                MajorFamilyChild::create([
                    'major_code' => $major->code,
                    'family_child_id' => $studentId
                ]);
            }
        }
    }
}
