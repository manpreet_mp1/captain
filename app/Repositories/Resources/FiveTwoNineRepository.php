<?php

namespace App\Repositories\Resources;

use App\Models\Data\FiveTwoNine\FiveTwoNineDropdownType;
use App\Models\Data\FiveTwoNine\FiveTwoNineMaster;
use App\Models\Dropdown\State;
use Illuminate\Http\Request;

class FiveTwoNineRepository
{
    /**
     * Get all the view data
     *
     * @return mixed
     */
    public function getViewData()
    {
        $data['states'] = ['' => ''] + State::pluck('name', 'id')->toArray();

        $data['types'] = ['' => ''] + FiveTwoNineDropdownType::pluck('label', 'value')->toArray();

        return $data;
    }

    /**
     * Get all items
     *
     * @param Request $request
     * @return mixed
     */
    public function getItems(Request $request)
    {
        return FiveTwoNineMaster::filter($request)
            ->orderBy('name')
            ->paginate(5);
    }

    /**
     * Get item
     *
     * @param $slug
     * @return mixed
     */
    public function getItem($slug)
    {
        $plan = FiveTwoNineMaster::where('slug', $slug)->first();
        $plan->load(['management', 'benefits', 'InvestmentOptions', 'InvestmentOptionsFirstDetails']);

        return $plan;
    }

    /**
     * Get plan dropdown
     *
     * @return mixed
     */
    public function getPlansDropdown()
    {
        return ['' => ''] + FiveTwoNineMaster::orderBy('name')->pluck('name', 'id')->toArray();
    }

    /**
     * Get four plans data
     *
     * @param Request $request
     * @return array
     */
    public function getPlans(Request $request)
    {
        $data['planOne'] = [];
        $data['planTwo'] = [];
        $data['planThree'] = [];

        if ($request->input('planOne')) {
            $data['planOne'] = FiveTwoNineMaster::find($request->input('planOne'));
            // $data['planOne']->load(['management', 'benefits', 'InvestmentOptions', 'InvestmentOptionsFirstDetails']);
        }

        if ($request->input('planTwo')) {
            $data['planTwo'] = FiveTwoNineMaster::find($request->input('planTwo'));
            // $data['planTwo']->load(['management', 'benefits', 'InvestmentOptions', 'InvestmentOptionsFirstDetails']);
        }

        if ($request->input('planThree')) {
            $data['planThree'] = FiveTwoNineMaster::find($request->input('planThree'));
            // $data['planThree']->load(['management', 'benefits', 'InvestmentOptions', 'InvestmentOptionsFirstDetails']);
        }

        return $data;
    }
}
