<?php

namespace App\Repositories\Family;

use App\Models\Core\User;
use App\Models\Data\College\College;
use App\Models\Family\Family;
use App\Models\Family\FamilyChild;
use App\Models\Family\Scenario;
use Illuminate\Http\Request;
use function array_merge;
use function now;
use function optional;

class FamilyQuickPlanRepository
{
    /**
     * Get all the scenarios for a family
     *
     * @param Family $family
     * @return \Illuminate\Support\Collection
     */
    public function getScenarios(Family $family)
    {
        return Scenario::where('family_id', $family->id)
            ->get();
    }

    /**
     * Get all the shared scenarios for a family
     *
     * @param Family $family
     * @return \Illuminate\Support\Collection
     */
    public function getSharedScenarios(Family $family)
    {
        return Scenario::where('family_id', $family->id)
            ->where('is_shared_with_family', 1)
            ->get();
    }

    /**
     * Get one scenario
     *
     * @param Family $family
     * @param $slug
     * @return Scenario|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object
     */
    public function getScenario(Family $family, $slug)
    {
        return Scenario::where('family_id', $family->id)
            ->whereSlug($slug)
            ->first();
    }

    /**
     * Update a scenario
     *
     * @param Scenario $scenario
     * @param $request
     */
    public function updateScenario(Scenario $scenario, $request)
    {
        if ($request->input('is_shared_with_family') == "on") {
            $request->merge([
                'is_shared_with_family' => 1
            ]);
        } else {
            $request->merge([
                'is_shared_with_family' => 0
            ]);
        }

        $scenario->update($request->all());
    }

    /**
     * Get all the students from a family
     *
     * @param Family $family
     * @return mixed
     */
    public function getStudentDropDown(Family $family)
    {
        $output = [];

        $studentUsers = User::students()
            ->where('family_id', $family->id)
            ->get();

        foreach ($studentUsers as $user) {
            $familyChild = FamilyChild::where('user_id', $user->id)
                ->first();
            if ($familyChild) {
                $output[$familyChild->id] = $user->first_name;
            }
        }

        return $output;
    }

    /**
     * Create a scenario
     *
     * @param Family $family
     * @param Request $request
     * @return Scenario|\Illuminate\Database\Eloquent\Model
     */
    public function createScenarios(Family $family, Request $request)
    {
        $data = $this->getScenarioData($family, $request);

        $item = Scenario::create($data);

        return $item;
    }

    /**
     * Get scenario data
     *
     * @param Family $family
     * @param Request $request
     * @return array
     */
    private function getScenarioData(Family $family, Request $request)
    {
        $data = array_merge($request->all(), [
            'family_id' => $family->id
        ]);

        $student = FamilyChild::find($request->input('family_child_id'));
        $parentOne = $family->parentOne();
        $parentTwo = $family->parentTwo();

        // student data
        $data['student_income_adjusted_gross_dollar'] = $student->income_adjusted_gross_dollar ?? 0;
        $data['student_income_work_dollar'] = $student->income_work_dollar ?? 0;
        $data['student_income_tax_dollar'] = $student->income_tax_dollar ?? 0;
        $data['student_income_untaxed_dollar_total'] = $student->income_untaxed_dollar_total ?? 0;
        $data['student_income_additional_dollar_total'] = $student->income_additional_dollar_total ?? 0;
        $data['student_asset_non_retirement_checking_savings_dollar'] = $student->asset_non_retirement_checking_savings_dollar ?? 0;
        $data['student_asset_non_retirement_ugma_utma_dollar'] = $student->asset_non_retirement_ugma_utma_dollar ?? 0;
        $data['student_asset_non_retirement_certificate_of_deposit_dollar'] = $student->asset_non_retirement_certificate_of_deposit_dollar ?? 0;
        $data['student_asset_non_retirement_t_bills_dollar'] = $student->asset_non_retirement_t_bills_dollar ?? 0;
        $data['student_asset_non_retirement_money_market_dollar'] = $student->asset_non_retirement_money_market_dollar ?? 0;
        $data['student_asset_non_retirement_mutual_dollar'] = $student->asset_non_retirement_mutual_dollar ?? 0;
        $data['student_asset_non_retirement_stock_dollar'] = $student->asset_non_retirement_stock_dollar ?? 0;
        $data['student_asset_non_retirement_bond_dollar'] = $student->asset_non_retirement_bond_dollar ?? 0;
        $data['student_asset_non_retirement_trust_dollar'] = $student->asset_non_retirement_trust_dollar ?? 0;
        $data['student_asset_non_retirement_other_investments_dollar'] = $student->asset_non_retirement_other_investments_dollar ?? 0;
        // parent data
        $data['parent_1_income_work_dollar'] = $parentOne->income_work_dollar ?? 0;
        $data['parent_2_income_work_dollar'] = optional($parentTwo)->income_work_dollar ?? 0;
        // family data
        $data['parent_income_adjusted_gross_dollar_total'] = $family->parent_income_adjusted_gross_dollar_total ?? 0;
        $data['parent_income_tax_dollar_total'] = $family->parent_income_tax_dollar_total ?? 0;
        $data['parent_income_untaxed_dollar_total'] = $family->parent_income_untaxed_dollar_total ?? 0;
        $data['parent_income_additional_dollar_total'] = $family->parent_income_additional_dollar_total ?? 0;
        $data['parent_asset_non_retirement_checking_savings_dollar_total'] = $family->parent_asset_non_retirement_checking_savings_dollar_total ?? 0;
        $data['parent_asset_non_retirement_certificate_of_deposit_dollar_total'] = $family->parent_asset_non_retirement_certificate_of_deposit_dollar_total ?? 0;
        $data['parent_asset_non_retirement_t_bills_dollar_total'] = $family->parent_asset_non_retirement_t_bills_dollar_total ?? 0;
        $data['parent_asset_non_retirement_money_market_dollar_total'] = $family->parent_asset_non_retirement_money_market_dollar_total ?? 0;
        $data['parent_asset_non_retirement_mutual_dollar_total'] = $family->parent_asset_non_retirement_mutual_dollar_total ?? 0;
        $data['parent_asset_non_retirement_stock_dollar_total'] = $family->parent_asset_non_retirement_stock_dollar_total ?? 0;
        $data['parent_asset_non_retirement_bond_dollar_total'] = $family->parent_asset_non_retirement_bond_dollar_total ?? 0;
        $data['parent_asset_non_retirement_trust_dollar_total'] = $family->parent_asset_non_retirement_trust_dollar_total ?? 0;
        $data['parent_asset_non_retirement_other_securities_dollar_total'] = $family->parent_asset_non_retirement_other_securities_dollar_total ?? 0;
        $data['parent_asset_non_retirement_annuities_dollar_total'] = $family->parent_asset_non_retirement_annuities_dollar_total ?? 0;
        $data['parent_asset_non_retirement_other_investments_dollar_total'] = $family->parent_asset_non_retirement_other_investments_dollar_total ?? 0;
        $data['parent_asset_education_student_sibling_dollar_total'] = $family->parent_asset_education_student_sibling_dollar_total ?? 0;
        $data['parent_asset_education_five_two_nine_dollar_total'] = $family->parent_asset_education_five_two_nine_dollar_total ?? 0;
        $data['parent_asset_education_coverdell_dollar_total'] = $family->parent_asset_education_coverdell_dollar_total ?? 0;
        $data['parent_asset_retirement_defined_benefit_dollar_total'] = $family->parent_asset_retirement_defined_benefit_dollar_total ?? 0;
        $data['parent_asset_retirement_four_o_one_dollar_total'] = $family->parent_asset_retirement_four_o_one_dollar_total ?? 0;
        $data['parent_asset_retirement_ira_dollar_total'] = $family->parent_asset_retirement_ira_dollar_total ?? 0;
        $data['parent_asset_retirement_roth_ira_dollar_total'] = $family->parent_asset_retirement_roth_ira_dollar_total ?? 0;
        $data['parent_asset_retirement_other_dollar_total'] = $family->parent_asset_retirement_other_dollar_total ?? 0;
        $data['parent_insurance_permanent_cash_value_dollar_total'] = $parentOne->insurance_permanent_cash_value_dollar +
            optional($parentTwo)->insurance_permanent_cash_value_dollar ?? 0;
        $data['parent_farm_net_worth_dollar'] = $family->parent_farm_value_dollar ?? 0;
        $data['parent_business_net_worth_dollar'] = $family->parent_business_value_dollar ?? 0;
        $data['parent_asset_real_estate_primary_net_worth_dollar'] = $family->parent_asset_real_estate_primary_net_worth_dollar ?? 0;
        $data['parent_asset_real_estate_other_net_worth_dollar'] = $family->parent_asset_real_estate_other_net_worth_dollar ?? 0;

        return $data;
    }

    /**
     * Get dropdown values
     *
     * @param Scenario $scenario
     * @return mixed
     */
    public function getCollegeDropdownPopulatedValue(Scenario $scenario)
    {
        $output['college_id_1'] = $this->overrideCollegeDropdown($scenario->college_id_1);
        $output['college_id_2'] = $this->overrideCollegeDropdown($scenario->college_id_2);
        $output['college_id_3'] = $this->overrideCollegeDropdown($scenario->college_id_3);
        $output['college_id_4'] = $this->overrideCollegeDropdown($scenario->college_id_4);

        return $output;
    }

    /**
     * If value is selected override the value
     *
     * @param $id
     * @return array|\Illuminate\Support\Collection
     */
    public function overrideCollegeDropdown($id)
    {
        if ($id) {
            return College::where('unitid', $id)
                ->pluck('inst_nm_city', 'unitid');
        }

        return ['0' => 'Select a College'];
    }

    /**
     * Get college year dropdown
     *
     * @return array
     */
    public function getCollegeYearDropdown()
    {
        return [
            1 => now()->addYears(0)->year . " - " . now()->addYears(1)->year,
            2 => now()->addYears(1)->year . " - " . now()->addYears(2)->year,
            3 => now()->addYears(2)->year . " - " . now()->addYears(3)->year,
            4 => now()->addYears(3)->year . " - " . now()->addYears(4)->year,
            5 => now()->addYears(4)->year . " - " . now()->addYears(5)->year,
        ];
    }

    /**
     * Get savings dropdown
     *
     * @return array
     */
    public function getSavingDropdown()
    {
        return [
            "0" => "$0",
            "500" => "$500",
            "1000" => "$1000",
            "1500" => "$1500",
            "2000" => "$2000",
            "2500" => "$2500",
            "3000" => "$3000",
            "3500" => "$3500",
            "4000" => "$4000",
            "4500" => "$4500",
            "5000" => "$5000",
            "5500" => "$5500",
            "6000" => "$6000",
            "6500" => "$6500",
            "7000" => "$7000",
            "7500" => "$7500",
            "8000" => "$8000",
            "8500" => "$8500",
            "9000" => "$9000",
            "9500" => "$9500",
            "10000" => "$10000",
            "11000" => "$11000",
            "12000" => "$12000",
            "13000" => "$13000",
            "14000" => "$14000",
            "15000" => "$15000",
            "16000" => "$16000",
            "17000" => "$17000",
            "18000" => "$18000",
            "19000" => "$19000",
            "20000" => "$20000",
            "21000" => "$21000",
            "22000" => "$22000",
            "23000" => "$23000",
            "24000" => "$24000",
            "25000" => "$25000",
            "30000" => "$30000",
            "35000" => "$35000",
            "40000" => "$40000",
            "45000" => "$45000",
            "50000" => "$50000",
            "60000" => "$60000",
            "70000" => "$70000",
            "80000" => "$80000",
            "90000" => "$90000",
            "100000" => "$100000",
        ];
    }
}
