<?php

namespace App\Repositories\Family;

use App\Models\Core\User;
use App\Models\Data\CfgCalculator;
use App\Models\Dropdown\InputValue;
use App\Models\Dropdown\State;
use App\Models\Family\Family;
use App\Models\Family\FamilyChild;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use function dd;
use function pow;

class FamilyRepository
{
    /**
     * Qualification Criteria
     * - both parent income > 75000 (if only parent check for one parent)
     * - has at least one asset (bank, 529, investment, retirement) more than 50k
     *
     * @param Family $family
     * @return int
     */
    public function checkIfQualifiedOrNot(Family $family)
    {
        // for each check income
        foreach ($family->parents as $parent) {
            // if income less than 75000
            if ($parent->income_work_dollar < 75000) {
                return 0;
            }
        }

        // has at least one asset (bank, 529, investment, retirement) more than 50k
        if ($family->parent_asset_non_retirement_dollar_total < 50000
            && $family->parent_asset_education_dollar_total < 50000
            && $family->parent_asset_retirement_dollar_total < 50000) {
            return 0;
        }

        return 1;
    }

    /**
     * Get family cost of college and impact on retirement
     *
     * @param Family $family
     * @return array
     */
    public function getFamilyCostOfCollegeAndImpactOnRetirement(Family $family)
    {
        $studentData = new Collection();
        $cfgCalculator = CfgCalculator::all();

        // for each check income
        foreach ($family->children as $child) {
            $studentData->push($this->getKidCost($child, $cfgCalculator));
        }

        $ageOfOldestParent = 0;
        foreach ($family->parents as $parent) {
            $ageOfOldestParent = $parent->age > $ageOfOldestParent ? $parent->age : $ageOfOldestParent;
        }

        // expected cost to educate family
        $output['directCost'] = (int)$studentData->sum('fourYearCost');
        $output['costIfBorrowed'] = (int)$studentData->sum('totalLoan');
        $output['ageOfOldestParent'] = $ageOfOldestParent;
        $output['parentDesiredRetirementAge'] = 65;
        $output['expectedAnnualGains'] = 5;
        $output['yearsUntilRetirement'] = $output['parentDesiredRetirementAge'] - $output['ageOfOldestParent'];
        $output['yearsUntilRetirement'] = $output['yearsUntilRetirement'] > 0 ? $output['yearsUntilRetirement'] : 0;
        $output['impactOnRetirement']['directCost'] =  $output['yearsUntilRetirement'] > 0
            ? (int)($output['directCost'] * pow((1 + $output['expectedAnnualGains']/100), $output['yearsUntilRetirement']))
            : $output['directCost'];
        $output['impactOnRetirement']['costIfBorrowed'] =  $output['yearsUntilRetirement'] > 0
            ? (int)($output['costIfBorrowed'] * pow((1 + $output['expectedAnnualGains']/100), $output['yearsUntilRetirement']))
            : $output['costIfBorrowed'];

        // student data
        $output['studentData'] = $studentData->toArray();

        return $output;
    }

    /**
     * Get each kid cost
     *
     * @param FamilyChild $child
     * @return array
     */
    private function getKidCost(FamilyChild $child, $cfgCalculator)
    {
        $return['firstName'] = $child->user->first_name;
        $return['firstYearInCollege'] = "--";
        $return['firstAcademicYearInCollege'] = "--";

        // get the inflation rate
        $inflation = (int)$cfgCalculator->firstWhere('key', 'QP_FCAR_CCI')->value;

        // ipeds average cost
        // get the average price of college - average net price income more than 75001
        $ipedsAverageCost = (int)$cfgCalculator->firstWhere('key', 'IPEDS_NET_PRICE_75001_PLUS_AVERAGE')->value;
        $ipedsYearData = (int)$cfgCalculator->firstWhere('key', 'IPEDS_YEAR_DATA')->value;

        // get the college cost for various grade levels
        switch ($child->grade_level_id) {
            case FamilyChild::GRADE_LEVEL_ID['COMMUNITY_COLLEGE']:
                $return['collegeType'] = "community";
                // first two years community college
                $return['avgCost'] = (int)$cfgCalculator->firstWhere('key', 'QP_COMMUNITY_COLLEGE_COST')->value;
                $return['firstYearCost'] = $return['avgCost'];
                $twoYearCommunityCost = (($return['firstYearCost'] * pow((1 + $inflation), 2) - $return['firstYearCost']) + ($return['firstYearCost']* 2));
                // next two year real college
                $return['firstYearInCollege'] = Carbon::now()->year + 2;
                $return['firstAcademicYearInCollege'] = $return['firstYearInCollege'] . "-" . ($return['firstYearInCollege'] + 1);
                $return['ipedsDataYearDifference'] = $return['firstYearInCollege'] - $ipedsYearData;
                $return['avgCost'] = $ipedsAverageCost;
                $firstYearCollegeCost = (($return['avgCost'] * pow((1 + (float)$inflation), (int)$return['ipedsDataYearDifference'])));
                $twoYearCollegeCost = (($firstYearCollegeCost * pow((1 + $inflation), 2) - $firstYearCollegeCost) + ($firstYearCollegeCost * 2));
                // total cost = two years community college + two years real college
                $return['fourYearCost'] = $twoYearCommunityCost + $twoYearCollegeCost;

                break;
            case FamilyChild::GRADE_LEVEL_ID['NOT_IN_SCHOOL']:
                $return['collegeType'] = "notInSchool";
                $return['avgCost'] = (int)$cfgCalculator->firstWhere('key', 'QP_NOT_IN_COLLEGE_COST')->value;
                $return['firstYearCost'] = $return['avgCost'];
                $return['fourYearCost'] = (($return['firstYearCost'] * pow((1 + $inflation), 4) - $return['firstYearCost']) + ($return['firstYearCost']* 4));

                break;
            default:
                $return['collegeType'] = "regular";
                $return['firstYearInCollege'] = $child->getKidYearInCollege()['firstYearInCollege'];
                $return['firstAcademicYearInCollege'] = $child->getKidYearInCollege()['firstAcademicYearInCollege'];
                // get first year and four year cost
                $return['ipedsDataYearDifference'] = $return['firstYearInCollege'] - $ipedsYearData;
                $return['avgCost'] = $ipedsAverageCost;
                // calculate the final amount using compound interest
                $return['firstYearCost'] = (($return['avgCost'] * pow((1 + (float)$inflation), (int)$return['ipedsDataYearDifference'])));
                $return['fourYearCost'] = (($return['firstYearCost'] * pow((1 + $inflation), 4) - $return['firstYearCost']) + ($return['firstYearCost']* 4));
        }

        // get the fixed student loan from cfg
        $return['studentLoan'] = (int)$cfgCalculator->firstWhere('key', 'QP_FCFSL_WT')->value;
        $return['studentLoan'] = $return['studentLoan'] <= $return['fourYearCost'] ? $return['studentLoan'] : $return['fourYearCost'];
        // get parent loan
        if ($return['fourYearCost'] - $return['studentLoan'] > 0) {
            $shortFall = $return['fourYearCost'] - $return['studentLoan'];
            // now we need to compound this for seven years
            // get the inflation rate from the DB
            $parentLoanFeesPercent = $cfgCalculator->firstWhere('key', 'QP_PL_LOR')->value;
            $parentLoanFees = $parentLoanFeesPercent * $shortFall;
            $shortFall = $shortFall + $parentLoanFees;
            $inflation = $cfgCalculator->firstWhere('key', 'QP_PL_IR')->value;
            $parentMonthlyPayment   = (($inflation / 12) * $shortFall) / (1 - pow((1 + ($inflation / 12)), -120));
            $return['parentLoan'] = (int)round($parentMonthlyPayment * 120, 0);
        } else {
            $return['parentLoan'] = 0;
        }
        $return['totalLoan'] = $return['studentLoan'] + $return['parentLoan'];

        return $return;
    }

    /**
     * Update family, parent, student
     *
     * @param Family $family
     * @param $request
     * @param null $familyColumn
     * @param int $familyColumnValue
     */
    public function updateFamilyParentStudents(Family $family, $request, $familyColumn = null, $familyColumnValue = 1)
    {
        if ($familyColumn) {
            $family->update([
                $familyColumn => $familyColumnValue
            ]);
        }

        // if parent one
        if ($family->parentOne() && $request->input('parentOne')) {
            $family->parentOne()->update($request->input('parentOne'));
        }

        // if parent two
        if ($family->parentTwo() && $request->input('parentTwo')) {
            $family->parentTwo()->update($request->input('parentTwo'));
        }

        // if family data
        if ($familyData = $request->input('family')) {
            $family->update($familyData);
        }

        // if student data
        if ($students = $request->input('student')) {
            foreach ($students as $studentId => $studentValue) {
                FamilyChild::find($studentId)->update($studentValue);
            }
        }
    }
}
