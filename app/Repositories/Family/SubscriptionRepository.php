<?php

namespace App\Repositories\Family;

use App\Models\Family\Family;
use App\Services\StripeService;
use function config;

class SubscriptionRepository
{
    public $smarttrackSku;
    public $smarttrackAdvisorSku;
    public $bundleSku;
    public $subscriptionThreePaymentSmarttrack;
    public $subscriptionThreePaymentSmarttrackAdvisor;
    public $subscriptionThreePaymentBundle;

    public function __construct()
    {
        $this->smarttrackSku = config('services.stripe.products.sku.smarttrack');
        $this->smarttrackAdvisorSku = config('services.stripe.products.sku.smarttrack_advisor');
        $this->bundleSku = config('services.stripe.products.sku.bundle');

        $this->subscriptionThreePaymentSmarttrack = config('services.stripe.subscription.three_payments.smarttrack');
        $this->subscriptionThreePaymentSmarttrackAdvisor = config('services.stripe.subscription.three_payments.smarttrack_advisor');
        $this->subscriptionThreePaymentBundle = config('services.stripe.subscription.three_payments.bundle');
    }

    /**
     * Get Pricing for each product
     *
     * @return mixed
     */
    public function getPricing()
    {
        $output['products']['smarttrack'] = (new StripeService())->retrieveASku($this->smarttrackSku);
        $output['products']['smarttrackAdvisor'] = (new StripeService())->retrieveASku($this->smarttrackAdvisorSku);
        $output['products']['bundle'] = (new StripeService())->retrieveASku($this->bundleSku);

        return $output;
    }

    /**
     * Get a fixed price product
     *
     * @param $slug
     * @return \Stripe\SKU
     */
    public function retrieveFixedProduct($slug)
    {
        switch ($slug) {
            case "smarttrack":
                return  (new StripeService())->retrieveASku($this->smarttrackSku);
            case "smarttrack-advisor":
                return  (new StripeService())->retrieveASku($this->smarttrackAdvisorSku);
            case "bundle":
                return  (new StripeService())->retrieveASku($this->bundleSku);
        }
    }

    /**
     * Get a fixed price product
     *
     * @param $slug
     * @return mixed
     */
    public function retrievePaymentPlans($slug)
    {
        switch ($slug) {
            case "smarttrack-3-payment":
                return (object) [
                    'months' => 3,
                    'plan' => (new StripeService())->retrieveAPlan($this->subscriptionThreePaymentSmarttrack)
                ];
            case "smarttrack-advisor-3-payment":
                return (object) [
                    'months' => 3,
                    'plan' => (new StripeService())->retrieveAPlan($this->subscriptionThreePaymentSmarttrackAdvisor)
                ];
            case "bundle-3-payment":
                return (object) [
                    'months' => 3,
                    'plan' => (new StripeService())->retrieveAPlan($this->subscriptionThreePaymentBundle)
                ];
        }
    }

    /**
     * Check if a coupon code is valid
     *
     * @param $couponCode
     * @param int $checkForSubscription
     * @return bool|mixed
     */
    public function validCouponCode($couponCode, $checkForSubscription = 0)
    {
        $coupon = (new StripeService())->retrieveACoupon($couponCode);

        if ($coupon && $checkForSubscription === 0 && $coupon->duration <> 'once') {
            return false;
        }

        return $coupon;
    }

    /**
     * Apply coupon return discount and due today
     *
     * @param $price
     * @param $couponCode
     * @return array
     */
    public function applyCoupon($price, $couponCode)
    {
        $coupon = (new StripeService())->retrieveACoupon($couponCode);

        // has a fixed amount
        if ($coupon->amount_off) {
            $output['discount'] = $coupon->amount_off;
            $difference = $price - $output['discount'];
            $output['dueToday'] = $difference > 0 ? $difference : 0;
        } else {
            $output['discount'] = $coupon->percent_off/100 * $price;
            $difference = $price - $output['discount'];
            $output['dueToday'] = $difference > 0 ? $difference : 0;
        }

        return $output;
    }

    /**
     * Create a order
     *
     * @param Family $family
     * @param $sku
     * @param $stripeToken
     * @return mixed
     */
    public function createAOrder(Family $family, $sku, $stripeToken)
    {
        // does not have a custom id, create a stripe customer
        if (!$family->payment_customer_id) {
            $customer = (new StripeService())->createACustomer($family);
        }

        // add the card
        (new StripeService())->createACard($family, $stripeToken);

        // add product to the customer
        return (new StripeService())->createAndPayAOrder($family, $sku);
    }

    /**
     * Create a subscription
     *
     * @param Family $family
     * @param $planId
     * @param $months
     * @param $stripeToken
     * @return mixed
     */
    public function createASubscription(Family $family, $planId, $months, $stripeToken)
    {
        // does not have a custom id, create a stripe customer
        if (!$family->payment_customer_id) {
            $customer = (new StripeService())->createACustomer($family);
        }

        // add the card
        (new StripeService())->createACard($family, $stripeToken);

        // create a subscription
        return (new StripeService())->createASubscription($family, $planId, $months);
    }

    /**
     * Get subscription information
     *
     * @param Family $family
     * @return array
     */
    public function getSubscription(Family $family)
    {
        return [];
    }
}
