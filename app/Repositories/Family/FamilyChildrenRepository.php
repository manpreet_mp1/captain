<?php

namespace App\Repositories\Family;

use App\Models\Core\User;
use App\Models\Dropdown\InputValue;
use App\Models\Family\Family;
use App\Models\Family\FamilyChild;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use function optional;

class FamilyChildrenRepository
{
    /**
     * Add a new parent
     *
     * @param $firstName
     * @param User $user
     * @param $gradeLevelId
     * @param $relationshipParentOneId
     * @param null $relationshipParentTwoId
     */
    public function addFamilyChildren($firstName, User $user, $gradeLevelId, $relationshipParentOneId, $relationshipParentTwoId = null)
    {
        // create a tmp password
        $tmpPasswordString = Str::random(10);

        // Create the user
        $userTwo = User::create([
            'family_id' => $user->family_id,
            'first_name' => $firstName,
            'last_name' => $user->last_name,
            'role' => 'STUDENT',
            'tmp_password' => $tmpPasswordString,
            'password' => Hash::make($tmpPasswordString),
        ]);

        // create a family student
        FamilyChild::create([
            'user_id' => $userTwo->id,
            'family_id' => $user->family_id,
            'grade_level_id' => $gradeLevelId,
            'relationship_parent_one_id' => $relationshipParentOneId,
            'relationship_parent_two_id' => $relationshipParentTwoId,
        ]);

        return $userTwo;
    }

    /**
     * Create a new student
     *
     * @param Family $family
     * @param $request
     * @return mixed
     */
    public function storeNewStudent(Family $family, $request)
    {
        $user = $this->addFamilyChildren(
            $request->input('first_name'),
            $family->parentOne()->user,
            $request->input('child.grade_level_id'),
            $request->input('child.relationship_parent_one_id'),
            $request->input('child.relationship_parent_two_id')
        );

        $user->update($request->all());

        return $user;
    }


    /**
     * Update child
     *
     * @param Family $family
     * @param $slug
     * @param $request
     */
    public function updateChild(Family $family, $slug, $request)
    {
        $data['user'] = User::where('slug', $slug)->first();
        $data['child'] = $data['user']->child;

        // update user object
        $data['user']->update($request->all());

        // update student object
        $data['child']->update($request->input('child'));
    }

    /**
     * Delete child
     *
     * @param $slug
     * @return bool
     */
    public function deleteChild($slug)
    {
        $childUser = User::where('slug', $slug)->first();
        $childUserId = $childUser->id;

        // delete the child user
        $childUser->delete();

        // delete the child
        FamilyChild::where('user_id', $childUserId)->delete();

        return true;
    }

    /**
     * Get child view data
     *
     * @param Family $family
     * @param $slug
     * @return mixed
     */
    public function getEditViewData(Family $family, $slug)
    {
        $return['family'] = $family;
        $return['user'] = User::where('slug', $slug)->first();
        $return['child'] = optional($return['user'])->child;

        $return['gradeLevel'] = ['' => ''] + InputValue::gradeLevel()->orderBy('order_seq')->pluck('label', 'value')->toArray();
        $return['relationshipToParent'] = ['' => ''] + InputValue::relationshipToParent()->orderBy('order_seq')->pluck('label', 'value')->toArray();
        $return['dropDownNumberTillFive'] = InputValue::getDropdownNumber(1, 5);

        return $return;
    }
}
