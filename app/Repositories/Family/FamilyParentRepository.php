<?php

namespace App\Repositories\Family;

use App\Models\Core\User;
use App\Models\Dropdown\InputValue;
use App\Models\Dropdown\State;
use App\Models\Family\Family;
use App\Models\Family\FamilyParent;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use function optional;

class FamilyParentRepository
{
    /**
     * Add a new parent
     *
     * @param $firstName
     * @param User $user
     */
    public function addNewParent($firstName, User $user)
    {
        // create a tmp password
        $tmpPasswordString = Str::random(10);

        // Create the user
        $userTwo = User::create([
            'family_id' => $user->family_id,
            'first_name' => $firstName,
            'last_name' => $user->last_name,
            'role' => 'PARENT',
            'tmp_password' => $tmpPasswordString,
            'password' => Hash::make($tmpPasswordString),
        ]);

        // create a family parent
        FamilyParent::create([
            'user_id' => $userTwo->id,
            'family_id' => $user->family_id,
            'marital_status_id' => $user->parent->marital_status_id,
            'state_id' => $user->parent->state_id,
        ]);
    }

    /**
     * Add a new parent with more details this is from family info
     *
     * @param Family $family
     * @param $request
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function storeNewParent(Family $family, $request)
    {
        $data['family'] = $family;
        $parentOneUser = $data['family']->parentOne()->user;

        // create a parent
        $this->addNewParent($request->input('first_name'), $parentOneUser);

        $parentTwo = $data['family']->parentTwo();
        $parentTwoUser = $parentTwo->user;

        // update the parent
        $this->updateParent($family, $parentTwoUser->slug, $request);

        return $parentTwoUser;
    }

    /**
     * Update parent
     *
     * @param Family $family
     * @param $slug
     * @param $request
     */
    public function updateParent(Family $family, $slug, $request)
    {
        // get the variables
        $data['family'] = $family;
        $data['user'] = User::where('slug', $slug)->first();
        $data['parent'] = $data['user']->parent;

        // update parent user object
        $data['user']->update($request->all());

        // update other object using the repository
        (new FamilyRepository())->updateFamilyParentStudents($data['family'], $request);

        // if the request is for parent 1 and marital status is single
        if ($request->input('is_main_parent') == 1 && $data['parent']->fresh()->is_parent_two === 0) {
            $this->deleteParentTwo($data['family']);
        }

        // update parent two state
        if ($request->input('is_main_parent') == 1 && $parentTwo = $data['family']->parentTwo()) {
            $parentTwo->update([
                'state_id' => $data['parent']->fresh()->state_id
            ]);
        }
    }

    /**
     * Delete parent two
     *
     * @param Family $family
     * @return bool
     */
    public function deleteParentTwo(Family $family)
    {
        // get parent two
        $parentTwo = $family->parentTwo();

        if ($parentTwo) {
            // delete parent
            FamilyParent::find($parentTwo->id)->delete();
            // delete parent user
            User::find($parentTwo->user_id)->delete();
        }

        return true;
    }

    /**
     * Get common view data for parent
     *
     * @param Family $family
     * @param $parentSlug
     * @return mixed
     */
    public function getEditParentView(Family $family, $parentSlug)
    {
        $return['family'] = $family;
        $return['parentUser'] = User::where('slug', $parentSlug)->first();
        $return['parent'] = optional($return['parentUser'])->parent;

        $parentOneId = $return['family']->parentOne()->user_id;
        $return['is_main_parent'] = $parentOneId === optional($return['parentUser'])->id ? 1 : 0;
        $return['parent_prefix'] = $return['is_main_parent'] == 1 ? 'parentOne' : 'parentTwo';
        $return['email_readonly'] = $return['is_main_parent'] == 1 ? 'readonly' : '';
        $return['state_disabled'] = $return['is_main_parent'] == 0 ? true : false;

        $return['states'] = ['' => ''] + State::pluck('name', 'id')->toArray();
        $return['employmentStatus'] = ['' => ''] + InputValue::employmentStatus()->orderBy('order_seq')->pluck('label', 'value')->toArray();
        $return['gradeLevel'] = ['' => ''] + InputValue::gradeLevel()->orderBy('order_seq')->pluck('label', 'value')->toArray();
        $return['relationshipToParent'] = ['' => ''] + InputValue::relationshipToParent()->orderBy('order_seq')->pluck('label', 'value')->toArray();
        $return['dropDownNumberTillFive'] = InputValue::getDropdownNumber(1, 5);
        $return['householdRole'] = InputValue::householdRole()->get()->toArray();
        $return['maritalStatus'] = InputValue::maritalStatus()->get()->toArray();
        $return['additionalIncomeSources'] = InputValue::additionalIncomeSources()->get()->toArray();

        return $return;
    }
}
