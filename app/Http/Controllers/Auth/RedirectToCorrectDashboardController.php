<?php
namespace App\Http\Controllers\Auth;

use function abort;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use function redirect;
use function session;

class RedirectToCorrectDashboardController extends Controller
{
    /**
     * Redirect to the correct dashboard
     *
     * @return RedirectResponse
     */
    public function index()
    {
        $user = Auth::user();

        // return is set in url
        if ($redirect = session()->get('return')) {
            session()->forget('return');

            return redirect($redirect);
        }

        switch (true) {
            case $user->is_parent === 1:
                return redirect()->route('parent.dashboard');

                break;
            case $user->is_advisor === 1:
                return redirect()->route('advisor.dashboard');

                break;
            case $user->is_admin === 1:
                return redirect()->route('admin.dashboard');

                break;
        }

        abort(404);
    }
}
