<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\Core\User;
use function event;
use function flash;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use function view;

class LoginController extends Controller
{
    /**
     * Show the form for login.
     *
     * @param Request $request
     * @return Factory|View
     */
    public function index(Request $request)
    {
        // if Request has return url, decode it and save it to session
        if ($request->has('return')) {
            $request->session()->put('return', base64_decode($request->get('return')));
        } else {
            $request->session()->forget('return');
        }

        // return view
        return view('auth.login.index');
    }

    /**
     * Authenticate the user.
     *
     * @param LoginRequest $request
     * @return Factory|RedirectResponse|View
     */
    public function authenticate(LoginRequest $request)
    {
        // lets try to authenticate the user
        $credentials = [
            'email' => $request->input('email'),
            'password' => $request->input('password'),
        ];

        // make sure the account related to email is valid
        $user = User::where('email', $credentials['email'])
            ->first();

        if ($user && $user->is_account === 0) {
            flash('Account is not active.', 'toastr.error');

            return view('auth.login.index');
        }

        // Lets try to login the user
        if (Auth::attempt($credentials)) {

            // Login the user
            Auth::login(Auth::user());

            // @todo
            // fire an event when the user is logged in
            // event(new UserLoggedInEvent(Auth::user()));

            // show success message and redirect to dashboard
            flash('Logged In Successfully.', 'toastr.info');

            return redirect()->route('dashboard');
        }

        // not able to login
        flash('Incorrect Email or Password. Try Again.', 'toastr.error');

        // return view
        return view('auth.login.index');
    }
}
