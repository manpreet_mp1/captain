<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class LogoutController extends Controller
{
    /**
     * Logout a user
     *
     * @return Factory|View
     */
    public function index()
    {
        // Logout user
        Auth::logout();

        // show success message and redirect to dashboard
        flash('Logged Out Successfully.', 'toastr.info');

        return redirect()->route('login');
    }
}
