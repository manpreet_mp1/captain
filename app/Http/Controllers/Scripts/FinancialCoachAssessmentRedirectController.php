<?php
namespace App\Http\Controllers\Scripts;

use App\Http\Controllers\Controller;
use App\Repositories\Misc\FinancialCoachAssessmentRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use function in_array;
use function redirect;

class FinancialCoachAssessmentRedirectController extends Controller
{
    /**
     * Get the values from URL and apply the logic
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function index(Request $request)
    {
        $source = $request->input('source');
        $configArray = config('scripts.financialCoachAssessment');

        // if source mapping is present
        if (isset($configArray[$source])) {
            $qualifiedUrl = $configArray[$source]['qualifiedUrl'];
            $notQualifiedUrl = $configArray[$source]['notQualifiedUrl'];
        } else {
            $qualifiedUrl = $configArray['general']['qualifiedUrl'];
            $notQualifiedUrl = $configArray['general']['notQualifiedUrl'];
        }

        // Get if they are qualified or not
        $qualifiedOrNotQualified = (new FinancialCoachAssessmentRepository())->getQualifiedOrNotQualified($request);

        // If they are qualified
        if ($qualifiedOrNotQualified === "qualified") {
            return redirect()->away($qualifiedUrl);
        }

        // By default they are not qualified
        return redirect()->away($notQualifiedUrl);
    }
}
