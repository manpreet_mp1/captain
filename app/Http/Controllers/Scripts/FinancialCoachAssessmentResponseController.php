<?php
namespace App\Http\Controllers\Scripts;

use App\Http\Controllers\Controller;
use App\Models\ThirdParty\TypeformResponse;
use App\Repositories\Misc\FinancialCoachAssessmentRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\View\View;
use function array_key_exists;
use function array_merge;
use function base64_decode;
use function compact;
use function is_array;
use function str_replace;
use function view;

class FinancialCoachAssessmentResponseController extends Controller
{
    /**
     * Retrieve Data from Typeform table and show in view
     *
     * @param Request $request
     * @return Factory|View
     */
    public function index(Request $request)
    {
        $email = base64_decode($request->input('record'));

        $records = TypeformResponse::financialCoachAssessmentForm()
            ->byEmail($email)
            ->get();

        $items = $this->transformRecords($records);

        return view('thirdParty.typeForm.index', compact('items'));
    }

    /**
     * Get the results from submissions
     *
     * @return Factory|View
     */
    public function result()
    {
        // get all the records
        $data['records'] = TypeformResponse::financialCoachAssessmentForm()
            ->orderBy('created_at')
            ->get();

        $data['results'] = new Collection();

        foreach ($data['records'] as $record) {
            $qualifiedNotQualified = (new FinancialCoachAssessmentRepository())->getQualifiedOrNotQualifiedDb($record->data);

            $data['results']->push([
                'id' => $record->id,
                'first_name' => $record->data['answers'][0]['text'],
                'last_name' => $record->data['answers'][1]['text'],
                'email' => $record->email,
                'qualified_not_qualified' => $qualifiedNotQualified['status'],
                'created_at' => Carbon::parse($record->data['submitted_at'])->toFormattedDateString(),
                'data' => $qualifiedNotQualified['data'],
            ]);
        }

        return view('thirdParty.typeForm.result', compact('data'));
    }

    /**
     * Transform the DB records
     *
     * @param $records
     * @return Collection
     */
    private function transformRecords($records)
    {
        $output = new Collection();

        foreach ($records as $record) {
            $tmpArray = [];
            $tmpArray['submitted_at'] = Carbon::parse($record->data['submitted_at'])->toFormattedDateString();

            $fields = collect($record->data['definition']['fields']);
            $answers = collect($record->data['answers']);

            foreach ($fields as $field) {
                $tmpArray['questionAnswer'][] = [
                    'question' => $this->getQuestion($field['title'], $answers),
                    'answer' => $this->getAnswer($field['id'], $answers),
                ];
            }

            $output->push($tmpArray);
        }

        return $output;
    }

    /**
     * Transform question
     *
     * @param $question
     * @param $answers
     * @return mixed
     */
    private function getQuestion($question, $answers)
    {
        $question = str_replace("{{answer_110840055}}", $this->getAnswer("I1piHIrzLQwH", $answers), $question);
        $question = str_replace("{{answer_110860844}}", $this->getAnswer("xq3rpATsOh8B", $answers), $question);
        $question = str_replace("{{answer_110842860}}", $this->getAnswer("ewK0uRjzKlc1", $answers), $question);

        $question = str_replace("<br><br>", "<br>", $question);

        return $question;
    }

    /**
     * Get answer
     *
     * @param $fieldId
     * @param $answers
     * @return string
     */
    private function getAnswer($fieldId, $answers)
    {
        $answerRecord = $answers->where('field.id', $fieldId)->first();

        switch ($answerRecord['type']) {
            case "text":
                return $answerRecord['text'];

            case "long_text":
                return $answerRecord['text'];

            case "email":
                return $answerRecord['email'];

            case "number":
                return $answerRecord['number'];

            case "choice":
                return $answerRecord['choice']['label'];

            case "boolean":
                return $answerRecord['boolean'] ? "Yes" : "No";

            case "choices":
                // Set default answers
                $output = [];
                // If there are labels i.e. multi-select add it to array
                if (is_array($answerRecord['choices']['labels'])) {
                    $output = array_merge($answerRecord['choices']['labels'], $output);
                }
                // If there are other records
                if (array_key_exists('other', $answerRecord['choices'])) {
                    $output = array_merge([$answerRecord['choices']['other']], $output);
                }

                return implode($output, ", ");

            default:
                return "N/A";

        }
    }
}
