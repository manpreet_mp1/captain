<?php
namespace App\Http\Controllers\Webhooks;

use App\Http\Controllers\Controller;
use App\Services\AcuityService;
use function config;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;
use function in_array;
use function response;

class AcuityUpdateAppointmentByIdController extends Controller
{
    private $acuityService;

    public function __construct()
    {
        $this->acuityService = new AcuityService();
    }

    /**
     * Update a acuity appointment
     *
     * @return ResponseFactory|Response
     */
    public function index()
    {
        parse_str(file_get_contents('php://input'), $body);

        // Get appointment id and appointment type id
        $appointmentId = $body['id'];
        $appointmentTypeID = $body['appointmentTypeID'];

        // Valid data
        if ($appointmentId && $appointmentTypeID && in_array($appointmentTypeID, config('scripts.acuity.validAppointmentTypeIds'))) {
            // Update appointment via service
            $this->acuityService->updateNCSAInternalDataForm($appointmentId);
        }

        return response([]);
    }
}
