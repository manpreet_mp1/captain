<?php
namespace App\Http\Controllers\Webhooks;

use App\Http\Controllers\Controller;
use App\Models\ThirdParty\TypeformResponse;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class FinancialCoachAssessmentSaveResponseController extends Controller
{
    /**
     * Insert Data into the Typeform Table
     *
     * @param Request $request
     * @return ResponseFactory|Response
     */
    public function index(Request $request)
    {
        // Get the basic data
        if ($data = $request->get('form_response')) {
            $formId = $data['form_id'];
            $email = collect($data['answers'])->where('type', 'email')->first()['email'];

            // Save the response to DB
            TypeformResponse::create([
                'data' => $data,
                'form_id' => $formId,
                'email' => $email
            ]);
        }

        return response([]);
    }
}
