<?php

namespace App\Http\Controllers\Premium;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use function view;

class SmartStepsPremiumController extends Controller
{
    /**
     * Show smart steps data
     *
     * @return Factory|View
     */
    public function index()
    {
        // @todo - for demo purpose just a static blade, move this DB, or I think we will be using WP
        return view('premium.smartSteps.index');
    }
}
