<?php

namespace App\Http\Controllers\Parent;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\AuthFamilyTrait;
use App\Models\Dropdown\InputValue;
use App\Repositories\Family\FamilyRepository;
use App\Repositories\Premium\EfcCalculatorRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use function compact;
use function view;

class ParentDashboardController extends Controller
{
    use AuthFamilyTrait;

    /**
     * Show Parent Dashboard
     *
     * @return Factory|RedirectResponse|View
     */
    public function index()
    {
        $data['family'] = $this->family();
        $data['familyParents'] = $data['family']->parents;
        $data['familyChildren'] = $data['family']->children;
        $data['familyCostOfCollegeAndImpactOnRetirement'] = (new FamilyRepository())->getFamilyCostOfCollegeAndImpactOnRetirement($data['family']);
        if ($data['family']->is_completed_efc === 1) {
            $data['efc'] = (new EfcCalculatorRepository())->getEfc($data['family']);
        }
        $data['timezone'] = InputValue::timezones()->orderBy('order_seq')->pluck('label', 'value')->toArray();

        return view('parent.dashboard.index', compact('data'));
    }
}
