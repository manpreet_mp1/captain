<?php

namespace App\Http\Controllers\Parent;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\AuthFamilyTrait;
use App\Http\Requests\Parent\GeneralParentRequest;
use App\Repositories\Premium\EfcQuestionnaireRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use function compact;
use function redirect;
use function view;

class ParentEfcQuestionnaireController extends Controller
{
    use AuthFamilyTrait;

    /**
     * Get the free assessment
     *
     * @param $slug
     * @param Request $request
     * @return Factory|RedirectResponse|View
     */
    public function index($slug, Request $request)
    {
        $firstEfcStepBladeName = (new EfcQuestionnaireRepository())->getFirstBladeName();

        // if slug is not set, get the current step user is on and redirect
        if ($slug === "0") {
            return redirect()->route('parent.questionnaires.efc', [
                'slug' => $firstEfcStepBladeName
            ]);
        }

        // get the required data
        $data = (new EfcQuestionnaireRepository())->getViewData($this->family(), $slug, $request);

        return view("parent.questionnaires.efc.{$slug}", compact('data'));
    }

    /**
     * Store the efc data
     *
     * @param $slug
     * @param GeneralParentRequest $request
     * @return RedirectResponse
     */
    public function store($slug, GeneralParentRequest $request)
    {
        // save and get the next form request
        $nextFormBlade = (new EfcQuestionnaireRepository())->store($this->family(), $slug, $request);
        
        // all questionnaire completed, show result
        if (!$nextFormBlade) {
            $this->family()->update([
                'is_completed_efc' => 1
            ]);
            // @todo - fire event to send slack notification

            return redirect()->route('parent.efc.result');
        }

        return redirect()->route('parent.questionnaires.efc', [
            'slug' => $nextFormBlade->blade_name
        ]);
    }
}
