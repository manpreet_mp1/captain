<?php

namespace App\Http\Controllers\Parent;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\AuthAdvisorTrait;
use App\Repositories\Resources\MeritScholarshipsRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;
use function compact;
use function view;

class ParentMeritScholarshipsController extends Controller
{
    /**
     * Show the merit scholarship landing page
     *
     * @param Request $request
     * @return Factory|View
     */
    public function index(Request $request)
    {
        $data = (new MeritScholarshipsRepository())->getViewData();

        $data['request'] = $request->all();

        $data['records'] = (new MeritScholarshipsRepository())->getItems($request);

        return view('parent.resources.scholarship.merit.index', compact('data'));
    }

    /**
     * Show the merit scholarship for a school
     *
     * @param $ipeds
     * @return Factory|View
     */
    public function show($ipeds)
    {
        $data = (new MeritScholarshipsRepository())->getViewData();

        $data['records'] = (new MeritScholarshipsRepository())->getItem($ipeds);

        return view('parent.resources.scholarship.merit.show', compact('data'));
    }
}
