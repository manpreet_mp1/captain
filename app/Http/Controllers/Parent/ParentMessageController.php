<?php

namespace App\Http\Controllers\Parent;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\AuthFamilyTrait;
use function compact;
use function view;

class ParentMessageController extends Controller
{
    use AuthFamilyTrait;

    public function index()
    {
        return view('parent.messages.index', compact('data'));
    }
}
