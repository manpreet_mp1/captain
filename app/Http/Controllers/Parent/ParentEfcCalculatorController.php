<?php

namespace App\Http\Controllers\Parent;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\AuthFamilyTrait;
use App\Repositories\Family\FamilyRepository;
use App\Repositories\Premium\EfcCalculatorRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use function compact;
use function redirect;
use function view;

class ParentEfcCalculatorController extends Controller
{
    use AuthFamilyTrait;

    /**
     * Show the efc result
     *
     * @return Factory|RedirectResponse|View
     */
    public function index()
    {
        $data['family'] = $this->family();

        // make sure the efc is completed
        if ($data['family']->is_completed_efc !== 1) {
            return redirect()->route('parent.questionnaires.efc', [
                'slug' => '0'
            ]);
        }

        $data['familyCostOfCollegeAndImpactOnRetirement'] = (new FamilyRepository())->getFamilyCostOfCollegeAndImpactOnRetirement($data['family']);
        $data['efc'] = (new EfcCalculatorRepository())->getEfc($data['family']);
        
        return view('parent.calculators.efc.result', compact('data'));
    }
}
