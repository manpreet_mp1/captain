<?php

namespace App\Http\Controllers\Parent;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\AuthFamilyTrait;
use App\Repositories\Resources\MajorRepository;
use function compact;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use function view;

class ParentMajorController extends Controller
{
    use AuthFamilyTrait;

    /**
     * Show the landing page for majors
     *
     * @return Factory|View
     */
    public function index()
    {
        $data = (new MajorRepository())->getViewData();

        return view('parent.resources.majors.index', compact('data'));
    }

    /**
     * Show the heading for the particular main category
     *
     * @param $slug
     * @return Factory|View
     */
    public function heading($slug)
    {
        $data = (new MajorRepository())->getHeadings($slug);

        return view('parent.resources.majors.heading', compact('data'));
    }

    /**
     * Show a resource
     *
     * @param $slug
     * @return Factory|View
     */
    public function show($slug)
    {
        $data = (new MajorRepository())->getViewData($slug);

        $data['record'] = (new MajorRepository())->getItem($slug);

        return view('parent.resources.majors.show', compact('data'));
    }

    /**
     * Save a major to students
     *
     * @param $slug
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save($slug, Request $request)
    {
        (new MajorRepository())->save($this->family(), $slug, $request->input('family_child_id'));

        flash('Success', 'toastr.info');

        return redirect()->route('parent.majors.show', ['slug' => $slug]);
    }
}
