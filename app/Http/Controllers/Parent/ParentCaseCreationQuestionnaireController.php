<?php

namespace App\Http\Controllers\Parent;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\AuthFamilyTrait;
use App\Http\Requests\Parent\GeneralParentRequest;
use App\Models\Core\FormBlade;
use App\Models\Dropdown\InputValue;
use App\Models\Dropdown\State;
use App\Repositories\Family\FamilyChildrenRepository;
use App\Repositories\Family\FamilyParentRepository;
use App\Repositories\Family\FamilyRepository;
use App\Repositories\Misc\FormRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use function compact;
use function redirect;
use function view;

class ParentCaseCreationQuestionnaireController extends Controller
{
    use AuthFamilyTrait;

    /**
     * Get the free assessment
     *
     * @param $slug
     * @param Request $request
     * @return Factory|RedirectResponse|View
     */
    public function index($slug, Request $request)
    {
        // if user has already completed the assessment, redirect to dashboard
        if ($this->family()->is_completed_assessment === 1) {
            return redirect()->route('parent.dashboard');
        }

        // if slug is not set, get the current step user is on and redirect
        if ($slug === "0") {
            return redirect()->route('parent.questionnaires.caseCreation', [
                'slug' => (new FormRepository())->getCurrentFormSlug("caseCreation", $this->family()->assessment_current_step_num)
            ]);
        }

        // get the required data
        $data = $this->getViewData();
        $data['request'] = $request->all();
        $data['formBlade'] =  FormBlade::caseCreation()->where('blade_name', $slug)->first();
        $data['formBladeMenu'] = (new FormRepository())->getFormBladeMenu("caseCreation", $slug);

        // current step should be same as sequence
        if ($data['formBlade']->order_seq !== $this->family()->assessment_current_step_num) {
            return redirect()->route('parent.questionnaires.caseCreation', [
                'slug' => (new FormRepository())->getCurrentFormSlug("caseCreation", $this->family()->assessment_current_step_num)
            ]);
        }

        // if we are parent two info and there is no parent two, skip blade
        if (strpos($slug, "parent-two-") !== false && $this->parent()->is_parent_two === 0) {
            $this->storeAssessmentStepData($this->family()->assessment_current_step_num);

            return redirect()->route('parent.questionnaires.caseCreation', [
                'slug' => (new FormRepository())->getCurrentFormSlug("caseCreation", $this->family()->assessment_current_step_num)
            ]);
        }
        
        return view("parent.questionnaires.caseCreation.{$slug}", compact('data'));
    }

    /**
     * Store the assessment data
     *
     * @param $slug
     * @param GeneralParentRequest $request
     * @return RedirectResponse
     */
    public function store($slug, GeneralParentRequest $request)
    {
        // post request should be from the current step
        if ((int)$request->input('assessment_current_step_num') === $this->family()->assessment_current_step_num) {

            // based on the slug store the data
            switch ($slug) {
                case "parent-two-first-name":
                    $parentTwo = $request->input('parentTwo');
                    // create a new parent
                    (new FamilyParentRepository())->addNewParent(
                        $firstName = $parentTwo['first_name'],
                        $this->user()
                    );

                    break;
                case "parent-two-last-name":
                    $this->userTwo()->update($request->input('parentTwo'));

                    break;
                case "parent-two-email":
                    $this->userTwo()->update($request->input('parentTwo'));

                    break;
                case "household-details":
                    $childRecords = $request->input('child');
                    foreach ($childRecords as $record) {
                        // create a new child
                        (new FamilyChildrenRepository())->addFamilyChildren(
                            $firstName = $record['first_name'],
                            $this->user(),
                            $gradeLevelId = $record['grade_level_id'],
                            $relationshipParentOneId = $record['relationship_parent_one_id'],
                            $relationshipParentTwoId = isset($record['relationship_parent_two_id']) ? $record['relationship_parent_two_id'] : null
                        );
                    }

                    break;
            }

            // Update info
            (new FamilyRepository())->updateFamilyParentStudents($this->family(), $request);
            
            // store the current assessment step
            if ($request->input('assessment_current_step_num')) {
                $this->storeAssessmentStepData($request->input('assessment_current_step_num'));
            }
        }

        return redirect()->route('parent.questionnaires.caseCreation', [
            'slug' => (new FormRepository())->getCurrentFormSlug("caseCreation", $this->family()->assessment_current_step_num)
        ]);
    }

    /**
     * Store data related to assessment steps
     *
     * @param $assessmentCurrentStepNum
     */
    private function storeAssessmentStepData($assessmentCurrentStepNum)
    {
        // get the max order sequence
        $maxOrder = FormBlade::caseCreation()->orderBy('order_seq', 'desc')->first()->order_seq;

        // if the current step is max order sequence, then mark the assessment complete
        if ($maxOrder === (int)$assessmentCurrentStepNum) {
            $this->family()->update([
                'is_completed_assessment' => 1
            ]);
        // @todo - fire event once assessment is complete
        } else {
            $this->family()->update([
                'assessment_current_step_num' => $assessmentCurrentStepNum + 1
            ]);
        }
    }

    /**
     * Get View Data
     *
     * @return array
     */
    private function getViewData()
    {
        $data['formBlades'] =  FormBlade::caseCreation()->orderBy('order_seq')->get();
        $data['landingSubBlade'] = $this->getLandingPageSubBlade();

        // get all dropdown, checkbox and radio box values
        $data['states'] = ['' => ''] + State::pluck('name', 'id')->toArray();
        $data['employmentStatus'] = ['' => ''] + InputValue::employmentStatus()->orderBy('order_seq')->pluck('label', 'value')->toArray();
        $data['gradeLevel'] = ['' => ''] + InputValue::gradeLevel()->orderBy('order_seq')->pluck('label', 'value')->toArray();
        $data['relationshipToParent'] = ['' => ''] + InputValue::relationshipToParent()->orderBy('order_seq')->pluck('label', 'value')->toArray();
        $data['dropDownNumberTillFive'] = InputValue::getDropdownNumber(1, 5);
        $data['householdRole'] = InputValue::householdRole()->get()->toArray();
        $data['maritalStatus'] = InputValue::maritalStatus()->get()->toArray();
        $data['additionalIncomeSources'] = InputValue::additionalIncomeSources()->get()->toArray();

        return $data;
    }
    
    /**
     * Get landing page sub blade
     *
     * @return string
     */
    private function getLandingPageSubBlade()
    {
        // currently it will only return general as landing pages are different
        return '_general';
//        switch ($this->user()->source) {
//            case "ncsa-mvp":
//                return '_ncsa_mvp';
//            default:
//                return '_general';
//        }
    }
}
