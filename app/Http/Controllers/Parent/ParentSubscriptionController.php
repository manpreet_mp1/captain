<?php

namespace App\Http\Controllers\Parent;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\AuthFamilyTrait;
use App\Repositories\Family\SubscriptionRepository;
use Illuminate\Http\Request;
use function compact;
use function flash;
use function in_array;
use function redirect;
use function view;

class ParentSubscriptionController extends Controller
{
    use AuthFamilyTrait;

    /**
     * Show all subscription for a parent also page for upgrade
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data['family'] = $this->family();

        $data['pricing'] = (new SubscriptionRepository())->getPricing();

        $data['subscription'] = (new SubscriptionRepository())->getSubscription($data['family']);

        return view('parent.subscription.index', compact('data'));
    }

    /**
     * Show the checkout page
     *
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function checkout($slug)
    {
        $data['family'] = $this->family();

        if (!in_array($slug, ['smarttrack', 'smarttrack-advisor', 'bundle'])) {
            return redirect()->route('parent.subscriptions');
        }

        $data['product'] = (new SubscriptionRepository())->retrieveFixedProduct($slug);
        $data['slug'] = $slug;
        $data['coupon_code'] = $this->family()->coupon_code;
        $data['discount'] = 0;
        $data['dueToday'] = $data['product']->price;

        // has a coupon
        if ($data['coupon_code']) {
            $output = (new SubscriptionRepository())->applyCoupon($data['product']->price, $data['coupon_code']);
            $data['discount'] = $output['discount'];
            $data['dueToday'] = $output['dueToday'];
        }

        return view('parent.subscription.checkoutFixed', compact('data'));
    }

    /**
     * Apply a coupon code
     *
     * @param $slug
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function coupon($slug, Request $request)
    {
        $coupon = (new SubscriptionRepository())->validCouponCode($request->input('coupon_code'));

        if ($coupon) {
            $this->family()->update([
                'coupon_code' => $request->input('coupon_code')
            ]);
            flash('Coupon Applied.', 'toastr.info');

            return redirect()->route('parent.subscriptions.checkout', ['slug' => $slug]);
        } else {
            $this->family()->update([
                'coupon_code' => null,
            ]);
        }

        flash('Invalid Coupon.', 'toastr.error');

        return redirect()->route('parent.subscriptions.checkout', ['slug' => $slug]);
    }

    /**
     * Subscribe to fixed plan
     *
     * @param $slug
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function subscribe($slug, Request $request)
    {
        $response = (new SubscriptionRepository())->createAOrder($this->family(), $request->input('sku'), $request->input('stripe_token'));

        // success
        if ($response['success']) {
            $this->family()->update([
               'subscription' => $slug
            ]);

            return redirect()->route('parent.subscriptions.thankYou', ['slug' => $slug]);
        }

        flash($response['error'], 'toastr.error');

        return redirect()->route('parent.subscriptions.checkout', ['slug' => $slug]);
    }

    /**
     * Show thank you page
     *
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function thankYou($slug)
    {
        $data['family'] = $this->family();

        $data['product'] = (new SubscriptionRepository())->retrieveFixedProduct($slug);

        return view('parent.subscription.thankYou.' . $slug, compact('data'));
    }
}
