<?php

namespace App\Http\Controllers\Parent;

use App\Events\Parent\ParentRegisteredEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\Parent\ParentRegisterRequest;
use App\Models\Core\User;
use App\Models\Family\Family;
use App\Models\Family\FamilyParent;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use function array_merge;
use function compact;
use function config;
use function flash;

class ParentRegisterController extends Controller
{
    /**
     * Register landing page for parent
     *
     * @param Request $request
     * @return Factory|View
     */
    public function index(Request $request)
    {
        $data = $request->all();

        $advisor['status'] = false;

        if ($slug = $request->input('advisor')) {
            if ($advisorData = User::advisors()->findBySlug($slug)) {
                $advisor['status'] = true;
                $advisor['data'] = $advisorData;
            }
        }

        return view('parent.register.index', compact('advisor', 'data'));
    }

    /**
     * Register a parent and redirect to dashboard
     *
     * @param ParentRegisterRequest $request
     * @return RedirectResponse
     */
    public function register(ParentRegisterRequest $request)
    {
        // create a family
        $family = Family::create([
            'name' => $request->input('last_name'),
            'source' => $request->input('source'),
            'advisor_id' => $request->input('advisor_id'),
        ]);

        // if source is present and is mapped to coupon
        // add the coupon to database
        $source = $family->fresh()->source;
        if ($source && isset(config('services.stripe.sourceMapping')[$source])) {
            $family->update([
                'coupon_code' => config('services.stripe.sourceMapping')[$source]
            ]);
        }

        // create the user
        $userInputData =  $request->only([
            'first_name',
            'last_name',
            'mobile',
            'email',
            'password',
            'tmp_password',
            'role'
        ]);
        $user = User::create(array_merge($userInputData, [
            'family_id' => $family->id,
            'is_account' => 1,
        ]));

        // create a family parent
        FamilyParent::create([
            'user_id' => $user->id,
            'family_id' => $family->id,
        ]);

        // login the user
        Auth::login($user);

        // fire New User Registered Event
        event(new ParentRegisteredEvent($user));

        flash('Success.', 'toastr.info');

        // redirect to dashboard
        return redirect()->route('parent.dashboard');
    }
}
