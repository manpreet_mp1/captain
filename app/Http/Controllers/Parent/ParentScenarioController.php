<?php

namespace App\Http\Controllers\Parent;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\AuthFamilyTrait;
use App\Repositories\Family\FamilyQuickPlanRepository;
use App\Repositories\Family\FamilyRepository;
use App\Repositories\Premium\QpCalculatorRepository;
use function compact;
use function view;

class ParentScenarioController extends Controller
{
    use AuthFamilyTrait;

    /**
     * Show all shared scenarios for a family
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data['family'] = $this->family();

        $data['items'] = (new FamilyQuickPlanRepository())->getSharedScenarios($data['family']);

        return view('parent.scenarios.index', compact('data'));
    }

    /**
     * Show calculation summary for quick plan
     *
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($slug)
    {
        $data['family'] = $this->family();
        $data['scenario'] = (new FamilyQuickPlanRepository())->getScenario($data['family'], $slug);
        $data['result'] = (new QpCalculatorRepository())->getQpResultFromScenario($data['family'], $data['scenario']);
        $data['familyCostOfCollegeAndImpactOnRetirement'] = (new FamilyRepository())->getFamilyCostOfCollegeAndImpactOnRetirement($data['family']);
        $data['savingDropdown'] = (new FamilyQuickPlanRepository())->getSavingDropdown();
        $data['collegeYearDropdown'] = (new FamilyQuickPlanRepository())->getCollegeYearDropdown();
        $data['college'] = (new FamilyQuickPlanRepository())->getCollegeDropdownPopulatedValue($data['scenario']);
        $data['inputDisabled'] = 'disabled';

        return view('parent.scenarios.show', compact('data'));
    }
}
