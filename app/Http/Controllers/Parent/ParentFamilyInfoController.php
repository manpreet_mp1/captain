<?php

namespace App\Http\Controllers\Parent;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\AuthFamilyTrait;
use App\Http\Requests\Parent\GeneralParentRequest;
use App\Repositories\Family\FamilyChildrenRepository;
use App\Repositories\Family\FamilyParentRepository;
use App\Repositories\Family\FamilyRepository;
use function compact;
use function flash;
use function redirect;
use function view;

class ParentFamilyInfoController extends Controller
{
    use AuthFamilyTrait;

    /**
     * Show all the parents
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function parent()
    {
        $data['family'] = $this->family();

        return view('parent.familyInfo.parents.index', compact('data'));
    }

    /**
     * Show an edit form for a parent
     *
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function parentEdit($slug)
    {
        $data = (new FamilyParentRepository())->getEditParentView($this->family(), $slug);

        return view('parent.familyInfo.parents.edit', compact('data'));
    }

    /**
     * Update a parent
     *
     * @param $slug
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function parentUpdate($slug, GeneralParentRequest $request)
    {
        (new FamilyParentRepository())->updateParent($this->family(), $slug, $request);

        flash('Updated.', 'toastr.info');

        return redirect()->route('parent.familyInfo.parents.edit', ['slug' => $slug]);
    }

    /**
     * Add a new parent
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function parentAdd()
    {
        $data = (new FamilyParentRepository())->getEditParentView($this->family(), null);

        return view('parent.familyInfo.parents.add', compact('data'));
    }

    /**
     * Store a parent
     *
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function parentStore(GeneralParentRequest $request)
    {
        $parentTwoUser = (new FamilyParentRepository())->storeNewParent($this->family(), $request);

        flash('Added.', 'toastr.info');

        return redirect()->route('parent.familyInfo.parents.edit', ['slug' => $parentTwoUser->slug]);
    }

    /**
     * Show all the students
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function student()
    {
        $data['family'] = $this->family();

        return view('parent.familyInfo.students.index', compact('data'));
    }

    /**
     * Show an edit form for a parent
     *
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function studentEdit($slug)
    {
        $data = (new FamilyChildrenRepository())->getEditViewData($this->family(), $slug);

        return view('parent.familyInfo.students.edit', compact('data'));
    }

    /**
     * Update a parent
     *
     * @param $slug
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function studentUpdate($slug, GeneralParentRequest $request)
    {
        (new FamilyChildrenRepository())->updateChild($this->family(), $slug, $request);

        flash('Updated.', 'toastr.info');

        return redirect()->route('parent.familyInfo.students.edit', ['slug' => $slug]);
    }

    /**
     * Update a parent
     *
     * @param $slug
     * @return \Illuminate\Http\RedirectResponse
     */
    public function studentDelete($slug)
    {
        (new FamilyChildrenRepository())->deleteChild($slug);

        flash('Deleted.', 'toastr.info');

        return redirect()->route('parent.familyInfo.students');
    }

    /**
     * Add a new student
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function studentAdd()
    {
        $data = (new FamilyChildrenRepository())->getEditViewData($this->family(), null);

        return view('parent.familyInfo.students.add', compact('data'));
    }

    /**
     * Store a parent
     *
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function studentStore(GeneralParentRequest $request)
    {
        $user = (new FamilyChildrenRepository())->storeNewStudent($this->family(), $request);

        flash('Added.', 'toastr.info');

        return redirect()->route('parent.familyInfo.students.edit', ['slug' => $user->slug]);
    }

    /**
     * Show the breakdown view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function incomeInfo()
    {
        $data['family'] = $this->family();

        return view('parent.familyInfo.income.incomeInfo', compact('data'));
    }

    /**
     * Update the breakdown view
     *
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function incomeInfoUpdate(GeneralParentRequest $request)
    {
        (new FamilyRepository())->updateFamilyParentStudents($this->family(), $request, 'is_income_info_breakdown_completed');

        flash('Information Updated.', 'toastr.info');

        return redirect()->route('parent.familyInfo.incomeInfo');
    }

    /**
     * Show the breakdown view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function incomeTax()
    {
        $data['family'] = $this->family();

        return view('parent.familyInfo.income.incomeTax', compact('data'));
    }

    /**
     * Update the breakdown view
     *
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function incomeTaxUpdate(GeneralParentRequest $request)
    {
        (new FamilyRepository())->updateFamilyParentStudents($this->family(), $request, 'is_income_tax_breakdown_completed');

        flash('Information Updated.', 'toastr.info');

        return redirect()->route('parent.familyInfo.incomeTax');
    }

    /**
     * Show the breakdown view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function incomeAdditional()
    {
        $data['family'] = $this->family();

        return view('parent.familyInfo.income.incomeAdditional', compact('data'));
    }

    /**
     * Update the breakdown view
     *
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function incomeAdditionalUpdate(GeneralParentRequest $request)
    {
        (new FamilyRepository())->updateFamilyParentStudents($this->family(), $request, 'is_income_additional_breakdown_completed');

        flash('Information Updated.', 'toastr.info');

        return redirect()->route('parent.familyInfo.incomeAdditional');
    }

    /**
     * Show the breakdown view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function incomeUntaxed()
    {
        $data['family'] = $this->family();

        return view('parent.familyInfo.income.incomeUntaxed', compact('data'));
    }

    /**
     * Update the breakdown view
     *
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function incomeUntaxedUpdate(GeneralParentRequest $request)
    {
        (new FamilyRepository())->updateFamilyParentStudents($this->family(), $request, 'is_income_untaxed_breakdown_completed');

        flash('Information Updated.', 'toastr.info');

        return redirect()->route('parent.familyInfo.incomeUntaxed');
    }

    /**
     * Show the breakdown view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function assetsEducation()
    {
        $data['family'] = $this->family();

        return view('parent.familyInfo.assets.assetsEducation', compact('data'));
    }

    /**
     * Update the breakdown view
     *
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function assetsEducationUpdate(GeneralParentRequest $request)
    {
        (new FamilyRepository())->updateFamilyParentStudents($this->family(), $request, 'is_assets_eduction_breakdown_completed');

        flash('Information Updated.', 'toastr.info');

        return redirect()->route('parent.familyInfo.assetsEducation');
    }

    /**
     * Show the breakdown view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function assetsNonRetirement()
    {
        $data['family'] = $this->family();

        return view('parent.familyInfo.assets.assetsNonRetirement', compact('data'));
    }

    /**
     * Update the breakdown view
     *
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function assetsNonRetirementUpdate(GeneralParentRequest $request)
    {
        (new FamilyRepository())->updateFamilyParentStudents($this->family(), $request, 'is_assets_non_retirement_breakdown_completed');

        flash('Information Updated.', 'toastr.info');

        return redirect()->route('parent.familyInfo.assetsNonRetirement');
    }

    /**
     * Show the breakdown view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function assetsRetirement()
    {
        $data['family'] = $this->family();

        return view('parent.familyInfo.assets.assetsRetirement', compact('data'));
    }

    /**
     * Update the breakdown view
     *
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function assetsRetirementUpdate(GeneralParentRequest $request)
    {
        (new FamilyRepository())->updateFamilyParentStudents($this->family(), $request, 'is_assets_retirement_breakdown_completed');

        flash('Information Updated.', 'toastr.info');

        return redirect()->route('parent.familyInfo.assetsRetirement');
    }

    /**
     * Show the breakdown view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function realEstate()
    {
        $data['family'] = $this->family();

        return view('parent.familyInfo.other.realEstate', compact('data'));
    }

    /**
     * Update the breakdown view
     *
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function realEstateUpdate(GeneralParentRequest $request)
    {
        (new FamilyRepository())->updateFamilyParentStudents($this->family(), $request, 'is_real_estate_breakdown_completed');

        flash('Information Updated.', 'toastr.info');

        return redirect()->route('parent.familyInfo.realEstate');
    }

    /**
     * Show the breakdown view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function business()
    {
        $data['family'] = $this->family();

        return view('parent.familyInfo.other.business', compact('data'));
    }

    /**
     * Update the breakdown view
     *
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function businessUpdate(GeneralParentRequest $request)
    {
        (new FamilyRepository())->updateFamilyParentStudents($this->family(), $request, 'is_business_breakdown_completed');

        flash('Information Updated.', 'toastr.info');

        return redirect()->route('parent.familyInfo.business');
    }

    /**
     * Show the breakdown view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function insurance()
    {
        $data['family'] = $this->family();

        return view('parent.familyInfo.other.insurance', compact('data'));
    }

    /**
     * Update the breakdown view
     *
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function insuranceUpdate(GeneralParentRequest $request)
    {
        (new FamilyRepository())->updateFamilyParentStudents($this->family(), $request, 'is_insurance_breakdown_completed');

        flash('Information Updated.', 'toastr.info');

        return redirect()->route('parent.familyInfo.insurance');
    }
}
