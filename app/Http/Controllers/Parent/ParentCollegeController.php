<?php

namespace App\Http\Controllers\Parent;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\AuthFamilyTrait;
use App\Repositories\Resources\CollegeRepository;
use App\Repositories\Resources\MeritScholarshipsRepository;
use Barryvdh\Snappy\Facades\SnappyImage;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;
use function compact;
use function file_get_contents;
use function flash;
use function redirect;
use function view;

class ParentCollegeController extends Controller
{
    use AuthFamilyTrait;

    /**
     * Show the merit scholarship landing page
     *
     * @param Request $request
     * @return Factory|View
     */
    public function index(Request $request)
    {
        $data = (new CollegeRepository())->getViewData();

        $data['request'] = $request->all();

        $data['records'] = (new CollegeRepository())->getItems($request);

        return view('parent.resources.colleges.index', compact('data'));
    }

    /**
     * Show the merit scholarship for a school
     *
     * @param $slug
     * @return Factory|View
     */
    public function show($slug)
    {
        $data = (new CollegeRepository())->getViewData($slug);

        $data['record'] = (new CollegeRepository())->getItem($slug);

        $data['meritScholarships'] = (new MeritScholarshipsRepository())->getItem($data['record']->unitid);

        return view('parent.resources.colleges.show', compact('data'));
    }

    /**
     * Save a college to students
     *
     * @param $slug
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save($slug, Request $request)
    {
        (new CollegeRepository())->save($this->family(), $slug, $request->input('family_child_id'));

        flash('Success', 'toastr.info');

        return redirect()->route('parent.colleges.show', ['slug' => $slug]);
    }

//    public function generosity($slug)
//    {
//        $data = (new CollegeRepository())->getViewData();
//
//        $data['record'] = (new CollegeRepository())->getItem($slug);
//
//        return view('elements.resources.colleges.generosity_image', compact('data'));
//    }
//
//    public function generosityImage($slug)
//    {
//        $data = (new CollegeRepository())->getViewData();
//
//        $data['record'] = (new CollegeRepository())->getItem($slug);
//
//        $snappyImage = SnappyImage::setOptions([
//            'enable-javascript' => '--images',
//            'javascript-delay' => '2000'
//        ]);
//
//        // return $snappyImage->loadView('elements.resources.colleges.generosity', ['data' => $data])->inline();
//
//        return $snappyImage->loadHTML(file_get_contents('http://app.smarttrackcollegefunding.test/nut/index.html'))->inline();
//    }
}
