<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;
use function optional;

trait AuthFamilyTrait
{
    /**
     * Get the user
     *
     * @return Authenticatable|null
     */
    public function user()
    {
        return Auth::user();
    }

    /**
     * Get the second user
     *
     * @return Authenticatable|null
     */
    public function userTwo()
    {
        return Auth::user()->userTwo();
    }

    /**
     * Get the family
     *
     * @return mixed
     */
    public function parent()
    {
        return Auth::user()->parent;
    }

    /**
     * Get the family
     *
     * @return mixed
     */
    public function parentTwo()
    {
        return optional(Auth::user()->userTwo())->parent;
    }

    /**
     * Get the family
     *
     * @return mixed
     */
    public function family()
    {
        $family = Auth::user()->family;

        $family->load(['children', 'parents', 'children.user', 'parents.user']);

        return $family;
    }

    /**
     * Get the advisor
     *
     * @return mixed
     */
    public function advisor()
    {
        // @todo - update me
        return Auth::user()->family;
    }

    /**
     * Get the family parent
     *
     * @return mixed
     * @deprecated  - use load relationship
     */
    public function familyParents()
    {
        return Auth::user()->family->parents;
    }

    /**
     * Get the family children
     *
     * @return mixed
     * @deprecated  - use load relationship
     */
    public function familyChildren()
    {
        return Auth::user()->family->children;
    }
}
