<?php

namespace App\Http\Controllers\Traits;

use App\Models\Family\Family;
use Illuminate\Support\Facades\Auth;

trait AuthAdvisorTrait
{
    /**
     * Get all cases
     *
     * @return mixed
     */
    public function cases()
    {
        return Family::where('advisor_id', Auth::user()->id)->get();
    }

    /**
     * Get the family
     *
     * @param $slug
     * @return Family|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function family($slug)
    {
        return Family::where('advisor_id', Auth::user()->id)
            ->where('slug', $slug)
            ->first();
    }

    /**
     * Get the family parent
     *
     * @param $slug
     * @return mixed
     */
    public function familyParents($slug)
    {
        return $this->family($slug)->parents;
    }

    /**
     * Get the family children
     *
     * @param $slug
     * @return mixed
     */
    public function familyChildren($slug)
    {
        return $this->family($slug)->children;
    }
}
