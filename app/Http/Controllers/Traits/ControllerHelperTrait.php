<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Support\Facades\Auth;

trait ControllerHelperTrait
{
    /**
     * Get template name for user
     *
     * @return string
     */
    public function getViewTemplateForUser()
    {
        switch (true) {
            case Auth::user()->is_admin === 1:
                return "fullwith-admin-default";

            case Auth::user()->is_advisor === 1:
                return "fullwith-advisor-default";

            case Auth::user()->is_parent === 1:
                return "fullwith-parent-default";
        }
    }
}
