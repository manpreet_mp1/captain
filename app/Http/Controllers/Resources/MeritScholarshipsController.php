<?php

namespace App\Http\Controllers\Resources;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\AuthFamilyTrait;
use App\Http\Controllers\Traits\ControllerHelperTrait;
use App\Models\Data\Scholarship\ScholarshipMerit;
use App\Models\Dropdown\State;
use function compact;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;
use function view;

class MeritScholarshipsController extends Controller
{
    use AuthFamilyTrait;
    use ControllerHelperTrait;

    /**
     * Show the merit scholarship landing page
     *
     * @param Request $request
     * @return Factory|View
     */
    public function index(Request $request)
    {
        $data = $this->getViewData();

        $data['request'] = $request->all();

        $data['records'] = ScholarshipMerit::search($request)
            ->selectRaw('
                ipeds,
                school_name,
                COUNT(id) AS scholarship_count
            ')
            ->groupBy('ipeds', 'school_name')
            ->orderBy('school_name')
            ->get();

        return view('resources.scholarship.merit.index', compact('data'));
    }

    /**
     * Show the merit scholarship for a school
     *
     * @param $ipeds
     * @return Factory|View
     */
    public function show($ipeds)
    {
        $data = $this->getViewData();

        $data['records'] = ScholarshipMerit::where('ipeds', $ipeds)->get();

        return view('resources.scholarship.merit.show', compact('data'));
    }

    /**
     * Get the required view data
     *
     * @return mixed
     */
    private function getViewData()
    {
        $output['viewTemplateForUser'] = $this->getViewTemplateForUser();
        $output['stateList'] = ['' => ''] + State::pluck('name', 'abbreviation')->toArray();
        $output['typeList'] = [
            '' => '',
            'Automatic' => 'Automatic',
            'Competitive' => 'Competitive',
            'Talent' => 'Talent',
            'National Merit/Achievement' => 'National Merit/Achievement',
        ];

        return $output;
    }
}
