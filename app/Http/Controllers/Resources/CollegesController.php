<?php

namespace App\Http\Controllers\Resources;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\AuthFamilyTrait;
use App\Http\Controllers\Traits\ControllerHelperTrait;
use function compact;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use function view;

class CollegesController extends Controller
{
    use AuthFamilyTrait;
    use ControllerHelperTrait;

    /**
     * for creating the college template
     *
     * @return Factory|View
     */
    public function test()
    {
        $data['viewTemplateForUser'] = $this->getViewTemplateForUser();

        return view('resources.colleges.show', compact('data'));
    }
}
