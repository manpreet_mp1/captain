<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Data\College\College;
use Illuminate\Http\Request;

class LiteController extends Controller
{
    /**
     * Get colleges
     *
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function colleges(Request $request)
    {
        return College::where('inst_nm_city', 'like', '%'.$request->input('term').'%')
            ->orderBy('name')
            ->get([
                'unitid as id',
                'inst_nm_city as text'
            ]);
    }
}
