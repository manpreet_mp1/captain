<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Core\User;
use App\Models\Family\Family;
use App\Services\AcuityService;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use function config;
use function response;

class AcuityController extends Controller
{
    /**
     * Get available dates for F1 appointment type
     *
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function availabilityDates(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'month' => 'required',
            'timezone' => 'required',
            'appointmentTypeID' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "error" => 'validation_error',
                "message" => $validator->errors(),
            ], 422);
        }

        $dates = (new AcuityService())->getAvailabilityDatesForAppointmentType(
            $month = $request->input('month'),
            $appointmentTypeID = $request->input('appointmentTypeID'),
            $timezone = $request->input('timezone')
        );

        return [
            'dates' => Arr::flatten($dates)
        ];
    }

    /**
     * Get available dates for F1 appointment type
     *
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function availabilityTimes(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'appointmentTypeID' => 'required',
            'date' => 'required|date',
            'timezone' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "error" => 'validation_error',
                "message" => $validator->errors(),
            ], 422);
        }

        $times = (new AcuityService())->getAvailabilityTimesForAppointmentType(
            $date = Carbon::parse($request->input('date'))->toDateString(),
            $appointmentTypeID = $request->input('appointmentTypeID'),
            $timezone = $request->input('timezone')
        );

        $returnTime = [];

        foreach ($times as $time) {
            $returnTime[] = Carbon::parse($time['time'])->format('h:i A');
        }

        return [
            'times' => $returnTime
        ];
    }

    /**
     * Get F1 appointment Id
     *
     * @return array
     */
    public function f1AppointmentId()
    {
        return [
            'id' => config('services.acuity.appointmentTypes.f1StrategyAppointmentId')
        ];
    }

    /**
     * Create an appointment
     *
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function appointment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'appointmentTypeID' => 'required',
            'date' => 'required|date',
            'time' => 'required',
            'familyId' => 'required',
            'timezone' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "error" => 'validation_error',
                "message" => $validator->errors(),
            ], 422);
        }

        $dateTime = Carbon::parse($request->input('date') . $request->input('time'), $request->input('timezone'))
            ->setTimezone(config('services.acuity.businessTimeZone'))
            ->toIso8601String();
        $appointmentTypeID = $request->input('appointmentTypeID');
        $timezone = $request->input('timezone');
        $user = Family::find($request->input('familyId'))->parentOne()->user;

        $appointment = (new AcuityService())->createAnAppointment($dateTime, $appointmentTypeID, $timezone, $user);

        $clientDateTime = Carbon::parse($appointment['datetime'])->setTimezone($appointment['timezone']);
        $clientAppointment = [
            'name' => $appointment['type'],
            'timezone' => $appointment['timezone'],
            'date' => $clientDateTime->format('l, F d, Y'),
            'start_time' => $clientDateTime->format('h:i A'),
            'end_time' => $clientDateTime->addMinutes((int)$appointment['duration'])->format('h:i A'),
        ];

        return [
            'appointment' => $appointment,
            'clientAppointment' => $clientAppointment,
            'success' => true
        ];
    }

    /**
     * Assign a family to advisor
     *
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function assignAdvisor(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'familyId' => 'required',
            'calendarId' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "error" => 'validation_error',
                "message" => $validator->errors(),
            ], 422);
        }

        $family = Family::find($request->input('familyId'));
        $advisor = User::where('acuity_calendar_id', $request->input('calendarId'))->first();

        $family->update([
            'advisor_id' => $advisor->id
        ]);

        return [
            'advisor' => $advisor,
            'success' => true
        ];
    }
}
