<?php

namespace App\Http\Controllers\Advisor;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\AuthAdvisorTrait;
use App\Repositories\Resources\CollegeRepository;
use App\Repositories\Resources\MeritScholarshipsRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;
use function compact;
use function flash;
use function redirect;
use function view;

class AdvisorCollegeController extends Controller
{
    use AuthAdvisorTrait;

    /**
     * Show the merit scholarship landing page
     *
     * @param $caseSlug
     * @param Request $request
     * @return Factory|View
     */
    public function index($caseSlug, Request $request)
    {
        $data = (new CollegeRepository())->getViewData(null, $caseSlug);

        $data['request'] = $request->all();

        $data['records'] = (new CollegeRepository())->getItems($request);

        return view('advisor.resources.colleges.index', compact('data'));
    }

    /**
     * Show the merit scholarship for a school
     *
     * @param $caseSlug
     * @param $slug
     * @return Factory|View
     */
    public function show($caseSlug, $slug)
    {
        $data = (new CollegeRepository())->getViewData($slug, $caseSlug);

        $data['record'] = (new CollegeRepository())->getItem($slug);

        $data['meritScholarships'] = (new MeritScholarshipsRepository())->getItem($data['record']->unitid);

        return view('advisor.resources.colleges.show', compact('data'));
    }

    /**
     * Save a college to students
     *
     * @param $caseSlug
     * @param $slug
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save($caseSlug, $slug, Request $request)
    {
        (new CollegeRepository())->save($this->family($caseSlug), $slug, $request->input('family_child_id'));

        flash('Success', 'toastr.info');

        return redirect()->route('advisor.case.colleges.show', ['slug' => $slug, 'caseSlug' => $caseSlug]);
    }
}
