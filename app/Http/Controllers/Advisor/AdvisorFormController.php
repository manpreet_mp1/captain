<?php

namespace App\Http\Controllers\Advisor;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\AuthAdvisorTrait;
use App\Repositories\Resources\CollegeRepository;
use App\Repositories\Resources\MeritScholarshipsRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;
use function compact;
use function flash;
use function redirect;
use function view;

class AdvisorFormController extends Controller
{
    public function index(Request $request)
    {
        return view('advisor.resources.1040-form.index');
    }
}
