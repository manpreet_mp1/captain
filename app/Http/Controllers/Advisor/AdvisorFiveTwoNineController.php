<?php

namespace App\Http\Controllers\Advisor;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\AuthAdvisorTrait;
use App\Repositories\Resources\FiveTwoNineRepository;
use Illuminate\Http\Request;
use function compact;
use function view;

class AdvisorFiveTwoNineController extends Controller
{
    use AuthAdvisorTrait;

    /**
     * Show a listing of resources
     *
     * @param $caseSlug
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($caseSlug, Request $request)
    {
        $data['request'] = $request->all();

        $data['records'] = (new FiveTwoNineRepository())->getItems($request);

        $data['view'] = (new FiveTwoNineRepository())->getViewData();

        return view('advisor.resources.fiveTwoNine.index', compact('data'));
    }

    /**
     * Show a resource
     *
     * @param $caseSlug
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($caseSlug, $slug)
    {
        $data['record'] = (new FiveTwoNineRepository())->getItem($slug);

        return view('advisor.resources.fiveTwoNine.show', compact('data'));
    }

    /**
     * Show common questions
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function commonQuestions()
    {
        return view('advisor.resources.fiveTwoNine.commonQuestions');
    }

    /**
     * Compare Plans
     *
     * @param $caseSlug
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function compare($caseSlug, Request $request)
    {
        $data['request'] = $request->all();

        $data['plansDropdown'] = (new FiveTwoNineRepository())->getPlansDropdown();

        $data['plans'] = (new FiveTwoNineRepository())->getPlans($request);

        return view('advisor.resources.fiveTwoNine.compare', compact('data'));
    }
}
