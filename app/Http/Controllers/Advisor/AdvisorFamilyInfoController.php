<?php

namespace App\Http\Controllers\Advisor;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\AuthAdvisorTrait;
use App\Http\Requests\Parent\GeneralParentRequest;
use App\Repositories\Family\FamilyChildrenRepository;
use App\Repositories\Family\FamilyParentRepository;
use App\Repositories\Family\FamilyRepository;
use function compact;
use function flash;
use function redirect;
use function view;

class AdvisorFamilyInfoController extends Controller
{
    use AuthAdvisorTrait;

    /**
     * Show all the parents
     *
     * @param $caseSlug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function parent($caseSlug)
    {
        $data['family'] = $this->family($caseSlug);

        return view('advisor.case.familyInfo.parents.index', compact('data'));
    }

    /**
     * Show an edit form for a parent
     *
     * @param $caseSlug
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function parentEdit($caseSlug, $slug)
    {
        $data = (new FamilyParentRepository())->getEditParentView($this->family($caseSlug), $slug);

        return view('advisor.case.familyInfo.parents.edit', compact('data'));
    }

    /**
     * Update a parent
     *
     * @param $caseSlug
     * @param $slug
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function parentUpdate($caseSlug, $slug, GeneralParentRequest $request)
    {
        (new FamilyParentRepository())->updateParent($this->family($caseSlug), $slug, $request);

        flash('Updated.', 'toastr.info');

        return redirect()->route('advisor.case.familyInfo.parents.edit', ['slug' => $slug, 'caseSlug' => $caseSlug]);
    }

    /**
     * Add a new parent
     *
     * @param $caseSlug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function parentAdd($caseSlug)
    {
        $data = (new FamilyParentRepository())->getEditParentView($this->family($caseSlug), null);

        return view('advisor.case.familyInfo.parents.add', compact('data'));
    }

    /**
     * Store a parent
     *
     * @param $caseSlug
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function parentStore($caseSlug, GeneralParentRequest $request)
    {
        $parentTwoUser = (new FamilyParentRepository())->storeNewParent($this->family($caseSlug), $request);

        flash('Added.', 'toastr.info');

        return redirect()->route('advisor.case.familyInfo.parents.edit', ['slug' => $parentTwoUser->slug, 'caseSlug' => $caseSlug]);
    }

    /**
     * Show all the students
     *
     * @param $caseSlug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function student($caseSlug)
    {
        $data['family'] = $this->family($caseSlug);

        return view('advisor.case.familyInfo.students.index', compact('data'));
    }

    /**
     * Show an edit form for a parent
     *
     * @param $caseSlug
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function studentEdit($caseSlug, $slug)
    {
        $data = (new FamilyChildrenRepository())->getEditViewData($this->family($caseSlug), $slug);

        return view('advisor.case.familyInfo.students.edit', compact('data'));
    }

    /**
     * Update a parent
     *
     * @param $caseSlug
     * @param $slug
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function studentUpdate($caseSlug, $slug, GeneralParentRequest $request)
    {
        (new FamilyChildrenRepository())->updateChild($this->family($caseSlug), $slug, $request);

        flash('Updated.', 'toastr.info');

        return redirect()->route('advisor.case.familyInfo.students.edit', ['slug' => $slug, 'caseSlug' => $caseSlug]);
    }

    /**
     * Update a parent
     *
     * @param $caseSlug
     * @param $slug
     * @return \Illuminate\Http\RedirectResponse
     */
    public function studentDelete($caseSlug, $slug)
    {
        (new FamilyChildrenRepository())->deleteChild($slug);

        flash('Deleted.', 'toastr.info');

        return redirect()->route('advisor.case.familyInfo.students', ['caseSlug' => $caseSlug]);
    }

    /**
     * Add a new student
     *
     * @param $caseSlug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function studentAdd($caseSlug)
    {
        $data = (new FamilyChildrenRepository())->getEditViewData($this->family($caseSlug), null);

        return view('advisor.case.familyInfo.students.add', compact('data'));
    }

    /**
     * Store a parent
     *
     * @param $caseSlug
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function studentStore($caseSlug, GeneralParentRequest $request)
    {
        $user = (new FamilyChildrenRepository())->storeNewStudent($this->family($caseSlug), $request);

        flash('Added.', 'toastr.info');

        return redirect()->route('advisor.case.familyInfo.students.edit', ['slug' => $user->slug, 'caseSlug' => $caseSlug]);
    }

    /**
     * Show the breakdown view
     *
     * @param $caseSlug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function incomeInfo($caseSlug)
    {
        $data['family'] = $this->family($caseSlug);

        return view('advisor.case.familyInfo.income.incomeInfo', compact('data'));
    }

    /**
     * Update the breakdown view
     *
     * @param $caseSlug
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function incomeInfoUpdate($caseSlug, GeneralParentRequest $request)
    {
        (new FamilyRepository())->updateFamilyParentStudents($this->family($caseSlug), $request, 'is_income_info_breakdown_completed');

        flash('Information Updated.', 'toastr.info');

        return redirect()->route('advisor.case.familyInfo.incomeInfo', ['caseSlug' => $caseSlug]);
    }

    /**
     * Show the breakdown view
     *
     * @param $caseSlug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function incomeTax($caseSlug)
    {
        $data['family'] = $this->family($caseSlug);

        return view('advisor.case.familyInfo.income.incomeTax', compact('data'));
    }

    /**
     * Update the breakdown view
     *
     * @param $caseSlug
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function incomeTaxUpdate($caseSlug, GeneralParentRequest $request)
    {
        (new FamilyRepository())->updateFamilyParentStudents($this->family($caseSlug), $request, 'is_income_tax_breakdown_completed');

        flash('Information Updated.', 'toastr.info');

        return redirect()->route('advisor.case.familyInfo.incomeTax', ['caseSlug' => $caseSlug]);
    }

    /**
     * Show the breakdown view
     *
     * @param $caseSlug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function incomeAdditional($caseSlug)
    {
        $data['family'] = $this->family($caseSlug);

        return view('advisor.case.familyInfo.income.incomeAdditional', compact('data'));
    }

    /**
     * Update the breakdown view
     *
     * @param $caseSlug
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function incomeAdditionalUpdate($caseSlug, GeneralParentRequest $request)
    {
        (new FamilyRepository())->updateFamilyParentStudents($this->family($caseSlug), $request, 'is_income_additional_breakdown_completed');

        flash('Information Updated.', 'toastr.info');

        return redirect()->route('advisor.case.familyInfo.incomeAdditional', ['caseSlug' => $caseSlug]);
    }

    /**
     * Show the breakdown view
     *
     * @param $caseSlug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function incomeUntaxed($caseSlug)
    {
        $data['family'] = $this->family($caseSlug);

        return view('advisor.case.familyInfo.income.incomeUntaxed', compact('data'));
    }

    /**
     * Update the breakdown view
     *
     * @param $caseSlug
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function incomeUntaxedUpdate($caseSlug, GeneralParentRequest $request)
    {
        (new FamilyRepository())->updateFamilyParentStudents($this->family($caseSlug), $request, 'is_income_untaxed_breakdown_completed');

        flash('Information Updated.', 'toastr.info');

        return redirect()->route('advisor.case.familyInfo.incomeUntaxed', ['caseSlug' => $caseSlug]);
    }

    /**
     * Show the breakdown view
     *
     * @param $caseSlug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function assetsEducation($caseSlug)
    {
        $data['family'] = $this->family($caseSlug);

        return view('advisor.case.familyInfo.assets.assetsEducation', compact('data'));
    }

    /**
     * Update the breakdown view
     *
     * @param $caseSlug
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function assetsEducationUpdate($caseSlug, GeneralParentRequest $request)
    {
        (new FamilyRepository())->updateFamilyParentStudents($this->family($caseSlug), $request, 'is_assets_eduction_breakdown_completed');

        flash('Information Updated.', 'toastr.info');

        return redirect()->route('advisor.case.familyInfo.assetsEducation', ['caseSlug' => $caseSlug]);
    }

    /**
     * Show the breakdown view
     *
     * @param $caseSlug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function assetsNonRetirement($caseSlug)
    {
        $data['family'] = $this->family($caseSlug);

        return view('advisor.case.familyInfo.assets.assetsNonRetirement', compact('data'));
    }

    /**
     * Update the breakdown view
     *
     * @param $caseSlug
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function assetsNonRetirementUpdate($caseSlug, GeneralParentRequest $request)
    {
        (new FamilyRepository())->updateFamilyParentStudents($this->family($caseSlug), $request, 'is_assets_non_retirement_breakdown_completed');

        flash('Information Updated.', 'toastr.info');

        return redirect()->route('advisor.case.familyInfo.assetsNonRetirement', ['caseSlug' => $caseSlug]);
    }

    /**
     * Show the breakdown view
     *
     * @param $caseSlug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function assetsRetirement($caseSlug)
    {
        $data['family'] = $this->family($caseSlug);

        return view('advisor.case.familyInfo.assets.assetsRetirement', compact('data'));
    }

    /**
     * Update the breakdown view
     *
     * @param $caseSlug
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function assetsRetirementUpdate($caseSlug, GeneralParentRequest $request)
    {
        (new FamilyRepository())->updateFamilyParentStudents($this->family($caseSlug), $request, 'is_assets_retirement_breakdown_completed');

        flash('Information Updated.', 'toastr.info');

        return redirect()->route('advisor.case.familyInfo.assetsRetirement', ['caseSlug' => $caseSlug]);
    }

    /**
     * Show the breakdown view
     *
     * @param $caseSlug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function realEstate($caseSlug)
    {
        $data['family'] = $this->family($caseSlug);

        return view('advisor.case.familyInfo.other.realEstate', compact('data'));
    }

    /**
     * Update the breakdown view
     *
     * @param $caseSlug
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function realEstateUpdate($caseSlug, GeneralParentRequest $request)
    {
        (new FamilyRepository())->updateFamilyParentStudents($this->family($caseSlug), $request, 'is_real_estate_breakdown_completed');

        flash('Information Updated.', 'toastr.info');

        return redirect()->route('advisor.case.familyInfo.realEstate', ['caseSlug' => $caseSlug]);
    }

    /**
     * Show the breakdown view
     *
     * @param $caseSlug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function business($caseSlug)
    {
        $data['family'] = $this->family($caseSlug);

        return view('advisor.case.familyInfo.other.business', compact('data'));
    }

    /**
     * Update the breakdown view
     *
     * @param $caseSlug
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function businessUpdate($caseSlug, GeneralParentRequest $request)
    {
        (new FamilyRepository())->updateFamilyParentStudents($this->family($caseSlug), $request, 'is_business_breakdown_completed');

        flash('Information Updated.', 'toastr.info');

        return redirect()->route('advisor.case.familyInfo.business', ['caseSlug' => $caseSlug]);
    }

    /**
     * Show the breakdown view
     *
     * @param $caseSlug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function insurance($caseSlug)
    {
        $data['family'] = $this->family($caseSlug);

        return view('advisor.case.familyInfo.other.insurance', compact('data'));
    }

    /**
     * Update the breakdown view
     *
     * @param $caseSlug
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function insuranceUpdate($caseSlug, GeneralParentRequest $request)
    {
        (new FamilyRepository())->updateFamilyParentStudents($this->family($caseSlug), $request, 'is_insurance_breakdown_completed');

        flash('Information Updated.', 'toastr.info');

        return redirect()->route('advisor.case.familyInfo.insurance', ['caseSlug' => $caseSlug]);
    }
}
