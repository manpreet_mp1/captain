<?php

namespace App\Http\Controllers\Advisor;

use App\Http\Controllers\Controller;
use App\Models\Wordpress\Category;
use App\Models\Wordpress\Post;
use function compact;
use function config;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\View\View;
use function is_array;
use function view;

class AdvisorClientStrategiesController extends Controller
{
    /**
     * Show a listing of strategies
     *
     * @param Request $request
     * @return Factory|View
     */
    public function strategies($caseSlug, Request $request)
    {
        $data['subcategories'] = [];

        // get all the categories from config
        $data['categories'] = config('corcel.strategies.categories');

        // get all the subcategories for each category
        foreach ($data['categories'] as $name => $slug) {
            $data['category'][$slug]['sub'] = Category::slug($slug)->first()->subCategories();
            // get all the subcategories as flat array
            $data['subcategories'] = array_merge($data['subcategories'], Arr::flatten(Category::slug($slug)->first()->subCategoriesSlug()));
        }

        // set the search Param
        $data['request'] = $request->all();

        // get all the posts
        $data['records'] = Post::type('strategy')
            ->published()
            ->paginate(10);
        
        // request has search
        if ($request->has('search') && $request->input('search') === 'search') {
            $recordsQueryBuilder = Post::type('strategy')->published();
            foreach ($request->all() as $main => $slug) {
                // only filter if the output is array
                if (is_array($slug)) {
                    $recordsQueryBuilder = $recordsQueryBuilder->catSlug($slug);
                }
            }

            $data['records'] = $recordsQueryBuilder->paginate(10);
        }

        return view('advisor.resources.clientStrategies.strategies.index', compact('data'));
    }

    /**
     * Show a strategy
     *
     * @param $slug
     * @return Factory|View
     */
    public function strategiesShow($caseSlug, $slug)
    {
        $data['record'] = Post::type('strategy')->slug($slug)->firstOrFail();

        return view('advisor.resources.clientStrategies.strategies.show', compact('data'));
    }

    /**
     * Show the tax talking point form
     *
     * @return Factory|View
     */
    public function taxTalkingPoints()
    {
        return view('advisor.resources.clientStrategies.taxTalkingPoints.index');
    }

    /**
     * Show all tax talking points for a particular line number
     *
     * @param $slug
     * @return Factory|View
     */
    public function taxTalkingPointsShow($caseSlug, $slug)
    {
        $data['records'] = Post::published()->taxonomy('category', $slug)->paginate(10);
        $data['category'] = Category::slug($slug)->first();

        return view('advisor.resources.clientStrategies.taxTalkingPoints.show', compact('data'));
    }
}
