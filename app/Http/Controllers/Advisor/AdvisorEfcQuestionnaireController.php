<?php

namespace App\Http\Controllers\Advisor;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\AuthAdvisorTrait;
use App\Http\Controllers\Traits\AuthFamilyTrait;
use App\Http\Requests\Parent\GeneralParentRequest;
use App\Repositories\Premium\EfcQuestionnaireRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use function compact;
use function redirect;
use function view;

class AdvisorEfcQuestionnaireController extends Controller
{
    use AuthAdvisorTrait;

    /**
     * Get the free assessment
     *
     * @param $caseSlug
     * @param $slug
     * @param Request $request
     * @return Factory|RedirectResponse|View
     */
    public function index($caseSlug, $slug, Request $request)
    {
        $firstEfcStepBladeName = (new EfcQuestionnaireRepository())->getFirstBladeName();

        // if slug is not set, get the current step user is on and redirect
        if ($slug === "0") {
            return redirect()->route('advisor.case.questionnaires.efc', [
                'slug' => $firstEfcStepBladeName,
                'caseSlug' => $caseSlug,
            ]);
        }

        // get the required data
        $data = (new EfcQuestionnaireRepository())->getViewData($this->family($caseSlug), $slug, $request);

        return view("advisor.case.questionnaires.efc.{$slug}", compact('data'));
    }

    /**
     * Store the efc data
     *
     * @param $caseSlug
     * @param $slug
     * @param GeneralParentRequest $request
     * @return RedirectResponse
     */
    public function store($caseSlug, $slug, GeneralParentRequest $request)
    {
        // save and get the next form request
        $nextFormBlade = (new EfcQuestionnaireRepository())->store($this->family($caseSlug), $slug, $request);
        
        // all questionnaire completed, show result
        if (!$nextFormBlade) {
            $this->family($caseSlug)->update([
                'is_completed_efc' => 1
            ]);
            // @todo - fire event to send slack notification

            return redirect()->route('advisor.case.efc.result', [
                'caseSlug'=> $caseSlug
            ]);
        }

        return redirect()->route('advisor.case.questionnaires.efc', [
            'slug' => $nextFormBlade->blade_name,
            'caseSlug'=> $caseSlug
        ]);
    }
}
