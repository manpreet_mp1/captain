<?php

namespace App\Http\Controllers\Advisor;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\AuthAdvisorTrait;
use App\Http\Requests\Parent\GeneralParentRequest;
use App\Repositories\Family\FamilyQuickPlanRepository;
use App\Repositories\Family\FamilyRepository;
use App\Repositories\Premium\QpCalculatorRepository;
use function flash;
use Illuminate\Http\Request;
use function compact;
use function redirect;
use function view;

class AdvisorQuickPlanController extends Controller
{
    use AuthAdvisorTrait;

    /**
     * Get the listing of resources
     *
     * @param $caseSlug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index($caseSlug)
    {
        // if efc is not completed, need to complete efc first
        if ($this->family($caseSlug)->is_completed_efc != 1) {
            return redirect()->route('advisor.case.questionnaires.efc', [
                'caseSlug' => $caseSlug,
                'slug'=> '0'
            ]);
        }

        $data['items'] = (new FamilyQuickPlanRepository())->getScenarios($this->family($caseSlug));

        $data['students'] = (new FamilyQuickPlanRepository())->getStudentDropDown($this->family($caseSlug));

        return view('advisor.case.quickPlan.index', compact('data'));
    }

    /**
     * Create a new scenario and redirect to the scenario
     *
     * @param $caseSlug
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store($caseSlug, Request $request)
    {
        $item = (new FamilyQuickPlanRepository())->createScenarios($this->family($caseSlug), $request);

        return redirect()->route('advisor.case.quickPlan.show', [
            'caseSlug' => $caseSlug,
            'slug' => $item->slug
        ]);
    }

    /**
     * Show a scenario
     *
     * @param $caseSlug
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($caseSlug, $slug)
    {
        $data['family'] = $this->family($caseSlug);
        $data['scenario'] = (new FamilyQuickPlanRepository())->getScenario($data['family'], $slug);
        $data['result'] = (new QpCalculatorRepository())->getQpResultFromScenario($data['family'], $data['scenario']);
        $data['familyCostOfCollegeAndImpactOnRetirement'] = (new FamilyRepository())->getFamilyCostOfCollegeAndImpactOnRetirement($data['family']);
        $data['savingDropdown'] = (new FamilyQuickPlanRepository())->getSavingDropdown();
        $data['collegeYearDropdown'] = (new FamilyQuickPlanRepository())->getCollegeYearDropdown();
        $data['college'] = (new FamilyQuickPlanRepository())->getCollegeDropdownPopulatedValue($data['scenario']);
        $data['inputDisabled'] = "";

        return view('advisor.case.quickPlan.show', compact('data'));
    }

    /**
     * Show a scenario
     *
     * @param $caseSlug
     * @param $slug
     * @param GeneralParentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($caseSlug, $slug, GeneralParentRequest $request)
    {
        $data['scenario'] = (new FamilyQuickPlanRepository())->getScenario($this->family($caseSlug), $slug);

        (new FamilyQuickPlanRepository())->updateScenario($data['scenario'], $request);

        flash('Save Successfully.', 'toastr.info');

        return redirect()->route('advisor.case.quickPlan.show', [
            'caseSlug' => $caseSlug,
            'slug' => $slug
        ]);
    }
}
