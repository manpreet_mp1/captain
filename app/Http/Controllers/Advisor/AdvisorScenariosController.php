<?php

namespace App\Http\Controllers\Advisor;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\AuthAdvisorTrait;
use App\Repositories\Family\FamilyQuickPlanRepository;
use App\Repositories\Family\FamilyRepository;
use App\Repositories\Premium\QpCalculatorRepository;
use function compact;
use function view;

class AdvisorScenariosController extends Controller
{
    use AuthAdvisorTrait;

    /**
     * Show all shared scenarios for a family
     *
     * @param $caseSlug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($caseSlug)
    {
        $data['family'] = $this->family($caseSlug);

        $data['items'] = (new FamilyQuickPlanRepository())->getSharedScenarios($data['family']);

        return view('advisor.case.scenarios.index', compact('data'));
    }

    /**
     * Show calculation summary for quick plan
     *
     * @param $caseSlug
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($caseSlug, $slug)
    {
        $data['family'] = $this->family($caseSlug);
        $data['scenario'] = (new FamilyQuickPlanRepository())->getScenario($data['family'], $slug);
        $data['result'] = (new QpCalculatorRepository())->getQpResultFromScenario($data['family'], $data['scenario']);
        $data['familyCostOfCollegeAndImpactOnRetirement'] = (new FamilyRepository())->getFamilyCostOfCollegeAndImpactOnRetirement($data['family']);
        $data['savingDropdown'] = (new FamilyQuickPlanRepository())->getSavingDropdown();
        $data['collegeYearDropdown'] = (new FamilyQuickPlanRepository())->getCollegeYearDropdown();
        $data['college'] = (new FamilyQuickPlanRepository())->getCollegeDropdownPopulatedValue($data['scenario']);
        $data['inputDisabled'] = 'disabled';

        return view('advisor.case.scenarios.show', compact('data'));
    }
}
