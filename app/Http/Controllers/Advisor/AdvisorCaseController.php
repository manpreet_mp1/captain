<?php

namespace App\Http\Controllers\Advisor;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\AuthAdvisorTrait;
use App\Models\Dropdown\InputValue;
use App\Models\Family\Family;
use App\Repositories\Family\FamilyRepository;
use App\Repositories\Premium\EfcCalculatorRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use function compact;
use function view;

class AdvisorCaseController extends Controller
{
    use AuthAdvisorTrait;

    /**
     * Show the advisor dashboard
     *
     * @return Factory|View
     */
    public function index()
    {
        $data['cases'] = $this->cases();

        // return view('advisor.dashboard.index', compact('data'));
    }

    /**
     * Show the advisor family dashboard
     *
     * @param $caseSlug
     * @return Factory|View
     */
    public function show($caseSlug)
    {
        $data['family'] = $this->family($caseSlug);

        $data['familyParents'] = $this->familyParents($caseSlug);
        $data['familyChildren'] = $this->familyChildren($caseSlug);
        $data['familyCostOfCollegeAndImpactOnRetirement'] = (new FamilyRepository())->getFamilyCostOfCollegeAndImpactOnRetirement($this->family($caseSlug));
        if ($this->family($caseSlug)->is_completed_efc === 1) {
            $data['efc'] = (new EfcCalculatorRepository())->getEfc($this->family($caseSlug));
        }
        $data['timezone'] = InputValue::timezones()->orderBy('order_seq')->pluck('label', 'value')->toArray();

        return view('advisor.case.dashboard.index', compact('data'));
    }
}
