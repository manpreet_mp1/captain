<?php

namespace App\Http\Controllers\Advisor;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\AuthAdvisorTrait;
use App\Repositories\Family\SubscriptionRepository;
use Illuminate\Http\Request;
use function compact;
use function flash;
use function in_array;
use function redirect;
use function view;

class AdvisorCaseSubscriptionController extends Controller
{
    use AuthAdvisorTrait;

    /**
     * Show all subscription for a parent also page for upgrade
     *
     * @param $caseSlug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($caseSlug)
    {
        $data['family'] = $this->family($caseSlug);

        $data['pricing'] = (new SubscriptionRepository())->getPricing();

        $data['subscription'] = (new SubscriptionRepository())->getSubscription($data['family']);

        return view('advisor.case.subscription.index', compact('data'));
    }

    /**
     * Show the checkout page
     *
     * @param $caseSlug
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function checkoutFixed($caseSlug, $slug)
    {
        $data['family'] = $this->family($caseSlug);

        if (!in_array($slug, ['smarttrack', 'smarttrack-advisor', 'bundle'])) {
            return redirect()->route('advisor.case.subscriptions', [
                'caseSlug' => $caseSlug
            ]);
        }

        $data['product'] = (new SubscriptionRepository())->retrieveFixedProduct($slug);
        $data['slug'] = $slug;
        $data['coupon_code'] = $this->family($caseSlug)->coupon_code;
        $data['discount'] = 0;
        $data['dueToday'] = $data['product']->price;

        // has a coupon
        if ($data['coupon_code']) {
            $output = (new SubscriptionRepository())->applyCoupon($data['product']->price, $data['coupon_code']);
            $data['discount'] = $output['discount'];
            $data['dueToday'] = $output['dueToday'];
        }

        return view('advisor.case.subscription.checkoutFixed', compact('data'));
    }

    /**
     * Show the checkout page
     *
     * @param $caseSlug
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function checkoutPaymentPlan($caseSlug, $slug)
    {
        $data['family'] = $this->family($caseSlug);

        if (!in_array($slug, ['smarttrack-3-payment', 'smarttrack-advisor-3-payment', 'bundle-3-payment'])) {
            return redirect()->route('advisor.case.subscriptions', [
                'caseSlug' => $caseSlug
            ]);
        }

        $data['productObject'] = (new SubscriptionRepository())->retrievePaymentPlans($slug);
        $data['plan'] = $data['productObject']->plan;
        $data['months'] = $data['productObject']->months;
        $data['slug'] = $slug;
        $data['coupon_code'] = $this->family($caseSlug)->coupon_code;
        $data['discount'] = 0;
        $data['dueToday'] = $data['plan']->amount;

        // has a coupon
        if ($data['coupon_code']) {
            $output = (new SubscriptionRepository())->applyCoupon($data['plan']->amount, $data['coupon_code']);
            $data['discount'] = $output['discount'];
            $data['dueToday'] = $output['dueToday'];
        }

        return view('advisor.case.subscription.checkoutPaymentPlan', compact('data'));
    }

    /**
     * Apply a coupon code
     *
     * @param $caseSlug
     * @param $slug
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function coupon($caseSlug, $slug, Request $request)
    {
        $isSubscription = (int)$request->input('is_subscription');
        $routeName = 'advisor.case.subscriptions.checkoutFixed';

        if ($isSubscription) {
            $routeName = 'advisor.case.subscriptions.checkoutPaymentPlan';
        }

        $coupon = (new SubscriptionRepository())->validCouponCode($request->input('coupon_code'), $isSubscription);

        if ($coupon) {
            $this->family($caseSlug)->update([
                'coupon_code' => $request->input('coupon_code')
            ]);
            flash('Coupon Applied.', 'toastr.info');

            return redirect()->route($routeName, [
                'caseSlug' => $caseSlug,
                'slug' => $slug
            ]);
        } else {
            $this->family($caseSlug)->update([
                'coupon_code' => null,
            ]);
        }

        flash('Invalid Coupon.', 'toastr.error');

        return redirect()->route($routeName, [
            'caseSlug' => $caseSlug,
            'slug' => $slug
        ]);
    }

    /**
     * Subscribe to fixed plan
     *
     * @param $caseSlug
     * @param $slug
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function subscribeFixed($caseSlug, $slug, Request $request)
    {
        $response = (new SubscriptionRepository())->createAOrder($this->family($caseSlug), $request->input('sku'), $request->input('stripe_token'));

        // success
        if ($response['success']) {
            $this->family($caseSlug)->update([
                'subscription' => $slug
            ]);

            return redirect()->route('advisor.case.subscriptions.thankYouFixed', [
                'caseSlug' => $caseSlug,
                'slug' => $slug
            ]);
        }

        flash($response['error'], 'toastr.error');

        return redirect()->route('advisor.case.subscriptions.checkoutFixed', [
            'caseSlug' => $caseSlug,
            'slug' => $slug
        ]);
    }

    /**
     * Subscribe to payment plan
     *
     * @param $caseSlug
     * @param $slug
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function subscribePaymentPlan($caseSlug, $slug, Request $request)
    {
        $response = (new SubscriptionRepository())->createASubscription(
            $this->family($caseSlug),
            $request->input('id'),
            $request->input('months'),
            $request->input('stripe_token')
        );

        // success
        if ($response['success']) {
            $this->family($caseSlug)->update([
                'subscription' => $slug,
                'is_on_payment_plan' => 1,
            ]);

            return redirect()->route('advisor.case.subscriptions.thankYouPaymentPlan', [
                'caseSlug' => $caseSlug,
                'slug' => $slug
            ]);
        }

        flash($response['error'], 'toastr.error');

        return redirect()->route('advisor.case.subscriptions.checkoutPaymentPlan', [
            'caseSlug' => $caseSlug,
            'slug' => $slug
        ]);
    }

    /**
     * Show thank you page
     *
     * @param $caseSlug
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function thankYouFixed($caseSlug, $slug)
    {
        $data['family'] = $this->family($caseSlug);

        // $data['item'] = (new SubscriptionRepository())->retrieveFixedProduct($slug);

        return view('advisor.case.subscription.thankYou.' . $slug, compact('data'));
    }

    /**
     * Show thank you page
     *
     * @param $caseSlug
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function thankYouPaymentPlan($caseSlug, $slug)
    {
        $data['family'] = $this->family($caseSlug);

        // $data['item'] = (new SubscriptionRepository())->retrieveFixedProduct($slug);

        return view('advisor.case.subscription.thankYou.' . $slug, compact('data'));
    }
}
