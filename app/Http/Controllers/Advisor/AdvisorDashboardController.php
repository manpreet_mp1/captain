<?php

namespace App\Http\Controllers\Advisor;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\AuthAdvisorTrait;
use function compact;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use function view;

class AdvisorDashboardController extends Controller
{
    use AuthAdvisorTrait;

    /**
     * Show the advisor dashboard
     *
     * @return Factory|View
     */
    public function index()
    {
        $data['cases'] = $this->cases();

        return view('advisor.dashboard.index', compact('data'));
    }
}
