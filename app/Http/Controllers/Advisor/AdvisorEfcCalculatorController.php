<?php

namespace App\Http\Controllers\Advisor;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\AuthAdvisorTrait;
use App\Http\Controllers\Traits\AuthFamilyTrait;
use App\Repositories\Family\FamilyRepository;
use App\Repositories\Premium\EfcCalculatorRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use function compact;
use function redirect;
use function view;

class AdvisorEfcCalculatorController extends Controller
{
    use AuthAdvisorTrait;

    /**
     * Show the efc result
     *
     * @param $caseSlug
     * @return Factory|RedirectResponse|View
     */
    public function index($caseSlug)
    {
        // make sure the efc is completed
        if ($this->family($caseSlug)->is_completed_efc !== 1) {
            return redirect()->route('advisor.case.questionnaires.efc', [
                'slug' => '0',
                'caseSlug' => $caseSlug
            ]);
        }

        $data['family'] = $this->family($caseSlug);
        $data['familyCostOfCollegeAndImpactOnRetirement'] = (new FamilyRepository())->getFamilyCostOfCollegeAndImpactOnRetirement($this->family($caseSlug));
        $data['efc'] = (new EfcCalculatorRepository())->getEfc($this->family($caseSlug));
        
        return view('advisor.case.calculators.efc.result', compact('data'));
    }
}
