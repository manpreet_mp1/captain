<?php

namespace App\Http\Controllers\Advisor;

use App\Http\Controllers\Controller;
use App\Repositories\Resources\MeritScholarshipsRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;
use function compact;
use function view;

class AdvisorMeritScholarshipsController extends Controller
{
    /**
     * Show the merit scholarship landing page
     *
     * @param $caseSlug
     * @param Request $request
     * @return Factory|View
     */
    public function index($caseSlug, Request $request)
    {
        $data = (new MeritScholarshipsRepository())->getViewData($caseSlug);

        $data['request'] = $request->all();

        $data['records'] = (new MeritScholarshipsRepository())->getItems($request);

        return view('advisor.resources.scholarship.merit.index', compact('data'));
    }

    /**
     * Show the merit scholarship for a school
     *
     * @param $caseSlug
     * @param $ipeds
     * @return Factory|View
     */
    public function show($caseSlug, $ipeds)
    {
        $data = (new MeritScholarshipsRepository())->getViewData($caseSlug);

        $data['records'] = (new MeritScholarshipsRepository())->getItem($ipeds);

        return view('advisor.resources.scholarship.merit.show', compact('data'));
    }
}
