<?php

namespace App\Http\Controllers\Advisor;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\AuthAdvisorTrait;
use App\Repositories\Resources\MajorRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;
use function compact;
use function view;

class AdvisorMajorController extends Controller
{
    use AuthAdvisorTrait;
    
    /**
     * Show the landing page for majors
     *
     * @return Factory|View
     */
    public function index()
    {
        $data = (new MajorRepository())->getViewData();

        return view('advisor.resources.majors.index', compact('data'));
    }

    /**
     * Show the heading for the particular main category
     *
     * @return Factory|View
     */
    public function heading($caseSlug, $slug)
    {
        $data = (new MajorRepository())->getHeadings($slug);

        return view('advisor.resources.majors.heading', compact('data'));
    }

    /**
     * Show a resource
     *
     * @param $caseSlug
     * @param $slug
     * @return Factory|View
     */
    public function show($caseSlug, $slug)
    {
        $data = (new MajorRepository())->getViewData($slug, $caseSlug);

        $data['record'] = (new MajorRepository())->getItem($slug);

        return view('advisor.resources.majors.show', compact('data'));
    }

    /**
     * Save a major to students
     *
     * @param $caseSlug
     * @param $slug
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save($caseSlug, $slug, Request $request)
    {
        (new MajorRepository())->save($this->family($caseSlug), $slug, $request->input('family_child_id'));

        flash('Success', 'toastr.info');

        return redirect()->route('advisor.case.majors.show', ['slug' => $slug, 'caseSlug' => $caseSlug]);
    }
}
