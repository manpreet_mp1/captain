<?php

namespace App\Http\Requests;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use function filter_var;
use function in_array;
use function is_array;
use function str_replace;
use function strtolower;
use const FILTER_SANITIZE_NUMBER_INT;

trait RequestTrait
{
    public $CLEANUP_TO_NUMBER = [
        'mobile',
        'income_work_dollar',
        'assessment_current_step_num',
        'parentOne' => [
            // Income - info
            'income_work_dollar',
            'income_adjusted_gross_dollar',
            // Income - tax
            'income_tax_dollar',
            // Income - additional
            'income_additional_tax_credit_dollar',
            'income_additional_child_support_paid_dollar',
            'income_additional_work_study_dollar',
            'income_additional_grant_scholarship_dollar',
            'income_additional_combat_pay_dollar',
            'income_additional_cooperative_education_dollar',
            // Income - untaxed
            'income_untaxed_tax_deferred_dollar',
            'income_untaxed_ira_deduction_dollar',
            'income_untaxed_child_support_received_dollar',
            'income_untaxed_tax_exempt_dollar',
            'income_untaxed_ira_distribution_dollar',
            'income_untaxed_pension_portion_dollar',
            'income_untaxed_paid_to_military_dollar',
            'income_untaxed_veteran_benefit_dollar',
            'income_untaxed_other_dollar',
            // Asset - education
            'asset_education_student_sibling_dollar',
            'asset_education_five_two_nine_dollar',
            'asset_education_coverdell_dollar',
            // Asset- non-retirement
            'asset_non_retirement_checking_savings_dollar',
            'asset_non_retirement_certificate_of_deposit_dollar',
            'asset_non_retirement_t_bills_dollar',
            'asset_non_retirement_money_market_dollar',
            'asset_non_retirement_mutual_dollar',
            'asset_non_retirement_stock_dollar',
            'asset_non_retirement_bond_dollar',
            'asset_non_retirement_trust_dollar',
            'asset_non_retirement_other_securities_dollar',
            'asset_non_retirement_annuities_dollar',
            'asset_non_retirement_other_investments_dollar',
            // Asset - retirement
            'asset_retirement_defined_benefit_dollar',
            'asset_retirement_four_o_one_dollar',
            'asset_retirement_ira_dollar',
            'asset_retirement_roth_ira_dollar',
            'asset_retirement_previous_employer_dollar',
            'asset_retirement_other_dollar',
            // Insurance
            'insurance_permanent_cash_value_dollar',
        ],
        'parentTwo' => [
            // Income - info
            'income_work_dollar',
            'income_adjusted_gross_dollar',
            // Income - tax
            'income_tax_dollar',
            // Income - additional
            'income_additional_tax_credit_dollar',
            'income_additional_child_support_paid_dollar',
            'income_additional_work_study_dollar',
            'income_additional_grant_scholarship_dollar',
            'income_additional_combat_pay_dollar',
            'income_additional_cooperative_education_dollar',
            // Income - untaxed
            'income_untaxed_tax_deferred_dollar',
            'income_untaxed_ira_deduction_dollar',
            'income_untaxed_child_support_received_dollar',
            'income_untaxed_tax_exempt_dollar',
            'income_untaxed_ira_distribution_dollar',
            'income_untaxed_pension_portion_dollar',
            'income_untaxed_paid_to_military_dollar',
            'income_untaxed_veteran_benefit_dollar',
            'income_untaxed_other_dollar',
            // Asset - education
            'asset_education_student_sibling_dollar',
            'asset_education_five_two_nine_dollar',
            'asset_education_coverdell_dollar',
            // Asset- non-retirement
            'asset_non_retirement_checking_savings_dollar',
            'asset_non_retirement_certificate_of_deposit_dollar',
            'asset_non_retirement_t_bills_dollar',
            'asset_non_retirement_money_market_dollar',
            'asset_non_retirement_mutual_dollar',
            'asset_non_retirement_stock_dollar',
            'asset_non_retirement_bond_dollar',
            'asset_non_retirement_trust_dollar',
            'asset_non_retirement_other_securities_dollar',
            'asset_non_retirement_annuities_dollar',
            'asset_non_retirement_other_investments_dollar',
            // Asset - retirement
            'asset_retirement_defined_benefit_dollar',
            'asset_retirement_four_o_one_dollar',
            'asset_retirement_ira_dollar',
            'asset_retirement_roth_ira_dollar',
            'asset_retirement_previous_employer_dollar',
            'asset_retirement_other_dollar',
            // Insurance
            'insurance_permanent_cash_value_dollar',
        ],
        'student' => [
            // Income - info
            'income_work_dollar',
            'income_adjusted_gross_dollar',
            // Income - tax
            'income_tax_dollar',
            // Income - additional
            'income_additional_tax_credit_dollar',
            'income_additional_child_support_paid_dollar',
            'income_additional_work_study_dollar',
            'income_additional_grant_scholarship_dollar',
            'income_additional_combat_pay_dollar',
            'income_additional_cooperative_education_dollar',
            // Income - untaxed
            'income_untaxed_tax_deferred_dollar',
            'income_untaxed_ira_deduction_dollar',
            'income_untaxed_child_support_received_dollar',
            'income_untaxed_tax_exempt_dollar',
            'income_untaxed_ira_distribution_dollar',
            'income_untaxed_pension_portion_dollar',
            'income_untaxed_paid_to_military_dollar',
            'income_untaxed_veteran_benefit_dollar',
            'income_untaxed_other_dollar',
            'income_untaxed_student_behalf_dollar',
            // Asset- non retirement
            'asset_non_retirement_checking_savings_dollar',
            'asset_non_retirement_ugma_utma_dollar',
            'asset_non_retirement_certificate_of_deposit_dollar',
            'asset_non_retirement_t_bills_dollar',
            'asset_non_retirement_money_market_dollar',
            'asset_non_retirement_mutual_dollar',
            'asset_non_retirement_stock_dollar',
            'asset_non_retirement_bond_dollar',
            'asset_non_retirement_trust_dollar',
            'asset_non_retirement_other_investments_dollar',
        ],
        'family' => [
            // case creation
            'parent_asset_non_retirement_dollar_total',
            'parent_asset_education_dollar_total',
            'parent_asset_retirement_dollar_total',
            // Income - info
            'parent_income_work_dollar_total',
            'parent_income_adjusted_gross_dollar',
            'parent_income_adjusted_gross_dollar_total',
            // Income - tax
            'parent_income_tax_dollar',
            'parent_income_tax_dollar_total',
            // Income - additional
            'parent_income_additional_tax_credit_dollar',
            'parent_income_additional_tax_credit_dollar_total',
            'parent_income_additional_child_support_paid_dollar',
            'parent_income_additional_child_support_paid_dollar_total',
            'parent_income_additional_work_study_dollar',
            'parent_income_additional_work_study_dollar_total',
            'parent_income_additional_grant_scholarship_dollar',
            'parent_income_additional_grant_scholarship_dollar_total',
            'parent_income_additional_combat_pay_dollar',
            'parent_income_additional_combat_pay_dollar_total',
            'parent_income_additional_cooperative_education_dollar',
            'parent_income_additional_cooperative_education_dollar_total',
            // Income - untaxed
            'parent_income_untaxed_tax_deferred_dollar',
            'parent_income_untaxed_tax_deferred_dollar_total',
            'parent_income_untaxed_ira_deduction_dollar',
            'parent_income_untaxed_ira_deduction_dollar_total',
            'parent_income_untaxed_child_support_received_dollar',
            'parent_income_untaxed_child_support_received_dollar_total',
            'parent_income_untaxed_tax_exempt_dollar',
            'parent_income_untaxed_tax_exempt_dollar_total',
            'parent_income_untaxed_ira_distribution_dollar',
            'parent_income_untaxed_ira_distribution_dollar_total',
            'parent_income_untaxed_pension_portion_dollar',
            'parent_income_untaxed_pension_portion_dollar_total',
            'parent_income_untaxed_paid_to_military_dollar',
            'parent_income_untaxed_paid_to_military_dollar_total',
            'parent_income_untaxed_veteran_benefit_dollar',
            'parent_income_untaxed_veteran_benefit_dollar_total',
            'parent_income_untaxed_other_dollar',
            'parent_income_untaxed_other_dollar_total',
            // Asset - education
            'parent_asset_education_student_sibling_dollar',
            'parent_asset_education_student_sibling_dollar_total',
            'parent_asset_education_five_two_nine_dollar',
            'parent_asset_education_five_two_nine_dollar_total',
            'parent_asset_education_coverdell_dollar',
            'parent_asset_education_coverdell_dollar_total',
            // Asset - non retirement
            'parent_asset_non_retirement_checking_savings_dollar',
            'parent_asset_non_retirement_checking_savings_dollar_total',
            'parent_asset_non_retirement_certificate_of_deposit_dollar',
            'parent_asset_non_retirement_certificate_of_deposit_dollar_total',
            'parent_asset_non_retirement_t_bills_dollar',
            'parent_asset_non_retirement_t_bills_dollar_total',
            'parent_asset_non_retirement_money_market_dollar',
            'parent_asset_non_retirement_money_market_dollar_total',
            'parent_asset_non_retirement_mutual_dollar',
            'parent_asset_non_retirement_mutual_dollar_total',
            'parent_asset_non_retirement_stock_dollar',
            'parent_asset_non_retirement_stock_dollar_total',
            'parent_asset_non_retirement_bond_dollar',
            'parent_asset_non_retirement_bond_dollar_total',
            'parent_asset_non_retirement_trust_dollar',
            'parent_asset_non_retirement_trust_dollar_total',
            'parent_asset_non_retirement_other_securities_dollar',
            'parent_asset_non_retirement_other_securities_dollar_total',
            'parent_asset_non_retirement_annuities_dollar',
            'parent_asset_non_retirement_annuities_dollar_total',
            'parent_asset_non_retirement_other_investments_dollar',
            'parent_asset_non_retirement_other_investments_dollar_total',
            // Asset - retirement
            'parent_asset_retirement_defined_benefit_dollar',
            'parent_asset_retirement_defined_benefit_dollar_total',
            'parent_asset_retirement_four_o_one_dollar',
            'parent_asset_retirement_four_o_one_dollar_total',
            'parent_asset_retirement_ira_dollar',
            'parent_asset_retirement_ira_dollar_total',
            'parent_asset_retirement_roth_ira_dollar',
            'parent_asset_retirement_roth_ira_dollar_total',
            'parent_asset_retirement_previous_employer_dollar',
            'parent_asset_retirement_previous_employer_dollar_total',
            'parent_asset_retirement_other_dollar',
            'parent_asset_retirement_other_dollar_total',
            // Real estate
            'parent_asset_real_estate_primary_purchase_dollar',
            'parent_asset_real_estate_primary_value_dollar',
            'parent_asset_real_estate_primary_owned_dollar',
            'parent_asset_real_estate_primary_payment_dollar',
            'parent_asset_real_estate_other_purchase_dollar',
            'parent_asset_real_estate_other_value_dollar',
            'parent_asset_real_estate_other_owned_dollar',
            'parent_asset_real_estate_other_payment_dollar',
            // Business
            'parent_business_value_dollar',
            'parent_farm_value_dollar',
        ],
        'student_income_adjusted_gross_dollar',
        'student_income_work_dollar',
        'student_income_tax_dollar',
        'student_income_untaxed_dollar_total',
        'student_income_additional_dollar_total',
        'student_asset_non_retirement_checking_savings_dollar',
        'student_asset_non_retirement_ugma_utma_dollar',
        'student_asset_non_retirement_certificate_of_deposit_dollar',
        'student_asset_non_retirement_t_bills_dollar',
        'student_asset_non_retirement_money_market_dollar',
        'student_asset_non_retirement_mutual_dollar',
        'student_asset_non_retirement_stock_dollar',
        'student_asset_non_retirement_bond_dollar',
        'student_asset_non_retirement_trust_dollar',
        'student_asset_non_retirement_other_investments_dollar',
        'parent_income_adjusted_gross_dollar_total',
        'parent_1_income_work_dollar',
        'parent_2_income_work_dollar',
        'parent_income_tax_dollar_total',
        'parent_income_untaxed_dollar_total',
        'parent_income_additional_dollar_total',
        'parent_asset_non_retirement_checking_savings_dollar_total',
        'parent_asset_non_retirement_certificate_of_deposit_dollar_total',
        'parent_asset_non_retirement_t_bills_dollar_total',
        'parent_asset_non_retirement_money_market_dollar_total',
        'parent_asset_non_retirement_mutual_dollar_total',
        'parent_asset_non_retirement_stock_dollar_total',
        'parent_asset_non_retirement_bond_dollar_total',
        'parent_asset_non_retirement_trust_dollar_total',
        'parent_asset_non_retirement_other_securities_dollar_total',
        'parent_asset_non_retirement_annuities_dollar_total',
        'parent_asset_non_retirement_other_investments_dollar_total',
        'parent_asset_education_student_sibling_dollar_total',
        'parent_asset_education_five_two_nine_dollar_total',
        'parent_asset_education_coverdell_dollar_total',
        'parent_asset_retirement_defined_benefit_dollar_total',
        'parent_asset_retirement_four_o_one_dollar_total',
        'parent_asset_retirement_ira_dollar_total',
        'parent_asset_retirement_roth_ira_dollar_total',
        'parent_asset_retirement_other_dollar_total',
        'parent_insurance_permanent_cash_value_dollar_total',
        'parent_farm_net_worth_dollar',
        'parent_business_net_worth_dollar',
        'parent_asset_real_estate_primary_net_worth_dollar',
        'parent_asset_real_estate_other_net_worth_dollar',
        'contributions_parent_annual_income_dollar',
        'contributions_parent_assets_education_dollar',
        'contributions_parent_assets_non_retirement_dollar',
        'contributions_parent_assets_retirement_dollar',
        'contributions_parent_life_insurance_dollar',
        'contributions_parent_business_farm_dollar',
        'contributions_parent_real_estate_dollar',
        'contributions_parent_plus_private_loans_dollar',
        'contributions_parent_other_contribution_dollar',
        'contributions_student_annual_income_dollar',
        'contributions_student_total_asset_dollar',
        'contributions_student_loans_dollar',
        'contributions_student_other_contribution_dollar',
    ];

    /**
     * Cleanup input
     */
    public function cleanupAllInputs()
    {
        // convert all input to array
        $inputArray = $this->all();

        // start cleanup
        $this->cleanupEmail($inputArray);
        $this->cleanupPassword($inputArray);
        $this->cleanupBirthDate($inputArray);
        $this->cleanupNumber($inputArray);
        $this->cleanUpPercent($inputArray);
    }

    /**
     * Cleanup all email related input
     *
     * @param $inputArray
     */
    private function cleanupEmail($inputArray)
    {
        $columns = [
            'email',
            'parentTwo' => [
                'email',
            ],
        ];

        foreach ($columns as $key => $column) {
            if (!is_array($column) && isset($inputArray[$column])) {
                $this->merge([
                    "{$column}" => strtolower($inputArray[$column]),
                ]);
            }

            // for nested arrays
            if (is_array($column)) {
                $mainInputArray = $this->input($key);
                foreach ($column as $item) {
                    if (isset($inputArray[$key][$item])) {
                        $mainInputArray[$item] = strtolower($inputArray[$key][$item]);
                    }
                }
                $this->merge([
                    "{$key}" => $mainInputArray
                ]);
            }
        }
    }

    /**
     * Cleanup all password related input
     *
     * @param $inputArray
     */
    private function cleanupPassword($inputArray)
    {
        if (isset($inputArray['password'])) {
            $this->merge([
                "tmp_password" => $inputArray['password'],
                "password" => Hash::make($inputArray['password']),
            ]);
        }
    }

    /**
     * Cleanup birth date
     *
     * @param $inputArray
     */
    private function cleanupBirthDate($inputArray)
    {
        $columns = [
            'parentOne' => [
                'birth_date',
            ],
            'parentTwo' => [
                'birth_date',
            ],
        ];

        foreach ($columns as $key => $column) {
            if (!is_array($column) && isset($inputArray[$column])) {
                $this->merge([
                    "{$column}" => Carbon::parse($inputArray[$column]),
                ]);
            }

            // for nested arrays
            if (is_array($column)) {
                $mainInputArray = $this->input($key);
                foreach ($column as $item) {
                    if (isset($inputArray[$key][$item])) {
                        $mainInputArray[$item] = Carbon::parse($inputArray[$key][$item]);
                    }
                }
                $this->merge([
                    "{$key}" => $mainInputArray
                ]);
            }
        }
    }

    /**
     * Cleanup to number
     *
     * @param $inputArray
     */
    private function cleanupNumber($inputArray)
    {
        foreach ($this->CLEANUP_TO_NUMBER as $key => $column) {
            if (!is_array($column) && isset($inputArray[$column])) {
                $this->merge([
                    "{$column}" => (int) str_replace(["-", "+"], "", filter_var($inputArray[$column], FILTER_SANITIZE_NUMBER_INT)),
                ]);
            }

            // for nested arrays
            if (is_array($column)) {
                $mainInputArray = $this->input($key);
                foreach ($column as $item) {
                    if (isset($inputArray[$key][$item])) {
                        $mainInputArray[$item] = (int) str_replace(["-", "+"], "", filter_var($inputArray[$key][$item], FILTER_SANITIZE_NUMBER_INT));
                    }
                }
                $this->merge([
                    "{$key}" => $mainInputArray
                ]);
            }

            // for students
            if ($key === "student" && isset($inputArray['student'])) {
                $studentArray = $inputArray['student'];
                $student = [];
                foreach ($studentArray as $studentId => $studentFields) {
                    foreach ($studentFields as $studentFieldKey => $studentFieldValue) {
                        foreach ($column as $predefinedField) {
                            if (isset($inputArray['student'][$studentId][$predefinedField])) {
                                $student[$studentId][$predefinedField] = (int)str_replace(["-", "+"], "", filter_var($inputArray['student'][$studentId][$predefinedField], FILTER_SANITIZE_NUMBER_INT));
                            }
                        }
                    }
                }
                $this->merge([
                    "student" => $student
                ]);
            }
        }
    }

    /**
     * Cleanup percent
     *
     * @param $inputArray
     */
    private function cleanUpPercent($inputArray)
    {
        $columns = [
            'family' => [
                'parent_asset_real_estate_primary_interest_percent',
                'parent_asset_real_estate_other_interest_percent',
            ],
        ];

        foreach ($columns as $key => $column) {
            if (!is_array($column) && isset($inputArray[$column])) {
                $this->merge([
                    "{$column}" => str_replace("%", "", $inputArray[$column]),
                ]);
            }

            // for nested arrays
            if (is_array($column)) {
                $mainInputArray = $this->input($key);
                foreach ($column as $item) {
                    if (isset($inputArray[$key][$item])) {
                        $mainInputArray[$item] = str_replace("%", "", $inputArray[$key][$item]);
                    }
                }
                $this->merge([
                    "{$key}" => $mainInputArray
                ]);
            }
        }
    }
}
