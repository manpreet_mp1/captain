<?php
namespace App\Http\Requests\Parent;

use App\Http\Requests\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

class ParentRegisterRequest extends FormRequest
{
    use RequestTrait;

    /**
     * determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'bail|required',
            'last_name' => 'bail|required',
            'mobile' => 'bail|required',
            'email' => 'bail|unique:users,email',
            'password' => 'bail|required',
            'advisor_id' => 'nullable|exists:users,id',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($validator->errors()->count()===0) {
                $this->cleanupAllInputs();
                $this->merge([
                    'role' => 'PARENT',
                ]);
            }
        });
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => 'Email is required.',
            'email.unique' => 'Email is already registered.',
            'password.required' => 'Password is required.',
        ];
    }
}
