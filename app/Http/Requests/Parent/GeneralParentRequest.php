<?php
namespace App\Http\Requests\Parent;

use App\Http\Requests\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

class GeneralParentRequest extends FormRequest
{
    use RequestTrait;

    /**
     * determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Configure the validator instance.
     *
     * @param  Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($validator->errors()->count()===0) {
                $this->cleanupAllInputs();
            }
        });
    }
}
