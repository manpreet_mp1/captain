<?php

namespace App\Http\Middleware;

use function abort;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class RoleAdvisor
{
    /**
     * Check if the logged in user is parent
     *
     * @param  Request $request
     * @param Closure $next
     * @return string
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->is_advisor === 1) {
            View::share('ST_USER', Auth::user());

            return $next($request);
        }

        abort(401);
    }
}
