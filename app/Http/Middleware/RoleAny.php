<?php

namespace App\Http\Middleware;

use function abort;
use Closure;
use function flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use function in_array;
use function optional;
use function redirect;

class RoleAny
{
    /**
     * Check if the logged in user is parent
     *
     * @param  Request $request
     * @param Closure $next
     * @return string
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            View::share('ST_USER', Auth::user());
            switch (true) {
                case Auth::user()->is_admin === 1:
                    return $next($request);

                    break;
                case Auth::user()->is_advisor === 1:
                    return $next($request);

                    break;
                case Auth::user()->is_parent === 1:
                    View::share('ST_USER', Auth::user());
                    View::share('ST_FAMILY', Auth::user()->family);
                    View::share('ST_USER_TWO', Auth::user()->userTwo());
                    View::share('ST_PARENT', Auth::user()->parent);
                    View::share('ST_PARENT_TWO', optional(Auth::user()->userTwo())->parent);
                    View::share('ST_FAMILY_CHILDREN', Auth::user()->family->children);

                    // not completed assessment and in one of these route
                    $noRedirectRouteName = [
                        "parent.questionnaires.caseCreation",
                        "parent.questionnaires.caseCreation.store",
                    ];
                    if (Auth::user()->family->is_completed_assessment === 0 && !in_array($request->route()->getName(), $noRedirectRouteName)) {
                        flash('YOU CAN NOT ACCESS THIS FEATURE UNTIL YOU COMPLETE YOUR FAMILY PROFILE.', 'toastr.error');

                        return redirect()->route('parent.questionnaires.caseCreation', ['slug' => 0]);
                    }

                    return $next($request);
            }
        }
        
        abort(401);
    }
}
