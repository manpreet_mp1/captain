<?php

namespace App\Http\Middleware;

use function abort;
use function base64_encode;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use function redirect;
use function str_replace;
use function url;

class Authenticate
{
    /**
     * Check if the logged in user is parent
     *
     * @param  Request $request
     * @param Closure $next
     * @return string
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            // set the return URL
            $returnUrl = str_replace(url('/'), "", $request->fullUrl());

            // redirect to login
            return redirect()->route('login', [
                'return' => base64_encode($returnUrl)
            ]);
        }

        if (Auth::check()) {
            View::share('ST_USER', Auth::user());

            return $next($request);
        }

        abort(401);
    }
}
