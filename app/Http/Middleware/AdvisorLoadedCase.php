<?php

namespace App\Http\Middleware;

use function abort;
use App\Models\Family\Family;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class AdvisorLoadedCase
{
    /**
     * Check if the logged in user is parent
     *
     * @param  Request $request
     * @param Closure $next
     * @return string
     */
    public function handle($request, Closure $next)
    {
        $family = Family::where('slug', $request->route('caseSlug'))
            ->where('advisor_id', Auth::user()->id)
            ->first();

        if ($family) {
            View::share('ST_ADVISOR_FAMILY', $family);

            return $next($request);
        }

        abort(401);
    }
}
