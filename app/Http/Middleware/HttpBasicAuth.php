<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class HttpBasicAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->getUser() !== config('auth.basic.user')
            or $request->getPassword() !== config('auth.basic.pass')) {
            $header = ['WWW-Authenticate' => 'Basic'];

            return response('Unauthorized', 401, $header);
        }

        return $next($request);
    }
}
