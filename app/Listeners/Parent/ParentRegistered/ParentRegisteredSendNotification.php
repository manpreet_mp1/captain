<?php
namespace App\Listeners\Parent\ParentRegistered;

use App\Events\Parent\ParentRegisteredEvent;
use App\Notifications\Parent\ParentRegisteredNotification;

class ParentRegisteredSendNotification
{
    /**
     * Handle the event.
     *
     * @param ParentRegisteredEvent $event
     * @return void
     */
    public function handle(ParentRegisteredEvent $event)
    {
        $event->user->notify(new ParentRegisteredNotification($event->user));
    }
}
