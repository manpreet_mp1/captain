<?php

namespace App\Providers;

use App\Events\Parent\ParentRegisteredEvent;
use App\Listeners\Parent\ParentRegistered\ParentRegisteredSendNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
//        UserLoggedInEvent::class => [
//            UserLoggedInSendNotifications::class,
//        ],
        ParentRegisteredEvent::class => [
            ParentRegisteredSendNotification::class,
            // @todo - need to code these emails
            // ParentRegisteredSendEmailToAdmin::class,
            // ParentRegisteredSendEmailToAdvisorAndParent::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
