## About

[![Build Status](https://travis-ci.com/akki-io/fsg-st-laravel.svg?token=zMEwQkUk5FifRMCkY2RY&branch=master)](https://travis-ci.com/akki-io/fsg-st-laravel)

College Planning software

## Requirements

- Docker
- Docker Compose

## Installation

#### Step 1

```
## Clone the repository
git clone git@github.com:akki-io/fsg-st-laravel.git .

## Build using docker-compose
chmod +x build_local.sh
./build_local.sh 

## Start the containers
chmod +x run_local.sh
./run_local.sh 
```

#### Step 2

```
## SSH into the app server
docker-compose exec st-app-server /bin/bash

## Run the migration
php artisan migrate 
```

#### Step 3

```
## Update the .env file, set this to false
RUN_OTHER_DB_MIGRATION=false 
```

## Day to Day Operation

```
## Start the containers
./run_local.sh 

## Stop the containers
docker-compose stop
```

## Documentation

The documentation for the project is available on the [Github Wiki](https://github.com/akki-io/fsg-st-laravel/wiki).

## Issues

Bug reports and feature requests can be submitted on the [Github Issue Tracker](https://github.com/akki-io/fsg-st-laravel/issues).
