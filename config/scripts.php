<?php

return [
    'financialCoachAssessment' => [
        'ncsa-mvp' => [
            'qualifiedUrl' => "https://go.smarttrackcollegefunding.com/assessment-ncsa-mvp-q",
            'notQualifiedUrl' => "https://go.smarttrackcollegefunding.com/assessment-ncsa-mvp-dq",
        ],
        'ncsa-mvp-chris-campbell' => [
            'qualifiedUrl' => "https://go.smarttrackcollegefunding.com/ncsa-mvp-chris-campbell-q",
            'notQualifiedUrl' => "https://go.smarttrackcollegefunding.com/ncsa-mvp-chris-campbell-dq",
        ],
        'ncsa-mvp-bryan-campbell' => [
            'qualifiedUrl' => "https://go.smarttrackcollegefunding.com/ncsa-mvp-bryan-campbell-q",
            'notQualifiedUrl' => "https://go.smarttrackcollegefunding.com/ncsa-mvp-bryan-campbell-dq",
        ],
        'ncsa-mvp-kevin-campbell' => [
            'qualifiedUrl' => "https://go.smarttrackcollegefunding.com/ncsa-mvp-kevin-campbell-q",
            'notQualifiedUrl' => "https://go.smarttrackcollegefunding.com/ncsa-mvp-kevin-campbell-dq",
        ],
        'ncsa-mvp-greg-stevens' => [
            'qualifiedUrl' => "https://go.smarttrackcollegefunding.com/ncsa-mvp-greg-stevens-q",
            'notQualifiedUrl' => "https://go.smarttrackcollegefunding.com/ncsa-mvp-greg-stevens-dq",
        ],
        'ncsa-mvp-mike-henjum' => [
            'qualifiedUrl' => "https://go.smarttrackcollegefunding.com/ncsa-mvp-mike-henjum-q",
            'notQualifiedUrl' => "https://go.smarttrackcollegefunding.com/ncsa-mvp-mike-henjum-dq",
        ],
        'ncsa-mvp-roy-sargent' => [
            'qualifiedUrl' => "https://go.smarttrackcollegefunding.com/ncsa-mvp-roy-sargent-q",
            'notQualifiedUrl' => "https://go.smarttrackcollegefunding.com/ncsa-mvp-roy-sargent-dq",
        ],
        'ncsa-mvp-zach-congelliere' => [
            'qualifiedUrl' => "https://go.smarttrackcollegefunding.com/ncsa-mvp-zach-congelliere-q",
            'notQualifiedUrl' => "https://go.smarttrackcollegefunding.com/ncsa-mvp-zach-congelliere-dq",
        ],
        'ncsa-regular' => [
            'qualifiedUrl' => "https://go.smarttrackcollegefunding.com/assessment-ncsa-regular-q",
            'notQualifiedUrl' => "https://go.smarttrackcollegefunding.com/assessment-ncsa-regular-dq",
        ],
        'general' => [
            'qualifiedUrl' => "https://go.smarttrackcollegefunding.com/assessment-general-q",
            'notQualifiedUrl' => "https://go.smarttrackcollegefunding.com/assessment-general-dq",
        ],
        'sylvan-gardena' => [
            'qualifiedUrl' => "https://go.smarttrackcollegefunding.com/sylvan-q",
            'notQualifiedUrl' => "https://go.smarttrackcollegefunding.com/sylvan-dq",
        ],
        'sylvan-woodland-hills' => [
            'qualifiedUrl' => "https://go.smarttrackcollegefunding.com/sylvan-q",
            'notQualifiedUrl' => "https://go.smarttrackcollegefunding.com/sylvan-dq",
        ],
        'sylvan-san-marino' => [
            'qualifiedUrl' => "https://go.smarttrackcollegefunding.com/sylvan-q",
            'notQualifiedUrl' => "https://go.smarttrackcollegefunding.com/sylvan-dq",
        ],
        'sylvan-westlake-village' => [
            'qualifiedUrl' => "https://go.smarttrackcollegefunding.com/sylvan-q",
            'notQualifiedUrl' => "https://go.smarttrackcollegefunding.com/sylvan-dq",
        ],
        'sylvan-torrance' => [
            'qualifiedUrl' => "https://go.smarttrackcollegefunding.com/sylvan-q",
            'notQualifiedUrl' => "https://go.smarttrackcollegefunding.com/sylvan-dq",
        ],
        'sylvan-west-covina' => [
            'qualifiedUrl' => "https://go.smarttrackcollegefunding.com/sylvan-q",
            'notQualifiedUrl' => "https://go.smarttrackcollegefunding.com/sylvan-dq",
        ],
    ],
    'acuity' => [
        'validAppointmentTypeIds' => [
            9323292, // SMARTTRACK® ADVISOR (NCSA MVP)
            9323298, // SMARTTRACK® COACH (NCSA MVP)
            9320192, // SMARTTRACK® ADVISOR (NCSA)
            9320215, // SMARTTRACK® COACH (NCSA)
        ],
        'financialCoachAssessmentResponseFieldId' => 6167415,
    ],
    'typeform' => [
        'financialCoachAssessmentFormId' => 'l6T6Wf',
    ],
];
