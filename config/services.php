<?php

use App\Models\Core\User;

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'acuity' => [
        'userId' => env('ACUITY_USER_ID', 17298956),
        'apiKey' => env('ACUITY_API_KEY', "d6f605cef2ea32e2d73a15a0bb5c41f6"),
        'businessTimeZone' => 'America/Los_Angeles',
        'appointmentTypes' => [
            'f1StrategyAppointmentId' => env('F1_STRATEGY_APPOINTMENT_ID', 9672304),
        ],
    ],

    'typeform' => [
        'personal_access_token' => env('TYPEFORM_PERSONAL_ACCESS_TOKEN', "AdNH6wEGhF3nD2pFPoHgReX6MqBCAGL5brC5Yse5J3bq"),
    ],

    'stripe' => [
        'model' => User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
        'webhook' => [
            'secret' => env('STRIPE_WEBHOOK_SECRET'),
            'tolerance' => env('STRIPE_WEBHOOK_TOLERANCE', 300),
        ],
        'products' => [
            'sku' => [
                'smarttrack' => env('STRIPE_PRODUCT_SKU_SMARTTRACK'),
                'smarttrack_advisor' => env('STRIPE_PRODUCT_SKU_SMARTTRACK_ADVISOR'),
                'bundle' => env('STRIPE_PRODUCT_SKU_SMARTTRACK_BUNDLE'),
            ],
        ],
        'subscription' => [
            'three_payments' => [
                'smarttrack' => env('STRIPE_SUBSCRIPTION_THREE_PAYMENTS_SMARTTRACK'),
                'smarttrack_advisor' => env('STRIPE_SUBSCRIPTION_THREE_PAYMENTS_SMARTTRACK_ADVISOR'),
                'bundle' => env('STRIPE_SUBSCRIPTION_THREE_PAYMENTS_SMARTTRACK_BUNDLE'),
            ],
        ],
        'mapping' => [
            env('STRIPE_PRODUCT_SKU_SMARTTRACK') => 'SMARTTRACK®',
            env('STRIPE_PRODUCT_SKU_SMARTTRACK_ADVISOR') => 'SMARTTRACK® ADVISOR',
            env('STRIPE_PRODUCT_SKU_SMARTTRACK_BUNDLE') => 'BUNDLE',
            env('STRIPE_SUBSCRIPTION_THREE_PAYMENTS_SMARTTRACK') => 'SMARTTRACK®',
            env('STRIPE_SUBSCRIPTION_THREE_PAYMENTS_SMARTTRACK_ADVISOR') => 'SMARTTRACK® ADVISOR',
            env('STRIPE_SUBSCRIPTION_THREE_PAYMENTS_SMARTTRACK_BUNDLE') => 'BUNDLE',
        ],
        'slugMapping' => [
            'smarttrack' => env('STRIPE_PRODUCT_SKU_SMARTTRACK'),
            'smarttrack-advisor' => env('STRIPE_PRODUCT_SKU_SMARTTRACK_ADVISOR'),
            'bundle' => env('STRIPE_PRODUCT_SKU_SMARTTRACK_BUNDLE'),
            'smarttrack-3-payment' => env('STRIPE_SUBSCRIPTION_THREE_PAYMENTS_SMARTTRACK'),
            'smarttrack-advisor-3-payment' => env('STRIPE_SUBSCRIPTION_THREE_PAYMENTS_SMARTTRACK_ADVISOR'),
            'bundle-3-payment' => env('STRIPE_SUBSCRIPTION_THREE_PAYMENTS_SMARTTRACK_BUNDLE'),
        ],
        'sourceMapping' => [
            'test-source-123' => '50PercentOffOneTime',
        ]
    ],

];
