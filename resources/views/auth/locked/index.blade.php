@extends('themes.metronics.layout.(obsolete)fullwith-nosidebar')
@section('title', 'Your account has been locked for security purposes.')
@section('pageHeader', 'Your account has been locked for security purposes.')
@section('content')
    <div class="kt-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
        <div class="kt-demo__preview">
            <p>Here are some reasons why your account may have been locked by Team:</p>
            <ul>
                <li>We’ve detected that your account might be compromised and we’ve initiated a protective lock.</li>
                <li>We’ve detected that your account has been inactive for an extended period of time.</li>
                <li>We’ve detected that your account is in violation of the Rules or Terms of Service.</li>
            </ul>
        </div>
    </div>
    <p class="font-w300 font-s20">
        Please send an email to <a
                href="mailto:{{ config('mail.support_email') }}">{{ config('mail.support_email') }}</a> with Subject
        line <span class="kt-link kt--font-boldest text-uppercase">"Account Unlock Request"</span> from your registered
        email address for further assistance.
    </p>
@endsection