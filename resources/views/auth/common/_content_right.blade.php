@section('contentRight')
    <div class="kt-grid__item kt-grid__item--fluid kt-grid__item--center kt-grid kt-grid--ver kt-login__content"
         style="background-image: url({{ config('app.cdn') }}images/bg-4.jpg)">
        <div class="kt-login__section">
            <div class="kt-login__block" style="padding: 0 40px;">
                <h3 class="kt-login__title text-uppercase">Welcome to {{ config('app.name') }}</h3>
                <div class="kt-login__desc">
                    {{ config('app.tagLine') }} dolor sit amet, coectetuer adipiscing
                    <br>elit sed diam nonummy et nibh euismod elit sed diam nonummy et nibh euismod
                </div>
            </div>
        </div>
    </div>
@endsection