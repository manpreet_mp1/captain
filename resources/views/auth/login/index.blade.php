<!-- Entends the main template -->
@extends('themes.metronics.layout.left-content-right-message')
@section('title', 'Login')
@section('content')
    <div class="kt-login__logo">
        <img src="{{ config('app.cdn') }}images/logo.png" style="max-width: 400px;" class="img-fluid" alt="logo">
    </div>
    <div class="kt-login__signin">
        <div class="kt-login__head">
            <h3 class="kt-login__title">Sign In</h3>
        </div>
        <div class="kt-login-form">
            {!! Form::open([
                'method'=>'POST',
                'class'=>'kt-form kt-form--label-right kt-margin-t-20',
                'id'=> 'login-form',
                'name'=>'login-form'
                ]) !!}
            @include('snippets.php.alert.field', ['field' => 'general'])
            <div class="form-group kt-margin-t-40">
                <div class="kt-input-icon kt-input-icon--left">
                    {!! Form::email('email', null, [
                        'class'=>'form-control form-control-lg',
                        'id'=>'email',
                        'placeholder'=> 'Enter Email',
                        'required'
                        ]) !!}
                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
                        <span>
                            <i class="fas fa-envelope"></i>
                        </span>
                    </span>
                </div>
                @include('snippets.php.alert.field', ['field' => 'email'])
            </div>
            <div class="form-group">
                <div class="kt-input-icon kt-input-icon--left">
                    {!! Form::password('password', [
                        'class'=>'form-control form-control-lg',
                        'id'=>'password',
                        'placeholder'=> 'Enter Password',
                        'required'
                        ]) !!}
                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
                        <span>
                            <i class="fas fa-unlock"></i>
                        </span>
                    </span>
                </div>
                @include('snippets.php.alert.field', ['field' => 'password'])
            </div>
            <div class="kt-login__actions">
                {!! Form::button('<span><i class="fas fa-lock"></i><span>Sign In</span></span>', [
                    'name' => 'login',
                    'id' => 'login',
                    'class'=> 'btn btn-brand btn-elevate btn-block',
                    'type'=>'submit',
                    'value' => 'Sign In'
                    ]) !!}
            </div>
            <p class="font-s13 text-center text-muted kt-margin-t-20">
                By clicking Sign In, you agree to the {{ config('app.name') }} Terms of Service, Terms of Use
                and have read and acknowledge our Privacy Statement.
            </p>
            <div class="kt-divider">
                <span></span>
                <span></span>
            </div>
            <p class="text-center kt-margin-t-20">
                <a href="#" id="kt_login_forget_password" class="kt-link">I forgot my password</a>
            </p>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('contentBottom')
    <div class="kt-login__account">
            <span class="kt-login__account-msg">
                New to {{ config('app.name') }}?
            </span>
        <a href="{{ route('parent.register') }}" id="kt_login_signup" class="kt-link kt-link--brand kt-login__account-link">
            Create an account.
        </a>
    </div>
@endsection

@include('auth.common._content_right')