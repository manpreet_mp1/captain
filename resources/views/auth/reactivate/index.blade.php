@extends('themes.metronics.layout.(obsolete)fullwith-nosidebar')
@section('title', 'Your account has not been verified.')
@section('pageHeader', 'Your account has not been verified.')
@section('content')
    <span class="font-w300 font-s20">
    Please check your email for verification link. If you have not received an email you can click on send verification button below.
    </span>
    <div class="kt-widget27__container kt--margin-top-40">
        <a class="btn btn-brand kt-btn kt-btn--square kt-btn--air text-white text-uppercase btn-lg"
           href="{{ route('reactivateResend') }}">
            <span>
                <span>RESEND VERIFICATION</span>
            </span>
        </a>
    </div>
@endsection