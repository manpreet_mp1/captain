@extends('themes.metronics.layout.(obsolete)fullwith-nosidebar')
@section('title', 'Account verification email is sent.')
@section('pageHeader', 'Account verification email is sent.')
@section('content')
    <p class="font-w300 font-s20">
        Please check your email for verification link.
    </p>
    <p class="font-w300 font-s20">If you have still not recevied the activation link, please send an email to <a
                href="mailto:{{ config('mail.support_email') }}">{{ config('mail.support_email') }}</a> with Subject
        line <span class="kt-link kt--font-boldest text-uppercase">"Account Verfication Request"</span> from your
        registered email address for further assistance.
    </p>
@endsection