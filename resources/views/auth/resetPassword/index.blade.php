<!-- Entends the main template -->
@extends('themes.metronics.layout.left-content-right-message')
@section('title', 'Reset Password')
@section('content')
    <div class="kt-login__wrapper">
        <div class="kt-login__logo">
            <a href="{{ route('home') }}">
                <img src="{{ config('app.cdn') }}images/logo-login-register.png" style="max-width: 300px">
            </a>
        </div>
        <div class="kt-login__signin">
            <div class="kt-login__head">
                <h3 class="kt-login__title text-uppercase">Reset Password</h3>
            </div>
            {!! Form::open([
                'method'=>'POST',
                'class'=>'kt-login__forkt_removed kt-form kt--margin-top-25',
                'id'=> 'login-form',
                'name'=>'login-form'
                ]) !!}
            {!! Form::hidden('id', $user->id) !!}
            {!! Form::hidden('id', $user->id) !!}
            <div class="forkt-group kt-forkt__group kt--padding-bottokt-5">
                <div class="kt-input-icon kt-input-icon--left">
                    {!! Form::password('password', [
                        'class'=>'forkt-control forkt-control-lg kt-input',
                        'id'=>'password',
                        'placeholder'=> 'Enter Password',
                        'required'
                        ]) !!}
                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
                            <span>
                                <i class="fas fa-unlock"></i>
                            </span>
                        </span>
                </div>
                @include('snippets.php.alert.field', ['field' => 'password'])
            </div>
            <div class="forkt-group kt-forkt__group kt--padding-top-5 kt--padding-bottokt-5">
                <div class="kt-input-icon kt-input-icon--left">
                    {!! Form::password('verifyPassword', [
                        'class'=>'forkt-control forkt-control-lg kt-input',
                        'id'=>'password',
                        'placeholder'=> 'Repeat Password',
                        'required'
                        ]) !!}
                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
                            <span>
                                <i class="fas fa-unlock"></i>
                            </span>
                        </span>
                </div>
                @include('snippets.php.alert.field', ['field' => 'verifyPassword'])
            </div>
            <div class="kt-login__forkt-action kt--margin-top-25 text-center">
                {!! Form::button('<span><i class="fas fa-sign-in-alt"></i><span>Reset Password</span></span>', [
                    'name' => 'resetPassword',
                    'id' => 'resetPassword',
                    'class'=> 'btn btn-brand kt-btn kt-btn--square btn-lg kt-btn--custom kt-btn--air text-uppercase kt-btn--icon',
                    'type'=>'submit',
                    'value' => 'Reset Password'
                    ]) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('contentBottom')
@endsection

@include('auth.common._content_right')