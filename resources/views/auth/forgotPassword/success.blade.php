<!-- Entends the main template -->
@extends('themes.metronics.layout.left-content-right-message')
@section('title', 'Success')
@section('content')
    <div class="kt-login__wrapper">
        <div class="kt-login__logo">
            <a href="{{ route('home') }}">
                <img src="{{ config('app.cdn') }}images/logo-login-register.png" style="max-width: 300px">
            </a>
        </div>
        <div class="kt-login__signin">
            <div class="kt-login__head">
                <h3 class="kt-login__title text-uppercase">Success</h3>
            </div>
            <div class="kt-demo__preview kt-demo__preview--badge kt--margin-top-40">
                <p class="font-s22">Please check your email to reset your password.</p>
            </div>
        </div>
    </div>
@endsection

@section('contentBottom')
@endsection

@include('auth.common._content_right')

@section('scripts')
    <script>
        $(document).ready(function () {
            $("#mobile").inputmask("(999) 999-9999");
        });
    </script>
@endsection