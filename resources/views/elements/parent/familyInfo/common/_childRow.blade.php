<tr>
    <td class="text-right font-w400">
        {{ $title }}
        @if(isset($popoverMessage))
            @include('elements.snippet.popoverInfo', [
                'message' => $popoverMessage
            ])
        @endif
    </td>
    @foreach($data['family']->children()->inCollege(isset($onlyShowInCollegeStudent) ? $onlyShowInCollegeStudent : 0)->get() as $child)
        <td>
            {!! Form::text("student[{$child->id}][{$columnName}]", $child->{$columnName}, [
                   'class'=> 'dollar form-control form-control-lg text-left border-grey bg-gray-light',
                   'id' => "student_{$child->id}_{$columnName}",
                   "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"
                ]) !!}
        </td>
    @endforeach
</tr>