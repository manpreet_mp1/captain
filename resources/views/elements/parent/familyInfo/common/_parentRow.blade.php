<tr>
    <td class="text-right font-w400">
        {{ $title }}
        @if(isset($popoverMessage))
            @include('elements.snippet.popoverInfo', [
                'message' => $popoverMessage
            ])
        @endif
    </td>
    <td>
        {!! Form::text("parentOne[{$columnName}]", $parentOne->{$columnName}, [
               'class'=> "dollar form-control form-control-lg text-left border-grey bg-gray-light {$columnName}",
               'id' => "parent_one_{$columnName}",
               "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"
            ]) !!}
    </td>
    @if($parentTwo)
        <td>
            {!! Form::text("parentTwo[{$columnName}]", $parentTwo->{$columnName}, [
               'class'=> "dollar form-control form-control-lg text-left border-grey bg-gray-light {$columnName}",
               'id' => "parent_two_{$columnName}",
               "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"
            ]) !!}
        </td>
        <td>
            @if($hasJointlyAccount == "yes")
                @php $jointlyColumnName = "parent_{$columnName}" @endphp
                {!! Form::text("family[parent_{$columnName}]", $data['family']->{$jointlyColumnName}, [
                    'class'=> "dollar form-control form-control-lg text-left border-grey bg-gray-light {$columnName}",
                    'id' => "family_{$columnName}",
                    "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"
                ]) !!}
            @endif
        </td>
    @endif
    <td>
        {!! Form::text("family[parent_{$columnName}_total]", null, [
           'class'=> "dollar form-control form-control-lg text-left border-grey bg-dark text-white",
           'id' => "total_{$columnName}",
           "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'", 'readonly'
        ]) !!}
    </td>
</tr>
