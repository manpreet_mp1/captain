@section('pageTitle', 'Family Info')
@section('subheader')
    <h3 class="kt-subheader__title">Family Info</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <span class="kt-subheader__desc">Parents</span>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-9">
            <div class="kt-portlet">
                <div class="kt-portlet__head bg-primary text-white">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Parents</h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        @if($data['family']->parentOne()->is_parent_two && !$data['family']->parentTwo())
                            <div class="text-right">
                                <a href="{{ route($addParentRouteName, $additionalParam) }}" class="btn btn-primary text-white">
                                    <i class="la la-plus"></i>ADD PARENT
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="table table-left-right-border table-condensed table-striped valign-middle">
                        <tbody>
                            <tr class="thead-dark">
                                <th class="font-w400">Name</th>
                                <th class="font-w400">Email</th>
                                <th class="font-w400">Phone</th>
                                <th class="font-w400">Role</th>
                                <th class="font-w400">Age</th>
                                <th class="font-w400">Actions</th>
                            </tr>
                        </tbody>
                        @foreach($data['family']->parents AS $parent)
                            <tr>
                                <td>{{ $parent->user->full_name }}</td>
                                <td>{{ $parent->user->email }}</td>
                                <td>{{ $parent->user->formatted_mobile }}</td>
                                <td>{{ $parent->role->label }}</td>
                                <td>{{ $parent->formatted_age }}</td>
                                <td>
                                    <div class="dropdown dropdown-inline">
                                        <button type="button" style="height: 1rem" class="btn btn-clean btn-icon btn-sm btn-icon-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-h"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end">
                                            <a
                                                class="dropdown-item" href="{{ route($editParentRouteName, array_merge($additionalParam, [
                                                'slug' => $parent->user->slug
                                            ])) }}"
                                            >
                                                <i class="la la-edit"></i>
                                                Edit
                                            </a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
