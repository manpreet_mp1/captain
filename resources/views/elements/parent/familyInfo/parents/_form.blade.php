{!! Form::model($data['parentUser'], [
                'method'=>'POST',
                'class'=>'kt-form kt-form--label-right kt-margin-t-20',
                'id'=> 'login-form',
                'name'=>'login-form'
            ]) !!}
<div class="kt-portlet">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                 {{ $portletTitle }}
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        @include('snippets.php.alert.field', ['field' => 'general'])
        <div class="">
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-6">
                        <label class="font-w400">First Name:
                            <small class="text-danger">*</small>
                        </label>
                        {!! Form::hidden('is_main_parent', $data['is_main_parent']) !!}
                        {!! Form::text('first_name', null, [
                            'class'=>'form-control form-control-lg text-left border-grey bg-gray-light',
                            'id'=>'first_name',
                            'required'
                        ]) !!}
                        @include('snippets.php.alert.field', ['field' => 'first_name'])
                    </div>
                    <div class="col-lg-6">
                        <label class="font-w400">Last Name:
                            <small class="text-danger">*</small>
                        </label>
                        {!! Form::text('last_name', null, [
                            'class'=>'form-control form-control-lg text-left border-grey bg-gray-light',
                            'id'=>'last_name',
                            'required'
                        ]) !!}
                        @include('snippets.php.alert.field', ['field' => 'last_name'])
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-6">
                        <label class="font-w400">Email:
                            <small class="text-danger">*</small>
                        </label>
                        {!! Form::email('email', null, [
                            'class'=>'form-control form-control-lg text-left border-grey bg-gray-light',
                            'id'=>'first_name',
                            'required',
                            $data['email_readonly']
                        ]) !!}
                        @include('snippets.php.alert.field', ['field' => 'email'])
                    </div>
                    <div class="col-lg-6">
                        <label class="font-w400">Mobile:
                            <small class="text-danger">*</small>
                        </label>
                        {!! Form::tel('mobile', null, [
                             'class'=>'form-control form-control-lg text-left border-grey bg-gray-light',
                             'id'=>'mobile',
                             'type' => 'tel',
                             'required'
                         ]) !!}
                        @include('snippets.php.alert.field', ['field' => 'mobile'])
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-6">
                        <label class="font-w400">State:
                            <small class="text-danger">*</small>
                        </label>
                        {!! Form::select("{$data['parent_prefix']}[state_id]", $data['states'], optional($data['parent'])->state_id, [
                            'class'=>'form-control form-control-lg text-left border-grey bg-gray-light',
                            'required' => 'true',
                            'disabled' => $data['state_disabled']
                        ]) !!}
                        @include('snippets.php.alert.field', ['field' => 'state_id'])
                    </div>
                    <div class="col-lg-6">
                        <label class="font-w400">Birth Date:
                            <small class="text-danger">*</small>
                        </label>
                        {!! Form::text("{$data['parent_prefix']}[birth_date]", optional($data['parent'])->birth_date_formatted, [
                            'class'=>'form-control form-control-lg text-left border-grey bg-gray-light',
                            'id'  => 'birth_date',
                            "data-inputmask-inputformat" => "mm/dd/yyyy",
                            "data-inputmask-alias" => "datetime",
                            'required' => 'true'
                        ]) !!}
                        @include('snippets.php.alert.field', ['field' => 'birth_date'])
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="font-w400">Role in household
                    <small class="text-danger">*</small>
                </label>
                @include('elements.forms._input_image', [
                    'required' => true,
                    'column' => "{$data['parent_prefix']}[role_id]",
                    'values' => $data['householdRole'],
                    'modelData' => optional($data['parent'])->role_id
                ])
            </div>
            @if($data['is_main_parent'] === 1)
                <div class="form-group">
                    <label class="font-w400">Martial Status
                        <small class="text-danger">*</small>
                    </label>
                    @include('elements.forms._input_image', [
                        'required' => true,
                        'column' => "{$data['parent_prefix']}[marital_status_id]",
                        'values' => $data['maritalStatus'],
                        'modelData' => optional($data['parent'])->marital_status_id
                    ])
                </div>
            @endif
        </div>
    </div>
    <div class="kt-portlet__foot">
        <div class="kt-form__actions">
            {!! Form::button($buttonText, [
                'name' => 'Update',
                'id' => 'Update',
                'class'=> 'btn btn-brand btn-elevate btn-wide',
                'type' =>'submit',
                'value' => 'Update'
            ]) !!}
        </div>
    </div>
</div>
{!! Form::close() !!}
