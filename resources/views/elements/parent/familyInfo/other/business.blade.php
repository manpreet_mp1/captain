@section('pageTitle', 'Family Info')
@section('subheader')
    <h3 class="kt-subheader__title">Family Info</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <span class="kt-subheader__desc">Business/Farm</span>
@endsection
@section('content')
    {!! Form::model([], [
           'method'=>'POST',
           'id'=> 'assessment-form',
           'class'=> 'kt-margin-t-30',
           'name'=>'assessment-form'
        ]) !!}
    <div class="row">
        <div class="col-lg-12">
            @include('elements.parent.familyInfo.tables._other_business_family')
        </div>
        <div class="col-lg-12">
            {!! Form::button('Update', [
               'name' => 'Update',
               'id' => 'Update',
               'class'=> 'btn btn-brand btn-elevate-air',
               'type'=>'submit',
               'value' => 'Update'
           ]) !!}
        </div>
    </div>
    {!! Form::close() !!}
@endsection