{!! Form::model($data['user'], [
    'method'=>'POST',
    'class'=>'kt-form kt-form--label-right kt-margin-t-20',
    'id'=> 'login-form',
    'name'=>'login-form'
]) !!}
<div class="kt-portlet">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                {{ $portletTitle }}
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        @include('snippets.php.alert.field', ['field' => 'general'])
        <div class="">
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-6">
                        <label class="font-w400">First Name:
                            <small class="text-danger">*</small>
                        </label>
                        {!! Form::text('first_name', null, [
                            'class'=>'form-control form-control-lg text-left border-grey bg-gray-light',
                            'id'=>'first_name',
                            'required'
                        ]) !!}
                        @include('snippets.php.alert.field', ['field' => 'first_name'])
                    </div>
                    <div class="col-lg-6">
                        <label class="font-w400">Last Name:
                            <small class="text-danger">*</small>
                        </label>
                        {!! Form::text('last_name', null, [
                            'class'=>'form-control form-control-lg text-left border-grey bg-gray-light',
                            'id'=>'last_name',
                            'required'
                        ]) !!}
                        @include('snippets.php.alert.field', ['field' => 'last_name'])
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-6">
                        <label class="font-w400">Email:
                            <small class="text-danger">*</small>
                        </label>
                        {!! Form::email('email', null, [
                            'class'=>'form-control form-control-lg text-left border-grey bg-gray-light',
                            'id'=>'first_name',
                            'required',
                        ]) !!}
                        @include('snippets.php.alert.field', ['field' => 'email'])
                    </div>
                    <div class="col-lg-6">
                        <label class="font-w400">Mobile:
                            <small class="text-danger">*</small>
                        </label>
                        {!! Form::tel('mobile', null, [
                             'class'=>'form-control form-control-lg text-left border-grey bg-gray-light',
                             'id'=>'mobile',
                             'type' => 'tel',
                             'required'
                         ]) !!}
                        @include('snippets.php.alert.field', ['field' => 'mobile'])
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-4">
                        <label class="font-w400">Grade Level:
                            <small class="text-danger">*</small>
                        </label>
                        {!! Form::select("child[grade_level_id]", $data['gradeLevel'], optional($data['child'])->grade_level_id, [
                            'class'=>'form-control form-control-lg text-left border-grey bg-gray-light',
                            'required' => 'true'
                        ]) !!}
                        @include('snippets.php.alert.field', ['field' => 'grade_level_id'])
                    </div>
                    <div class="col-lg-4">
                        <label class="font-w400">Relationship to {{ $data['family']->parentOne()->user->first_name }}:
                            <small class="text-danger">*</small>
                        </label>
                        {!! Form::select("child[relationship_parent_one_id]", $data['relationshipToParent'], optional($data['child'])->relationship_parent_one_id, [
                             'class'=>'form-control form-control-lg text-left border-grey bg-gray-light',
                             'required' => 'true'
                         ]) !!}
                        @include('snippets.php.alert.field', ['field' => 'birth_date'])
                    </div>
                    @if($data['family']->parentOne()->is_parent_two && $data['family']->parentTwo())
                        <div class="col-lg-4">
                            <label class="font-w400">Relationship to {{ $data['family']->parentTwo()->user->first_name }}:
                                <small class="text-danger">*</small>
                            </label>
                            {!! Form::select("child[relationship_parent_two_id]", $data['relationshipToParent'], optional($data['child'])->relationship_parent_two_id, [
                                 'class'=>'form-control form-control-lg text-left border-grey bg-gray-light',
                                 'required' => 'true'
                             ]) !!}
                            @include('snippets.php.alert.field', ['field' => 'birth_date'])
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__foot">
        <div class="kt-form__actions">
            {!! Form::button($buttonText, [
                'name' => 'Update',
                'id' => 'Update',
                'class'=> 'btn btn-brand btn-elevate btn-wide',
                'type' =>'submit',
                'value' => 'Update'
            ]) !!}
        </div>
    </div>
</div>
{!! Form::close() !!}
