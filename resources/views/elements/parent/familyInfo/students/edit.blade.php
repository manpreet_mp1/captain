@section('pageTitle', 'Family Info')
@section('subheader')
    <h3 class="kt-subheader__title">Family Info</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <span class="kt-subheader__desc">Student | {{ $data['user']->full_name }}</span>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-9">
            @include('elements.parent.familyInfo.students._form', [
                'portletTitle' => 'Edit Student',
                'buttonText' => 'Update',
            ])
        </div>
    </div>
@endsection

@section('scripts')
    <style>
        .form-control:disabled, .form-control[readonly] {
            background-color: #343a40 !important;
            color: white !important;
        }
    </style>
    <script>
        $(document).ready(function () {
            $("#mobile").inputmask("(999) 999-9999");
            $("#birth_date").inputmask();
        });
    </script>
@endsection
