@section('pageTitle', 'Family Info')
@section('subheader')
    <h3 class="kt-subheader__title">Family Info</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <span class="kt-subheader__desc">Students</span>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-9">
            <div class="kt-portlet">
                <div class="kt-portlet__head bg-primary text-white">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Students</h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        @if($data['family']->children->count() < 5)
                            <div class="text-right">
                                <a href="{{ route($addStudentRouteName, $additionalParam) }}" class="btn btn-primary text-white">
                                    <i class="la la-plus"></i>ADD STUDENT
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="table table-left-right-border table-condensed table-striped valign-middle">
                        <tbody>
                            <tr class="thead-dark">
                                <th class="font-w400">Name</th>
                                <th class="font-w400">Email</th>
                                <th class="font-w400">Phone</th>
                                <th class="font-w400">Grade Level</th>
                                <th class="font-w400">Relationship to Parent 1</th>
                                @if($data['family']->parentOne()->is_parent_two && $data['family']->parentTwo())
                                    <th class="font-w400">Relationship to Parent 2</th>
                                @endif
                                <th class="font-w400">Actions</th>
                            </tr>
                        </tbody>
                        @foreach($data['family']->children AS $item)
                            <tr>
                                <td>{{ $item->user->full_name }}</td>
                                <td>{{ $item->user->email }}</td>
                                <td>{{ $item->user->formatted_mobile }}</td>
                                <td>{{ optional($item->gradeLevel)->label }}</td>
                                <td>{{ optional($item->relationshipParentOne)->label }}</td>
                                @if($data['family']->parentOne()->is_parent_two && $data['family']->parentTwo())
                                    <td>{{ optional($item->relationshipParentTwo)->label }}</td>
                                @endif
                                <td>
                                    <div class="dropdown dropdown-inline">
                                        <button type="button" style="height: 1rem" class="btn btn-clean btn-icon btn-sm btn-icon-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-h"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end">
                                            <a class="dropdown-item" href="{{ route($editStudentRouteName, array_merge($additionalParam, [
                                                'slug' => $item->user->slug
                                            ])) }}">
                                                <i class="la la-edit"></i>
                                                Edit
                                            </a>
                                            <a class="dropdown-item" href="{{ route($deleteStudentRouteName, array_merge($additionalParam, [
                                                'slug' => $item->user->slug
                                            ])) }}">
                                                <i class="la la-trash"></i>
                                                Delete
                                            </a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
