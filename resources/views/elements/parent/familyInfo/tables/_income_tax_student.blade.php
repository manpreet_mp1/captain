<div class="kt-portlet">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Student(s) Tax </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-xl-12">
                <div class="table-responsive">
                    <table class="table table-noborder valign-middle no-border font-s18">
                        <tbody>
                            <tr class="">
                                <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400"></th>
                                @foreach($data['family']->children()->inCollege(isset($onlyShowInCollegeStudent) ? $onlyShowInCollegeStudent : 0)->get() as $child)
                                    <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">{{ $child->user->first_name }}</th>
                                @endforeach
                            </tr>
                            @include('elements.parent.familyInfo.common._childRow', [
                                'title' => 'Federal taxes paid',
                                'popoverMessage' => 'Enter the student’s income tax paid amount. This can be found on IRS Form 1040—line 55; 1040A—line 35; or 1040EZ—line 10.',
                                'columnName' => 'income_tax_dollar'
                            ])
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>