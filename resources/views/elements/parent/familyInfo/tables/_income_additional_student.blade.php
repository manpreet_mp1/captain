<div class="kt-portlet">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Student(s) ADDITIONAL FINANCIAL INFORMATION </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-xl-12">

                <div class="table-responsive">
                    <table class="table table-noborder valign-middle no-border font-s18">
                        <tbody>
                            <tr class="">
                                <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400"></th>
                                @foreach($data['family']->children()->inCollege(isset($onlyShowInCollegeStudent) ? $onlyShowInCollegeStudent : 0)->get() as $child)
                                    <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">{{ $child->user->first_name }}</th>
                                @endforeach
                            </tr>
                            @include('elements.parent.familyInfo.common._childRow', [
                                'title' => 'American Opportunity Tax Credit or Lifetime Learning Tax Credit',
                                'popoverMessage' => 'If the student filed:<br/>IRS Form 1040 Use Line: 50<br/>IRS Form 1040A Use Line: 33',
                                'columnName' => 'income_additional_tax_credit_dollar'
                            ])
                            @include('elements.parent.familyInfo.common._childRow', [
                               'title' => 'Child support paid',
                               'popoverMessage' => 'Enter the total amount of child support the student (and if married, your spouse) paid because of divorce or separation or as a result of a legal requirement in 2014. Do not include support for children in the household.',
                               'columnName' => 'income_additional_child_support_paid_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._childRow', [
                               'title' => 'Taxable earnings from Work-study, Assistantships or Fellowships',
                               'popoverMessage' => 'Federal Work-study is income earned from work. This income should appear on your student\' W-2 form and should be reported as wages, whether or not your student is a tax filer. Do not worry about the fact that you are reporting work-study income in both places. The amounts from the student\' Additional Financial Information fields are treated differently in the EFC calculation, and your student will not be penalized.',
                               'columnName' => 'income_additional_work_study_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._childRow', [
                                'title' => 'Grant and scholarship aid reported to the IRS',
                                'popoverMessage' => 'Student aid types to report as part of the AGI may include:<br>Grants<br>Scholarships<br>Waivers / Remissions<br>Fellowships / Assistantships (grant or scholarship portions)<br>AmeriCorps education awards<br>AmeriCorps living allowances (but not insurance or child care payments)<br>AmeriCorps interest accrual payments (for student loan interest that accrued during the student\' AmeriCorps term of service)',
                                'columnName' => 'income_additional_grant_scholarship_dollar'
                             ])
                            @include('elements.parent.familyInfo.common._childRow', [
                                'title' => 'Combat pay or special combat pay',
                                'popoverMessage' => 'Enter the amount of taxable combat pay or special combat pay that your student received. Only enter the amount that was taxable and included in the adjusted gross income. Do not enter untaxed combat pay reported on the W-2 in Box 12, Code Q.',
                                'columnName' => 'income_additional_combat_pay_dollar'
                             ])
                            @include('elements.parent.familyInfo.common._childRow', [
                                 'title' => 'Cooperative education program earnings',
                                 'popoverMessage' => 'Enter the amount of income the student earned from work under a cooperative education program offered by a college.',
                                 'columnName' => 'income_additional_cooperative_education_dollar'
                              ])
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>