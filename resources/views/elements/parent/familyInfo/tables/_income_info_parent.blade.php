@php
    $parentOne = $data['family']->parentOne();
    $parentTwo = $data['family']->parentTwo();
@endphp
<div class="kt-portlet">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Parent(s) Income </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-xl-12">
                <div class="table-responsive">
                    <table class="table table-noborder valign-middle no-border font-s18">
                        <tbody>
                                <tr class="">
                                    <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400"></th>
                                    <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">{{ $parentOne->user->first_name }}</th>
                                    @if($parentTwo)
                                        <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">{{ optional($parentTwo)->user->first_name }}</th>
                                        <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">Jointly</th>
                                    @endif
                                    <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">Total</th>
                                </tr>

                            @include('elements.parent.familyInfo.common._parentRow', [
                                'title' => 'Annual earnings from work',
                                'popoverMessage' => 'Enter wages, salaries and self employment income for the parent. For most accurate results, add the number in box 5 of all W-2s and self-employment income from line 3 of Schedule SE of the tax return and enter here. This entry calculates the SS Tax adjustment from income, therefore it will be higher than Total amount of parents\' taxable Adjusted Gross Income:.',
                                'hasJointlyAccount' => 'no',
                                'columnName' => 'income_work_dollar'
                            ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                                'title' => 'Adjusted Gross Income',
                                'popoverMessage' => 'Use 1040 line 37, 1040A line 21, 1040EZ line 4, or Telefile line 1. If a tax return has not been filed, use accurate estimated figures or the previous years return.',
                                'hasJointlyAccount' => 'yes',
                                'columnName' => 'income_adjusted_gross_dollar'
                            ])
                        </tbody>
                    </table>
                </div>
                {!! Form::hidden("is_income_info_breakdown_completed", 1) !!}
            </div>
        </div>
    </div>
</div>
