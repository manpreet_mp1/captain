@php
    $parentOne = $data['family']->parentOne();
    $parentTwo = $data['family']->parentTwo();
@endphp
<div class="kt-portlet">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Parent(s) ADDITIONAL FINANCIAL INFORMATION </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-xl-12">

                <div class="table-responsive">
                    <table class="table table-noborder valign-middle no-border font-s18">
                        <tbody>
                                <tr class="">
                                    <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400"></th>
                                    <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">{{ $parentOne->user->first_name }}</th>
                                    @if($parentTwo)
                                        <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">{{ optional($parentTwo)->user->first_name }}</th>
                                        <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">Jointly</th>
                                    @endif
                                    <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">Total</th>
                                </tr>

                            @include('elements.parent.familyInfo.common._parentRow', [
                               'title' => 'American Opportunity Tax Credit or Lifetime Learning Tax Credit',
                               'popoverMessage' => 'If the parents filed:<br/>IRS Form 1040 – Use Line: 50<br/>IRS Form 1040A – Use Line: 33.',
                               'hasJointlyAccount' => 'yes',
                               'columnName' => 'income_additional_tax_credit_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                               'title' => 'Child support paid',
                               'popoverMessage' => 'How much does the parent pay in child support?',
                               'hasJointlyAccount' => 'yes',
                               'columnName' => 'income_additional_child_support_paid_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                               'title' => 'Taxable earnings from Work-study, Assistantships or Fellowships',
                               'popoverMessage' => 'Federal Work-study is income earned from work. This income should appear on your parents\' W-2 form and should be reported as wages, whether or not your parent is a tax filer. Do not worry about the fact that you are reporting work-study income in both places. The amounts from the Parents\' Additional Financial Information fields are treated differently in the EFC calculation, and your parents will not be penalized.',
                               'hasJointlyAccount' => 'yes',
                               'columnName' => 'income_additional_work_study_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                                'title' => 'Grant and scholarship aid reported to the IRS',
                                'popoverMessage' => 'Student aid types to report as part of the AGI may include:<br>Grants<br>Scholarships<br>Waivers / Remissions<br>Fellowships / Assistantships (grant or scholarship portions)<br>AmeriCorps education awards<br>AmeriCorps living allowances (but not insurance or child care payments)<br>AmeriCorps interest accrual payments (for student loan interest that accrued during the parents\' AmeriCorps term of service)',
                                'hasJointlyAccount' => 'yes',
                                'columnName' => 'income_additional_grant_scholarship_dollar'
                             ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                                'title' => 'Combat pay or special combat pay',
                                'popoverMessage' => 'Enter the amount of taxable combat pay or special combat pay that your parents received. Only enter the amount that was taxable and included in the adjusted gross income. Do not enter untaxed combat pay reported on the W-2 in Box 12, Code Q.',
                                'hasJointlyAccount' => 'yes',
                                'columnName' => 'income_additional_combat_pay_dollar'
                             ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                                 'title' => 'Cooperative education program earnings',
                                 'popoverMessage' => 'If the parents filed:<br/>IRS Form 1040 – Use Line: 50<br/>IRS Form 1040A – Use Line: 33.',
                                 'hasJointlyAccount' => 'yes',
                                 'columnName' => 'income_additional_cooperative_education_dollar'
                              ])
                        </tbody>
                    </table>
                </div>
                {!! Form::hidden("is_income_additional_breakdown_completed", 1) !!}
            </div>
        </div>
    </div>
</div>
