@php
    $parentOne = $data['family']->parentOne();
    $parentTwo = $data['family']->parentTwo();
@endphp
<div class="kt-portlet">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Parent(s) Real-Estate </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-xl-12">
                <div class="table-responsive">
                    <table class="table table-noborder valign-middle no-border font-s18">
                        <tbody>
                            <tr class="">
                                <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-right">Property Type</th>
                                <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">Original purchase price</th>
                                <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">Current Value</th>
                                <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">Current Balance Owned
                                    @include('elements.snippet.popoverInfo', [
                                        'message' => 'Current balance means the amount you owe according to your statement. The next day, you will owe more. In other words, if you are trying to pay off a credit card and the statement says your balance is $514, you may not be able to bring your balance to zero and satisfy the debt by writing a check for $514'])
                                </th>
                                <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">Interest Rate @include('elements.snippet.popoverInfo', [
                                                    'message' => 'The amount charged, expressed as a percentage of principal, by a lender to a borrower for the use of assets. Interest rates are typically noted on an annual basis, known as the annual percentage rate (APR).'])</th>
                                <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">Monthly Payment</th>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    Primary Residence
                                </td>
                                <td>
                                    {!! Form::text("family[parent_asset_real_estate_primary_purchase_dollar]", $data['family']->parent_asset_real_estate_primary_purchase_dollar, [ 'class'=> "dollar form-control form-control-lg text-left border-grey bg-gray-light ", 'id' => "parent_asset_real_estate_primary_purchase_dollar", "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'" ]) !!}
                                </td>
                                <td>
                                    {!! Form::text("family[parent_asset_real_estate_primary_value_dollar]", $data['family']->parent_asset_real_estate_primary_value_dollar, [ 'class'=> "dollar form-control form-control-lg text-left border-grey bg-gray-light ", 'id' => "parent_asset_real_estate_primary_value_dollar", "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'" ]) !!}
                                </td>
                                <td>
                                    {!! Form::text("family[parent_asset_real_estate_primary_owned_dollar]", $data['family']->parent_asset_real_estate_primary_owned_dollar, [ 'class'=> "dollar form-control form-control-lg text-left border-grey bg-gray-light ", 'id' => "parent_asset_real_estate_primary_owned_dollar", "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'" ]) !!}
                                </td>
                                <td>
                                    {!! Form::text("family[parent_asset_real_estate_primary_interest_percent]", $data['family']->parent_asset_real_estate_primary_interest_percent, [ 'class'=> "dollar form-control form-control-lg text-left border-grey bg-gray-light ", 'id' => "parent_asset_real_estate_primary_interest_percent", "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 3, 'digitsOptional': false, 'suffix': '% ', 'placeholder': '0'" ]) !!}
                                </td>
                                <td>
                                    {!! Form::text("family[parent_asset_real_estate_primary_payment_dollar]", $data['family']->parent_asset_real_estate_primary_payment_dollar, [ 'class'=> "dollar form-control form-control-lg text-left border-grey bg-gray-light ", 'id' => "parent_asset_real_estate_primary_payment_dollar", "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'" ]) !!}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    All other real estate property excluding primary residence
                                </td>
                                <td>
                                    {!! Form::text("family[parent_asset_real_estate_other_purchase_dollar]", $data['family']->parent_asset_real_estate_other_purchase_dollar, [ 'class'=> "dollar form-control form-control-lg text-left border-grey bg-gray-light ", 'id' => "parent_asset_real_estate_other_purchase_dollar", "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'" ]) !!}
                                </td>
                                <td>
                                    {!! Form::text("family[parent_asset_real_estate_other_value_dollar]", $data['family']->parent_asset_real_estate_other_value_dollar, [ 'class'=> "dollar form-control form-control-lg text-left border-grey bg-gray-light ", 'id' => "parent_asset_real_estate_other_value_dollar", "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'" ]) !!}
                                </td>
                                <td>
                                    {!! Form::text("family[parent_asset_real_estate_other_owned_dollar]", $data['family']->parent_asset_real_estate_other_owned_dollar, [ 'class'=> "dollar form-control form-control-lg text-left border-grey bg-gray-light ", 'id' => "parent_asset_real_estate_other_owned_dollar", "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'" ]) !!}
                                </td>
                                <td>
                                    {!! Form::text("family[parent_asset_real_estate_other_interest_percent]", $data['family']->parent_asset_real_estate_other_interest_percent, [ 'class'=> "dollar form-control form-control-lg text-left border-grey bg-gray-light ", 'id' => "parent_asset_real_estate_other_interest_percent", "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 3, 'digitsOptional': false, 'suffix': '% ', 'placeholder': '0'" ]) !!}
                                </td>
                                <td>
                                    {!! Form::text("family[parent_asset_real_estate_other_payment_dollar]", $data['family']->parent_asset_real_estate_other_payment_dollar, [ 'class'=> "dollar form-control form-control-lg text-left border-grey bg-gray-light ", 'id' => "parent_asset_real_estate_other_payment_dollar", "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'" ]) !!}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                {!! Form::hidden("is_real_estate_breakdown_completed", 1) !!}
            </div>
        </div>
    </div>
</div>