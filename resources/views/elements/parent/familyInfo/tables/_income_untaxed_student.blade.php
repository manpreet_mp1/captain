<div class="kt-portlet">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Student(s) UNTAXED INCOME </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-xl-12">
                <div class="table-responsive">
                    <table class="table table-noborder valign-middle no-border font-s18">
                        <tbody>
                            <tr class="">
                                <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400"></th>
                                @foreach($data['family']->children()->inCollege(isset($onlyShowInCollegeStudent) ? $onlyShowInCollegeStudent : 0)->get() as $child)
                                    <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">{{ $child->user->first_name }}</th>
                                @endforeach
                            </tr>
                            @include('elements.parent.familyInfo.common._childRow', [
                               'title' => 'Payments to tax-deferred pension and retirement savings plans',
                               'popoverMessage' => 'Enter the amount student paid to their tax-deferred pension and retirement savings plans (paid directly or withheld from their earnings). These amounts are reported on the W-2 form in boxes 12a through 12d, codes D, E, F, G, H, and S. Don\'t include amounts reported in code DD (employer contributions toward employee health benefits).',
                               'columnName' => 'income_untaxed_tax_deferred_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._childRow', [
                               'title' => 'IRA deductions and payments to self-employed SEP, SIMPLE and Keogh',
                               'popoverMessage' => 'Enter the amount of the student\' IRA deductions and payments to self-employed SEP, SIMPLE, Keogh, and other qualified plans.',
                               'columnName' => 'income_untaxed_ira_deduction_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._childRow', [
                               'title' => 'Child support received',
                               'popoverMessage' => 'Enter the amount of child support the student received for all children in their household.',
                               'columnName' => 'income_untaxed_child_support_received_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._childRow', [
                                'title' => 'Tax exempt interest income ',
                                'popoverMessage' => 'Enter the amount of tax-exempt interest income the student earned. If the student filed:<br > IRS Form 1040 Use Line: 8b<br> IRS Form 1040A Use Line: 8b<br> If the student filed separate tax returns, add line 8b from both tax returns and enter the total amount.',
                                'columnName' => 'income_untaxed_tax_exempt_dollar'
                             ])
                            @include('elements.parent.familyInfo.common._childRow', [
                                'title' => 'Untaxed portions of IRA distributions',
                                'popoverMessage' => 'Enter the total amount of the untaxed portions of IRS distributions the student received. If the total is a negative amount, enter a zero (0).<br>If the student filed:<br>IRS Form 1040 Use Lines: 15a minus 15b, and subtract rollover amounts from the total.<br>IRS Form 1040A Use Lines: 11a minus 11b, and subtract rollover amounts from the total.<br><br>Note: The line number references above are from the IRS tax form, not from the W-2 form.',
                                'columnName' => 'income_untaxed_ira_distribution_dollar'
                             ])
                            @include('elements.parent.familyInfo.common._childRow', [
                             'title' => 'Untaxed portions of pensions',
                             'popoverMessage' => 'Enter the total amount of untaxed portions of the student\' pensions earned. If the total is a negative amount, enter a zero (0). <br>If the student filed:<br>IRS Form 1040 Use Lines: 16a minus 16b, and subtract rollover amounts from the total.<br>IRS Form 1040A Use Lines: 12a minus 12b, and subtract rollover amounts from the total.',
                             'columnName' => 'income_untaxed_pension_portion_dollar'
                          ])
                            @include('elements.parent.familyInfo.common._childRow', [
                                 'title' => 'Housing, food, and other living allowances paid to military, clergy, and others',
                                 'popoverMessage' => 'Enter the total cash value of housing, food, and any other living allowances the student received. These allowances are often paid to military, clergy and others.<br>',
                                 'columnName' => 'income_untaxed_paid_to_military_dollar'
                              ])
                            @include('elements.parent.familyInfo.common._childRow', [
                                 'title' => 'Veterans noneducational benefits',
                                 'popoverMessage' => 'Enter the total amount of veterans noneducational benefits received by student. Veterans noneducational benefits include Disability, Death Pension, Dependency and Indemnity Compensation (DIC), and / or VA Educational Work-Study allowances.',
                                 'columnName' => 'income_untaxed_veteran_benefit_dollar'
                              ])
                            @include('elements.parent.familyInfo.common._childRow', [
                                 'title' => 'Other untaxed income not reported such as workers\' compensation or disability',
                                 'popoverMessage' => 'Enter the total amount of any other untaxed income or benefits, such as workers compensation, Black Lung Benefits, untaxed portions of Railroad Retirement Benefits, disability, etc. that student received. Also include the untaxed portions of health savings accounts from IRS Form 1040 line 25. <br><br>',
                                 'columnName' => 'income_untaxed_other_dollar'
                              ])
                            @include('elements.parent.familyInfo.common._childRow', [
                                 'title' => 'Money received or paid on student behalf',
                                 'popoverMessage' => 'Enter the total amount of cash support student (and if married, your spouse) received from a friend or relative (other than your parents, if student are a dependent student). Report the amount if it is not reported elsewhere on this application.<br>If someone is paying rent, utility bills, etc., for student while student attend school, include the amount of that person\'s contributions, unless the person is your parent whose information is reported on this application.<br>Include money that student received from a parent whose financial information is not reported on this form and that is not part of a legal child support agreement.<br><br>Note: This includes distributions to student (the student beneficiary) from a 529 plan that is owned by someone other than student or your parents (such as your grandparents, aunts, uncles, and non-custodial parents).',
                                 'columnName' => 'income_untaxed_student_behalf_dollar'
                              ])
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>