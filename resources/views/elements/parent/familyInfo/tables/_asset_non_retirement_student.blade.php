<div class="kt-portlet">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Student(s) Non-Retirement Assets </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-xl-12">

                <div class="table-responsive">
                    <table class="table table-noborder valign-middle no-border font-s18">
                        <tbody>
                            <tr class="">
                                <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400"></th>
                                @foreach($data['family']->children()->inCollege(isset($onlyShowInCollegeStudent) ? $onlyShowInCollegeStudent : 0)->get() as $child)
                                    <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">{{ $child->user->first_name }}</th>
                                @endforeach
                            </tr>
                            @include('elements.parent.familyInfo.common._childRow', [
                               'title' => 'Cash, Checking, and Savings',
                               'columnName' => 'asset_non_retirement_checking_savings_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._childRow', [
                               'title' => 'UGMA / UTMA trust accounts',
                               'columnName' => 'asset_non_retirement_ugma_utma_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._childRow', [
                               'title' => 'Certificates of Deposit',
                               'columnName' => 'asset_non_retirement_certificate_of_deposit_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._childRow', [
                               'title' => 'T-Bills',
                               'columnName' => 'asset_non_retirement_t_bills_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._childRow', [
                               'title' => 'Money Market Funds',
                               'columnName' => 'asset_non_retirement_money_market_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._childRow', [
                               'title' => 'Mutual Funds',
                               'columnName' => 'asset_non_retirement_mutual_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._childRow', [
                               'title' => 'Stocks/Stock Options',
                               'columnName' => 'asset_non_retirement_stock_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._childRow', [
                               'title' => 'Bonds',
                               'columnName' => 'asset_non_retirement_bond_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._childRow', [
                               'title' => 'Trust Funds',
                               'columnName' => 'asset_non_retirement_trust_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._childRow', [
                               'title' => 'All other investments',
                               'hasJointlyAccount' => 'yes',
                               'columnName' => 'asset_non_retirement_other_investments_dollar'
                           ])
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>