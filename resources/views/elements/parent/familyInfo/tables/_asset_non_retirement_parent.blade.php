@php
    $parentOne = $data['family']->parentOne();
    $parentTwo = $data['family']->parentTwo();
@endphp
<div class="kt-portlet">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Parent(s) Non-Retirement Assets </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-xl-12">
                <div class="table-responsive">
                    <table class="table table-noborder valign-middle no-border font-s18">
                        <tbody>

                                <tr class="">
                                    <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400"></th>
                                    <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">{{ $parentOne->user->first_name }}</th>
                                    @if($parentTwo)
                                        <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">{{ optional($parentTwo)->user->first_name }}</th>
                                        <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">Jointly</th>
                                    @endif
                                    <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">Total</th>
                                </tr>

                            @include('elements.parent.familyInfo.common._parentRow', [
                               'title' => 'Checking/Savings Account',
                               'hasJointlyAccount' => 'yes',
                               'columnName' => 'asset_non_retirement_checking_savings_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                               'title' => 'Certificates of Deposit',
                               'hasJointlyAccount' => 'yes',
                               'columnName' => 'asset_non_retirement_certificate_of_deposit_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                               'title' => 'T-Bills',
                               'hasJointlyAccount' => 'yes',
                               'columnName' => 'asset_non_retirement_t_bills_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                               'title' => 'Money Market Funds',
                               'hasJointlyAccount' => 'yes',
                               'columnName' => 'asset_non_retirement_money_market_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                               'title' => 'Mutual Funds',
                               'hasJointlyAccount' => 'yes',
                               'columnName' => 'asset_non_retirement_mutual_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                               'title' => 'Stocks/Stock Options',
                               'hasJointlyAccount' => 'yes',
                               'columnName' => 'asset_non_retirement_stock_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                               'title' => 'Bonds',
                               'hasJointlyAccount' => 'yes',
                               'columnName' => 'asset_non_retirement_bond_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                               'title' => 'Trust Funds',
                               'hasJointlyAccount' => 'yes',
                               'columnName' => 'asset_non_retirement_trust_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                               'title' => 'Other Securities',
                               'hasJointlyAccount' => 'yes',
                               'columnName' => 'asset_non_retirement_other_securities_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                               'title' => 'Non-Retirement Annuities',
                               'hasJointlyAccount' => 'yes',
                               'columnName' => 'asset_non_retirement_annuities_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                               'title' => 'Other Investments',
                               'hasJointlyAccount' => 'yes',
                               'columnName' => 'asset_non_retirement_other_investments_dollar'
                           ])
                        </tbody>
                    </table>
                </div>
                {!! Form::hidden("is_assets_non_retirement_breakdown_completed", 1) !!}
            </div>
        </div>
    </div>
</div>
