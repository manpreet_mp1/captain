@php
    $parentOne = $data['family']->parentOne();
    $parentTwo = $data['family']->parentTwo();
@endphp
<div class="kt-portlet">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Parent(s) Education Assets </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-xl-12">
                <div class="table-responsive">
                    <table class="table table-noborder valign-middle no-border font-s18">
                        <tbody>

                                <tr class="">
                                    <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400"></th>
                                    <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">{{ $parentOne->user->first_name }}</th>
                                    @if($parentTwo)
                                        <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">{{ optional($parentTwo)->user->first_name }}</th>
                                        <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">Jointly</th>
                                    @endif
                                    <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">Total</th>
                                </tr>

                            @include('elements.parent.familyInfo.common._parentRow', [
                               'title' => 'Student\'s siblings assets',
                               'popoverMessage' => 'Student\'s siblings assets - used for IM formula calculation.',
                               'hasJointlyAccount' => 'yes',
                               'columnName' => 'asset_education_student_sibling_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                               'title' => '529 / Tuition Savings Plans',
                               'hasJointlyAccount' => 'yes',
                               'columnName' => 'asset_education_five_two_nine_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                               'title' => 'Coverdell Education IRA',
                               'hasJointlyAccount' => 'yes',
                               'columnName' => 'asset_education_coverdell_dollar'
                           ])
                        </tbody>
                    </table>
                </div>
                {!! Form::hidden("is_assets_eduction_breakdown_completed", 1) !!}
            </div>
        </div>
    </div>
</div>
