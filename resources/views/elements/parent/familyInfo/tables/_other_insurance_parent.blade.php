@php
    $parentOne = $data['family']->parentOne();
    $parentTwo = $data['family']->parentTwo();
@endphp
<div class="kt-portlet">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Parent(s) Insurance</h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-xl-12">
                <div class="table-responsive">
                    <table class="table table-noborder valign-middle no-border font-s18">
                        <tbody>
                            @if($parentTwo)
                                <tr class="">
                                    <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400"></th>
                                    <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">{{ $parentOne->user->first_name }}</th>
                                    <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">{{ optional($parentTwo)->user->first_name }}</th>
                                </tr>
                            @endif
                            <tr>
                                <td class="text-right">
                                    If you own a permanent life insurance, what is the cash value?
                                    @include('elements.snippet.popoverInfo', [
                                        'message' => 'Permanent life insurance is life insurance that covers the remaining lifetime of the insured. A permanent insurance policy accumulates a cash value up to its date of maturation. The owner can access the money in the cash value by withdrawing money, borrowing the cash value, or surrendering the policy and receiving the surrender value.'])
                                </td>
                                <td>
                                    {!! Form::text("parentOne[insurance_permanent_cash_value_dollar]", $parentOne->insurance_permanent_cash_value_dollar, [ 'class'=> "dollar form-control form-control-lg text-left border-grey bg-gray-light ", 'id' => "parent_one_insurance_permanent_cash_value_dollar", "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'" ]) !!}
                                </td>
                                @if($parentTwo)
                                    <td>
                                        {!! Form::text("parentTwo[insurance_permanent_cash_value_dollar]", $parentTwo->insurance_permanent_cash_value_dollar, [ 'class'=> "dollar form-control form-control-lg text-left border-grey bg-gray-light ", 'id' => "parent_two_insurance_permanent_cash_value_dollar", "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'" ]) !!}
                                    </td>
                                @endif
                            </tr>
                        </tbody>
                    </table>
                </div>
                {!! Form::hidden("is_insurance_breakdown_completed", 1) !!}
            </div>
        </div>
    </div>
</div>