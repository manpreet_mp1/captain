@php
    $parentOne = $data['family']->parentOne();
    $parentTwo = $data['family']->parentTwo();
@endphp
<div class="kt-portlet">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Parent(s) Business/Farm </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-xl-12">
                <div class="table-responsive">
                    <table class="table table-noborder valign-middle no-border font-s18">
                        <tbody>
                            <tr>
                                <td class="text-right">
                                    If you own a business, what is the current value?
                                </td>
                                <td>
                                    {!! Form::text("family[parent_business_value_dollar]", $data['family']->parent_business_value_dollar, [ 'class'=> "dollar form-control form-control-lg text-left border-grey bg-gray-light ", 'id' => "parent_business_value_dollar", "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'" ]) !!}
                                </td>
                            </tr> <tr>
                                <td class="text-right">
                                    If you own a farm, what is the current value?
                                </td>
                                <td>
                                    {!! Form::text("family[parent_farm_value_dollar]", $data['family']->parent_farm_value_dollar, [ 'class'=> "dollar form-control form-control-lg text-left border-grey bg-gray-light ", 'id' => "parent_farm_value_dollar", "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'" ]) !!}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                {!! Form::hidden("is_business_breakdown_completed", 1) !!}
            </div>
        </div>
    </div>
</div>
