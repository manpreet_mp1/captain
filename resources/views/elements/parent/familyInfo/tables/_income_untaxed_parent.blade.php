@php
    $parentOne = $data['family']->parentOne();
    $parentTwo = $data['family']->parentTwo();
@endphp
<div class="kt-portlet">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Parent(s) UNTAXED INCOME </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-xl-12">
                <div class="table-responsive">
                    <table class="table table-noborder valign-middle no-border font-s18">
                        <tbody>
                                <tr class="">
                                    <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400"></th>
                                    <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">{{ $parentOne->user->first_name }}</th>
                                    @if($parentTwo)
                                        <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">{{ optional($parentTwo)->user->first_name }}</th>
                                        <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">Jointly</th>
                                    @endif
                                    <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">Total</th>
                                </tr>

                            @include('elements.parent.familyInfo.common._parentRow', [
                               'title' => 'Payments to tax-deferred pension and retirement savings plans',
                               'popoverMessage' => 'Enter the amount parent paid to their tax-deferred pension and retirement savings plans (paid directly or withheld from their earnings). These amounts are reported on the W-2 form in boxes 12a through 12d, codes D, E, F, G, H, and S. Don\'t include amounts reported in code DD (employer contributions toward employee health benefits).',
                               'hasJointlyAccount' => 'yes',
                               'columnName' => 'income_untaxed_tax_deferred_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                               'title' => 'IRA deductions and payments to self-employed SEP, SIMPLE and Keogh',
                               'popoverMessage' => 'Enter the amount of the parents\' IRA deductions and payments to self-employed SEP, SIMPLE, Keogh, and other qualified plans.',
                               'hasJointlyAccount' => 'yes',
                               'columnName' => 'income_untaxed_ira_deduction_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                               'title' => 'Child support received',
                               'popoverMessage' => 'Enter the amount of child support the parents received for all children in their household.',
                               'hasJointlyAccount' => 'yes',
                               'columnName' => 'income_untaxed_child_support_received_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                                'title' => 'Tax exempt interest income ',
                                'popoverMessage' => 'Enter the amount of tax-exempt interest income the parents earned. If the parents filed:<br > IRS Form 1040 – Use Line: 8b<br> IRS Form 1040A – Use Line: 8b<br> If the parents filed separate tax returns, add line 8b from both tax returns and enter the total amount.',
                                'hasJointlyAccount' => 'yes',
                                'columnName' => 'income_untaxed_tax_exempt_dollar'
                             ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                                'title' => 'Untaxed portions of IRA distributions',
                                'popoverMessage' => 'Enter the total amount of the untaxed portions of IRS distributions the parents received. If the total is a negative amount, enter a zero (0).<br>If the parents filed:<br>IRS Form 1040 – Use Lines: 15a minus 15b, and subtract rollover amounts from the total.<br>IRS Form 1040A – Use Lines: 11a minus 11b, and subtract rollover amounts from the total.<br><br>Note: The line number references above are from the IRS tax form, not from the W-2 form.',
                                'hasJointlyAccount' => 'yes',
                                'columnName' => 'income_untaxed_ira_distribution_dollar'
                             ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                             'title' => 'Untaxed portions of pensions',
                             'popoverMessage' => 'Enter the total amount of untaxed portions of the parents\' pensions earned. If the total is a negative amount, enter a zero (0). <br>If the parents filed:<br>IRS Form 1040 – Use Lines: 16a minus 16b, and subtract rollover amounts from the total.<br>IRS Form 1040A – Use Lines: 12a minus 12b, and subtract rollover amounts from the total.',
                             'hasJointlyAccount' => 'yes',
                             'columnName' => 'income_untaxed_pension_portion_dollar'
                          ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                                 'title' => 'Housing, food, and other living allowances paid to military, clergy, and others',
                                 'popoverMessage' => 'Enter the total cash value of housing, food, and any other living allowances the parents received. These allowances are often paid to military, clergy and others.<br>',
                                 'hasJointlyAccount' => 'yes',
                                 'columnName' => 'income_untaxed_paid_to_military_dollar'
                              ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                                 'title' => 'Veterans noneducational benefits',
                                 'popoverMessage' => 'Enter the total amount of veterans noneducational benefits received by parents. Veterans noneducational benefits include Disability, Death Pension, Dependency and Indemnity Compensation (DIC), and / or VA Educational Work-Study allowances.',
                                 'hasJointlyAccount' => 'yes',
                                 'columnName' => 'income_untaxed_veteran_benefit_dollar'
                              ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                                 'title' => 'Other untaxed income not reported such as workers\' compensation or disability',
                                 'popoverMessage' => 'Enter the total amount of any other untaxed income or benefits, such as workers compensation, Black Lung Benefits, untaxed portions of Railroad Retirement Benefits, disability, etc. that parents received. Also include the untaxed portions of health savings accounts from IRS Form 1040 – line 25. <br><br>',
                                 'hasJointlyAccount' => 'yes',
                                 'columnName' => 'income_untaxed_other_dollar'
                              ])
                        </tbody>
                    </table>
                </div>
                {!! Form::hidden("is_income_untaxed_breakdown_completed", 1) !!}
            </div>
        </div>
    </div>
</div>
