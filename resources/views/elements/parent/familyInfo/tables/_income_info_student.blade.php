<div class="kt-portlet">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Student(s) Income </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-xl-12">
                <div class="table-responsive">
                    <table class="table table-noborder valign-middle no-border font-s18">
                        <tbody>
                            <tr class="">
                                <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400"></th>
                                @foreach($data['family']->children()->inCollege(isset($onlyShowInCollegeStudent) ? $onlyShowInCollegeStudent : 0)->get() as $child)
                                    <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">{{ $child->user->first_name }}</th>
                                @endforeach
                            </tr>
                            @include('elements.parent.familyInfo.common._childRow', [
                                'title' => 'Annual earnings from work',
                                'popoverMessage' => 'Enter the student’s YTD income earned from work which can be found on Box 1 of student’s W2 form or on Form 1040—lines 7 + 12 + 18 + Box 14 (Code A) of IRS Schedule K-1 (Form 1065); or on 1040A—line 7; or on 1040EZ—line 1.',
                                'columnName' => 'income_work_dollar'
                            ])
                            @include('elements.parent.familyInfo.common._childRow', [
                                'title' => 'Adjusted Gross Income',
                                'popoverMessage' => 'Enter the student’s adjusted gross income from tax form 1040 line 37, 1040A line 21, 1040EZ line 4, or Telefile line 1. If a tax return has not been filed, use accurate estimated figures or the previous year’s return.',
                                'columnName' => 'income_adjusted_gross_dollar'
                            ])
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>