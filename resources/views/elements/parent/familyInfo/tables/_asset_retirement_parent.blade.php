@php
    $parentOne = $data['family']->parentOne();
    $parentTwo = $data['family']->parentTwo();
@endphp
<div class="kt-portlet">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Parent(s) Retirement Assets </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-xl-12">

                <div class="table-responsive">
                    <table class="table table-noborder valign-middle no-border font-s18">
                        <tbody>

                                <tr class="">
                                    <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400"></th>
                                    <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">{{ $parentOne->user->first_name }}</th>
                                    @if($parentTwo)
                                        <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">{{ optional($parentTwo)->user->first_name }}</th>
                                        <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">Jointly</th>
                                    @endif
                                    <th style="min-width: {{ isset($cellMinWidth) ? $cellMinWidth : 225 }}px;" class="font-w400 text-center">Total</th>
                                </tr>

                            @include('elements.parent.familyInfo.common._parentRow', [
                               'title' => 'Defined Benefit Account',
                               'hasJointlyAccount' => 'yes',
                               'columnName' => 'asset_retirement_defined_benefit_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                               'title' => '401k/403bs',
                               'hasJointlyAccount' => 'yes',
                               'columnName' => 'asset_retirement_four_o_one_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                               'title' => 'IRAs',
                               'hasJointlyAccount' => 'yes',
                               'columnName' => 'asset_retirement_ira_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                               'title' => 'Roth IRAs',
                               'hasJointlyAccount' => 'yes',
                               'columnName' => 'asset_retirement_roth_ira_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                               'title' => 'Previous Employer Retirement Plans',
                               'hasJointlyAccount' => 'yes',
                               'columnName' => 'asset_retirement_previous_employer_dollar'
                           ])
                            @include('elements.parent.familyInfo.common._parentRow', [
                               'title' => 'Other Retirement Plans',
                               'hasJointlyAccount' => 'yes',
                               'columnName' => 'asset_retirement_other_dollar'
                           ])
                        </tbody>
                    </table>
                </div>
                {!! Form::hidden("is_assets_retirement_breakdown_completed", 1) !!}
            </div>
        </div>
    </div>
</div>
