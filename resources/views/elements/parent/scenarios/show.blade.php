@section('pageTitle', 'Quick Plan')
@section('subheader')
    <h3 class="kt-subheader__title">Quick Plan</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <span class="kt-subheader__desc">{{ $data['scenario']->name }}</span>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-warning fade show" role="alert">
                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                <div class="alert-text">The calculations below are in read-only mode.</div>
                <div class="alert-close">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="la la-close"></i></span>
                    </button>
                </div>
            </div>
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head kt-portlet__space-x bg-primary text-white">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <i class="blueprintone-document199"></i> Scenario - {{ $data['scenario']->name }} ({{ $data['scenario']->child->user->first_name }} 2019 - 2020 ) </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    @include('elements.advisor.quickPlan._read_only')
                </div>
            </div>

        </div>
        <div class="col-lg-12">
            @include('elements.advisor.quickPlan._efc_compare')
        </div>
        <div class="col-lg-12">
            @include('elements.advisor.quickPlan._summary')
        </div>
        <div class="col-lg-12">
            @include('elements.parent.widgets._family_cost')
        </div>
    </div>
@endsection