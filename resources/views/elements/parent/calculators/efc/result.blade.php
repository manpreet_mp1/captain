@section('pageTitle', 'EFC Result')
@section('content')
    <div class="row">
        <div class="col-xl-6">
            @include('elements.parent.widgets._efc')
        </div>
        <div class="col-xl-6">
            @include('elements.parent.widgets._student_efc')
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            @include('elements.parent.widgets._family_cost')
        </div>
    </div>
@endsection