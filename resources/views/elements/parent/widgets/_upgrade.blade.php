<div class="kt-portlet kt-callout kt-callout--success kt-callout--diagonal-bg">
    <div class="kt-portlet__body" style="padding: 10px">
        <div class="kt-callout__body">
            <div class="kt-callout__content">
                <h3 class="kt-callout__title">UPGRADE</h3>
                <p class="kt-callout__desc">
                    @if(isset($data) && isset($data['family']) && $data['family']->is_qualified)
                        Upgrade to a paid plan and gain access to our premium features and services. Search merit scholarships, find out how generous a college is with their financial aid, access our vast learning library of the exact steps you need to follow for college success, and get one-on-one expert guidance from a SMARTTRACK® ADVISOR.
                    @else
                        Upgrade to our paid version and gain access to our premium features. Search merit scholarships, find out how generous a college is with their financial aid, and access our vast learning library of the exact steps you need to follow for college success.
                    @endif
                </p>
            </div>
            <div class="kt-callout__action">
                <a href="{{ route($upgradeSubscriptionRoute, $additionalParam) }}" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">UPGRADE NOW!</a>
            </div>
        </div>
    </div>
</div>
