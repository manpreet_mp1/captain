<div class="kt-widget4__item">
    <span class="kt-widget4__icon">
        @if(!$condition)
            <i class="{{ $icon }} kt-font-brand"></i>
        @else
            <i class="fas fa-check kt-font-success"></i>
        @endif
    </span>
    <span class="kt-widget4__title kt-widget4__title--light font-s18">
        @if(!$condition)
            {{ $title }}
        @else
            {{ $title }} <span class="kt-badge kt-badge--inline kt-badge--success">Completed</span>
        @endif

    </span>
    <span class="kt-widget4__number font-s18">
        @if(!$condition)
            @if(isset($type) && $type === "acuity")
                @include('elements.parent.widgets._book_f1_appointment_api')
            @else
                <a href="{{ $aUrl }}" class="kt-badge kt-badge--inline kt-badge--success text-uppercase">
                {{ $aText }}
            </a>
            @endif
        @endif
    </span>
</div>
