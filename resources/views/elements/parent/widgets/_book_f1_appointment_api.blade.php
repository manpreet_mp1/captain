<a href="#book-appointment" class="kt-badge kt-badge--inline kt-badge--success text-uppercase">
    {{ $aText }}
</a>

@component('components.modal', ['id' => 'book-appointment', 'maxWidth' => '50%'])
    <div id="app">
        <div id="acuity-appointment-form" v-if="visibility.form">
            <div class="row text-left">
                <div class="col-lg-12">
                    <h3>Book Appointment</h3>
                    <div class="kt-divider kt-margin-b-30">
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <div class="col-lg-12 timezone">
                    <h4 class="text-uppercase kt-margin-b-10">YOUR TIME ZONE</h4>
                    {!! Form::select('timezone', $data['timezone'], null, [
                        'class' => 'form-control form-control-lg',
                        'v-model' => "appointment.timezone",
                        '@change' => "timezoneChanged",
                    ]) !!}
                </div>
                <div class="col-lg-12 kt-margin-t-20 dates" v-if="visibility.calendar">
                    <h4 class="text-uppercase kt-margin-b-10">SELECT DATE </h4>
                    <div class="row">
                        <div class="col-lg-6">
                            <v-calendar></v-calendar>
                            <v-date-picker
                                mode='single'
                                is-inline
                                is-expanded
                                :first-day-of-week="appointment.firstDay"
                                v-model='appointment.selectedDate'
                                :available-dates="appointment.availableDates"
                                @update:topage="setDatePicker"
                                @input="dateSelected"
                            ></v-date-picker>
                        </div>
                    </div>
                    <div class="" v-if="loading.calendar">
                        @include('elements.snippet._loading', ['leftPercent' => 25])
                    </div>
                </div>
                <div class="col-lg-12 kt-margin-t-20 timing" v-if="visibility.time">
                    <div class="kt-radio-list font-s20 kt-padding-l-20">
                        <h4 class="text-uppercase kt-margin-b-10">SELECT TIME </h4>
                    </div>
                    <div class="row kt-padding-l-30">
                        <div class="col-md-3" v-for="item in appointment.availableTimes">
                            <label class="kt-radio kt-radio--bold kt-radio--brand">
                                <input v-model="appointment.selectedTime"
                                    :id="'times-'+item"
                                    :value="item"
                                    name="selectedTime"
                                    @change="timeSelected()"
                                    type="radio">@{{ item }}
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="" v-if="loading.time">
                        @include('elements.snippet._loading', ['leftPercent' => 25])
                    </div>
{{--                    <div class="kt-radio-list font-s20">--}}
{{--                        <label class="">--}}
{{--                            <input type="radio" name="radio6"> Brand state--}}
{{--                            <span></span>--}}
{{--                        </label>--}}
{{--                        <label class="kt-radio kt-radio--bold kt-radio--brand">--}}
{{--                            <input type="radio" name="radio6"> Brand state--}}
{{--                            <span></span>--}}
{{--                        </label>--}}
{{--                        <label class="kt-radio kt-radio--bold kt-radio--brand">--}}
{{--                            <input type="radio" name="radio6"> Brand state--}}
{{--                            <span></span>--}}
{{--                        </label>--}}
{{--                        <label class="kt-radio kt-radio--bold kt-radio--brand">--}}
{{--                            <input type="radio" name="radio6"> Brand state--}}
{{--                            <span></span>--}}
{{--                        </label>--}}
{{--                    </div>--}}
                </div>
                <div class="col-lg-12 kt-margin-t-20 apt-button" v-if="visibility.button">
                    <button name ='Book Appointment'
                        class='btn btn-brand btn-elevate'
                        @click="bookAppointment()">
                        <span class="fa fa-calendar"></span> Book Appointment
                    </button>
                </div>
            </div>
            <div class="" v-if="loading.form">
                @include('elements.snippet._loading', ['leftPercent' => 50])
            </div>
        </div>
        <div id="acuity-appointment-result" v-if="visibility.result">
            <div class="row text-left">
                <div class="col-lg-12">
                    <h3>Success</h3>
                    <div class="kt-divider kt-margin-b-30">
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <div class="col-lg-12 font-s20">
                    <p>Your appointment @{{ appointment.clientAppointment.name }} is booked.</p>
                    <p>
                        @{{ appointment.clientAppointment.date }} <br>
                        @{{ appointment.clientAppointment.start_time }} - @{{ appointment.clientAppointment.end_time }} (@{{ appointment.clientAppointment.timezone }})
                    </p>
                    <button name ='Book Appointment'
                        class='btn btn-brand btn-elevate'
                        onclick="location.reload();">
                        Continue
                </div>
            </div>
        </div>
    </div>
@endcomponent

@section('scripts')
    <script type="text/javascript">
        window.familyId = "{{ $data['family']->id }}";
    </script>
    <script src="{{ mix('js/acuityF1Appointment.js') }}"></script>
@endsection
