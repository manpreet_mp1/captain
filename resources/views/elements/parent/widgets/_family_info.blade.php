<div class="kt-portlet kt-portlet--height-fluid">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                FAMILY INFO
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-xl-12">
                <table class="table table-left-right-border table-condensed table-striped valign-middle">
                    <tbody>
                    <tr class="thead-dark">
                        <th class="font-w400">Name</th>
                        <th class="font-w400">Email</th>
                        <th class="font-w400">Phone</th>
                        <th class="font-w400">Age</th>
                        <th class="font-w400">Role</th>
                        <th class="font-w400">Actions</th>
                    </tr>
                    </tbody>
                    @foreach($data['familyParents'] AS $parent)
                        <tr>
                            <td>{{ $parent->user->full_name }}</td>
                            <td>{{ $parent->user->email }}</td>
                            <td>{{ $parent->user->formatted_mobile }}</td>
                            <td>{{ $parent->formatted_age }}</td>
                            <td>Parent</td>
                            <td>
                                <div class="dropdown dropdown-inline">
                                    <button type="button"
                                            style="height: 1rem"
                                            class="btn btn-clean btn-icon btn-sm btn-icon-sm"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-h"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end">
                                        <a class="dropdown-item" href="#"><i class="la la-eye"></i> View</a>
                                        <a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit</a>
                                        <a class="dropdown-item" href="#"><i class="la la-cloud-download"></i>
                                            Export</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    @foreach($data['familyChildren'] AS $child)
                    <tr>
                        <td>{{ $child->user->first_name }}</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>Student</td>
                        <td>
                            <div class="dropdown dropdown-inline">
                                <button type="button"
                                        style="height: 1rem"
                                        class="btn btn-clean btn-icon btn-sm btn-icon-sm"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-h"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end">
                                    <a class="dropdown-item" href="#"><i class="la la-eye"></i> View</a>
                                    <a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit</a>
                                    <a class="dropdown-item" href="#"><i class="la la-cloud-download"></i> Export</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>