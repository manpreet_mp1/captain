<div class="kt-portlet kt-portlet--height-fluid">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                YOUR EXPECTED FAMILY CONTRIBUTION (EFC)
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            @if($data['family']->is_completed_efc === 1)
                <div class="text-right">
                    <a href="{{ route($routeName, array_merge(['slug' => "0"], $additionalParam)) }}" class="btn btn-primary text-white">
                        <i class="la la-refresh"></i>RECALCULATE
                    </a>
                </div>
            @endif
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row text-center">
            <div class="col-md-6">
                <i class="flaticon-monument font-sba80 text-primary"></i> <br>
                FEDERAL METHODOLOGY (FM)
            </div>
            <div class="col-md-6"><i class="flaticon-school font-sba80 text-primary"></i>
                <br> INSTITUTIONAL METHODOLOGY (IM)
            </div>
            @if($data['family']->is_completed_efc === 1)
                <div class="col-md-6 font-s70 nomargintop">
                    ${{ number_format($data['efc']['efc_fm_final'], 0) }}
                </div>
                <div class="col-md-6 font-s70 nomargintop">
                    ${{ number_format($data['efc']['efc_im_final'], 0) }}
                </div>
            @else
                <div class="col-md-6 font-s40 nomargintop">
                    <a href="{{ route($routeName, array_merge(['slug' => "0"], $additionalParam)) }}">CALCULATE MY EFC</a>
                </div>
                <div class="col-md-6 font-s40 nomargintop">
                    <a href="{{ route($routeName, array_merge(['slug' => "0"], $additionalParam)) }}">CALCULATE MY EFC</a>
                </div>
            @endif
        </div>
    </div>
</div>
