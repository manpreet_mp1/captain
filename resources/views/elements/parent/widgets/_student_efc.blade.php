<div class="kt-portlet kt-portlet--height-fluid">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                STUDENT EFC BREAKDOWN
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            @if($data['family']->is_completed_efc === 1)
                <div class="text-right">
                    <a href="{{ route($routeName, array_merge(['slug' => "0"], $additionalParam)) }}"
                       class="btn btn-primary text-white">
                        <i class="la la-refresh"></i>RECALCULATE
                    </a>
                </div>
            @endif
        </div>
    </div>
    <div class="kt-portlet__body">
        <table class="table table-left-right-border table-condensed table-striped valign-middle">
            <tbody>
            <tr class="thead-dark">
                <th class="font-w400">Name</th>
                <th class="font-w400">EFC FEDERAL METHODOLOGY (FM)</th>
                <th class="font-w400">EFC INSTITUTIONAL METHODOLOGY (IM)</th>
            </tr>
            </tbody>
            @foreach($data['efc']['studentInCollege'] AS $child)
                <tr>
                    <td>{{ $child->user->first_name }}</td>
                    <td>${{ number_format($data['efc']['efcResultArray'][$child->id]['efc_final_fm'], 0) }}</td>
                    <td>${{ number_format($data['efc']['efcResultArray'][$child->id]['efc_final_im'], 0) }}</td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
