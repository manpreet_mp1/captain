<script src="https://embed.acuityscheduling.com/embed/button/17298956.js" async></script>
<a href="https://app.acuityscheduling.com/schedule.php?owner=17298956
    &appointmentType={{ config('services.acuity.appointmentTypes.f1StrategyAppointmentId') }}
    &firstName={{ $data['family']->parentOne()->user->first_name }}
    &lastName={{ $data['family']->parentOne()->user->last_name }}
    &email={{ $data['family']->parentOne()->user->email }}
    &phone={{ $data['family']->parentOne()->user->mobile }}
    &field:6610393={{ $data['family']->id }}
    &field:6610402={{ $data['family']->name_text }}
    "
    target="_blank"
    class="acuity-embed-button btn btn-custom btn-bold btn-upper btn-font-sm btn-success">
    {{ $aText }}
</a>

<style type="text/css">
    body.acuity-modal-visible {
        overflow: hidden
    }
    .acuity-embed-modal {
        display: block!important;
        position: fixed;
        content: "";
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background: rgba(0, 0, 0, .6);
        z-index: -10;
        opacity: 0;
        -webkit-transition: opacity .2s, z-index 0s .2s;
        transition: opacity .2s, z-index 0s .2s;
        text-align: center;
        overflow: hidden;
        overflow-y: auto;
        white-space: nowrap;
        -webkit-overflow-scrolling: touch;
        visibility: hidden
    }

    .acuity-embed-modal iframe {
        visibility: visible!important;
        opacity: 1!important;
        margin: 0
    }

    .acuity-modal .acuity-embed-modal>* {
        display: inline-block;
        white-space: normal;
        vertical-align: middle;
        text-align: left
    }

    .acuity-modal .acuity-embed-modal:before {
        display: inline-block;
        overflow: hidden;
        width: 0;
        height: 100%;
        vertical-align: middle;
        content: ""
    }

    .acuity-modal.acuity-modal-visible .acuity-embed-modal {
        visibility: visible;
        z-index: 20001;
        opacity: 1;
        -webkit-transition: opacity .2s;
        transition: opacity .2s
    }

    .acuity-modal.acuity-modal-visible .acuity-modal-inner {
        z-index: 1;
        opacity: 1;
        -webkit-transform: scale(1);
        transform: scale(1);
        -webkit-transition: opacity .2s, -webkit-transform .2s;
        transition: opacity .2s, -webkit-transform .2s;
        transition: opacity .2s, transform .2s;
        transition: opacity .2s, transform .2s, -webkit-transform .2s
    }

    .acuity-modal-inner {
        padding: 26px 6.5px 6.5px;
        position: relative;
        max-width: 98%;
        max-height: 98%;
        overflow: hidden;
        overflow-y: auto;
        background: #fff;
        z-index: -1;
        opacity: 0;
        -webkit-transform: scale(0);
        transform: scale(0);
        -webkit-transition: opacity .2s, z-index 0s .2s, -webkit-transform .2s;
        transition: opacity .2s, z-index 0s .2s, -webkit-transform .2s;
        transition: opacity .2s, transform .2s, z-index 0s .2s;
        transition: opacity .2s, transform .2s, z-index 0s .2s, -webkit-transform .2s;
        -webkit-box-sizing: content-box;
        box-sizing: content-box
    }

    .acuity-modal-content {
        -webkit-overflow-scrolling: touch;
        overflow: auto;
        line-height: 22px;
        font-size: 15px;
        -webkit-box-sizing: content-box;
        box-sizing: content-box
    }

    .acuity-modal-close {
        position: absolute;
        z-index: 2;
        right: 0;
        top: 0;
        height: 28px;
        width: 28px;
        line-height: 28px;
        cursor: pointer;
        text-align: center;
        color: #5fb8b9;
        font-size: 28px;
        font-weight: 700;
        background: 0 0;
        border: 0;
        padding: 0
    }

    .acuity-embed-button,
    a.acuity-embed-button,
    a.acuity-embed-button:link {
        color: #fff
    }

    .acuity-embed-button {
        background: #74c5da;
        padding: 8px 12px;
        border: 0;
        -webkit-box-shadow: 0 -2px 0 rgba(0, 0, 0, .15) inset;
        box-shadow: 0 -2px 0 rgba(0, 0, 0, .15) inset;
        border-radius: 4px;
        text-decoration: none;
        display: inline-block
    }

    .acuity-embed-button:active,
    .acuity-embed-button:focus,
    .acuity-embed-button:hover,
    .acuity-embed-button:visited {
        color: #fff
    }

    .acuity-embed-button:hover {
        -webkit-filter: brightness(112%);
        filter: brightness(112%)
    }

    .acuity-embed-button {
        background-color: #74c5da !important;
    }

    .acuity-modal-close {
        color: #604C9E !important;
    }
</style>
