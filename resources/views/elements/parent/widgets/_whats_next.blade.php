<div class="kt-portlet kt-portlet--height-fluid">
    <div class="kt-portlet__head kt-portlet__space-x bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Next Steps.. </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-lg-6">
                <!--begin::widget 12-->
                <div class="kt-widget4">
                    <!-- Book Initial Assessment -->
                    @if($data['family']->is_qualified)
                        @include('elements.parent.widgets.__what_next_template', [
                            'title' => 'Book Initial Assessment',
                            'condition' => (bool)$data['family']->advisor_id,
                            'icon' => 'fa fa-calendar',
                            'aText' => 'Book Now',
                            'aUrl' => '',
                            'type' => 'acuity',
                        ])
                    @endif
                    @include('elements.parent.widgets.__what_next_template', [
                        'title' => 'Complete Income Information',
                        'condition' => (bool)$data['family']->is_income_info_breakdown_completed,
                        'icon' => 'fas fa-th-list',
                        'aText' => 'Start',
                        'aUrl' => route($completeIncomeInformationRouteName, $additionalParam),
                    ])
                    @include('elements.parent.widgets.__what_next_template', [
                        'title' => 'Complete Income Tax Information',
                        'condition' => (bool)$data['family']->is_income_tax_breakdown_completed,
                        'icon' => 'fas fa-th-list',
                        'aText' => 'Start',
                        'aUrl' => route($completeIncomeTaxInformationRouteName, $additionalParam),
                    ])
                    @include('elements.parent.widgets.__what_next_template', [
                        'title' => 'Complete Additional Income Information',
                        'condition' => (bool)$data['family']->is_income_additional_breakdown_completed,
                        'icon' => 'fas fa-th-list',
                        'aText' => 'Start',
                        'aUrl' => route($completeAdditionalIncomeInformationRouteName, $additionalParam),
                    ])
                    @include('elements.parent.widgets.__what_next_template', [
                        'title' => 'Complete Untaxed Income Information',
                        'condition' => (bool)$data['family']->is_income_untaxed_breakdown_completed,
                        'icon' => 'fas fa-th-list',
                        'aText' => 'Start',
                        'aUrl' => route($completeUntaxedIncomeInformationRouteName, $additionalParam),
                    ])
                    @include('elements.parent.widgets.__what_next_template', [
                        'title' => 'Complete Education Assets Information',
                        'condition' => (bool)$data['family']->is_assets_eduction_breakdown_completed,
                        'icon' => 'fas fa-th-list',
                        'aText' => 'Start',
                        'aUrl' => route($completeEducationAssetsInformationRouteName, $additionalParam),
                    ])
                </div>
                <!--end::Widget 12-->
            </div>
            <div class="col-lg-6">
                <!--begin::widget 12-->
                <div class="kt-widget4">
                    @include('elements.parent.widgets.__what_next_template', [
                       'title' => 'Complete Non-Retirement Assets Information',
                       'condition' => (bool)$data['family']->is_assets_non_retirement_breakdown_completed,
                       'icon' => 'fas fa-th-list',
                       'aText' => 'Start',
                       'aUrl' => route($completeNonRetirementAssetsInformationRouteName, $additionalParam),
                   ])
                    @include('elements.parent.widgets.__what_next_template', [
                        'title' => 'Complete Retirement Assets Information',
                        'condition' => (bool)$data['family']->is_assets_retirement_breakdown_completed,
                        'icon' => 'fas fa-th-list',
                        'aText' => 'Start',
                        'aUrl' => route($completeRetirementAssetsInformationRouteName, $additionalParam),
                    ])
                    @include('elements.parent.widgets.__what_next_template', [
                       'title' => 'Complete Real-Estate Information',
                       'condition' => (bool)$data['family']->is_real_estate_breakdown_completed,
                       'icon' => 'fas fa-th-list',
                       'aText' => 'Start',
                       'aUrl' => route($completeRealEstateInformationRouteName, $additionalParam),
                   ])
                    @include('elements.parent.widgets.__what_next_template', [
                        'title' => 'Complete Business/Farm Ownership Information',
                        'condition' => (bool)$data['family']->is_business_breakdown_completed,
                        'icon' => 'fas fa-th-list',
                        'aText' => 'Start',
                        'aUrl' => route($completeBusinessFarmOwnershipInformationRouteName, $additionalParam),
                    ])
                    @include('elements.parent.widgets.__what_next_template', [
                       'title' => 'Complete Insurance Information',
                       'condition' => (bool)$data['family']->is_insurance_breakdown_completed,
                       'icon' => 'fas fa-th-list',
                       'aText' => 'Start',
                       'aUrl' => route($completeInsuranceInformationRouteName, $additionalParam),
                   ])
                    @include('elements.parent.widgets.__what_next_template', [
                        'title' => 'Calculate EFC',
                        'condition' => (bool)$data['family']->is_completed_efc,
                        'icon' => 'fas fa-calculator',
                        'aText' => 'Calculate',
                        'aUrl' => route($calculateEFCRouteName, $additionalParam),
                    ])
                </div>
                <!--end::Widget 12-->
            </div>
        </div>

    </div>
</div>

