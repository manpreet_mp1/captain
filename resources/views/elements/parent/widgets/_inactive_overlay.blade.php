@if(isset($data['family']) && !$data['family']->is_active)
    <div style="font-size: 35px;text-align: center;color: white;background: rgba(0, 0, 0, 0.3);padding: 30px;">
        UPGRADE YOUR SUBSCRIPTION!
    </div>
@endif