<div class="kt-portlet kt-portlet--height-fluid">
    <div class="kt-portlet__head kt-portlet__space-x bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                <i class="blueprintone-family25"></i> FAMILY'S COST OF COLLEGE & IMPACT ON RETIREMENT
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-xl-12">
                <div class="table-responsive">
                    <table class="table table-left-right-border table-condensed table-striped valign-middle">
                        <tbody>
                            <tr class="thead-dark">
                                <th class="font-w400">Student <br></th>
                                <th class="font-w400">First Year In <br>
                                    College
                                </th>
                                <th class="font-w400"> First Year Cost<sup>+</sup>
                                    <br> (National Average)
                                </th>
                                <th class="font-w400">4-Year Cost<sup>+</sup> <br> (National Average)
                                </th>
                                <th class="font-w400 ">Student Loans <br>
                                    (10 Year Cost)
                                </th>
                                <th class="font-w400">Parent Loans <br> (10 Year Cost)
                                </th>
                                <th class="font-w400">Total Education Loans
                                    <br> (10 Year Cost)
                                </th>
                            </tr>
                            @foreach($data['familyCostOfCollegeAndImpactOnRetirement']['studentData'] as $child)
                                <tr>
                                    <td class="font-w300"> {{ $child['firstName'] }} <sup>
                                            ++ </sup></td>
                                    <td class="">{{ $child['firstAcademicYearInCollege'] }}</td>
                                    <td class="">${{ number_format($child['firstYearCost']) }}</td>
                                    <td class="">${{ number_format($child['fourYearCost']) }}</td>
                                    <td class="">${{ number_format($child['studentLoan']) }}</td>
                                    <td class="">${{ number_format($child['parentLoan']) }}</td>
                                    <td class="">${{ number_format($child['totalLoan']) }}</td>
                                </tr>
                            @endforeach
                            <tr class="thead-dark">
                                <th colspan="7"></th>
                            </tr>
                            <tr class="bg-primary text-white">
                                <td class="font-s20 font-w400" colspan="3">
                                    Expected Cost to Educate Family
                                </td>
                                <td class="">
                                    <span class="font-s20">${{ number_format($data['familyCostOfCollegeAndImpactOnRetirement']['directCost']) }}</span> <br>
                                    <span class="font-s12 font-w400">(Direct Cost)</span>
                                </td>
                                <td></td>
                                <td></td>
                                <td class="">
                                    <span class="font-s20">${{ number_format($data['familyCostOfCollegeAndImpactOnRetirement']['costIfBorrowed']) }}</span> <br>
                                    <span class="font-s12 font-w400">(Cost if Borrowed)</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="font-s18 font-w300" colspan="3">
                                    Age of Oldest Parent
                                </td>
                                <td class="font-s18"> {{$data['familyCostOfCollegeAndImpactOnRetirement']['ageOfOldestParent']}} Years</td>
                                <td></td>
                                <td></td>
                                <td class="font-s18"> {{$data['familyCostOfCollegeAndImpactOnRetirement']['ageOfOldestParent']}} Years</td>
                            </tr>
                            <tr>
                                <td class="font-s18 font-w300" colspan="3">
                                    Years Until Retirement (Retirement Age {{$data['familyCostOfCollegeAndImpactOnRetirement']['parentDesiredRetirementAge']}})
                                </td>
                                <td class="font-s18"> {{$data['familyCostOfCollegeAndImpactOnRetirement']['yearsUntilRetirement']}}</td>
                                <td></td>
                                <td></td>
                                <td class="font-s18"> {{$data['familyCostOfCollegeAndImpactOnRetirement']['yearsUntilRetirement']}}</td>
                            </tr>
                            <tr>
                                <td class="font-s18 font-w300" colspan="3"> Expected Annual Gains (ROR)
                                </td>
                                <td class="font-s18"> {{$data['familyCostOfCollegeAndImpactOnRetirement']['expectedAnnualGains']}}.00%</td>
                                <td></td>
                                <td></td>
                                <td class="font-s18"> {{$data['familyCostOfCollegeAndImpactOnRetirement']['expectedAnnualGains']}}.00%</td>
                            </tr>
                            <tr class="bg-primary text-white">
                                <td class="font-s20 font-w400" colspan="3"> Impact on Retirement (Lost Opportunity)
                                </td>
                                <td class="font-s20"> ${{ number_format($data['familyCostOfCollegeAndImpactOnRetirement']['impactOnRetirement']['directCost']) }}</td>
                                <td></td>
                                <td></td>
                                <td class="font-s20"> ${{ number_format($data['familyCostOfCollegeAndImpactOnRetirement']['impactOnRetirement']['costIfBorrowed']) }}</td>
                            </tr>
                            <tr class="thead-dark">
                                <th colspan="7"></th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>