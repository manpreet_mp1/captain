<div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid {{ $data['family']->is_active_class }}">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Tasks </h3>
        </div>
    </div>
    @include('elements.parent.widgets._inactive_overlay')
    <div class="kt-portlet__body">
        <div class="kt-widget2">
            <div class="kt-widget2__item kt-widget2__item--success">
                <div class="kt-widget2__checkbox">
                    <label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
                        <input type="checkbox">
                        <span></span>
                    </label>
                </div>
                <div class="kt-widget2__info">
                    <a href="#" class="kt-widget2__title">
                        Make Metronic Great Again.Lorem Ipsum Amet
                    </a>
                    <a href="#" class="kt-widget2__username">
                        By Bob
                    </a>
                </div>
                <div class="kt-widget2__actions">
                    <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
                        <i class="fas fa-ellipsis-h"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                        <ul class="kt-nav">
                            <li class="kt-nav__item">
                                <a href="#" class="kt-nav__link">
                                    <i class="kt-nav__link-icon far fa-check-square"></i>
                                    <span class="kt-nav__link-text">Mark Completed</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="kt-widget2__item kt-widget2__item--success">
                <div class="kt-widget2__checkbox">
                    <label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
                        <input type="checkbox">
                        <span></span>
                    </label>
                </div>

                <div class="kt-widget2__info">
                    <a href="#" class="kt-widget2__title">
                        Prepare Docs For Metting On Monday
                    </a>
                    <a href="#" class="kt-widget2__username">
                        By Sean
                    </a>
                </div>

                <div class="kt-widget2__actions">
                    <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
                        <i class="fas fa-ellipsis-h"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                        <ul class="kt-nav">
                            <li class="kt-nav__item">
                                <a href="#" class="kt-nav__link">
                                    <i class="kt-nav__link-icon far fa-check-square"></i>
                                    <span class="kt-nav__link-text">Mark Completed</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="kt-widget2__item kt-widget2__item--success">
                <div class="kt-widget2__checkbox">
                    <label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
                        <input type="checkbox">
                        <span></span>
                    </label>
                </div>

                <div class="kt-widget2__info">
                    <a href="#" class="kt-widget2__title">
                        Make Widgets Great Again.Estudiat Communy Elit
                    </a>
                    <a href="#" class="kt-widget2__username">
                        By Aziko
                    </a>
                </div>

                <div class="kt-widget2__actions">
                    <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
                        <i class="fas fa-ellipsis-h"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                        <ul class="kt-nav">
                            <li class="kt-nav__item">
                                <a href="#" class="kt-nav__link">
                                    <i class="kt-nav__link-icon far fa-check-square"></i>
                                    <span class="kt-nav__link-text">Mark Completed</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="kt-widget2__item kt-widget2__item--success">
                <div class="kt-widget2__checkbox">
                    <label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
                        <input type="checkbox">
                        <span></span>
                    </label>
                </div>

                <div class="kt-widget2__info">
                    <a href="#" class="kt-widget2__title">
                        Make Metronic Great Again. Lorem Ipsum
                    </a>
                    <a class="kt-widget2__username">
                        By James
                    </a>
                </div>
                <div class="kt-widget2__actions">
                    <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
                        <i class="fas fa-ellipsis-h"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                        <ul class="kt-nav">
                            <li class="kt-nav__item">
                                <a href="#" class="kt-nav__link">
                                    <i class="kt-nav__link-icon far fa-check-square"></i>
                                    <span class="kt-nav__link-text">Mark Completed</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="kt-widget2__item kt-widget2__item--success">
                <div class="kt-widget2__checkbox">
                    <label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
                        <input type="checkbox">
                        <span></span>
                    </label>
                </div>

                <div class="kt-widget2__info">
                    <a href="#" class="kt-widget2__title">
                        Completa Financial Report For Emirates Airlines
                    </a>
                    <a href="#" class="kt-widget2__username">
                        By Bob
                    </a>
                </div>

                <div class="kt-widget2__actions">
                    <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
                        <i class="fas fa-ellipsis-h"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                        <ul class="kt-nav">
                            <li class="kt-nav__item">
                                <a href="#" class="kt-nav__link">
                                    <i class="kt-nav__link-icon far fa-check-square"></i>
                                    <span class="kt-nav__link-text">Mark Completed</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>