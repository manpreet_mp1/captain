<div class="kt-portlet kt-portlet--height-fluid {{ $data['family']->is_active_class }}">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                UPCOMING APPOINTMENTS </h3>
        </div>
    </div>
    @include('elements.parent.widgets._inactive_overlay')
    <div class="kt-portlet__body">
        <!--Begin::Timeline 3 -->
        <div class="kt-timeline-v2">
            <div class="kt-timeline-v2__items  kt-padding-top-25 kt-padding-bottom-30">
                <div class="kt-timeline-v2__item">
                    <span class="kt-timeline-v2__item-time">{{ now()->format('m/d') }} <br/>10:00 AM</span>
                    <div class="kt-timeline-v2__item-cricle">
                        <i class="fa fa-genderless kt-font-success"></i>
                    </div>
                    <div class="kt-timeline-v2__item-text">
                        <span class="font-w400 text-primary font-s18">EFC Meeting</span>
                        <br/>
                        Lorem ipsum dolor sit amit,consectetur eiusmdd tempor
                        <br>
                        incididunt ut labore et dolore magna elit enim at minim
                        <br>
                    </div>
                </div>
                <div class="kt-timeline-v2__item">
                    <span class="kt-timeline-v2__item-time">{{ now()->format('m/d') }} <br/>10:00 AM</span>
                    <div class="kt-timeline-v2__item-cricle">
                        <i class="fa fa-genderless kt-font-success"></i>
                    </div>
                    <div class="kt-timeline-v2__item-text">
                        <span class="font-w400 text-primary font-s18">EFC Meeting</span>
                        <br/>
                        Lorem ipsum dolor sit amit,consectetur eiusmdd tempor
                        <br>
                        incididunt ut labore et dolore magna elit enim at minim
                        <br>
                    </div>
                </div>
                <div class="kt-timeline-v2__item">
                    <span class="kt-timeline-v2__item-time">{{ now()->format('m/d') }} <br/>10:00 AM</span>
                    <div class="kt-timeline-v2__item-cricle">
                        <i class="fa fa-genderless kt-font-success"></i>
                    </div>
                    <div class="kt-timeline-v2__item-text">
                        <span class="font-w400 text-primary font-s18">EFC Meeting</span>
                        <br/>
                        Lorem ipsum dolor sit amit,consectetur eiusmdd tempor
                        <br>
                        incididunt ut labore et dolore magna elit enim at minim
                        <br>
                    </div>
                </div>
            </div>
        </div>
        <!--End::Timeline 3 -->
    </div>
</div>

<style>
    .kt-timeline-v2 .kt-timeline-v2__items .kt-timeline-v2__item .kt-timeline-v2__item-text {
        padding: 0.35rem 0 0 8rem !important;
    }

    .kt-timeline-v2:before {
        left: 7rem !important;
    }

    .kt-timeline-v2 .kt-timeline-v2__items .kt-timeline-v2__item .kt-timeline-v2__item-text {
        padding: 0.35rem 0 0 8rem !important;
    }

    .kt-timeline-v2 .kt-timeline-v2__items .kt-timeline-v2__item .kt-timeline-v2__item-cricle {
        left: 6.30rem !important;
    }
</style>