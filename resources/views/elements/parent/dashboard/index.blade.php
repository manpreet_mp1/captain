@section('pageTitle', 'Dashboard')
@section('subheader')
    <h3 class="kt-subheader__title">Dashboard</h3>
@endsection
@section('content')
    @if(!$data['family']->is_active)
        <div class="row">
            <div class="col-xl-12">
                @include('elements.parent.widgets._upgrade')
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-xl-12">
            @include('elements.parent.widgets._whats_next')
        </div>
    </div>
    <!-- @todo - add logic for things here -->
        <div class="row">
            <div class="col-xl-6">
                @include('elements.parent.widgets._todo')
            </div>
            <div class="col-xl-6">
                @include('elements.parent.widgets._upcoming_appointment')
            </div>
        </div>
    <div class="row">
        <div class="col-xl-6">
            @include('elements.parent.widgets._efc')
        </div>
        <div class="col-xl-6">
            @include('elements.parent.widgets._family_info')
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            @include('elements.parent.widgets._family_cost')
        </div>
    </div>
@endsection
