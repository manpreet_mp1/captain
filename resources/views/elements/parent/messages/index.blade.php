@section('pageTitle', 'Messages')
@section('subheader')
    <h3 class="kt-subheader__title">Messages</h3>
@endsection
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <!--Begin::App-->
        <div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">
            <!--Begin:: App Aside Mobile Toggle-->
{{--            <button class="kt-app__aside-close" id="kt_chat_aside_close">--}}
{{--                <i class="la la-close"></i>--}}
{{--            </button>--}}
            <!--End:: App Aside Mobile Toggle-->
            {{--            <!--Begin:: App Aside-->--}}
{{--            <div class="kt-grid__item kt-app__toggle kt-app__aside kt-app__aside--lg kt-app__aside--fit" id="kt_chat_aside" style="opacity: 1;">--}}
{{--                <!--begin::Portlet-->--}}
{{--                <div class="kt-portlet kt-portlet--last">--}}
{{--                    <div class="kt-portlet__body">--}}
{{--                        <div class="kt-widget kt-widget--users kt-mt-20">--}}
{{--                            <div class="kt-scroll kt-scroll--pull ps ps--active-y" style="">--}}
{{--                                <div class="kt-widget__items">--}}
{{--                                    <!-- Start Item -->--}}
{{--                                    <div class="kt-widget__item">--}}
{{--                                        <span class="kt-media kt-media--circle">--}}
{{--                                            <img src="./assets/media/users/300_9.jpg" alt="image">--}}
{{--                                        </span>--}}
{{--                                        <div class="kt-widget__info">--}}
{{--                                            <div class="kt-widget__section">--}}
{{--                                                <a href="#" class="kt-widget__username">Matt Pears</a>--}}
{{--                                                <span class="kt-badge kt-badge--success kt-badge--dot"></span>--}}
{{--                                            </div>--}}
{{--                                            <span class="kt-widget__desc">--}}
{{--                                                Head of Development--}}
{{--                                            </span>--}}
{{--                                        </div>--}}
{{--                                        <div class="kt-widget__action">--}}
{{--                                            <span class="kt-widget__date">36 Mines</span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <!-- End Item -->--}}
{{--                                </div>--}}
{{--                                <div class="ps__rail-x" style="left: 0px; bottom: 0px;">--}}
{{--                                    <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>--}}
{{--                                </div>--}}
{{--                                <div class="ps__rail-y" style="top: 0px; height: 430px; right: -2px;">--}}
{{--                                    <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 160px;"></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <!--end::Portlet-->--}}
{{--            </div>--}}
{{--            <!--End:: App Aside-->--}}

            <!--Begin:: App Content-->
            <div class="kt-grid__item kt-grid__item--fluid kt-app__content_new" id="kt_chat_content">
                <div class="kt-chat">
                    <div class="kt-portlet kt-portlet--head-lg kt-portlet--last">
                        <div class="kt-portlet__head">
                            <div class="kt-chat__head ">
                                <div class="kt-chat__left">
                                    <!--begin:: Aside Mobile Toggle -->
                                    <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md kt-hidden-desktop" id="kt_chat_aside_mobile_toggle">
                                        <i class="flaticon2-open-text-book"></i>
                                    </button>
                                    <!--end:: Aside Mobile Toggle-->
                                </div>

                                <div class="kt-chat__center">
                                    <div class="kt-chat__label">
                                        Messages
                                    </div>
                                    <div class="kt-chat__pic kt-hidden">
                                    </div>
                                </div>

                                <div class="kt-chat__right"></div>
                            </div>
                        </div>

                        <div class="kt-portlet__body">
                            <div class="kt-scroll kt-scroll--pull ps ps--active-y" data-mobile-height="300" style="height: 252px; overflow: hidden;">
                                <div class="kt-chat__messages">
                                    <div class="kt-chat__message">
                                        <div class="kt-chat__user">
                                            <span class="kt-media kt-media--circle kt-media--sm">
                                                <img src="./assets/media/users/100_12.jpg" alt="image">
                                            </span>
                                            <a href="#" class="kt-chat__username">Jason Muller</a>
                                            <span class="kt-chat__datetime">2 Hours</span>
                                        </div>
                                        <div class="kt-chat__text kt-bg-light-success">
                                            How likely are you to recommend our company
                                            <br>
                                            to your friends and family?
                                        </div>
                                    </div>
                                    <div class="kt-chat__message kt-chat__message--right">
                                        <div class="kt-chat__user">
                                            <span class="kt-chat__datetime">30 Seconds</span>
                                            <a href="#" class="kt-chat__username">You</a>
                                            <span class="kt-media kt-media--circle kt-media--sm">
                                        <img src="./assets/media/users/300_21.jpg" alt="image">
                                    </span>
                                        </div>
                                        <div class="kt-chat__text kt-bg-light-brand">
                                            Hey there, we’re just writing to let you know
                                            <br>
                                            that you’ve been subscribed to a repository on GitHub.
                                        </div>
                                    </div>
                                    <div class="kt-chat__message">
                                        <div class="kt-chat__user">
                                            <span class="kt-media kt-media--circle kt-media--sm">
                                                <img src="./assets/media/users/100_12.jpg" alt="image">
                                            </span>
                                            <a href="#" class="kt-chat__username">Jason Muller</a>
                                            <span class="kt-chat__datetime">30 Seconds</span>
                                        </div>
                                        <div class="kt-chat__text kt-bg-light-success">
                                            Ok, Understood!
                                        </div>
                                    </div>
                                    <div class="kt-chat__message kt-chat__message--right">
                                        <div class="kt-chat__user">
                                            <span class="kt-chat__datetime">Just Now</span>
                                            <a href="#" class="kt-chat__username">You</a>
                                            <span class="kt-media kt-media--circle kt-media--sm">
                                        <img src="./assets/media/users/300_21.jpg" alt="image">
                                    </span>
                                        </div>
                                        <div class="kt-chat__text kt-bg-light-brand">
                                            You’ll receive notifications for all issues, pull requests!
                                        </div>
                                    </div>
                                    <div class="kt-chat__message">
                                        <div class="kt-chat__user">
                                            <span class="kt-media kt-media--circle kt-media--sm">
                                                <img src="./assets/media/users/100_12.jpg" alt="image">
                                            </span>
                                            <a href="#" class="kt-chat__username">Jason Muller</a>
                                            <span class="kt-chat__datetime">2 Hours</span>
                                        </div>
                                        <div class="kt-chat__text kt-bg-light-success">
                                            You were automatically
                                            <b class="kt-font-brand">subscribed</b>
                                            <br>
                                            because you’ve been given access to the repository
                                        </div>
                                    </div>
                                    <div class="kt-chat__message kt-chat__message--right">
                                        <div class="kt-chat__user">
                                            <span class="kt-chat__datetime">30 Seconds</span>
                                            <a href="#" class="kt-chat__username">You</a>
                                            <span class="kt-media kt-media--circle kt-media--sm">
                                        <img src="./assets/media/users/300_21.jpg" alt="image">
                                    </span>
                                        </div>
                                        <div class="kt-chat__text kt-bg-light-brand">
                                            You can unwatch this repository immediately
                                            <br>
                                            by clicking here:
                                            <a href="#" class="kt-font-bold kt-link">https://github.com</a>
                                        </div>
                                    </div>
                                    <div class="kt-chat__message">
                                        <div class="kt-chat__user">
                                            <span class="kt-media kt-media--circle kt-media--sm">
                                                <img src="./assets/media/users/100_12.jpg" alt="image">
                                            </span>
                                            <a href="#" class="kt-chat__username">Jason Muller</a>
                                            <span class="kt-chat__datetime">30 Seconds</span>
                                        </div>
                                        <div class="kt-chat__text kt-bg-light-success">
                                            Discover what students who viewed Learn Figma - UI/UX Design
                                            <br>
                                            Essential Training also viewed
                                        </div>
                                    </div>
                                    <div class="kt-chat__message kt-chat__message--right">
                                        <div class="kt-chat__user">
                                            <span class="kt-chat__datetime">Just Now</span>
                                            <a href="#" class="kt-chat__username">You</a>
                                            <span class="kt-media kt-media--circle kt-media--sm">
                                        <img src="./assets/media/users/300_21.jpg" alt="image">
                                    </span>
                                        </div>
                                        <div class="kt-chat__text kt-bg-light-brand">
                                            Most purchased Business courses during this sale!
                                        </div>
                                    </div>
                                </div>
                                <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                                    <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                                </div>
                                <div class="ps__rail-y" style="top: 0px; height: 252px; right: -2px;">
                                    <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 64px;"></div>
                                </div>
                            </div>
                        </div>

                        <div class="kt-portlet__foot">
                            <div class="kt-chat__input">
                                <div class="kt-chat__editor">
                                    <textarea style="height: 50px" placeholder="Type here..."></textarea>
                                </div>
                                <div class="kt-chat__toolbar">
                                    <div class="kt_chat__tools"></div>
                                    <div class="kt_chat__actions">
                                        <button type="button" class="btn btn-brand btn-md btn-upper btn-bold kt-chat__reply">reply</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--End:: App Content-->
        </div>
        <!--End::App-->    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        "use strict";

        // Class definition
        var KTAppChat = function () {
            var chatAsideEl;
            var chatContentEl;

            // Private functions
            var initAside = function () {
                // Mobile offcanvas for mobile mode
                var offcanvas = new KTOffcanvas(chatAsideEl, {
                    overlay: true,
                    baseClass: 'kt-app__aside',
                    closeBy: 'kt_chat_aside_close',
                    toggleBy: 'kt_chat_aside_mobile_toggle'
                });

                // User listing
                var userListEl = KTUtil.find(chatAsideEl, '.kt-scroll');
                if (!userListEl) {
                    return;
                }

                // Initialize perfect scrollbar(see:  https://github.com/utatti/perfect-scrollbar)
                KTUtil.scrollInit(userListEl, {
                    mobileNativeScroll: true,  // enable native scroll for mobile
                    desktopNativeScroll: false, // disable native scroll and use custom scroll for desktop
                    resetHeightOnDestroy: true,  // reset css height on scroll feature destroyed
                    handleWindowResize: true, // recalculate hight on window resize
                    rememberPosition: true, // remember scroll position in cookie
                    height: function () {  // calculate height
                        var height;
                        var portletBodyEl = KTUtil.find(chatAsideEl, '.kt-portlet > .kt-portlet__body');
                        var widgetEl = KTUtil.find(chatAsideEl, '.kt-widget.kt-widget--users');
                        var searchbarEl = KTUtil.find(chatAsideEl, '.kt-searchbar');

                        if (KTUtil.isInResponsiveRange('desktop')) {
                            height = KTLayout.getContentHeight();
                        } else {
                            height = KTUtil.getViewPort().height;
                        }

                        if (chatAsideEl) {
                            height = height - parseInt(KTUtil.css(chatAsideEl, 'margin-top')) - parseInt(KTUtil.css(chatAsideEl, 'margin-bottom'));
                            height = height - parseInt(KTUtil.css(chatAsideEl, 'padding-top')) - parseInt(KTUtil.css(chatAsideEl, 'padding-bottom'));
                        }

                        if (widgetEl) {
                            height = height - parseInt(KTUtil.css(widgetEl, 'margin-top')) - parseInt(KTUtil.css(widgetEl, 'margin-bottom'));
                            height = height - parseInt(KTUtil.css(widgetEl, 'padding-top')) - parseInt(KTUtil.css(widgetEl, 'padding-bottom'));
                        }

                        if (portletBodyEl) {
                            height = height - parseInt(KTUtil.css(portletBodyEl, 'margin-top')) - parseInt(KTUtil.css(portletBodyEl, 'margin-bottom'));
                            height = height - parseInt(KTUtil.css(portletBodyEl, 'padding-top')) - parseInt(KTUtil.css(portletBodyEl, 'padding-bottom'));
                        }

                        if (searchbarEl) {
                            height = height - parseInt(KTUtil.css(searchbarEl, 'height'));
                            height = height - parseInt(KTUtil.css(searchbarEl, 'margin-top')) - parseInt(KTUtil.css(searchbarEl, 'margin-bottom'));
                        }

                        // remove additional space
                        height = height - 5;

                        return height;
                    }
                });
            }

            return {
                // public functions
                init: function () {
                    // elements
                    chatAsideEl = KTUtil.getByID('kt_chat_aside');

                    // init aside and user list
                    initAside();

                    // init inline chat example
                    KTChat.setup(KTUtil.getByID('kt_chat_content'));

                    // trigger click to show popup modal chat on page load
                    if (KTUtil.getByID('kt_app_chat_launch_btn')) {
                        setTimeout(function () {
                            KTUtil.getByID('kt_app_chat_launch_btn').click();
                        }, 1000);
                    }
                }
            };
        }();

        $(document).ready(function () {
            KTUtil.ready(function () {
                KTAppChat.init();
            });
        });
    </script>
@endsection
