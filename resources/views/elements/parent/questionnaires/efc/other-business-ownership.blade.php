@section('pageTitle', 'Business/Farm')
@section('subheader')
    <h3 class="kt-subheader__title">EFC</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <span class="kt-subheader__desc">Business/Farm</span>
@endsection
@section('content')
    {!! Form::model($data['request'], [
           'method'=>'POST',
           'id'=> 'assessment-form',
           'class'=> 'kt-margin-t-30',
           'name'=>'assessment-form'
        ]) !!}
    <div class="row">
        <div class="col-md-12">
            @include('elements.parent.familyInfo.tables._other_business_family', ['cellMinWidth' => 0])
        </div>
    </div>
    @include('elements.parent.questionnaires.common._continue_button')
    {!! Form::close() !!}
@endsection