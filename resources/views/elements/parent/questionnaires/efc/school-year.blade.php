@section('pageTitle', 'School Year')
@section('subheader')
    <h3 class="kt-subheader__title">EFC</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <span class="kt-subheader__desc">School Year</span>
@endsection
@section('content')
    {!! Form::model($data['request'], [
           'method'=>'POST',
           'id'=> 'assessment-form',
           'class'=> 'kt-margin-t-30',
           'name'=>'assessment-form'
        ]) !!}
    <div class="row">
        <div class="col-md-12">
            <h2 class="font-weight-light kt-margin-b-20">
                What school year should we use to calculate your household’s Estimated Family Contribution (EFC)?
                <sup><span class="error font-s12 text-danger">*Required</span></sup>
            </h2>
        </div>
        <div class="col-md-6">
            {!! Form::select('family[efc_school_year]', $data['schoolYear'], null, ['class'=>'form-control form-control-lg', 'required' => 'true']) !!}
        </div>
        <div class="col-md-6">
        </div>
    </div>
    @include('elements.parent.questionnaires.common._continue_button')
    {!! Form::close() !!}
@endsection