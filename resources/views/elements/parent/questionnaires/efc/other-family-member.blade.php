@section('pageTitle', 'Family Members')
@section('subheader')
    <h3 class="kt-subheader__title">EFC</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <span class="kt-subheader__desc">Family Member</span>
@endsection
@section('content')
    {!! Form::model($data['request'], [
           'method'=>'POST',
           'id'=> 'assessment-form',
           'class'=> 'kt-margin-t-30',
           'name'=>'assessment-form'
        ]) !!}
    @php
        $parentOne = $data['family']->parentOne();
        $parentTwo = $data['family']->parentTwo();
    @endphp
    <div class="row">
        <div class="col-md-12">
            <p class="lead text-brand text-primary">
                The colleges will also want to know if you have any other family members (like an elderly parent) that
                reside in your household that derives more than 50% of their support from
                you @if($parentTwo)
                    and {{$parentTwo->user->first_name}} @endif?
            </p>
            @include('elements.forms._input_image', [
                'title' => 'Do you have any other family members in your household?',
                'type' => "radio",
                'required' => 'true',
                'column' => 'family[is_other_family_members]',
                'class' => 'is_other_family_members',
                'modelData' => $data['family']->is_other_family_members,
                'values' => [
                    ['label' => 'Yes', 'value' => '1', 'image' => 'images/forms/yes.png'],
                    ['label' => 'No', 'value' => '0', 'image' => 'images/forms/no.png'],
                ]
            ])
        </div>
    </div>
    <div id="other_family_members_div">
        @include('elements.parent.questionnaires.common._divider')
        <div class="row">
            <div class="col-md-12">
                <h2 class="font-weight-light kt-margin-b-20">
                    How many additional family members do you and @if($parentTwo)
                        and {{$parentTwo->user->first_name}} @endif support?
                    <sup><span class="error font-s12 text-danger">*Required</span></sup>
                </h2>
            </div>
            <div class="col-md-6">
                {!! Form::select('family[other_family_members_num]', $data['dropDownNumberTillFour'], $data['family']->other_family_members_num, ['class'=>'form-control form-control-lg', 'required' => 'true']) !!}
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
    @include('elements.parent.questionnaires.common._continue_button')
    {!! Form::close() !!}
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            showHideDiv();
            $("input.is_other_family_members").click(function () {
                showHideDiv();
            });
        });

        function showHideDiv() {
            hideAllAndMakeNotRequired();
            // Get All the checked values for checkbox
            let selectedAsset = [];
            $('.is_other_family_members:checked').each(function () {
                selectedAsset.push($(this).val());
            });
            if (selectedAsset.includes("1")) {
                $("#other_family_members_div").show();
            }
        }

        function hideAllAndMakeNotRequired() {
            $("#other_family_members_div").hide();
        }
    </script>
@endsection