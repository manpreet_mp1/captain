@section('pageTitle', 'Income Tax')
@section('subheader')
    <h3 class="kt-subheader__title">EFC</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <span class="kt-subheader__desc">Income Tax</span>
@endsection
@section('content')
    {!! Form::model($data['request'], [
           'method'=>'POST',
           'id'=> 'assessment-form',
           'class'=> 'kt-margin-t-30',
           'name'=>'assessment-form'
        ]) !!}
    <div class="row">
        <div class="col-md-12">
            @include('elements.parent.familyInfo.tables._income_tax_parent', ['cellMinWidth' => 0])
        </div>
        <div class="col-md-12">
            @include('elements.parent.familyInfo.tables._income_tax_student', ['cellMinWidth' => 0, 'onlyShowInCollegeStudent' => 1])
        </div>
    </div>
    @include('elements.parent.questionnaires.common._continue_button')
    {!! Form::close() !!}
@endsection