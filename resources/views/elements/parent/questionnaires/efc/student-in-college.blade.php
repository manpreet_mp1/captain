@section('pageTitle', 'Student In College')
@section('subheader')
    <h3 class="kt-subheader__title">EFC</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <span class="kt-subheader__desc">Student In College</span>
@endsection
@section('content')
    {!! Form::model($data['request'], [
           'method'=>'POST',
           'id'=> 'assessment-form',
           'class'=> 'kt-margin-t-30',
           'name'=>'assessment-form',
           'onsubmit' => "return minOneCheckbox(event)"
        ]) !!}
    <div class="row">
        <div class="col-md-12">
            <p class="lead text-brand text-primary">
                You mentioned that you have {{ $data['family']->children->count() }}
                child/children in your household.
            </p>
            <h2 class="font-weight-light kt-margin-b-20">
                Please select the child/children that will be in college during {{ $data['family']->efc_school_year }}
                <sup><span class="error font-s12 text-danger">*Required</span></sup>
            </h2>
        </div>
        <div class="col-md-10">
            <div class="kt-checkbox-list">
                @foreach($data['family']->children as $child)
                    <label class="kt-checkbox kt-checkbox--brand">
                        {!! Form::checkbox('in_college[]', $child->id, []) !!} {{ $child->user->first_name }}
                        <span></span>
                    </label>
                @endforeach
            </div>
        </div>
    </div>
    @include('elements.parent.questionnaires.common._continue_button')
    {!! Form::close() !!}
@endsection
@section('scripts')
    <script type="text/javascript">
        function minOneCheckbox(e) {
            if ($("input[type=checkbox]:checked").length === 0) {
                e.preventDefault();
                Swal.fire(
                    "You need to select at least one child that will be attending college in {{ $data['family']->efc_school_year }}",
                    '',
                    'error'
                );
                return false;
            }
        }
    </script>
@endsection