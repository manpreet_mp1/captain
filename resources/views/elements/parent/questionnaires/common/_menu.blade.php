<div class="kt-grid__item kt-wizard-v2__aside">
    <div class="kt-wizard-v2__nav">
        <div class="kt-wizard-v2__nav-items">
            @foreach($data['formBladeMenu'] as $menu)
            <a class="kt-wizard-v2__nav-item" href="{{ $menu->href }}" data-ktwizard-type="step" data-ktwizard-state="{{ $menu->active_class }}">
                <div class="kt-wizard-v2__nav-body">
                    <div class="kt-wizard-v2__nav-icon">
                        <i class="{{ $menu->icon_class }}"></i>
                    </div>
                    <div class="kt-wizard-v2__nav-label">
                        <div class="kt-wizard-v2__nav-label-title">
                            {{ $menu->menu_name }}
                        </div>
                        <div class="kt-wizard-v2__nav-label-desc">
                            {{ $menu->menu_sub_text }}
                        </div>
                    </div>
                </div>
            </a>
            @endforeach
        </div>
    </div>
</div>

{{--<!-- begin:: Aside Menu -->--}}
{{--<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">--}}
{{--    <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1"--}}
{{--         data-ktmenu-dropdown-timeout="500">--}}
{{--        <ul class="kt-menu__nav ">--}}
{{--            @foreach($data['formBladeMenu'] as $menu)--}}
{{--                <li class="kt-menu__item {{ $menu->active_class }}">--}}
{{--                    <a href="{{ $menu->href }}" class="kt-menu__link ">--}}
{{--                        <span class="kt-menu__link-icon">--}}
{{--                            <i class="{{ $menu->icon_class }}"></i>--}}
{{--                        </span>--}}
{{--                        <span class="kt-menu__link-text">--}}
{{--                            {{ $menu->menu_name }}--}}
{{--                        </span>--}}
{{--                        @if($menu->is_completed === 1)--}}
{{--                            <span class="kt-menu__link-badge">--}}
{{--                                <span class="kt-badge kt-badge--square kt-badge--success">--}}
{{--                                    <i class="fas fa-check"></i>--}}
{{--                                </span>--}}
{{--                            </span>--}}
{{--                        @endif--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--            @endforeach--}}
{{--        </ul>--}}
{{--    </div>--}}
{{--</div>--}}
{{--<!-- end:: Aside Menu -->--}}