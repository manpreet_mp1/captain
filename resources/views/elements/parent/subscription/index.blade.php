@section('pageTitle', 'Subscriptions')
@section('subheader')
    <h3 class="kt-subheader__title">Subscriptions</h3>
@endsection
@section('content')
    <div class="kt-portlet">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-pricing-2">
                <div class="kt-pricing-2__head bg-primary">
                    <div class="kt-pricing-2__title kt-font-light">
                        <h1>Subscription Pricing</h1>
                    </div>
                </div>
                <div class="tab-content">
                    <div class="tab-pane active" id="kt-pricing-2_content1" aria-expanded="true">
                        <div class="kt-pricing-2__content">
                            <div class="kt-pricing-2__container">
                                <div class="kt-pricing-2__items row">
                                    <div class="kt-pricing-2__item col-lg-4 @if(!$data['family']->is_qualified) offset-lg-4 @endif">
                                        <div class="kt-pricing-2__visual">
                                            <div class="kt-pricing-2__hexagon"></div>
                                            <span class="kt-pricing-2__icon kt-font-info"><i class="fa flaticon-confetti"></i></span>
                                        </div>
                                        <h2 class="kt-pricing-2__subtitle text-uppercase">{{ config('app.shortName') }}</h2>
                                        <div class="kt-pricing-2__features">
                                            <span>Lorem ipsum dolor sit ame</span>
                                            <span>Lorem ipsum dolor sit ame</span>
                                            <span>Lorem ipsum dolor sit ame</span>
                                            <span>Lorem ipsum dolor sit ame</span>
                                        </div>
                                        <span class="kt-pricing-2__label">$</span>
                                        <span class="kt-pricing-2__price kt-padding-l-20">{{ number_format($data['pricing']['products']['smarttrack']->price/100, 0) }}</span>
                                        <div class="kt-pricing-2__btn">
                                            <a href="{{ route($routeName, array_merge(['slug' => 'smarttrack'], $additionalParam)) }}"
                                                class="btn btn-brand btn-wide btn-uppercase btn-bolder btn-lg text-white">
                                                Purchase
                                            </a>
                                            @if($showSubscriptionPlans === 1)
                                                <br />
                                                <a href="{{ route($checkoutPaymentPlanRouteName, array_merge(['slug' => 'smarttrack-3-payment'], $additionalParam)) }}"
                                                    class="btn btn-brand btn-wide btn-uppercase btn-bolder btn-lg text-white kt-margin-t-20">
                                                    3 Payment Plan
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                    @if($data['family']->is_qualified)
                                        <div class="kt-pricing-2__item col-lg-4">
                                        <div class="kt-pricing-2__visual">
                                            <div class="kt-pricing-2__hexagon"></div>
                                            <span class="kt-pricing-2__icon kt-font-info"><i class="fa  flaticon-rocket"></i></span>
                                        </div>
                                        <h2 class="kt-pricing-2__subtitle text-uppercase">{{ config('app.shortName') }} Advisor</h2>
                                        <div class="kt-pricing-2__features">
                                            <span>Lorem ipsum dolor sit ame</span>
                                            <span>Lorem ipsum dolor sit ame</span>
                                            <span>Lorem ipsum dolor sit ame</span>
                                            <span>Lorem ipsum dolor sit ame</span>
                                        </div>
                                        <span class="kt-pricing-2__label">$</span>
                                        <span class="kt-pricing-2__price kt-padding-l-20">{{ number_format($data['pricing']['products']['smarttrackAdvisor']->price/100, 0) }}</span>
                                        <div class="kt-pricing-2__btn">
                                            <a href="{{ route($routeName, array_merge(['slug' => 'smarttrack-advisor'], $additionalParam)) }}"
                                                class="btn btn-brand btn-wide btn-uppercase btn-bolder btn-lg text-white">
                                                Purchase
                                            </a>
                                            @if($showSubscriptionPlans === 1)
                                                <br />
                                                <a href="{{ route($checkoutPaymentPlanRouteName, array_merge(['slug' => 'smarttrack-advisor-3-payment'], $additionalParam)) }}"
                                                    class="btn btn-brand btn-wide btn-uppercase btn-bolder btn-lg text-white kt-margin-t-20">
                                                    3 Payment Plan
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                        <div class="kt-pricing-2__item col-lg-4">
                                        <div class="kt-pricing-2__visual">
                                            <div class="kt-pricing-2__hexagon"></div>
                                            <span class="kt-pricing-2__icon kt-font-info"><i class="fa flaticon-gift"></i></span>
                                        </div>
                                        <h2 class="kt-pricing-2__subtitle text-uppercase">Bundle</h2>
                                        <div class="kt-pricing-2__features">
                                            <span>Lorem ipsum dolor sit ame</span>
                                            <span>Lorem ipsum dolor sit ame</span>
                                            <span>Lorem ipsum dolor sit ame</span>
                                            <span>Lorem ipsum dolor sit ame</span>
                                        </div>
                                        <span class="kt-pricing-2__label">$</span>
                                        <span class="kt-pricing-2__price kt-padding-l-20">{{ number_format($data['pricing']['products']['bundle']->price/100, 0) }}</span>
                                        <div class="kt-pricing-2__btn">
                                            <a href="{{ route($routeName, array_merge(['slug' => 'bundle'], $additionalParam)) }}"
                                                class="btn btn-brand btn-wide btn-uppercase btn-bolder btn-lg text-white">
                                                Purchase
                                            </a>
                                            @if($showSubscriptionPlans === 1)
                                                <br />
                                                <a href="{{ route($checkoutPaymentPlanRouteName, array_merge(['slug' => 'bundle-3-payment'], $additionalParam)) }}"
                                                    class="btn btn-brand btn-wide btn-uppercase btn-bolder btn-lg text-white kt-margin-t-20">
                                                    3 Payment Plan
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
