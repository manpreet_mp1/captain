@section('pageTitle', 'Thank You')
@section('subheader')
    <h3 class="kt-subheader__title">Thank You</h3>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head bg-primary text-white">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Thank You
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    Copy needs to provide content
                </div>
            </div>

        </div>
    </div>
@endsection
