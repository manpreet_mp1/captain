@section('pageTitle', 'Subscriptions')
@section('subheader')
    <h3 class="kt-subheader__title">Checkout</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <span class="kt-subheader__desc">{{ config('services.stripe.mapping')[$data['product']->id] }}</span>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-6">
            @component('components.creditCard', [
                'route' => $subscribeRouteName,
                'additionalParam' => array_merge($additionalParam, [
                        'slug' => $data['slug']
                    ]),
                'hiddenFields' => [
                    ['name' => 'sku', 'value' => config('services.stripe.slugMapping')[$data['slug']]]
                ],
            ])
            @endcomponent
        </div>
        <div class="col-lg-3">
                @component('components.coupon', [
                    'route' => $couponRouteName,
                    'additionalParam' => array_merge($additionalParam, [
                        'slug' => $data['slug']
                    ]),
                    'price' => $data['product']->price,
                    'discount' => $data['discount'],
                    'dueToday' => $data['dueToday'],
                    'couponCode' => $data['coupon_code'],
                     'hiddenFields' => [
                        ['name' => 'is_subscription', 'value' => 0]
                    ],
                ])
                @endcomponent
        </div>
    </div>
@endsection
