@section('pageTitle', 'Subscriptions')
@section('subheader')
    <h3 class="kt-subheader__title">Checkout</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <span class="kt-subheader__desc">{{ config('services.stripe.mapping')[$data['plan']->id] }}</span>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-6">
            @component('components.creditCard', [
                'route' => $subscribeRouteName,
                'additionalParam' => array_merge($additionalParam, [
                        'slug' => $data['slug']
                    ]),
                'hiddenFields' => [
                    ['name' => 'id', 'value' => config('services.stripe.slugMapping')[$data['slug']]],
                    ['name' => 'months', 'value' => $data['months']],
                ],
            ])
            @endcomponent
        </div>
        <div class="col-lg-3">
                @component('components.coupon', [
                    'route' => $couponRouteName,
                    'additionalParam' => array_merge($additionalParam, [
                        'slug' => $data['slug']
                    ]),
                    'price' => $data['plan']->amount,
                    'months' => $data['months'],
                    'discount' => $data['discount'],
                    'dueToday' => $data['dueToday'],
                    'couponCode' => $data['coupon_code'],
                     'hiddenFields' => [
                        ['name' => 'is_subscription', 'value' => 1]
                    ],
                ])
                @endcomponent
        </div>
    </div>
@endsection
