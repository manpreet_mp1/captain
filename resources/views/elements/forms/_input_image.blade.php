@php $type = isset($type) ? $type : 'radio' @endphp
@php
    $labelStyle = isset($labelStyle) ? $labelStyle : '';
    $class = isset($class) ? $class : $column;
@endphp
@if(isset($title))
    <h2 class="font-weight-light kt-margin-b-20">
        {!! $title !!}
        @if($required==="true")
            <sup><span class="error font-s12 text-danger">*Required</span></sup>
        @endif
    </h2>
@endif
<div class="row">
    <div class="{{$type}}-input col-md-12">
        @php $i = 1; @endphp
        @foreach($values as $value)
            <div class="holder-{{ $column }}-{{ $i }} holder-{{ $type }}">
                @if($type === 'radio')
                    @php
                        $modelData = isset($modelData) ? $modelData : null;
                        $isChecked = $value['value'] == $modelData ? true : false;
                    @endphp
                    {!! Form::radio("{$column}", "{$value['value']}", $isChecked, [
                       "id" => "{$column}-{$i}",
                       "class" => "input-image-hidden image-{$type}-input {$class}",
                       "required" => "{$required}",
                    ]) !!}
                @else
                    @php
                        if (is_array($modelData)) {
                            $isChecked = in_array($value['value'], $modelData) ? true : false;
                        } else {
                            $isChecked = false;
                        }
                    @endphp
                    {!! Form::checkbox("{$column}[]", "{$value['value']}", $isChecked, [
                       "id" => "{$column}-{$i}",
                       "class" => "input-image-hidden image-{$type}-input {$class}",
                    ]) !!}
                @endif
                <label for="{{ $column }}-{{ $i }}" class="image-input text-center" style="{{ $labelStyle }}">
                    <img src="{{ config('app.cdn') }}{!! $value['image'] !!}"> <br/>
                    {!! $value['label'] !!}
                </label>
            </div>
            <!-- End-->
            @php $i++ @endphp
        @endforeach
    </div>
</div>
<style>
    .image-input {
        border-radius: 12px;
        background-color: #ffffff;
        box-shadow: 0 2px 10px 0 #d4d7dc;
        padding: 20px;
        float: left;
        margin-right: 20px;
        margin-bottom: 20px;
        text-align: center;
        width: 180px;
        min-height: 165px;
    }
    .input-image-hidden {
        position: absolute;
        left: -9999px;
    }
    /* Stuff after this is only to make things more pretty */
    input[type=radio].image-radio-input + label>img,
    input[type=checkbox].image-checkbox-input + label>img {
        width: 80px;
        height: 80px;
        transition: 200ms all;
        margin: 10px;
    }
    input[type=radio].image-radio-input + label,
    input[type=checkbox].image-checkbox-input + label {
        border: 2px solid #fff;
        text-transform: none !important;
        font-weight: 400;
    }
    input[type=radio].image-radio-input:checked + label,
    input[type=checkbox].image-checkbox-input:checked + label {
        border: 2px solid #6239bd;
    }
    input[type=radio].image-radio-input:checked + label>img,
    input[type=checkbox].image-checkbox-input:checked + label>img {
        width: 60px;
        height: 60px;
        margin: 20px;
        content:url("{{ config('app.cdn') }}images/forms/check.png");
    }
</style>
