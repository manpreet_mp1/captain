<h2 class="font-weight-light kt-margin-b-20">
    {!! $title !!}
    @if($required==="required")
        <sup><span class="error font-s12 text-danger">*Required</span></sup>
    @endif
</h2>
<div class="row">
    <div class="col-md-12">
        <p class="text-left pull-left text-uppercase">
            {{ $textLeft }}
        </p>
        <p class="text-right pull-right text-uppercase">
            {{ $textRight }}
        </p>
    </div>
    <div class="radio-input btn-group btn-group-lg col-md-12" role="group">
        @foreach(range(0, 10) as $value)
            <div class="holder-{{ $column }}-{{ $value }} holder-radio" style="width: 9%">
                {!! Form::radio("{$column}", "{$value}", false, [
                      "id" => "{$column}-{$value}",
                      "class" => "input-feedback-hidden feedback-radio-input",
                      "required" => "{$required}",
                   ]) !!}
                <label for="{{ $column }}-{{$value}}" class="btn btn-outline-success" style="width: 100%">
                    {{ $value }}
                </label>
            </div>
        @endforeach
    </div>
</div>
<style>
    .input-feedback-hidden {
        position: absolute;
        left: -9999px;
    }
    /* Stuff after this is only to make things more pretty */
    input[type=radio].feedback-radio-input + label,
    input[type=checkbox].feedback-checkbox-input + label {
        transition: 200ms all;
        padding: 1.15rem 1.65rem;
        font-size: 1.25rem;
        line-height: 1.5;
        border-radius: 0rem;
    }
    input[type=radio].feedback-radio-input + label,
    input[type=checkbox].feedback-checkbox-input + label {
        /*border: 2px solid #fff;*/
        /*text-transform: none !important;*/
        /*font-weight: 400;*/
    }
    input[type=radio].feedback-radio-input:checked + label,
    input[type=checkbox].feedback-checkbox-input:checked + label {
        border: 1px solid #6239bd;
        background: #6239bd;
        color: #fff;
    }
</style>