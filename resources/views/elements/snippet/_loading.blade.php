<div class="blockUI" style="display:none"></div>
<div class="blockUI blockOverlay"
    style="z-index: 10; border: none; margin: 0px; padding: 0px; width: 100%; height: 100%; top: 0px; left: 0px; background-color: rgb(255, 255, 255); opacity: 0.3; cursor: wait; position: absolute;"></div>
<div class="blockUI blockMsg blockElement"
    style="z-index: 1011; position: absolute; padding: 0px; margin: 0px; width: 160px; top: 50%; left: {{ $leftPercent }}%; text-align: center; color: rgb(0, 0, 0); border: 0px; cursor: wait;">
    <div class="m-blockui">
        <span>
            <div class="kt-spinner kt-spinner--sm kt-spinner--brand"></div>
        </span>
    </div>
</div>
