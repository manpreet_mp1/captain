<div class="card">
    <div class="card-header" id="{{ $headingDivId }}">
        <div class="card-title collapsed" data-toggle="collapse" data-target="#{{ $questionId }}"
             aria-expanded="false" aria-controls="{{ $questionId }}">
            {{ $question }}
        </div>
    </div>
    <div id="{{ $questionId }}" class="collapse" aria-labelledby="{{ $headingDivId }}"
         data-parent="#{{ $parentDivId }}" style="">
        <div class="card-body">
            {!! $answer !!}
        </div>
    </div>
</div>