{!! Form::model($data['scenario'], ['method' => 'post' ]) !!}
<div class="kt-grid  kt-wizard-v2 kt-wizard-v2--white" id="kt_wizard_v2" data-ktwizard-state="step-first">
    <div class="kt-grid__item kt-wizard-v2__aside" style="padding:0.5rem 2.5rem 4.5rem 1.5rem">
        <!--begin: Form Wizard Nav -->
        <div class="kt-wizard-v2__nav">
            <div class="kt-wizard-v2__nav-items">
                <a class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step" data-ktwizard-state="current">
                    <div class="kt-wizard-v2__nav-body">
                        <div class="kt-wizard-v2__nav-icon">
                            <i class="blueprintone-graduation19 text-primary"></i>
                        </div>
                        <div class="kt-wizard-v2__nav-label">
                            <div class="kt-wizard-v2__nav-label-title font-s18 kt-margin-t-5 text-uppercase">
                                College Choices
                            </div>
                        </div>
                    </div>
                </a>
                <a class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
                    <div class="kt-wizard-v2__nav-body">
                        <div class="kt-wizard-v2__nav-icon">
                            <i class="blueprintone-couple82 text-primary"></i>
                        </div>
                        <div class="kt-wizard-v2__nav-label">
                            <div class="kt-wizard-v2__nav-label-title font-s18 kt-margin-t-5 text-uppercase">
                                Parent Data
                            </div>
                        </div>
                    </div>
                </a>
                <a class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
                    <div class="kt-wizard-v2__nav-body">
                        <div class="kt-wizard-v2__nav-icon">
                            <i class="blueprintone-college6 text-primary"></i>
                        </div>
                        <div class="kt-wizard-v2__nav-label">
                            <div class="kt-wizard-v2__nav-label-title font-s18 kt-margin-t-5 text-uppercase">
                                Student Data
                            </div>
                        </div>
                    </div>
                </a>
                <a class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
                    <div class="kt-wizard-v2__nav-body">
                        <div class="kt-wizard-v2__nav-icon">
                            <i class="blueprintone-money215 text-primary"></i>
                        </div>
                        <div class="kt-wizard-v2__nav-label">
                            <div class="kt-wizard-v2__nav-label-title font-s18 kt-margin-t-5 text-uppercase">
                                Planned Contributions
                            </div>
                        </div>
                    </div>
                </a>
                <a class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
                    <div class="kt-wizard-v2__nav-body">
                        <div class="kt-wizard-v2__nav-icon">
                            <i class="blueprinttwo-money315 text-primary"></i>
                        </div>
                        <div class="kt-wizard-v2__nav-label">
                            <div class="kt-wizard-v2__nav-label-title font-s18 kt-margin-t-5 text-uppercase">
                                {{ config('app.shortName') }} Savings
                            </div>
                        </div>
                    </div>
                </a>
                <a class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
                    <div class="kt-wizard-v2__nav-body">
                        <div class="kt-wizard-v2__nav-icon">
                            <i class="fa fa-cog text-primary"></i>
                        </div>
                        <div class="kt-wizard-v2__nav-label">
                            <div class="kt-wizard-v2__nav-label-title font-s18 kt-margin-t-5 text-uppercase">
                                Other
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <!--end: Form Wizard Nav -->
    </div>
    <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v2__wrapper">
        <!--begin: Form Wizard Form-->
        <div style="padding: 2rem;">
            <!--begin: Form Wizard Step 1-->
            <div class="kt-wizard-v2__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
                @include('elements.advisor.quickPlan.tabs._college')
            </div>
            <!--end: Form Wizard Step 1-->

            <!--begin: Form Wizard Step 2-->
            <div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
                @include('elements.advisor.quickPlan.tabs._parent')
            </div>
            <!--end: Form Wizard Step 2-->

            <!--begin: Form Wizard Step 3-->
            <div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
                @include('elements.advisor.quickPlan.tabs._student')
            </div>
            <!--end: Form Wizard Step 3-->

            <!--begin: Form Wizard Step 4-->
            <div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
                @include('elements.advisor.quickPlan.tabs._contribution')
            </div>
            <!--end: Form Wizard Step 4-->

            <!--begin: Form Wizard Step 5-->
            <div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
                @include('elements.advisor.quickPlan.tabs._saving')
            </div>
            <!--end: Form Wizard Step 5-->

            <!--begin: Form Wizard Step 6-->
            <div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
                @include('elements.advisor.quickPlan.tabs._other')
            </div>
            <!--end: Form Wizard Step 6-->
        </div>
        <!--end: Form Wizard Form-->
    </div>
</div>
<div class="kt-divider">
    <span></span>
    <span></span>
</div>
<div class="row kt-margin-t-30">
    <div class="col-lg-12 text-center">
        <p>
            <label class="kt-checkbox kt-checkbox--brand">
                {!! Form::checkbox("is_shared_with_family", null, null, []) !!} SHARE SCENARIO WITH FAMILY
                <span></span>
            </label>
        </p>

        {!! Form::button('<span class="fa fa-calculator"></span> Re-calculate / Save', ['name' => 'save', 'class'=> 'btn btn-brand btn-elevate', 'type'=>'submit', 'value' => 'save' ]) !!}
    </div>
</div>

{!! Form::close() !!}

@section('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function () {
            // Initialize form wizard
            wizard = new KTWizard('kt_wizard_v2', {
                startStep: 1,
            });
            // Change event
            wizard.on('change', function (wizard) {
                var position = $("#kt_wizard_v2").offset().top - 210;
                $("body, html").animate({
                    scrollTop: position
                }, 'slow');
            });
            // Select 2
            $('.js-college-year-select').select2();
            $('.js-college-select').select2({
                placeholder: "Select a college",
                allowClear: false,
                ajax: {
                    url: "/api/lite/colleges",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            term: params.term
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: data,
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                },
                minimumInputLength: 4,
            });
        });
    </script>
@endsection
