{!! Form::model([], [ 'method'=>'POST',
         'id'=> 'assessment-form',
         'class'=> 'kt-margin-t-30',
         'name'=>'assessment-form'
      ]) !!}
<div class="kt-portlet">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                CREATE A NEW SCENARIO </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row kt-margin-b-10">
            <div class="col-lg-6">
                <h4 class="text-uppercase">
                    Scenario Name
                    <small>*</small>
                </h4>
                {!! Form::text('name', null, ['class' => 'form-control form-control-lg','placeholder' => 'Scenario Name', 'required'])!!}
            </div>
            <div class="col-lg-6">
                <h4 class="text-uppercase">
                    Select Student
                    <small>*</small>
                </h4>
                {!! Form::select('family_child_id', $data['students'], null, [ 'class' => 'form-control form-control-lg']) !!}
            </div>
        </div>
        <div class="row kt-margin-t-20">
            <div class="col-lg-12 ">
                <h4 class="text-uppercase kt-margin-b-10">
                    SUMMARY <small>*</small></h4>
                {!! Form::text('summary', null, [
                    'class' => 'form-control form-control-lg',
                    'placeholder' => 'Summary',
                    'required'
                    ])
                !!}
            </div>
        </div>
    </div>
    <div class="kt-portlet__foot">
        <div class="row align-items-center">
            <div class="col-lg-6 m--valign-middle">
                {!! Form::button('Create Scenario', [
                    'name' => 'Create',
                    'id' => 'Create',
                    'class'=> 'btn btn-brand btn-elevate-air',
                    'type'=>'submit',
                    'value' => 'Create'
            ]) !!}
            </div>
        </div>
    </div>

</div>{!! Form::close() !!}