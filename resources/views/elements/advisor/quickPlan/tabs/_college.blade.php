<div class="table-responsive no-border">
    <table class="table table-condensed table-left-right-border valign-center font-s18">
        <tr>
            <td class="text-right">Year Entering College</td>
            <td>
                {!! Form::select("college_year", $data['collegeYearDropdown'], null, ['class'=>'form-control form-control-lg border-grey bg-gray-light js-college-year-select', $data['inputDisabled']]) !!}
            </td>
        </tr>
        <tr>
            <td class="text-right">Select first college choice</td>
            <td>
                {!! Form::select("college_id_1", $data['college']['college_id_1'], null, ['class'=>'form-control form-control-lg border-grey bg-gray-light js-college-select', $data['inputDisabled']]) !!}
            </td>
        </tr>
        <tr>
            <td class="text-right">Select second college choice</td>
            <td>
                {!! Form::select("college_id_2", $data['college']['college_id_2'], null, ['class'=>'form-control form-control-lg border-grey bg-gray-light js-college-select', $data['inputDisabled']]) !!}
            </td>
        </tr>
        <tr>
            <td class="text-right">Select third college choice</td>
            <td>
                {!! Form::select("college_id_3", $data['college']['college_id_3'], null, ['class'=>'form-control form-control-lg border-grey bg-gray-light js-college-select', $data['inputDisabled']]) !!}
            </td>
        </tr>
        <tr>
            <td class="text-right">Select fourth college choice</td>
            <td>
                {!! Form::select("college_id_4", $data['college']['college_id_4'], null, ['class'=>'form-control form-control-lg border-grey bg-gray-light js-college-select', $data['inputDisabled']]) !!}
            </td>
        </tr>
    </table>
</div>

