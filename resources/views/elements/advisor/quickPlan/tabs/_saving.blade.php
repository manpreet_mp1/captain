<div class="table-responsive no-border">
    <table class="table table-condensed table-left-right-border valign-center font-s18">
        <tr class="bg-dark text-white">
            <th class="text-right">FINANCIAL PLANNING</th>
            <th class="text-right">Min Savings</th>
            <th class="text-right">Max Savings</th>
        </tr>
        @include('elements.advisor.quickPlan.tabs._range_row', [
            'title' => 'Tax Reduction & Planning',
            'dropdown' => $data['savingDropdown'],
            'columnOne' => 'savings_tax_planning_min',
            'columnTwo' => 'savings_tax_planning_max',
        ])
        @include('elements.advisor.quickPlan.tabs._range_row', [
            'title' => 'Asset Management',
            'dropdown' => $data['savingDropdown'],
            'columnOne' => 'savings_assets_management_min',
            'columnTwo' => 'savings_assets_management_max',
        ])
        @include('elements.advisor.quickPlan.tabs._range_row', [
            'title' => 'Borrowing & Loan Strategies',
            'dropdown' => $data['savingDropdown'],
            'columnOne' => 'savings_borrowing_loan_min',
            'columnTwo' => 'savings_borrowing_loan_max',
        ])
        @include('elements.advisor.quickPlan.tabs._range_row', [
            'title' => 'Cashflow Improvements',
            'dropdown' => $data['savingDropdown'],
            'columnOne' => 'savings_cashflow_improvement_min',
            'columnTwo' => 'savings_cashflow_improvement_max',
        ])
        @include('elements.advisor.quickPlan.tabs._range_row', [
            'title' => 'Other Savings ',
            'dropdown' => $data['savingDropdown'],
            'columnOne' => 'savings_other_min',
            'columnTwo' => 'savings_other_max',
        ])
        @include('elements.advisor.quickPlan.tabs._comment', [
            'title' => 'Comments/Notes',
            'column' => 'comments_savings_financial'
        ])
        <tr class="bg-dark text-white">
            <th class="text-right">OTHER FINANCIAL AID</th>
            <th class="text-right">Min Savings</th>
            <th class="text-right">Max Savings</th>
        </tr>
        @include('elements.advisor.quickPlan.tabs._range_row', [
            'title' => 'Outside Scholarships & Grants',
            'dropdown' => $data['savingDropdown'],
            'columnOne' => 'savings_outside_scholarship_min',
            'columnTwo' => 'savings_outside_scholarship_max',
        ])
        @include('elements.advisor.quickPlan.tabs._range_row', [
            'title' => 'Other Savings',
            'dropdown' => $data['savingDropdown'],
            'columnOne' => 'savings_other_financial_other_min',
            'columnTwo' => 'savings_other_financial_other_max',
        ])
        @include('elements.advisor.quickPlan.tabs._comment', [
            'title' => 'Comments/Notes',
            'column' => 'comments_savings_other'
        ])
    </table>
</div>