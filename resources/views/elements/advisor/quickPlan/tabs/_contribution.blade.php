<div class="table-responsive no-border">
    <table class="table table-condensed table-left-right-border valign-center font-s18">
        <tr class="bg-dark text-white">
            <th class="text-right">PARENT'S CONTRIBUTION</th>
            <th class="text-right">Available</th>
            <th class="text-right">Contribution</th>
        </tr>
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => '""Annual" Income Contribution',
            'readValue' => $data['scenario']->parent_available_income_work_dollar,
            'column' => 'contributions_parent_annual_income_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Total Education Assets (529, cesa)',
            'readValue' => $data['scenario']->parent_available_asset_education_dollar,
            'column' => 'contributions_parent_assets_education_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => '"Total Non-Retirement Assets',
            'readValue' => $data['scenario']->parent_available_asset_non_retirement_dollar,
            'column' => 'contributions_parent_assets_non_retirement_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => '"Total Retirement Assets',
            'readValue' => $data['scenario']->parent_available_asset_retirement_dollar,
            'column' => 'contributions_parent_assets_retirement_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => '"Total Life Insurance',
            'readValue' => $data['scenario']->parent_insurance_permanent_cash_value_dollar_total,
            'column' => 'contributions_parent_life_insurance_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => '"Total Business / Farm Equity',
            'readValue' => $data['scenario']->parent_farm_net_worth_dollar + $data['scenario']->parent_business_net_worth_dollar,
            'column' => 'contributions_parent_business_farm_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => '"Total Real Estate Equity',
            'readValue' => $data['scenario']->parent_asset_real_estate_primary_net_worth_dollar + $data['scenario']->parent_asset_real_estate_other_net_worth_dollar,
            'column' => 'contributions_parent_real_estate_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => '"Total Plus & Private loans',
            'readValue' => -1,
            'column' => 'contributions_parent_plus_private_loans_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => '"Other Contributions (gifts,etc.) ',
            'readValue' => -1,
            'column' => 'contributions_parent_other_contribution_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._comment', [
            'title' => 'Comments/Notes',
            'column' => 'comments_planned_parent_contribution'
        ])
        <tr class="bg-dark text-white">
            <th class="text-right">STUDENT'S CONTRIBUTION</th>
            <th class="text-right">Available</th>
            <th class="text-right">Contribution</th>
        </tr>
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => '"Annual" Income Contribution',
            'readValue' => $data['scenario']->student_income_work_dollar,
            'column' => 'contributions_student_annual_income_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Total Assets Contribution',
            'readValue' => $data['scenario']->student_available_asset_dollar,
            'column' => 'contributions_student_total_asset_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Total Student Loans',
            'readValue' => -1,
            'column' => 'contributions_student_loans_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Other Contributions (gifts,etc.)',
            'readValue' => -1,
            'column' => 'contributions_student_other_contribution_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._comment', [
            'title' => 'Comments/Notes',
            'column' => 'comments_planned_student_contribution'
        ])
    </table>
</div>