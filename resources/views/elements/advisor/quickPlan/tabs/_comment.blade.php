<tr>
    <td class="text-right">
        {{ $title }}
    </td>
    <td class="" colspan="2">
        {!! Form::textarea("{$column}", null, [
            'class'=>'form-control form-control-lg border-grey bg-gray-light',
            'id'=> "{$column}",
            'style' => 'width: 100%;',
            'rows' => '3',
            $data['inputDisabled']
        ]) !!}
    </td>
</tr>