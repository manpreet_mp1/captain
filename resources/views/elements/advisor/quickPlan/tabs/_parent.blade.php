<div class="table-responsive no-border">
    <table class="table table-condensed table-left-right-border valign-center font-s18">
        <tr class="bg-dark text-white">
            <th class="text-right">INCOME</th>
            <th class="text-right">Original Value</th>
            <th class="text-right">New Value</th>
        </tr>
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Parents’ Adjusted Gross Income',
            'readValue' => $data['family']->parent_income_adjusted_gross_dollar_total,
            'column' => 'parent_income_adjusted_gross_dollar_total'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Annual earnings from work - Parent 1',
            'readValue' => $data['family']->parentOne()->income_work_dollar,
            'column' => 'parent_1_income_work_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Annual earnings from work - Parent 2',
            'readValue' => optional($data['family']->parentTwo())->income_work_dollar,
            'column' => 'parent_2_income_work_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Total untaxed income and benefits',
            'readValue' => $data['family']->parent_income_untaxed_dollar_total,
            'column' => 'parent_income_untaxed_dollar_total'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Total additional financial information',
            'readValue' => $data['family']->parent_income_additional_dollar_total,
            'column' => 'parent_income_additional_dollar_total'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'U.S Income tax paid',
            'readValue' => $data['family']->parent_income_tax_dollar_total,
            'column' => 'parent_income_tax_dollar_total'
        ])
        @include('elements.advisor.quickPlan.tabs._comment', [
            'title' => 'Comments/Notes',
            'column' => 'comments_parent_income'
        ])
        <tr class="bg-dark text-white">
            <th class="text-right">ASSETS - EDUCATION</th>
            <th class="text-right">Original Value</th>
            <th class="text-right">New Value</th>
        </tr>
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Student\'s siblings assets',
            'readValue' => $data['family']->parent_asset_education_student_sibling_dollar_total,
            'column' => 'parent_asset_education_student_sibling_dollar_total'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => '529 / Tuition Savings Plans',
            'readValue' => $data['family']->parent_asset_education_five_two_nine_dollar_total,
            'column' => 'parent_asset_education_five_two_nine_dollar_total'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Coverdell Education IRA',
            'readValue' => $data['family']->parent_asset_education_coverdell_dollar_total,
            'column' => 'parent_asset_education_coverdell_dollar_total'
        ])
        @include('elements.advisor.quickPlan.tabs._comment', [
            'title' => 'Comments/Notes',
            'column' => 'comments_parent_assets_education'
        ])
        <tr class="bg-dark text-white">
            <th class="text-right">ASSETS - NON-RETIREMENT</th>
            <th class="text-right">Original Value</th>
            <th class="text-right">New Value</th>
        </tr>
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Checking/Savings Account',
            'readValue' => $data['family']->parent_asset_non_retirement_checking_savings_dollar_total,
            'column' => 'parent_asset_non_retirement_checking_savings_dollar_total'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Certificates of Deposit',
            'readValue' => $data['family']->parent_asset_non_retirement_certificate_of_deposit_dollar_total,
            'column' => 'parent_asset_non_retirement_certificate_of_deposit_dollar_total'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'T-Bills',
            'readValue' => $data['family']->parent_asset_non_retirement_t_bills_dollar_total,
            'column' => 'parent_asset_non_retirement_t_bills_dollar_total'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Money Market Funds',
            'readValue' => $data['family']->parent_asset_non_retirement_money_market_dollar_total,
            'column' => 'parent_asset_non_retirement_money_market_dollar_total'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Mutual Funds',
            'readValue' => $data['family']->parent_asset_non_retirement_mutual_dollar_total,
            'column' => 'parent_asset_non_retirement_mutual_dollar_total'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Stocks/Stock Options',
            'readValue' => $data['family']->parent_asset_non_retirement_stock_dollar_total,
            'column' => 'parent_asset_non_retirement_stock_dollar_total'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Bonds',
            'readValue' => $data['family']->parent_asset_non_retirement_bond_dollar_total,
            'column' => 'parent_asset_non_retirement_bond_dollar_total'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Trust Funds',
            'readValue' => $data['family']->parent_asset_non_retirement_trust_dollar_total,
            'column' => 'parent_asset_non_retirement_trust_dollar_total'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Other Securities',
            'readValue' => $data['family']->parent_asset_non_retirement_other_securities_dollar_total,
            'column' => 'parent_asset_non_retirement_other_securities_dollar_total'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Non-Retirement Annuities',
            'readValue' => $data['family']->parent_asset_non_retirement_annuities_dollar_total,
            'column' => 'parent_asset_non_retirement_annuities_dollar_total'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Other Investments',
            'readValue' => $data['family']->parent_asset_non_retirement_other_investments_dollar_total,
            'column' => 'parent_asset_non_retirement_other_investments_dollar_total'
        ])
        @include('elements.advisor.quickPlan.tabs._comment', [
            'title' => 'Comments/Notes',
            'column' => 'comments_parent_assets_non_retirement'
        ])
        <tr class="bg-dark text-white">
            <th class="text-right">ASSETS - RETIREMENT</th>
            <th class="text-right">Original Value</th>
            <th class="text-right">New Value</th>
        </tr>
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Value of the DB account',
            'readValue' => $data['family']->parent_asset_retirement_defined_benefit_dollar_total,
            'column' => 'parent_asset_retirement_defined_benefit_dollar_total'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => '401k/403bs',
            'readValue' => $data['family']->parent_asset_retirement_four_o_one_dollar_total,
            'column' => 'parent_asset_retirement_four_o_one_dollar_total'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'IRAs',
            'readValue' => $data['family']->parent_asset_retirement_ira_dollar_total,
            'column' => 'parent_asset_retirement_ira_dollar_total'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Roth IRAs',
            'readValue' => $data['family']->parent_asset_retirement_roth_ira_dollar_total,
            'column' => 'parent_asset_retirement_roth_ira_dollar_total'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Other Retirement Plans',
            'readValue' => $data['family']->parent_asset_retirement_other_dollar_total,
            'column' => 'parent_asset_retirement_other_dollar_total'
        ])
        @include('elements.advisor.quickPlan.tabs._comment', [
            'title' => 'Comments/Notes',
            'column' => 'comments_parent_assets_retirement'
        ])
        <tr class="bg-dark text-white">
            <th class="text-right">INSURANCE</th>
            <th class="text-right">Original Value</th>
            <th class="text-right">New Value</th>
        </tr>
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Life Insurance - Cash value',
            'readValue' => $data['family']->parentOne()->insurance_permanent_cash_value_dollar + optional($data['family']->parentTwo())->insurance_permanent_cash_value_dollar,
            'column' => 'parent_insurance_permanent_cash_value_dollar_total'
        ])
        @include('elements.advisor.quickPlan.tabs._comment', [
            'title' => 'Comments/Notes',
            'column' => 'comments_parent_assets_insurance'
        ])
        <tr class="bg-dark text-white">
            <th class="text-right">BUSINESS/FARM</th>
            <th class="text-right">Original Value</th>
            <th class="text-right">New Value</th>
        </tr>
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Net Worth of Business',
            'readValue' => $data['family']->parent_business_value_dollar,
            'column' => 'parent_business_net_worth_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Net Worth of Farm',
            'readValue' => $data['family']->parent_farm_value_dollar,
            'column' => 'parent_farm_net_worth_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._comment', [
            'title' => 'Comments/Notes',
            'column' => 'comments_parent_assets_business_farm'
        ])
        <tr class="bg-dark text-white">
            <th class="text-right">REAL ESTATE</th>
            <th class="text-right">Original Value</th>
            <th class="text-right">New Value</th>
        </tr>
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Net Worth of Primary Residence',
            'readValue' => $data['family']->parent_asset_real_estate_primary_net_worth_dollar,
            'column' => 'parent_asset_real_estate_primary_net_worth_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Net Worth of Additional Real Estate (excluding Primary Residence)',
            'readValue' => $data['family']->parent_asset_real_estate_other_net_worth_dollar,
            'column' => 'parent_asset_real_estate_other_net_worth_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._comment', [
            'title' => 'Comments/Notes',
            'column' => 'comments_parent_assets_real_estate'
        ])
    </table>
</div>