<tr>
    <td class="text-right">
        {{ $title }}
    </td>
    <td class="text-right">
        @if($readValue === -1)
            -
        @else
            $ {{ number_format($readValue, 0) }}
        @endif
    </td>
    <td>
        {!! Form::text("{$column}", null, [
        'class'=> "dollar form-control form-control-lg text-left border-grey bg-gray-light text-right",
        'id' => "{$column}",
        "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'",
        $data['inputDisabled']
        ]) !!}
    </td>
</tr>