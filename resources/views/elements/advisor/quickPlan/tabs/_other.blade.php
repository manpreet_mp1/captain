<div class="row kt-margin-b-10">
    <div class="col-lg-6">
        <h4 class="text-uppercase">
            Scenario Name
            <small>*</small>
        </h4>
        {!! Form::text('name', null, ['class' => 'form-control form-control-lg border-grey bg-gray-light','placeholder' => 'Scenario Name', 'required', $data['inputDisabled']])!!}
    </div>
</div>
<div class="row kt-margin-t-20">
    <div class="col-lg-12 ">
        <h4 class="text-uppercase kt-margin-b-10">
            SUMMARY <small>*</small></h4>
        {!! Form::text('summary', null, [
            'class' => 'form-control form-control-lg border-grey bg-gray-light',
            'placeholder' => 'Summary',
            'required',
            $data['inputDisabled']
            ])
        !!}
    </div>
</div>