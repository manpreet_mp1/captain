<div class="table-responsive no-border">
    <table class="table table-condensed table-left-right-border valign-center font-s18">
        <tr class="bg-dark text-white">
            <th class="text-right">INCOME</th>
            <th class="text-right">Original Value</th>
            <th class="text-right">New Value</th>
        </tr>
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Adjusted Gross Income',
            'readValue' => $data['scenario']->child->income_adjusted_gross_dollar,
            'column' => 'student_income_adjusted_gross_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Annual earnings from work',
            'readValue' => $data['scenario']->child->income_work_dollar,
            'column' => 'student_income_work_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Total untaxed income and benefits',
            'readValue' => $data['scenario']->child->income_untaxed_dollar_total,
            'column' => 'student_income_untaxed_dollar_total'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Total additional financial information',
            'readValue' => $data['scenario']->child->income_additional_dollar_total,
            'column' => 'student_income_additional_dollar_total'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'U.S Income tax paid',
            'readValue' => $data['scenario']->child->income_tax_dollar,
            'column' => 'student_income_tax_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._comment', [
            'title' => 'Comments/Notes',
            'column' => 'comments_student_income'
        ])
        <tr class="bg-dark text-white">
            <th class="text-right">ASSETS - NON-RETIREMENT</th>
            <th class="text-right">Original Value</th>
            <th class="text-right">New Value</th>
        </tr>
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Checking/Savings Account',
            'readValue' => $data['scenario']->child->asset_non_retirement_checking_savings_dollar,
            'column' => 'student_asset_non_retirement_checking_savings_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'UGMA / UTMA trust accounts',
            'readValue' => $data['scenario']->child->asset_non_retirement_ugma_utma_dollar,
            'column' => 'student_asset_non_retirement_ugma_utma_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Certificates of Deposit',
            'readValue' => $data['scenario']->child->asset_non_retirement_certificate_of_deposit_dollar,
            'column' => 'student_asset_non_retirement_certificate_of_deposit_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'T-Bills',
            'readValue' => $data['scenario']->child->asset_non_retirement_t_bills_dollar,
            'column' => 'student_asset_non_retirement_t_bills_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Money Market Funds',
            'readValue' => $data['scenario']->child->asset_non_retirement_money_market_dollar,
            'column' => 'student_asset_non_retirement_money_market_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Mutual Funds',
            'readValue' => $data['scenario']->child->asset_non_retirement_mutual_dollar,
            'column' => 'student_asset_non_retirement_mutual_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Stocks/Stock Options',
            'readValue' => $data['scenario']->child->asset_non_retirement_stock_dollar,
            'column' => 'student_asset_non_retirement_stock_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Bonds',
            'readValue' => $data['scenario']->child->asset_non_retirement_bond_dollar,
            'column' => 'student_asset_non_retirement_bond_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'Trust Funds',
            'readValue' => $data['scenario']->child->asset_non_retirement_trust_dollar,
            'column' => 'student_asset_non_retirement_trust_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._row', [
            'title' => 'All other investments in student\'s name',
            'readValue' => $data['scenario']->child->asset_non_retirement_other_investments_dollar,
            'column' => 'student_asset_non_retirement_other_investments_dollar'
        ])
        @include('elements.advisor.quickPlan.tabs._comment', [
            'title' => 'Comments/Notes',
            'column' => 'comments_parent_assets_non_retirement'
        ])
    </table>
</div>