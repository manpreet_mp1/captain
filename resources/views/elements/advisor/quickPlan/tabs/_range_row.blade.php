<tr>
    <td class="text-right">
        {{ $title }}
    </td>
    <td class="text-right">
        {!! Form::select("{$columnOne}", $dropdown, null, ['class'=>'form-control form-control-lg border-grey bg-gray-light', $data['inputDisabled']]) !!}
    </td>
    <td>
        {!! Form::select("{$columnTwo}", $dropdown, null, ['class'=>'form-control form-control-lg border-grey bg-gray-light', $data['inputDisabled']]) !!}
    </td>
</tr>