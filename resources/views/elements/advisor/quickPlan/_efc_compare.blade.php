<div class="kt-portlet kt-portlet--height-fluid">
    <div class="kt-portlet__head kt-portlet__space-x bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                EFC Summary
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="table-responsive no-border">
            <table class="table table-condensed table-left-right-border valign-center font-s15 largemarginbottom shadow-bottom bg-white">
                <tbody>
                    <tr class="bg-dark text-white">
                        <th class="col-xs-4 text-center font-s20 font-w400 custom_table_labels ">Estimated
                            <br>
                            EFC / IM
                        </th>
                        <th class="col-xs-2 text-center font-s20 font-w400 custom_table_labels">Original
                            <br>
                            Value
                        </th>
                        <th class="col-xs-2 text-center font-s20 font-w400 custom_table_labels">New
                            <br>
                            Value
                        </th>
                        <th class="col-xs-2 text-center font-s20 font-w400 custom_table_labels" style="line-height: 120% !important"> Increase in Aid
                            <br>
                            (First Year)
                        </th>
                        <th class="col-xs-2 text-center font-s20 font-w400 custom_table_labels" style="line-height: 120% !important"> Increase in Aid
                            <br>
                            (Over 4-Years)
                        </th>
                    </tr>
                    <tr>
                        <th class="active text-center font-s20 font-w300 custom_table_labels">
                            <i class="blueprintone-government1 font-sba80 text-primary"></i>
                            <br>
                            FEDERAL METHODOLOGY (FM)
                        </th>
                        <td class="text-center font-s22">$ {{ number_format($data['result']['efc']['original']['efc_fm_final'] ,0) }}</td>
                        <td class="text-center font-s22">$ {{ number_format($data['result']['efc']['new']['efc_fm_final'] ,0) }}</td>
                        <td class="text-center font-s22">$ {{ number_format($data['result']['efc']['compare']['aid_first_year_fm'] ,0) }}</td>
                        <td class="text-center font-s22">$ {{ number_format($data['result']['efc']['compare']['aid_four_year_fm'] ,0) }}</td>
                    </tr>
                    <tr>
                        <th class="active text-center font-s20 font-w300 custom_table_labels">
                            <i class="blueprintone-school37 font-sba80 text-primary"></i>
                            <br>
                            INSTITUTIONAL METHODOLOGY (IM)
                        </th>
                        <td class="text-center font-s22">$ {{ number_format($data['result']['efc']['original']['efc_im_final'] ,0) }}</td>
                        <td class="text-center font-s22">$ {{ number_format($data['result']['efc']['new']['efc_im_final'] ,0) }}</td>
                        <td class="text-center font-s22">$ {{ number_format($data['result']['efc']['compare']['aid_first_year_im'] ,0) }}</td>
                        <td class="text-center font-s22">$ {{ number_format($data['result']['efc']['compare']['aid_four_year_im'] ,0) }}</td>
                    </tr>
                    <tr class="bg-dark text-white">
                        <td colspan="5"></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
