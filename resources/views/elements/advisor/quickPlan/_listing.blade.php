<div class="kt-portlet">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                LOAD AN EXISTING SCENARIO </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="kt-section kt-margin-b-0">
            <div class="kt-section__content">
                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable datatable" id="kt_table_1">
                    <thead>
                        <tr class="bg-dark text-uppercase">
                            <th class="text-white" title="Field #1"></th>
                            <th class="text-white" title="Field #2">SCENARIO</th>
                            <th class="text-white" title="Field #2">SUMMARY</th>
                            <th class="text-white" title="Field #3">STUDENT NAME</th>
                            <th class="text-white" title="Field #6">SHARED WITH FAMILY</th>
                            <th class="text-white" title="Field #4">CREATED</th>
                            <th class="text-white" title="Field #7">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1; @endphp
                        @foreach($data['items'] as $item)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->summary }}</td>
                                <td>{{ $item->child->user->first_name }}</td>
                                <td>{{ $item->is_shared_text }}</td>
                                <td>{{ $item->created_at->toDateString() }}</td>
                                <td>
                                    <a href="{{ route($routeName, array_merge(['slug' => $item->slug], $additionalParam)) }}" class="btn btn-primary btn-sm">
                                        Load
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>