<tr class=>
    <td class="font-s15 font-w300 text-uppercase"> FINANCIAL AID SAVINGS</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? $data['result']['collegesDetails'][1]['financialPlanningStrategies']['financialAidSavingsTextRange'] : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? $data['result']['collegesDetails'][2]['financialPlanningStrategies']['financialAidSavingsTextRange'] : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? $data['result']['collegesDetails'][3]['financialPlanningStrategies']['financialAidSavingsTextRange'] : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? $data['result']['collegesDetails'][4]['financialPlanningStrategies']['financialAidSavingsTextRange'] : "" }}
    </td>
</tr>
<tr class=>
    <td class="font-s15 font-w300 text-uppercase"> TAX REDUCTION &amp; PLANNING</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? $data['result']['collegesDetails'][1]['financialPlanningStrategies']['taxReductionPlanningTextRange'] : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? $data['result']['collegesDetails'][2]['financialPlanningStrategies']['taxReductionPlanningTextRange'] : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? $data['result']['collegesDetails'][3]['financialPlanningStrategies']['taxReductionPlanningTextRange'] : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? $data['result']['collegesDetails'][4]['financialPlanningStrategies']['taxReductionPlanningTextRange'] : "" }}
    </td>
</tr>
<tr class=>
    <td class="font-s15 font-w300 text-uppercase"> ASSET MANAGEMENT</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? $data['result']['collegesDetails'][1]['financialPlanningStrategies']['assetManagementTextRange'] : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? $data['result']['collegesDetails'][2]['financialPlanningStrategies']['assetManagementTextRange'] : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? $data['result']['collegesDetails'][3]['financialPlanningStrategies']['assetManagementTextRange'] : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? $data['result']['collegesDetails'][4]['financialPlanningStrategies']['assetManagementTextRange'] : "" }}
    </td>
</tr>
<tr class=>
    <td class="font-s15 font-w300 text-uppercase"> BORROWING &amp; LOAN STRATEGIES</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? $data['result']['collegesDetails'][1]['financialPlanningStrategies']['borrowingLoanStrategiesTextRange'] : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? $data['result']['collegesDetails'][2]['financialPlanningStrategies']['borrowingLoanStrategiesTextRange'] : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? $data['result']['collegesDetails'][3]['financialPlanningStrategies']['borrowingLoanStrategiesTextRange'] : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? $data['result']['collegesDetails'][4]['financialPlanningStrategies']['borrowingLoanStrategiesTextRange'] : "" }}
    </td>
</tr>
<tr class=>
    <td class="font-s15 font-w300 text-uppercase"> CASHFLOW IMPROVEMENTS</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? $data['result']['collegesDetails'][1]['financialPlanningStrategies']['cashflowImprovementsTextRange'] : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? $data['result']['collegesDetails'][2]['financialPlanningStrategies']['cashflowImprovementsTextRange'] : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? $data['result']['collegesDetails'][3]['financialPlanningStrategies']['cashflowImprovementsTextRange'] : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? $data['result']['collegesDetails'][4]['financialPlanningStrategies']['cashflowImprovementsTextRange'] : "" }}
    </td>
</tr>
<tr class=>
    <td class="font-s15 font-w300 text-uppercase"> OTHER</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? $data['result']['collegesDetails'][1]['financialPlanningStrategies']['otherTextRange'] : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? $data['result']['collegesDetails'][2]['financialPlanningStrategies']['otherTextRange'] : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? $data['result']['collegesDetails'][3]['financialPlanningStrategies']['otherTextRange'] : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? $data['result']['collegesDetails'][4]['financialPlanningStrategies']['otherTextRange'] : "" }}
    </td>
</tr>
<tr class="bg-gray-light">
    <td class="font-s15 font-w400 text-uppercase"> POTENTIAL SAVINGS FOR {{ $data['scenario']->child->user->first_name }}'S EDUCATION</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? $data['result']['collegesDetails'][1]['financialPlanningStrategies']['totalTextRange'] : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? $data['result']['collegesDetails'][2]['financialPlanningStrategies']['totalTextRange'] : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? $data['result']['collegesDetails'][3]['financialPlanningStrategies']['totalTextRange'] : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? $data['result']['collegesDetails'][4]['financialPlanningStrategies']['totalTextRange'] : "" }}
    </td>
</tr><!-- Table Row Ends -->
<tr class="smallpaddingall">
    <td colspan="5"></td>
</tr><!-- Table Row Start -->
<tr class="">
    <td class="font-s15 font-w300 text-uppercase">
        YEARS TO RETIREMENT (AGE {{ $data['scenario']->planning_retirement_age }} )
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? $data['result']['collegesDetails'][1]['financialPlanningStrategies']['yearsToRetirement']. " Years" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? $data['result']['collegesDetails'][2]['financialPlanningStrategies']['yearsToRetirement']. " Years" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? $data['result']['collegesDetails'][3]['financialPlanningStrategies']['yearsToRetirement']. " Years" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? $data['result']['collegesDetails'][4]['financialPlanningStrategies']['yearsToRetirement']. " Years" : "" }}
    </td>
</tr>
<tr class="">
    <td class="font-s15 font-w300 text-uppercase"> EXPECTED INVESTMENT RATE</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? number_format($data['result']['collegesDetails'][1]['financialPlanningStrategies']['rateOfReturn'], 2) . " %" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? number_format($data['result']['collegesDetails'][2]['financialPlanningStrategies']['rateOfReturn'], 2) . " %" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? number_format($data['result']['collegesDetails'][3]['financialPlanningStrategies']['rateOfReturn'], 2) . " %" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? number_format($data['result']['collegesDetails'][4]['financialPlanningStrategies']['rateOfReturn'], 2) . " %" : "" }}
    </td>
</tr>
<tr class="bg-primary text-white">
    <td class="font-s15 font-w400 text-uppercase medpaddingall">
        <i class="blueprinttwo-beach13"></i>
        POTENTIAL POSITIVE IMPACT ON RETIREMENT
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? $data['result']['collegesDetails'][1]['financialPlanningStrategies']['impactTextRange'] : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? $data['result']['collegesDetails'][2]['financialPlanningStrategies']['impactTextRange'] : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? $data['result']['collegesDetails'][3]['financialPlanningStrategies']['impactTextRange'] : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? $data['result']['collegesDetails'][4]['financialPlanningStrategies']['impactTextRange'] : "" }}
    </td>
</tr>