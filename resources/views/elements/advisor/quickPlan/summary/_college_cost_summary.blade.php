<tr class="">
    <td class="font-s15 font-w300"> ANNUAL COST OF ATTENDANCE (2019 - 2020)</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['cost']['annualCost'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['cost']['annualCost'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['cost']['annualCost'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['cost']['annualCost'], 0) : "" }}
    </td>
</tr>
<tr class="">
    <td class="font-s15 font-w300"> 4-YEAR COST OF ATTENDANCE</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['cost']['fourYearCostWithoutAid'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['cost']['fourYearCostWithoutAid'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['cost']['fourYearCostWithoutAid'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['cost']['fourYearCostWithoutAid'], 0) : "" }}
    </td>
</tr>
<tr class="">
    <td class="font-s15 font-w300"> COLLEGE SCHOLARSHIPS &amp; GRANTS</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['cost']['amountOfGiftAidFourYear'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['cost']['amountOfGiftAidFourYear'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['cost']['amountOfGiftAidFourYear'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['cost']['amountOfGiftAidFourYear'], 0) : "" }}
    </td>
</tr>
<tr class="bg-gray-light font-s15">
    <td class="font-s15 font-w400"> 4-YEAR NET COST OF ATTENDANCE</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['cost']['fourYearCostWithAid'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['cost']['fourYearCostWithAid'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['cost']['fourYearCostWithAid'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['cost']['fourYearCostWithAid'], 0) : "" }}
    </td>
</tr>
<tr class="">
    <td class="font-s15 font-w300"> EXPECTED YEARS TO GRADUATE</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? number_format($data['result']['collegesDetails'][1]['cost']['avgGraduationYear'], 1). " Years" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? number_format($data['result']['collegesDetails'][2]['cost']['avgGraduationYear'], 1). " Years" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? number_format($data['result']['collegesDetails'][3]['cost']['avgGraduationYear'], 1). " Years" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? number_format($data['result']['collegesDetails'][4]['cost']['avgGraduationYear'], 1). " Years" : "" }}
    </td>
</tr>
<tr class="bg-gray-light font-s15">
    <td class="font-s15 font-w400 text-uppercase"> {{ $data['scenario']->child->user->first_name }}'S TOTAL COST OF EDUCATION</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['cost']['graduationYearCostWithAid'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['cost']['graduationYearCostWithAid'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['cost']['graduationYearCostWithAid'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['cost']['graduationYearCostWithAid'], 0) : "" }}
    </td>
</tr><!-- Table Row Ends -->
<tr class="bg-dark smallpaddingall">
    <td colspan="5"></td>
</tr><!-- Table Row Start -->
<tr class="">
    <td class="font-s15 font-w300"> AVERAGE SALARY - 6 YEARS AFTER ENROLLMENT</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['cost']['avgSalary6YearsAfterEnrollment'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['cost']['avgSalary6YearsAfterEnrollment'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['cost']['avgSalary6YearsAfterEnrollment'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['cost']['avgSalary6YearsAfterEnrollment'], 0) : "" }}
    </td>
</tr>
<tr class="">
    <td class="font-s15 font-w300"> EDUCATION INVESTMENT RECOVERY RATE (EIRR)</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? number_format($data['result']['collegesDetails'][1]['cost']['educationInvestmentRecoveryRate6years'], 2). " %" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? number_format($data['result']['collegesDetails'][2]['cost']['educationInvestmentRecoveryRate6years'], 2). " %" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? number_format($data['result']['collegesDetails'][3]['cost']['educationInvestmentRecoveryRate6years'], 2). " %" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? number_format($data['result']['collegesDetails'][4]['cost']['educationInvestmentRecoveryRate6years'], 2). " %" : "" }}
    </td>
</tr><!-- Table Row Ends -->
<tr class="bg-dark smallpaddingall">
    <td colspan="5"></td>
</tr><!-- Table Row Start -->
<tr class="">
    <td class="font-s15 font-w300"> AVERAGE SALARY - 10 YEARS AFTER ENROLLMENT</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['cost']['avgSalary10YearsAfterEnrollment'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['cost']['avgSalary10YearsAfterEnrollment'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['cost']['avgSalary10YearsAfterEnrollment'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['cost']['avgSalary10YearsAfterEnrollment'], 0) : "" }}
    </td>
</tr>
<tr class="">
    <td class="font-s15 font-w300"> EDUCATION INVESTMENT RECOVERY RATE (EIRR)</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? number_format($data['result']['collegesDetails'][1]['cost']['educationInvestmentRecoveryRate10years'], 2). " %" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? number_format($data['result']['collegesDetails'][2]['cost']['educationInvestmentRecoveryRate10years'], 2). " %" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? number_format($data['result']['collegesDetails'][3]['cost']['educationInvestmentRecoveryRate10years'], 2). " %" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? number_format($data['result']['collegesDetails'][4]['cost']['educationInvestmentRecoveryRate10years'], 2). " %" : "" }}
    </td>
</tr>
<tr class="bg-dark smallpaddingall">
    <td colspan="5"></td>
</tr>
<tr class="smallpaddingall">
    <td colspan="5"></td>
</tr>