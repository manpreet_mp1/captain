<tr class="bg-gray-light font-s15">
    <td class="font-s15 font-w400 text-uppercase"> {{ $data['scenario']->child->user->first_name }}'S TOTAL COST OF EDUCATION</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['cost']['graduationYearCostWithAid'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['cost']['graduationYearCostWithAid'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['cost']['graduationYearCostWithAid'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['cost']['graduationYearCostWithAid'], 0) : "" }}
    </td>
</tr><!-- Table Row Ends -->
<tr class="">
    <td class="font-s15 font-w300"> YEARS TO RETIREMENT (AGE {{ $data['scenario']->planning_retirement_age }} )</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? number_format($data['result']['collegesDetails'][1]['collegeCostsImpactOnRetirement']['yearsToRetirement'], 0). " Years" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? number_format($data['result']['collegesDetails'][2]['collegeCostsImpactOnRetirement']['yearsToRetirement'], 0). " Years" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? number_format($data['result']['collegesDetails'][3]['collegeCostsImpactOnRetirement']['yearsToRetirement'], 0). " Years" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? number_format($data['result']['collegesDetails'][4]['collegeCostsImpactOnRetirement']['yearsToRetirement'], 0). " Years" : "" }}
    </td>
</tr>
<tr class="">
    <td class="font-s15 font-w300"> ESTIMATED MARKET ROI</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? number_format($data['result']['collegesDetails'][1]['collegeCostsImpactOnRetirement']['rateOfReturn'], 2). " %" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? number_format($data['result']['collegesDetails'][2]['collegeCostsImpactOnRetirement']['rateOfReturn'], 2). " %" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? number_format($data['result']['collegesDetails'][3]['collegeCostsImpactOnRetirement']['rateOfReturn'], 2). " %" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? number_format($data['result']['collegesDetails'][4]['collegeCostsImpactOnRetirement']['rateOfReturn'], 2). " %" : "" }}
    </td>
<tr class="bg-gray-light font-s15">
    <td class="font-s15 font-w400 text-uppercase"> COLLEGE COST IMPACT ON RETIREMENT</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['collegeCostsImpactOnRetirement']['collegeCostImpactOnRetirement'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['collegeCostsImpactOnRetirement']['collegeCostImpactOnRetirement'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['collegeCostsImpactOnRetirement']['collegeCostImpactOnRetirement'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['collegeCostsImpactOnRetirement']['collegeCostImpactOnRetirement'], 0) : "" }}
    </td>
</tr><!-- Table Row Ends -->
<tr class="bg-dark smallpaddingall">
    <td colspan="5"></td>
</tr>
<tr class="smallpaddingall">
    <td colspan="5"></td>
</tr>