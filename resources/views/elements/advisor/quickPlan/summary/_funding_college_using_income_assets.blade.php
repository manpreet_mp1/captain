<!-- Table Row Start -->
<tr class="bg-gray-light font-s15">
    <td class="font-s15 font-w400 text-uppercase"> {{ $data['scenario']->child->user->first_name }}'S TOTAL COST OF EDUCATION</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['cost']['graduationYearCostWithAid'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['cost']['graduationYearCostWithAid'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['cost']['graduationYearCostWithAid'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['cost']['graduationYearCostWithAid'], 0) : "" }}
    </td>
</tr>
<tr class="">
    <td class="font-s15 font-w300 text-uppercase"> {{ $data['scenario']->child->user->first_name }}'S INCOME CONTRIBUTION</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['fundingCollegeUsingIncomeAssets']['studentContributionFromIncomeAtGraduation'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['fundingCollegeUsingIncomeAssets']['studentContributionFromIncomeAtGraduation'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['fundingCollegeUsingIncomeAssets']['studentContributionFromIncomeAtGraduation'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['fundingCollegeUsingIncomeAssets']['studentContributionFromIncomeAtGraduation'], 0) : "" }}
    </td>
</tr>
<tr class="">
    <td class="font-s15 font-w300 text-uppercase"> {{ $data['scenario']->child->user->first_name }}'S ASSET CONTRIBUTION</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['fundingCollegeUsingIncomeAssets']['studentContributionFromAssetAtGraduation'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['fundingCollegeUsingIncomeAssets']['studentContributionFromAssetAtGraduation'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['fundingCollegeUsingIncomeAssets']['studentContributionFromAssetAtGraduation'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['fundingCollegeUsingIncomeAssets']['studentContributionFromAssetAtGraduation'], 0) : "" }}
    </td>
</tr>
<tr class="">
    <td class="font-s15 font-w300 text-uppercase"> PARENT'S INCOME CONTRIBUTION</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['fundingCollegeUsingIncomeAssets']['parentContributionFromIncomeAtGraduation'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['fundingCollegeUsingIncomeAssets']['parentContributionFromIncomeAtGraduation'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['fundingCollegeUsingIncomeAssets']['parentContributionFromIncomeAtGraduation'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['fundingCollegeUsingIncomeAssets']['parentContributionFromIncomeAtGraduation'], 0) : "" }}
    </td>
</tr>
<tr class="">
    <td class="font-s15 font-w300 text-uppercase"> PARENT'S ASSET CONTRIBUTION</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['fundingCollegeUsingIncomeAssets']['parentContributionFromAssetAtGraduation'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['fundingCollegeUsingIncomeAssets']['parentContributionFromAssetAtGraduation'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['fundingCollegeUsingIncomeAssets']['parentContributionFromAssetAtGraduation'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['fundingCollegeUsingIncomeAssets']['parentContributionFromAssetAtGraduation'], 0) : "" }}
    </td>
</tr>
<tr class="bg-gray-light font-s15">
    <td class="font-s15 font-w400 text-uppercase"> TOTAL PLANNED CONTRIBUTIONS</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['fundingCollegeUsingIncomeAssets']['totalPlannedContributions'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['fundingCollegeUsingIncomeAssets']['totalPlannedContributions'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['fundingCollegeUsingIncomeAssets']['totalPlannedContributions'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['fundingCollegeUsingIncomeAssets']['totalPlannedContributions'], 0) : "" }}
    </td>
</tr>
<tr class="bg-gray-light font-s15">
    <td class="font-s15 font-w400 text-uppercase"> {{ $data['scenario']->child->user->first_name }}'S UNFUNDED COST OF EDUCATION</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['fundingCollegeUsingIncomeAssets']['unfundedCostOfEducation'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['fundingCollegeUsingIncomeAssets']['unfundedCostOfEducation'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['fundingCollegeUsingIncomeAssets']['unfundedCostOfEducation'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['fundingCollegeUsingIncomeAssets']['unfundedCostOfEducation'], 0) : "" }}
    </td>
</tr>
<tr class="bg-dark smallpaddingall">
    <td colspan="5"></td>
</tr>
<tr class="smallpaddingall">
    <td colspan="5"></td>
</tr>