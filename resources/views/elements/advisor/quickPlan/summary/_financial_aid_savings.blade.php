<tr class="">
    <td class="font-s15 font-w300 text-uppercase text-uppercase"> EFC or IM - BEFORE</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['financialAidSavings']['efcBefore'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['financialAidSavings']['efcBefore'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['financialAidSavings']['efcBefore'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['financialAidSavings']['efcBefore'], 0) : "" }}
    </td>
</tr>
<tr class="">
    <td class="font-s15 font-w300 text-uppercase text-uppercase"> EFC or IM - AFTER</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['financialAidSavings']['efcAfter'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['financialAidSavings']['efcAfter'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['financialAidSavings']['efcAfter'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['financialAidSavings']['efcAfter'], 0) : "" }}
    </td>
</tr><!-- Table Row Ends -->
<tr class="smallpaddingall">
    <td colspan="5"></td>
</tr><!-- Table Row Start -->
<tr class="">
    <td class="font-s15 font-w300 text-uppercase"> NEED MET %</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? number_format($data['result']['collegesDetails'][1]['financialAidSavings']['needMet'], 2). " %" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? number_format($data['result']['collegesDetails'][2]['financialAidSavings']['needMet'], 2). " %" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? number_format($data['result']['collegesDetails'][3]['financialAidSavings']['needMet'], 2). " %" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? number_format($data['result']['collegesDetails'][4]['financialAidSavings']['needMet'], 2). " %" : "" }}
    </td>
</tr>
<tr class="">
    <td class="font-s15 font-w300 text-uppercase"> GIFT AID %</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? number_format($data['result']['collegesDetails'][1]['financialAidSavings']['giftAid'], 2). " %" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? number_format($data['result']['collegesDetails'][2]['financialAidSavings']['giftAid'], 2). " %" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? number_format($data['result']['collegesDetails'][3]['financialAidSavings']['giftAid'], 2). " %" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? number_format($data['result']['collegesDetails'][4]['financialAidSavings']['giftAid'], 2). " %" : "" }}
    </td>
</tr>
<tr class="">
    <td class="font-s15 font-w300 text-uppercase"> SELF-HELP AID %</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? number_format($data['result']['collegesDetails'][1]['financialAidSavings']['selfHelpAid'], 2). " %" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? number_format($data['result']['collegesDetails'][2]['financialAidSavings']['selfHelpAid'], 2). " %" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? number_format($data['result']['collegesDetails'][3]['financialAidSavings']['selfHelpAid'], 2). " %" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? number_format($data['result']['collegesDetails'][4]['financialAidSavings']['selfHelpAid'], 2). " %" : "" }}
    </td>
</tr><!-- Table Row Ends -->
<tr class="smallpaddingall">
    <td colspan="5"></td>
</tr><!-- Table Row Start -->
<tr class="">
    <td class="font-s15 font-w300 text-uppercase text-uppercase"> POTENTIAL ANNUAL GIFT AID SAVINGS</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['financialAidSavings']['potentialAnnualGiftAidSavings'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['financialAidSavings']['potentialAnnualGiftAidSavings'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['financialAidSavings']['potentialAnnualGiftAidSavings'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['financialAidSavings']['potentialAnnualGiftAidSavings'], 0) : "" }}
    </td>
</tr>
<tr class="">
    <td class="font-s15 font-w300 text-uppercase"> EXPECTED YEARS TO GRADUATE</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? number_format($data['result']['collegesDetails'][1]['financialAidSavings']['expectedYearsToGraduate'], 2). " Years" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? number_format($data['result']['collegesDetails'][2]['financialAidSavings']['expectedYearsToGraduate'], 2). " Years" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? number_format($data['result']['collegesDetails'][3]['financialAidSavings']['expectedYearsToGraduate'], 2). " Years" : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? number_format($data['result']['collegesDetails'][4]['financialAidSavings']['expectedYearsToGraduate'], 2). " Years" : "" }}
    </td>
</tr>
<tr class="bg-gray-light font-s15">
    <td class="font-s15 font-w400 text-uppercase"> TOTAL FINANCIAL AID SAVINGS</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['financialAidSavings']['totalFinancialAidSavings'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['financialAidSavings']['totalFinancialAidSavings'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['financialAidSavings']['totalFinancialAidSavings'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['financialAidSavings']['totalFinancialAidSavings'], 0) : "" }}
    </td>
</tr>
<tr class="bg-dark smallpaddingall">
    <td colspan="5"></td>
</tr>
<tr class="smallpaddingall">
    <td colspan="5"></td>
</tr>