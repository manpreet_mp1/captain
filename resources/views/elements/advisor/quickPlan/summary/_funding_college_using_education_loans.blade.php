<!-- Table Row Start -->
<tr class="bg-gray-light font-s15">
    <td class="font-s15 font-w400 text-uppercase"> {{ $data['scenario']->child->user->first_name }}'S UNFUNDED COST OF EDUCATION</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['fundingCollegeUsingEducationLoans']['unfundedCostOfEducation'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['fundingCollegeUsingEducationLoans']['unfundedCostOfEducation'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['fundingCollegeUsingEducationLoans']['unfundedCostOfEducation'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['fundingCollegeUsingEducationLoans']['unfundedCostOfEducation'], 0) : "" }}
    </td>
</tr>
<tr class="">
    <td class="font-s15 font-w300 text-uppercase"> TOTAL STUDENT LOANS</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['fundingCollegeUsingEducationLoans']['studentTotalLoans'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['fundingCollegeUsingEducationLoans']['studentTotalLoans'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['fundingCollegeUsingEducationLoans']['studentTotalLoans'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['fundingCollegeUsingEducationLoans']['studentTotalLoans'], 0) : "" }}
    </td>
</tr>
<tr class="">
    <td class="font-s15 font-w300 text-uppercase"> LOAN FEES</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['fundingCollegeUsingEducationLoans']['studentLoanFees'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['fundingCollegeUsingEducationLoans']['studentLoanFees'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['fundingCollegeUsingEducationLoans']['studentLoanFees'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['fundingCollegeUsingEducationLoans']['studentLoanFees'], 0) : "" }}
    </td>
</tr>
<tr class="">
    <td class="font-s15 font-w300 text-uppercase"> TOTAL INTEREST PAID (INCLUDING LOAN FEES)</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['fundingCollegeUsingEducationLoans']['studentInterestPaidIncludingLoanFees'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['fundingCollegeUsingEducationLoans']['studentInterestPaidIncludingLoanFees'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['fundingCollegeUsingEducationLoans']['studentInterestPaidIncludingLoanFees'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['fundingCollegeUsingEducationLoans']['studentInterestPaidIncludingLoanFees'], 0) : "" }}
    </td>
</tr>
<tr class="">
    <td class="font-s15 font-w300 text-uppercase"> MONTHLY PAYMENTS</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['fundingCollegeUsingEducationLoans']['studentMonthlyPayment'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['fundingCollegeUsingEducationLoans']['studentMonthlyPayment'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['fundingCollegeUsingEducationLoans']['studentMonthlyPayment'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['fundingCollegeUsingEducationLoans']['studentMonthlyPayment'], 0) : "" }}
    </td>
</tr>
<tr class="bg-gray-light font-s15">
    <td class="font-s15 font-w400 text-uppercase"> TOTAL STUDENT LOAN COST - 10 YEARS</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['fundingCollegeUsingEducationLoans']['studentTotalLoanCost10Years'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['fundingCollegeUsingEducationLoans']['studentTotalLoanCost10Years'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['fundingCollegeUsingEducationLoans']['studentTotalLoanCost10Years'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['fundingCollegeUsingEducationLoans']['studentTotalLoanCost10Years'], 0) : "" }}
    </td>
</tr><!-- Table Row Ends -->
<tr class="bg-dark smallpaddingall">
    <td colspan="5"></td>
</tr><!-- Table Row Start -->
<tr class="">
    <td class="font-s15 font-w300 text-uppercase"> TOTAL PARENT LOANS (BALANCE)</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['fundingCollegeUsingEducationLoans']['parentTotalLoans'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['fundingCollegeUsingEducationLoans']['parentTotalLoans'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['fundingCollegeUsingEducationLoans']['parentTotalLoans'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['fundingCollegeUsingEducationLoans']['parentTotalLoans'], 0) : "" }}
    </td>
</tr>
<tr class="">
    <td class="font-s15 font-w300 text-uppercase"> LOAN FEES</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['fundingCollegeUsingEducationLoans']['parentLoanFees'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['fundingCollegeUsingEducationLoans']['parentLoanFees'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['fundingCollegeUsingEducationLoans']['parentLoanFees'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['fundingCollegeUsingEducationLoans']['parentLoanFees'], 0) : "" }}
    </td>
</tr>
<tr class="">
    <td class="font-s15 font-w300 text-uppercase"> TOTAL INTEREST PAID (INCLUDING LOAN FEES)</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['fundingCollegeUsingEducationLoans']['parentInterestPaidIncludingLoanFees'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['fundingCollegeUsingEducationLoans']['parentInterestPaidIncludingLoanFees'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['fundingCollegeUsingEducationLoans']['parentInterestPaidIncludingLoanFees'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['fundingCollegeUsingEducationLoans']['parentInterestPaidIncludingLoanFees'], 0) : "" }}
    </td>
</tr>
<tr class="">
    <td class="font-s15 font-w300 text-uppercase"> MONTHLY PAYMENTS</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['fundingCollegeUsingEducationLoans']['parentMonthlyPayment'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['fundingCollegeUsingEducationLoans']['parentMonthlyPayment'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['fundingCollegeUsingEducationLoans']['parentMonthlyPayment'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['fundingCollegeUsingEducationLoans']['parentMonthlyPayment'], 0) : "" }}
    </td>
</tr>
<tr class="bg-gray-light font-s15">
    <td class="font-s15 font-w400 text-uppercase"> TOTAL PARENT LOAN COST - 10 YEARS</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['fundingCollegeUsingEducationLoans']['parentTotalLoanCost10Years'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['fundingCollegeUsingEducationLoans']['parentTotalLoanCost10Years'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['fundingCollegeUsingEducationLoans']['parentTotalLoanCost10Years'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['fundingCollegeUsingEducationLoans']['parentTotalLoanCost10Years'], 0) : "" }}
    </td>
</tr><!-- Table Row Ends -->
<tr class="bg-dark smallpaddingall">
    <td colspan="5"></td>
</tr><!-- Table Row Start -->
<tr class="">
    <td class="font-s15 font-w300 text-uppercase"> TOTAL ALL EDUCATION LOANS</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['fundingCollegeUsingEducationLoans']['allTotalLoans'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['fundingCollegeUsingEducationLoans']['allTotalLoans'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['fundingCollegeUsingEducationLoans']['allTotalLoans'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['fundingCollegeUsingEducationLoans']['allTotalLoans'], 0) : "" }}
    </td>
</tr>
<tr class="">
    <td class="font-s15 font-w300 text-uppercase"> TOTAL LOAN FEES</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['fundingCollegeUsingEducationLoans']['allLoanFees'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['fundingCollegeUsingEducationLoans']['allLoanFees'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['fundingCollegeUsingEducationLoans']['allLoanFees'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['fundingCollegeUsingEducationLoans']['allLoanFees'], 0) : "" }}
    </td>
</tr>
<tr class="">
    <td class="font-s15 font-w300 text-uppercase"> TOTAL INTEREST PAID (INCLUDING LOAN FEES)</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['fundingCollegeUsingEducationLoans']['allInterestPaidIncludingLoanFees'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['fundingCollegeUsingEducationLoans']['allInterestPaidIncludingLoanFees'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['fundingCollegeUsingEducationLoans']['allInterestPaidIncludingLoanFees'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['fundingCollegeUsingEducationLoans']['allInterestPaidIncludingLoanFees'], 0) : "" }}
    </td>
</tr>
<tr class="">
    <td class="font-s15 font-w300 text-uppercase"> TOTAL MONTHLY PAYMENTS</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['fundingCollegeUsingEducationLoans']['allMonthlyPayment'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['fundingCollegeUsingEducationLoans']['allMonthlyPayment'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['fundingCollegeUsingEducationLoans']['allMonthlyPayment'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['fundingCollegeUsingEducationLoans']['allMonthlyPayment'], 0) : "" }}
    </td>
</tr>
<tr class="bg-gray-light font-s15">
    <td class="font-s15 font-w400 text-uppercase"> TOTAL LOAN COST - 10 YEARS</td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][1]) ? "$ ".number_format($data['result']['collegesDetails'][1]['fundingCollegeUsingEducationLoans']['allTotalLoanCost10Years'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][2]) ? "$ ".number_format($data['result']['collegesDetails'][2]['fundingCollegeUsingEducationLoans']['allTotalLoanCost10Years'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][3]) ? "$ ".number_format($data['result']['collegesDetails'][3]['fundingCollegeUsingEducationLoans']['allTotalLoanCost10Years'], 0) : "" }}
    </td>
    <td class="">
        {{ isset($data['result']['collegesDetails'][4]) ? "$ ".number_format($data['result']['collegesDetails'][4]['fundingCollegeUsingEducationLoans']['allTotalLoanCost10Years'], 0) : "" }}
    </td>
</tr>
<tr class="bg-dark smallpaddingall">
    <td colspan="5"></td>
</tr>
<tr class="smallpaddingall">
    <td colspan="5"></td>
</tr>