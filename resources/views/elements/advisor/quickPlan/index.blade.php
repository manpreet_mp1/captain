@section('pageTitle', 'Quick Plan')
@section('subheader')
    <h3 class="kt-subheader__title">Quick Plan</h3>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            @include('elements.advisor.quickPlan._create')
        </div>
        <div class="col-lg-12">
            @include('elements.advisor.quickPlan._listing')
        </div>
    </div>
@endsection