<div class="kt-portlet kt-portlet--height-fluid">
    <div class="kt-portlet__head kt-portlet__space-x bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                {{ config('app.shortName') }} SCENARIO SUMMARY </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="table-responsive no-border largemarginbottom ">
            <table class="table table-condensed table-left-right-border valign-center font-s15 nomarginbottom">

                <tr class="bg-primary text-white font-s22">
                    <th class="font-w400 text-center" colspan="5">
                        <i class="flaticon-tools-and-utensils"></i> COLLEGE COST SUMMARY
                    </th>
                </tr>
                @include('elements.advisor.quickPlan.summary._college_names_row')
                @include('elements.advisor.quickPlan.summary._college_cost_summary')

                <tr class="bg-primary text-white font-s22">
                    <th class="font-w400 text-center" colspan="5">
                        <i class="blueprinttwo-rest5"></i> COLLEGE COSTS IMPACT ON RETIREMENT
                    </th>
                </tr>
                @include('elements.advisor.quickPlan.summary._college_names_row')
                @include('elements.advisor.quickPlan.summary._college_costs_impact_on_retirement')

                <tr class="bg-primary text-white font-s22">
                    <th class="font-w400 text-center" colspan="5">
                        <i class="blueprinttwo-money315"></i> FUNDING COLLEGE - USING INCOME & ASSETS
                    </th>
                </tr>
                @include('elements.advisor.quickPlan.summary._college_names_row')
                @include('elements.advisor.quickPlan.summary._funding_college_using_income_assets')

                <tr class="bg-primary text-white font-s22">
                    <th class="font-w400 text-center" colspan="5">
                        <i class="blueprinttwo-money184"></i> FUNDING COLLEGE - USING EDUCATION LOANS
                    </th>
                </tr>
                @include('elements.advisor.quickPlan.summary._college_names_row')
                @include('elements.advisor.quickPlan.summary._funding_college_using_education_loans')

                <tr class="bg-primary text-white font-s22">
                    <th class="font-w400 text-center" colspan="5">
                        <i class="blueprinttwo-money315"></i> FINANCIAL AID SAVINGS
                    </th>
                </tr>
                @include('elements.advisor.quickPlan.summary._college_names_row')
                @include('elements.advisor.quickPlan.summary._financial_aid_savings')

                <tr class="bg-primary text-white font-s22">
                    <th class="font-w400 text-center" colspan="5">
                        <i class="blueprinttwo-money315"></i> {{ config('app.shortName') }} FINANCIAL PLANNING STRATEGIES
                    </th>
                </tr>
                @include('elements.advisor.quickPlan.summary._college_names_row')
                @include('elements.advisor.quickPlan.summary._financial_planning_strategies')
            </table>
        </div>
    </div>
</div>

