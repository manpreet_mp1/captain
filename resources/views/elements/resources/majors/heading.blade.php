@section('pageTitle', 'Majors')
@section('subheader')
    <h3 class="kt-subheader__title">Majors</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <span class="kt-subheader__desc">{{ $data['main']->title }}</span>
@endsection
@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head bg-primary text-white">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            {{ $data['main']->title }}
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-section kt-margin-b-0">
                        <div class="kt-section__content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <p class="lead">
                                        {!! $data['main']->definition !!}
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                @foreach($data['heading'] AS $heading)
                                    <div class="col-lg-12">
                                        <p class="lead">{{ $heading->title }}</p>
                                        <ul>
                                            @foreach($heading->items() as $item)
                                                <li>
                                                    <a href="{{ route($routeName, array_merge(['slug' => $item->slug,], $additionalParam)) }}">
                                                        {{ $item->title }}
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection