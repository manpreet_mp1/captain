@section('pageTitle', 'Majors')
@section('subheader')
    <h3 class="kt-subheader__title">Majors</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <span class="kt-subheader__desc"> {{ $data['record']->title }}</span>
@endsection
@section('subheader-toolbar')
    <a href="{{ route($routeName, $additionalParam) }}" class="btn btn-elevate btn-secondary btn-square">
        << Back
    </a>
@endsection
@section('content')
@include('elements.resources.majors._save_major_modal')
    <div class="col-lg-12 text-right">
        <a href="#save-major" style="min-width: 150px;" class="btn btn-primary">Save Major</a><br/><br/>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head bg-primary text-white">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            {{ $data['record']->title }}
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-section kt-margin-b-0">
                        <div class="kt-section__content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <p class="lead">
                                        {!! $data['record']->definition !!}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head bg-primary text-white">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            SCHOOLS OFFERING {{ $data['record']->title }}
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-section kt-margin-b-0">
                        <div class="kt-section__content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <!--begin: Datatable -->
                                    <table class="table table-striped- table-bordered table-hover table-checkable datatable"
                                           id="kt_table_1">
                                        <thead>
                                        <tr class="bg-dark text-uppercase">
                                            <th class="text-white" title="Field #1">College</th>
                                            <th class="text-white" title="Field #2">Size of College</th>
                                            <th class="text-white" title="Field #3">Degrees Granted</th>
                                            <th class="text-white" title="Field #4">Degrees Granted by type</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($data['record']->colleges as $college)
                                            <tr>
                                                <td>
                                                    <a href="{{ route($collegeRouteName, array_merge($additionalParam, ['slug' => $college->slug])) }}">
                                                        {{ $college->name }}
                                                    </a>
                                                </td>
                                                <td>{{ $college->size_of_college }}</td>
                                                <td>{{ $college->degree_count }}</td>
                                                <td>Associate({{ $college->associate_count ? $college->associate_count : 0 }}),
                                                    Bachelor({{ $college->bachelor_count ? $college->bachelor_count : 0 }}),
                                                    Certificate ({{ $college->certificate_count ? $college->certificate_count : 0 }}),
                                                    Doctor({{ $college->doctor_count ? $college->doctor_count : 0 }}),
                                                    Master({{ $college->master_count ? $college->master_count : 0 }}),
                                                    Other({{ $college->other_count ? $college->other_count : 0 }})
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <!--end: Datatable -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-xl-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head bg-primary text-white">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            {{ $data['record']->title }} STATISTICS
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-section kt-margin-b-0">
                        <div class="kt-section__content">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
                                        <div class="col-md-4 font-w500">
                                            DEGREE TYPE
                                        </div>
                                        <div class="col-md-4 font-w500">
                                            # DEGREES LAST YEAR
                                        </div>
                                        <div class="col-md-4 font-w500">
                                            % OF TOTAL
                                        </div>
                                    </div>
                                    <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
                                        <div class="col-md-4">
                                            Associate's
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getNumber('associate_count') }}
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getPercent('associate_percent') }}
                                        </div>
                                    </div>
                                    <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
                                        <div class="col-md-4">
                                            Bachelor's
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getNumber('bachelor_count') }}
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getPercent('bachelor_percent') }}
                                        </div>
                                    </div>
                                    <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
                                        <div class="col-md-4">
                                            Certificate
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getNumber('certificate_count') }}
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getPercent('certificate_percent') }}
                                        </div>
                                    </div>
                                    <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
                                        <div class="col-md-4">
                                            Doctor's
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getNumber('doctor_count') }}
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getPercent('doctor_percent') }}
                                        </div>
                                    </div>
                                    <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
                                        <div class="col-md-4">
                                            Master's
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getNumber('master_count') }}
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getPercent('master_percent') }}
                                        </div>
                                    </div>
                                    <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
                                        <div class="col-md-4">
                                            Other's
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getNumber('other_count') }}
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getPercent('other_percent') }}
                                        </div>
                                    </div>
                                    <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5 kt-margin-t-20">
                                        <div class="col-md-4 font-w500">
                                            GENDER
                                        </div>
                                        <div class="col-md-4 font-w500">
                                            # DEGREES LAST YEAR
                                        </div>
                                        <div class="col-md-4 font-w500">
                                            % OF TOTAL
                                        </div>
                                    </div>
                                    <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
                                        <div class="col-md-4">
                                            Male
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getNumber('male_count') }}
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getPercent('male_percent') }}
                                        </div>
                                    </div>
                                    <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
                                        <div class="col-md-4">
                                            Female
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getNumber('female_count') }}
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getPercent('female_percent') }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6" style="padding-left: 50px;">
                                    <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
                                        <div class="col-md-4 font-w500">
                                            ETHNICITY
                                        </div>
                                        <div class="col-md-4 font-w500">
                                            # DEGREES LAST YEAR
                                        </div>
                                        <div class="col-md-4 font-w500">
                                            % OF TOTAL
                                        </div>
                                    </div>
                                    <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
                                        <div class="col-md-4">
                                            White
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getNumber('white_count') }}
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getPercent('white_percent') }}
                                        </div>
                                    </div>
                                    <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
                                        <div class="col-md-4">
                                            American Indian or Alaska Native
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getNumber('indian_count') }}
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getPercent('indian_percent') }}
                                        </div>
                                    </div>
                                    <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
                                        <div class="col-md-4">
                                            Asian
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getNumber('asian_count') }}
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getPercent('asian_percent') }}
                                        </div>
                                    </div>
                                    <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
                                        <div class="col-md-4">
                                            Black or African American
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getNumber('black_count') }}
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getPercent('black_percent') }}
                                        </div>
                                    </div>
                                    <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
                                        <div class="col-md-4">
                                            Hispanic
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getNumber('hispanic_count') }}
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getPercent('hispanic_percent') }}
                                        </div>
                                    </div>
                                    <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
                                        <div class="col-md-4">
                                            Native Hawaiian or Other Pacific Islander
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getNumber('native_count') }}
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getPercent('native_percent') }}
                                        </div>
                                    </div>
                                    <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
                                        <div class="col-md-4">
                                            Two or more races
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getNumber('two_or_more_count') }}
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getPercent('two_or_more_percent') }}
                                        </div>
                                    </div>
                                    <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
                                        <div class="col-md-4">
                                            Nonresident alien
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getNumber('non_resident_count') }}
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getPercent('non_resident_percent') }}
                                        </div>
                                    </div>
                                    <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
                                        <div class="col-md-4">
                                            Race/ethnicity unknown
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getNumber('unknown_count') }}
                                        </div>
                                        <div class="col-md-4">
                                            {{ $data['record']->getPercent('unknown_percent') }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function () {
            $('.datatable').DataTable({
                responsive: true,
            });
        });
    </script>
@endsection