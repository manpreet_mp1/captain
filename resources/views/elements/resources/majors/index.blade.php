@section('pageTitle', 'Majors')
@section('subheader')
    <h3 class="kt-subheader__title">Majors</h3>
@endsection
@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head bg-primary text-white">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Click on a category to help narrow it down.
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-section kt-margin-b-0">
                        <div class="kt-section__content">
                            <div class="row">
                                @foreach($data['main']->chunk(3) AS $chunk)
                                    <div class="col-lg-4">
                                        @foreach($chunk AS $item)
                                            <p>
                                                <a href="{{ route($routeName, array_merge(['slug' => $item->slug,], $additionalParam)) }}">
                                                    {{ $item->title }}
                                                </a>
                                            </p>
                                        @endforeach
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection