<div class="row kt-padding-25 splash-image">
    <div class="col-lg-12 text-right">
        <a href="#save-school" style="min-width: 150px;" class="btn btn-primary">Save School</a>
    </div>
    <div class="col-lg-12 text-center">
        <p class="college-title text-white">
            {{ $data['record']->name }}
        </p>
    </div>
    <div class="col-lg-12 text-center">
        <p class="college-address college-reg-text text-white">
            {{ $data['record']->address }} | {{ $data['record']->city }}, {{ $data['record']->state_abbr }} | {{ $data['record']->zip }}
        </p>
    </div>
    <div class="col-lg-12 text-center">
        <p class="college-website college-reg-text text-white">
            <a href="{{ $data['record']->custom_web_addr }}" target="_blank">Visit Official Website</a>
        </p>
    </div>
    <div class="col-lg-12 text-center">
        <div class="college-stats">
            <div class="row">
                <div class="col-sm-3">
                    <i class="blueprintone-university19 font-sba50 text-white"></i>
                    <p class="college-num-text">{{ $data['record']->institute_type }}</p>
                </div>
                <div class="col-sm-3">
                    <i class="{{ $data['record']->custom_locale['class'] }} font-sba50 text-white"></i>
                    <p class="college-num-text">{{ $data['record']->custom_locale['data'] }}</p>
                </div>
                <div class="col-sm-3">
                    <i class="{{ $data['record']->custom_inst_size['class'] }} font-sba50 text-white"></i>
                    <p class="college-num-text">{{ $data['record']->custom_inst_size['data'] }}</p>
                </div>
                <div class="col-sm-3">
                    <i class="blueprintone-calendar146 font-sba50 text-white"></i>
                    <p class="college-num-text">{{ $data['record']->custom_cal_sys['data'] }}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12 text-center">
        <div class="college-stats">
            <div class="row">
                <div class="col-md-4">
                    <p class="college-label-text">Acceptance Rate</p>
                    <p class="college-num-text">{{ $data['record']->getPercent('percent_admitted') }}</p>
                </div>
                <div class="col-md-4">
                    <p class="college-label-text"> SAT Scores (EBRW + Math)</p>
                    <p class="college-num-text">{{ $data['record']->custom_sat_score_range }}</p>
                </div>
                <div class="col-md-4">
                    <p class="college-label-text"> ACT Score</p>
                    <p class="college-num-text">{{ $data['record']->custom_act_score_range }}</p>
                </div>
                <div class="col-md-4">
                    <p class="college-label-text"> Tuition (In-State)</p>
                    <p class="college-num-text">{{ $data['record']->getDollar('original_in_state_cost') }}</p>
                </div>
                <div class="col-md-4">
                    <p class="college-label-text"> Graduation Rate</p>
                    <p class="college-num-text">{{ $data['record']->getPercent('graduation_rate_total') }}</p>
                </div>
                <div class="col-md-4">
                    <p class="college-label-text">Salary After Attending
                        <i class="fa fa-info-circle small text-muted" data-skin="dark" data-toggle="kt-tooltip" data-placement="top" data-html="true" title="" data-original-title="<strong>Salary After Attending</strong> The median earnings of former students who received federal financial aid, at 10 years after entering the school.<br><br>This information is based on all locations of this school."></i>
                    </p>
                    <p class="college-num-text">{{ $data['record']->getDollar('avg_earn_wne_p10') }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row college-about kt-padding-t-25">
    <div class="col-lg-12 text-center">
        <p class="college-about-header">College Generosity Scale </p>
        @include('elements.resources.colleges.generosity_image_horizontal')
    </div>
    <div class="col-lg-12 kt-padding-t-25">
        <p class="college-about-header text-center">
            About </p>
        <div class="about-college-stats">
            <div class="row">
                <div class="col-lg-6">
                    <p class="college-about-paragraph">{{ $data['record']->about_one }} </p>
                    <p class="college-about-paragraph">{{ $data['record']->about_three }} </p>
                </div>
                <div class="col-lg-6">
                    <p class="college-about-paragraph">{{ $data['record']->about_two }} </p>
                    <p class="college-about-meta">{!! $data['record']->photo_meta !!}</p>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .college-about {
        background-color: #141b2e;
    }

    .college-about-paragraph {
        font-size: 17px;
        font-weight: 300;
        line-height: 1.7;
        color: #fff;
    }

    .college-about-meta, .college-about-meta a {
        color: #fff;
        font-size: 13px;
        font-style: italic;
        font-weight: 300;
        line-height: 1.7;
        padding: 0 0 30px;
    }

    .splash-gradient {
        background-image: -webkit-gradient(linear, left top, left bottom, from(rgba(20, 27, 46, .5)), color-stop(10%, rgba(20, 27, 46, .3)), color-stop(50%, rgba(20, 27, 46, .5)), to(#141b2e));
        background-image: linear-gradient(rgba(20, 27, 46, .5), rgba(20, 27, 46, .3) 10%, rgba(20, 27, 46, .5) 50%, #141b2e);
    }

    .splash-image {
        background: linear-gradient(rgba(20, 27, 46, .5), rgba(20, 27, 46, .3) 10%, rgba(20, 27, 46, .5) 50%, #141b2e), url({{ $data['record']->splash }});
        {{--background: linear-gradient(to right, rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), url({{ $data['record']->splash }});--}}
         background-position: 50% 50%;
        background-size: cover;
        background-repeat: no-repeat;
    }

    .splash-image:after {
        background-color: rgba(0, 0, 0, 0.8);
    }

    .splash-image .college-title {
        color: #fff;
        -webkit-box-flex: 1;
        -ms-flex: 1;
        flex: 1 1;
        font-weight: 400;
        letter-spacing: .03em;
        padding: 0 10px;
        text-shadow: 0 2px 6px #000;
        text-transform: uppercase;
        font-size: 50px;
        line-height: 1.1;
    }

    .college-about-header {
        color: #fff;
        -webkit-box-flex: 1;
        -ms-flex: 1;
        flex: 1 1;
        font-weight: 400;
        letter-spacing: .03em;
        padding: 0 10px;
        text-shadow: 0 2px 6px #000;
        text-transform: uppercase;
        font-size: 30px;
        line-height: 1.1;
    }

    .splash-image .college-reg-text, .splash-image .college-reg-text a {
        font-size: 20px;
        font-weight: 400;
        text-shadow: 0 2px 12px #000;
        color: #fff !important;
    }

    .splash-image .college-stats {
        justify-content: center;
        margin: 25px auto;
        max-width: 850px;
        width: 100%;
    }

    .about-college-stats {
        margin: 25px auto;
        max-width: 1000px;
        width: 100%;
    }

    .splash-image .college-num-text {
        font-size: 30px;
        font-weight: 300;
        text-shadow: 0 2px 12px #000;
        color: #fff !important;
    }

    .splash-image .college-label-text {
        font-size: 12px;
        font-weight: 600;
        line-height: 1.5;
        text-shadow: 0 2px 6px #000;
        text-transform: uppercase;
        color: #fff !important;
        margin-bottom: 5px;
    }

</style>
