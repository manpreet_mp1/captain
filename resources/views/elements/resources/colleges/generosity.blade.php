<div class="canvas-holder">
    <div class="chart-gauge" width="400" height="220" style="width: 400px; height: 220px;"></div>
</div>

<style type="text/css">
    .chart-gauge {
        width: 400px;
    }

    .chart-color1 {
        fill: red;
    }

    .chart-color2 {
        fill: yellow;
    }

    .chart-color3 {
        fill: green;
    }

    .needle,
    .needle-center {
        fill: #6239bd;
    }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.3.3/d3.min.js"></script>
<script>
    // jQuery(document).ready(function () {
        const percent = 0;
        const barWidth = 60;
        const numSections = 3;

        // / 2 for HALF circle
        const sectionPerc = 1 / numSections / 2;
        const padRad = 0.05;

        const chartInset = 10;

        // start at 270deg
        let totalPercent = .75;

        const el = d3.select('.chart-gauge');

        const margin = {top: 20, right: 20, bottom: 30, left: 20};
        const width = el[0][0].offsetWidth - margin.left - margin.right;
        const height = width;
        const radius = Math.min(width, height) / 2;

        const percToDeg = perc => perc * 360;

        const percToRad = perc => degToRad(percToDeg(perc));

        var degToRad = deg => (deg * Math.PI) / 180;

        const svg = el.append('svg')
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom);

        const chart = svg.append('g')
            .attr('transform', `translate(${(width + margin.left) / 2}, ${(height + margin.top) / 2})`);

        // build gauge bg
        for (let sectionIndx = 1, end = numSections, asc = 1 <= end; asc ? sectionIndx <= end : sectionIndx >= end; asc ? sectionIndx++ : sectionIndx--) {

            const arcStartRad = percToRad(totalPercent);
            const arcEndRad = arcStartRad + percToRad(sectionPerc);
            totalPercent += sectionPerc;

            const startPadRad = sectionIndx === 0 ? 0 : padRad / 2;
            const endPadRad = sectionIndx === numSections ? 0 : padRad / 2;

            const arc = d3.svg.arc()
                .outerRadius(radius - chartInset)
                .innerRadius(radius - chartInset - barWidth)
                .startAngle(arcStartRad + startPadRad)
                .endAngle(arcEndRad - endPadRad);

            chart.append('path')
                .attr('class', `arc chart-color${sectionIndx}`)
                .attr('d', arc);
        }

        class Needle {

            constructor(len, radius1) {
                this.len = len;
                this.radius = radius1;
            }

            drawOn(el, perc) {

                el.append('circle')
                    .attr('class', 'needle-center')
                    .attr('cx', 0)
                    .attr('cy', 0)
                    .attr('r', this.radius);


                return el.append('path')
                    .attr('class', 'needle')
                    .attr('d', this.mkCmd(perc));
            }

            animateOn(el, perc) {
                const self = this;
                return el
                    .transition()
                    .delay(500)
                    .ease('elastic')
                    .duration(3000)
                    .selectAll('.needle')
                    .tween('progress', () => (function (percentOfPercent) {
                        const progress = percentOfPercent * perc;
                        return d3
                            .select(this)
                            .attr('d', self.mkCmd(progress));
                    }));
            }

            mkCmd(perc) {
                const thetaRad = percToRad(perc / 2); // half circle

                const centerX = 0;
                const centerY = 0;

                const topX = centerX - (this.len * Math.cos(thetaRad));
                const topY = centerY - (this.len * Math.sin(thetaRad));

                const leftX = centerX - (this.radius * Math.cos(thetaRad - (Math.PI / 2)));
                const leftY = centerY - (this.radius * Math.sin(thetaRad - (Math.PI / 2)));

                const rightX = centerX - (this.radius * Math.cos(thetaRad + (Math.PI / 2)));
                const rightY = centerY - (this.radius * Math.sin(thetaRad + (Math.PI / 2)));

                return `M ${leftX} ${leftY} L ${topX} ${topY} L ${rightX} ${rightY}`;
            }
        }

        const needle = new Needle(90, 15);
        needle.drawOn(chart, 0);
        needle.animateOn(chart, percent);

    // });
</script>
<style>
    svg {
        height: 220px;
    }
</style>
