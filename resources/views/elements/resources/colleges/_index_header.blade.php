<div class="kt-portlet">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                {{ $record->name }} <br/>
                <span style="font-size: 80%">{{ $record->address }} | {{ $record->city }}, {{ $record->state_abbr }} | {{ $record->zip }}</span>
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="kt-section kt-margin-b-0">
            <div class="kt-section__content">
                <div class="row">
                    <div class="col-lg-3 text-center">
                        <img src="{{ $record->logo }}"
                             alt="" style="max-width: 200px" class="img-responsive">
                    </div>
                    <div class="col-lg-9">
                        <div class="row">
                            <div class="col-sm-2 col-xs-4 text-center"><span class="font-s75 text-primary-dark">4</span>
                                <p class="font-s20">Years</p>
                            </div>
                            <div class="col-sm-2 col-xs-4 text-center"><i
                                        class="blueprintone-university19 font-sba75 text-primary-dark"> </i>
                                <p class="font-s20">{{ $record->institute_type }}
                                </p>
                            </div>
                            <div class="col-sm-2 col-xs-4 text-center"><i
                                        class="{{ $record->custom_locale['class'] }} font-sba75 text-primary-dark"> </i>
                                <p class="font-s20">{{ $record->custom_locale['data'] }}
                                </p>
                            </div>
                            <div class="col-sm-2 col-xs-4 text-center"><i
                                        class="{{ $record->custom_inst_size['class'] }} font-sba75 text-primary-dark"> </i>
                                <p class="font-s20">{{ $record->custom_inst_size['data'] }}
                                </p>
                            </div>
                            <div class="col-sm-2 col-xs-4 text-center"><i
                                        class="blueprintone-calendar146 font-sba75 text-primary-dark"> </i>
                                <p class="font-s20">{{ $record->custom_cal_sys['data'] }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-divider kt-margin-t-20 kt-margin-b-20">
                    <span></span>
                    <span><i class="fa fa-university"></i></span>
                    <span></span>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-xs-4 text-center" style="border-right: 1px solid #d1cece">
                        <span class="font-s45 text-primary-dark">{{ $record->getPercent('percent_admitted') }}</span>
                        <p class="font-s20 font-w500">Acceptance Rate</p>
                    </div>
                    <div class="col-sm-3 col-xs-4 text-center" style="border-right: 1px solid #d1cece">
                        <span class="font-s45 text-primary-dark">{{ $record->getDollar('original_in_state_cost') }}</span>
                        <p class="font-s20 font-w500"> Tuition (In-State)</p>
                    </div>

                    <div class="col-sm-3 col-xs-4 text-center" style="border-right: 1px solid #d1cece">
                        <span class="font-s45 text-primary-dark">{{ $record->getPercent('graduation_rate_total') }}</span>
                        <p class="font-s20 font-w500"> Graduation Rate</p>
                    </div>
                    <div class="col-sm-3 col-xs-4 text-center" style="">
                        <span class="font-s45 text-primary-dark">{{ $record->getDollar('avg_earn_wne_p10') }}</span>
                        <p class="font-s20 font-w500">Salary After Attending
                            <i class="fa fa-info-circle small text-muted" data-skin="dark" data-toggle="kt-tooltip"
                               data-placement="top"
                               data-html="true" title=""
                               data-original-title="<strong>Salary After Attending</strong> The median earnings of former students who received federal financial aid, at 10 years after entering the school.<br><br>This information is based on all locations of this school."></i>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__foot">
        <div class="row align-items-center">
            <div class="col-lg-6 m--valign-middle">
                <a class="btn btn-brand btn-elevate"
                   href="{{ route($routeName, array_merge(['slug' => $record->slug], $additionalParam)) }}">
                    View College
                </a>
            </div>
        </div>
    </div>
</div>

