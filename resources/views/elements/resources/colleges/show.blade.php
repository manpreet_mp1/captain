@section('pageTitle', 'Colleges')
@section('subheader')
    <h3 class="kt-subheader__title">Colleges</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <span class="kt-subheader__desc">
        <img src="{{ $data['record']->logo }}" alt="" style="max-width: 30px" class="img-responsive"> {{ $data['record']->name }}
    </span>
@endsection
@section('subheader-toolbar')
    <a href="{{ route($routeName, $additionalParam) }}" class="btn btn-elevate btn-secondary btn-square">
        << Back
    </a>
@endsection
@section('content')
    @include('elements.resources.colleges._header_data_usa')
    @include('elements.resources.colleges._save_college_modal')
{{--    @include('elements.resources.colleges._header')--}}
    <div class="row kt-portlet"
         style="-webkit-border-radius: 0;-moz-border-radius: 0;border-radius: 0;">
        <div class="col-lg-12 kt-portlet__body">
            <div class="kt-grid  kt-wizard-v2 kt-wizard-v2--white" id="kt_wizard_v2" data-ktwizard-state="step-first">
                <div class="kt-grid__item kt-wizard-v2__aside">
                    <!--begin: Form Wizard Nav -->
                    <div class="kt-wizard-v2__nav">
                        <div class="kt-wizard-v2__nav-items">
                            <a class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step"
                               data-ktwizard-state="current">
                                <div class="kt-wizard-v2__nav-body">
                                    <div class="kt-wizard-v2__nav-icon">
                                        <i class="fa fa-globe"></i>
                                    </div>
                                    <div class="kt-wizard-v2__nav-label">
                                        <div class="kt-wizard-v2__nav-label-title font-s22 kt-margin-t-5 text-uppercase">
                                            Admissions
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
                                <div class="kt-wizard-v2__nav-body">
                                    <div class="kt-wizard-v2__nav-icon">
                                        <i class="fa fa-globe"></i>
                                    </div>
                                    <div class="kt-wizard-v2__nav-label">
                                        <div class="kt-wizard-v2__nav-label-title font-s22 kt-margin-t-5 text-uppercase">
                                            Cost
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
                                <div class="kt-wizard-v2__nav-body">
                                    <div class="kt-wizard-v2__nav-icon">
                                        <i class="fa fa-globe"></i>
                                    </div>
                                    <div class="kt-wizard-v2__nav-label">
                                        <div class="kt-wizard-v2__nav-label-title font-s22 kt-margin-t-5 text-uppercase">
                                            Financial Aid
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
                                <div class="kt-wizard-v2__nav-body">
                                    <div class="kt-wizard-v2__nav-icon">
                                        <i class="fa fa-globe"></i>
                                    </div>
                                    <div class="kt-wizard-v2__nav-label">
                                        <div class="kt-wizard-v2__nav-label-title font-s22 kt-margin-t-5 text-uppercase">
                                            Academics
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
                                <div class="kt-wizard-v2__nav-body">
                                    <div class="kt-wizard-v2__nav-icon">
                                        <i class="fa fa-globe"></i>
                                    </div>
                                    <div class="kt-wizard-v2__nav-label">
                                        <div class="kt-wizard-v2__nav-label-title font-s22 kt-margin-t-5 text-uppercase">
                                            Student Body
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
                                <div class="kt-wizard-v2__nav-body">
                                    <div class="kt-wizard-v2__nav-icon">
                                        <i class="fa fa-globe"></i>
                                    </div>
                                    <div class="kt-wizard-v2__nav-label">
                                        <div class="kt-wizard-v2__nav-label-title font-s22 kt-margin-t-5 text-uppercase">
                                            Merit Scholarships
                                        </div>
                                    </div>
                                </div>
                            </a>

                        </div>
                    </div>

                    <!--end: Form Wizard Nav -->
                </div>
                <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v2__wrapper">

                    <!--begin: Form Wizard Form-->
                    <div style="padding: 4rem 6rem 6rem;">

                        <!--begin: Form Wizard Step 1-->
                        <div class="kt-wizard-v2__content" data-ktwizard-type="step-content"
                             data-ktwizard-state="current">
                            @include('elements.resources.colleges._admissions')
                        </div>
                        <!--end: Form Wizard Step 1-->

                        <!--begin: Form Wizard Step 2-->
                        <div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
                            @include('elements.resources.colleges._cost')
                        </div>
                        <!--end: Form Wizard Step 2-->

                        <!--begin: Form Wizard Step 3-->
                        <div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
                            @include('elements.resources.colleges._financial_aid')
                        </div>
                        <!--end: Form Wizard Step 3-->

                        <!--begin: Form Wizard Step 4-->
                        <div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
                            @include('elements.resources.colleges._academics')
                        </div>
                        <!--end: Form Wizard Step 4-->

                        <!--begin: Form Wizard Step 5-->
                        <div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
                            @include('elements.resources.colleges._student_body')
                        </div>
                        <!--end: Form Wizard Step 5-->

                        <!--begin: Form Wizard Step 6-->
                        <div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
                            @include('elements.resources.colleges._merit_scholarships')
                        </div>
                        <!--end: Form Wizard Step 6-->
                    </div>
                    <!--end: Form Wizard Form-->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function () {
            // Initialize form wizard
            wizard = new KTWizard('kt_wizard_v2', {
                startStep: 1,
            });

            // Change event
            wizard.on('change', function (wizard) {
                var position = $("#kt_wizard_v2").offset().top - 100;
                $("body, html").animate({
                    scrollTop: position
                }, 'slow');
            });
        });
    </script>
@endsection
