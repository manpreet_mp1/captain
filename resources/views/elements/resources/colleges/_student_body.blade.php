<div class="row">
    <div class="col-lg-12">
        <h2 class="text-primary font-s45 font-w400 text-uppercase kt-margin-b-30">
            About the Students
        </h2>
    </div>
    <div class="col-lg-6">
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Female Undergrads
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getPercent('ug_women_percent') }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Male Undergrads
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getPercent('ug_men_percent') }}
            </div>
        </div>
    </div>
    <div class="col-lg-6">
    </div>
    <div class="col-lg-6">
        <div class="kt-divider kt-margin-t-20 kt-margin-b-20"></div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-12 font-w500">
                Student Primary Residence
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                In-State
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getPercent('ug_in_state_percent') }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Out-of-State
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getPercent('ug_out_state_percent') }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                International
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getPercent('ug_foreign_percent') }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Unknown
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getPercent('ug_unknown_percent') }}
            </div>
        </div>
    </div>
    <div class="col-lg-6" style="padding-left: 50px;">
        <div class="kt-divider kt-margin-t-20 kt-margin-b-20"></div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-12 font-w500">
                Household Income Levels
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                <$30k
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->ug_30k_percent }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                $30-48k
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->ug_30k_48k_percent }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                $49-75k
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->ug_48k_75k_percent }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                $76-110k
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->ug_75k_110k_percent }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                $110k+
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->ug_110k_plus_percent }}
            </div>
        </div>
    </div>
</div>
<div class="kt-divider kt-margin-t-20 kt-margin-b-20">
    <span></span>
    <span><i class="fa fa-university"></i></span>
    <span></span>
</div>
<div class="row">
    <div class="col-lg-12">
        <h2 class="text-primary font-s45 font-w400 text-uppercase kt-margin-b-30">
            Ethnic Diversity
        </h2>
    </div>
    <div class="col-lg-12">
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                African American
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getPercent('ug_african_percent') }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Asian
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getPercent('ug_asian_percent') }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Hispanic
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getPercent('ug_hispanic_percent') }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                International (Non-Citizen)
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getPercent('ug_non_resident_percent') }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Multiracial
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getPercent('ug_multiracial_percent') }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Native American
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getPercent('ug_native_percent') }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Pacific Islander
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getPercent('ug_pacific_percent') }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Unknown
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getPercent('ug_unknown_race_percent') }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                White
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getPercent('ug_white_percent') }}
            </div>
        </div>
    </div>
</div>