<div class="row bg-success kt-padding-25">
    <div class="col-lg-2 text-center">
        <img src="{{ $data['record']->logo }}"
            alt="" style="max-width: 200px" class="img-responsive">
    </div>
    <div class="col-lg-6">
        <h1>
            {{ $data['record']->name }}
        </h1>
        <h3 class="font-w400">
            {{ $data['record']->address }} | {{ $data['record']->city }}, {{ $data['record']->state_abbr }} | {{ $data['record']->zip }}
        </h3>
        <h3 class="font-w400">
            <a href="{{ $data['record']->custom_web_addr }}" target="_blank">Visit Official Website</a>
        </h3>
        <div class="row">
{{--            <div class="col-sm-2 col-xs-4 text-left"><span class="font-s75 text-primary-dark">4</span>--}}
{{--                <p class="font-s20">Years</p>--}}
{{--            </div>--}}
            <div class="col-sm-3 col-xs-4 text-left"><i
                    class="blueprintone-university19 font-sba75 text-primary-dark"> </i>
                <p class="font-s20">{{ $data['record']->institute_type }}
                </p>
            </div>
            <div class="col-sm-3 col-xs-4 text-left"><i
                    class="{{ $data['record']->custom_locale['class'] }} font-sba75 text-primary-dark"> </i>
                <p class="font-s20">{{ $data['record']->custom_locale['data'] }}
                </p>
            </div>
            <div class="col-sm-3 col-xs-4 text-left"><i
                    class="{{ $data['record']->custom_inst_size['class'] }} font-sba75 text-primary-dark"> </i>
                <p class="font-s20">{{ $data['record']->custom_inst_size['data'] }}
                </p>
            </div>
            <div class="col-sm-3 col-xs-4 text-left"><i
                    class="blueprintone-calendar146 font-sba75 text-primary-dark"> </i>
                <p class="font-s20">{{ $data['record']->custom_cal_sys['data'] }}
                </p>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="row">
            <div class="col-lg-12 text-right">
                <a href="#" style="min-width: 150px;" class="btn btn-primary">Save School</a>
            </div>
            <div class="col-lg-12">
                @include('elements.resources.colleges._generosity')
            </div>
        </div>
    </div>
</div>
<div class="row bg-muted" style="background: #f7f8fa; padding: 20px">
    <div class="col-sm-2 col-xs-4 text-center" style="border-right: 1px solid #d1cece">
        <span class="font-s45 text-primary-dark">{{ $data['record']->getPercent('percent_admitted') }}</span>
        <p class="font-s20 font-w500">Acceptance Rate</p>
    </div>
    <div class="col-sm-2 col-xs-4 text-center" style="border-right: 1px solid #d1cece">
        <span class="font-s45 text-primary-dark">{{ $data['record']->custom_sat_score_range }}</span>
        <p class="font-s20 font-w500"> SAT Scores (EBRW + Math)</p>
    </div>
    <div class="col-sm-2 col-xs-4 text-center" style="border-right: 1px solid #d1cece">
        <span class="font-s45 text-primary-dark">{{ $data['record']->custom_act_score_range }}</span>
        <p class="font-s20 font-w500"> ACT Score</p>
    </div>
    <div class="col-sm-2 col-xs-4 text-center" style="border-right: 1px solid #d1cece">
        <span class="font-s45 text-primary-dark">{{ $data['record']->getDollar('original_in_state_cost') }}</span>
        <p class="font-s20 font-w500"> Tuition (In-State)</p>
    </div>

    <div class="col-sm-2 col-xs-4 text-center" style="border-right: 1px solid #d1cece">
        <span class="font-s45 text-primary-dark">{{ $data['record']->getPercent('graduation_rate_total') }}</span>
        <p class="font-s20 font-w500"> Graduation Rate</p>
    </div>
    <div class="col-sm-2 col-xs-4 text-center" style="">
        <span class="font-s45 text-primary-dark">{{ $data['record']->getDollar('avg_earn_wne_p10') }}</span>
        <p class="font-s20 font-w500">Salary After Attending
            <i class="fa fa-info-circle small text-muted" data-skin="dark" data-toggle="kt-tooltip" data-placement="top"
                data-html="true" title=""
                data-original-title="<strong>Salary After Attending</strong> The median earnings of former students who received federal financial aid, at 10 years after entering the school.<br><br>This information is based on all locations of this school."></i>
        </p>
    </div>
</div>
