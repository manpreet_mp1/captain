<div class="generosity-hor-main kt-margin-t-20 kt-margin-b-20">
    @if($data['family']->is_software_or_bundle)
        <div class="hor-scale"></div>
    @endif
</div>
@if(!$data['family']->is_software_or_bundle)
    <div class="">
        <p class="upgrade-text">Upgrade your subscription</p>
    </div>
@endif
<style type="text/css">
    .upgrade-text {
        font-size: 20px;
        font-weight: 300;
        text-shadow: 0 2px 12px #000;
        color: #6239bd !important;
        text-transform: uppercase;
    }
    .generosity-hor-main {
        width: 800px;
        height: 118px;
        position: relative;
        display: inline-block;
        background-image: url({{ asset('images/resources/horizontalScale.png') }});
        background-size: contain;
        @if(!$data['family']->is_software_or_bundle)
        -webkit-filter: blur(5px);
            -moz-filter: blur(5px);
            -o-filter: blur(5px);
            -ms-filter: blur(5px);
            filter: blur(5px);
            background-color: #cccccc36;
        @endif
    }
    .hor-scale {
        width: 30px;
        height: 118px;
        position: absolute;
        background-image: url({{ asset('images/resources/slider.png') }});
        background-size: contain;
        background-repeat: no-repeat;
        left: {{$data['record']->generosity_number}}%;
    }
</style>
