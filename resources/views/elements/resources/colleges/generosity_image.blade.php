<div class="outer">
    @if($data['family']->is_software_or_bundle)
        <div class="needle"></div>
    @endif
</div>
@if(!$data['family']->is_software_or_bundle)
    <div class="">
        <p class="upgrade-text">Upgrade your subscription</p>
    </div>
@endif
<style type="text/css">
    .upgrade-text {
        font-size: 20px;
        font-weight: 300;
        text-shadow: 0 2px 12px #000;
        color: #6239bd !important;
        text-transform: uppercase;
    }
    .outer {
        position: relative;
        display: inline-block;
        width: 780px;
        height: 460px;
        transform:rotateX(0deg);
        background-image: url({{ asset('images/resources/dial.png') }});
        background-size: contain;
        @if(!$data['family']->is_software_or_bundle)
            -webkit-filter: blur(5px);
            -moz-filter: blur(5px);
            -o-filter: blur(5px);
            -ms-filter: blur(5px);
            filter: blur(5px);
            background-color: #cccccc36;
        @endif
    }
    .needle {
        height: 100%;
        width: 100%;
        position: absolute;
        top: 75px;
        left: 0;
        border-radius: 50%;
        /*transition: all 3s;*/
        transform: rotate({{ $data['record']->generosity_degree }}deg);
        transform-origin: center center;
        transition: 1s ease-in-out;
    }
    .needle:before {
        content: "";
        position: absolute;
        top: 5%;
        left: calc(50% - 7.5px);
        height: 53%;
        width: 15px;
        background: #6239bd;
    }
    .needle:after {
        content: "";
        position: absolute;
        top: 15px;
        left: 50%;
        height: 40px;
        width: 40px;
        transform: rotate(45deg);
        transform-origin: top left;
        border-top: 10px solid #6239bd;
        border-left: 10px solid #6239bd;
    }
    .outer:hover .needle {
    }
</style>


{{--<div class="tachometer">--}}
{{--    <div class="needle">--}}
{{--        <img class="arrow" src="{{ asset('images/resources/needle.png') }}">--}}
{{--    </div>--}}
{{--</div>--}}

{{--<style type="text/css">--}}
{{--    .tachometer {--}}
{{--        position: absolute;--}}
{{--        top: 0px;--}}
{{--        left: 0px;--}}
{{--        height: 207px;--}}
{{--        width: 380px;--}}
{{--        background-image: url({{ asset('images/resources/dial.png') }});--}}
{{--        background-repeat: no-repeat;--}}
{{--        z-index: 1;--}}
{{--    }--}}

{{--    .needle {--}}
{{--        z-index: 2;--}}
{{--        position: absolute;--}}
{{--        height: 50px;--}}
{{--        width: 50px;--}}
{{--        background-image: url('');--}}
{{--        background-repeat: no-repeat;--}}
{{--        left: 45%;--}}
{{--        top: 87px;--}}
{{--    }--}}

{{--    .arrow {--}}
{{--        height: 50px;--}}
{{--        transition: transform 2s linear;--}}
{{--        transform-origin: 29% 70%;--}}
{{--        transform-style: preserve-3D;--}}
{{--        -webkit-transform: rotate(0deg);--}}
{{--        transform: rotate(0deg);--}}
{{--    }--}}
{{--</style>--}}

{{--<div class="main-dial">--}}
{{--    <div class="needle">--}}

{{--    </div>--}}
{{--</div>--}}

{{--<style type="text/css">--}}
{{--    .main-dial {--}}
{{--        width: 380px;--}}
{{--        height: 207px;--}}
{{--        background-image: url({{ asset('images/resources/dial.png') }});--}}
{{--    }--}}
{{--    .needle {--}}
{{--        position: relative;--}}
{{--        margin: 0 auto;--}}
{{--        /*top: 155px;*/--}}
{{--        /*left: 100px;*/--}}
{{--        width: 123px;--}}
{{--        height: 46px;--}}
{{--        background-image: url({{ asset('images/resources/needle.png') }});--}}
{{--        -webkit-transform: rotate(45deg);--}}
{{--        transform: rotate(45deg);--}}
{{--    }--}}
{{--</style>--}}
