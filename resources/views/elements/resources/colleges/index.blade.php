@section('pageTitle', 'Colleges')
@section('subheader')
    <h3 class="kt-subheader__title">Colleges</h3>
@endsection
@section('content')
    <div class="row">
        <div class="col-xl-3">
            @include('elements.resources.colleges._search')
        </div>
        <div class="col-xl-9">
            @if($data['records']->count() == 0)
                <div class="alert alert-warning fade show" role="alert">
                    <div class="alert-icon"><i style="font-size: 1.2em;" class="fa fa-exclamation-triangle"></i></div>
                    <div class="alert-text">Warning! No School Found.</div>
                </div>
            @else
                @if(Request::input('search'))
                    <div class="alert alert-success fade show" role="alert">
                        <div class="alert-icon"><i style="font-size: 1.2em;" class="fa fa-th-list"></i></div>
                        <div class="alert-text">{{$data['records']->total()}} records(s) found.</div>
                    </div>
                @endif
                <table class="table table-bordered" style="border: none">
                    @foreach($data['records'] AS $record)
                        @include('elements.resources.colleges._index_header')
                    @endforeach
                </table>
                {!! $data['records']->appends(Request::except('page'))->render() !!}
            @endif
        </div>
    </div>
@endsection