<div class="row">
    <div class="col-lg-12">
        <h2 class="text-primary font-s45 font-w400 text-uppercase kt-margin-b-30">
            Academic Statistics
        </h2>
    </div>
    <div class="col-lg-6">
        <p class="kt-margin-b-0 font-s20">
            Graduation Rate
        </p>
        <p class="text-primary font-w400 text-uppercase font-s45" style="line-height: 1 !important;">
            {{ $data['record']->getPercent('graduation_rate_total') }}
        </p>
    </div>
    <div class="col-lg-6">
    </div>
    <div class="col-lg-6">
        <div class="kt-divider kt-margin-t-20 kt-margin-b-20"></div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Student to Faculty Ratio
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->student_to_faculty }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Full-Time Retention Rate
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getPercent('full_time_retention_rate') }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Part-Time Retention Rate
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getPercent('part_time_retention_rate') }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Academic Calendar
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->custom_cal_sys['data'] }}
            </div>
        </div>
    </div>
    <div class="col-lg-6" style="padding-left: 50px;">
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-12 font-w500">
                Non-traditional Learning
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Evening Degree Programs
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getYesNo('weekend_eve_college') }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Teacher Certification
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getYesNo('teacher_certification') }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Distance Education
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getYesNo('distance_education') }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Study Abroad
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getYesNo('study_abroad') }}
            </div>
        </div>
    </div>
</div>
<!--
<div class="kt-divider kt-margin-t-20 kt-margin-b-20">
    <span></span>
    <span><i class="fa fa-university"></i></span>
    <span></span>
</div>
<div class="row">
    <div class="col-lg-12">
        <h2 class="text-primary font-s45 font-w400 text-uppercase kt-margin-b-30">
            Most Popular Majors
        </h2>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Economics
            </div>
            <div class="col-md-6 text-right font-w500">
                338 <small>Graduates</small>
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Economics
            </div>
            <div class="col-md-6 text-right font-w500">
                338 <small>Graduates</small>
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Economics
            </div>
            <div class="col-md-6 text-right font-w500">
                338 <small>Graduates</small>
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Economics
            </div>
            <div class="col-md-6 text-right font-w500">
                338 <small>Graduates</small>
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Economics
            </div>
            <div class="col-md-6 text-right font-w500">
                338 <small>Graduates</small>
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Economics
            </div>
            <div class="col-md-6 text-right font-w500">
                338 <small>Graduates</small>
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Economics
            </div>
            <div class="col-md-6 text-right font-w500">
                338 <small>Graduates</small>
            </div>
        </div>
    </div>
</div>
-->