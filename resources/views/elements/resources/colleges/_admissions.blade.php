<div class="row">
    <div class="col-lg-12">
        <h2 class="text-primary font-s45 font-w400 text-uppercase kt-margin-b-30">
            Admissions Statistics
        </h2>
    </div>
    <div class="col-lg-6">
        <p class="kt-margin-b-0 font-s20">
            Acceptance Rate
        </p>
        <p class="text-primary font-w400 text-uppercase font-s45" style="line-height: 1 !important;">
            {{ $data['record']->getPercent('percent_admitted') }}
        </p>
    </div>
    <div class="col-lg-6" style="padding-left: 50px;">
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Total Applicants
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getNumber('applicants_total') }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Total Admissions
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getNumber('admissions_total') }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Total Enrolled
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getPercent('admissions_yield') }}
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="kt-divider kt-margin-t-20 kt-margin-b-20"></div>
        <p class="kt-margin-b-0 font-s20">
            SAT Range
        </p>
        <p class="text-primary font-w400 text-uppercase font-s45" style="line-height: 1 !important;">
            {{ $data['record']->custom_sat_score_range }}
        </p>
        <div class="kt-divider kt-margin-t-10 kt-margin-b-10"></div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                SAT Reading
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->sat_read_25 }}-{{ $data['record']->sat_read_75 }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                SAT Math
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->sat_math_25 }}-{{ $data['record']->sat_math_75 }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Students Submitting SAT
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getPercent('sat_submitted_percent') }}
            </div>
        </div>
    </div>
    <div class="col-lg-6" style="padding-left: 50px;">
        <div class="kt-divider kt-margin-t-20 kt-margin-b-20"></div>
        <p class="kt-margin-b-0 font-s20">
            ACT Range
        </p>
        <p class="text-primary font-w400 text-uppercase font-s45" style="line-height: 1 !important;">
            {{ $data['record']->custom_act_score_range }}
        </p>
        <div class="kt-divider kt-margin-t-10 kt-margin-b-10"></div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                ACT English
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->act_eng_25 }}-{{ $data['record']->act_eng_75 }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                ACT Math
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->act_math_25 }}-{{ $data['record']->act_math_75 }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Students Submitting ACT
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getPercent('act_submitted_percent') }}
            </div>
        </div>
    </div>
</div>
<div class="kt-divider kt-margin-t-20 kt-margin-b-20">
    <span></span>
    <span><i class="fa fa-university"></i></span>
    <span></span>
</div>
<div class="row">
    <div class="col-lg-12">
        <h2 class="text-primary font-s45 font-w400 text-uppercase kt-margin-b-30">
            Admissions Requirements
        </h2>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-12 font-w500">
                What Really Matters When Applying
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                High School GPA
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->admission_req_gpa }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                High School Rank
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->admission_req_rank }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                High School Transcript
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->admission_req_record }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                College Prep Courses
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->admission_req_comp_prep_prog }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                SAT/ACT
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->admission_req_test_score }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Recommendations
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->admission_req_rec }}
            </div>
        </div>
    </div>
</div>