<div class="row">
    <div class="col-lg-12">
        <h2 class="text-primary font-s45 font-w400 text-uppercase kt-margin-b-30">
            Financial Aid
        </h2>
    </div>
    <div class="col-lg-6">
        <p class="kt-margin-b-0 font-s20">
            Students Receiving Financial Aid
        </p>
        <p class="text-primary font-w400 text-uppercase font-s45" style="line-height: 1 !important;">
            {{ $data['record']->getPercent('full_first_ug_any_aid_percent') }}
        </p>
        <div class="kt-divider kt-margin-t-10 kt-margin-b-10"></div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-12 font-w500">
                Students Receiving Aid
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Federal Grant Aid
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getPercent('full_first_ug_fed_aid_percent') }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                State Grant Aid
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getPercent('full_first_ug_state_aid_percent') }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Institution Grant Aid
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getPercent('full_first_ug_inst_aid_percent') }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Pell Grant Amount
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getPercent('full_first_ug_pell_grant_percent') }}
            </div>
        </div>
    </div>
    <div class="col-lg-6" style="padding-left: 50px;">
        <p class="kt-margin-b-0 font-s20">
            Average Total Aid Awarded
        </p>
        <p class="text-primary font-w400 text-uppercase font-s45" style="line-height: 1 !important;">
            {{ $data['record']->getDollar('full_first_ug_any_aid_amount') }}<span class="font-s20">/ year</span>
        </p>
        <div class="kt-divider kt-margin-t-10 kt-margin-b-10"></div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-12 font-w500">
                Average Aid Awarded
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Federal Grant Aid
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getDollar('full_first_ug_fed_aid_amount') }}<span class="font-s20">/ year</span>
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                State Grant Aid
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getDollar('full_first_ug_state_aid_amount') }}<span class="font-s20">/ year</span>
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Institution Grant Aid
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getDollar('full_first_ug_inst_aid_amount') }}<span class="font-s20">/ year</span>
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Pell Grant Amount
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getDollar('full_first_ug_pell_grant_amount') }}<span class="font-s20">/ year</span>
            </div>
        </div>
    </div>
</div>