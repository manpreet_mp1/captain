@if($data['meritScholarships']->count() == 0)
    <div class="style-msg alertmsg">
        <div class="alert alert-warning fade show" role="alert">
            <div class="alert-icon"><i style="font-size: 1.2em;" class="fa fa-exclamation-triangle"></i></div>
            <div class="alert-text">Warning! No Scholarship Found.</div>
        </div>
    </div>
@else
    @foreach($data['meritScholarships'] as $record)
        @include('elements.resources.scholarship.merit._item')
    @endforeach
@endif