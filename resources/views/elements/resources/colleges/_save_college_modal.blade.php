@component('components.modal', ['id' => 'save-school', 'maxWidth' => '50%'])
    {!! Form::open([
          'method'=>'POST',
          'id'=> 'save-form',
          'name'=>'save-form',
          'url' => route($saveRouteName, array_merge($additionalParam, ['slug' => $data['record']->slug]))
       ]) !!}
    <div id="save-school-form">
        <div class="row text-left">
            <div class="col-lg-12">
                <h3 class="text-uppercase">Save School</h3>
                <div class="kt-divider kt-margin-b-30">
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div class="col-lg-12">
                <h4 class="kt-margin-b-10">Select the students</h4>
                <div class="kt-checkbox-list">
                    @foreach($data['students'] as $child)
                        <label class="kt-checkbox kt-checkbox--brand">
                            {!! Form::checkbox('family_child_id[]', $child->id, in_array($child->id, $data['savedArray']) ? true : false, []) !!} {{ $child->user->first_name }}
                            <span></span>
                        </label>
                    @endforeach
                </div>
            </div>
            <div class="col-lg-12 kt-margin-t-20 apt-button" v-if="visibility.button">
                <button name='Book Appointment' class='btn btn-brand btn-elevate'>
                    <span class="fa fa-save kt-padding-r-5"></span> Save
                </button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endcomponent
