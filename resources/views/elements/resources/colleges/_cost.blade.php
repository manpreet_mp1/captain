<div class="row">
    <div class="col-lg-12">
        <h2 class="text-primary font-s45 font-w400 text-uppercase kt-margin-b-30">
            Sticker Price
        </h2>
    </div>
    <div class="col-lg-6">
        <p class="kt-margin-b-0 font-s20">
            In-State Tuition
        </p>
        <p class="text-primary font-w400 text-uppercase font-s45" style="line-height: 1 !important;">
            {{ $data['record']->getDollar('original_in_state_cost') }}<span class="font-s20">/ year</span>
        </p>
        <div class="kt-divider kt-margin-t-10 kt-margin-b-10"></div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                On Campus Housing Cost
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getDollar('on_camp_room_board') }}
                <small>/ year</small>
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Off Campus Housing Cost
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getDollar('off_camp_not_family_room_board') }}
                <small>/ year</small>
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Books & Supplies
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getDollar('books_supplies') }}
                <small>/ year</small>
            </div>
        </div>
    </div>
    <div class="col-lg-6" style="padding-left: 50px;">
        <p class="kt-margin-b-0 font-s20">
            Out-of-State Tuition
        </p>
        <p class="text-primary font-w400 text-uppercase font-s45" style="line-height: 1 !important;">
            {{ $data['record']->getDollar('original_out_state_cost') }}<span class="font-s20">/ year</span>
        </p>
        <div class="kt-divider kt-margin-t-10 kt-margin-b-10"></div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Tuition Guarantee Plan
                <i class="fa fa-info-circle small text-muted" data-skin="dark" data-toggle="kt-tooltip"
                   data-placement="top"
                   data-html="true" title=""
                   data-original-title="<strong>Salary After Attending</strong> The median earnings of former students who received federal financial aid, at 10 years after entering the school.<br><br>This information is based on all locations of this school."></i>
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getYesNo('tuition_guaranteed_plan') }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Tuition Payment Plan
                <i class="fa fa-info-circle small text-muted" data-skin="dark" data-toggle="kt-tooltip"
                   data-placement="top"
                   data-html="true" title=""
                   data-original-title="<strong>Salary After Attending</strong> The median earnings of former students who received federal financial aid, at 10 years after entering the school.<br><br>This information is based on all locations of this school."></i>
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getYesNo('tuition_payment_plan') }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Prepaid Tuition Plan
                <i class="fa fa-info-circle small text-muted" data-skin="dark" data-toggle="kt-tooltip"
                   data-placement="top"
                   data-html="true" title=""
                   data-original-title="<strong>Salary After Attending</strong> The median earnings of former students who received federal financial aid, at 10 years after entering the school.<br><br>This information is based on all locations of this school."></i>
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getYesNo('prepaid_tuition_plan') }}
            </div>
        </div>
    </div>
</div>
<div class="kt-divider kt-margin-t-20 kt-margin-b-20">
    <span></span>
    <span><i class="fa fa-university"></i></span>
    <span></span>
</div>
<div class="row">
    <div class="col-lg-12">
        <h2 class="text-primary font-s45 font-w400 text-uppercase kt-margin-b-30">
            Net Price
        </h2>
    </div>
    <div class="col-lg-6">
        <p class="kt-margin-b-0 font-s20">
            Net Price
        </p>
        <p class="text-primary font-w400 text-uppercase font-s45" style="line-height: 1 !important;">
            {{ $data['record']->getDollar('net_price') }}<span class="font-s20">/ year</span>
        </p>
        <div class="kt-divider kt-margin-t-10 kt-margin-b-10"></div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                Net Price Calculator
            </div>
            <div class="col-md-6 text-right font-w500">
                <a href="{{ $data['record']->custom_net_price_addr }}" target="_blank">Click Here</a>
            </div>
        </div>
    </div>
    <div class="col-lg-6" style="padding-left: 50px;">
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-12 font-w500">
                Net Price by Household Income
                <i class="fa fa-info-circle small text-muted" data-skin="dark" data-toggle="kt-tooltip"
                   data-placement="top"
                   data-html="true" title=""
                   data-original-title="<strong>Salary After Attending</strong> The median earnings of former students who received federal financial aid, at 10 years after entering the school.<br><br>This information is based on all locations of this school."></i>
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                <$30k
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getDollar('net_price_0_30k') }}<span class="font-s20">/ year</span>
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                $30-48k
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getDollar('net_price_30k_48k') }}<span class="font-s20">/ year</span>
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                $49-75k
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getDollar('net_price_48k_75k') }}<span class="font-s20">/ year</span>
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                $76-110k
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getDollar('net_price_75k_110k') }}<span class="font-s20">/ year</span>
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                $110k+
            </div>
            <div class="col-md-6 text-right font-w500">
                {{ $data['record']->getDollar('net_price_110k_plus') }}<span class="font-s20">/ year</span>
            </div>
        </div>
        <div class="border-bottom row font-s17 text-muted kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-12">
                Net price is the average cost after financial aid for students receiving grant or scholarship aid, as
                reported by the college.
            </div>
        </div>
    </div>
</div>