<div class="kt-portlet">
    <div class="kt-portlet__body">
        {!! Form::model($data['request'], ['method' => 'get' ]) !!}
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-uppercase kt-margin-b-10">
                    ENTER COLLEGE NAME
                </h4>
                {!! Form::text('name', null, [
                    'class' => 'form-control',
                    'placeholder' => 'College Name'
                    ])
                !!}
            </div>
            <div class="col-lg-12 kt-margin-t-20">
                <h4 class="text-uppercase kt-margin-b-10">
                    SELECT STATE
                </h4>
                {!! Form::select('state_abbr', $data['states'], null, [ 'class' => 'form-control']) !!}
            </div>
            <div class="col-lg-12 kt-margin-t-20">
                <h4 class="text-uppercase kt-margin-b-10">
                    INSTITUTE TYPE
                </h4>
                {!! Form::select('institute_type', $data['instituteType'], null, [ 'class' => 'form-control']) !!}
            </div>
            <div class="col-lg-12 kt-margin-t-20">
                <h4 class="text-uppercase kt-margin-b-10">
                    ENROLLMENT SIZE
                </h4>
                {!! Form::select('enrollment_size', $data['enrollmentSize'], null, [ 'class' => 'form-control']) !!}
            </div>
            <div class="col-lg-12 kt-margin-t-20">
                <h4 class="text-uppercase kt-margin-b-10">
                    CAMPUS SETTING
                </h4>
                {!! Form::select('locale', $data['locale'], null, [ 'class' => 'form-control']) !!}
            </div>
            <div class="col-lg-6 kt-margin-t-20">
                {!! Form::button('<span class="fa fa-search"></span> Search', ['name' => 'search', 'class'=> 'btn btn-brand btn-elevate btn-block', 'type'=>'submit', 'value' => 'search' ]) !!}
            </div>
        </div>

        {!! Form::close() !!}
    </div>
</div>