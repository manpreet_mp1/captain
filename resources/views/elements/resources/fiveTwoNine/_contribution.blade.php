<div class="row">
    <div class="col-lg-12">
        <h2 class="text-primary font-s45 font-w400 text-uppercase kt-margin-b-30">
            CONTRIBUTIONS
        </h2>
    </div>
    <div class="col-lg-8">
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-8">
                "Minimum" Initial Contribution
            </div>
            <div class="col-md-4 text-right font-w500">
                {{ optional($data['record']->investmentOptions->first())->minimum_initial_contribution }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-8">
                "Minimum" Subsequent Contribution
            </div>
            <div class="col-md-4 text-right font-w500">
                {{ optional($data['record']->investmentOptions->first())->minimum_subsequent_contribution }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-8">
                "Maximum" Total Contribution
            </div>
            <div class="col-md-4 text-right font-w500">
                {{ optional($data['record']->investmentOptions->first())->maximum_total_contribution }}
            </div>
        </div>
    </div>
</div>