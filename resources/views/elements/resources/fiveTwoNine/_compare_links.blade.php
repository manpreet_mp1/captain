<div class="row kt-margin-t-20">
    <div class="col-lg-12 bg-dark text-white font-s18 kt-padding-5">
        Links
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5 kt-margin-t-5">
    <div class="col-md-3 font-w500 text-uppercase">
        WEBSITE
    </div>
    <div class="col-md-3">
        @if(optional($data['plans']['planOne'])->getUrl('website') <> '#' && optional($data['plans']['planOne'])->getUrl('website') <> null)
            <a href="{{ optional($data['plans']['planOne'])->getUrl('website') }}" target="_blank">Link</a>
        @endif
    </div>
    <div class="col-md-3">
        @if(optional($data['plans']['planTwo'])->getUrl('website') <> '#' && optional($data['plans']['planTwo'])->getUrl('website') <> null)
            <a href="{{ optional($data['plans']['planTwo'])->getUrl('website') }}" target="_blank">Link</a>
        @endif
    </div>
    <div class="col-md-3">
        @if(optional($data['plans']['planThree'])->getUrl('website') <> '#' && optional($data['plans']['planThree'])->getUrl('website') <> null)
            <a href="{{ optional($data['plans']['planThree'])->getUrl('website') }}" target="_blank">Link</a>
        @endif
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5">
    <div class="col-md-3 font-w500 text-uppercase">
        OFFERING MATERIALS
    </div>
    <div class="col-md-3">
        @if(optional($data['plans']['planOne'])->getUrl('offering_material_url') <> '#' && optional($data['plans']['planOne'])->getUrl('offering_material_url') <> null)
            <a href="{{ optional($data['plans']['planOne'])->getUrl('offering_material_url') }}" target="_blank">Link</a>
        @endif
    </div>
    <div class="col-md-3">
        @if(optional($data['plans']['planTwo'])->getUrl('offering_material_url') <> '#' && optional($data['plans']['planTwo'])->getUrl('offering_material_url') <> null)
            <a href="{{ optional($data['plans']['planTwo'])->getUrl('offering_material_url') }}" target="_blank">Link</a>
        @endif
    </div>
    <div class="col-md-3">
        @if(optional($data['plans']['planThree'])->getUrl('offering_material_url') <> '#' && optional($data['plans']['planThree'])->getUrl('offering_material_url') <> null)
            <a href="{{ optional($data['plans']['planThree'])->getUrl('offering_material_url') }}" target="_blank">Link</a>
        @endif
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5">
    <div class="col-md-3 font-w500 text-uppercase">
        ENROLLMENT MATERIALS
    </div>
    <div class="col-md-3">
        @if(optional($data['plans']['planOne'])->getUrl('enrollment_material_url') <> '#' && optional($data['plans']['planOne'])->getUrl('enrollment_material_url') <> null)
            <a href="{{ optional($data['plans']['planOne'])->getUrl('enrollment_material_url') }}" target="_blank">Link</a>
        @endif
    </div>
    <div class="col-md-3">
        @if(optional($data['plans']['planTwo'])->getUrl('enrollment_material_url') <> '#' && optional($data['plans']['planTwo'])->getUrl('enrollment_material_url') <> null)
            <a href="{{ optional($data['plans']['planTwo'])->getUrl('enrollment_material_url') }}" target="_blank">Link</a>
        @endif
    </div>
    <div class="col-md-3">
        @if(optional($data['plans']['planThree'])->getUrl('enrollment_material_url') <> '#' && optional($data['plans']['planThree'])->getUrl('enrollment_material_url') <> null)
            <a href="{{ optional($data['plans']['planThree'])->getUrl('enrollment_material_url') }}" target="_blank">Link</a>
        @endif
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5">
    <div class="col-md-3 font-w500 text-uppercase">
        ONLINE ACCOUNT ACCESS
    </div>
    <div class="col-md-3">
        @if(optional($data['plans']['planOne'])->getUrl('online_account_access_url') <> '#' && optional($data['plans']['planOne'])->getUrl('online_account_access_url') <> null)
            <a href="{{ optional($data['plans']['planOne'])->getUrl('online_account_access_url') }}" target="_blank">Link</a>
        @endif
    </div>
    <div class="col-md-3">
        @if(optional($data['plans']['planTwo'])->getUrl('online_account_access_url') <> '#' && optional($data['plans']['planTwo'])->getUrl('online_account_access_url') <> null)
            <a href="{{ optional($data['plans']['planTwo'])->getUrl('online_account_access_url') }}" target="_blank">Link</a>
        @endif
    </div>
    <div class="col-md-3">
        @if(optional($data['plans']['planThree'])->getUrl('online_account_access_url') <> '#' && optional($data['plans']['planThree'])->getUrl('online_account_access_url') <> null)
            <a href="{{ optional($data['plans']['planThree'])->getUrl('online_account_access_url') }}" target="_blank">Link</a>
        @endif
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5">
    <div class="col-md-3 font-w500 text-uppercase">
        GENERAL PERFORMANCE
    </div>
    <div class="col-md-3">
        @if(optional($data['plans']['planOne'])->getUrl('general_performance_url') <> '#' && optional($data['plans']['planOne'])->getUrl('general_performance_url') <> null)
            <a href="{{ optional($data['plans']['planOne'])->getUrl('general_performance_url') }}" target="_blank">Link</a>
        @endif
    </div>
    <div class="col-md-3">
        @if(optional($data['plans']['planTwo'])->getUrl('general_performance_url') <> '#' && optional($data['plans']['planTwo'])->getUrl('general_performance_url') <> null)
            <a href="{{ optional($data['plans']['planTwo'])->getUrl('general_performance_url') }}" target="_blank">Link</a>
        @endif
    </div>
    <div class="col-md-3">
        @if(optional($data['plans']['planThree'])->getUrl('general_performance_url') <> '#' && optional($data['plans']['planThree'])->getUrl('general_performance_url') <> null)
            <a href="{{ optional($data['plans']['planThree'])->getUrl('general_performance_url') }}" target="_blank">Link</a>
        @endif
    </div>
</div>