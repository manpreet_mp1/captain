<div class="row kt-margin-t-20">
    <div class="col-lg-12 bg-dark text-white font-s18 kt-padding-5">
        Contributions
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5 kt-margin-t-5">
    <div class="col-md-3 font-w500 text-uppercase">
        INVESTMENT OPTION TYPE(S)
    </div>
    <div class="col-md-3">
        @if(optional(optional(optional($data['plans']['planOne'])->investmentOptions())->get())->unique('option_type'))
            <ul class="kt-padding-l-0">
                @foreach (optional(optional(optional($data['plans']['planOne'])->investmentOptions())->get())->unique('option_type') as $option)
                    <li> {{ $option->option_type }} </li>
                @endforeach
            </ul>
        @endif
    </div>
    <div class="col-md-3">
        @if(optional(optional(optional($data['plans']['planTwo'])->investmentOptions())->get())->unique('option_type'))
            <ul class="kt-padding-l-0">
                @foreach (optional(optional(optional($data['plans']['planTwo'])->investmentOptions())->get())->unique('option_type') as $option)
                    <li> {{ $option->option_type }} </li>
                @endforeach
            </ul>
        @endif
    </div>
    <div class="col-md-3">
        @if(optional(optional(optional($data['plans']['planThree'])->investmentOptions())->get())->unique('option_type'))
            <ul class="kt-padding-l-0">
                @foreach (optional(optional(optional($data['plans']['planThree'])->investmentOptions())->get())->unique('option_type') as $option)
                    <li> {{ $option->option_type }} </li>
                @endforeach
            </ul>
        @endif
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5">
    <div class="col-md-3 font-w500 text-uppercase">
        "MINIMUM" INITIAL CONTRIBUTION
    </div>
    <div class="col-md-3">
        {{ optional(optional(optional($data['plans']['planOne'])->investmentOptions)->first())->minimum_initial_contribution }}
    </div>
    <div class="col-md-3">
        {{ optional(optional(optional($data['plans']['planTwo'])->investmentOptions)->first())->minimum_initial_contribution }}
    </div>
    <div class="col-md-3">
        {{ optional(optional(optional($data['plans']['planThree'])->investmentOptions)->first())->minimum_initial_contribution }}
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5">
    <div class="col-md-3 font-w500 text-uppercase">
        "MINIMUM" SUBSEQUENT CONTRIBUTION
    </div>
    <div class="col-md-3">
        {{ optional(optional(optional($data['plans']['planOne'])->investmentOptions)->first())->minimum_subsequent_contribution }}
    </div>
    <div class="col-md-3">
        {{ optional(optional(optional($data['plans']['planTwo'])->investmentOptions)->first())->minimum_subsequent_contribution }}
    </div>
    <div class="col-md-3">
        {{ optional(optional(optional($data['plans']['planThree'])->investmentOptions)->first())->minimum_subsequent_contribution }}
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5">
    <div class="col-md-3 font-w500 text-uppercase">
        "MAXIMUM" TOTAL CONTRIBUTION
    </div>
    <div class="col-md-3">
        {{ optional(optional(optional($data['plans']['planOne'])->investmentOptions)->first())->maximum_total_contribution }}
    </div>
    <div class="col-md-3">
        {{ optional(optional(optional($data['plans']['planTwo'])->investmentOptions)->first())->maximum_total_contribution }}
    </div>
    <div class="col-md-3">
        {{ optional(optional(optional($data['plans']['planThree'])->investmentOptions)->first())->maximum_total_contribution }}
    </div>
</div>