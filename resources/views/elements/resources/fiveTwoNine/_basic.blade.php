<div class="row">
    <div class="col-lg-12">
        <h2 class="text-primary font-s45 font-w400 text-uppercase kt-margin-b-30">
            BASIC INFORMATION
        </h2>
    </div>
    <div class="col-lg-6">
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-4">
                <i class="blueprinttwo-categories"> </i>  Program type
            </div>
            <div class="col-md-8 text-right font-w500">
                {{ $data['record']->type_2 }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-4">
                <i class="blueprinttwo-clipboard105"> </i>  Enroll
            </div>
            <div class="col-md-8 text-right font-w500">
                <a href="{{ $data['record']->getUrl('enrollment_material_url') }}" class="link-effect" target="_blank"> Click Here</a>
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-4">
                <i class="blueprinttwo-us"> </i>  State
            </div>
            <div class="col-md-8 text-right font-w500">
                {{ $data['record']->state_name }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-4">
                <i class="blueprinttwo-us"> </i>  Residency
            </div>
            <div class="col-md-8 text-right font-w500">
                {{ $data['record']->residency_details }}
            </div>
        </div>
    </div>
    <div class="col-lg-6" style="padding-left: 50px;">
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-4">
                Offering Materials
            </div>
            <div class="col-md-8 text-right font-w500">
                <a href="{{ $data['record']->getUrl('offering_material_url') }}" class="link-effect" target="_blank"> Click Here</a>
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-4">
                Enrollment Materials
            </div>
            <div class="col-md-8 text-right font-w500">
                <a href="{{ $data['record']->getUrl('enrollment_material_url') }}" class="link-effect" target="_blank"> Click Here</a>
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-4">
                Online Account Access
            </div>
            <div class="col-md-8 text-right font-w500">
                <a href="{{ $data['record']->getUrl('online_account_access_url') }}" class="link-effect" target="_blank"> Click Here</a>
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-4">
                General Performance
            </div>
            <div class="col-md-8 text-right font-w500">
                <a href="{{ $data['record']->getUrl('general_performance_url') }}" class="link-effect" target="_blank"> Click Here</a>
            </div>
        </div>
    </div>
</div>
<div class="kt-divider kt-margin-t-20 kt-margin-b-20">
    <span></span>
</div>
<div class="row">
    <div class="col-lg-12">
        <h2 class="text-primary font-s45 font-w400 text-uppercase kt-margin-b-30">
            Managers
        </h2>
    </div>
    <div class="col-lg-12">
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6 font-w500">
                Name
            </div>
            <div class="col-md-3 font-w500">
                Type
            </div>
            <div class="col-md-3 font-w500">
                Contract Termination Date
            </div>
        </div>
        @foreach($data['record']->management as $manager)
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-6">
                {{ $manager->manager_name }}
            </div>
            <div class="col-md-3">
                {{ $manager->type }}
            </div>
            <div class="col-md-3">
                {{ $manager->contract_termination_date }}
            </div>
        </div>
        @endforeach
    </div>
</div>