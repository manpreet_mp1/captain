<div class="row">
    <div class="col-lg-12 bg-dark text-white font-s18 kt-padding-5">
        Basic Information
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5 kt-margin-t-5">
    <div class="col-md-3 font-w500 text-uppercase">
        Plan Name
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planOne'])->name }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planTwo'])->name }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planThree'])->name }}
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5">
    <div class="col-md-3 font-w500 text-uppercase">
        State
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planOne'])->state_name }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planTwo'])->state_name }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planThree'])->state_name }}
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5">
    <div class="col-md-3 font-w500 text-uppercase">
        Type
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planOne'])->type_3_name }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planTwo'])->type_3_name }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planThree'])->type_3_name }}
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5">
    <div class="col-md-3 font-w500 text-uppercase">
        Program Type
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planOne'])->type_2 }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planTwo'])->type_2 }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planThree'])->type_2 }}
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5">
    <div class="col-md-3 font-w500 text-uppercase">
        Residency
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planOne'])->residency }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planTwo'])->residency }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planThree'])->residency }}
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5">
    <div class="col-md-3 font-w500 text-uppercase">
        Residency Requirement
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planOne'])->residency_details }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planTwo'])->residency_details }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planThree'])->residency_details }}
    </div>
</div>