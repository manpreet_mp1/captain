@if($data['record']->benefits->count() == 0)
    <div class="style-msg alertmsg">
        <div class="alert alert-warning fade show" role="alert">
            <div class="alert-icon"><i style="font-size: 1.2em;" class="fa fa-exclamation-triangle"></i></div>
            <div class="alert-text">Warning! No records Found.</div>
        </div>
    </div>
@else
    <div class="row">
        <div class="col-lg-12">
            <h2 class="text-primary font-s45 font-w400 text-uppercase kt-margin-b-30">
                Plan Benefit
            </h2>
        </div>
    </div>
    @foreach($data['record']->benefits as $benefit)
        <div class="kt-portlet">
            <div class="kt-portlet__head bg-primary text-white">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        {{ $benefit->benefit_heading }}
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-actions">
                        @if($benefit->additional_information)
                            <a href="{{ $benefit->getUrl('additional_information') }}"
                               class="btn btn-primary no-border-radius text-white"
                               target="_blank">
                                MORE..
                            </a>
                        @else
                            N/A
                        @endif
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="kt-section kt-margin-b-0">
                    <div class="kt-section__content">
                        {!! $benefit->benefit_details !!}
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif