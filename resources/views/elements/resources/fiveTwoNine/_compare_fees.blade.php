<div class="row kt-margin-t-20">
    <div class="col-lg-12 bg-dark text-white font-s18 kt-padding-5">
        Fees
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5 kt-margin-t-5">
    <div class="col-md-3 font-w500 text-uppercase">
        ENROLLMENT OR APPLICATION FEE
    </div>
    <div class="col-md-3">
        {{ optional(optional(optional($data['plans']['planOne'])->investmentOptions)->first())->enrollment_fee }}
    </div>
    <div class="col-md-3">
        {{ optional(optional(optional($data['plans']['planTwo'])->investmentOptions)->first())->enrollment_fee }}
    </div>
    <div class="col-md-3">
        {{ optional(optional(optional($data['plans']['planThree'])->investmentOptions)->first())->enrollment_fee }}
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5">
    <div class="col-md-3 font-w500 text-uppercase">
        ANNUAL ACCOUNT MAINTENANCE FEE
    </div>
    <div class="col-md-3">
        {{ optional(optional(optional($data['plans']['planOne'])->investmentOptionsFirstDetails)->first())->annual_account_maintenance_fee }}
    </div>
    <div class="col-md-3">
        {{ optional(optional(optional($data['plans']['planTwo'])->investmentOptionsFirstDetails)->first())->annual_account_maintenance_fee }}
    </div>
    <div class="col-md-3">
        {{ optional(optional(optional($data['plans']['planThree'])->investmentOptionsFirstDetails)->first())->annual_account_maintenance_fee }}
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5">
    <div class="col-md-3 font-w500 text-uppercase">
        PROGRAM MANAGER FEE
    </div>
    <div class="col-md-3">
        {{ optional(optional(optional($data['plans']['planOne'])->investmentOptionsFirstDetails)->first())->program_manager_fee }}
    </div>
    <div class="col-md-3">
        {{ optional(optional(optional($data['plans']['planTwo'])->investmentOptionsFirstDetails)->first())->program_manager_fee }}
    </div>
    <div class="col-md-3">
        {{ optional(optional(optional($data['plans']['planThree'])->investmentOptionsFirstDetails)->first())->program_manager_fee }}
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5">
    <div class="col-md-3 font-w500 text-uppercase">
        STATE FEE
    </div>
    <div class="col-md-3">
        {{ optional(optional($data['plans']['planOne'])->investmentOptionsFirstDetails)->min('state_fee') }} -
        {{ optional(optional($data['plans']['planOne'])->investmentOptionsFirstDetails)->max('state_fee') }}
    </div>
    <div class="col-md-3">
        {{ optional(optional($data['plans']['planTwo'])->investmentOptionsFirstDetails)->min('state_fee') }} -
        {{ optional(optional($data['plans']['planTwo'])->investmentOptionsFirstDetails)->max('state_fee') }}
    </div>
    <div class="col-md-3">
        {{ optional(optional($data['plans']['planThree'])->investmentOptionsFirstDetails)->min('state_fee') }} -
        {{ optional(optional($data['plans']['planThree'])->investmentOptionsFirstDetails)->max('state_fee') }}
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5">
    <div class="col-md-3 font-w500 text-uppercase">
        MISC FEES
    </div>
    <div class="col-md-3">
        {{ optional(optional($data['plans']['planOne'])->investmentOptionsFirstDetails)->min('misc_fees') }} -
        {{ optional(optional($data['plans']['planOne'])->investmentOptionsFirstDetails)->max('misc_fees') }}
    </div>
    <div class="col-md-3">
        {{ optional(optional($data['plans']['planTwo'])->investmentOptionsFirstDetails)->min('misc_fees') }} -
        {{ optional(optional($data['plans']['planTwo'])->investmentOptionsFirstDetails)->max('misc_fees') }}
    </div>
    <div class="col-md-3">
        {{ optional(optional($data['plans']['planThree'])->investmentOptionsFirstDetails)->min('misc_fees') }} -
        {{ optional(optional($data['plans']['planThree'])->investmentOptionsFirstDetails)->max('misc_fees') }}
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5">
    <div class="col-md-3 font-w500 text-uppercase">
        ANNUAL DISTRIBUTION FEE
    </div>
    <div class="col-md-3">
        {{ optional(optional($data['plans']['planOne'])->investmentOptionsFirstDetails)->min('annual_distribution_fee') }} -
        {{ optional(optional($data['plans']['planOne'])->investmentOptionsFirstDetails)->max('annual_distribution_fee') }}
    </div>
    <div class="col-md-3">
        {{ optional(optional($data['plans']['planTwo'])->investmentOptionsFirstDetails)->min('annual_distribution_fee') }} -
        {{ optional(optional($data['plans']['planTwo'])->investmentOptionsFirstDetails)->max('annual_distribution_fee') }}
    </div>
    <div class="col-md-3">
        {{ optional(optional($data['plans']['planThree'])->investmentOptionsFirstDetails)->min('annual_distribution_fee') }} -
        {{ optional(optional($data['plans']['planThree'])->investmentOptionsFirstDetails)->max('annual_distribution_fee') }}
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5">
    <div class="col-md-3 font-w500 text-uppercase">
        TOTAL ANNUAL ASSET-BASED FEES
    </div>
    <div class="col-md-3">
        {{ optional(optional($data['plans']['planOne'])->investmentOptionsFirstDetails)->min('total_annual_asset_based_fees') }} -
        {{ optional(optional($data['plans']['planOne'])->investmentOptionsFirstDetails)->max('total_annual_asset_based_fees') }}
    </div>
    <div class="col-md-3">
        {{ optional(optional($data['plans']['planTwo'])->investmentOptionsFirstDetails)->min('total_annual_asset_based_fees') }} -
        {{ optional(optional($data['plans']['planTwo'])->investmentOptionsFirstDetails)->max('total_annual_asset_based_fees') }}
    </div>
    <div class="col-md-3">
        {{ optional(optional($data['plans']['planThree'])->investmentOptionsFirstDetails)->min('total_annual_asset_based_fees') }} -
        {{ optional(optional($data['plans']['planThree'])->investmentOptionsFirstDetails)->max('total_annual_asset_based_fees') }}
    </div>
</div>