@section('pageTitle', '529 PLANS')
@section('subheader')
    <h3 class="kt-subheader__title">529 PLANS</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <span class="kt-subheader__desc">Common Questions</span>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet kt-faq-v1">
                <div class="kt-portlet__head bg-primary text-white">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            SAVING FOR COLLEGE WITH A 529 PLAN
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="accordion accordion-solid accordion-toggle-plus" id="section1">
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section1',
                            'headingDivId' => 'faq1_heading_q1',
                            'questionId' => 'faq1_q1',
                            'question' => 'What is a 529 college savings plan?',
                            'answer' =>
                                '<p>
                                  A Section 529 college savings plan is a tax-advantaged state-administered investment program that is authorized under Internal Revenue Code Section 529. These plans allow investors to save money in an account in which the earnings will grow free from federal income tax and, when used to pay for “qualified higher education expenses”, may be withdrawn federal income tax-free. In many states, a participant can receive special state incentives, including state tax treatment that mirrors the federal tax treatment, tax deductions/credits and/or other state tax benefits, based on participation in their state’s program(s).
                                </p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section1',
                            'headingDivId' => 'faq1_heading_q2',
                            'questionId' => 'faq1_q2',
                            'question' => 'What’s the difference between a 529 prepaid tuition program and a 529 savings program?',
                            'answer' =>
                                '<p><strong>Prepaid Tuition</strong>: Essentially, parents, grandparents, and other interested parties may purchase future tuition at a set price today. The program will then pay the future college tuition of the beneficiary at any of the state’s eligible colleges or universities (or comparable payment to private or out-of-state institutions). Amounts of tuition (years or units) may be purchased through a one-time lump sum purchase or monthly installment payments. The program pools the money and makes investments to enable the earnings to meet or exceed college tuition increases in that state.</p>
				<p><strong>Savings</strong>: Savings plans (also known as investment plans) enable participants to save money in a college savings account on behalf of a designated beneficiary. Amounts contributed and any earnings on the account may then be used to pay the beneficiary’s qualified higher education expenses. Contributions can vary, depending on individual savings goals. The plans offer various investment options that provide a variable rate of return usually based on stock or bond funds, although some plans offer investment options that guarantee a minimum rate of return.</p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section1',
                            'headingDivId' => 'faq1_heading_q3',
                            'questionId' => 'faq1_q3',
                            'question' => 'Which type of plan is better?',
                            'answer' =>
                                '<p>It depends upon the college plans, investment needs and goals of the family. Most states have created innovative college savings programs individually designed to reflect the unique needs of its citizens. The plans offer affordable, flexible, and tax-advantaged options that can ensure the door of opportunity is open for our children to access higher education. While prepaid tuition plans offer the opportunity to assure future tuition payments, savings plan assets can be used for tuition and other qualified expenses such as room and board. Some states offer their citizens both types of programs, giving families the option to choose the 529 plan that is right for them. It’s also important to consider that many families choose more than one investment option in order to diversify their college savings portfolios.</p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section1',
                            'headingDivId' => 'faq1_heading_q4',
                            'questionId' => 'faq1_q4',
                            'question' => 'Who can be a beneficiary?',
                            'answer' =>
                                '<p>Generally, anyone can be named the beneficiary of a 529 account regardless of their relationship to the person who establishes the account. You can even establish an account with yourself as the named beneficiary. The only requirement is that the beneficiary must be a US citizen or a resident alien, and must have a social security number or federal tax identification number. Be aware that maximum contribution per beneficiary varies between different 529 plans.</p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section1',
                            'headingDivId' => 'faq1_heading_q5',
                            'questionId' => 'faq1_q5',
                            'question' => 'Can a beneficiary have more than one account?',
                            'answer' =>
                                '<p>Yes. Since only one account owner can be named per account, family members may choose to open their own account for the same beneficiary. Be aware that a 529 plan’s impact on financial aid calculations can vary depending on the relationship of the account owner to the student beneficiary.</p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section1',
                            'headingDivId' => 'faq1_heading_q6',
                            'questionId' => 'faq1_q6',
                            'question' => 'Can anyone open a 529 account? What about grandparents?',
                            'answer' =>
                                '<p>A 529 account can be opened by anyone. Grandparents, other relatives or family friends can all be account owners, or simply choose to contribute to an existing account. In most states, a trust, corporation, non-profit or government entity can also open an account.</p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section1',
                            'headingDivId' => 'faq1_heading_q7',
                            'questionId' => 'faq1_q7',
                            'question' => 'Does my child have to go to an in-state school?',
                            'answer' =>
                                '<p>No. Funds can be used at any eligible educational institution in the country to pay for qualified higher education expenses. “Eligible educational institutions” are accredited post-secondary educational institutions offering credit toward a bachelor’s degree, an associate degree, a graduate level or professional degree, or another recognized post-secondary credential. Certain proprietary institutions and post-secondary vocational institutions and certain institutions located in foreign countries are also eligible educational institutions. To be an eligible educational institution, the institution must be eligible to participate in U.S. Department of Education student aid programs.</p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section1',
                            'headingDivId' => 'faq1_heading_q8',
                            'questionId' => 'faq1_q8',
                            'question' => 'What if my child doesnt go to college?',
                            'answer' =>
                                '<p>You have several options available if the beneficiary decides not to go to college:
					</p><ul>
						<li>Change the beneficiary to a member of the beneficiary’s family.</li>
						<li>Defer the use of your savings and leave contributions invested in the account.</li>
						<li>Withdraw the assets in your account for a “non-qualified” distribution (a distribution not used for qualified higher education expenses). Earnings (but not contribution amounts) would be subject to state and federal tax plus a 10% federal tax penalty on the earnings. Some plans may charge additional fees or penalties on non-qualified distributions.</li>
					</ul>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section1',
                            'headingDivId' => 'faq1_heading_q9',
                            'questionId' => 'faq1_q9',
                            'question' => 'What if my child is in high school? Is it too late to open an account?',
                            'answer' =>
                                '<p>It is never too late to save for higher education. You may open an account for an individual of any age, and the account may be used immediately. Note that some prepaid tuition plans may need a longer timeline to see a significant return on investment, so be sure to check with plan administrators.</p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section1',
                            'headingDivId' => 'faq1_heading_q10',
                            'questionId' => 'faq1_q10',
                            'question' => 'What if my beneficiary receives a scholarship?',
                            'answer' =>
                                '<p>You can use your funds to pay for expenses not covered by the scholarship, such as room and board, books and other required supplies. If you withdraw funds and do not use them for qualified expenses, the earnings portion of your withdrawal may be taxed at the scholarship recipient’s tax rate, but will not be subject to the 10% additional federal tax penalty.</p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section1',
                            'headingDivId' => 'faq1_heading_q11',
                            'questionId' => 'faq1_q11',
                            'question' => 'Who qualifies as a family member of the beneficiary?',
                            'answer' =>
                                '<p>
					A qualifying family member includes:
					</p><ul>
						<li> Natural or legally adopted children </li>
						<li> Parents or ancestors of parents </li>
						<li> Siblings or stepsiblings </li>
						<li> Stepchildren </li>
						<li> Stepparents </li>
						<li> First cousins </li>
						<li> Nieces or nephews </li>
						<li> Aunts or uncles </li>
					</ul>
					<p>In addition, the spouse of the beneficiary or the spouse of any of those listed above also qualifies as a family member of the beneficiary.</p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section1',
                            'headingDivId' => 'faq1_heading_q12',
                            'questionId' => 'faq1_q12',
                            'question' => 'Are there age or income limitations for participating in a 529 plan?',
                            'answer' =>
                                '<p>Anyone can participate in a 529 plan regardless of income of the account owner and in most states, regardless of the age of the beneficiary.</p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section1',
                            'headingDivId' => 'faq1_heading_q13',
                            'questionId' => 'faq1_q13',
                            'question' => 'How do I open a 529 plan?',
                            'answer' =>
                                '<p>To learn more about a particular 529 plan and open an account, you can contact the state which administers the program directly. Most states offer residents the opportunity to invest in the plan directly though the state. These plans are often called “Direct Sold” and are typically offered with relatively low fees and without sales commissions. For those looking for professional advice on how to invest in a 529, “Advisor Sold” programs are offered by many of the state plans. Advisor Sold programs offer professional investment advice and service with standard sales commissions applying.</p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section1',
                            'headingDivId' => 'faq1_heading_q14',
                            'questionId' => 'faq1_q14',
                            'question' => 'How can I change the beneficiary on an account?',
                            'answer' =>
                                '<p>Each 529 plan can provide the forms necessary for changing the beneficiary on an account. Contact your 529 plan to determine the specific requirements and forms necessary to complete this procedure. Depending on the relationship of the new and old beneficiaries, changing the beneficiary of an account may trigger a taxable event, which could also include a penalty, gift tax or both.</p>',
                        ])
                        @include('elements.widgets._faq', [
                           'parentDivId' => 'section1',
                           'headingDivId' => 'faq1_heading_q15',
                           'questionId' => 'faq1_q15',
                           'question' => 'Is investment in 529 plans recommended by financial advisors?',
                           'answer' =>
                               '<p>Many financial planners, tax accountants, and other financial advisors recommend 529 plans to their clients as a program that may fit their college planning needs. You may want to consult an advisor to see if 529 plans would be best for you.</p>',
                       ])
                        @include('elements.widgets._faq', [
                           'parentDivId' => 'section1',
                           'headingDivId' => 'faq1_heading_q16',
                           'questionId' => 'faq1_q16',
                           'question' => 'Are there restrictions regarding 529 plans and educational savings accounts?',
                           'answer' =>
                               '<p>Individuals can contribute to both 529 plans and Coverdell Education Savings Accounts. The Economic Growth and Tax Relief Reconciliation Act of 2001 permits contributions to the Coverdell Education Savings Account to cover K-12 education expenses on a tax favored basis. Individuals may benefit by funding a 529 plan for the child’s college expenses and utilizing the Coverdell Education Savings Account for elementary and secondary education expenses. Note that the annual contribution limit for Coverdell accounts is $2000 per beneficiary.</p>',
                       ])
                        @include('elements.widgets._faq', [
                          'parentDivId' => 'section1',
                          'headingDivId' => 'faq1_heading_q17',
                          'questionId' => 'faq1_q17',
                          'question' => 'Once an account is established, who controls the investments?',
                          'answer' =>
                              '<p>Many states contract with an investment manager to work with the state to develop investment portfolios and options that will help investors meet their college savings needs. Federal law prohibits the investor from having direct control over the selection of specific investments; therefore the state and the investment manager typically offer multiple savings options for the investor to choose from when they open an account. The account owner may change investment options subject to certain federal tax law limitations.</p>',
                      ])
                        @include('elements.widgets._faq', [
                          'parentDivId' => 'section1',
                          'headingDivId' => 'faq1_heading_q18',
                          'questionId' => 'faq1_q18',
                          'question' => 'Who can contribute to an account?',
                          'answer' =>
                              '<p>Generally, anyone can make a contribution to an account for any beneficiary. However, you should contact the 529 plan of your choice to determine any restrictions that may apply. You may find that you will only be eligible for specific state tax incentives by being recognized as the account owner.</p>',
                      ])
                        @include('elements.widgets._faq', [
                          'parentDivId' => 'section1',
                          'headingDivId' => 'faq1_heading_q19',
                          'questionId' => 'faq1_q19',
                          'question' => 'What are the most common investment options offered by Section 529 savings / investment plans?',
                          'answer' =>
                              '<p>The most common investment option is the age-based allocation strategy in which the age of the beneficiary determines the specific mix of investments. As the child ages, the investment mix is automatically reallocated and becomes more conservative as the beneficiary approaches college. There are many other options available, including 100% equity funds, fixed income funds, stable value funds, as well as a variety of equity and fixed income options within many plans. Some states offer guaranteed or principal protected options, as well as FDIC insured bank options.</p>',
                      ])
                        @include('elements.widgets._faq', [
                          'parentDivId' => 'section1',
                          'headingDivId' => 'faq1_heading_q20',
                          'questionId' => 'faq1_q20',
                          'question' => 'Can you change investment options once you have opened an account?',
                          'answer' =>
                              '<p>You may transfer all or any portion of the funds already invested in a particular investment option to another investment option once per calendar year or upon a change of the beneficiary of your Savings Trust Account to a family member of the beneficiary. However, each time a new contribution is made to an account, the investor can select a different investment option for the new contribution into the plan.</p>',
                      ])
                        @include('elements.widgets._faq', [
                          'parentDivId' => 'section1',
                          'headingDivId' => 'faq1_heading_q21',
                          'questionId' => 'faq1_q21',
                          'question' => 'Can the savings in a 529 account be rolled over to another 529 program?',
                          'answer' =>
                              '<p>Yes. The account owner can choose to move funds from one state’s 529 plan to another states’ plan one time within a 12-month period for the same beneficiary.</p>',
                      ])
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="kt-portlet kt-faq-v1">
                <div class="kt-portlet__head bg-primary text-white">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            529 PREPAID PLANS
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="accordion accordion-solid accordion-toggle-plus" id="section2">
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section2',
                            'headingDivId' => 'faq2_heading_q1',
                            'questionId' => 'faq2_q1',
                            'question' => 'Who controls a 529 prepaid tuition account?',
                            'answer' =>
                                '<p>
                                  The account purchaser maintains control over all of the money in the account and is the only one who can request account changes or refunds. Typically, a prepaid account has only one owner, check with the plan in your state for details. The student beneficiary does not have any control over the account, unless he or she is also the designated purchaser.
                                </p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section2',
                            'headingDivId' => 'faq2_heading_q2',
                            'questionId' => 'faq2_q2',
                            'question' => 'Does the account owner have to be related to the beneficiary?',
                            'answer' =>
                                '<p>No. In most states, you can open an account for your child, grandchild, niece or nephew, friend – even yourself. Review the program materials for naming and changing the designated student beneficiary.</p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section2',
                            'headingDivId' => 'faq2_heading_q3',
                            'questionId' => 'faq2_q3',
                            'question' => 'What are the eligibility requirements to participate in a prepaid tuition plan?',
                            'answer' =>
                                '<p>Typically, the beneficiary of a prepaid tuition account must be U.S. citizen or a legal resident. Additionally, either the account owner or the beneficiary must be a resident of the state that administers the plan at the time the application is signed.</p>
				<p>Most prepaid plans also require that the beneficiary be a participant in the plan for at least three years prior to using the account to pay for higher education.  Additionally, the beneficiary is typically required to be 15 years old or younger when the account is opened, thereby providing the plan with a minimal amount of time to begin investing the assets of the account in anticipation of the beneficiary using the plan to pay for college</p>
				<p>Prepaid tuition plans can vary; you should always check with the plan in your state to determine the specific requirements necessary to participate in the plan.</p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section2',
                            'headingDivId' => 'faq2_heading_q4',
                            'questionId' => 'faq2_q4',
                            'question' => 'Can more than one person make contributions to a prepaid tuition account?',
                            'answer' =>
                                '<p>Yes. Generally, anyone can contribute to an account. Prepaying tuition is an excellent gift idea for grandparents, other family members and friends. You should contact the program in your state to determine the specific process to follow to make additional contributions to the account.</p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section2',
                            'headingDivId' => 'faq2_heading_q5',
                            'questionId' => 'faq2_q5',
                            'question' => 'Can prepaid tuition plans only be used at in-state schools?',
                            'answer' =>
                                '<p>Prepaid tuition plan benefits are generally designed to be used at in-state public universities and community colleges; however, in some cases, they can also be used at private institutions and at out-of-state public and private colleges and universities. Be sure to check the plan details for the prepaid plan in your state.</p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section2',
                            'headingDivId' => 'faq2_heading_q6',
                            'questionId' => 'faq2_q6',
                            'question' => 'What happens to my prepaid tuition plan if my child receives a full or partial scholarship?',
                            'answer' =>
                                '<p>If the scholarship covers some or all of the student’s tuition and fees, the unused prepaid tuition benefits may be able to be used to cover other qualified higher education expenses. Alternatively, the remaining benefits can be transferred to another member of the family, held for possible future use, or a refund can be paid to the purchaser on a semester-by-semester basis.</p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section2',
                            'headingDivId' => 'faq2_heading_q7',
                            'questionId' => 'faq2_q7',
                            'question' => 'What if my child decides not to attend college?',
                            'answer' =>
                                '<p>Refund and transfer options are available. You should check with the program to determine who the benefits can be transferred to or how to receive a refund from the account.</p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section2',
                            'headingDivId' => 'faq2_heading_q8',
                            'questionId' => 'faq2_q8',
                            'question' => 'Does a 529 prepaid tuition account guarantee college admission or in-state tuition?',
                            'answer' =>
                                '<p>No. Having a prepaid tuition account does not affect your child’s chances of getting in to a particular college or your eligibility for in-state tuition rates.</p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section2',
                            'headingDivId' => 'faq2_heading_q9',
                            'questionId' => 'faq2_q9',
                            'question' => 'Can a prepaid tuition account be rolled over to another 529 program?',
                            'answer' =>
                                '<p>Generally rollovers are allowed. For instance, if the beneficiary of the account decides not to attend a post-secondary institution, the account owner can typically transfer funds in the account to another eligible beneficiary. To avoid penalty and income tax, the new beneficiary must be a member of the family of the preceding beneficiary. Additionally, you should check with the program you participate in to determine if there are other requirements that may apply.</p>',
                        ])
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="kt-portlet kt-faq-v1">
                <div class="kt-portlet__head bg-primary text-white">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            TAX ADVANTAGES OF 529 PLANS
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="accordion accordion-solid accordion-toggle-plus" id="section3">
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section3',
                            'headingDivId' => 'faq3_heading_q1',
                            'questionId' => 'faq3_q1',
                            'question' => 'What are the tax benefits of participating in a 529 plan?',
                            'answer' =>
                                '<p>Earnings in a 529 plan grow tax-deferred and are free of federal income tax when used for qualified higher education expenses under Internal Revenue Code Section 529 (26 U.S.C. 529). Qualified higher education expenses include tuition, mandatory fees, books, supplies, and equipment required for enrollment or attendance. Room and board expenses are also eligible for students enrolled half-time or more based on the current allowance for room and board determined by the eligible educational institution for federal financial aid purposes, or actual invoice amount charged by the institution to the beneficiary, if greater. In addition, qualified higher education expenses also include expenses of a special needs beneficiary that are necessary in connection with his or her enrollment or attendance at an eligible educational institution.</p>
				<p>Earnings on non-qualified withdrawals may be subject to federal income tax and a 10% federal penalty tax, as well as state and local income taxes.   Additionally, most states allow tax-deferred earnings and tax-free withdrawals for qualified higher education expenses, and some states allow families to deduct the full or a partial amount of their contribution from their state income taxes.</p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section3',
                            'headingDivId' => 'faq3_heading_q2',
                            'questionId' => 'faq3_q2',
                            'question' => 'Can I claim a federal income tax deduction based on my contributions into a 529 plan?',
                            'answer' =>
                                '<p>Unfortunately, there is not a federal income tax deduction for contributions into a 529 college savings plan.</p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section3',
                            'headingDivId' => 'faq3_heading_q3',
                            'questionId' => 'faq3_q3',
                            'question' => 'Can I claim a state income tax deduction based on my contributions into a 529 plan?',
                            'answer' =>
                                '<p>Many states offer residents a deduction or credit on personal income tax returns for contributions made to the in-state program and in some states for contributions to any 529 plan. As a general rule, you should contact the program in your state to determine the specific state tax rules that apply to investing in a 529 plan.</p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section3',
                            'headingDivId' => 'faq3_heading_q4',
                            'questionId' => 'faq3_q4',
                            'question' => 'Are withdrawals from a 529 plan exempt from federal income tax?',
                            'answer' =>
                                '<p>As long as the withdrawal is used to pay “qualified higher education expenses”, it is exempt from federal income tax.</p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section3',
                            'headingDivId' => 'faq3_heading_q5',
                            'questionId' => 'faq3_q5',
                            'question' => 'Are withdrawals from a 529 plan exempt from state income tax?',
                            'answer' =>
                                '<p>Most states do not tax withdrawals used to pay “qualified higher education expenses”. Several states do not have an income tax, therefore they do not tax distributions from 529 plans. As a general rule, you should contact the program in your state to determine the specific state tax rules that apply to investing in a 529 plan.</p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section3',
                            'headingDivId' => 'faq3_heading_q6',
                            'questionId' => 'faq3_q6',
                            'question' => 'What are “qualified higher education expenses?”',
                            'answer' =>
                                '<p>Qualified higher education expenses include tuition, mandatory fees, books, supplies, and equipment required for enrollment or attendance. Room and board expenses are also eligible for students enrolled half-time or more based on the current allowance for room and board determined by the eligible educational institution for federal financial aid purposes, or actual invoice amount charged by the institution to the beneficiary, if greater.</p>
				<p>In addition, qualified higher education expenses also include expenses of a special needs beneficiary that are necessary in connection with his or her enrollment or attendance at an eligible educational institution. However, qualified higher education expenses are reduced to the extent that such expenses are taken into account in claiming the Hope Scholarship Credit or Lifetime Learning credit.</p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section3',
                            'headingDivId' => 'faq3_heading_q7',
                            'questionId' => 'faq3_q7',
                            'question' => 'Are there other tax benefits with the program?',
                            'answer' =>
                                '<p>An individual may contribute up to $14,000 annually ($28,000 for married couples filing jointly) without paying gift taxes or filing a gift tax return (assuming no other gifts are made to the beneficiary in the same year). You also may accelerate up to five years’ worth of the annual exclusion amount and reduce the value of your estate by contributing up to $70,000 ($140,000 for married couples filing jointly) per beneficiary (this amount is subject to “add-back” in the event of the participant’s death within five years and also assumes no other gifts are made to the same beneficiary during the same period).</p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section3',
                            'headingDivId' => 'faq3_heading_q8',
                            'questionId' => 'faq3_q8',
                            'question' => 'If I contribute to a 529 plan, can I also claim a Hope Scholarship or Lifetime Learning Credit?',
                            'answer' =>
                                '<p>The beneficiary or the beneficiary’s parent may claim a Hope Scholarship Credit or Lifetime Learning Credit for qualified tuition and related expenses, provided other eligibility requirements are met, but cannot use the same expenses to justify a tax-free distribution from a qualified tuition program.</p>',
                        ])
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="kt-portlet kt-faq-v1">
                <div class="kt-portlet__head bg-primary text-white">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            SCHOOL ADMISSION FINANCIAL AID
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="accordion accordion-solid accordion-toggle-plus" id="section4">
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section4',
                            'headingDivId' => 'faq4_heading_q1',
                            'questionId' => 'faq4_q1',
                            'question' => 'Do 529 plans guarantee college admission for my child?',
                            'answer' =>
                                '<p>No. Your child will still be required to meet entry requirements as determined by individual colleges or universities.</p>',
                        ])
                        @include('elements.widgets._faq', [
                            'parentDivId' => 'section4',
                            'headingDivId' => 'faq4_heading_q2',
                            'questionId' => 'faq4_q2',
                            'question' => 'How will participating in a qualified tuition program affect federal financial aid eligibility?',
                            'answer' =>
                                '<p>When it comes to financial aid, ANY assets that you or the beneficiary own (not just 529 plan assets) can affect your eligibility for need-based financial aid. With 529 plans, your account is considered to be an asset of the account owner. Assuming the account owner is the parent, this means that, on average, about 5.6 percent of the value of the account is considered in determining the Expected Family Contributions (EFC). The EFC is the amount the family of the beneficiary is expected to pay toward that beneficiary’s higher education. With many other savings vehicles, such as a custodial accounts or assets that are in the name of the student, 20 percent of the value of the assets is considered in determining the EFC.</p>
				<p>Remember, the majority of need-based financial aid is in the form of student loans, so whatever savings you accumulate for college expenses may help reduce the parent’s or student’s future debt load.</p>',
                        ])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection