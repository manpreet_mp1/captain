@section('pageTitle', '529 Plans')
@section('subheader')
    <h3 class="kt-subheader__title">529 Plans</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <span class="kt-subheader__desc">{{ $data['record']->name }}</span>
@endsection
@section('subheader-toolbar')
    <a href="{{ route($routeName, $additionalParam) }}" class="btn btn-elevate btn-secondary btn-square">
        << Back
    </a>
@endsection
@section('content')
    <div class="row bg-success kt-padding-25">
        <div class="col-lg-4 text-center">
            <img src="{{ $data['record']->sshot_url}}"
                 alt="" style="max-width: 400px" class="img-responsive">
        </div>
        <div class="col-lg-6">
            <h1>
                {{ $data['record']->name }} - {{ $data['record']->state_name }} ({{ $data['record']->state_abbr }})
            </h1>
            <h3 class="font-w400">
                {{ $data['record']->type_3_name }}
            </h3>
            <h3 class="font-w400">
                {{ $data['record']->phone }}
            </h3>
            <h3 class="font-w400">
                <a href="{{ $data['record']->getUrl('website') }}" target="_blank">Visit Official Website</a>
            </h3>
        </div>
        <div class="col-lg-2 text-right">
            <a href="#" style="min-width: 150px;" class="btn btn-primary">Save Plan</a>
        </div>
    </div>
    <div class="row kt-portlet"
         style="-webkit-border-radius: 0;-moz-border-radius: 0;border-radius: 0;">
        <div class="col-lg-12 kt-portlet__body">
            <div class="kt-grid  kt-wizard-v2 kt-wizard-v2--white" id="kt_wizard_v2" data-ktwizard-state="step-first">
                <div class="kt-grid__item kt-wizard-v2__aside">
                    <!--begin: Form Wizard Nav -->
                    <div class="kt-wizard-v2__nav">
                        <div class="kt-wizard-v2__nav-items">
                            <a class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step"
                               data-ktwizard-state="current">
                                <div class="kt-wizard-v2__nav-body">
                                    <div class="kt-wizard-v2__nav-icon">
                                        <i class="blueprinttwo-settings6"></i>
                                    </div>
                                    <div class="kt-wizard-v2__nav-label">
                                        <div class="kt-wizard-v2__nav-label-title font-s22 kt-margin-t-5 text-uppercase">
                                            Basic Information
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
                                <div class="kt-wizard-v2__nav-body">
                                    <div class="kt-wizard-v2__nav-icon">
                                        <i class="blueprinttwo-agreement1"></i>
                                    </div>
                                    <div class="kt-wizard-v2__nav-label">
                                        <div class="kt-wizard-v2__nav-label-title font-s22 kt-margin-t-5 text-uppercase">
                                            Plan Benefits
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
                                <div class="kt-wizard-v2__nav-body">
                                    <div class="kt-wizard-v2__nav-icon">
                                        <i class="blueprintone-money215"></i>
                                    </div>
                                    <div class="kt-wizard-v2__nav-label">
                                        <div class="kt-wizard-v2__nav-label-title font-s22 kt-margin-t-5 text-uppercase">
                                            Contributions
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
                                <div class="kt-wizard-v2__nav-body">
                                    <div class="kt-wizard-v2__nav-icon">
                                        <i class="blueprinttwo-businessman53"></i>
                                    </div>
                                    <div class="kt-wizard-v2__nav-label">
                                        <div class="kt-wizard-v2__nav-label-title font-s22 kt-margin-t-5 text-uppercase">
                                            Fees & Expenses
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
                                <div class="kt-wizard-v2__nav-body">
                                    <div class="kt-wizard-v2__nav-icon">
                                        <i class="blueprinttwo-agenda8"></i>
                                    </div>
                                    <div class="kt-wizard-v2__nav-label">
                                        <div class="kt-wizard-v2__nav-label-title font-s22 kt-margin-t-5 text-uppercase">
                                            Contact Information
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <!--end: Form Wizard Nav -->
                </div>
                <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v2__wrapper">

                    <!--begin: Form Wizard Form-->
                    <div style="padding: 4rem 6rem 6rem;">

                        <!--begin: Form Wizard Step 1-->
                        <div class="kt-wizard-v2__content" data-ktwizard-type="step-content"
                             data-ktwizard-state="current">
                            @include('elements.resources.fiveTwoNine._basic')
                        </div>
                        <!--end: Form Wizard Step 1-->

                        <!--begin: Form Wizard Step 2-->
                        <div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
                            @include('elements.resources.fiveTwoNine._benefit')
                        </div>
                        <!--end: Form Wizard Step 2-->

                        <!--begin: Form Wizard Step 3-->
                        <div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
                            @include('elements.resources.fiveTwoNine._contribution')
                        </div>
                        <!--end: Form Wizard Step 3-->

                        <!--begin: Form Wizard Step 4-->
                        <div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
                            @include('elements.resources.fiveTwoNine._fees')
                        </div>
                        <!--end: Form Wizard Step 4-->

                        <!--begin: Form Wizard Step 5-->
                        <div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
                            @include('elements.resources.fiveTwoNine._contact')
                        </div>
                        <!--end: Form Wizard Step 5-->
                    </div>
                    <!--end: Form Wizard Form-->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function () {
            // Initialize form wizard
            wizard = new KTWizard('kt_wizard_v2', {
                startStep: 1,
            });

            // Change event
            wizard.on('change', function (wizard) {
                var position = $("#kt_wizard_v2").offset().top - 100;
                $("body, html").animate({
                    scrollTop: position
                }, 'slow');
            });
        });
    </script>
@endsection