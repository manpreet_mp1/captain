<div class="kt-portlet">
    <div class="kt-portlet__body">
        {!! Form::model($data['request'], ['method' => 'get' ]) !!}
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-uppercase kt-margin-b-10">
                    Enter Plan Name
                </h4>
                {!! Form::text('name', null, [
                    'class' => 'form-control',
                    'placeholder' => 'Plan Name'
                    ])
                !!}
            </div>
            <div class="col-lg-12 kt-margin-t-20">
                <h4 class="text-uppercase kt-margin-b-10">
                    SELECT STATE
                </h4>
                {!! Form::select('state_id', $data['view']['states'], null, [ 'class' => 'form-control']) !!}
            </div>
            <div class="col-lg-12 kt-margin-t-20">
                <h4 class="text-uppercase kt-margin-b-10">
                    SELECT TYPE
                </h4>
                {!! Form::select('type_3_key', $data['view']['types'], null, [ 'class' => 'form-control']) !!}
            </div>
            <div class="col-lg-6 kt-margin-t-20">
                {!! Form::button('<span class="fa fa-search"></span> Search', ['name' => 'search', 'class'=> 'btn btn-brand btn-elevate btn-block', 'type'=>'submit', 'value' => 'search' ]) !!}
            </div>
        </div>

        {!! Form::close() !!}
    </div>
</div>