<div class="kt-portlet">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                {{ $record->name }} - {{ $record->state_name }} ({{ $record->state_abbr }})
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="kt-section kt-margin-b-0">
            <div class="kt-section__content">
                <div class="row">
                    <div class="col-lg-5 text-center">
                        <img src="{{ $record->sshot_url }}"
                             alt="" style="max-width: 400px" class="img-responsive">
                    </div>
                    <div class="col-lg-7">
                        <h4 class="font-w400 text-uppercase kt-margin-b-10"> {{ $record->type_3_name }} </h4>
                        <h5 class="font-w300 text-uppercase kt-margin-b-10"> <i class="fa fa-phone-square"></i> {{ $record->phone }} </h5>
                        <h5 class="font-w300 kt-margin-b-10"> <a href="{{ $record->getUrl('website') }}" target="_blank"> <i class="fa fa-link"></i> Website  </a> </h5>
                        <h5 class="font-w300 "> <strong>Residency Requirement :</strong> {{ $record->residency_details }} </h5>
                        <hr class="kt-margin-b-10" />
                        <div class="row">
                            <div class="col-md-5">
                                <h5 class="font-w300 kt-margin-b-10"> Program Manager </h5>
                                @foreach ($record->management()->programManager()->get() as $pm)
                                    {{ $pm->manager_name }}
                                @endforeach
                            </div>
                            <hr class="kt-margin-t-10 kt-margin-b-10 visible-sm visible-xs" />
                            <div class="col-md-7 border-md-l">
                                <h5 class="font-w300 kt-margin-b-10"> Investment Manager(s) </h5>
                                <ul>
                                    @foreach ($record->management()->InvestmentManager()->get() as $im)
                                        <li> {{ $im->manager_name }} </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__foot">
        <div class="row align-items-center">
            <div class="col-lg-6 m--valign-middle">
                <a class="btn btn-brand btn-elevate"
                   href="{{ route($routeName, array_merge(['slug' => $record->slug], $additionalParam)) }}">
                    View Plan
                </a>
            </div>
        </div>
    </div>
</div>

