<div class="row kt-margin-t-20">
    <div class="col-lg-12 bg-dark text-white font-s18 kt-padding-5">
        Managers
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5 kt-margin-t-5">
    <div class="col-md-3 font-w500 text-uppercase">
        PROGRAM MANAGER
    </div>
    <div class="col-md-3">
        @if(optional(optional(optional($data['plans']['planOne'])->management())->programManager())->get())
            <ul class="kt-padding-l-0">
                @foreach (optional(optional(optional($data['plans']['planOne'])->management())->programManager())->get() as $manager)
                    <li> {{ $manager->manager_name }} </li>
                @endforeach
            </ul>
        @endif
    </div>
    <div class="col-md-3">
        @if(optional(optional(optional($data['plans']['planTwo'])->management())->programManager())->get())
            <ul class="kt-padding-l-0">
                @foreach (optional(optional(optional($data['plans']['planTwo'])->management())->programManager())->get() as $manager)
                    <li> {{ $manager->manager_name }} </li>
                @endforeach
            </ul>
        @endif
    </div>
    <div class="col-md-3">
        @if(optional(optional(optional($data['plans']['planThree'])->management())->programManager())->get())
            <ul class="kt-padding-l-0">
                @foreach (optional(optional(optional($data['plans']['planThree'])->management())->programManager())->get() as $manager)
                    <li> {{ $manager->manager_name }} </li>
                @endforeach
            </ul>
        @endif
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5 kt-margin-t-5">
    <div class="col-md-3 font-w500 text-uppercase">
        INVESTMENT MANAGER(S)
    </div>
    <div class="col-md-3">
        @if(optional(optional(optional($data['plans']['planOne'])->management())->investmentManager())->get())
            <ul class="kt-padding-l-0">
                @foreach (optional(optional(optional($data['plans']['planOne'])->management())->investmentManager())->get() as $manager)
                    <li> {{ $manager->manager_name }} </li>
                @endforeach
            </ul>
        @endif
    </div>
    <div class="col-md-3">
        @if(optional(optional(optional($data['plans']['planTwo'])->management())->investmentManager())->get())
            <ul class="kt-padding-l-0">
                @foreach (optional(optional(optional($data['plans']['planTwo'])->management())->investmentManager())->get() as $manager)
                    <li> {{ $manager->manager_name }} </li>
                @endforeach
            </ul>
        @endif
    </div>
    <div class="col-md-3">
        @if(optional(optional(optional($data['plans']['planThree'])->management())->investmentManager())->get())
            <ul class="kt-padding-l-0">
                @foreach (optional(optional(optional($data['plans']['planThree'])->management())->investmentManager())->get() as $manager)
                    <li> {{ $manager->manager_name }} </li>
                @endforeach
            </ul>
        @endif
    </div>
</div>