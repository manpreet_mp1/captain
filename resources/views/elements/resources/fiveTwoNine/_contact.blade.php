<div class="row">
    <div class="col-lg-12">
        <h2 class="text-primary font-s45 font-w400 text-uppercase kt-margin-b-30">
            Contact Information
        </h2>
    </div>
    <div class="col-lg-8">
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-8">
                Mailing Address
            </div>
            <div class="col-md-4 text-right font-w500">
                {{ $data['record']->address_line_1 }} {{ $data['record']->address_line_2 }} <br /> {{ $data['record']->city_state_zip }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-8">
                Toll-free
            </div>
            <div class="col-md-4 text-right font-w500">
                {{ $data['record']->toll_free }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-8">
                Fax
            </div>
            <div class="col-md-4 text-right font-w500">
                {{ $data['record']->fax }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-8">
                Email
            </div>
            <div class="col-md-4 text-right font-w500">
                {{ $data['record']->email }}
            </div>
        </div>
    </div>
</div>