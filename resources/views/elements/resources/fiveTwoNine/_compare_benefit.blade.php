<div class="row kt-margin-t-20">
    <div class="col-lg-12 bg-dark text-white font-s18 kt-padding-5">
        Benefits
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5 kt-margin-t-5">
    <div class="col-md-3 font-w500 text-uppercase">
        STATE TAX CREDIT OR DEDUCTION?
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planOne'])->offer_state_tax_credit_or_deduction }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planTwo'])->offer_state_tax_credit_or_deduction }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planThree'])->offer_state_tax_credit_or_deduction }}
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5">
    <div class="col-md-3 font-w500 text-uppercase">
        STATE TAX DEFERRED EARNINGS/ WITHDRAWALS
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planOne'])->offer_state_tax_deferred_earnings_withdrawals }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planTwo'])->offer_state_tax_deferred_earnings_withdrawals }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planThree'])->offer_state_tax_deferred_earnings_withdrawals }}
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5">
    <div class="col-md-3 font-w500 text-uppercase">
        FINANCIAL AID BENEFITS
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planOne'])->offer_financial_aid_benefits }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planTwo'])->offer_financial_aid_benefits }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planThree'])->offer_financial_aid_benefits }}
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5">
    <div class="col-md-3 font-w500 text-uppercase">
        STATE MATCHING GRANTS
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planOne'])->offer_state_matching_grants }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planTwo'])->offer_state_matching_grants }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planThree'])->offer_state_matching_grants }}
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5">
    <div class="col-md-3 font-w500 text-uppercase">
        REWARD PROGRAMS
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planOne'])->offer_reward_programs }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planTwo'])->offer_reward_programs }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planThree'])->offer_reward_programs }}
    </div>
</div>