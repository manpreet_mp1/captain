<div class="row kt-margin-t-20">
    <div class="col-lg-12 bg-dark text-white font-s18 kt-padding-5">
        Contact
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5 kt-margin-t-5">
    <div class="col-md-3 font-w500 text-uppercase">
        MAILING ADDRESS
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planOne'])->address_line_1 }} <br/>
        {{ optional($data['plans']['planOne'])->city_state_zip }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planTwo'])->address_line_1 }} <br/>
        {{ optional($data['plans']['planTwo'])->city_state_zip }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planThree'])->address_line_1 }} <br/>
        {{ optional($data['plans']['planThree'])->city_state_zip }}
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5">
    <div class="col-md-3 font-w500 text-uppercase">
        TOLL-FREE
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planOne'])->toll_free }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planTwo'])->toll_free }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planThree'])->toll_free }}
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5">
    <div class="col-md-3 font-w500 text-uppercase">
        FAX
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planOne'])->fax }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planTwo'])->fax }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planThree'])->fax }}
    </div>
</div>
<div class="border-bottom row kt-padding-b-5 kt-margin-b-5">
    <div class="col-md-3 font-w500 text-uppercase">
        EMAIL
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planOne'])->email }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planTwo'])->email }}
    </div>
    <div class="col-md-3">
        {{ optional($data['plans']['planThree'])->email }}
    </div>
</div>