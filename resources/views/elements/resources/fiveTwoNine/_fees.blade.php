<div class="row">
    <div class="col-lg-12">
        <h2 class="text-primary font-s45 font-w400 text-uppercase kt-margin-b-30">
            FEES AND EXPENSES
        </h2>
    </div>
    <div class="col-lg-8">
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-8">
                Enrollment or application fee
            </div>
            <div class="col-md-4 text-right font-w500">
                {{ optional($data['record']->investmentOptions->first())->enrollment_fee }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-8">
                Annual Account Maintenance Fee
            </div>
            <div class="col-md-4 text-right font-w500">
                {{ optional($data['record']->investmentOptionsFirstDetails->first())->annual_account_maintenance_fee }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-8">
                Program Manager Fee
            </div>
            <div class="col-md-4 text-right font-w500">
                {{ optional($data['record']->investmentOptionsFirstDetails->first())->program_manager_fee }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-8">
                State Fee
            </div>
            <div class="col-md-4 text-right font-w500">
                {{ $data['record']->investmentOptionsFirstDetails->min('state_fee') }} - {{ $data['record']->investmentOptionsFirstDetails->max('state_fee') }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-8">
                Misc Fees
            </div>
            <div class="col-md-4 text-right font-w500">
                {{ $data['record']->investmentOptionsFirstDetails->min('misc_fees') }} - {{ $data['record']->investmentOptionsFirstDetails->max('misc_fees') }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-8">
                Annual Distribution Fee
            </div>
            <div class="col-md-4 text-right font-w500">
                {{ $data['record']->investmentOptionsFirstDetails->min('annual_distribution_fee') }} - {{ $data['record']->investmentOptionsFirstDetails->max('annual_distribution_fee') }}
            </div>
        </div>
        <div class="border-bottom row font-s20 kt-padding-b-5 kt-margin-b-5">
            <div class="col-md-8">
                Total Annual Asset-Based Fees
            </div>
            <div class="col-md-4 text-right font-w500">
                {{ $data['record']->investmentOptionsFirstDetails->min('total_annual_asset_based_fees') }} - {{ $data['record']->investmentOptionsFirstDetails->max('total_annual_asset_based_fees') }}
            </div>
        </div>
    </div>
</div>