@section('pageTitle', '529 Plans')
@section('subheader')
    <h3 class="kt-subheader__title">529 PLANS</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <span class="kt-subheader__desc">Compare Plans</span>
@endsection
@section('content')
    {!! Form::model($data['request'], ['method' => 'get' ]) !!}
    <div class="row">
        <div class="col-xl-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head bg-primary text-white">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Select up to 3 Plans to compare
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-section kt-margin-b-0">
                        <div class="kt-section__content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            {!! Form::select('planOne', $data['plansDropdown'], null, [ 'class' => 'form-control']) !!}
                                        </div>
                                        <div class="col-lg-4">
                                            {!! Form::select('planTwo', $data['plansDropdown'], null, [ 'class' => 'form-control']) !!}
                                        </div>
                                        <div class="col-lg-4">
                                            {!! Form::select('planThree', $data['plansDropdown'], null, [ 'class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                    <div class="kt-divider kt-margin-t-20 kt-margin-b-20">
                                        <span></span>
                                        <span></span>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 offset-lg-5">
                                            {!! Form::button('Compare Plans', ['name' => 'search', 'class'=> 'btn btn-brand btn-elevate btn-block', 'type'=>'submit', 'value' => 'search' ]) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-divider kt-margin-t-20 kt-margin-b-20">
                            </div>
                            @if(Request::input('search'))
                                @include('elements.resources.fiveTwoNine._compare_basic')
                                @include('elements.resources.fiveTwoNine._compare_benefit')
                                @include('elements.resources.fiveTwoNine._compare_fees')
                                @include('elements.resources.fiveTwoNine._compare_contribution')
                                @include('elements.resources.fiveTwoNine._compare_managers')
                                @include('elements.resources.fiveTwoNine._compare_contact')
                                @include('elements.resources.fiveTwoNine._compare_links')
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection