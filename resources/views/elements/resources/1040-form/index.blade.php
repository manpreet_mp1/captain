@section('pageTitle', 'Form 1040')
@section('subheader')
<h3 class="kt-subheader__title">Form 1040</h3>
@endsection
@section('content')

<style>
    .accordion {
        background-color: #eff0f6;
        margin: 0; 
        padding: 0;
        color: #444;
        cursor: pointer;
        width: 100%;
        border: none;
        text-align: left;
        outline: none;
        font-size: 15px;
        transition: 0.4s;
    }

    .active, .accordion:hover {
        background-color: #eff0f6;
    }

    .accordion:after {
        color: #777;
        font-weight: bold;
        float: right;
        margin-left: 5px;
        font-size: 8px;
    }

    .active:after {
        font-size: 15px;
    }

    .accordion.accordion-solid .card .card-body{
        overflow-x: scroll;
    }

    /*    .panel {
            position: relative;
            padding: 0 18px;
            background-color: white;
            max-height: 0;
            overflow: hidden;
            transition: max-height 0.2s ease-out;
            margin: 0px 0px 0px 10px;
            overflow-x: scroll;
        }*/

    .common {
        width: 1000px;
        height: 600px;
        background-size: cover;
        background-repeat: no-repeat;
        display: block;
        margin-left: auto;
        margin-right: auto;
    }

    .common2 {
        width: 1000px;
        height: 340px;
        background-size: contain;
        background-repeat: no-repeat;
        display: block;
        margin-left: auto;
        margin-right: auto;
    }

    .common1img {
        width: 1000px;
        height: 641px;
        background-size: contain;
        background-repeat: no-repeat;
        display: block;
        margin-left: auto;
        margin-right: auto;
    }

    .common2img {
        width: 1000px;
        height: 847px;
        background-size: contain;
        background-repeat: no-repeat;
        display: block;
        margin-left: auto;
        margin-right: auto;
    }

    .img_one {
        height: 694px;
        background-image: url('../../../../1040-form/1040-1.png');
    }

    .img_two {
        position: relative;
        background-image: url('../../../../1040-form/1040-2.png');
    }

    .img_three {
        height: 847px;
        position: relative;
        background-image: url('../../../../1040-form/Schedule_1.png');
    }

    .img_four {
        position: relative;
        height: 273px;
        background-image: url('../../../../1040-form/Schedule_2.png');
    }

    .img_five {
        height: 339px;
        position: relative;
        background-image: url('../../../../1040-form/Schedule_3.png');
    }

    .img_six {
        height: 473px;
        position: relative;
        background-image: url('../../../../1040-form/Schedule_4.png');
    }

    .btnCommon {
        position: absolute;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        font-size: 12px !important;
        padding: 7px 12px 10px 10px !important;
        margin: 28px 0px 0px 1px;
        font-weight: bold;
    }
    .btnCommonf4 {
        position: absolute;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        font-size: 12px !important;
        padding: 7px 4px 10px 7px !important;
        margin: 28px 0px 0px 1px;
        font-weight: bold;
    }

    .btn1 {
        top: 10px;
        left: 875px;
    }
    .btn2 {
        top: 31px;
        left: 875px;
    }
    .btn3 {
        top: 52px;
        left: 875px;
    }
    .btn4 {
        top: 74px;
        left: 875px;
    }
    .btn5 {
        top: 95px;
        left: 875px;
    }
    .btn6 {
        top: 170px;
        left: 875px;
    }
    .btn7 {
        top: 31px;
        left: 454px;
    }
    .btn8 {
        top: 52px;
        left: 454px;
    }
    .btn9 {
        top: 74px;
        left: 454px;
    }
    .btn10 {
        top: 95px;
        left: 454px;
    }
    .btn11 {
        top: 143px;
        left: 889px;
    }
    .btn12 {
        top: 165px;
        left: 889px;
    }
    .btn13 {
        top: 187px;
        left: 889px;
    }
    .btn14 {
        top: 209px;
        left: 889px;
    }
    .btn15 {
        top: 231px;
        left: 889px;
    }
    .btn16 {
        top: 297px;
        left: 889px;
    }
    .btn17 {
        top: 319px;
        left: 889px;
    }
    .btn18 {
        top: 341px;
        left: 889px;
    }
    .btn19 {
        top: 450px;
        left: 703px;
    }
    .btn20 {
        top: 516px;
        left: 703px;
    }
    .btn21 {
        top: 561px;
        left: 703px;
    }
    .btn21a {
        top: 604px;
        left: 703px;
    }
    .btn21b {
        top: 670px;
        left: 703px;
    }
    .btn21c {
        top: 692px;
        left: 703px;
    }
    .btn21d {
        top: 714px;
        left: 703px;
    }
    .btn22 {
        top: 143px;
        left: 901px;
    }
    .btn23 {
        top: 165px;
        left: 912px;
    }
    .btn24 {
        top: 187px;
        left: 912px;
    }
    .btn25 {
        top: 231px;
        left: 912px;
    }
    .btn26 {
        top: 320px;
        left: 884px;
    }
</style>

<div class="row">
    <div class="col-xl-12">

        <div class="accordion accordion-solid accordion-toggle-svg" id="accordionExample8">
            <div class="card">
                <div class="card-header" id="headingOne8">
                    <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseOne8" aria-expanded="false" aria-controls="collapseOne8">
                        Form 1040 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <polygon id="Shape" points="0 0 24 0 24 24 0 24"></polygon>
                                <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero"></path>
                                <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
                            </g>
                        </svg>					
                    </div>
                </div>
                <div id="collapseOne8" class="collapse" aria-labelledby="headingOne8" data-parent="#accordionExample8" style="">
                    <div class="card-body">
                        <div class="common img_one">
                        </div>
                        <div class="common1img img_two">
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommon btn1">View Strategies</a>
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommon btn2">View Strategies</a>
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommon btn3">View Strategies</a>
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommon btn4">View Strategies</a>
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommon btn5">View Strategies</a>
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommon btn6">View Strategies</a>
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommon btn7">View Strategies</a>
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommon btn8">View Strategies</a>
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommon btn9">View Strategies</a>
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommon btn10">View Strategies</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingTwo8">
                    <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseTwo8" aria-expanded="false" aria-controls="collapseTwo8">
                        Schedule 1 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <polygon id="Shape" points="0 0 24 0 24 24 0 24"></polygon>
                                <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero"></path>
                                <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
                            </g>
                        </svg>					
                    </div>
                </div>
                <div id="collapseTwo8" class="collapse" aria-labelledby="headingTwo8" data-parent="#accordionExample8">
                    <div class="card-body">
                        <div class="common2img img_three">
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommon btn11">View Strategies</a>
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommon btn12">View Strategies</a>
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommon btn13">View Strategies</a>
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommon btn14">View Strategies</a>
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommon btn15">View Strategies</a>
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommon btn16">View Strategies</a>
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommon btn17">View Strategies</a>
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommon btn18">View Strategies</a>
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommon btn19">View Strategies</a>
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommon btn20">View Strategies</a>
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommon btn21">View Strategies</a>
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommon btn21a">View Strategies</a>
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommon btn21b">View Strategies</a>
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommon btn21c">View Strategies</a>
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommon btn21d">View Strategies</a>
                        </div>					
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingThree8">
                    <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseThree8" aria-expanded="false" aria-controls="collapseThree8">
                        Schedule 2 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <polygon id="Shape" points="0 0 24 0 24 24 0 24"></polygon>
                                <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero"></path>
                                <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
                            </g>
                        </svg>					
                    </div>
                </div>
                <div id="collapseThree8" class="collapse" aria-labelledby="headingThree8" data-parent="#accordionExample8">
                    <div class="card-body">
                        <div class="common2 img_four">
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommon btn22">View Strategies</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingFour8">
                    <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseFour8" aria-expanded="false" aria-controls="collapseThree8">
                        Schedule 3 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <polygon id="Shape" points="0 0 24 0 24 24 0 24"></polygon>
                                <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero"></path>
                                <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
                            </g>
                        </svg>					
                    </div>
                </div>
                <div id="collapseFour8" class="collapse" aria-labelledby="headingFour8" data-parent="#accordionExample8">
                    <div class="card-body">
                        <div class="common2 img_five">
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommonf4 btn23">View Strategies</a>
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommonf4 btn24">View Strategies</a>
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommonf4 btn25">View Strategies</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingFive8">
                    <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseFive8" aria-expanded="false" aria-controls="collapseThree8">
                        Schedule 4 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <polygon id="Shape" points="0 0 24 0 24 24 0 24"></polygon>
                                <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero"></path>
                                <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
                            </g>
                        </svg>					
                    </div>
                </div>
                <div id="collapseFive8" class="collapse" aria-labelledby="headingFive8" data-parent="#accordionExample8">
                    <div class="card-body">
                        <div class="common2 img_six">
                            <a href="" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20 btnCommonf4 btn26">View Strategies</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection