<div class="kt-portlet">
    <div class="kt-portlet__body">
        {!! Form::model($data['request'], ['method' => 'get' ]) !!}
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-uppercase kt-margin-b-10">
                    ENTER COLLEGE NAME
                </h4>
                {!! Form::text('collegeName', null, [
                    'class' => 'form-control',
                    'placeholder' => 'College Name'
                    ])
                !!}
            </div>
            <div class="col-lg-12 kt-margin-t-20">
                <h4 class="text-uppercase kt-margin-b-10">
                    SELECT STATE
                </h4>
                {!! Form::select('state', $data['stateList'], null, [ 'class' => 'form-control']) !!}
            </div>
            <div class="col-lg-12 kt-margin-t-20">
                <h4 class="text-uppercase kt-margin-b-10">
                    SELECT TYPE
                </h4>
                {!! Form::select('type', $data['typeList'], null, [ 'class' => 'form-control']) !!}
            </div>
            <div class="col-lg-6 kt-margin-t-20">
                <button class="btn btn-brand btn-elevate btn-block" title="Search">
                    <span class="fa fa-search"></span> Search
                </button>
            </div>
        </div>

        {!! Form::close() !!}
    </div>
</div>