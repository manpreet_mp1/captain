<div class="kt-portlet {{ $data['family']->is_active_class }}">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                {{ $record->name }}
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-actions">
                @if($record->scholarship_url)
                    <a href="{{ $record->scholarship_url }}"
                       class="btn btn-primary no-border-radius text-white"
                       target="_blank">
                        APPLY NOW
                    </a>
                @else
                    N/A
                @endif
            </div>
        </div>
    </div>
    @include('elements.parent.widgets._inactive_overlay')
    <div class="kt-portlet__body">
        <div class="kt-section kt-margin-b-0">
            <div class="kt-section__content">
                <div class="row">
                    <div class="col-lg-3">
                        <p class="font-w400 text-uppercase text-muted kt-margin-b-0">Type</p>
                        <h2 class="font-w300">{{ $record->type }}</h2>
                    </div>
                    <div class="col-lg-3">
                        <p class="font-w400 text-uppercase text-muted kt-margin-b-0">Amount</p>
                        <h2 class="font-w300">{{ $record->amount }}</h2>
                    </div>
                    <div class="col-lg-3">
                        <p class="font-w400 text-uppercase text-muted kt-margin-b-0">Required GPA</p>
                        <h2 class="font-w300">{{ $record->required_gpa }}</h2>
                    </div>
                    <div class="col-lg-3 col_last">
                        <p class="font-w400 text-uppercase text-muted kt-margin-b-0">Required SAT</p>
                        <h2 class="font-w300">{{ $record->required_sat }}</h2>
                    </div>
                    <div class="col-lg-3">
                        <p class="font-w400 text-uppercase text-muted kt-margin-b-0">Required Act</p>
                        <h2 class="font-w300">{{ $record->required_act }}</h2>
                    </div>
                    <div class="col-lg-3">
                        <p class="font-w400 text-uppercase text-muted kt-margin-b-0">Mid 50 ACT</p>
                        <h2 class="font-w300">{{ $record->mid_50_act }}</h2>
                    </div>
                    <div class="col-lg-6">
                        <p class="font-w400 text-uppercase text-muted kt-margin-b-0">Mid 50 SAT</p>
                        <h2 class="font-w300">{!! nl2br(e($record->mid_50_sat)) !!}</h2>
                    </div>
                </div>
                <div class="kt-divider kt-margin-t-20 kt-margin-b-20">
                    <span></span>
                    <span><i class="fa fa-university"></i></span>
                    <span></span>
                </div>
                <p class="font-w400 text-uppercase text-muted">Description</p>
                <p class="lead font-w300">{!! nl2br(e($record->scholarship_description)) !!}</p>
            </div>
        </div>
    </div>
</div>