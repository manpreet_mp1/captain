@section('pageTitle', 'Merit Scholarships')
@section('subheader')
    <h3 class="kt-subheader__title">Merit Scholarships</h3>
@endsection
@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__body text-center">
                    <h2>FIND MERIT SCHOLARSHIPS TO HELP PAY FOR COLLEGE</h2>
                    <span class="">Below is our Merit Scholarship Directory. You can filter by college, by state, and by type of scholarship.</span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-3">
            @include('elements.resources.scholarship.merit._search')
        </div>
        <div class="col-xl-9">
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <div class="kt-section">
                        <div class="kt-section__content">
                            @if($data['records']->count() == 0)
                                <div class="alert alert-warning fade show" role="alert">
                                    <div class="alert-icon"><i style="font-size: 1.2em;" class="fa fa-exclamation-triangle"></i></div>
                                    <div class="alert-text">Warning! No School Found.</div>
                                </div>
                            @else
                                <table class="table table-bordered" style="border: none">
                                    @foreach($data['records']->chunk(3) AS $chunk)
                                        <tr class="row">
                                            @foreach($chunk AS $school)
                                                <td class="col-md-4">
                                                    <a href="{{ route($routeName, array_merge(['ipeds' => $school->ipeds,], $additionalParam)) }}">
                                                        {{ $school->school_name }}
                                                        (<strong>{{$school->scholarship_count}}</strong>)
                                                    </a>
                                                </td>
                                            @endforeach
                                        </tr>
                                    @endforeach
                                </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection