<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                {{ $data['records']->first()->school_name }}, {{ $data['records']->first()->state_abbr }}
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="kt-section kt-margin-b-0">
            <div class="kt-section__content">
                <div class="row">
                    <div class="col-md-2 text-center">
                        <img src="{{ $data['records']->first()->college_logo_url }}"
                             alt="{{ $data['records']->first()->school_name }}"
                             style=""
                             class="img-responsive">
                    </div>
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-4">
                                <p class="font-w400 text-uppercase text-muted kt-margin-b-0">School Type</p>
                                <h3 class="font-w300">{{ $data['records']->first()->school_type }}</h3>
                            </div>
                            <div class="col-md-4">
                                <p class="font-w400 text-uppercase text-muted kt-margin-b-0">Acceptance Rate</p>
                                <h3 class="font-w300">{{ $data['records']->first()->acceptance_rate }}</h3>
                            </div>
                            <div class="col-md-4">
                                <p class="font-w400 text-uppercase text-muted kt-margin-b-0">Tuition</p>
                                <h3 class="font-w300">{!! nl2br(e($data['records']->first()->tuition)) !!}</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <p class="font-w400 text-uppercase text-muted kt-margin-b-0">Eligibility For Public Schools</p>
                                <h3 class="font-w300">{{ $data['records']->first()->eligibility_for_public_schools ? $data['records']->first()->eligibility_for_public_schools : "N/A" }}</h3>
                            </div>
                            <div class="col-md-4">
                                <p class="font-w400 text-uppercase text-muted kt-margin-b-0">International Students</p>
                                <h3 class="font-w300">{{ $data['records']->first()->international_students ? $data['records']->first()->international_students : "N/A" }}</h3>
                            </div>
                            <div class="col-md-4">
                                <p class="font-w400 text-uppercase text-muted kt-margin-b-0">US News Ranking</p>
                                <h3 class="font-w300">{{ $data['records']->first()->us_news_ranking }}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>