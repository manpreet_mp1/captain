@section('pageTitle', 'Merit Scholarship')
@section('subheader')
    <h3 class="kt-subheader__title">Merit Scholarship</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <span class="kt-subheader__desc">{{ $data['records']->first()->school_name }}</span>
@endsection
@section('subheader-toolbar')
    <a href="{{ route($routeName, $additionalParam) }}" class="btn btn-elevate btn-secondary btn-square">
        << Back
    </a>
@endsection
@section('content')
    @if($data['records']->count() == 0)
        <div class="style-msg alertmsg">
            <div class="alert alert-warning fade show" role="alert">
                <div class="alert-icon"><i style="font-size: 1.2em;" class="fa fa-exclamation-triangle"></i></div>
                <div class="alert-text">Warning! No Scholarship Found.</div>
            </div>
        </div>
    @else
        <div class="row">
            <div class="col-lg-12">
                @include('elements.resources.scholarship.merit._school_header')
            </div>
        </div>
        <h3 class="font-w300 text-uppercase kt-margin-b-30">
            Scholarships
        </h3>
        @foreach($data['records'] AS $record)
            @include('elements.resources.scholarship.merit._item')
        @endforeach
    @endif
@endsection