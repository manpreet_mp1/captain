@if($errors->default->first($field))
    {!! $errors->default->first($field, '<div class="alert alert-danger col-md-12">:message</div>') !!}
@endif