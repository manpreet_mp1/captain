<div class="kt-portlet kt-portlet--height-fluid">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                {{ isset($title) ? $title : "Billing Information" }}
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        {!! Form::open(['method' => 'POST', 'id' => "payment-form", 'url' => route($route, $additionalParam)]) !!}
        <script src="https://js.stripe.com/v3/"></script>
        <div class="row">
            <div class="col-md-9">
                <div class="form-group m-form__group">
                    <label for="cardholder-name">Name on card
                        <span class="text-danger">*</span>
                    </label>
                    {!! Form::text('cardholder-name', null, [
                        'class'=>'form-control m-input m-input--square border-primary m-input--air',
                        'id'=>'cardholder-name',
                        'placeholder'=> 'John Doe',
                         'required'
                    ]) !!}
                    @foreach($hiddenFields as $hiddenField)
                        {!! Form::hidden($hiddenField['name'], $hiddenField['value']) !!}
                    @endforeach
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group m-form__group">
                    <label for="address-zip">Billing ZIP code
                        <span class="text-danger">*</span>
                    </label>
                    {!! Form::number('address-zip', null, [
                        'class'=>'form-control m-input m-input--square border-primary m-input--air',
                        'id'=>'address-zip',
                        'placeholder'=> '92629',
                        'required'
                    ]) !!}
                </div>
            </div>
            <div class="col-md-12">
                <div id="card-element"><!-- A Stripe Element will be inserted here. --></div>
                <!-- Used to display form errors. -->
                <div id="card-errors" class="alert alert-danger" role="alert" style="padding: 10px 20px;margin-top: 20px;margin-bottom: 0px;"></div>
            </div>
        </div>
        <div class="" id="paymentLoading" style="display: none">
            @include('elements.snippet._loading', ['leftPercent' => 45])
        </div>
        <div class="kt-margin-t-20">
            <img src="https://stripe.com/img/about/logos/badge/outline-dark.svg" alt="">
        </div>
    </div>
    <div class="kt-portlet__foot">
        <button type="submit" class="btn btn-brand" id="m--btn-modal-card-submit">
            {{ isset($buttonName) ? $buttonName : "Continue" }}
        </button>
    </div>
    {!! Form::close() !!}
</div>

<script>
    document.getElementById('card-errors').style.display = 'none';
    // Create a Stripe client.
    var stripe = Stripe('{{ config('services.stripe.key') }}');
    // Create an instance of Elements.
    var elements = stripe.elements();
    // Custom styling can be passed to options when creating an Element.
    // (Note that this demo uses a wider set of styles than the guide below.)
    var style = {
        base: {
            iconColor: '#F99A52',
            color: '#32315E',
            lineHeight: '18px',
            fontSmoothing: 'antialiased',
            fontSize: '16px',
            '::placeholder': {
                color: '#CFD7DF',
            }
        },
        invalid: {
            color: '#fa755a',
            iconColor: '#fa755a'
        }
    };
    // Create an instance of the card Element.
    var card = elements.create('card', {hidePostalCode: true}, {style: style});
    // Add an instance of the card Element into the `card-element` <div>.
    card.mount('#card-element');
    // Handle real-time validation errors from the card Element.
    card.addEventListener('change', function (event) {
        var displayError = document.getElementById('card-errors');
        if (event.error) {
            document.getElementById('card-errors').style.display = 'block';
            displayError.textContent = event.error.message;
        } else {
            document.getElementById('card-errors').style.display = 'none';
            displayError.textContent = '';
        }
    });
    // Handle form submission.
    var form = document.getElementById('payment-form');
    form.addEventListener('submit', function (event) {
        event.preventDefault();
        document.getElementById('paymentLoading').style.display = 'block';
        document.getElementById('m--btn-modal-card-submit').disabled = 'true';
        var extraDetails = {
            name: form.querySelector('input[name=cardholder-name]').value,
            address_zip: form.querySelector('input[name=address-zip]').value
        };
        stripe.createToken(card, extraDetails).then(function (result) {
            if (result.error) {
                // Inform the user if there was an error.
                document.getElementById('paymentLoading').style.display = 'none';
                var errorElement = document.getElementById('card-errors');
                errorElement.textContent = result.error.message;
                document.getElementById('m--btn-modal-card-submit').disabled = 'false';
            } else {
                // Lets post the message to the same url
                stripeTokenHandler(result.token);
            }
        });
    });

    // Submit the form
    function stripeTokenHandler(token) {
        // Insert the token ID into the form so it gets submitted to the server
        var form = document.getElementById('payment-form');
        var hiddenInput = document.createElement('input');
        hiddenInput.setAttribute('type', 'hidden');
        hiddenInput.setAttribute('name', 'stripe_token');
        hiddenInput.setAttribute('value', token.id);
        form.appendChild(hiddenInput);

        // Submit the form
        form.submit();
    }
</script>
