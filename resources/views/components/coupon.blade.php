<div class="kt-portlet kt-portlet--height-fluid">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Coupon
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        {!! Form::open(['method' => 'POST', 'id' => "payment-form", 'url' => route($route, $additionalParam)]) !!}
        <div class="row">
            <div class="col-md-9">
                <div class="form-group m-form__group">
                    {!! Form::text('coupon_code', $couponCode, [
                        'class'=>'form-control m-input m-input--square border-primary m-input--air',
                        'id'=>'coupon_code',
                        'placeholder'=> 'Enter discount code',
                         'required'
                    ]) !!}
                    @foreach($hiddenFields as $hiddenField)
                        {!! Form::hidden($hiddenField['name'], $hiddenField['value']) !!}
                    @endforeach
                </div>
            </div>
            <div class="col-md-3">
                <button type="submit" class="btn btn-brand btn-block" id="m--btn-modal-card-submit">
                    {{ isset($buttonName) ? $buttonName : "Apply" }}
                </button>
            </div>
            <div class="col-md-12 font-s20">
                <div class="kt-widget4">
                    <div class="kt-widget4__item">
                        <div class="kt-widget4__info">
                            <span class="kt-widget4__username font-s20 text-">PRICE</span>
                        </div>
                        <span class="">
                            ${{ number_format($price/100, 2) }}@if(isset($months) && $months)/month for {{ $months }} months @endif
                        </span>
                    </div>
                    <div class="kt-widget4__item">
                        <div class="kt-widget4__info">
                            <span class="kt-widget4__username font-s20 text-uppercase">Discount</span>
                        </div>
                        <span class="">
                            ${{ number_format($discount/100, 2) }}
                        </span>
                    </div>
                    <div class="kt-widget4__item">
                        <div class="kt-widget4__info">
                            <span class="kt-widget4__username font-s20 text-uppercase">Due Today</span>
                        </div>
                        <span class="">
                            ${{ number_format($dueToday/100, 2) }}
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
