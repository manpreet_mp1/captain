<div id="{{ $id }}" class="custom-modal-overlay">
    <a href="#" class="cancel"></a>
    <div class="custom-modal" style="max-width:{{ isset($maxWidth) ? $maxWidth : "90%" }}">
        {{ $slot }}
        <a href="#" class="close">&times;</a>
    </div>
</div>
