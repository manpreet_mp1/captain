<div class="kt-header__topbar-item kt-header__topbar-item--user">
    <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
        <div class="kt-header__topbar-user">
            <span class="kt-header__topbar-welcome kt-hidden-mobile">Hi,</span>
            <span class="kt-header__topbar-username kt-hidden-mobile">{{ $ST_USER->first_name }}</span>
            <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
            <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold">{{ $ST_USER->first_name_initial }}</span>
        </div>
    </div>
    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">
        <!--begin: Head -->
        <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x"
             style="background-image: url({{ config('app.cdn') }}images/bg-1.jpg)">
            <div class="kt-user-card__avatar">
                <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold">{{ $ST_USER->first_name_initial }}</span>
            </div>
            <div class="kt-user-card__name">
                {{ $ST_USER->full_name }}
            </div>
        </div>
        <!--end: Head -->
        <!--begin: Navigation -->
        <!--begin: Navigation -->
        <div class="kt-notification">
            <a href="javascript:;" onclick="noAccess()" class="kt-notification__item">
                <div class="kt-notification__item-icon">
                    <i class="flaticon-resume-and-cv kt-font-success"></i>
                </div>
                <div class="kt-notification__item-details">
                    <div class="kt-notification__item-title kt-font-bold">
                        My Profile
                    </div>
                    <div class="kt-notification__item-time">
                        Account settings and more
                    </div>
                </div>
            </a>
            <a href="javascript:;" onclick="noAccess()" class="kt-notification__item">
                <div class="kt-notification__item-icon">
                    <i class="flaticon-price kt-font-success"></i>
                </div>
                <div class="kt-notification__item-details">
                    <div class="kt-notification__item-title kt-font-bold">
                        My Subscription
                    </div>
                    <div class="kt-notification__item-time">
                        Upgrade or downgrade your plan
                    </div>
                </div>
            </a>
            <a href="javascript:;" onclick="noAccess()" class="kt-notification__item">
                <div class="kt-notification__item-icon">
                    <i class="flaticon-pay kt-font-success"></i>
                </div>
                <div class="kt-notification__item-details">
                    <div class="kt-notification__item-title kt-font-bold">
                        Update Billing
                    </div>
                    <div class="kt-notification__item-time">
                        Add or edit your payment info
                    </div>
                </div>
            </a>
            <a href="javascript:;" onclick="noAccess()" class="kt-notification__item">
                <div class="kt-notification__item-icon">
                    <i class="flaticon-mail kt-font-success"></i>
                </div>
                <div class="kt-notification__item-details">
                    <div class="kt-notification__item-title kt-font-bold">
                        My Messages
                    </div>
                    <div class="kt-notification__item-time">
                        See new messages and updates
                    </div>
                </div>
            </a>
            <a href="javascript:;" onclick="noAccess()" class="kt-notification__item">
                <div class="kt-notification__item-icon">
                    <i class="flaticon-task kt-font-success"></i>
                </div>
                <div class="kt-notification__item-details">
                    <div class="kt-notification__item-title kt-font-bold">
                        My Tasks
                    </div>
                    <div class="kt-notification__item-time">
                        Check your list of upcoming tasks
                    </div>
                </div>
            </a>
            <div class="kt-notification__custom">
                <a href="{{ route('logout') }}" class="btn btn-label-brand btn-sm btn-bold">Sign Out</a>
            </div>
        </div>
        <!--end: Navigation -->
    </div>
</div>