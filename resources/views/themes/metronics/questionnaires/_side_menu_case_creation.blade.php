<!-- begin:: Aside Menu -->
<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
    <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
        <ul class="kt-menu__nav ">
            <li class="kt-menu__item">
                <a href="javascript:;" onclick="noAccess()" class="kt-menu__link ">
                        <span class="kt-menu__link-icon">
                            <i class="flaticon-dashboard"></i>
                        </span>
                    <span class="kt-menu__link-text text-uppercase">
                            DASHBOARD
                    </span>
                </a>
            </li>
            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                <a href="javascript:;" onclick="noAccess()" class="kt-menu__link kt-menu__toggle">
                    <span class="kt-menu__link-icon">
                         <i class="flaticon-parents"></i>
                    </span>
                    <span class="kt-menu__link-text text-uppercase">FAMILY INFO</span>
                    <i class="kt-menu__ver-arrow la la-angle-right"></i>
                </a>
            </li>
            <li class="kt-menu__section ">
                <h4 class="kt-menu__section-text">RESOURCES</h4>
            </li>
            <li class="kt-menu__item">
                <a href="javascript:;" onclick="noAccess()" class="kt-menu__link ">
                        <span class="kt-menu__link-icon">
                            <i class="flaticon-tools-and-utensils"></i>
                        </span>
                    <span class="kt-menu__link-text text-uppercase">
                            Colleges
                    </span>
                </a>
            </li>
            <li class="kt-menu__item">
                <a href="javascript:;" onclick="noAccess()" class="kt-menu__link ">
                        <span class="kt-menu__link-icon">
                            <i class="flaticon-worker"></i>
                        </span>
                    <span class="kt-menu__link-text text-uppercase">
                            Majors
                    </span>
                </a>
            </li>
            <li class="kt-menu__item">
                <a href="javascript:;" onclick="noAccess()" class="kt-menu__link ">
                        <span class="kt-menu__link-icon">
                            <i class="flaticon-give-money"></i>
                        </span>
                    <span class="kt-menu__link-text text-uppercase">
                            Scholarships
                    </span>
                </a>
            </li>
            <li class="kt-menu__section ">
                <h4 class="kt-menu__section-text">SMARTTRACK® PREMIUM</h4>
            </li>
            <li class="kt-menu__item">
                <a href="javascript:;" onclick="noAccess()" class="kt-menu__link ">
                        <span class="kt-menu__link-icon">
                            <i class="flaticon-dot"></i>
                        </span>
                    <span class="kt-menu__link-text text-uppercase">
                            EFC Calculator
                    </span>
                </a>
            </li>
            <li class="kt-menu__item">
                <a href="javascript:;" onclick="noAccess()" class="kt-menu__link ">
                        <span class="kt-menu__link-icon">
                            <i class="flaticon-dot"></i>
                        </span>
                    <span class="kt-menu__link-text text-uppercase">
                            Quick Plan Scenarios
                    </span>
                </a>
            </li>
            <li class="kt-menu__item">
                <a href="javascript:;" onclick="noAccess()" class="kt-menu__link ">
                        <span class="kt-menu__link-icon">
                            <i class="flaticon-dot"></i>
                        </span>
                    <span class="kt-menu__link-text text-uppercase">
                            SMART STEPS
                    </span>
                </a>
            </li>
            <li class="kt-menu__item">
                <a href="javascript:;" onclick="noAccess()" class="kt-menu__link ">
                        <span class="kt-menu__link-icon">
                            <i class="flaticon-dot"></i>
                        </span>
                    <span class="kt-menu__link-text text-uppercase">
                            NAVIGATOR
                    </span>
                </a>
            </li>
        </ul>
    </div>
</div>
<!-- end:: Aside Menu -->