<!DOCTYPE html>
<html lang="en">
    <!-- begin::Head -->
    <head>
        <meta charset="utf-8"/>
        <title>@yield('title')</title>
        <meta name="description" content="{{ config('app.name') }}">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!--begin::Base Styles -->
        <link rel="stylesheet" href="{{ mix('css/style-3.css') }}">
        <link rel="stylesheet" href="{{ mix('css/vendors.css') }}">
        <!--end::Base Styles -->

        <link rel="shortcut icon" href="{{ config('app.cdn') }}images/favicon.ico"/>
    </head>
    <!-- end::Head -->
    <!-- begin::Body -->
    <body style="" class="kt-page--boxed kt-body--fixed kt-header--static kt-aside--offcanvas-default"
          data-gr-c-s-loaded="true">
        <!-- begin:: Page -->
        <div class="kt-grid kt-grid--hor kt-grid--root">
            <!-- BEGIN: Header -->
            <header id="kt_header" class="kt-grid__item bg-white" style="padding: 20px 0;">
                <div class="text-center">
                    <img alt="Logo" src="{{ config('app.cdn') }}images/logo.png" class="img-fluid" style="max-height: 60px;">
                </div>
            </header>
            <!-- END: Header -->
            <!-- begin::Body -->
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid m-grid--hor m-container m-container--responsive"
                 style="max-width: {{ isset($maxWidth) ? $maxWidth : "1200px" }}; margin: 0 auto;">
                <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
                    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--desktop m-grid--ver-desktop m-body__content">
                        <div class="m-grid__item m-grid__item--fluid m-wrapper">
                            <!-- BEGIN: Subheader -->
                            <div class="kt-subheader kt-grid__item" id="kt_subheader" style="background: none !important;padding-top: 25px;">
                                <div class="d-flex align-items-center text-center" style="width: 100%">
                                    <h3 class="font-s30 font-w400 text-uppercase" style="width: 100%">
                                        @yield('pageHeader')
                                    </h3>
                                </div>
                            </div>
                            <!-- END: Subheader -->
                            <div class="kt-content" style="padding-top: 0px !important;">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="" style="background: #1e1e2d; color:#9899ac; padding: 10px 0; text-align: center">
                &copy; {{ config('app.name') }} | ALL RIGHTS RESERVED | This site created and managed by Spike Sales NOW!
            </footer>
        </div>
        <!-- end:: Page -->
        <!-- begin::Scrolltop -->
        <div id="kt_scrolltop" class="kt-scrolltop">
            <i class="fa fa-arrow-up"></i>
        </div>
        <!-- end::Scrolltop -->
        <!--begin::Base Scripts -->
        @include('themes.metronics.common._scripts')
        <!--end::Base Scripts -->
    </body>
    <!-- end::Body -->
</html>