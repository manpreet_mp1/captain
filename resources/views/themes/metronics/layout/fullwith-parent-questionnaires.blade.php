<!DOCTYPE html>
<html lang="en">
    <!-- begin::Head -->
    <head>
        <meta charset="utf-8"/>
        <title>@yield('pageTitle') | {{config('app.name')}}</title>
        <meta name="description" content="{{ config('app.name') }}">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!--begin::Base Styles -->
        <link rel="stylesheet" href="{{ mix('css/metronics-vendors.bundle.css') }}">
        <link rel="stylesheet" href="{{ mix('css/default.css') }}">
        <link rel="stylesheet" href="{{ mix('css/vendors.css') }}">
        <style>
            .kt-wizard-v2__nav-icon {
                width: 35px;
            }

            .kt-wizard-v2__nav-icon i {
                font-size: 30px;
            }

            .kt-wizard-v2 .kt-wizard-v2__aside {
                flex: 0 0 300px;
            }

            .kt-wizard-v2 .kt-wizard-v2__wrapper .kt-form {
                padding: 2rem 2rem 2rem;
            }
        </style>
        <!--end::Base Styles -->
        <link rel="shortcut icon" href="{{ config('app.cdn') }}images/favicon.ico"/>
    </head>
    <!-- end::Head -->

    <!-- begin::Body -->
    <body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading kt-aside--minimize">
        <!-- begin:: Page -->
        <!-- begin:: Header Mobile -->
        <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
            <div class="kt-header-mobile__logo">
                <img src="{{ config('app.cdn') }}images/logo.png" style="width: 200px;height: 20px;" alt="logo">
            </div>
            <div class="kt-header-mobile__toolbar">
                <button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler">
                    <span></span>
                </button>
                <button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler">
                    <i class="far fa-plus-square"></i>
                </button>
            </div>
        </div>
        <!-- end:: Header Mobile -->
        <div class="kt-grid kt-grid--hor kt-grid--root">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
                <!-- begin:: Aside -->
                <button class="kt-aside-close " id="kt_aside_close_btn">
                    <i class="la la-close"></i>
                </button>
                <div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">
                    <!-- begin:: Aside -->
                    <div class="kt-aside__brand kt-grid__item " id="kt_aside_brand">
                        <div class="kt-aside__brand-logo">
                            <img src="{{ config('app.cdn') }}images/logo.png" style="width: 200px;height: 20px;" alt="logo">
                        </div>
                    </div>
                    <!-- end:: Aside -->
                    <!-- begin:: Aside Menu -->
                    @include('themes.metronics.common._parent_side_menu')
                    <!-- end:: Aside Menu -->
                </div>
                <!-- end:: Aside -->
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
            <!-- begin:: Header -->
            <div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed">
                <!-- begin:: Header Menu -->
                <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
                    <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">
                        @include('themes.metronics.common._parent_top_line')
                    </div>
                </div>
                <!-- end:: Header Menu -->
                <!-- begin:: Header Topbar -->
                <div class="kt-header__topbar">
                    <!--begin: User Bar -->
                @include('themes.metronics.common._parent_user_bar')
                <!--end: User Bar -->
                </div>
                <!-- end:: Header Topbar -->
            </div>
            <!-- end:: Header -->
            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
                    <!-- begin:: Content Head -->
                    <div class="kt-subheader kt-grid__item" id="kt_subheader">
                        <div class="kt-container  kt-container--fluid ">
                            <div class="kt-subheader__main">
                                @yield('subheader')
                            </div>
                            <div class="kt-subheader__toolbar">
                                <div class="kt-subheader__wrapper">
                                    @yield('subheader-toolbar')
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end:: Content Head -->
                    <!-- begin:: Content -->
                    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                        <div class="kt-portlet">
                            <div class="kt-portlet__body kt-portlet__body--fit">
                                <div class="kt-grid  kt-wizard-v2 kt-wizard-v2--white" id="kt_wizard_v2" data-ktwizard-state="step-first">
                                    <!--begin: Form Wizard Nav -->
                                @include('elements.parent.questionnaires.common._menu')
                                <!--end: Form Wizard Nav -->
                                    <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v2__wrapper">
                                        <!--begin: Form Wizard Form-->
                                        <div class="kt-form" id="kt_form" style="width:100%">
                                            @yield('content')
                                        </div>
                                        <!--end: Form Wizard Form-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end:: Content -->
            </div>
            <!-- end:: Content -->
            <!-- begin:: Footer -->
             @include('themes.metronics.common._footer')
            <!-- end:: Footer -->
        </div>
    </div>
</div>
<!-- end:: Page -->
<!-- begin::Scroll Top -->
<div id="kt_scrolltop" class="kt-scrolltop">
    <i class="la la-arrow-up"></i>
</div>
<!-- end::Scroll Top -->

<!--begin::Base Scripts -->
@include('themes.metronics.common._scripts')
<script>
    function noAccess() {
        Swal.fire(
            'YOU CAN NOT ACCESS THIS FEATURE UNTIL YOU COMPLETE YOUR FAMILY PROFILE.',
            '',
            'error'
        );
    }
</script>
<!--end::Base Scripts -->
</body>
<!-- end::Body -->
</html>
