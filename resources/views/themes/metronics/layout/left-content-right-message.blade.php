<!DOCTYPE html>
<html lang="en">
<!-- begin::Head -->
<head>
    <meta charset="utf-8"/>
    <title>@yield('title') | {{ config('app.name') }}</title>
    <meta name="description" content="{{ config('app.name') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--begin::Base Styles -->
    <link rel="stylesheet" href="{{ mix('css/metronics-vendors.bundle.css') }}">
    <link rel="stylesheet" href="{{ mix('css/default.css') }}">
    <link rel="stylesheet" href="{{ mix('css/vendors.css') }}">
    <!--end::Base Styles -->
    <link rel="shortcut icon" href="{{ config('app.cdn') }}images/favicon.ico"/>
</head>
<!-- end::Head -->
<!-- begin::Body -->
<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
<!-- begin:: Page -->
<div class="kt-grid kt-grid--ver kt-grid--root">
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v6 kt-login--signin" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
            <div class="kt-grid__item  kt-grid__item--order-tablet-and-mobile-2  kt-grid kt-grid--hor kt-login__aside">
                <div class="kt-login__wrapper">
                    <div class="kt-login__container">
                        <div class="kt-login__body">
                            @yield('content')
                        </div>
                    </div>
                    <div class="kt-login__account">
                        @yield('contentBottom')
                    </div>
                </div>
            </div>
            @yield('contentRight')
        </div>
    </div>
</div>
<!-- end:: Page -->
<!--begin::Base Scripts -->
@include('themes.metronics.common._scripts')
<!--end::Base Scripts -->
</body>
<!-- end::Body -->
</html>