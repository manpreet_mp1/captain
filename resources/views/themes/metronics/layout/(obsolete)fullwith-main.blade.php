<!DOCTYPE html>
<html lang="en">

<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>@yield('pageTitle') | {{config('app.name')}}</title>
    <meta name="description" content="{{ config('app.name') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--begin::Base Styles -->
    <link rel="stylesheet" href="{{ mix('css/metronics-vendors.bundle.css') }}">
    <link rel="stylesheet" href="{{ mix('css/style-3.css') }}">
    <link rel="stylesheet" href="{{ mix('css/vendors.css') }}">

    <!--end::Base Styles -->
    <link rel="shortcut icon" href="{{ config('app.cdn') }}images/favicon.ico"/>
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body class="kt-page--fluid kt--skin- kt-content--skin-light2 kt-header--fixed kt-header--fixed-mobile kt-aside-left--enabled kt-aside-left--skin-dark kt-aside-left--offcanvas kt-footer--push kt-aside--offcanvas-default">

<!-- begin:: Page -->
<div class="kt-grid kt-grid--hor kt-grid--root kt-page">

    @include('themes.metronics.common._header')

    <!-- begin::Body -->
    <div class="kt-grid__item kt-grid__itekt--fluid kt-grid kt-grid--ver-desktop kt-grid--desktop kt-body">

        @include('themes.metronics.common._left_menu')

        <!-- END: Left Aside -->
        <div class="kt-grid__item kt-grid__itekt--fluid kt-wrapper">

            <!-- BEGIN: Subheader -->
            <div class="kt-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="kt-subheader__title kt-subheader__title--separator">
                            @yield('title')
                        </h3>
                    </div>
                </div>
            </div>
            <!-- END: Subheader -->

            <div id="app" class="kt-content">
                @yield('content')
            </div>
        </div>
    </div>
    <!-- end:: Body -->

    @include('themes.metronics.common._footer')

</div>

<!-- end:: Page -->

<!-- begin::Scroll Top -->
<div id="kt_scroll_top" class="kt-scroll-top">
    <i class="la la-arrow-up"></i>
</div>
<!-- end::Scroll Top -->


<!--begin::Base Scripts -->
@include('themes.metronics.common._scripts')
<!--end::Base Scripts -->

<!--begin::Page Snippets -->

<!--end::Page Snippets -->
</body>

<!-- end::Body -->
</html>