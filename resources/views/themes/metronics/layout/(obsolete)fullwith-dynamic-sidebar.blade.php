<!DOCTYPE html>
<html lang="en">
    <!-- begin::Head -->
    <head>
        <meta charset="utf-8"/>
        <title>@yield('pageTitle') | {{config('app.name')}}</title>
        <meta name="description" content="{{ config('app.name') }}">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!--begin::Base Styles -->
        <link rel="stylesheet" href="{{ mix('css/metronics-vendors.bundle.css') }}">
        <link rel="stylesheet" href="{{ mix('css/default.css') }}">
        <link rel="stylesheet" href="{{ mix('css/vendors.css') }}">
        <!--end::Base Styles -->
        <link rel="shortcut icon" href="{{ config('app.cdn') }}images/favicon.ico"/>
    </head>
    <!-- end::Head -->

    <!-- begin::Body -->
    <body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed">
        <!-- begin:: Page -->
        <!-- begin:: Header Mobile -->
        <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
            <div class="kt-header-mobile__logo">
                <img src="{{ config('app.cdn') }}images/logo.png" style="width: 200px;height: 20px;" alt="logo">
            </div>
            <div class="kt-header-mobile__toolbar">
                <button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler">
                    <span></span>
                </button>
                <button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler">
                    <i class="far fa-plus-square"></i>
                </button>
            </div>
        </div>
        <!-- end:: Header Mobile -->

        <div class="kt-grid kt-grid--hor kt-grid--root">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
                <!-- begin:: Aside -->
                <button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
                <div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">
                    <!-- begin:: Aside -->
                    <div class="kt-aside__brand kt-grid__item " id="kt_aside_brand">
                        <div class="kt-aside__brand-logo">
                            <img src="{{ config('app.cdn') }}images/logo.png" style="width: 200px;height: 20px;" alt="logo">
                        </div>
                    </div>
                    <!-- end:: Aside -->
                    <!-- begin:: Aside Menu -->
                    @yield('menu')
                    <!-- end:: Aside Menu -->
                </div>
                <!-- end:: Aside -->
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
                    <!-- begin:: Header -->
                    <div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed">
                        <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
                        </div>
                        <!-- begin:: Header Topbar -->
                        <div class="kt-header__topbar">
                            <!--begin: User Bar -->
                            @include('themes.metronics.common._user_bar_name_logout')
                            <!--end: User Bar -->
                        </div>
                        <!-- end:: Header Topbar -->
                    </div>
                    <!-- end:: Header -->
                    <!-- begin:: Content -->
                    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
                        @yield('content')
                    </div>
                    <!-- end:: Content -->
                    <!-- begin:: Footer -->
                    @include('themes.metronics.common._footer')
                    <!-- end:: Footer -->
                </div>
            </div>
        </div>
        <!-- end:: Page -->
        <!-- begin::Scroll Top -->
        <div id="kt_scrolltop" class="kt-scrolltop">
            <i class="la la-arrow-up"></i>
        </div>
        <!-- end::Scroll Top -->

        <!--begin::Base Scripts -->
        @include('themes.metronics.common._scripts')
        <!--end::Base Scripts -->
    </body>
    <!-- end::Body -->
</html>
