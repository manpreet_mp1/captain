<!-- begin:: Footer -->
<div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-footer__copyright">
            {{now()->year}} &copy; {{ config('app.name') }}
        </div>
        <div class="kt-footer__menu">
            <a href="#" class="kt-footer__menu-link kt-link">Privacy</a>
            <a href="#" class="kt-footer__menu-link kt-link">T&C</a>
        </div>
    </div>
</div>
<!-- end:: Footer -->