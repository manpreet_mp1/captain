<li class="kt-nav__item kt-topbar__quick-actions kt-topbar__quick-actions--img kt-dropdown kt-dropdown--large kt-dropdown--header-bg-fill kt-dropdown--arrow kt-dropdown--align-right kt-dropdown--align-push kt-dropdown--mobile-full-width kt-dropdown--skin-light"
    kt-dropdown-toggle="click">
    <a href="#" class="kt-nav__link kt-dropdown__toggle">
        <span class="kt-nav__link-badge kt-badge kt-badge--dot kt-badge--info kt--hide"></span>
        <span class="kt-nav__link-icon">
            <i class="flaticon-checklist"></i>
        </span>
    </a>
    <div class="kt-dropdown__wrapper">
        <span class="kt-dropdown__arrow kt-dropdown__arrow--right kt-dropdown__arrow--adjust"></span>
        <div class="kt-dropdown__inner">
            <div class="kt-dropdown__header kt--align-center"
                 style="background: url({{ config('app.cdn') }}images/quick_actions_bg.jpg); background-size: cover;">
                <span class="kt-dropdown__header-title">Quick Actions</span>
                <span class="kt-dropdown__header-subtitle">Shortcuts</span>
            </div>
            <div class="kt-dropdown__body kt-dropdown__body--paddingless">
                <div class="kt-dropdown__content">
                    <div class="kt-scrollable" data-scrollable="false" data-height="380" data-mobile-height="200">
                        <div class="kt-nav-grid kt-nav-grid--skin-light">
                            <div class="kt-nav-grid__row">
                                <a href="{{route('leads.mortgageProtection')}}" class="kt-nav-grid__item">
                                    <i class="kt-nav-grid__icon flaticon-contract"></i>
                                    <span class="kt-nav-grid__text">Mortgage Leads</span>
                                </a>
                                <a href="{{route('data.business')}}" class="kt-nav-grid__item">
                                    <i class="kt-nav-grid__icon flaticon-report-1"></i>
                                    <span class="kt-nav-grid__text">Business Data</span>
                                </a>
                            </div>
                            <div class="kt-nav-grid__row">
                                <a href="{{route('data.consumer')}}" class="kt-nav-grid__item">
                                    <i class="kt-nav-grid__icon flaticon-teakt-1"></i>
                                    <span class="kt-nav-grid__text">Consumer Data</span>
                                </a>
                                <a href="{{route('data.executive')}}" class="kt-nav-grid__item">
                                    <i class="kt-nav-grid__icon flaticon-user-2"></i>
                                    <span class="kt-nav-grid__text">Executive Data</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</li>