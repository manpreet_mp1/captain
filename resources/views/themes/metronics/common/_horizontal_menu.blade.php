<!-- BEGIN: Horizontal Menu -->
<button class="kt-aside-header-menu-mobile-close  kt-aside-header-menu-mobile-close--skin-dark "
        id="kt_aside_header_menu_mobile_close_btn">
    <i class="la la-close"></i>
</button>
<div id="kt_header_menu"
     class="kt-header-menu kt-aside-header-menu-mobile kt-aside-header-menu-mobile--offcanvas  kt-header-menu--skin-light kt-header-menu--submenu-skin-light kt-aside-header-menu-mobile--skin-dark kt-aside-header-menu-mobile--submenu-skin-dark ">
    <ul class="kt-menu__nav  kt-menu__nav--submenu-arrow ">
        <li class="kt-menu__item  kt-menu__itekt--active  kt-menu__itekt--submenu kt-menu__itekt--rel"
            kt-menu-submenu-toggle="click" aria-haspopup="true">
            <a href="javascript:;" class="kt-menu__link kt-menu__toggle" title="Non functional dummy link">
                <i class="kt-menu__link-icon flaticon-add"></i>
                <span class="kt-menu__link-text">Actions</span>
                <i class="kt-menu__hor-arrow la la-angle-down"></i>
                <i class="kt-menu__ver-arrow la la-angle-right"></i>
            </a>
            <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
                <span class="kt-menu__arrow kt-menu__arrow--adjust"></span>
                <ul class="kt-menu__subnav">
                    <li class="kt-menu__item  kt-menu__itekt--active " aria-haspopup="true">
                        <a href="inner.html" class="kt-menu__link ">
                            <i class="kt-menu__link-icon flaticon-file"></i>
                            <span class="kt-menu__link-text">Create New Post</span>
                        </a>
                    </li>
                    <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                        <a href="inner.html" class="kt-menu__link ">
                            <i class="kt-menu__link-icon flaticon-diagram"></i>
                            <span class="kt-menu__link-title">
															<span class="kt-menu__link-wrap">
																<span class="kt-menu__link-text">Generate Reports</span>
																<span class="kt-menu__link-badge">
																	<span class="kt-badge kt-badge--success">2</span>
																</span>
															</span>
														</span>
                        </a>
                    </li>
                    <li class="kt-menu__item  kt-menu__itekt--submenu" kt-menu-submenu-toggle="hover"
                        kt-menu-link-redirect="1" aria-haspopup="true">
                        <a href="javascript:;" class="kt-menu__link kt-menu__toggle" title="Non functional dummy link">
                            <i class="kt-menu__link-icon flaticon-business"></i>
                            <span class="kt-menu__link-text">Manage Orders</span>
                            <i class="kt-menu__hor-arrow la la-angle-right"></i>
                            <i class="kt-menu__ver-arrow la la-angle-right"></i>
                        </a>
                        <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--right">
                            <span class="kt-menu__arrow "></span>
                            <ul class="kt-menu__subnav">
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <span class="kt-menu__link-text">Latest Orders</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <span class="kt-menu__link-text">Pending Orders</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <span class="kt-menu__link-text">Processed Orders</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <span class="kt-menu__link-text">Delivery Reports</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <span class="kt-menu__link-text">Payments</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <span class="kt-menu__link-text">Customers</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="kt-menu__item  kt-menu__itekt--submenu" kt-menu-submenu-toggle="hover"
                        kt-menu-link-redirect="1" aria-haspopup="true">
                        <a href="#" class="kt-menu__link kt-menu__toggle">
                            <i class="kt-menu__link-icon flaticon-chat-1"></i>
                            <span class="kt-menu__link-text">Customer Feedbacks</span>
                            <i class="kt-menu__hor-arrow la la-angle-right"></i>
                            <i class="kt-menu__ver-arrow la la-angle-right"></i>
                        </a>
                        <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--right">
                            <span class="kt-menu__arrow "></span>
                            <ul class="kt-menu__subnav">
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <span class="kt-menu__link-text">Customer Feedbacks</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <span class="kt-menu__link-text">Supplier Feedbacks</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <span class="kt-menu__link-text">Reviewed Feedbacks</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <span class="kt-menu__link-text">Resolved Feedbacks</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <span class="kt-menu__link-text">Feedback Reports</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                        <a href="inner.html" class="kt-menu__link ">
                            <i class="kt-menu__link-icon flaticon-users"></i>
                            <span class="kt-menu__link-text">Register Member</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="kt-menu__item  kt-menu__itekt--submenu kt-menu__itekt--rel" kt-menu-submenu-toggle="click"
            kt-menu-link-redirect="1" aria-haspopup="true">
            <a href="javascript:;" class="kt-menu__link kt-menu__toggle" title="Non functional dummy link">
                <span class="kt-menu__link-text">Reports</span>
                <i class="kt-menu__hor-arrow la la-angle-down"></i>
                <i class="kt-menu__ver-arrow la la-angle-right"></i>
            </a>
            <div class="kt-menu__submenu  kt-menu__submenu--fixed kt-menu__submenu--left" style="width:1000px">
                <span class="kt-menu__arrow kt-menu__arrow--adjust"></span>
                <div class="kt-menu__subnav">
                    <ul class="kt-menu__content">
                        <li class="kt-menu__item">
                            <h3 class="kt-menu__heading kt-menu__toggle">
                                <span class="kt-menu__link-text">Finance Reports</span>
                                <i class="kt-menu__ver-arrow la la-angle-right"></i>
                            </h3>
                            <ul class="kt-menu__inner">
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <i class="kt-menu__link-icon flaticon-map"></i>
                                        <span class="kt-menu__link-text">Annual Reports</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <i class="kt-menu__link-icon flaticon-user"></i>
                                        <span class="kt-menu__link-text">HR Reports</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <i class="kt-menu__link-icon flaticon-clipboard"></i>
                                        <span class="kt-menu__link-text">IPO Reports</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <i class="kt-menu__link-icon flaticon-graphic-1"></i>
                                        <span class="kt-menu__link-text">Finance Margins</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <i class="kt-menu__link-icon flaticon-graphic-2"></i>
                                        <span class="kt-menu__link-text">Revenue Reports</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="kt-menu__item">
                            <h3 class="kt-menu__heading kt-menu__toggle">
                                <span class="kt-menu__link-text">Project Reports</span>
                                <i class="kt-menu__ver-arrow la la-angle-right"></i>
                            </h3>
                            <ul class="kt-menu__inner">
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <i class="kt-menu__link-bullet kt-menu__link-bullet--line">
                                            <span></span>
                                        </i>
                                        <span class="kt-menu__link-text">Coca Cola CRM</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <i class="kt-menu__link-bullet kt-menu__link-bullet--line">
                                            <span></span>
                                        </i>
                                        <span class="kt-menu__link-text">Delta Airlines Booking Site</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <i class="kt-menu__link-bullet kt-menu__link-bullet--line">
                                            <span></span>
                                        </i>
                                        <span class="kt-menu__link-text">Malibu Accounting</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <i class="kt-menu__link-bullet kt-menu__link-bullet--line">
                                            <span></span>
                                        </i>
                                        <span class="kt-menu__link-text">Vineseed Website Rewamp</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <i class="kt-menu__link-bullet kt-menu__link-bullet--line">
                                            <span></span>
                                        </i>
                                        <span class="kt-menu__link-text">Zircon Mobile App</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <i class="kt-menu__link-bullet kt-menu__link-bullet--line">
                                            <span></span>
                                        </i>
                                        <span class="kt-menu__link-text">Mercury CMS</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="kt-menu__item">
                            <h3 class="kt-menu__heading kt-menu__toggle">
                                <span class="kt-menu__link-text">HR Reports</span>
                                <i class="kt-menu__ver-arrow la la-angle-right"></i>
                            </h3>
                            <ul class="kt-menu__inner">
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="kt-menu__link-text">Staff Directory</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="kt-menu__link-text">Client Directory</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="kt-menu__link-text">Salary Reports</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="kt-menu__link-text">Staff Payslips</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="kt-menu__link-text">Corporate Expenses</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="kt-menu__link-text">Project Expenses</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="kt-menu__item">
                            <h3 class="kt-menu__heading kt-menu__toggle">
                                <span class="kt-menu__link-text">Reporting Apps</span>
                                <i class="kt-menu__ver-arrow la la-angle-right"></i>
                            </h3>
                            <ul class="kt-menu__inner">
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <span class="kt-menu__link-text">Report Adjusments</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <span class="kt-menu__link-text">Sources & Mediums</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <span class="kt-menu__link-text">Reporting Settings</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <span class="kt-menu__link-text">Conversions</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <span class="kt-menu__link-text">Report Flows</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <span class="kt-menu__link-text">Audit & Logs</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="kt-menu__item  kt-menu__itekt--submenu kt-menu__itekt--rel" kt-menu-submenu-toggle="click"
            kt-menu-link-redirect="1" aria-haspopup="true">
            <a href="javascript:;" class="kt-menu__link kt-menu__toggle" title="Non functional dummy link">
											<span class="kt-menu__link-title">
												<span class="kt-menu__link-wrap">
													<span class="kt-menu__link-text">Apps</span>
													<span class="kt-menu__link-badge">
														<span class="kt-badge kt-badge--brand">3</span>
													</span>
												</span>
											</span>
                <i class="kt-menu__hor-arrow la la-angle-down"></i>
                <i class="kt-menu__ver-arrow la la-angle-right"></i>
            </a>
            <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
                <span class="kt-menu__arrow kt-menu__arrow--adjust"></span>
                <ul class="kt-menu__subnav">
                    <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                        <a href="inner.html" class="kt-menu__link ">
                            <i class="kt-menu__link-icon flaticon-business"></i>
                            <span class="kt-menu__link-text">eCommerce</span>
                        </a>
                    </li>
                    <li class="kt-menu__item  kt-menu__itekt--submenu" kt-menu-submenu-toggle="hover"
                        kt-menu-link-redirect="1" aria-haspopup="true">
                        <a href="crud/datatable_v1.html" class="kt-menu__link kt-menu__toggle">
                            <i class="kt-menu__link-icon flaticon-computer"></i>
                            <span class="kt-menu__link-text">Audience</span>
                            <i class="kt-menu__hor-arrow la la-angle-right"></i>
                            <i class="kt-menu__ver-arrow la la-angle-right"></i>
                        </a>
                        <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--right">
                            <span class="kt-menu__arrow "></span>
                            <ul class="kt-menu__subnav">
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <i class="kt-menu__link-icon flaticon-users"></i>
                                        <span class="kt-menu__link-text">Active Users</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <i class="kt-menu__link-icon flaticon-interface-1"></i>
                                        <span class="kt-menu__link-text">User Explorer</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <i class="kt-menu__link-icon flaticon-lifebuoy"></i>
                                        <span class="kt-menu__link-text">Users Flows</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <i class="kt-menu__link-icon flaticon-graphic-1"></i>
                                        <span class="kt-menu__link-text">Market Segments</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <i class="kt-menu__link-icon flaticon-graphic"></i>
                                        <span class="kt-menu__link-text">User Reports</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                        <a href="inner.html" class="kt-menu__link ">
                            <i class="kt-menu__link-icon flaticon-map"></i>
                            <span class="kt-menu__link-text">Marketing</span>
                        </a>
                    </li>
                    <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                        <a href="inner.html" class="kt-menu__link ">
                            <i class="kt-menu__link-icon flaticon-graphic-2"></i>
                            <span class="kt-menu__link-title">
															<span class="kt-menu__link-wrap">
																<span class="kt-menu__link-text">Campaigns</span>
																<span class="kt-menu__link-badge">
																	<span class="kt-badge kt-badge--success">3</span>
																</span>
															</span>
														</span>
                        </a>
                    </li>
                    <li class="kt-menu__item  kt-menu__itekt--submenu" kt-menu-submenu-toggle="hover"
                        kt-menu-link-redirect="1" aria-haspopup="true">
                        <a href="javascript:;" class="kt-menu__link kt-menu__toggle" title="Non functional dummy link">
                            <i class="kt-menu__link-icon flaticon-infinity"></i>
                            <span class="kt-menu__link-text">Cloud Manager</span>
                            <i class="kt-menu__hor-arrow la la-angle-right"></i>
                            <i class="kt-menu__ver-arrow la la-angle-right"></i>
                        </a>
                        <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
                            <span class="kt-menu__arrow "></span>
                            <ul class="kt-menu__subnav">
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <i class="kt-menu__link-icon flaticon-add"></i>
                                        <span class="kt-menu__link-title">
																		<span class="kt-menu__link-wrap">
																			<span class="kt-menu__link-text">File Upload</span>
																			<span class="kt-menu__link-badge">
																				<span class="kt-badge kt-badge--danger">3</span>
																			</span>
																		</span>
																	</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <i class="kt-menu__link-icon flaticon-signs-1"></i>
                                        <span class="kt-menu__link-text">File Attributes</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <i class="kt-menu__link-icon flaticon-folder"></i>
                                        <span class="kt-menu__link-text">Folders</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " kt-menu-link-redirect="1" aria-haspopup="true">
                                    <a href="inner.html" class="kt-menu__link ">
                                        <i class="kt-menu__link-icon flaticon-cogwheel-2"></i>
                                        <span class="kt-menu__link-text">System Settings</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </li>
    </ul>
</div>

<!-- END: Horizontal Menu -->