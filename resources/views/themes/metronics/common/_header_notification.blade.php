<li class="kt-nav__item kt-topbar__notifications kt-topbar__notifications--img kt-dropdown kt-dropdown--large kt-dropdown--header-bg-fill kt-dropdown--arrow kt-dropdown--align-right 	kt-dropdown--mobile-full-width"
    kt-dropdown-toggle="click" kt-dropdown-persistent="1">
    <a href="#" class="kt-nav__link kt-dropdown__toggle" id="kt_topbar_notification_icon">
        <span class="kt-nav__link-icon">
            <i class="flaticon-alarm"></i>
        </span>
    </a>
    <div class="kt-dropdown__wrapper">
        <span class="kt-dropdown__arrow kt-dropdown__arrow--right"></span>
        <div class="kt-dropdown__inner">
            <div class="kt-dropdown__header kt--align-center"
                 style="background: url({{ config('app.cdn') }}images/notification_bg.jpg); background-size: cover;">
                <span class="kt-dropdown__header-title">Recent Activities</span>
                <span class="kt-dropdown__header-subtitle">App User Notifications</span>
            </div>
            <div class="kt-dropdown__body">
                <div class="kt-dropdown__content">
                    <div class="tab-content">
                        <div class="tab-pane active" id="topbar_notifications_notifications" role="tabpanel">
                            <div class="kt-scrollable" data-scrollable="true" data-height="400" data-mobile-height="200">
                                <div class="kt-list-timeline kt-list-timeline--skin-light">
                                    <div class="kt-list-timeline__items">
                                        @foreach($appMaskedPurchases as $purchase)
                                            <div class="kt-list-timeline__item">
                                                <span class="kt-list-timeline__badge kt-list-timeline__badge--brand-5"></span>
                                                <span class="kt-list-timeline__text">
                                                <span class="kt-link kt--font-bolder text-brand">{{ $appMaskedRandomName[rand(0, 5)] }}</span>
                                                purchased
                                                <span class="kt-link kt--font-bolder text-brand">{{ $purchase->purchase->formatted_count }}</span>
                                                    {{ $purchase->formatted_type }}.
                                                </span>
                                                <span class="kt-list-timeline__time"
                                                      style="width: 120px;">{{ $purchase->updated_at->diffForHumans() }}</span>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</li>