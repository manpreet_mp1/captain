<script src="{{ mix('js/main.js') }}"></script>
<script src="{{ mix('js/vendors.js') }}"></script>
<script src="{{ mix('js/app.js') }}"></script>
@include('flash::message')
@yield('scripts')
