<!-- BEGIN: Header -->
<header id="kt_header" class="kt-grid__item kt-header " kt-minimize-offset="200" kt-minimize-mobile-offset="200">
    <div class="kt-container kt-container--fluid kt-container--full-height">
        <div class="kt-stack kt-stack--ver kt-stack--desktop">

            <!-- BEGIN: Brand -->
            <div class="kt-stack__item kt-brand  kt-brand--skin-dark ">
                <div class="kt-stack kt-stack--ver kt-stack--general">
                    <div class="kt-stack__item kt-stack__itekt--middle kt-stack__itekt--center kt-brand__logo">
                        <a href="{{ route('dashboard')  }}" class="kt-brand__logo-wrapper">
                            <img alt="" src="{{ config('app.cdn') }}images/dashboard-logo-white.png" />
                        </a>
                    </div>
                    <div class="kt-stack__item kt-stack__itekt--middle kt-brand__tools">

                        <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                        <a href="javascript:;" id="kt_aside_left_offcanvas_toggle" class="kt-brand__icon kt-brand__toggler kt-brand__toggler--left kt--visible-tablet-and-mobile-inline-block">
                            <span></span>
                        </a>
                        <!-- END -->

                        <!-- BEGIN: Responsive Header Menu Toggler -->
                        <a id="kt_aside_header_menu_mobile_toggle" href="javascript:;" class="kt-brand__icon kt-brand__toggler kt--visible-tablet-and-mobile-inline-block">
                            <span></span>
                        </a>
                        <!-- END -->

                        <!-- BEGIN: Topbar Toggler -->
                        <a id="kt_aside_header_topbar_mobile_toggle" href="javascript:;" class="kt-brand__icon kt--visible-tablet-and-mobile-inline-block">
                            <i class="flaticon-more"></i>
                        </a>
                        <!-- BEGIN: Topbar Toggler -->
                    </div>
                </div>
            </div>
            <!-- END: Brand -->
            <div class="kt-stack__item kt-stack__itekt--fluid kt-header-head" id="kt_header_nav">
                <!-- BEGIN: Topbar -->
                <div id="kt_header_topbar" class="kt-topbar  kt-stack kt-stack--ver kt-stack--general">
                    <div class="kt-stack__item kt-topbar__nav-wrapper">
                        <ul class="kt-topbar__nav kt-nav kt-nav--inline">
                            @include('themes.metronics.common._header_notification')
                            @include('themes.metronics.common._header_quick_actions')
                            @include('themes.metronics.common._header_profile')
                        </ul>
                    </div>
                </div>

                <!-- END: Topbar -->
            </div>
        </div>
    </div>
</header>

<!-- END: Header -->