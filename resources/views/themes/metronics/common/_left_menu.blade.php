<!-- BEGIN: Left Aside -->
<button class="kt-aside-left-close kt-aside-left-close--skin-dark" id="kt_aside_left_close_btn">
    <i class="la la-close"></i>
</button>
<div id="kt_aside_left" class="kt-grid__item	kt-aside-left  kt-aside-left--skin-dark ">
    <!-- BEGIN: Aside Menu -->
    <div id="kt_ver_menu"
         class="kt-aside-menu  kt-aside-menu--skin-dark kt-aside-menu--submenu-skin-dark kt-aside-menu--dropdown "
         data-menu-vertical="true" kt-menu-dropdown="1" kt-menu-scrollable="0" kt-menu-dropdown-timeout="500">
        <ul class="kt-menu__nav  kt-menu__nav--dropdown-submenu-arrow ">
            <li class="kt-menu__item {{ Route::currentRouteName() == 'dashboard' ? 'kt-menu__itekt--active' : ''}}"
                aria-haspopup="true">
                <a href="{{route('dashboard')}}" class="kt-menu__link ">
                    <span class="kt-menu__itekt-here"></span>
                    <i class="kt-menu__link-icon flaticon-report"></i>
                    <span class="kt-menu__link-text">Dashboard</span>
                </a>
            </li>
            <li class="kt-menu__item {{ Request::is('leads*') ? 'kt-menu__itekt--active' : ''}}" aria-haspopup="true">
                <a href="{{route('leads.index')}}" class="kt-menu__link" id="kt-menu__leads">
                    <span class="kt-menu__itekt-here"></span>
                    <i class="kt-menu__link-icon flaticon-user"></i>
                    <span class="kt-menu__link-text">Leads</span>
                </a>
            </li>
            <li class="kt-menu__item {{ Request::is('lead-orders*') ? 'kt-menu__itekt--active' : ''}}" aria-haspopup="true">
                <a href="{{route('my.leads.orders')}}" class="kt-menu__link ">
                    <span class="kt-menu__itekt-here"></span>
                    <i class="kt-menu__link-icon flaticon-user"></i>
                    <span class="kt-menu__link-text">Lead Orders</span>
                </a>
            </li>
            <li class="kt-menu__item {{ Request::is('data*') ? 'kt-menu__itekt--active' : ''}}" aria-haspopup="true">
                <a href="{{route('data.index')}}" class="kt-menu__link ">
                    <span class="kt-menu__itekt-here"></span>
                    <i class="kt-menu__link-icon flaticon-data"></i>
                    <span class="kt-menu__link-text">Data</span>
                </a>
            </li>
            <li class="kt-menu__item kt-menu__itekt--submenu {{ Request::is('my*') ? 'kt-menu__itekt--active' : ''}}"
                aria-haspopup="true"
                kt-menu-submenu-toggle="hover"
                kt-menu-link-redirect="1">
                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                    <span class="kt-menu__itekt-here"></span>
                    <i class="kt-menu__link-icon flaticon-search"></i>
                    <span class="kt-menu__link-text">My</span>
                    <i class="kt-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                    <ul class="kt-menu__subnav">
                        <li class="kt-menu__item" aria-haspopup="true" kt-menu-link-redirect="1">
                            <a href="{{route('search')}}" class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-title">
                                    <span class="kt-menu__link-wrap">
                                        <span class="kt-menu__link-text">Searches</span>
                                    </span>
                                </span>
                            </a>
                        </li>
                        <li class="kt-menu__item" aria-haspopup="true" kt-menu-link-redirect="1">
                            <a href="{{route('purchases')}}" class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-title">
                                    <span class="kt-menu__link-wrap">
                                        <span class="kt-menu__link-text">Purchases</span>
                                    </span>
                                </span>
                            </a>
                        </li>
                        <li class="kt-menu__item" aria-haspopup="true" kt-menu-link-redirect="1">
                            <a href="{{route('my.leads')}}" class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-title">
                                    <span class="kt-menu__link-wrap">
                                        <span class="kt-menu__link-text">Leads</span>
                                    </span>
                                </span>
                            </a>
                        </li>
                        <li class="kt-menu__item" aria-haspopup="true" kt-menu-link-redirect="1">
                            <a href="{{route('billing')}}" class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-title">
                                    <span class="kt-menu__link-wrap">
                                        <span class="kt-menu__link-text">Billing Information</span>
                                    </span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        <!--
            <li class="kt-menu__item { Request::is('searches*') ? 'kt-menu__itekt--active' : ''}}" aria-haspopup="true">
                <a href="{route('search')}}" class="kt-menu__link ">
                    <span class="kt-menu__itekt-here"></span>
                    <i class="kt-menu__link-icon flaticon-search"></i>
                    <span class="kt-menu__link-text">Searches</span>
                </a>
            </li>
            <li class="kt-menu__item { Request::is('purchases*') ? 'kt-menu__itekt--active' : ''}}" aria-haspopup="true">
                <a href="{{route('purchases')}}" class="kt-menu__link ">
                    <span class="kt-menu__itekt-here"></span>
                    <i class="kt-menu__link-icon flaticon-cart-3"></i>
                    <span class="kt-menu__link-text">Purchases</span>
                </a>
            </li>
            -->
            <li class="kt-menu__item {{ Request::is('dasdsadsadsa*') ? 'kt-menu__itekt--active' : ''}}"
                aria-haspopup="true">
                <a href="{{route('purchases')}}" class="kt-menu__link ">
                    <span class="kt-menu__itekt-here"></span>
                    <i class="kt-menu__link-icon flaticon-help"></i>
                    <span class="kt-menu__link-text">Getting Started</span>
                </a>
            </li>
            @if(auth()->user()->isAdmin())
                <li class="kt-menu__item {{ Request::is('pricing*') ? 'kt-menu__itekt--active' : ''}}"
                    aria-haspopup="true">
                    <a href="{{route('admin.pricing.index')}}" class="kt-menu__link ">
                        <span class="kt-menu__itekt-here"></span>
                        <i class="kt-menu__link-icon flaticon-price"></i>
                        <span class="kt-menu__link-text">Set Pricing</span>
                    </a>
                </li>
                <li class="kt-menu__item {{ Request::is('users*') ? 'kt-menu__itekt--active' : ''}}" aria-haspopup="true">
                    <a href="{{route('admin.users.index')}}" class="kt-menu__link ">
                        <span class="kt-menu__itekt-here"></span>
                        <i class="kt-menu__link-icon flaticon-team"></i>
                        <span class="kt-menu__link-text">Users</span>
                    </a>
                </li>
            @endif
        </ul>
    </div>
    <!-- END: Aside Menu -->
</div>