<li class="kt-nav__item kt-topbar__user-profile kt-topbar__user-profile--img kt-dropdown kt-dropdown--medium kt-dropdown--arrow kt-dropdown--header-bg-fill kt-dropdown--align-right kt-dropdown--mobile-full-width kt-dropdown--skin-light"
    kt-dropdown-toggle="click">
    <a href="#" class="kt-nav__link kt-dropdown__toggle">
        <span class="kt-topbar__userpic">
		    <img src="{{ config('app.cdn') }}images/user-placeholder.png" alt=""/>
        </span>
    </a>
    <div class="kt-dropdown__wrapper">
        <span class="kt-dropdown__arrow kt-dropdown__arrow--right kt-dropdown__arrow--adjust"></span>
        <div class="kt-dropdown__inner">
            <div class="kt-dropdown__header kt--align-center"
                 style="background: url({{ config('app.cdn') }}images/user_profile_bg.jpg); background-size: cover;">
                <div class="kt-card-user kt-card-user--skin-dark">
                    <div class="kt-card-user__pic">
                        <img src="{{ config('app.cdn') }}images/user-placeholder.png" alt=""/>
                    </div>
                    <div class="kt-card-user__details">
                        <span class="kt-card-user__name kt--font-weight-500">{{auth()->user()->full_name}}</span>
                        <span class="kt-card-user__email kt--font-weight-300 kt-link">{{auth()->user()->email}}</span>
                    </div>
                </div>
            </div>
            <div class="kt-dropdown__body">
                <div class="kt-dropdown__content">
                    <ul class="kt-nav kt-nav--skin-light">
                        <li class="kt-nav__section kt--hide">
                            <span class="kt-nav__section-text">Section</span>
                        </li>
                        <li class="kt-nav__item">
                            <a href="{{route('purchases')}}" class="kt-nav__link">
                                <i class="kt-nav__link-icon flaticon-user"></i>
                                <span class="kt-nav__link-text">My Profile</span>
                            </a>
                        </li>
                        <li class="kt-nav__separator kt-nav__separator--fit">
                        </li>
                        <li class="kt-nav__item">
                            <a href="{{route('purchases')}}" class="kt-nav__link">
                                <i class="kt-nav__link-icon flaticon-help"></i>
                                <span class="kt-nav__link-text">FAQ</span>
                            </a>
                        </li>
                        <li class="kt-nav__item">
                            <a href="{{route('purchases')}}" class="kt-nav__link">
                                <i class="kt-nav__link-icon flaticon-refund"></i>
                                <span class="kt-nav__link-text">Request Refund</span>
                            </a>
                        </li>
                        <li class="kt-nav__item">
                            <a href="{{route('purchases')}}" class="kt-nav__link">
                                <i class="kt-nav__link-icon flaticon-support"></i>
                                <span class="kt-nav__link-text">Support</span>
                            </a>
                        </li>
                        <li class="kt-nav__separator kt-nav__separator--fit">
                        </li>
                        <li class="kt-nav__item">
                            <a href="{{ route('logout') }}"
                               class="btn btn-brand btn-sm kt-btn kt-btn--custom kt-btn--label-brand kt-btn--bolder">Logout</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</li>