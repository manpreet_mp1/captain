<!-- begin:: Aside Menu -->
<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
    <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
        <ul class="kt-menu__nav ">
            <li class="kt-menu__item {{ Request::is('*dashboard') ? 'kt-menu__item--active' : ''}}">
                <a href="{{ route('advisor.dashboard') }}" class="kt-menu__link ">
                        <span class="kt-menu__link-icon">
                            <i class="flaticon-dashboard"></i>
                        </span>
                    <span class="kt-menu__link-text text-uppercase">
                            DASHBOARD
                    </span>
                </a>
            </li>
{{--            <li class="kt-menu__item">--}}
{{--                <a href="javascript:" onclick="noAccessRemoved()" class="kt-menu__link ">--}}
{{--                        <span class="kt-menu__link-icon">--}}
{{--                            <i class="fa fa-users"></i>--}}
{{--                        </span>--}}
{{--                    <span class="kt-menu__link-text text-uppercase">--}}
{{--                            All Cases--}}
{{--                    </span>--}}
{{--                </a>--}}
{{--            </li>--}}
            @if($showFullMenu === 1)
                <li class="kt-menu__section ">
                    <h4 class="kt-menu__section-text">{{ $ST_ADVISOR_FAMILY->name_text }}</h4>
                </li>
                <!-- Case Dashboard -->
                <li class="kt-menu__item {{ Request::is("*advisor/case/$ST_ADVISOR_FAMILY->slug") ? 'kt-menu__item--active' : ''}}">
                    <a href="{{ route('advisor.cases.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="kt-menu__link ">
                        <span class="kt-menu__link-icon">
                            <i class="flaticon-dashboard"></i>
                        </span>
                        <span class="kt-menu__link-text text-uppercase">
                            {{ $ST_ADVISOR_FAMILY->name_text }}
                    </span>
                    </a>
                </li>
                <!-- Case Family breakdown -->
                <li class="kt-menu__item  kt-menu__item--submenu {{ Request::is('advisor/case/*/family-info*') ? 'kt-menu__item--active kt-menu__item--open' : ''}}" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                    <span class="kt-menu__link-icon">
                         <i class="flaticon-parents"></i>
                    </span>
                        <span class="kt-menu__link-text text-uppercase">FAMILY INFO</span>
                        <i class="kt-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="kt-menu__submenu ">
                        <span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link">
                                <span class="kt-menu__link-text text-uppercase">FAMILY INFO</span></span>
                            </li>
                            <li class="kt-menu__item {{ Request::is('advisor/case/*/family-info/parents*') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                                <a href="{{ route('advisor.case.familyInfo.parents', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text text-uppercase">Parents</span>
                                </a>
                            </li>
                            <li class="kt-menu__item {{ Request::is('advisor/case/*/family-info/students*') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                                <a href="{{ route('advisor.case.familyInfo.students', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text text-uppercase">Students</span>
                                </a>
                            </li>
                            <!-- Income -->
                            <li class="kt-menu__item kt-menu__item--submenu {{ Request::is('advisor/case/*/family-info/income*') ? 'kt-menu__item--active kt-menu__item--open' : ''}}" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--line">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">INCOME</span>
                                    <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="kt-menu__submenu" style="">
                                    <span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item {{ Request::is('advisor/case/*/family-info/income-info') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                                            <a href="{{ route('advisor.case.familyInfo.incomeInfo', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">INCOME</span>
                                            </a>
                                        </li>
                                        <li class="kt-menu__item {{ Request::is('advisor/case/*/family-info/income-tax') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                                            <a href="{{ route('advisor.case.familyInfo.incomeTax', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">TAX</span>
                                            </a>
                                        </li>
                                        <li class="kt-menu__item {{ Request::is('advisor/case/*/family-info/income-additional') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                                            <a href="{{ route('advisor.case.familyInfo.incomeAdditional', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">ADDITIONAL FINANCIAL</span>
                                            </a>
                                        </li>
                                        <li class="kt-menu__item {{ Request::is('advisor/case/*/family-info/income-untaxed') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                                            <a href="{{ route('advisor.case.familyInfo.incomeUntaxed', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">UNTAXED</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <!-- Assets -->
                            <li class="kt-menu__item kt-menu__item--submenu {{ Request::is('advisor/case/*/family-info/assets*') ? 'kt-menu__item--active kt-menu__item--open' : ''}}" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--line">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">ASSETS</span>
                                    <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="kt-menu__submenu" style="">
                                    <span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item {{ Request::is('advisor/case/*/family-info/assets-education') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                                            <a href="{{ route('advisor.case.familyInfo.assetsEducation', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">EDUCATION</span>
                                            </a>
                                        </li>
                                        <li class="kt-menu__item {{ Request::is('advisor/case/*/family-info/assets-non-retirement') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                                            <a href="{{ route('advisor.case.familyInfo.assetsNonRetirement', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">NON-RETIREMENT</span>
                                            </a>
                                        </li>
                                        <li class="kt-menu__item {{ Request::is('advisor/case/*/family-info/assets-retirement') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                                            <a href="{{ route('advisor.case.familyInfo.assetsRetirement', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">RETIREMENT</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="kt-menu__item {{ Request::is('advisor/case/*/family-info/real-estate') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                                <a href="{{ route('advisor.case.familyInfo.realEstate', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text text-uppercase">Real Estate</span>
                                </a>
                            </li>
                            <li class="kt-menu__item {{ Request::is('advisor/case/*/family-info/business') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                                <a href="{{ route('advisor.case.familyInfo.business', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text text-uppercase">Business Ownership</span>
                                </a>
                            </li>
                            <li class="kt-menu__item {{ Request::is('advisor/case/*/family-info/insurance') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                                <a href="{{ route('advisor.case.familyInfo.insurance', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text text-uppercase">Insurance</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <!-- RESOURCES -->
                <li class="kt-menu__section ">
                    <h4 class="kt-menu__section-text">RESOURCES</h4>
                </li>
                <!-- Colleges -->
                <li class="kt-menu__item {{ Request::is('advisor/case/*/colleges*') ? 'kt-menu__item--active' : ''}}">
                    <a href="{{ route('advisor.case.colleges', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="kt-menu__link ">
                        <span class="kt-menu__link-icon">
                            <i class="flaticon-tools-and-utensils"></i>
                        </span>
                        <span class="kt-menu__link-text text-uppercase">
                        Colleges
                        </span>
                    </a>
                </li>
                <!-- Majors -->
                <li class="kt-menu__item {{ Request::is('advisor/case/*/majors*') ? 'kt-menu__item--active' : ''}}">
                    <a href="{{ route('advisor.case.majors', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="kt-menu__link ">
                        <span class="kt-menu__link-icon">
                            <i class="flaticon-worker"></i>
                        </span>
                        <span class="kt-menu__link-text text-uppercase">Majors</span>
                    </a>
                </li>

                <li class="kt-menu__item {{ Request::is('advisor/case/*/efc') ? 'kt-menu__item--active' : ''}}">
                    <a href="{{ route('advisor.case.efc.result', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="kt-menu__link ">
                    <span class="kt-menu__link-icon">
                        <i class="flaticon-dot"></i>
                    </span>
                        <span class="kt-menu__link-text text-uppercase">
                        EFC Calculator
                    </span>
                    </a>
                </li>
                <!-- SMARTTRACK® PREMIUM -->
                <!-- @todo - only show if user is paid -->
                <li class="kt-menu__section ">
                    <h4 class="kt-menu__section-text">SMARTTRACK® PREMIUM</h4>
                </li>
                <li class="kt-menu__item {{ Request::is('advisor/case/*/scholarships/merit*') ? 'kt-menu__item--active' : ''}}">
                    <a href="{{ route('advisor.case.scholarships.merit', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="kt-menu__link ">
                    <span class="kt-menu__link-icon">
                        <i class="flaticon-give-money"></i>
                    </span>
                        <span class="kt-menu__link-text text-uppercase">
                        Merit Scholarships
                    </span>
                    </a>
                </li>
                <!-- Scholarship -->
{{--                <li class="kt-menu__item  kt-menu__item--submenu {{ Request::is('advisor/case/*/scholarships/*') ? 'kt-menu__item--active kt-menu__item--open' : ''}}" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">--}}
{{--                    <a href="javascript:" class="kt-menu__link kt-menu__toggle">--}}
{{--                    <span class="kt-menu__link-icon">--}}
{{--                         <i class="flaticon-give-money"></i>--}}
{{--                    </span>--}}
{{--                        <span class="kt-menu__link-text text-uppercase">Scholarships</span>--}}
{{--                        <i class="kt-menu__ver-arrow la la-angle-right"></i>--}}
{{--                    </a>--}}
{{--                    <div class="kt-menu__submenu ">--}}
{{--                        <span class="kt-menu__arrow"></span>--}}
{{--                        <ul class="kt-menu__subnav">--}}
{{--                            <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true">--}}
{{--                                <span class="kt-menu__link">--}}
{{--                                <span class="kt-menu__link-text text-uppercase">Scholarships</span>--}}
{{--                                </span>--}}
{{--                            </li>--}}
{{--                            <li class="kt-menu__item {{ Request::is('advisor/case/*/scholarships/merit*') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">--}}
{{--                                <a href="{{ route('advisor.case.scholarships.merit', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="kt-menu__link ">--}}
{{--                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="kt-menu__link-text text-uppercase">Merit</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            --}}{{--                            <li class="kt-menu__item " aria-haspopup="true">--}}
{{--                            --}}{{--                                <a href="javascript:;" onclick="noAccessRemoved()" class="kt-menu__link ">--}}
{{--                            --}}{{--                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">--}}
{{--                            <span></span></i>--}}
{{--                            --}}{{--                                    <span class="kt-menu__link-text text-uppercase">Private</span>--}}
{{--                            --}}{{--                                </a>--}}
{{--                            --}}{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
                <li class="kt-menu__item {{ Request::is('advisor/case/*/scenarios*') ? 'kt-menu__item--active' : ''}}">
                    <a href="{{ route('advisor.case.scenarios', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="kt-menu__link ">
                    <span class="kt-menu__link-icon">
                        <i class="flaticon-dot"></i>
                    </span>
                        <span class="kt-menu__link-text text-uppercase">
                        Quick Plan Scenarios
                    </span>
                    </a>
                </li>
{{--                <li class="kt-menu__item {{ Request::is('*smart-steps') ? 'kt-menu__item--active' : ''}}">--}}
{{--                    <a href="#" class="kt-menu__link ">--}}
{{--                    <span class="kt-menu__link-icon">--}}
{{--                        <i class="flaticon-dot"></i>--}}
{{--                    </span>--}}
{{--                        <span class="kt-menu__link-text text-uppercase">--}}
{{--                        SMART STEPS--}}
{{--                    </span>--}}
{{--                    </a>--}}
{{--                </li>--}}
                <li class="kt-menu__item">
                    <a href="{{ route('advisor.case.colleges', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="kt-menu__link ">
                    <span class="kt-menu__link-icon">
                        <i class="flaticon-dot"></i>
                    </span>
                        <span class="kt-menu__link-text text-uppercase">
                        COLLEGE NAVIGATOR
                    </span>
                    </a>
                </li>
                <li class="kt-menu__section ">
                    <h4 class="kt-menu__section-text">ADVISOR ONLY</h4>
                </li>
                <!-- Quick PLan -->
                <li class="kt-menu__item {{ Request::is('advisor/case/*/quick-plan*') ? 'kt-menu__item--active' : ''}}">
                    <a href="{{ route('advisor.case.quickPlan', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="kt-menu__link ">
                    <span class="kt-menu__link-icon">
                        <i class="flaticon-dot"></i>
                    </span>
                        <span class="kt-menu__link-text text-uppercase">
                        QUICK PLAN
                    </span>
                    </a>
                </li>
                <!-- 529 Plans -->
                <li class="kt-menu__item  kt-menu__item--submenu {{ Request::is('advisor/case/*/five-two-nine*') ? 'kt-menu__item--active kt-menu__item--open' : ''}}" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <a href="javascript:" class="kt-menu__link kt-menu__toggle">
                    <span class="kt-menu__link-icon">
                         <i class="fa fa-graduation-cap"></i>
                    </span>
                        <span class="kt-menu__link-text text-uppercase">529 PLANS</span>
                        <i class="kt-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="kt-menu__submenu ">
                        <span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true">
                                <span class="kt-menu__link">
                                <span class="kt-menu__link-text text-uppercase">529 PLANS</span>
                                </span>
                            </li>
                            <li class="kt-menu__item {{ Request::is('advisor/case/*/five-two-nine*')
                            &&  !Request::is('advisor/case/*/five-two-nine/compare*')
                            && !Request::is('advisor/case/*/five-two-nine/common-questions*') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true"
                            >
                                <a href="{{ route('advisor.case.fiveTwoNine', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text text-uppercase">Search Plans</span>
                                </a>
                            </li>
                            <li class="kt-menu__item {{ Request::is('advisor/case/*/five-two-nine/compare*') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                                <a href="{{ route('advisor.case.fiveTwoNine.compare', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text text-uppercase">Compare Plans</span>
                                </a>
                            </li>
                            <li class="kt-menu__item {{ Request::is('advisor/case/*/five-two-nine/common-questions*') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                                <a href="{{ route('advisor.case.fiveTwoNine.commonQuestions', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text text-uppercase">Common Questions</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <!-- Strategies -->
                <li class="kt-menu__item  kt-menu__item--submenu {{ Request::is('advisor/case/*/client-strategies/*') ? 'kt-menu__item--active kt-menu__item--open' : ''}}" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <a href="javascript:" class="kt-menu__link kt-menu__toggle">
                    <span class="kt-menu__link-icon">
                         <i class="fa fa-chart-bar"></i>
                    </span>
                        <span class="kt-menu__link-text text-uppercase">Client Strategies</span>
                        <i class="kt-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="kt-menu__submenu ">
                        <span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true">
                                <span class="kt-menu__link">
                                    <span class="kt-menu__link-text text-uppercase">Client Strategies</span>
                                </span>
                            </li>
                            <li class="kt-menu__item {{ Request::is('advisor/case/*/client-strategies/strategies*') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                                <a href="{{ route('advisor.case.clientStrategies.strategies', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text text-uppercase">Strategies</span>
                                </a>
                            </li>
                            <li class="kt-menu__item {{ Request::is('advisor/case/*/client-strategies/tax-talking-points*') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text text-uppercase">Tax Talking Points</span>
                                </a>
                            </li>
                            <li class="kt-menu__item {{ Request::is('advisor/case/*/client-strategies/form-1040*') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                                <a href="{{ route('advisor.form-1040', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text text-uppercase">Form 1040</span>
                                </a>
                            </li>
{{--                            <li class="kt-menu__item {{ Request::is('advisor/case/*/client-strategies/my-strategies*') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">--}}
{{--                                <a href="{{ route('scholarships.merit') }}" class="kt-menu__link ">--}}
{{--                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="kt-menu__link-text text-uppercase">My Strategies</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="kt-menu__item {{ Request::is('advisor/case/*/client-strategies/default-strategies*') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">--}}
{{--                                <a href="{{ route('scholarships.merit') }}" class="kt-menu__link ">--}}
{{--                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">--}}
{{--                                        <span></span>--}}
{{--                                    </i>--}}
{{--                                    <span class="kt-menu__link-text text-uppercase">Default Strategies</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
                        </ul>
                    </div>
                </li>
                <!-- QP Calculator -->

                {{--                <li class="kt-menu__section ">--}}
                {{--                    <h4 class="kt-menu__section-text">SMARTTRACK® PREMIUM</h4>--}}
                {{--                </li>--}}
                {{--                <li class="kt-menu__item {{ Request::is('*smart-steps') ? 'kt-menu__item--active' : ''}}">--}}
                {{--                    <a href="{{ route('premium.smartSteps.index') }}" class="kt-menu__link ">--}}
                {{--                        <span class="kt-menu__link-icon">--}}
                {{--                            <i class="flaticon-dot"></i>--}}
                {{--                        </span>--}}
                {{--                        <span class="kt-menu__link-text text-uppercase">--}}
                {{--                            SMART STEPS--}}
                {{--                    </span>--}}
                {{--                    </a>--}}
                {{--                </li>--}}
                {{--                <li class="kt-menu__item">--}}
                {{--                    <a href="javascript:;" onclick="noAccessRemoved()" class="kt-menu__link ">--}}
                {{--                        <span class="kt-menu__link-icon">--}}
                {{--                            <i class="flaticon-dot"></i>--}}
                {{--                        </span>--}}
                {{--                        <span class="kt-menu__link-text text-uppercase">--}}
                {{--                            NAVIGATOR--}}
                {{--                    </span>--}}
                {{--                    </a>--}}
                {{--                </li>--}}
                {{--                <li class="kt-menu__item">--}}
                {{--                    <a href="javascript:;" onclick="noAccessRemoved()" class="kt-menu__link ">--}}
                {{--                        <span class="kt-menu__link-icon">--}}
                {{--                            <i class="flaticon-dot"></i>--}}
                {{--                        </span>--}}
                {{--                        <span class="kt-menu__link-text text-uppercase">--}}
                {{--                            ADVISOR--}}
                {{--                    </span>--}}
                {{--                    </a>--}}
                {{--                </li>--}}
            @endif
        </ul>
    </div>
</div>
<!-- end:: Aside Menu -->
