<!-- begin:: Aside Menu -->
<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
    <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
        <ul class="kt-menu__nav ">
            <li class="kt-menu__item {{ Request::is('parent/dashboard') ? 'kt-menu__item--active' : ''}}">
                <a href="{{ route('parent.dashboard') }}" class="kt-menu__link ">
                    <span class="kt-menu__link-icon">
                        <i class="flaticon-dashboard"></i>
                    </span>
                    <span class="kt-menu__link-text text-uppercase">
                        DASHBOARD
                    </span>
                </a>
            </li>
            <li class="kt-menu__item  kt-menu__item--submenu {{ Request::is('parent/family-info*') ? 'kt-menu__item--active kt-menu__item--open' : ''}}" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                    <span class="kt-menu__link-icon">
                         <i class="flaticon-parents"></i>
                    </span>
                    <span class="kt-menu__link-text text-uppercase">FAMILY INFO</span>
                    <i class="kt-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="kt-menu__submenu ">
                    <span class="kt-menu__arrow"></span>
                    <ul class="kt-menu__subnav">
                        <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link">
                                <span class="kt-menu__link-text text-uppercase">FAMILY INFO</span></span>
                        </li>
                        <li class="kt-menu__item {{ Request::is('parent/family-info/parents*') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                            <a href="{{ route('parent.familyInfo.parents') }}" class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="kt-menu__link-text text-uppercase">Parents</span>
                            </a>
                        </li>
                        <li class="kt-menu__item {{ Request::is('parent/family-info/students*') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                            <a href="{{ route('parent.familyInfo.students') }}" class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="kt-menu__link-text text-uppercase">Students</span>
                            </a>
                        </li>
                        <!-- Income -->
                        <li class="kt-menu__item kt-menu__item--submenu {{ Request::is('parent/family-info/income*') ? 'kt-menu__item--active kt-menu__item--open' : ''}}" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                            <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--line">
                                    <span></span>
                                </i>
                                <span class="kt-menu__link-text">INCOME</span>
                                <i class="kt-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="kt-menu__submenu" style="">
                                <span class="kt-menu__arrow"></span>
                                <ul class="kt-menu__subnav">
                                    <li class="kt-menu__item {{ Request::is('parent/family-info/income-info') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                                        <a href="{{ route('parent.familyInfo.incomeInfo') }}" class="kt-menu__link ">
                                            <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                <span></span>
                                            </i>
                                            <span class="kt-menu__link-text">INCOME</span>
                                        </a>
                                    </li>
                                    <li class="kt-menu__item {{ Request::is('parent/family-info/income-tax') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                                        <a href="{{ route('parent.familyInfo.incomeTax') }}" class="kt-menu__link ">
                                            <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                <span></span>
                                            </i>
                                            <span class="kt-menu__link-text">TAX</span>
                                        </a>
                                    </li>
                                    <li class="kt-menu__item {{ Request::is('parent/family-info/income-additional') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                                        <a href="{{ route('parent.familyInfo.incomeAdditional') }}" class="kt-menu__link ">
                                            <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                <span></span>
                                            </i>
                                            <span class="kt-menu__link-text">ADDITIONAL FINANCIAL</span>
                                        </a>
                                    </li>
                                    <li class="kt-menu__item {{ Request::is('parent/family-info/income-untaxed') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                                        <a href="{{ route('parent.familyInfo.incomeUntaxed') }}" class="kt-menu__link ">
                                            <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                <span></span>
                                            </i>
                                            <span class="kt-menu__link-text">UNTAXED</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!-- Assets -->
                        <li class="kt-menu__item kt-menu__item--submenu {{ Request::is('parent/family-info/assets*') ? 'kt-menu__item--active kt-menu__item--open' : ''}}" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                            <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--line">
                                    <span></span>
                                </i>
                                <span class="kt-menu__link-text">ASSETS</span>
                                <i class="kt-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="kt-menu__submenu" style="">
                                <span class="kt-menu__arrow"></span>
                                <ul class="kt-menu__subnav">
                                    <li class="kt-menu__item {{ Request::is('parent/family-info/assets-education') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                                        <a href="{{ route('parent.familyInfo.assetsEducation') }}" class="kt-menu__link ">
                                            <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                <span></span>
                                            </i>
                                            <span class="kt-menu__link-text">EDUCATION</span>
                                        </a>
                                    </li>
                                    <li class="kt-menu__item {{ Request::is('parent/family-info/assets-non-retirement') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                                        <a href="{{ route('parent.familyInfo.assetsNonRetirement') }}" class="kt-menu__link ">
                                            <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                <span></span>
                                            </i>
                                            <span class="kt-menu__link-text">NON-RETIREMENT</span>
                                        </a>
                                    </li>
                                    <li class="kt-menu__item {{ Request::is('parent/family-info/assets-retirement') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                                        <a href="{{ route('parent.familyInfo.assetsRetirement') }}" class="kt-menu__link ">
                                            <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                <span></span>
                                            </i>
                                            <span class="kt-menu__link-text">RETIREMENT</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="kt-menu__item {{ Request::is('parent/family-info/real-estate') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                            <a href="{{ route('parent.familyInfo.realEstate') }}" class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="kt-menu__link-text text-uppercase">Real Estate</span>
                            </a>
                        </li>
                        <li class="kt-menu__item {{ Request::is('parent/family-info/business') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                            <a href="{{ route('parent.familyInfo.business') }}" class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="kt-menu__link-text text-uppercase">Business Ownership</span>
                            </a>
                        </li>
                        <li class="kt-menu__item {{ Request::is('parent/family-info/insurance') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">
                            <a href="{{ route('parent.familyInfo.insurance') }}" class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="kt-menu__link-text text-uppercase">Insurance</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="kt-menu__section ">
                <h4 class="kt-menu__section-text">RESOURCES</h4>
            </li>
            <!-- Colleges -->
            <li class="kt-menu__item {{ Request::is('parent/colleges*') ? 'kt-menu__item--active' : ''}}">
                <a href="{{ route('parent.colleges') }}" class="kt-menu__link ">
                        <span class="kt-menu__link-icon">
                            <i class="flaticon-tools-and-utensils"></i>
                        </span>
                    <span class="kt-menu__link-text text-uppercase">
                        Colleges
                        </span>
                </a>
            </li>
            <!-- Majors -->
            <li class="kt-menu__item {{ Request::is('parent/majors*') ? 'kt-menu__item--active' : ''}}">
                <a href="{{ route('parent.majors') }}" class="kt-menu__link ">
                        <span class="kt-menu__link-icon">
                            <i class="flaticon-worker"></i>
                        </span>
                    <span class="kt-menu__link-text text-uppercase">Majors</span>
                </a>
            </li>
            <li class="kt-menu__item {{ Request::is('parent/efc') ? 'kt-menu__item--active' : ''}}">
                <a href="{{ route('parent.efc.result') }}" class="kt-menu__link ">
                    <span class="kt-menu__link-icon">
                        <i class="flaticon-dot"></i>
                    </span>
                    <span class="kt-menu__link-text text-uppercase">
                        EFC Calculator
                    </span>
                </a>
            </li>
            <li class="kt-menu__section ">
                <h4 class="kt-menu__section-text">SMARTTRACK® PREMIUM</h4>
            </li>
            <!-- Scholarship -->
            <li class="kt-menu__item {{ Request::is('parent/scholarships/merit*') ? 'kt-menu__item--active' : ''}}">
                <a href="{{ route('parent.scholarships.merit') }}" class="kt-menu__link ">
                    <span class="kt-menu__link-icon">
                        <i class="flaticon-give-money"></i>
                    </span>
                    <span class="kt-menu__link-text text-uppercase">
                        Merit Scholarships
                    </span>
                </a>
            </li>
{{--            <li class="kt-menu__item  kt-menu__item--submenu {{ Request::is('parent/scholarships/*') ? 'kt-menu__item--active kt-menu__item--open' : ''}}" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">--}}
{{--                <a href="javascript:" class="kt-menu__link kt-menu__toggle">--}}
{{--                    <span class="kt-menu__link-icon">--}}
{{--                         <i class="flaticon-give-money"></i>--}}
{{--                    </span>--}}
{{--                    <span class="kt-menu__link-text text-uppercase">Scholarships</span>--}}
{{--                    <i class="kt-menu__ver-arrow la la-angle-right"></i>--}}
{{--                </a>--}}
{{--                <div class="kt-menu__submenu ">--}}
{{--                    <span class="kt-menu__arrow"></span>--}}
{{--                    <ul class="kt-menu__subnav">--}}
{{--                        <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true">--}}
{{--                            <span class="kt-menu__link">--}}
{{--                                <span class="kt-menu__link-text text-uppercase">Scholarships</span>--}}
{{--                            </span>--}}
{{--                        </li>--}}
{{--                        <li class="kt-menu__item {{ Request::is('parent/scholarships/merit*') ? 'kt-menu__item--active' : ''}}" aria-haspopup="true">--}}
{{--                            <a href="{{ route('parent.scholarships.merit') }}" class="kt-menu__link ">--}}
{{--                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">--}}
{{--                                    <span></span>--}}
{{--                                </i>--}}
{{--                                <span class="kt-menu__link-text text-uppercase">Merit</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            </li>--}}
{{--            @if(isset($data['family']) && $data['family']->is_qualified)--}}
            <li class="kt-menu__item {{ Request::is('parent/scenarios*') ? 'kt-menu__item--active' : ''}}">
                <a href="{{ route('parent.scenarios') }}" class="kt-menu__link ">
                    <span class="kt-menu__link-icon">
                        <i class="flaticon-dot"></i>
                    </span>
                    <span class="kt-menu__link-text text-uppercase">
                        Quick Plan Scenarios
                    </span>
                </a>
            </li>
{{--            @endif--}}
            <li class="kt-menu__item {{ Request::is('parent/messages*') ? 'kt-menu__item--active' : ''}}">
                <a href="{{ route('parent.messages') }}" class="kt-menu__link ">
                    <span class="kt-menu__link-icon">
                        <i class="flaticon-dot"></i>
                    </span>
                    <span class="kt-menu__link-text text-uppercase">
                        MESSAGES
                    </span>
                </a>
            </li>
{{--            <li class="kt-menu__item {{ Request::is('*smart-steps') ? 'kt-menu__item--active' : ''}}">--}}
{{--                <a href="#" class="kt-menu__link ">--}}
{{--                    <span class="kt-menu__link-icon">--}}
{{--                        <i class="flaticon-dot"></i>--}}
{{--                    </span>--}}
{{--                    <span class="kt-menu__link-text text-uppercase">--}}
{{--                        SMART STEPS--}}
{{--                    </span>--}}
{{--                </a>--}}
{{--            </li>--}}
            <li class="kt-menu__item">
                <a href="{{ route('parent.colleges') }}" class="kt-menu__link ">
                    <span class="kt-menu__link-icon">
                        <i class="flaticon-dot"></i>
                    </span>
                    <span class="kt-menu__link-text text-uppercase">
                        COLLEGE NAVIGATOR
                    </span>
                </a>
            </li>
            
            <li class="kt-menu__item {{ Request::is('parent/form-1040*') ? 'kt-menu__item--active' : ''}}">
                <a href="{{ route('parent.form-1040') }}" class="kt-menu__link ">
                    <span class="kt-menu__link-icon">
                        <i class="flaticon-dot"></i>
                    </span>
                    <span class="kt-menu__link-text text-uppercase">
                        Form 1040
                    </span>
                </a>
            </li>
        </ul>
    </div>
</div>
<!-- end:: Aside Menu -->
