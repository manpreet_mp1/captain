<ul class="kt-menu__nav ">
    <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel kt-menu__item--active text-uppercase lead">
        @if(isset($data) && isset($data['family']) && !$data['family']->is_active)
            Welcome back {{ $ST_USER->first_name }}, you are currently using the free version of {{ config('app.shortName') }}.
            <a href="{{ route('parent.subscriptions') }}">&nbsp;Upgrade today and unlock our premium features!</a>
        @endif
    </li>
</ul>
