<!-- Entends the main template -->
@extends('themes.metronics.layout.(obsolete)fullwith-nosidebar')
@section('title', 'Financial Coach Assessment Response')
@section('pageHeader')
    Assessment Responses
@endsection
@section('content')
    @if($items->count() > 0)
        <div class="kt-content  kt-grid__item kt-grid__itekt--fluid" id="kt_content">
            @foreach($items as $item)
                @include('thirdParty.typeForm._response', ['item' => $item])
            @endforeach
        </div>
    @else
        <div class="alert alert-dark" role="alert" style="background: #282a3c; border: 1px solid #282a3c; color:#fff;">
            <div class="alert-text">No response found for this record.</div>
        </div>
    @endif
@endsection