<div class="kt-portlet kt-portlet--height-fluid">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title text-uppercase">
                Submitted at -  {{ $item['submitted_at'] }}
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        @foreach($item['questionAnswer'] as $questionAnswer)
            <div class="questionAnswer" style="padding: 10px 0;">
                <p class="question" style="padding: 0;margin: 0;font-weight: 600;"> {!! $questionAnswer['question'] !!} </p>
                <p class="answer" style="padding: 0;margin: 0;"> {!! $questionAnswer['answer'] !!} </p>
            </div>
        @endforeach
    </div>
</div>