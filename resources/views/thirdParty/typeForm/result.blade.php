<!-- Extends the main template -->
@extends('themes.metronics.layout.(obsolete)fullwith-nosidebar', ['maxWidth' => '100% !important'])
@section('title', 'All Assessment Results')
@section('pageHeader')
    All Assessment Results
@endsection
@section('content')
    <style type="text/css">
        .table-bordered, .table-bordered th, .table-bordered td {
            border: 1px solid #0a0302 !important;
        }
    </style>
    <div class="m-content  m-grid__item m-grid__item--fluid" id="m_content">
        <table class="table table-bordered">
            <tr class="bg-primary text-white text-uppercase">
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Qualified/Not Qualified</th>
                <th>Submission Date</th>
                <th>Data</th>
            </tr>
            @foreach($data['results'] as $result)
                <tr>
                    <td>{{ $result['first_name'] }}</td>
                    <td>{{ $result['last_name'] }}</td>
                    <td>{{ $result['email'] }}</td>
                    <td>{{ $result['qualified_not_qualified'] }}</td>
                    <td>{{ $result['created_at'] }}</td>
                    <td>
                        <ul>
                        @foreach($result['data'] as $key => $value)
                            <li> <strong>{{ $key }} : </strong>  {{ $value }} </li>
                        @endforeach
                        </ul>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection