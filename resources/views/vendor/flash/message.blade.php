@foreach (session('flash_notification', collect())->toArray() as $message)
    <?php
        /**
         * Decode Message Type and Class from the second param
         * of the flash notification
         * @var [type]
         */        
        $decodeMessage = $message['level'];
        $decodedMessage = explode(".", $decodeMessage);
        $type = $decodedMessage[0];
        $class = $decodedMessage[1];
    ?>
    
    <!-- If Alert is Sweet Alert -->
    @if($type == 'sweetalert')
        @include('flash::sweetalert', [
            'message' => $message['message'],
            'class' => $class
        ])
    @endif

    @if($type == 'toastr')
        @include('flash::toastr', [
            'message' => $message['message'],
            'class' => $class
        ])
    @endif
        
    <!--
    if ($message['overlay'])
        include('flash::modal', [
            'modalClass' => 'flash-modal',
            'title'      => $message['title'],
            'body'       => $message['message']
        ])
    else
        <div class="alert
                    alert-{ $message['level'] }}
                    { $message['important'] ? 'alert-important' : '' }}"
                    role="alert"
        >
            if ($message['important'])
                <button type="button"
                        class="close"
                        data-dismiss="alert"
                        aria-hidden="true"
                >&times;</button>
            endif

            !! $message['message'] !!}
        </div>
    endif
    -->
@endforeach

{{ session()->forget('flash_notification') }}
