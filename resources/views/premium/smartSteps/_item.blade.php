<!--begin:: Widgets/Blog-->
<div class="kt-portlet kt-portlet--height-fluid kt-widget19">
    <div class="kt-portlet__body kt-portlet__body--fit kt-portlet__body--unfill">
        <div class="kt-widget19__pic kt-portlet-fit--top kt-portlet-fit--sides"
             style="min-height: 300px; background-image: url( {{ config('app.cdn') }}images/smart-steps/{{$thumbnail}}); background-position: center;">
            <h3 class="kt-widget19__title kt-font-light">
                {{ $title }}
            </h3>
            <div class="kt-widget19__shadow"></div>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="kt-widget19__wrapper">
            <div class="kt-widget19__text">
                {!! $description !!}
            </div>
        </div>
    </div>
</div>
<!--end:: Widgets/Blog-->