@extends('themes.metronics.layout.fullwith-parent-default')
@section('pageTitle', 'Smart Steps')
@section('content')
    <div class="row">
        <div class="col-xl-4">
            @include('premium.smartSteps._item', [
                'thumbnail' => 'video-1.jpg',
                'title' => 'Introducing New Feature',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting scrambled a type specimen book text
                of the dummy text of the printing printing and typesetting industry scrambled dummy text of the
                printing.',
            ])
        </div>
        <div class="col-xl-4">
            @include('premium.smartSteps._item', [
                'thumbnail' => 'video-2.jpg',
                'title' => 'Introducing New Feature',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting scrambled a type specimen book text
                of the dummy text of the printing printing and typesetting industry scrambled dummy text of the
                printing.',
            ])
        </div>
        <div class="col-xl-4">
            @include('premium.smartSteps._item', [
                'thumbnail' => 'video-3.jpg',
                'title' => 'Introducing New Feature',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting scrambled a type specimen book text
                of the dummy text of the printing printing and typesetting industry scrambled dummy text of the
                printing.',
            ])
        </div>
        <div class="col-xl-4">
            @include('premium.smartSteps._item', [
                'thumbnail' => 'video-4.jpg',
                'title' => 'Introducing New Feature',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting scrambled a type specimen book text
                of the dummy text of the printing printing and typesetting industry scrambled dummy text of the
                printing.',
            ])
        </div>
        <div class="col-xl-4">
            @include('premium.smartSteps._item', [
                'thumbnail' => 'video-5.jpg',
                'title' => 'Introducing New Feature',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting scrambled a type specimen book text
                of the dummy text of the printing printing and typesetting industry scrambled dummy text of the
                printing.',
            ])
        </div>
        <div class="col-xl-4">
            @include('premium.smartSteps._item', [
                'thumbnail' => 'video-6.jpg',
                'title' => 'Introducing New Feature',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting scrambled a type specimen book text
                of the dummy text of the printing printing and typesetting industry scrambled dummy text of the
                printing.',
            ])
        </div>
        <div class="col-xl-4">
            @include('premium.smartSteps._item', [
                'thumbnail' => 'video-7.jpg',
                'title' => 'Introducing New Feature',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting scrambled a type specimen book text
                of the dummy text of the printing printing and typesetting industry scrambled dummy text of the
                printing.',
            ])
        </div>
        <div class="col-xl-4">
            @include('premium.smartSteps._item', [
                'thumbnail' => 'video-8.jpg',
                'title' => 'Introducing New Feature',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting scrambled a type specimen book text
                of the dummy text of the printing printing and typesetting industry scrambled dummy text of the
                printing.',
            ])
        </div>
        <div class="col-xl-4">
            @include('premium.smartSteps._item', [
                'thumbnail' => 'video-9.jpg',
                'title' => 'Introducing New Feature',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting scrambled a type specimen book text
                of the dummy text of the printing printing and typesetting industry scrambled dummy text of the
                printing.',
            ])
        </div>
        <div class="col-xl-4">
            @include('premium.smartSteps._item', [
                'thumbnail' => 'video-10.jpg',
                'title' => 'Introducing New Feature',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting scrambled a type specimen book text
                of the dummy text of the printing printing and typesetting industry scrambled dummy text of the
                printing.',
            ])
        </div>
        <div class="col-xl-4">
            @include('premium.smartSteps._item', [
                'thumbnail' => 'video-11.jpg',
                'title' => 'Introducing New Feature',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting scrambled a type specimen book text
                of the dummy text of the printing printing and typesetting industry scrambled dummy text of the
                printing.',
            ])
        </div>
        <div class="col-xl-4">
            @include('premium.smartSteps._item', [
                'thumbnail' => 'video-12.jpg',
                'title' => 'Introducing New Feature',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting scrambled a type specimen book text
                of the dummy text of the printing printing and typesetting industry scrambled dummy text of the
                printing.',
            ])
        </div>
        <div class="col-xl-4">
            @include('premium.smartSteps._item', [
                'thumbnail' => 'video-13.jpg',
                'title' => 'Introducing New Feature',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting scrambled a type specimen book text
                of the dummy text of the printing printing and typesetting industry scrambled dummy text of the
                printing.',
            ])
        </div>
        <div class="col-xl-4">
            @include('premium.smartSteps._item', [
                'thumbnail' => 'video-14.jpg',
                'title' => 'Introducing New Feature',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting scrambled a type specimen book text
                of the dummy text of the printing printing and typesetting industry scrambled dummy text of the
                printing.',
            ])
        </div>
        <div class="col-xl-4">
            @include('premium.smartSteps._item', [
                'thumbnail' => 'video-15.jpg',
                'title' => 'Introducing New Feature',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting scrambled a type specimen book text
                of the dummy text of the printing printing and typesetting industry scrambled dummy text of the
                printing.',
            ])
        </div>
    </div>
@endsection