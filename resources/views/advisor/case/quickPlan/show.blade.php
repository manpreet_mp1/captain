@extends("themes.metronics.layout.fullwith-advisor-with-case")
@include('elements.advisor.quickPlan.show', [
    'routeName'=> 'advisor.case.quickPlan',
    'additionalParam' => [
       'caseSlug' => $ST_ADVISOR_FAMILY->slug
    ]
])