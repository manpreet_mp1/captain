@extends("themes.metronics.layout.fullwith-advisor-with-case")
@include('elements.advisor.quickPlan.index', [
    'routeName'=> 'advisor.case.quickPlan.show',
    'additionalParam' => [
       'caseSlug' => $ST_ADVISOR_FAMILY->slug
    ]
])