@extends('themes.metronics.layout.fullwith-advisor-questionnaire')
@include('elements.parent.questionnaires.efc.other-family-member', [
    'additionalParam' => [
       'caseSlug' => $ST_ADVISOR_FAMILY->slug
    ]
])