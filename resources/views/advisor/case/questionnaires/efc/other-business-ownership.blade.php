@extends('themes.metronics.layout.fullwith-advisor-questionnaire')
@include('elements.parent.questionnaires.efc.other-business-ownership', [
    'additionalParam' => [
       'caseSlug' => $ST_ADVISOR_FAMILY->slug
    ]
])