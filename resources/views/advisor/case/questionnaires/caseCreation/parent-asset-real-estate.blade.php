@extends('themes.metronics.layout.fullwith-parent-case-creation')
@section('pageTitle', ' Assets Details')
@section('content')
    {!! Form::model($data['request'], [
           'method'=>'POST',
           'id'=> 'assessment-form',
           'class'=> 'kt-margin-t-30',
           'name'=>'assessment-form'
        ]) !!}
    @include('parent.questionnaires.caseCreation._assessment_step')
    <div class="row">
        <div class="col-md-12">
            @include('elements.forms._input_image', [
                'title' => 'Check all that apply regarding your real estate.',
                'type' => "checkbox",
                'required' => 'true',
                'column' => 'family[parent_asset_real_estate_options]',
                'class' => 'parent_asset_real_estate_options',
                'modelData' => $ST_FAMILY->parent_asset_real_estate_options,
                'values' => [
                    ['label' => 'Own a<br />Primary</br />Residence', 'value' => '1', 'image' => 'images/forms/RealEstateIconsPrimaryResidence.png'],
                    ['label' => 'Own a Secondary<br/>Residence/Vacation<br/>Home', 'value' => '2', 'image' => 'images/forms/RealEstateIconsSecondaryVacationHome.png'],
                    ['label' => 'Own<br/>Rental</br>Property', 'value' => '3', 'image' => 'images/forms/RealEstateIconsRentalProperties.png'],
                    ['label' => 'Own<br/>Commercial/Other<br/>Property', 'value' => '4', 'image' => 'images/forms/RealEstateIconsCommercialOtherProperty.png'],
                ]
            ])
        </div>
    </div>
    <div id="asset_primary">
        @include('elements.parent.questionnaires.common._divider')
        <div class="row">
            <div class="col-md-6">
                <h2 class="font-weight-light kt-margin-b-20">
                    What is the current estimated value of your <br/>primary residence?
                    <sup><span class="error font-s12 text-danger">*Required</span></sup>
                </h2>
                {!! Form::text('family[parent_asset_real_estate_primary_value_dollar]', null, [
                    'class'=>'form-control form-control-lg text-left',
                    'id' => 'parent_asset_real_estate_primary_value_dollar',
                    'required' => 'false',
                    "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"
                ]) !!}
            </div>
            <div class="col-md-6">
                <h2 class="font-weight-light kt-margin-b-20">
                    What is the total amount owed on your <br/>primary residence?
                    <sup><span class="error font-s12 text-danger">*Required</span></sup>
                </h2>
                {!! Form::text('family[parent_asset_real_estate_primary_owned_dollar]', null, [
                    'class'=>'form-control form-control-lg text-left',
                    'id' => 'parent_asset_real_estate_primary_owned_dollar',
                    'required' => 'false',
                    "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"
                ]) !!}
            </div>
        </div>
    </div>
    <div id="asset_secondary">
        @include('elements.parent.questionnaires.common._divider')
        <div class="row">
            <div class="col-md-6">
                <h2 class="font-weight-light kt-margin-b-20">
                    Total value of all other real estate property owned excluding your primary residence?
                    <sup><span class="error font-s12 text-danger">*Required</span></sup>
                </h2>
                {!! Form::text('family[parent_asset_real_estate_other_value_dollar]', null, [
                    'class'=>'form-control form-control-lg text-left',
                    'id' => 'parent_asset_real_estate_other_value_dollar',
                    'required' => 'false',
                    "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"
                ]) !!}
            </div>
            <div class="col-md-6">
                <h2 class="font-weight-light kt-margin-b-20">
                    Total amount owed on all of your other real estate property excluding your primary residence?
                    <sup><span class="error font-s12 text-danger">*Required</span></sup>
                </h2>
                {!! Form::text('family[parent_asset_real_estate_other_owned_dollar]', null, [
                    'class'=>'form-control form-control-lg text-left',
                    'id' => 'parent_asset_real_estate_other_owned_dollar',
                    'required' => 'false',
                    "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"
                ]) !!}
            </div>
        </div>
    </div>
    @include('elements.parent.questionnaires.common._continue_button')
    {!! Form::close() !!}
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            showHideDiv();
            $("input.parent_asset_real_estate_options").click(function () {
                showHideDiv();
            });
        });

        function showHideDiv() {
            hideAllAndMakeNotRequired();
            // Get All the checked values for checkbox
            let selectedAsset = [];
            $('.parent_asset_real_estate_options:checked').each(function () {
                selectedAsset.push($(this).val());
            });
            console.log(selectedAsset);
            if (selectedAsset.includes("1")) {
                showPrimary();
            }
            if (selectedAsset.includes("2")) {
                showSecondary();
            }
            if (selectedAsset.includes("3")) {
                showSecondary();
            }
            if (selectedAsset.includes("4")) {
                showSecondary();
            }
        }

        function showPrimary() {
            $("#asset_primary").show();
            $("#parent_asset_real_estate_primary_value_dollar").inputmask();
            $("#parent_asset_real_estate_primary_owned_dollar").inputmask();
            document.getElementById("parent_asset_real_estate_primary_value_dollar").required = true;
            document.getElementById("parent_asset_real_estate_primary_owned_dollar").required = true;
        }

        function showSecondary() {
            $("#asset_secondary").show();
            $("#parent_asset_real_estate_other_value_dollar").inputmask();
            $("#parent_asset_real_estate_other_owned_dollar").inputmask();
            document.getElementById("parent_asset_real_estate_other_value_dollar").required = true;
            document.getElementById("parent_asset_real_estate_other_owned_dollar").required = true;
        }

        function hideAllAndMakeNotRequired() {
            $("#asset_primary").hide();
            $("#asset_secondary").hide();
            document.getElementById("parent_asset_real_estate_primary_value_dollar").required = false;
            document.getElementById("parent_asset_real_estate_primary_owned_dollar").required = false;
            document.getElementById("parent_asset_real_estate_other_value_dollar").required = false;
            document.getElementById("parent_asset_real_estate_other_owned_dollar").required = false;
        }
    </script>
@endsection