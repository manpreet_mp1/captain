@extends('themes.metronics.layout.fullwith-parent-case-creation')
@section('pageTitle', ' Household Details')
@section('content')
    {!! Form::model($data['request'], [
           'method'=>'POST',
           'class'=> 'kt-margin-t-30',
           'id'=> 'assessment-form',
           'name'=>'assessment-form'
        ]) !!}
    @include('parent.questionnaires.caseCreation._assessment_step')
    <div class="row">
        <div class="col-md-12">
            <p class="lead text-brand text-primary">Ok {{ $ST_USER->first_name }}, you mentioned
                that you have {{ $ST_FAMILY->household_dependent_children_num }} child/children
                in your household.</p>
            <p class="lead text-brand text-primary">Please provide the details of all the children,
                step-children and adult children, that reside in your household and derive more than 50%
                of their support from you @if($ST_PARENT->is_parent_two === 1)
                    and {{ $ST_USER_TWO->first_name }}@endif.</p>
        </div>
    </div>
    <div class="row kt-margin-t-20">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <tr>
                        <th>
                            First Name <sup><span class="error font-s12 text-danger">*Required</span></sup>
                        </th>
                        <th>
                            Grade Level <sup><span class="error font-s12 text-danger">*Required</span></sup>
                        </th>
                        <th>
                            Relationship to {{ $ST_USER->first_name }} <sup><span
                                        class="error font-s12 text-danger">*Required</span></sup>
                        </th>
                        @if($ST_PARENT->is_parent_two === 1)
                            <th>
                                Relationship to {{ $ST_USER_TWO->first_name }} <sup><span
                                            class="error font-s12 text-danger">*Required</span></sup>
                            </th>
                        @endif
                    </tr>
                    @for($i=1; $i <= $ST_FAMILY->household_dependent_children_num; $i++)
                        <tr>
                            @include('parent.questionnaires.caseCreation._child_info_row', ['sequence' => $i])
                        </tr>
                    @endfor
                </table>
            </div>
        </div>
    </div>
    @include('elements.parent.questionnaires.common._continue_button')
    {!! Form::close() !!}
@endsection