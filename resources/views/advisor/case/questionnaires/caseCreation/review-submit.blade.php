@extends('themes.metronics.layout.fullwith-parent-case-creation')
@section('pageTitle', ' Household Details')
@section('content')
    {!! Form::model($data['request'], [
           'method'=>'POST',
           'id'=> 'assessment-form',
           'name'=>'assessment-form'
        ]) !!}
    @include('parent.questionnaires.caseCreation._assessment_step')
    <div class="row">
        <div class="col-md-12">
            <p class="text-center kt-margin-b-30">
                <img src="{{ config('app.cdn') }}images/financial-assessment.png"
                     alt="Financial Assessment" class="img-fluid">
            </p>
            <div class="kt-divider">
                <span></span>
                <span></span>
            </div>
            <h2 class="text-center kt-margin-t-50 kt-margin-b-30">
                Congrats {{ $ST_USER->first_name }}! You are now ready to hit that SUBMIT
                button.</h2>
            <p class="font-weight-light font-s18">You will be redirected to an online calendar where you
                will be
                able to schedule your initial call with a SMARTTRACK® College Funding Advisor.</p>
            <p class="font-weight-light font-s18">Your SMARTTRACK® College Funding Advisor will review
                and analyze your
                answers and then prepare an initial college funding game plan that is custom tailored
                for you and your
                student.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="kt-divider kt-margin-t-20 kt-margin-b-20">
                <span></span>
                <span></span>
            </div>
            <div class="text-center">
                {!! Form::button('SUBMIT', [
                    'name' => 'start',
                    'id' => 'start',
                    'class'=> 'btn btn-brand btn-elevate-air font-s18',
                    'type'=>'submit',
                    'value' => 'SUBMIT'
                ]) !!}
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection