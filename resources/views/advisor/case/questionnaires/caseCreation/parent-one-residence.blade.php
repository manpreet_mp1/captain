@extends('themes.metronics.layout.fullwith-parent-case-creation')
@section('pageTitle', 'Your Information')
@section('content')
    {!! Form::model($data['request'], [
           'method'=>'POST',
           'class'=> 'kt-margin-t-30',
           'id'=> 'assessment-form',
           'name'=>'assessment-form'
        ]) !!}
    @include('parent.questionnaires.caseCreation._assessment_step')
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="font-weight-light kt-margin-b-20">
                        What state do you consider your state of residency?
                        <sup><span class="error font-s12 text-danger">*Required</span></sup>
                    </h2>
                </div>
                <div class="col-md-6">
                    {!! Form::select('parentOne[state_id]', $data['states'], null, ['class'=>'form-control form-control-lg', 'required' => 'true']) !!}
                </div>
            </div>
        </div>
    </div>
    @include('elements.parent.questionnaires.common._continue_button')
    {!! Form::close() !!}
@endsection