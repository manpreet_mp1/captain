@extends('themes.metronics.layout.fullwith-parent-case-creation')
@section('pageTitle', ' Employment Information')
@section('content')
    {!! Form::model($data['request'], [
           'method'=>'POST',
           'class'=> 'kt-margin-t-30',
           'id'=> 'assessment-form',
           'name'=>'assessment-form'
        ]) !!}
    @include('parent.questionnaires.caseCreation._assessment_step')
    <div class="row">
        <div class="col-md-12">
            <h2 class="font-weight-light kt-margin-b-20">
                What is your employment status {{ $ST_USER->first_name }}?<sup><span
                            class="error font-s12 text-danger">*Required</span></sup>
            </h2>
        </div>
        <div class="col-md-6">
            {!! Form::select('parentOne[employment_status_id]', $data['employmentStatus'], null, ['class'=>'form-control form-control-lg', 'required' => 'true']) !!}
        </div>
        <div class="col-md-6">
        </div>
    </div>
    @if($ST_PARENT->is_parent_two === 1)
        @include('elements.parent.questionnaires.common._divider')
        <div class="row">
            <div class="col-md-12">
                <h2 class="font-weight-light kt-margin-b-20">
                    How about {{ $ST_USER_TWO->first_name }}’s employment status?<sup><span
                                class="error font-s12 text-danger">*Required</span></sup>
                </h2>
            </div>
            <div class="col-md-6">
                {!! Form::select("parentTwo[employment_status_id]", $data['employmentStatus'], null, ['class'=>'form-control form-control-lg', 'required' => 'true']) !!}
            </div>
            <div class="col-md-6">
            </div>
        </div>
    @endif
    @include('elements.parent.questionnaires.common._continue_button')
    {!! Form::close() !!}
@endsection