@extends('themes.metronics.layout.fullwith-parent-case-creation')
@section('pageTitle', ' Assets Details')
@section('content')
    {!! Form::model($data['request'], [
           'method'=>'POST',
           'id'=> 'assessment-form',
           'class'=> 'kt-margin-t-30',
           'name'=>'assessment-form'
        ]) !!}
    @include('parent.questionnaires.caseCreation._assessment_step')
    <div class="row">
        <div class="col-md-12">
            @php
                $parentTwoText = $ST_PARENT->is_parent_two === 1 ? " or {$ST_USER_TWO->first_name} " : "";
                $question = "Do you {$parentTwoText}have any money in investments accounts such as stocks, bonds, and mutual funds?";
            @endphp
            @include('elements.forms._input_image', [
                'title' => $question,
                'type' => "radio",
                'required' => 'true',
                'column' => 'family[is_parent_asset_investment_accounts]',
                'class' => 'is_parent_asset_investment_accounts',
                'modelData' => $ST_FAMILY->is_parent_asset_investment_accounts,
                'values' => [
                    ['label' => 'Yes', 'value' => '1', 'image' => 'images/forms/yes.png'],
                    ['label' => 'No', 'value' => '0', 'image' => 'images/forms/no.png'],
                ]
            ])
        </div>
    </div>
    <div id="asset_details">
        @include('elements.parent.questionnaires.common._divider')
        <div class="row">
            <div class="col-md-12">
                <p class="lead text-brand text-primary">
                    To be clear, we only are talking about investment accounts such as stocks, bonds, and mutual funds.
                    Not the money you have in your 401k or any other retirement accounts.
                </p>
                <h2 class="font-weight-light kt-margin-b-20">
                    What is the amount of all your investment accounts NOT including retirement accounts?
                    <sup><span class="error font-s12 text-danger">*Required</span></sup>
                </h2>
            </div>
            <div class="col-md-6">
                {!! Form::text("family[parent_asset_investment_accounts]", null, [
                    'class'=>'form-control form-control-lg text-left',
                    'id' => 'parent_asset_investment_accounts',
                    'required' => 'false',
                    "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"
                 ]) !!}
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
    @include('elements.parent.questionnaires.common._continue_button')
    {!! Form::close() !!}
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            showHideDiv();
            $("input.is_parent_asset_investment_accounts").click(function () {
                showHideDiv();
            });
        });

        function showHideDiv() {
            hideAllAndMakeNotRequired();
            // Get All the checked values for checkbox
            let selectedAsset = [];
            $('.is_parent_asset_investment_accounts:checked').each(function () {
                selectedAsset.push($(this).val());
            });
            if (selectedAsset.includes("1")) {
                $("#asset_details").show();
                $("#parent_asset_investment_accounts").inputmask();
                document.getElementById("parent_asset_investment_accounts").required = true;
            }
        }

        function hideAllAndMakeNotRequired() {
            $("#asset_details").hide();
            document.getElementById("parent_asset_investment_accounts").required = false;
        }
    </script>
@endsection