@extends('themes.metronics.layout.fullwith-parent-case-creation')
@section('pageTitle', 'Your Information')
@section('content')
    {!! Form::model($data['request'], [
           'method'=>'POST',
           'class'=> 'kt-margin-t-30',
           'id'=> 'assessment-form',
           'name'=>'assessment-form'
        ]) !!}
    @include('parent.questionnaires.caseCreation._assessment_step')
    <div class="row">
        <div class="col-md-12">
            @include('elements.forms._input_image', [
                'title' => "Welcome {$ST_USER->first_name}, what is your role in your household?",
                'required' => "required",
                'column' => 'role_id',
                'values' => $data['householdRole']
            ])
        </div>
    </div>
    @include('elements.parent.questionnaires.common._divider')
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="font-weight-light kt-margin-b-20">
                        What state do you consider your state of residency?
                        <sup><span class="error font-s12 text-danger">*Required</span></sup>
                    </h2>
                </div>
                <div class="col-md-12">
                    {!! Form::select('state_id', $data['states'], null, ['class'=>'form-control form-control-lg', 'required' => 'true']) !!}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="font-weight-light kt-margin-b-20">
                        What is your date of birth?
                        <sup><span class="error font-s12 text-danger">*Required</span></sup>
                    </h2>
                </div>
                <div class="col-md-12">
                    {!! Form::text('birth_date', null, [
                        'class'=>'form-control form-control-lg',
                        'id'  => 'birth_date',
                        "data-inputmask-inputformat" => "mm/dd/yyyy",
                        "data-inputmask-alias" => "datetime",
                        'required' => 'true'
                    ]) !!}
                </div>
            </div>
        </div>
    </div>
    @include('elements.parent.questionnaires.common._divider')
    <div class="row">
        <div class="col-md-12">
            @include('elements.forms._input_image', [
                'title' => "What is your marital status?",
                'required' => "required",
                'column' => 'marital_status_id',
                'values' => $data['maritalStatus'],
            ])
        </div>
    </div>
    @include('elements.parent.questionnaires.common._continue_button')
    {!! Form::close() !!}
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#birth_date").inputmask();
        });
    </script>
@endsection