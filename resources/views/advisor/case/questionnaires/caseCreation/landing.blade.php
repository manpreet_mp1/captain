@extends('themes.metronics.layout.fullwith-parent-case-creation')
@section('pageTitle', 'Getting Started')
@section('content')
    @include("parent.questionnaires.caseCreation.landing.{$data['landingSubBlade']}")
@endsection