@extends('themes.metronics.layout.fullwith-parent-case-creation')
@section('pageTitle', 'Spouse Information')
@section('content')
    {!! Form::model($data['request'], [
           'method'=>'POST',
           'class'=> 'kt-margin-t-30',
           'id'=> 'assessment-form',
           'name'=>'assessment-form'
        ]) !!}
    @include('parent.questionnaires.caseCreation._assessment_step')
    <div class="row">
        <div class="col-md-12">
            @include('elements.forms._input_image', [
                'title' => "What is your spouse's role",
                'required' => "required",
                'column' => 'parentTwo[role_id]',
                'values' => $data['householdRole']
            ])
        </div>
    </div>
    @include('elements.parent.questionnaires.common._continue_button')
    {!! Form::close() !!}
@endsection