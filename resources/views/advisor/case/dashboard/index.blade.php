@extends("themes.metronics.layout.fullwith-advisor-with-case")
@include('elements.parent.dashboard.index', [
    'routeName' => 'advisor.case.questionnaires.efc',
    'completeIncomeInformationRouteName' => 'advisor.case.familyInfo.incomeInfo',
    'completeIncomeTaxInformationRouteName' => 'advisor.case.familyInfo.incomeTax',
    'completeAdditionalIncomeInformationRouteName' => 'advisor.case.familyInfo.incomeAdditional',
    'completeUntaxedIncomeInformationRouteName' => 'advisor.case.familyInfo.incomeUntaxed',
    'completeEducationAssetsInformationRouteName' => 'advisor.case.familyInfo.assetsEducation',
    'completeNonRetirementAssetsInformationRouteName' => 'advisor.case.familyInfo.assetsNonRetirement',
    'completeRetirementAssetsInformationRouteName' => 'advisor.case.familyInfo.assetsRetirement',
    'completeRealEstateInformationRouteName' => 'advisor.case.familyInfo.realEstate',
    'completeBusinessFarmOwnershipInformationRouteName' => 'advisor.case.familyInfo.business',
    'completeInsuranceInformationRouteName' => 'advisor.case.familyInfo.insurance',
    'calculateEFCRouteName' => 'advisor.case.efc.result',
    'upgradeSubscriptionRoute' => 'advisor.case.subscriptions',
    'additionalParam' => [
        'caseSlug' => $ST_ADVISOR_FAMILY->slug
     ]
])
