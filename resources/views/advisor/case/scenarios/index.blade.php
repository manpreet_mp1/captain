@extends("themes.metronics.layout.fullwith-advisor-with-case")
@include('elements.parent.scenarios.index', [
    'routeName'=> 'advisor.case.scenarios.show',
    'additionalParam' => [
       'caseSlug' => $ST_ADVISOR_FAMILY->slug
    ]
])