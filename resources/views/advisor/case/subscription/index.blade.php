@extends("themes.metronics.layout.fullwith-advisor-with-case")
@include('elements.parent.subscription.index', [
    'routeName' => 'advisor.case.subscriptions.checkoutFixed',
    'checkoutPaymentPlanRouteName' => 'advisor.case.subscriptions.checkoutPaymentPlan',
    'showSubscriptionPlans' => 1,
    'additionalParam' => [
        'caseSlug' => $ST_ADVISOR_FAMILY->slug
    ]
])
