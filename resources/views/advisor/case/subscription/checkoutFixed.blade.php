@extends("themes.metronics.layout.fullwith-advisor-with-case")
@include('elements.parent.subscription.checkoutFixed', [
    'subscribeRouteName' => 'advisor.case.subscriptions.subscribeFixed',
    'couponRouteName' => 'advisor.case.subscriptions.checkoutFixed',
    'additionalParam' => [
        'caseSlug' => $ST_ADVISOR_FAMILY->slug
    ]
])
