@extends("themes.metronics.layout.fullwith-advisor-with-case")
@include('elements.parent.subscription.checkoutPaymentPlan', [
    'subscribeRouteName' => 'advisor.case.subscriptions.subscribePaymentPlan',
    'couponRouteName' => 'advisor.case.subscriptions.checkoutPaymentPlan',
    'additionalParam' => [
        'caseSlug' => $ST_ADVISOR_FAMILY->slug
    ]
])
