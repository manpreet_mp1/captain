@extends("themes.metronics.layout.fullwith-advisor-with-case")
@include('elements.parent.subscription.thankYou.smarttrack-advisor', [
    'routeName' => 'parent.subscriptions.checkout',
    'showSubscriptionPlans' => 0,
    'additionalParam' => [
        'caseSlug' => $ST_ADVISOR_FAMILY->slug
    ]
])
