@extends("themes.metronics.layout.fullwith-advisor-with-case")
@include('elements.parent.familyInfo.parents.index', [
    'editParentRouteName' => 'advisor.case.familyInfo.parents.edit',
    'addParentRouteName' => 'advisor.case.familyInfo.parents.add',
    'additionalParam' => [
       'caseSlug' => $ST_ADVISOR_FAMILY->slug
     ]
])
