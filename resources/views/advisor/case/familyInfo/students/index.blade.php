@extends("themes.metronics.layout.fullwith-advisor-with-case")
@include('elements.parent.familyInfo.students.index', [
    'editStudentRouteName' => 'advisor.case.familyInfo.students.edit',
    'addStudentRouteName' => 'advisor.case.familyInfo.students.add',
    'deleteStudentRouteName' => 'advisor.case.familyInfo.students.delete',
    'additionalParam' => [
       'caseSlug' => $ST_ADVISOR_FAMILY->slug
    ]
])
