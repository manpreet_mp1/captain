@extends("themes.metronics.layout.fullwith-advisor-with-case")
@include('elements.parent.familyInfo.assets.assetsNonRetirement', [
    'additionalParam' => [
       'caseSlug' => $ST_ADVISOR_FAMILY->slug
    ]
])