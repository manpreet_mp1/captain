@extends("themes.metronics.layout.fullwith-advisor-with-case")
@include('elements.parent.familyInfo.income.incomeUntaxed', [
    'additionalParam' => [
       'caseSlug' => $ST_ADVISOR_FAMILY->slug
    ]
])