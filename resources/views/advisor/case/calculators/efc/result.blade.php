@extends("themes.metronics.layout.fullwith-advisor-with-case")
@include('elements.parent.calculators.efc.result', [
    'routeName' => 'advisor.case.questionnaires.efc',
    'additionalParam' => [
        'caseSlug' => $ST_ADVISOR_FAMILY->slug
     ]
])