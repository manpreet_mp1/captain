<div class="kt-portlet">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                FAMILIES
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="kt-section kt-margin-b-0">
            <div class="kt-section__content">
                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable datatable" id="kt_table_1">
                    <thead>
                    <tr class="bg-dark text-uppercase">
                        <th class="text-white" title="Field #1"></th>
                        <th class="text-white" title="Field #2">Family Name</th>
                        <th class="text-white" title="Field #3"># of Parents</th>
                        <th class="text-white" title="Field #4"># of Children</th>
                        <th class="text-white" title="Field #6">Is Completed EFC?</th>
                        <th class="text-white" title="Field #5">Appointment Status</th>
                        <th class="text-white" title="Field #5">Created</th>
                        <th class="text-white" title="Field #7">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $i = 1; @endphp
                    @foreach($data['cases'] as $case)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $case->name }} Family</td>
                        <td>{{ $case->parents->count() }}</td>
                        <td>{{ $case->children->count() }}</td>
                        <td>{{ $case->is_completed_efc_text }}</td>
                        <td></td>
                        <td>{{ $case->created_at->toDateString() }}</td>
                        <td>
                            <a href="{{ route('advisor.cases.show', ['caseSlug' => $case->slug]) }}" class="btn btn-primary btn-sm">
                                Load
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>
