@extends('themes.metronics.layout.fullwith-advisor-no-case')
@section('pageTitle', 'Dashboard')
@section('subheader')
    <h3 class="kt-subheader__title">Dashboard</h3>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            @include('advisor.dashboard._header')
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @include('advisor.dashboard._cases')
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function() {
            $('.datatable').DataTable({
                responsive: true,
            });
        });
    </script>
@endsection