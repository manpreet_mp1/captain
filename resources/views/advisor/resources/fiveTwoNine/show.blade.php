@extends("themes.metronics.layout.fullwith-advisor-with-case")
@include('elements.resources.fiveTwoNine.show', [
    'routeName' => 'advisor.case.fiveTwoNine',
    'additionalParam' => [
        'caseSlug' => $ST_ADVISOR_FAMILY->slug
     ]
])