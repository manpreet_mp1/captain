@extends("themes.metronics.layout.fullwith-advisor-with-case")
@include('elements.resources.fiveTwoNine.index', [
    'routeName' => 'advisor.case.fiveTwoNine.show',
    'additionalParam' => [
        'caseSlug' => $ST_ADVISOR_FAMILY->slug
     ]
])