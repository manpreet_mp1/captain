@extends("themes.metronics.layout.fullwith-advisor-with-case")
@include('elements.resources.majors.heading', [
    'routeName' => 'advisor.case.majors.show',
    'additionalParam' => [
        'caseSlug' => $ST_ADVISOR_FAMILY->slug
     ]
])