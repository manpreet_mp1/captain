@extends("themes.metronics.layout.fullwith-advisor-with-case")
@include('elements.resources.majors.show', [
    'routeName' => 'advisor.case.majors',
    'collegeRouteName' => 'advisor.case.colleges.show',
    'saveRouteName' => 'advisor.case.majors.save',
    'additionalParam' => [
        'caseSlug' => $ST_ADVISOR_FAMILY->slug
     ]
])