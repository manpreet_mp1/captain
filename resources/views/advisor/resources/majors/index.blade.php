@extends("themes.metronics.layout.fullwith-advisor-with-case")
@include('elements.resources.majors.index', [
    'routeName' => 'advisor.case.majors.heading',
    'additionalParam' => [
        'caseSlug' => $ST_ADVISOR_FAMILY->slug
     ]
])