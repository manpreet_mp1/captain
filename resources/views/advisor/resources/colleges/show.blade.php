@extends("themes.metronics.layout.fullwith-advisor-with-case")
@include('elements.resources.colleges.show', [
    'routeName' => 'advisor.case.colleges',
    'saveRouteName' => 'advisor.case.colleges.save',
    'additionalParam' => [
        'caseSlug' => $ST_ADVISOR_FAMILY->slug
     ]
])
