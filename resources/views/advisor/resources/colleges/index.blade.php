@extends("themes.metronics.layout.fullwith-advisor-with-case")
@include('elements.resources.colleges.index', [
    'routeName' => 'advisor.case.colleges.show',
    'additionalParam' => [
        'caseSlug' => $ST_ADVISOR_FAMILY->slug
     ]
])