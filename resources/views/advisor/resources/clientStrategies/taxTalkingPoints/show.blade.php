@extends("themes.metronics.layout.fullwith-advisor-with-case")
@section('pageTitle', 'Strategies')
@section('subheader')
    <h3 class="kt-subheader__title">Tax Talking Points</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <span class="kt-subheader__desc">{{$data['category']->name}}</span>
@endsection
@section('subheader-toolbar')
    <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="btn btn-elevate btn-secondary btn-square">
        << Back
    </a>
@endsection
@section('content')
    <div class="col-xl-12">
        @if($data['records']->count() == 0)
            <div class="alert alert-warning fade show" role="alert">
                <div class="alert-icon"><i style="font-size: 1.2em;" class="fa fa-exclamation-triangle"></i></div>
                <div class="alert-text">Warning! No records Found.</div>
            </div>
        @else
            @foreach($data['records'] AS $record)
                <div class="kt-portlet">
                    <div class="kt-portlet__head bg-primary text-white">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{ $record->title }}
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <p class="text-uppercase">{!! $record->getStrategyRating($record->terms['category']) !!}</p>
                        {!! $record->post_content !!}
                    </div>
                </div>
            @endforeach
            {!! $data['records']->appends(Request::except('page'))->render() !!}
        @endif
    </div>
@endsection