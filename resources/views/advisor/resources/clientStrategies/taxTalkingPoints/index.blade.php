@extends("themes.metronics.layout.fullwith-advisor-with-case")
@section('pageTitle', 'Tax Talking Points')
@section('subheader')
    <h3 class="kt-subheader__title">Tax Talking Points</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <span class="kt-subheader__desc">View specific strategies focused on a client's 1040 Tax Form.</span>
@endsection
@section('content')
    @include('advisor.resources.clientStrategies.taxTalkingPoints._css')
    <div class="row">
        <div class="col-xl-12">
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <div id="pf1" class="pf w0 h0" data-page-no="1">
                        <div class="pc pc1 w0 h0"><img class="bi x0 y0 w1 h1" alt="" src="/images/app/taxTalkingPoint-bg1.png" />
                            <div class="t m0 x1 h2 y1 ff1 fs0 fc0 sc0 ls0 ws0">Form</div>
                            <div class="t m1 x2 h3 y2 ff2 fs1 fc0 sc0 ls0 ws0">1040 </div>
                            <div class="t m1 x3 h4 y3 ff1 fs2 fc0 sc0 ls0 ws0">Department of the Treasury—Internal Revenue Service </div>
                            <div class="t m1 x4 h5 y4 ff1 fs3 fc0 sc0 ls0 ws0">(99)</div>
                            <div class="t m1 x3 h6 y5 ff3 fs4 fc0 sc0 ls0 ws0">U.S. Individual Income Tax Return </div>
                            <div class="t m1 x5 h7 y6 ff4 fs5 fc0 sc0 ls0 ws0">20<span class="ff5">XX</span>
                            </div>
                            <div class="t m1 x6 h5 y7 ff1 fs3 fc0 sc0 ls0 ws0">OMB No. 1545-0074</div>
                            <div class="t m1 x7 h4 y8 ff1 fs2 fc0 sc0 ls0 ws0">IRS Use Only—Do not write or staple in this space. </div>
                            <div class="t m2 x8 h5 y9 ff1 fs3 fc0 sc0 ls0 ws0">For the year Jan. 1–Dec. 31, 20XX, or other tax year beginning <span class="_ _0"> </span>, 20XX, ending </div>
                            <div class="t m3 x9 h5 y9 ff1 fs3 fc0 sc0 ls0 ws0">, 20 </div>
                            <div class="t m1 xa h8 ya ff1 fs6 fc0 sc0 ls0 ws0">See separate instructions.</div>
                            <div class="t m1 x8 h5 yb ff1 fs3 fc0 sc0 ls0 ws0">Your first name and initial </div>
                            <div class="t m1 xb h5 yc ff1 fs3 fc0 sc0 ls0 ws0">Last name <span class="_ _1"> </span><span class="ff6">Your social security number </span>
                            </div>

                            <div class="t m1 x8 h5 yd ff1 fs3 fc0 sc0 ls0 ws0">If a joint return, spouse’s first name and initial </div>
                            <div class="t m1 xb h5 ye ff1 fs3 fc0 sc0 ls0 ws0">Last name </div>
                            <div class="t m3 xa h9 yf ff6 fs3 fc0 sc0 ls0 ws0">Spouse’s social security number </div>
                            <div class="t m1 xc ha y10 ff7 fs7 fc0 sc0 ls0 ws0">▲</div>
                            <div class="t m1 xd h5 y11 ff1 fs3 fc0 sc0 ls0 ws0">Make sure the SSN(s) above </div>
                            <div class="t m1 xe h5 y12 ff1 fs3 fc0 sc0 ls0 ws0">and on line 6c are correct.</div>
                            <div class="t m1 x8 h5 y13 ff1 fs3 fc0 sc0 ls0 ws0">Home address (number and street). If you have a P.O. box, see instructions. </div>
                            <div class="t m1 xf h5 y14 ff1 fs3 fc0 sc0 ls0 ws0">Apt. no. </div>
                            <div class="t m3 x8 h5 y15 ff1 fs3 fc0 sc0 ls0 ws0">City, town or post office, state, and ZIP code. If you have a foreign address, also complete spaces below (see instructions). </div>
                            <div class="t m1 x8 h5 y16 ff1 fs3 fc0 sc0 ls0 ws0">Foreign country name </div>
                            <div class="t m1 x10 h5 y17 ff1 fs3 fc0 sc0 ls0 ws0">Foreign province/state/county </div>
                            <div class="t m3 x11 h5 y16 ff1 fs3 fc0 sc0 ls0 ws0">Foreign postal code </div>
                            <div class="t m3 x12 h9 y18 ff6 fs3 fc0 sc0 ls0 ws0">Presidential Election Campaign</div>
                            <div class="t m4 xc hb y19 ff1 fs8 fc0 sc0 ls0 ws0">Check here if you, or your spouse if filing </div>
                            <div class="t m4 xc hb y1a ff1 fs8 fc0 sc0 ls0 ws0">jointly, want $3 to go to this fund. Checking </div>
                            <div class="t m4 xc hb y1b ff1 fs8 fc0 sc0 ls0 ws0">a box below will not change your tax or </div>
                            <div class="t m4 xc hb y1c ff1 fs8 fc0 sc0 ls0 ws0">refund. </div>
                            <div class="t m1 x13 hc y1d ff6 fs9 fc0 sc0 ls0 ws0">You </div>
                            <div class="t m5 x14 hc y1d ff6 fs9 fc0 sc0 ls0 ws0">Spouse</div>
                            <div class="t m1 x15 hd y1e ff6 fs7 fc0 sc0 ls0 ws0">Filing Status </div>
                            <div class="t m1 x15 h8 y1f ff1 fs6 fc0 sc0 ls0 ws0">Check only one </div>
                            <div class="t m1 x15 h8 y20 ff1 fs6 fc0 sc0 ls0 ws0">box. </div>
                            <div class="t m1 x16 he y21 ff6 fs6 fc0 sc0 ls0 ws0">1 </div>
                            <div class="t m1 x17 h8 y22 ff1 fs6 fc0 sc0 ls0 ws0">Single </div>
                            <div class="t m1 x16 he y23 ff6 fs6 fc0 sc0 ls0 ws0">2 </div>
                            <div class="t m1 x17 h8 y24 ff1 fs6 fc0 sc0 ls0 ws0">Married filing jointly (even if only one had income) </div>
                            <div class="t m1 x16 he y25 ff6 fs6 fc0 sc0 ls0 ws0">3 </div>
                            <div class="t m1 x17 h8 y26 ff1 fs6 fc0 sc0 ls0 ws0">Married filing separately. Enter spouse’s SSN above </div>
                            <div class="t m1 x17 h8 y27 ff1 fs6 fc0 sc0 ls0 ws0">and full name here. </div>
                            <div class="t m1 x18 hf y28 ff7 fs2 fc0 sc0 ls0 ws0">▶</div>
                            <div class="t m1 x19 he y22 ff6 fs6 fc0 sc0 ls0 ws0">4 </div>
                            <div class="t m1 x1a hb y29 ff1 fs8 fc0 sc0 ls0 ws0">Head of household (with qualifying person). (See instructions.) If </div>
                            <div class="t m1 x1a hb y2a ff1 fs8 fc0 sc0 ls0 ws0">the qualifying person is a child but not your dependent, enter this </div>
                            <div class="t m1 x1a hb y2b ff1 fs8 fc0 sc0 ls0 ws0">child’s name here. </div>
                            <div class="t m1 x1b hf y2c ff7 fs2 fc0 sc0 ls0 ws0">▶</div>
                            <div class="t m1 x19 he y27 ff6 fs6 fc0 sc0 ls0 ws0">5 </div>
                            <div class="t m1 x1a h10 y2d ff1 fsa fc0 sc0 ls0 ws0">Qualifying widow(er) with dependent child </div>
                            <div class="t m1 x15 hd y2e ff6 fs7 fc0 sc0 ls0 ws0">Exemptions </div>
                            <div class="t m1 x16 h8 y2f ff6 fs6 fc0 sc0 ls0 ws0">6a<span class="_ _2"> </span>Yourself. <span class="ff1">If someone can claim you as a dependent,</span> do not <span class="ff1">check box 6a <span class="_ _3"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.</span>
                            </div>
                            <div class="t m1 x1c he y30 ff6 fs6 fc0 sc0 ls0 ws0">b </div>
                            <div class="t m1 x1d h8 y31 ff6 fs6 fc0 sc0 ls0 ws0">Spouse <span class="_ _5"> </span><span class="ff1">.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.</span>
                            </div>
                            <div class="t m6 x1e h11 y32 ff8 fs1 fc0 sc0 ls0 ws0">}</div>
                            <div class="t m1 x1c he y33 ff6 fs6 fc0 sc0 ls0 ws0">c</div>
                            <div class="t m1 x1f he y34 ff6 fs6 fc0 sc0 ls0 ws0">Dependents: </div>
                            <div class="t m1 x20 h12 y35 ff9 fs3 fc0 sc0 ls0 ws0">(1) <span class="ffa"> First name </span> <span class="ffa">Last name </span>
                            </div>

                            <div class="t m1 x20s h12 y35s ff9 fs3 fc0 sc0 ls0 ws0 line6">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-6']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>

                            <div class="t m1 x21 h12 y36 ff9 fs3 fc0 sc0 ls0 ws0">(2) <span class="ffa">Dependent’s</span> </div>
                            <div class="t m1 x22 h12 y37 ffa fs3 fc0 sc0 ls0 ws0">social security number </div>
                            <div class="t m1 x23 h12 y36 ff9 fs3 fc0 sc0 ls0 ws0">(3)<span class="ffa"> Dependent’s </span> </div>
                            <div class="t m1 x24 h12 y37 ffa fs3 fc0 sc0 ls0 ws0">relationship to <span class="ff9"> </span>you </div>
                            <div class="t m1 x25 h12 y38 ff9 fs3 fc0 sc0 ls0 ws0">(4) <span class="ffb">✓</span> <span class="ffa">if child under age 17 </span>
                            </div>
                            <div class="t m1 x26 h12 y39 ffa fs3 fc0 sc0 ls0 ws0">qualifying for child tax credit <span class="ff9"> </span>
                            </div>
                            <div class="t m1 x27 h12 y3a ffa fs3 fc0 sc0 ls0 ws0">(see instructions) </div>
                            <div class="t m1 x15 h8 y3b ff1 fs6 fc0 sc0 ls0 ws0">If more than four </div>
                            <div class="t m1 x15 h8 y3c ff1 fs6 fc0 sc0 ls0 ws0">dependents, see </div>
                            <div class="t m1 x15 h8 y3d ff1 fs6 fc0 sc0 ls0 ws0">instructions and </div>
                            <div class="t m1 x15 h8 y3e ff1 fs6 fc0 sc0 ls0 ws0">check here </div>
                            <div class="t m1 x28 hf y3f ff7 fs2 fc0 sc0 ls0 ws0">▶</div>
                            <div class="t m1 x1c h8 y40 ff6 fs6 fc0 sc0 ls0 ws0">d <span class="_ _6"> </span><span class="ff1">Total number of exemptions claimed <span class="_ _7"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.</span>
                            </div>
                            <div class="t m1 x29 h9 y41 ff6 fs3 fc0 sc0 ls0 ws0">Boxes checked </div>
                            <div class="t m1 x29 h9 y42 ff6 fs3 fc0 sc0 ls0 ws0">on 6a and 6b</div>
                            <div class="t m1 x29 h9 y43 ff6 fs3 fc0 sc0 ls0 ws0">No. of children </div>
                            <div class="t m1 x29 h9 y44 ff6 fs3 fc0 sc0 ls0 ws0">on 6c who: </div>
                            <div class="t m1 x29 h5 y45 ff1 fs3 fc0 sc0 ls0 ws0">• <span class="ff6">lived with you </span>
                            </div>
                            <div class="t m1 x29 h13 y46 ff1 fsb fc0 sc0 ls0 ws0">• <span class="ff6">did not live with </span> </div>
                            <div class="t m1 x29 h13 y47 ff6 fsb fc0 sc0 ls0 ws0">you due to divorce <span class="ff1"> </span>
                            </div>
                            <div class="t m1 x29 h13 y48 ff6 fsb fc0 sc0 ls0 ws0">or separation<span class="ff1">  </span>
                            </div>
                            <div class="t m1 x29 h14 y49 ff6 fsb fc0 sc0 ls0 ws0">(see instructions)</div>
                            <div class="t m1 x29 h14 y4a ff6 fsb fc0 sc0 ls0 ws0">Dependents on 6c </div>
                            <div class="t m1 x29 h14 y4b ff6 fsb fc0 sc0 ls0 ws0">not entered above </div>
                            <div class="t m1 x29 h9 y4c ff6 fs3 fc0 sc0 ls0 ws0">Add numbers on </div>
                            <div class="t m1 x29 h9 y4d ff6 fs3 fc0 sc0 ls0 ws0">lines above </div>
                            <div class="t m1 x2a hf y4e ff7 fs2 fc0 sc0 ls0 ws0">▶</div>
                            <div class="t m1 x15 h15 y4f ff6 fs4 fc0 sc0 ls0 ws0">Income </div>
                            <div class="t m1 x15 h16 y50 ff6 fsc fc0 sc0 ls0 ws0">Attach Form(s) </div>
                            <div class="t m1 x15 h16 y51 ff6 fsc fc0 sc0 ls0 ws0">W-2 here. Also </div>
                            <div class="t m1 x15 h16 y52 ff6 fsc fc0 sc0 ls0 ws0">attach Forms </div>
                            <div class="t m1 x15 h16 y53 ff6 fsc fc0 sc0 ls0 ws0">W-2G and </div>
                            <div class="t m1 x15 h16 y54 ff6 fsc fc0 sc0 ls0 ws0">1099-R if tax </div>
                            <div class="t m1 x15 h16 y55 ff6 fsc fc0 sc0 ls0 ws0">was withheld. </div>
                            <div class="t m1 x15 h8 y56 ff1 fs6 fc0 sc0 ls0 ws0">If you did not </div>
                            <div class="t m1 x15 h8 y57 ff1 fs6 fc0 sc0 ls0 ws0">get a W-2, </div>
                            <div class="t m1 x15 h8 y58 ff1 fs6 fc0 sc0 ls0 ws0">see instructions. </div>
                            <div class="t m1 x16 h8 y59 ff6 fs6 fc0 sc0 ls0 ws0">7 <span class="_ _8"> </span><span class="ff1">Wages, salaries, tips, etc. Attach Form(s) W-2 <span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.</span>
                            </div>
                            <div class="t m1 x16s h8 y59s ff6 fs6 fc0 sc0 ls0 ws0 line7">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-7']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>
                            <div class="t m1 x2b he y5a ff6 fs6 fc0 sc0 ls0 ws0">7 </div>
                            <div class="t m1 x16 h8 y5b ff6 fs6 fc0 sc0 ls0 ws0">8a<span class="_ _4"> </span>Taxable <span class="ff1">interest. Attach Schedule B if required <span class="_ _9"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _a"> </span></span>8a </div>
                            <div class="t m1 x16 h8 y5b ff6 fs6 fc0 sc0 ls0 ws0 line8a">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-8a']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>

                            <div class="t m1 x1c h8 y5c ff6 fs6 fc0 sc0 ls0 ws0">b <span class="_ _6"> </span>Tax-exempt <span class="ff1">interest. </span>Do not <span class="ff1">include on line 8a <span class="_ _b"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _c"> </span></span>8b </div>
                            <div class="t m1 x1c h8 y5c ff6 fs6 fc0 sc0 ls0 ws0 line8b">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-8b']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>
                            <div class="t m1 x16 h8 y5d ff6 fs6 fc0 sc0 ls0 ws0">9 <span class="_ _d"></span>a<span class="_ _4"> </span><span class="ff1">Ordinary dividends. Attach Schedule B if required <span class="_ _e"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _a"> </span></span>9a </div>
                            <div class="t m1 x16 h8 y5d ff6 fs6 fc0 sc0 ls0 ws0 line9a">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-9a']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>

                            <div class="t m1 x1c h8 y5e ff6 fs6 fc0 sc0 ls0 ws0">b <span class="_ _6"> </span><span class="ff1">Qualified dividends <span class="_ _f"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _c"> </span></span>9b </div>
                            <div class="t m1 x2c h8 y5f ff6 fs6 fc0 sc0 ls0 ws0">10 <span class="_ _8"> </span><span class="ff1">Taxable refunds, credits, or offsets of state and local income taxes <span class="_ _6"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _a"> </span></span>10 </div>
                            <div class="t m1 x2c h8 y5f ff6 fs6 fc0 sc0 ls0 ws0 line10">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-10']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>

                            <div class="t m1 x2c h8 y60 ff6 fs6 fc0 sc0 ls0 ws0">11 <span class="_ _8"> </span><span class="ff1">Alimony received <span class="_ _10"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _a"> </span></span>11 </div>
                            <div class="t m1 x2c h8 y60 ff6 fs6 fc0 sc0 ls0 ws0 line11">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-11']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>

                            <div class="t m1 x2c h8 y61 ff6 fs6 fc0 sc0 ls0 ws0">12 <span class="_ _8"> </span><span class="ff1">Business income or (loss). Attach Schedule C or C-EZ <span class="_ _b"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _a"> </span></span>12 </div>
                            <div class="t m1 x2c h8 y61 ff6 fs6 fc0 sc0 ls0 ws0 line12">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-12']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>

                            <div class="t m1 x2c h8 y62 ff6 fs6 fc0 sc0 ls0 ws0">13 <span class="_ _8"> </span><span class="ff1">Capital gain or (loss). Attach Schedule D if required. If not required, check here  </span>
                            </div>
                            <div class="t m1 x2d hf y63 ff7 fs2 fc0 sc0 ls0 ws0">▶</div>
                            <div class="t m1 x2e he y62 ff6 fs6 fc0 sc0 ls0 ws0">13 </div>
                            <div class="t m1 x2e he y62 ff6 fs6 fc0 sc0 ls0 ws0 line13">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-13']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>

                            <div class="t m1 x2c h8 y64 ff6 fs6 fc0 sc0 ls0 ws0">14 <span class="_ _8"> </span><span class="ff1">Other gains or (losses). Attach Form 4797 <span class="_ _11"></span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _a"> </span></span>14 </div>
                            <div class="t m1 x2c h8 y64 ff6 fs6 fc0 sc0 ls0 ws0 line14">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-14']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>


                            <div class="t m1 x2c h8 y65 ff6 fs6 fc0 sc0 ls0 ws0">15 <span class="_ _d"></span>a<span class="_ _4"> </span><span class="ff1">IRA distributions <span class="_ _7"> </span>.<span class="_ _a"> </span></span>15a <span class="_ _12"> </span>b </div>

                            <div class="t m7 x2f h8 y65 ff1 fs6 fc0 sc0 ls0 ws0">Taxable amount <span class="_ _13"> </span>.<span class="_ _13"> </span>.<span class="_ _14"> </span>.</div>
                            <div class="t m1 x30 he y65 ff6 fs6 fc0 sc0 ls0 ws0">15b </div>
                            <div class="t m1 x30 he y65 ff6 fs6 fc0 sc0 ls0 ws0 line15">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-15']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>

                            <div class="t m1 x2c he y66 ff6 fs6 fc0 sc0 ls0 ws0">16 <span class="_ _d"></span>a</div>
                            <div class="t m8 x1f h8 y66 ff1 fs6 fc0 sc0 ls0 ws0">Pensions and annuities </div>
                            <div class="t m1 x31 he y66 ff6 fs6 fc0 sc0 ls0 ws0">16a <span class="_ _12"> </span>b </div>
                            <div class="t m7 x2f h8 y66 ff1 fs6 fc0 sc0 ls0 ws0">Taxable amount <span class="_ _13"> </span>.<span class="_ _14"> </span>.<span class="_ _13"> </span>.</div>
                            <div class="t m1 x30 he y66 ff6 fs6 fc0 sc0 ls0 ws0">16b </div>
                            <div class="t m1 x30 he y65 ff6 fs6 fc0 sc0 ls0 ws0 line16">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-16']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>

                            <div class="t m1 x2c h8 y67 ff6 fs6 fc0 sc0 ls0 ws0">17 <span class="_ _8"> </span><span class="ff1">Rental real estate, royalties, partnerships, S corporations, trusts, etc. Attach Schedule E <span class="_ _15"> </span></span><span class="fc1">17 </span>
                            </div>
                            <div class="t m1 x30 he y65 ff6 fs6 fc0 sc0 ls0 ws0 line17">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-17']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>

                            <div class="t m1 x2c h8 y68 ff6 fs6 fc0 sc0 ls0 ws0">18 <span class="_ _8"> </span><span class="ff1">Farm income or (loss). Attach Schedule F <span class="_ _3"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _a"> </span></span>18 </div>
                            <div class="t m1 x30 he y65 ff6 fs6 fc0 sc0 ls0 ws0 line18">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-18']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>

                            <div class="t m1 x2c h8 y69 ff6 fs6 fc0 sc0 ls0 ws0">19 <span class="_ _8"> </span><span class="ff1">Unemployment compensation<span class="_ _5"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _a"> </span></span>19 </div>
                            <div class="t m1 x30 he y65 ff6 fs6 fc0 sc0 ls0 ws0 line19">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-19']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>

                            <div class="t m1 x2c he y6a ff6 fs6 fc0 sc0 ls0 ws0">20 <span class="_ _d"></span>a</div>
                            <div class="t m8 x1f h8 y6a ff1 fs6 fc0 sc0 ls0 ws0">Social security benefits </div>
                            <div class="t m1 x31 he y6a ff6 fs6 fc0 sc0 ls0 ws0">20a <span class="_ _12"> </span>b </div>
                            <div class="t m7 x2f h8 y6a ff1 fs6 fc0 sc0 ls0 ws0">Taxable amount <span class="_ _13"> </span>.<span class="_ _14"> </span>.<span class="_ _13"> </span>.</div>
                            <div class="t m1 x30 he y6a ff6 fs6 fc0 sc0 ls0 ws0">20b </div>
                            <div class="t m1 x30 he y65 ff6 fs6 fc0 sc0 ls0 ws0 line20">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-20']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>

                            <div class="t m1 x2c h8 y6b ff6 fs6 fc0 sc0 ls0 ws0">21 <span class="_ _8"> </span><span class="ff1">Other income. List type and amount <span class="_ _16"> </span></span>21 </div>
                            <div class="t m1 x2c he y6c ff6 fs6 fc0 sc0 ls0 ws0">22 </div>
                            <div class="t m9 x1f h8 y6c ff1 fs6 fc0 sc0 ls0 ws0">Combine the amounts in the far right column for lines 7 through 21. This is your <span class="ff6">total income </span> </div>
                            <div class="t m9 x32 hf y6d ff7 fs2 fc0 sc0 ls0 ws0">▶</div>
                            <div class="t m1 x2e he y6e ff6 fs6 fc0 sc0 ls0 ws0">22 </div>
                            <div class="t m1 x15 h15 y6f ff6 fs4 fc0 sc0 ls0 ws0">Adjusted </div>
                            <div class="t m1 x15 h15 y70 ff6 fs4 fc0 sc0 ls0 ws0">Gross </div>
                            <div class="t m1 x15 h15 y71 ff6 fs4 fc0 sc0 ls0 ws0">Income </div>
                            <div class="t m1 x2c h8 y72 ff6 fs6 fc0 sc0 ls0 ws0">23 <span class="_ _8"> </span><span class="ff1">Educator expenses<span class="_ _17"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _c"> </span></span>23 </div>
                            <div class="t m1 x30 he y65 ff6 fs6 fc0 sc0 ls0 ws0 line23">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-23']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>

                            <div class="t m1 x2c he y73 ff6 fs6 fc0 sc0 ls0 ws0">24 </div>
                            <div class="t ma x1f h8 y74 ff1 fs6 fc0 sc0 ls0 ws0">Certain <span class="_ _11"></span>business expenses <span class="_ _11"></span>of <span class="_ _11"></span>reservists, performing <span class="_ _11"></span>artists, <span class="_ _11"></span>and </div>
                            <div class="t ma x1f h8 y75 ff1 fs6 fc0 sc0 ls0 ws0">fee-basis government officials. Attach Form 2106 or 2106-EZ </div>
                            <div class="t m1 x33 he y76 ff6 fs6 fc0 sc0 ls0 ws0">24 </div>
                            <div class="t m1 x2c h8 y77 ff6 fs6 fc0 sc0 ls0 ws0">25 <span class="_ _8"> </span><span class="ff1">Health savings account deduction. Attach Form 8889 <span class="_ _18"> </span>.<span class="_ _c"> </span></span>25 </div>
                            <div class="t m1 x30 he y65 ff6 fs6 fc0 sc0 ls0 ws0 line25">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-25']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>


                            <div class="t m1 x2c h8 y78 ff6 fs6 fc0 sc0 ls0 ws0">26 <span class="_ _8"> </span><span class="ff1">Moving expenses. Attach Form 3903 <span class="_ _19"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _c"> </span></span>26 </div>
                            <div class="t m1 x30 he y65 ff6 fs6 fc0 sc0 ls0 ws0 line26">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-26']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>

                            <div class="t m1 x2c he y79 ff6 fs6 fc0 sc0 ls0 ws0">27 </div>
                            <div class="t m9 x1f h8 y79 ff1 fs6 fc0 sc0 ls0 ws0">Deductible part of self-employment tax. Attach Schedule SE <span class="_ _10"> </span>.</div>
                            <div class="t m1 x33 he y79 ff6 fs6 fc0 sc0 ls0 ws0">27 </div>
                            <div class="t m1 x2c h8 y7a ff6 fs6 fc0 sc0 ls0 ws0">28 <span class="_ _8"> </span><span class="ff1">Self-employed SEP, SIMPLE, and qualified plans <span class="_ _17"> </span>.<span class="_ _4"> </span>.<span class="_ _1a"> </span></span>28 </div>
                            <div class="t m1 x30 he y65 ff6 fs6 fc0 sc0 ls0 ws0 line28">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-28']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>

                            <div class="t m1 x2c h8 y7b ff6 fs6 fc0 sc0 ls0 ws0">29 <span class="_ _8"> </span><span class="ff1">Self-employed health insurance deduction <span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _1a"> </span></span>29 </div>
                            <div class="t m1 x2c h8 y7c ff6 fs6 fc0 sc0 ls0 ws0">30 <span class="_ _8"> </span><span class="ff1">Penalty on early withdrawal of savings <span class="_ _11"></span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _1a"> </span></span>30 </div>
                            <div class="t m1 x2c he y7d ff6 fs6 fc0 sc0 ls0 ws0">31 <span class="_ _d"></span>a</div>
                            <div class="t m9 x1f h8 y7e ff1 fs6 fc0 sc0 ls0 ws0">Alimony paid </div>
                            <div class="t m1 x34 h8 y7e ff6 fs6 fc0 sc0 ls0 ws0">b <span class="ff1">Recipient’s SSN </span><span class="fs3"> </span>
                            </div>
                            <div class="t m1 x21 hf y7f ff7 fs2 fc0 sc0 ls0 ws0">▶</div>
                            <div class="t m1 x35 he y7d ff6 fs6 fc0 sc0 ls0 ws0">31a </div>
                            <div class="t m1 x30 he y65 ff6 fs6 fc0 sc0 ls0 ws0 line31">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-31']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>

                            <div class="t m1 x2c h8 y80 ff6 fs6 fc0 sc0 ls0 ws0">32 <span class="_ _8"> </span><span class="ff1">IRA deduction  .<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _c"> </span></span>32 </div>
                            <div class="t m1 x30 he y65 ff6 fs6 fc0 sc0 ls0 ws0 line32">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-32']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>

                            <div class="t m1 x2c h8 y81 ff6 fs6 fc0 sc0 ls0 ws0">33 <span class="_ _8"> </span><span class="ff1">Student loan interest deduction <span class="_ _11"></span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _1a"> </span></span>33 </div>
                            <div class="t m1 x30 he y65 ff6 fs6 fc0 sc0 ls0 ws0 line33">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-33']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>

                            <div class="t m1 x2c h8 y82 ff6 fs6 fc0 sc0 ls0 ws0">34 <span class="_ _8"> </span><span class="ff1">Tuition and fees. Attach Form 8917<span class="_ _1b"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _c"> </span></span>34 </div>
                            <div class="t m1 x2c he y83 ff6 fs6 fc0 sc0 ls0 ws0">35 </div>
                            <div class="t m7 x1f h8 y83 ff1 fs6 fc0 sc0 ls0 ws0">Domestic production activities deduction. Attach Form 8903 </div>
                            <div class="t m1 x33 he y83 ff6 fs6 fc0 sc0 ls0 ws0">35 </div>
                            <div class="t m1 x2c h8 y84 ff6 fs6 fc0 sc0 ls0 ws0">36 <span class="_ _8"> </span><span class="ff1">Add lines 23 through 35 <span class="_ _10"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _a"> </span></span>36 </div>
                            <div class="t m1 x30 he y65 ff6 fs6 fc0 sc0 ls0 ws0 line36">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-36']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>

                            <div class="t m1 x2c h8 y85 ff6 fs6 fc0 sc0 ls0 ws0">37 <span class="_ _8"> </span><span class="ff1">Subtract line 36 from line 22. This is your </span>adjusted gross income <span class="ff1"> <span class="_ _5"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _19"> </span> </span>
                            </div>
                            <div class="t m1 x36 hf y86 ff7 fs2 fc0 sc0 ls0 ws0">▶</div>
                            <div class="t m1 x2e he y87 ff6 fs6 fc0 sc0 ls0 ws0">37 </div>
                            <div class="t m1 x15 he y88 ff6 fs6 fc0 sc0 ls0 ws0">For Disclosure, Privacy Act, and Paperwork Reduction Act Notice, see separate instructions.</div>
                            <div class="t m1 x37 h5 y89 ff1 fs3 fc0 sc0 ls0 ws0">Cat. No. 11320B </div>
                            <div class="t m1 x38 hd y8a ff1 fs3 fc0 sc0 ls0 ws0">Form <span class="ff6 fs7">1040 </span> (20XX) </div>
                        </div>
                        <div class="pi" data-data='{"ctm":[1.673203,0.000000,0.000000,1.673203,0.000000,0.000000]}'></div>
                    </div>
                    <div id="pf2" class="pf w0 h0" data-page-no="2">
                        <div class="pc pc2 w0 h0"><img class="bi x0 y8b w2 h17" alt="" src="/images/app/taxTalkingPoint-bg2.png" />
                            <div class="t m1 x15 h18 y8c ff1 fsd fc0 sc0 ls0 ws0">Form 1040 (20XX) </div>
                            <div class="t m1 x39 hd y8d ff1 fs3 fc0 sc0 ls0 ws0">Page <span class="ff6 fs7">2 </span>
                            </div>
                            <div class="t m1 x15 hd y8e ff6 fs7 fc0 sc0 ls0 ws0">Tax and </div>
                            <div class="t m1 x15 h15 y8f ff6 fs7 fc0 sc0 ls0 ws0">Credits<span class="fs4"> </span>
                            </div>
                            <div class="t m1 x3a h8 y90 ff6 fs6 fc0 sc0 ls0 ws0">38 <span class="_ _8"> </span><span class="ff1">Amount from line 37 (adjusted gross income) <span class="_ _17"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _1c"> </span></span>38 </div>
                            <div class="t m1 x3a he y91 ff6 fs6 fc0 sc0 ls0 ws0">39a </div>
                            <div class="t m1 x3b h8 y92 ff1 fs6 fc0 sc0 ls0 ws0">Check </div>
                            <div class="t m1 x3b h8 y93 ff1 fs6 fc0 sc0 ls0 ws0">if: </div>
                            <div class="t m6 x3c h11 y94 ff8 fs1 fc0 sc0 ls0 ws0">{</div>
                            <div class="t m1 x3d h8 y91 ff6 fs6 fc0 sc0 ls0 ws0">You<span class="ff1"> were born before January 2, 1950, <span class="_ _1d"> </span>Blind.</span>
                            </div>
                            <div class="t m1 x3d h8 y95 ff6 fs6 fc0 sc0 ls0 ws0">Spouse<span class="ff1"> was born before January 2, 1950, <span class="_ _1e"> </span>Blind.</span>
                            </div>
                            <div class="t m6 x3e h11 y96 ff8 fs1 fc0 sc0 ls0 ws0">}</div>
                            <div class="t m9 x1a he y97 ff6 fs6 fc0 sc0 ls0 ws0">Total boxes </div>
                            <div class="t m9 x1a he y95 ff6 fs6 fc0 sc0 ls0 ws0">checked</div>
                            <div class="t m1 x3f h19 y95 ff6 fsd fc0 sc0 ls0 ws0"> </div>
                            <div class="t m1 x40 h4 y98 ff1 fs2 fc0 sc0 ls0 ws0"> <span class="ff7">▶</span>
                            </div>
                            <div class="t m1 x41 he y95 ff6 fs6 fc0 sc0 ls0 ws0">39a </div>
                            <div class="t m1 x42 he y99 ff6 fs6 fc0 sc0 ls0 ws0">b </div>
                            <div class="t mb x3b h1a y9a ff1 fse fc0 sc0 ls0 ws0">If your spouse itemizes on a separate return or you were a dual-status alien, check here<span class="fs2"> </span>
                            </div>
                            <div class="t mb x41 hf y9b ff7 fs2 fc0 sc0 ls0 ws0">▶</div>
                            <div class="t m1 x1b he y99 ff6 fs6 fc0 sc0 ls0 ws0">39b </div>
                            <div class="t m1 x15 h1b y9c ff6 fsa fc0 sc0 ls0 ws0">Standard </div>
                            <div class="t m1 x15 h1b y9d ff6 fsa fc0 sc0 ls0 ws0">Deduction </div>
                            <div class="t m1 x15 h1b y9e ff6 fsa fc0 sc0 ls0 ws0">for— </div>
                            <div class="t m1 x15 h10 y9f ff1 fsa fc0 sc0 ls0 ws0">• People who </div>
                            <div class="t m1 x15 h10 ya0 ff1 fsa fc0 sc0 ls0 ws0">check any </div>
                            <div class="t m1 x15 h10 ya1 ff1 fsa fc0 sc0 ls0 ws0">box on line </div>
                            <div class="t m1 x15 h10 ya2 ff1 fsa fc0 sc0 ls0 ws0">39a or 39b <span class="ff6">or </span>
                            </div>
                            <div class="t m1 x15 h10 ya3 ff1 fsa fc0 sc0 ls0 ws0">who can be </div>
                            <div class="t m1 x15 h10 ya4 ff1 fsa fc0 sc0 ls0 ws0">claimed as a </div>
                            <div class="t m1 x15 h10 ya5 ff1 fsa fc0 sc0 ls0 ws0">dependent, </div>
                            <div class="t m1 x15 h10 ya6 ff1 fsa fc0 sc0 ls0 ws0">see </div>
                            <div class="t m1 x15 h10 ya7 ff1 fsa fc0 sc0 ls0 ws0">instructions. </div>
                            <div class="t m1 x15 h10 ya8 ff1 fsa fc0 sc0 ls0 ws0">• All others: </div>
                            <div class="t m1 x15 h10 ya9 ff1 fsa fc0 sc0 ls0 ws0">Single or </div>
                            <div class="t m1 x15 h10 yaa ff1 fsa fc0 sc0 ls0 ws0">Married filing </div>
                            <div class="t m1 x15 h10 yab ff1 fsa fc0 sc0 ls0 ws0">separately, </div>
                            <div class="t m1 x15 h10 yac ff1 fsa fc0 sc0 ls0 ws0">$6,200 </div>
                            <div class="t m1 x15 h10 yad ff1 fsa fc0 sc0 ls0 ws0">Married filing </div>
                            <div class="t m1 x15 h10 yae ff1 fsa fc0 sc0 ls0 ws0">jointly or </div>
                            <div class="t m1 x15 h10 yaf ff1 fsa fc0 sc0 ls0 ws0">Qualifying </div>
                            <div class="t m1 x15 h10 yb0 ff1 fsa fc0 sc0 ls0 ws0">widow(er), </div>
                            <div class="t m1 x15 h10 yb1 ff1 fsa fc0 sc0 ls0 ws0">$12,400 </div>
                            <div class="t m1 x15 h10 yb2 ff1 fsa fc0 sc0 ls0 ws0">Head of </div>
                            <div class="t m1 x15 h10 yb3 ff1 fsa fc0 sc0 ls0 ws0">household, </div>
                            <div class="t m1 x15 h10 yb4 ff1 fsa fc0 sc0 ls0 ws0">$9,100 </div>
                            <div class="t m1 x3a h8 yb5 ff6 fs6 fc0 sc0 ls0 ws0">40 <span class="_ _8"> </span>Itemized deductions <span class="ff1">(from Schedule A) </span>or <span class="ff1">your </span>standard deduction <span class="ff1">(see left margin) <span class="_ _6"> </span>.<span class="_ _4"> </span>.<span class="_ _1c"> </span></span>40</div>
                            <div class="t m1 x30 he y65 ff6 fs6 fc0 sc0 ls0 ws0 line40">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-40']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>

                            <div class="t m1 x3a h8 yb6 ff6 fs6 fc0 sc0 ls0 ws0">41 <span class="_ _8"> </span><span class="ff1">Subtract line 40 from line 38 <span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _a"> </span></span>41 </div>
                            <div class="t m1 x3a h8 yb7 ff6 fs6 fc0 sc0 ls0 ws0">42 <span class="_ _8"> </span>Exemptions.<span class="ff1"> </span>
                            </div>
                            <div class="t mc x43 h8 yb7 ff1 fs6 fc0 sc0 ls0 ws0">If line 38 is $152,525 or less, multiply $3,950 by the number on line 6d. Otherwise, see instructions</div>
                            <div class="t m1 x2e he yb8 ff6 fs6 fc0 sc0 ls0 ws0">42 </div>
                            <div class="t m1 x30 he y65 ff6 fs6 fc0 sc0 ls0 ws0 line42">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-42']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>

                            <div class="t m1 x3a h8 yb9 ff6 fs6 fc0 sc0 ls0 ws0">43 <span class="_ _8"> </span>Taxable income. <span class="ff1">Subtract line 42 from line 41. If line 42 is more than line 41, enter -0- <span class="_ _1f"> </span>.<span class="_ _4"> </span>.<span class="_ _a"> </span></span>43 </div>
                            <div class="t m1 x3a he yba ff6 fs6 fc0 sc0 ls0 ws0">44 </div>
                            <div class="t m9 x3b h8 yba ff6 fs6 fc0 sc0 ls0 ws0">Tax <span class="ff1">(see instructions). Check if any from: </span>
                            </div>
                            <div class="t m1 x44 h8 yba ff6 fs6 fc0 sc0 ls0 ws0">a <span class="_ _c"> </span><span class="ff1">Form(s) 8814 <span class="_ _4"> </span></span>b </div>
                            <div class="t m1 x45 h8 ybb ff1 fs6 fc0 sc0 ls0 ws0">Form 4972<span class="_ _7"> </span><span class="ff6">c</span>
                            </div>
                            <div class="t m1 x2e he yba ff6 fs6 fc0 sc0 ls0 ws0">44 </div>
                            <div class="t m1 x3a h8 ybc ff6 fs6 fc0 sc0 ls0 ws0">45 <span class="_ _8"> </span>Alternative minimum tax <span class="ff1">(see instructions). Attach Form 6251 <span class="_ _1f"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _a"> </span></span>45 </div>
                            <div class="t m1 x30 he y65 ff6 fs6 fc0 sc0 ls0 ws0 line45">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-45']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>

                            <div class="t m1 x3a h8 ybd ff6 fs6 fc0 sc0 ls0 ws0">46 <span class="_ _8"> </span><span class="ff1">Excess advance premium tax credit repayment. Attach Form 8962 <span class="_ _20"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _a"> </span></span>46 </div>
                            <div class="t m1 x3a h8 ybe ff6 fs6 fc0 sc0 ls0 ws0">47 <span class="_ _8"> </span><span class="ff1">Add lines 44, 45, and 46 <span class="_ _21"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _14"> </span> </span>
                            </div>
                            <div class="t m1 x46 hf ybf ff7 fs2 fc0 sc0 ls0 ws0">▶</div>
                            <div class="t m1 x2e he yc0 ff6 fs6 fc0 sc0 ls0 ws0">47 </div>
                            <div class="t m1 x3a h8 yc1 ff6 fs6 fc0 sc0 ls0 ws0">48 <span class="_ _8"> </span><span class="ff1">Foreign tax credit. Attach Form 1116 if required <span class="_ _22"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _23"> </span></span>48 </div>
                            <div class="t m1 x3a he yc2 ff6 fs6 fc0 sc0 ls0 ws0">49 </div>
                            <div class="t m1 x3b h10 yc3 ff1 fsa fc0 sc0 ls0 ws0">Credit for child and dependent care expenses. Attach Form 2441 </div>
                            <div class="t m1 x33 he yc2 ff6 fs6 fc0 sc0 ls0 ws0">49 </div>
                            <div class="t m1 x30 he y65 ff6 fs6 fc0 sc0 ls0 ws0 line49">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-49']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>

                            <div class="t m1 x3a h8 yc4 ff6 fs6 fc0 sc0 ls0 ws0">50 <span class="_ _8"> </span><span class="ff1">Education credits from Form 8863, line 19 <span class="_ _f"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _23"> </span></span>50 </div>
                            <div class="t m1 x30 he y65 ff6 fs6 fc0 sc0 ls0 ws0 line50">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-50']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>

                            <div class="t m1 x3a he yc5 ff6 fs6 fc0 sc0 ls0 ws0">51 </div>
                            <div class="t md x3b h8 yc5 ff1 fs6 fc0 sc0 ls0 ws0">Retirement savings contributions credit. Attach Form 8880</div>
                            <div class="t m1 x33 he yc5 ff6 fs6 fc0 sc0 ls0 ws0">51 </div>
                            <div class="t m1 x3a he yc6 ff6 fs6 fc0 sc0 ls0 ws0">52 </div>
                            <div class="t me x3b h8 yc7 ff1 fs6 fc0 sc0 ls0 ws0">Child tax credit. Attach Schedule 8812, if required</div>
                            <div class="t md x47 h8 yc7 ff1 fs6 fc0 sc0 ls0 ws0">.<span class="_ _24"> </span>.<span class="_ _24"> </span>.</div>
                            <div class="t m1 x33 he yc6 ff6 fs6 fc0 sc0 ls0 ws0">52 </div>
                            <div class="t m1 x30 he y65 ff6 fs6 fc0 sc0 ls0 ws0 line52">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-52']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>

                            <div class="t m1 x3a h8 yc8 ff6 fs6 fc0 sc0 ls0 ws0">53 <span class="_ _8"> </span><span class="ff1">Residential energy credits. Attach Form 5695<span class="_ _21"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _23"> </span></span>53</div>
                            <div class="t m1 x3a he yc9 ff6 fs6 fc0 sc0 ls0 ws0">54 </div>
                            <div class="t mf x3b h8 yc9 ff1 fs6 fc0 sc0 ls0 ws0">Other credits from Form: </div>
                            <div class="t m1 x48 he yc9 ff6 fs6 fc0 sc0 ls0 ws0">a </div>
                            <div class="t m1 x49 h8 yca ff1 fs6 fc0 sc0 ls0 ws0">3800 </div>
                            <div class="t m1 x4a he yc9 ff6 fs6 fc0 sc0 ls0 ws0">b </div>
                            <div class="t m1 x21 h8 yca ff1 fs6 fc0 sc0 ls0 ws0">8801 </div>
                            <div class="t m1 x4b he yc9 ff6 fs6 fc0 sc0 ls0 ws0">c <span class="_ _25"> </span>54</div>
                            <div class="t m1 x3a h8 ycb ff6 fs6 fc0 sc0 ls0 ws0">55 <span class="_ _8"> </span><span class="ff1">Add lines 48 through 54. These are your </span>total credits <span class="_ _10"> </span><span class="ff1">.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.</span>
                            </div>
                            <div class="t m1 x2e he ycc ff6 fs6 fc0 sc0 ls0 ws0">55</div>
                            <div class="t m1 x3a he ycd ff6 fs6 fc0 sc0 ls0 ws0">56 </div>
                            <div class="t m1 x3b h8 yce ff1 fs6 fc0 sc0 ls0 ws0">Subtract line 55 from line 47. If line 55 is more than line 47, enter -0- <span class="_ _21"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _14"> </span> </div>
                            <div class="t m1 x46 hf ycf ff7 fs2 fc0 sc0 ls0 ws0">▶</div>
                            <div class="t m1 x2e he ycd ff6 fs6 fc0 sc0 ls0 ws0">56 </div>
                            <div class="t m1 x15 h15 yd0 ff6 fs4 fc0 sc0 ls0 ws0">Other </div>
                            <div class="t m1 x15 h15 yd1 ff6 fs4 fc0 sc0 ls0 ws0">Taxes </div>
                            <div class="t m1 x3a h8 yd2 ff6 fs6 fc0 sc0 ls0 ws0">57 <span class="_ _8"> </span><span class="ff1">Self-employment tax. Attach Schedule SE <span class="_ _f"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _a"> </span></span>57 </div>
                            <div class="t m1 x3a h8 yd3 ff6 fs6 fc0 sc0 ls0 ws0">58 <span class="_ _8"> </span><span class="ff1">Unreported social security and Medicare tax from Form: <span class="_ _7"> </span></span>a <span class="_ _15"> </span><span class="ff1">4137 <span class="_ _26"> </span></span>b <span class="_ _1a"> </span><span class="ff1">8919 <span class="_ _24"> </span>.<span class="_ _4"> </span>.<span class="_ _1c"> </span></span>58</div>
                            <div class="t m1 x3a he yd4 ff6 fs6 fc0 sc0 ls0 ws0">59</div>
                            <div class="t m1 x3b h1c yd5 ff1 fsf fc0 sc0 ls0 ws0">Additional tax on IRAs, other qualified retirement plans, etc. Attach Form 5329 if required <span class="_ _24"> </span>.<span class="_ _17"> </span>.</div>
                            <div class="t m1 x2e he yd4 ff6 fs6 fc0 sc0 ls0 ws0">59</div>
                            <div class="t m1 x3a h8 yd6 ff6 fs6 fc0 sc0 ls0 ws0">60 <span class="_ _d"></span>a<span class="_ _4"> </span><span class="ff1 fsf">Household employment taxes from Schedule H<span class="fs6"> <span class="_ _5"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _27"> </span></span>
	            </span>60a</div>
                            <div class="t m1 x42 he yd7 ff6 fs6 fc0 sc0 ls0 ws0">b</div>
                            <div class="t m1 x3b h8 yd8 ff1 fsf fc0 sc0 ls0 ws0">First-time homebuyer credit repayment. Attach Form 5405 if required<span class="fs6"> <span class="_ _19"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _28"> </span><span class="ff6">60b</span></span>
                            </div>
                            <div class="t m1 x3a he yd9 ff6 fs6 fc0 sc0 ls0 ws0">61</div>
                            <div class="t m1 x3b h1c yda ff1 fsf fc0 sc0 ls0 ws0">Health care: individual responsibility (see instructions)</div>
                            <div class="t m1 x4c h8 yd9 ff1 fs6 fc0 sc0 ls0 ws0">Full-year coverage</div>
                            <div class="t m1 x4d h1c yda ff1 fsf fc0 sc0 ls0 ws0">.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _17"> </span>.</div>
                            <div class="t m1 x2e he ydb ff6 fs6 fc0 sc0 ls0 ws0">61</div>
                            <div class="t m1 x3a h8 ydc ff6 fs6 fc0 sc0 ls0 ws0">62<span class="_ _29"> </span><span class="ff1">Taxes from:<span class="_ _e"> </span></span>a </div>
                            <div class="t m9 x4e h8 ydc ff1 fs6 fc0 sc0 ls0 ws0">Form 8959</div>
                            <div class="t m1 x4f he y5b ff6 fs6 fc0 sc0 ls0 ws0">b </div>
                            <div class="t m9 x22 h8 y5b ff1 fs6 fc0 sc0 ls0 ws0">Form 8960</div>
                            <div class="t m1 x50 he y5b ff6 fs6 fc0 sc0 ls0 ws0">c </div>
                            <div class="t m1 x51 h8 ydc ff1 fs6 fc0 sc0 ls0 ws0">Instructions;<span class="_ _6"> </span>enter code(s)</div>
                            <div class="t m1 x2e he y5b ff6 fs6 fc0 sc0 ls0 ws0">62</div>
                            <div class="t m1 x30 he y65 ff6 fs6 fc0 sc0 ls0 ws0 line62">
                                <a href="{{ route('advisor.case.clientStrategies.taxTalkingPoints.show', ['caseSlug' => $ST_ADVISOR_FAMILY->slug, 'cat_slug' => 'line-62']) }}" class="kt-badge kt-badge--inline kt-badge--brand font-s28 kt-padding-20">View Strategies</a>
                            </div>

                            <div class="t m1 x3a h8 ydd ff6 fs6 fc0 sc0 ls0 ws0">63<span class="_ _29"> </span><span class="ff1">Add lines 56 through 62. This is your </span>total tax <span class="_ _18"> </span><span class="ff1">.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _19"> </span></span> <span class="ff1"> </span>
                            </div>
                            <div class="t m1 x46 hf yde ff7 fs2 fc0 sc0 ls0 ws0">▶</div>
                            <div class="t m1 x2e he ydd ff6 fs6 fc0 sc0 ls0 ws0">63</div>
                            <div class="t m1 x15 hd ydf ff6 fs7 fc0 sc0 ls0 ws0">Payments </div>
                            <div class="t m1 x3a h8 ye0 ff6 fs6 fc0 sc0 ls0 ws0">64<span class="_ _29"> </span><span class="ff1">Federal income tax withheld from Forms W-2 and 1099 <span class="_ _2a"> </span>.<span class="_ _4"> </span>.</span>
                            </div>
                            <div class="t m1 x33 he ye1 ff6 fs6 fc0 sc0 ls0 ws0">64</div>
                            <div class="t m1 x3a he ye2 ff6 fs6 fc0 sc0 ls0 ws0">65</div>
                            <div class="t m7 x3b h8 y5e ff1 fs6 fc0 sc0 ls0 ws0">20XX estimated tax payments and amount applied from 2013 return </div>
                            <div class="t m1 x33 he y5e ff6 fs6 fc0 sc0 ls0 ws0">65</div>
                            <div class="t m1 x15 h10 ye3 ff1 fsa fc0 sc0 ls0 ws0">If you have a </div>
                            <div class="t m1 x15 h10 ye4 ff1 fsa fc0 sc0 ls0 ws0">qualifying </div>
                            <div class="t m1 x15 h10 ye5 ff1 fsa fc0 sc0 ls0 ws0">child, attach </div>
                            <div class="t m1 x15 h10 ye6 ff1 fsa fc0 sc0 ls0 ws0">Schedule EIC. </div>
                            <div class="t m1 x3a h8 ye7 ff6 fs6 fc0 sc0 ls0 ws0">66a<span class="_ _4"> </span>Earned income credit (EIC) <span class="_ _e"> </span><span class="ff1">.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.</span>
                            </div>
                            <div class="t m1 x35 he y5f ff6 fs6 fc0 sc0 ls0 ws0">66a </div>
                            <div class="t m1 x42 he ye8 ff6 fs6 fc0 sc0 ls0 ws0">b </div>
                            <div class="t m9 x3b h8 ye8 ff1 fs6 fc0 sc0 ls0 ws0">Nontaxable combat pay election </div>
                            <div class="t m1 x52 he ye8 ff6 fs6 fc0 sc0 ls0 ws0">66b </div>
                            <div class="t m1 x3a he ye9 ff6 fs6 fc0 sc0 ls0 ws0">67</div>
                            <div class="t m8 x3b h8 y61 ff1 fs6 fc0 sc0 ls0 ws0">Additional child tax credit. Attach Schedule 8812 <span class="_ _19"> </span>.<span class="_ _17"> </span>.<span class="_ _17"> </span>.<span class="_ _17"> </span>.<span class="_ _19"> </span> .</div>
                            <div class="t m1 x33 he yea ff6 fs6 fc0 sc0 ls0 ws0">67</div>
                            <div class="t m1 x3a he yeb ff6 fs6 fc0 sc0 ls0 ws0">68</div>
                            <div class="t m10 x3b h8 yec ff1 fs6 fc0 sc0 ls0 ws0">American opportunity credit from Form 8863, line 8 <span class="_ _2b"> </span>.<span class="_ _24"> </span>.<span class="_ _24"> </span>.</div>
                            <div class="t m1 x33 he yed ff6 fs6 fc0 sc0 ls0 ws0">68</div>
                            <div class="t m1 x3a he yee ff6 fs6 fc0 sc0 ls0 ws0">69</div>
                            <div class="t m10 x3b h8 yef ff1 fs6 fc0 sc0 ls0 ws0">Net premium tax credit. Attach Form 8962<span class="_ _10"> </span>.<span class="_ _24"> </span>.<span class="_ _2a"> </span>.<span class="_ _24"> </span>.<span class="_ _24"> </span>.<span class="_ _2a"> </span>.</div>
                            <div class="t m1 x33 he yf0 ff6 fs6 fc0 sc0 ls0 ws0">69</div>
                            <div class="t m1 x3a he yf1 ff6 fs6 fc0 sc0 ls0 ws0">70</div>
                            <div class="t m1 x3b h8 yf2 ff1 fs6 fc0 sc0 ls0 ws0">Amount paid with request for extension to file<span class="_ _17"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.</div>
                            <div class="t m1 x33 he y65 ff6 fs6 fc0 sc0 ls0 ws0">70</div>
                            <div class="t m1 x3a he yf3 ff6 fs6 fc0 sc0 ls0 ws0">71</div>
                            <div class="t m11 x3b h8 yf4 ff1 fsa fc0 sc0 ls0 ws0">Excess social security and tier 1 RRTA tax withheld<span class="fs6"> <span class="_ _6"> </span>.<span class="_ _2a"> </span>.<span class="_ _2a"> </span>.<span class="_ _2a"> </span>.</span>
                            </div>
                            <div class="t m1 x33 he yf4 ff6 fs6 fc0 sc0 ls0 ws0">71</div>
                            <div class="t m1 x3a h8 yf5 ff6 fs6 fc0 sc0 ls0 ws0">72<span class="_ _29"> </span><span class="ff1">Credit for federal tax on fuels. Attach Form 4136 <span class="_ _17"> </span>.<span class="_ _2a"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.</span>
                            </div>
                            <div class="t m1 x33 he yf6 ff6 fs6 fc0 sc0 ls0 ws0">72</div>
                            <div class="t m1 x3a he yf7 ff6 fs6 fc0 sc0 ls0 ws0">73</div>
                            <div class="t m12 x3b h8 y68 ff1 fs6 fc0 sc0 ls0 ws0">Credits from Form: </div>
                            <div class="t m1 x53 he y68 ff6 fs6 fc0 sc0 ls0 ws0">a </div>
                            <div class="t m4 x4e h8 y68 ff1 fs6 fc0 sc0 ls0 ws0">2439</div>
                            <div class="t m1 x54 he y68 ff6 fs6 fc0 sc0 ls0 ws0">b </div>
                            <div class="t m13 x55 h8 y68 ff1 fs6 fc0 sc0 ls0 ws0">Reserved</div>
                            <div class="t m1 x56 he y68 ff6 fs6 fc0 sc0 ls0 ws0">c </div>
                            <div class="t m14 x57 h8 y68 ff1 fs6 fc0 sc0 ls0 ws0">Reserved</div>
                            <div class="t m1 x58 he y68 ff6 fs6 fc0 sc0 ls0 ws0">d <span class="_ _2c"> </span>73</div>
                            <div class="t m1 x3a he yf8 ff6 fs6 fc0 sc0 ls0 ws0">74</div>
                            <div class="t m1 x3b h8 yf9 ff1 fs6 fc0 sc0 ls0 ws0">Add lines 64, 65, 66a, and 67 through 73. These are your <span class="ff6">total payments <span class="_ _1f"> </span></span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _19"> </span><span class="ff6">  </span> </div>
                            <div class="t m1 x46 hf yfa ff7 fs2 fc0 sc0 ls0 ws0">▶</div>
                            <div class="t m1 x2e he y69 ff6 fs6 fc0 sc0 ls0 ws0">74</div>
                            <div class="t m1 x15 hd yfb ff6 fs7 fc0 sc0 ls0 ws0">Refund </div>
                            <div class="t m1 x15 h5 yfc ff1 fs3 fc0 sc0 ls0 ws0">Direct deposit? </div>
                            <div class="t m1 x15 h5 yfd ff1 fs3 fc0 sc0 ls0 ws0">See </div>
                            <div class="t m1 x15 h5 yfe ff1 fs3 fc0 sc0 ls0 ws0">instructions. </div>
                            <div class="t m1 x3a he yff ff6 fs6 fc0 sc0 ls0 ws0">75</div>
                            <div class="t m15 x3b h8 y100 ff1 fs6 fc0 sc0 ls0 ws0">If line 74 is more than line 63, subtract line 63 from line 74. This is the amount you <span class="ff6">overpaid </span>
                            </div>
                            <div class="t m1 x2e he y100 ff6 fs6 fc0 sc0 ls0 ws0">75</div>
                            <div class="t m1 x3a h8 y101 ff6 fs6 fc0 sc0 ls0 ws0">76a<span class="_ _4"> </span><span class="ff1">Amount of line 75 you want </span>refunded to you.<span class="ff1"> If Form 8888 is attached, check here <span class="_ _6"> </span>.<span class="_ _5"> </span> </span>
                            </div>
                            <div class="t m1 xf hf y102 ff7 fs2 fc0 sc0 ls0 ws0">▶</div>
                            <div class="t m1 x30 he y103 ff6 fs6 fc0 sc0 ls0 ws0">76a</div>
                            <div class="t m1 x59 hf y104 ff7 fs2 fc0 sc0 ls0 ws0">▶ </div>
                            <div class="t m1 x59 hf y105 ff7 fs2 fc0 sc0 ls0 ws0">▶</div>
                            <div class="t m1 x42 h8 y106 ff6 fs6 fc0 sc0 ls0 ws0">b <span class="_ _6"> </span><span class="ff1">Routing number </span>
                            </div>
                            <div class="t m1 x5a hf y107 ff7 fs2 fc0 sc0 ls0 ws0">▶</div>
                            <div class="t m1 x5b h4 y106 ff1 fs2 fc0 sc0 ls0 ws0"> </div>
                            <div class="t m9 x23 h8 y106 ff6 fs6 fc0 sc0 ls0 ws0">c <span class="ff1">Type: <span class="_ _2d"> </span>Checking</span>
                            </div>
                            <div class="t m1 x5c h8 y106 ff1 fs6 fc0 sc0 ls0 ws0"> </div>
                            <div class="t m9 x5d h8 y106 ff1 fs6 fc0 sc0 ls0 ws0">Savings</div>
                            <div class="t m1 x42 h8 y108 ff6 fs6 fc0 sc0 ls0 ws0">d <span class="_ _6"> </span><span class="ff1">Account number</span>
                            </div>
                            <div class="t m1 x3a he y109 ff6 fs6 fc0 sc0 ls0 ws0">77</div>
                            <div class="t m16 x3b h8 y10a ff1 fs6 fc0 sc0 ls0 ws0">Amount of line 75 you want <span class="ff6">applied to your 20XX estimated tax</span>
                            </div>
                            <div class="t m1 x5b h8 y10a ff1 fs6 fc0 sc0 ls0 ws0"> </div>
                            <div class="t m1 x5e hf y10b ff7 fs2 fc0 sc0 ls0 ws0">▶</div>
                            <div class="t m1 x33 he y10c ff6 fs6 fc0 sc0 ls0 ws0">77</div>
                            <div class="t m1 x15 h1d y10d ff6 fs10 fc0 sc0 ls0 ws0">Amount </div>
                            <div class="t m1 x15 h1d y10e ff6 fs10 fc0 sc0 ls0 ws0">You Owe </div>
                            <div class="t m1 x3a h8 y10f ff6 fs6 fc0 sc0 ls0 ws0">78<span class="_ _29"> </span>Amount you owe.<span class="ff1"> Subtract line 74 from line 63. For details on how to pay, see instructions   </span> </div>
                            <div class="t m1 x5f hf y110 ff7 fs2 fc0 sc0 ls0 ws0">▶</div>
                            <div class="t m1 x2e he y111 ff6 fs6 fc0 sc0 ls0 ws0">78</div>
                            <div class="t m1 x3a h8 y112 ff6 fs6 fc0 sc0 ls0 ws0">79<span class="_ _29"> </span><span class="ff1">Estimated tax penalty (see instructions) <span class="_ _18"> </span>.<span class="_ _2a"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.<span class="_ _4"> </span>.</span>
                            </div>
                            <div class="t m1 x33 he y113 ff6 fs6 fc0 sc0 ls0 ws0">79</div>
                            <div class="t m17 x15 hd y114 ff6 fs7 fc0 sc0 ls0 ws0">Third Party </div>
                            <div class="t m17 x15 hd y115 ff6 fs7 fc0 sc0 ls0 ws0">Designee </div>
                            <div class="t m1 x42 h8 y116 ff1 fs6 fc0 sc0 ls0 ws0">Do you want to allow another person to discuss this return with the IRS (see instructions)?</div>
                            <div class="t m1 x60 h8 y78 ff6 fs6 fc0 sc0 ls0 ws0">Yes. <span class="ff1">Complete below. <span class="_ _1e"> </span></span>No</div>
                            <div class="t m1 x42 h5 y117 ff1 fs3 fc0 sc0 ls0 ws0">Designee’s </div>
                            <div class="t m1 x42 h5 y118 ff1 fs3 fc0 sc0 ls0 ws0">name </div>
                            <div class="t m1 x1c hf y119 ff7 fs2 fc0 sc0 ls0 ws0">▶</div>
                            <div class="t m1 x61 h5 y11a ff1 fs3 fc0 sc0 ls0 ws0">Phone </div>
                            <div class="t m1 x61 h5 y11b ff1 fs3 fc0 sc0 ls0 ws0">no. </div>
                            <div class="t m1 x62 hf y11c ff7 fs2 fc0 sc0 ls0 ws0">▶</div>
                            <div class="t m1 x41 h5 y11d ff1 fs3 fc0 sc0 ls0 ws0">Personal identification </div>
                            <div class="t m1 x41 h5 y11e ff1 fs3 fc0 sc0 ls0 ws0">number (PIN) </div>
                            <div class="t m1 x63 hf y11f ff7 fs2 fc0 sc0 ls0 ws0">▶</div>
                            <div class="t m1 x15 h15 y120 ff6 fs4 fc0 sc0 ls0 ws0">Sign </div>
                            <div class="t m1 x15 h15 y121 ff6 fs4 fc0 sc0 ls0 ws0">Here </div>
                            <div class="t m3 x15 h5 y122 ff1 fs3 fc0 sc0 ls0 ws0">Joint return? See </div>
                            <div class="t m3 x15 h5 y123 ff1 fs3 fc0 sc0 ls0 ws0">instructions. </div>
                            <div class="t m3 x15 h5 y124 ff1 fs3 fc0 sc0 ls0 ws0">Keep a copy for </div>
                            <div class="t m3 x15 h5 y125 ff1 fs3 fc0 sc0 ls0 ws0">your recor<span class="_ _2e"></span>ds. </div>
                            <div class="t m1 x42 h13 y126 ff1 fsb fc0 sc0 ls0 ws0">Under penalties of perjury, I declare that I have examined this return and accompanying schedules and statements, and to the best of my knowledge and belief, </div>
                            <div class="t m1 x42 h13 y127 ff1 fsb fc0 sc0 ls0 ws0">they are true, correct, and complete. Declaration of preparer (other than taxpayer) is based on all information of which preparer has any knowledge.</div>
                            <div class="t m1 x42 h5 y128 ff1 fs3 fc0 sc0 ls0 ws0">Your signature <span class="_ _2f"> </span>Date <span class="_ _30"> </span>Your occupation <span class="_ _31"> </span>Daytime phone number</div>
                            <div class="t m1 x42 h5 y129 ff1 fs3 fc0 sc0 ls0 ws0">Spouse’s signature. If a joint return, <span class="ff6">both </span>must sign. <span class="_ _8"> </span>Date </div>
                            <div class="t m18 x64 h1e y12a ffc fs4 fc0 sc0 ls0 ws0">▲</div>
                            <div class="t m1 x65 h5 y129 ff1 fs3 fc0 sc0 ls0 ws0">Spouse’s occupation</div>
                            <div class="t m19 x30 h5 y12b ff1 fs3 fc0 sc0 ls0 ws0">If the IRS sent you an Identity Protection </div>
                            <div class="t m19 x30 h5 y12c ff1 fs3 fc0 sc0 ls0 ws0">PIN, enter it </div>
                            <div class="t m19 x30 h5 y12d ff1 fs3 fc0 sc0 ls0 ws0">here (see inst.)</div>
                            <div class="t m1 x15 hd y12e ff6 fs7 fc0 sc0 ls0 ws0">Paid </div>
                            <div class="t m1 x15 hd y12f ff6 fs7 fc0 sc0 ls0 ws0">Preparer </div>
                            <div class="t m1 x15 hd y130 ff6 fs7 fc0 sc0 ls0 ws0">Use Only </div>
                            <div class="t m1 x42 h5 y131 ff1 fs3 fc0 sc0 ls0 ws0">Print/Type preparer’s name </div>
                            <div class="t m1 x49 h5 y132 ff1 fs3 fc0 sc0 ls0 ws0">Preparer’s signature <span class="_ _32"> </span>Date </div>
                            <div class="t m1 x30 h5 y133 ff1 fs3 fc0 sc0 ls0 ws0">Check if </div>
                            <div class="t m1 x30 h5 y134 ff1 fs3 fc0 sc0 ls0 ws0">self-employed</div>
                            <div class="t m1 x66 h5 y131 ff1 fs3 fc0 sc0 ls0 ws0"> PTIN</div>
                            <div class="t m1 x42 h5 y135 ff1 fs3 fc0 sc0 ls0 ws0">Firm’s name </div>
                            <div class="t m1 x67 h1f y136 ff7 fs11 fc0 sc0 ls0 ws0">▶</div>
                            <div class="t m1 x42 h5 y137 ff1 fs3 fc0 sc0 ls0 ws0">Firm’s address </div>
                            <div class="t m1 x67 h1f y138 ff7 fs11 fc0 sc0 ls0 ws0">▶</div>
                            <div class="t m1 x30 h5 y135 ff1 fs3 fc0 sc0 ls0 ws0">Firm&apos;s EIN </div>
                            <div class="t m1 x68 h1f y136 ff7 fs11 fc0 sc0 ls0 ws0">▶</div>
                            <div class="t m1 x30 h5 y137 ff1 fs3 fc0 sc0 ls0 ws0">Phone no. </div>
                            <div class="t m1 x15 h5 y139 ff1 fs3 fc0 sc0 ls0 ws0">www.irs.gov/form1040</div>
                            <div class="t m1 x69 hd y8a ff1 fs3 fc0 sc0 ls0 ws0">Form <span class="ff6 fs7">1040 </span>(20XX) </div>
                        </div>
                        <div class="pi" data-data='{"ctm":[1.673203,0.000000,0.000000,1.673203,0.000000,0.000000]}'></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection