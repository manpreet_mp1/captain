@extends("themes.metronics.layout.fullwith-advisor-with-case")
@section('pageTitle', 'Strategies')
@section('subheader')
    <h3 class="kt-subheader__title">Strategies</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <span class="kt-subheader__desc">View and filter strategies</span>
@endsection
@section('subheader-toolbar')
    <a href="{{ route('advisor.case.clientStrategies.strategies', ['caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}" class="btn btn-elevate btn-secondary btn-square">
        << Back
    </a>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            {{ $data['record']->title }}
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <p class="text-uppercase">{!! $data['record']->getStrategyRating($data['record']->terms['category']) !!}</p>
                    <p class="text-muted">
                        <i class="fa fa-tag"></i> {{ implode(", ", $data['record']->terms['category']) }}
                    </p>
                    {!! $data['record']->post_content !!}
                </div>
                <div class="kt-portlet__foot">
                    <div class="row align-items-center">
                        <div class="col-lg-6 m--valign-middle">
                            <a class="btn btn-brand btn-elevate kt-margin-r-10"
                               data-toggle="kt-popover" title=""
                               data-content="These are the strategies that will be added automatically whenever a new case is created."
                               data-original-title="Default Strategy"
                               href="#">
                                <i class="fa fa-check" aria-hidden="true"></i> Make Default Strategy
                            </a>
                            <a class="btn btn-brand btn-elevate"
                               href="#}">
                                <i class="fa fa-plus" aria-hidden="true"></i> Add to Case
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection