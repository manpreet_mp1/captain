{!! Form::model($data['request'], ['method' => 'get' ]) !!}
<div class="kt-portlet">
    <div class="kt-portlet__body">
        <div class="row">
            @foreach ($data['categories'] as $name => $slug)
                <div class="col-lg-12">
                    <div class="col-lg-12">
                        <h4 class="text-uppercase kt-margin-b-10">{{ $name }}</h4>
                    </div>
                    @foreach ($data['category'][$slug]['sub'] AS $subcategory)
                        <div class="col-lg-12">
                            <label class="kt-checkbox kt-checkbox--brand">
                                {!! Form::checkbox("{$slug}[]", $subcategory['slug'], null, []) !!} {!! \App\Models\Wordpress\Category::getRatingCategoryStars($subcategory['name'], $subcategory['slug']) !!}
                                <span></span>
                            </label>
                        </div>
                    @endforeach
                    <div class="kt-divider kt-margin-t-10 kt-margin-b-10">
                        <span></span>
                        <span></span>
                    </div>
                </div>

            @endforeach
            <div class="col-lg-6 kt-margin-t-20">
                {!! Form::button('<span class="fa fa-search"></span> Search', ['name' => 'search', 'class'=> 'btn btn-brand btn-elevate btn-block', 'type'=>'submit', 'value' => 'search' ]) !!}
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}