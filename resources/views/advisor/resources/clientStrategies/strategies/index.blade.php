@extends("themes.metronics.layout.fullwith-advisor-with-case")
@section('pageTitle', 'Strategies')
@section('subheader')
    <h3 class="kt-subheader__title">Strategies</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <span class="kt-subheader__desc">View and filter strategies</span>
@endsection
@section('content')
    <div class="row">
        <div class="col-xl-3">
            @include('advisor.resources.clientStrategies.strategies._search')
        </div>
        <div class="col-xl-9">
            @if($data['records']->count() == 0)
                <div class="alert alert-warning fade show" role="alert">
                    <div class="alert-icon"><i style="font-size: 1.2em;" class="fa fa-exclamation-triangle"></i></div>
                    <div class="alert-text">Warning! No records Found.</div>
                </div>
            @else
                @if(Request::input('search'))
                    <div class="alert alert-success fade show" role="alert">
                        <div class="alert-icon"><i style="font-size: 1.2em;" class="fa fa-th-list"></i></div>
                        <div class="alert-text">{{$data['records']->total()}} strategies(s) found.</div>
                    </div>
                @endif
                @foreach($data['records'] AS $record)
                    @include('advisor.resources.clientStrategies.strategies._item')
                @endforeach
                {!! $data['records']->appends(Request::except('page'))->render() !!}
            @endif
        </div>
    </div>
@endsection