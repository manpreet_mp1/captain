<div class="kt-portlet">
    <div class="kt-portlet__head bg-primary text-white">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                {{ $record->title }}
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <p class="text-uppercase">{!! $record->getStrategyRating($record->terms['category']) !!}</p>
        <p class="text-muted">
            <i class="fa fa-tag"></i> {{ implode(", ", $record->terms['category']) }}
        </p>
        <p>{{ $record->strategy_intro_text }}</p>
    </div>
    <div class="kt-portlet__foot">
        <div class="row align-items-center">
            <div class="col-lg-6 m--valign-middle">
                <a class="btn btn-brand btn-elevate" href="{{ route('advisor.case.clientStrategies.strategies.show', ['slug' => $record->slug, 'caseSlug' => $ST_ADVISOR_FAMILY->slug]) }}">
                    View Strategy
                </a>
            </div>
        </div>
    </div>
</div>