@extends("themes.metronics.layout.fullwith-advisor-with-case")
@include('elements.resources.scholarship.merit.show', [
    'routeName' => 'advisor.case.scholarships.merit',
    'additionalParam' => [
        'caseSlug' => $ST_ADVISOR_FAMILY->slug
     ]
])