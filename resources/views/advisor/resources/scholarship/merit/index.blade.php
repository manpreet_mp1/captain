@extends("themes.metronics.layout.fullwith-advisor-with-case")
@include('elements.resources.scholarship.merit.index', [
    'routeName' => 'advisor.case.scholarships.merit.show',
    'additionalParam' => [
        'caseSlug' => $ST_ADVISOR_FAMILY->slug
     ]
])