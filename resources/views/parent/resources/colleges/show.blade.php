@extends("themes.metronics.layout.fullwith-parent-default")
@include('elements.resources.colleges.show', [
    'routeName' => 'parent.colleges',
    'saveRouteName' => 'parent.colleges.save',
    'additionalParam' => []
])
