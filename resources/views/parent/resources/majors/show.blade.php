@extends("themes.metronics.layout.fullwith-parent-default")
@include('elements.resources.majors.show', [
    'routeName' => 'parent.majors',
    'collegeRouteName' => 'parent.colleges.show',
    'saveRouteName' => 'parent.majors.save',
    'additionalParam' => []
])