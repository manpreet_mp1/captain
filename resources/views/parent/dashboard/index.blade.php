@extends("themes.metronics.layout.fullwith-parent-default")
@include('elements.parent.dashboard.index', [
    'routeName' => 'parent.questionnaires.efc',
    'completeIncomeInformationRouteName' => 'parent.familyInfo.incomeInfo',
    'completeIncomeTaxInformationRouteName' => 'parent.familyInfo.incomeTax',
    'completeAdditionalIncomeInformationRouteName' => 'parent.familyInfo.incomeAdditional',
    'completeUntaxedIncomeInformationRouteName' => 'parent.familyInfo.incomeUntaxed',
    'completeEducationAssetsInformationRouteName' => 'parent.familyInfo.assetsEducation',
    'completeNonRetirementAssetsInformationRouteName' => 'parent.familyInfo.assetsNonRetirement',
    'completeRetirementAssetsInformationRouteName' => 'parent.familyInfo.assetsRetirement',
    'completeRealEstateInformationRouteName' => 'parent.familyInfo.realEstate',
    'completeBusinessFarmOwnershipInformationRouteName' => 'parent.familyInfo.business',
    'completeInsuranceInformationRouteName' => 'parent.familyInfo.insurance',
    'calculateEFCRouteName' => 'parent.efc.result',
    'upgradeSubscriptionRoute' => 'parent.subscriptions',
    'additionalParam' => [
     ]
])
