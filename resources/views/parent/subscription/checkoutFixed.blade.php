@extends("themes.metronics.layout.fullwith-parent-default")
@include('elements.parent.subscription.checkoutFixed', [
    'subscribeRouteName' => 'parent.subscriptions.subscribe',
    'couponRouteName' => 'parent.subscriptions.checkout',
    'additionalParam' => []
])
