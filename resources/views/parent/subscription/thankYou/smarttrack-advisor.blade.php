@extends("themes.metronics.layout.fullwith-parent-default")
@include('elements.parent.subscription.thankYou.smarttrack-advisor', [
    'routeName' => 'parent.subscriptions.checkout',
    'showSubscriptionPlans' => 0,
    'additionalParam' => []
])
