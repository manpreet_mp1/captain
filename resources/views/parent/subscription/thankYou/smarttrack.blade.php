@extends("themes.metronics.layout.fullwith-parent-default")
@include('elements.parent.subscription.thankYou.smarttrack', [
    'routeName' => 'parent.subscriptions.checkout',
    'showSubscriptionPlans' => 0,
    'additionalParam' => []
])
