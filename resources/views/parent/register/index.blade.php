<!-- Entends the main template -->
@extends('themes.metronics.layout.left-content-right-message')
@section('title', 'Register')
@section('content')
    <div class="kt-login__logo">
        <a href="{{ route('login') }}">
            <img src="{{ config('app.cdn') }}images/logo.png" style="max-width: 400px;" class="img-fluid" alt="logo">
        </a>
    </div>
    <div class="kt-login__signin">
        <div class="kt-login__head">
            <h3 class="kt-login__title">Create an account</h3>
        </div>
        @include('parent.register._register_form')
    </div>
@endsection

@section('contentBottom')
    <div class="kt-login__account">
            <span class="kt-login__account-msg">
			    Already have an account ?&nbsp;&nbsp;
            </span>
        <a href="{{ route('login') }}" id="kt_login_signup" class="kt-link kt-link--brand kt-login__account-link">
            Sign In
        </a>
    </div>
@endsection

@include('parent.register._register_content_right')

@section('scripts')
    <script>
        $(document).ready(function () {
            $("#mobile").inputmask("(999) 999-9999");
        });
    </script>
@endsection