<div class="kt-login-form">
    {!! Form::model($data, [
        'method'=>'POST',
        'class'=>'kt-login__forkt_removed kt-form kt--margin-top-25',
        'id'=> 'login-form',
        'name'=>'login-form'
    ]) !!}
    @if($advisor['status'])
        {!! Form::hidden('advisor_id', $advisor['data']->id) !!}
    @endif
    {!! Form::hidden('source') !!}
    <div class="form-group kt-margin-t-40">
        <div class="row">
            <div class="col-md-6">
                <div class="kt-input-icon kt-input-icon--left">
                    {!! Form::text('first_name', null, [
                        'class'=>'form-control form-control-lg',
                        'id'=>'first_name',
                        'placeholder'=> 'First Name',
                        'required'
                    ]) !!}
                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
                    <span><i class="fas fa-user"></i></span>
                </span>
                </div>
                @include('snippets.php.alert.field', ['field' => 'first_name'])
            </div>
            <div class="col-md-6 kt-margin-t-20-mobile kt-margin-t-0-tablet">
                <div class="kt-input-icon kt-input-icon--left">
                    {!! Form::text('last_name', null, [
                        'class'=>'form-control form-control-lg',
                        'id'=>'last_name',
                        'placeholder'=> 'Last Name',
                        'required'
                    ]) !!}
                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
                    <span><i class="fas fa-user"></i></span>
                </span>
                </div>
                @include('snippets.php.alert.field', ['field' => 'last_name'])
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="kt-input-icon kt-input-icon--left">
            {!! Form::tel('mobile', null, [
                'class'=>'form-control form-control-lg',
                'id'=>'mobile',
                'type' => 'tel',
                'placeholder'=> 'Mobile',
                'required'
            ]) !!}
            <span class="kt-input-icon__icon kt-input-icon__icon--left">
            <span><i class="fas fa-phone"></i></span>
        </span>
        </div>
        @include('snippets.php.alert.field', ['field' => 'mobile'])
    </div>
    <div class="form-group">
        <div class="kt-input-icon kt-input-icon--left">
            {!! Form::email('email', null, [
                'class'=>'form-control form-control-lg',
                'id'=>'email',
                'placeholder'=> 'Enter Email',
                'required'
                ]) !!}
            <span class="kt-input-icon__icon kt-input-icon__icon--left">
            <span>
                <i class="fas fa-envelope"></i>
            </span>
        </span>
        </div>
        @include('snippets.php.alert.field', ['field' => 'email'])
    </div>
    <div class="form-group">
        <div class="kt-input-icon kt-input-icon--left">
            {!! Form::password('password', [
                'class'=>'form-control form-control-lg',
                'id'=>'password',
                'placeholder'=> 'Enter Password',
                'required'
                ]) !!}
            <span class="kt-input-icon__icon kt-input-icon__icon--left">
            <span>
                <i class="fas fa-unlock"></i>
            </span>
        </span>
        </div>
        @include('snippets.php.alert.field', ['field' => 'password'])
    </div>
    <div class="kt-login__actions">
        {!! Form::button('<span><i class="fas fa-lock"></i><span>Register</span></span>', [
            'name' => 'register',
            'id' => 'register',
             'class'=> 'btn btn-brand btn-elevate btn-block',
            'type'=>'submit',
            'value' => 'Register'
            ]) !!}
    </div>
    <p class="font-s13 text-center text-muted kt-margin-t-20">
        By clicking Sign In, you agree to the {{ config('app.name') }} Terms of Service, Terms of Use
        and have read and acknowledge our Privacy Statement.
    </p>
    {!! Form::close() !!}
    <div class="kt-divider">
        <span></span>
        <span></span>
    </div>
    <div class="kt-section kt-margin-t-20">
        <div class="kt-section__content kt-section__content--solid">
            <p class="text-center font-s12 kt-margin-0">
                Invisible reCAPTCHA by <a href="https://policies.google.com/privacy?hl=en" target="_blank">Google
                    Privacy
                    Policy</a> and
                <a href="https://policies.google.com/terms?hl=en" target="_blank">Terms of Use</a>.
            </p>
        </div>
    </div>
</div>