<div class="row">
    <div class="col-md-12">
        <div class="kt-divider kt-margin-t-40 kt-margin-b-40">
            <span></span>
            <span></span>
        </div>
        <div class="text-right float-right">
            {!! Form::button('Continue', [
                'name' => 'start',
                'id' => 'start',
                'class'=> 'btn btn-brand btn-elevate-air font-s18',
                'type'=>'submit',
                'value' => 'Continue'
            ]) !!}
        </div>
    </div>
</div>