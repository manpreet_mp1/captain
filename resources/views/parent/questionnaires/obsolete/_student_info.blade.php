<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                @include('elements.forms._input_image', [
                    'title' => "Select all that apply to " . $familyChild->user->first_name,
                    'type' => "checkbox",
                    'required' => 'false',
                    'column' => 'financial_options',
                    'modelData' => $familyChild->financial_options,
                    'values' => [
                        ['label' => 'Filed Tax Return<br/>last year', 'value' => '1', 'image' => 'images/forms/taxReturn.png'],
                        ['label' => 'Have a checking or<br/>savings account', 'value' => '2', 'image' => 'images/forms/savingChecking.png'],
                        ['label' => 'Have investment<br/>accounts', 'value' => '3', 'image' => 'images/forms/investmentAccount.png'],
                        ['label' => 'Have trust accounts<br/>like UGMA/UTMA', 'value' => '4', 'image' => 'images/forms/trustAccount.png'],
                    ]
                ])
            </div>
        </div>
        <div id="taxReturn">
            @include('elements.parent.questionnaires.common._divider')
            <div class="row">
                <div class="col-md-12">
                    <h2 class="font-weight-light kt-margin-b-20">
                        If you have {{ $familyChild->user->first_name }}'s tax return handy, enter the reported amount
                        from either line 37 of the 1040, line 21 of the 1040A, or line 4 of the 1040EZ.
                        <sup><span class="error font-s12 text-danger">*Required</span></sup>
                    </h2>
                </div>
                <div class="col-md-6">
                    {!! Form::text('adjusted_gross_income', null, [
                        'class'=>'form-control form-control-lg text-left',
                        'id' => 'adjusted_gross_income',
                        'required' => 'false',
                        "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"
                    ]) !!}
                </div>
            </div>
        </div>
        <div id="savingChecking">
            @include('elements.parent.questionnaires.common._divider')
            <div class="row">
                <div class="col-md-12">
                    <p class="lead text-brand text-primary">
                        Enter the estimated balances of all checking and savings accounts
                        in {{ $familyChild->user->first_name }}'s name. Don't count
                        college savings accounts like 529's. We only need the checking and savings account balances that
                        are in {{ $familyChild->user->first_name }}'s name.
                    </p>
                    <h2 class="font-weight-light kt-margin-b-20">
                        Enter current checking and savings account balances.
                        <sup><span class="error font-s12 text-danger">*Required</span></sup>
                    </h2>
                </div>
                <div class="col-md-6">
                    {!! Form::text('cash_checking_saving', null, [
                        'class'=>'form-control form-control-lg text-left',
                        'id' => 'cash_checking_saving',
                        'required' => 'false',
                        "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"
                    ]) !!}
                </div>
            </div>
        </div>
        <div id="investmentAccount">
            @include('elements.parent.questionnaires.common._divider')
            <div class="row">
                <div class="col-md-12">
                    <p class="lead text-brand text-primary">
                        Alright, it looks like {{ $familyChild->user->first_name }} has money markets, mutual funds,
                        CDs, stocks, bonds, or other
                        securities in their name. We just need an estimated total rounded up to the nearest $1,000.
                    </p>
                    <h2 class="font-weight-light kt-margin-b-20">
                        Enter the total amount in the investment account(s).
                        <sup><span class="error font-s12 text-danger">*Required</span></sup>
                    </h2>
                </div>
                <div class="col-md-6">
                    {!! Form::text('investment_account', null, [
                        'class'=>'form-control form-control-lg text-left',
                        'id' => 'investment_account',
                        'required' => 'false',
                        "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"
                    ]) !!}
                </div>
            </div>
        </div>
        <div id="trustAccount">
            @include('elements.parent.questionnaires.common._divider')
            <div class="row">
                <div class="col-md-12">
                    <p class="lead text-brand text-primary">
                        Looks like {{ $familyChild->user->first_name }} is a lucky kid.
                    </p>
                    <h2 class="font-weight-light kt-margin-b-20">
                        Rounding up to the nearest $1,000, enter the estimated total balance of all trust accounts.
                        <sup><span class="error font-s12 text-danger">*Required</span></sup>
                    </h2>
                </div>
                <div class="col-md-6">
                    {!! Form::text('trust_account', null, [
                        'class'=>'form-control form-control-lg text-left',
                        'id' => 'trust_account',
                        'required' => 'false',
                        "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"
                    ]) !!}
                </div>
            </div>
        </div>
    </div>
</div>
@section('scripts')
    <script>
        $(document).ready(function () {
            showHideDiv();
            $("input.financial_options").click(function () {
                showHideDiv();
            });
        });

        function showHideDiv() {
            hideAllAndMakeNotRequired();
            // Get All the checked values for checkbox
            let selectedAsset = [];
            $('.financial_options:checked').each(function () {
                selectedAsset.push($(this).val());
            });
            if (selectedAsset.includes("1")) {
                $("#taxReturn").show();
                $("#adjusted_gross_income").inputmask();
                document.getElementById("adjusted_gross_income").required = true;
            }
            if (selectedAsset.includes("2")) {
                $("#savingChecking").show();
                $("#cash_checking_saving").inputmask();
                document.getElementById("cash_checking_saving").required = true;
            }
            if (selectedAsset.includes("3")) {
                $("#investmentAccount").show();
                $("#investment_account").inputmask();
                document.getElementById("investment_account").required = true;
            }
            if (selectedAsset.includes("4")) {
                $("#trustAccount").show();
                $("#trust_account").inputmask();
                document.getElementById("trust_account").required = true;
            }
        }

        function hideAllAndMakeNotRequired() {
            $("#taxReturn").hide();
            $("#savingChecking").hide();
            $("#investmentAccount").hide();
            $("#trustAccount").hide();
            document.getElementById("adjusted_gross_income").required = false;
            document.getElementById("cash_checking_saving").required = false;
            document.getElementById("investment_account").required = false;
            document.getElementById("trust_account").required = false;
        }
    </script>
@endsection