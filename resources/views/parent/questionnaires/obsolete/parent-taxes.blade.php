@extends('themes.metronics.layout.fullwith-parent-questionnaires')
@section('pageTitle', 'Income Tax')
@section('content')
    {!! Form::model($data['request'], [
           'method'=>'POST',
           'id'=> 'assessment-form',
           'class'=> 'kt-margin-t-30',
           'name'=>'assessment-form'
        ]) !!}
    <div class="row">
        <div class="col-md-12">
            <p class="lead text-brand text-primary">No one likes paying taxes.</p>
            <p class="lead text-brand text-primary"> A lot people that paid the IRS taxes last year can tell us how much
                they paid Uncle Sam down to the penny.</p>
            <p class="lead text-brand text-primary">Those that can’t, usually can estimate how much they paid with a
                fair degree of accuracy.</p>
            <p class="lead text-brand text-primary">If you don’t know, or can’t estimate how much your household paid
                the IRS, then look at line 61 on your 1040, or line 36 of your 1040A, or line 10 of your 1040EZ.</p>
            <p class="lead text-brand text-primary">Rounding to the nearest $1,000, enter how much your household paid
                the IRS.</p>
            <p class="lead text-brand text-primary">If you got a refund, just smile and enter 0.</p>
            <h2 class="font-weight-light kt-margin-b-20">
                How much did your household pay in taxes?
                <sup><span class="error font-s12 text-danger">*Required</span></sup>
            </h2>
        </div>
        <div class="col-md-6">
            {!! Form::text("family[income_tax]", $ST_FAMILY->income_tax, [
                    'class'=>'form-control form-control-lg text-left',
                    'id' => 'income_tax',
                    'required' => 'false',
                    "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"
                 ]) !!}
        </div>
        <div class="col-md-6">
        </div>
    </div>
    @include('elements.parent.questionnaires.common._continue_button')
    {!! Form::close() !!}
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $("#income_tax").inputmask();
        });
    </script>
@endsection