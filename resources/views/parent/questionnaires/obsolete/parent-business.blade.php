@extends('themes.metronics.layout.fullwith-parent-questionnaires')
@section('pageTitle', 'Business')
@section('content')
    {!! Form::model($data['request'], [
           'method'=>'POST',
           'id'=> 'assessment-form',
           'class'=> 'kt-margin-t-30',
           'name'=>'assessment-form'
        ]) !!}
    <div class="row">
        <div class="col-md-12">
            @php
                $parentTwoText = $ST_PARENT->is_parent_two === 1 ? " or {$ST_USER_TWO->first_name} " : "";
                $question = "Do you {$parentTwoText}own any businesses?";
            @endphp
            @include('elements.forms._input_image', [
                'title' => $question,
                'type' => "radio",
                'required' => 'true',
                'column' => 'family[is_own_business]',
                'class' => 'is_own_business',
                'modelData' => $ST_FAMILY->is_own_business,
                'values' => [
                    ['label' => 'Yes', 'value' => '1', 'image' => 'images/forms/yes.png'],
                    ['label' => 'No', 'value' => '0', 'image' => 'images/forms/no.png'],
                ]
            ])
        </div>
    </div>
    <div id="own_business_div">
        @include('elements.parent.questionnaires.common._divider')
        <div class="row">
            <div class="col-md-12">
                <h2 class="font-weight-light kt-margin-b-20">
                    What is the net worth of all the businesses you @if($ST_PARENT->is_parent_two === 1)
                        and/or {{$ST_USER_TWO->first_name}} @endif own?
                    <sup><span class="error font-s12 text-danger">*Required</span></sup>
                </h2>
            </div>
            <div class="col-md-6">
                {!! Form::text("family[net_worth_business_farm]", $ST_FAMILY->net_worth_business_farm, [
                    'class'=>'form-control form-control-lg text-left',
                    'id' => 'net_worth_business_farm',
                    'required' => 'false',
                    "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"
                 ]) !!}
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
    @include('elements.parent.questionnaires.common._continue_button')
    {!! Form::close() !!}
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            showHideDiv();
            $("input.is_own_business").click(function () {
                showHideDiv();
            });
        });

        function showHideDiv() {
            hideAllAndMakeNotRequired();
            // Get All the checked values for checkbox
            let selectedAsset = [];
            $('.is_own_business:checked').each(function () {
                selectedAsset.push($(this).val());
            });
            if (selectedAsset.includes("1")) {
                $("#own_business_div").show();
                $("#net_worth_business_farm").inputmask();
                document.getElementById("net_worth_business_farm").required = true;
            }
        }

        function hideAllAndMakeNotRequired() {
            $("#own_business_div").hide();
            document.getElementById("net_worth_business_farm").required = false;
        }
    </script>
@endsection