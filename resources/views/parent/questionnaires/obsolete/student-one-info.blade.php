@extends('themes.metronics.layout.fullwith-parent-questionnaires')
@section('pageTitle', $data['familyChildren'][0]->user->first_name . " Details")
@section('content')
    {!! Form::model($data['familyChildren'][0], [
           'method'=>'POST',
           'id'=> 'assessment-form',
           'class'=> 'kt-margin-t-30',
           'name'=>'assessment-form',
        ]) !!}
    {!! Form::hidden('student', $data['familyChildren'][0]->id) !!}
    @include('parent.questionnaires.efc._student_info', [
        'familyChild' => $data['familyChildren'][0]
     ])
    @include('elements.parent.questionnaires.common._continue_button')
    {!! Form::close() !!}
@endsection