@extends('themes.metronics.layout.fullwith-parent-questionnaires')
@section('pageTitle', 'Child Support')
@section('content')
    {!! Form::model($data['request'], [
           'method'=>'POST',
           'id'=> 'assessment-form',
           'class'=> 'kt-margin-t-30',
           'name'=>'assessment-form'
        ]) !!}
    {!! Form::hidden("family[assessment_current_step_num]", $data['formBlade']->order_seq) !!}
    <div class="row">
        <div class="col-md-12">
            @php
                $parentTwoText = $ST_PARENT->is_parent_two === 1 ? " or {$ST_USER_TWO->first_name} " : "";
                $question = "Do you {$parentTwoText}pay or receive child support?";
            @endphp
            @include('elements.forms._input_image', [
                'title' => $question,
                'type' => "checkbox",
                'required' => 'false',
                'column' => 'family[child_support_options]',
                'class' => 'child_support_options',
                'modelData' => $ST_FAMILY->child_support_options,
                'values' => [
                    ['label' => 'Pay', 'value' => '1', 'image' => 'images/forms/pay.png'],
                    ['label' => 'Receive', 'value' => '2', 'image' => 'images/forms/receive.png'],
                ]
            ])
        </div>
    </div>
    <div id="pay_child_support_div">
        @include('elements.parent.questionnaires.common._divider')
        <div class="row">
            <div class="col-md-12">
                <h2 class="font-weight-light kt-margin-b-20">
                    What is the total amount of child support paid for children outside the household?
                    <sup><span class="error font-s12 text-danger">*Required</span></sup>
                </h2>
            </div>
            <div class="col-md-6">
                {!! Form::text("family[pay_child_support]", $ST_FAMILY->pay_child_support, [
                    'class'=>'form-control form-control-lg text-left',
                    'id' => 'pay_child_support',
                    'required' => 'false',
                    "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"
                 ]) !!}
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
    <div id="receive_child_support_div">
        @include('elements.parent.questionnaires.common._divider')
        <div class="row">
            <div class="col-md-12">
                <h2 class="font-weight-light kt-margin-b-20">
                    What is the total amount of child support received for children in the household?
                    <sup><span class="error font-s12 text-danger">*Required</span></sup>
                </h2>
            </div>
            <div class="col-md-6">
                {!! Form::text("family[receive_child_support]", $ST_FAMILY->receive_child_support, [
                    'class'=>'form-control form-control-lg text-left',
                    'id' => 'receive_child_support',
                    'required' => 'false',
                    "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"
                 ]) !!}
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
    @include('elements.parent.questionnaires.common._continue_button')
    {!! Form::close() !!}
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            showHideDiv();
            $("input.child_support_options").click(function () {
                showHideDiv();
            });
        });

        function showHideDiv() {
            hideAllAndMakeNotRequired();
            // Get All the checked values for checkbox
            let selectedAsset = [];
            $('.child_support_options:checked').each(function () {
                selectedAsset.push($(this).val());
            });
            if (selectedAsset.includes("1")) {
                $("#pay_child_support_div").show();
                $("#pay_child_support").inputmask();
                document.getElementById("pay_child_support").required = true;
            }
            if (selectedAsset.includes("2")) {
                $("#receive_child_support_div").show();
                $("#receive_child_support").inputmask();
                document.getElementById("receive_child_support").required = true;
            }
        }

        function hideAllAndMakeNotRequired() {
            $("#pay_child_support_div").hide();
            $("#receive_child_support_div").hide();
            document.getElementById("pay_child_support").required = false;
            document.getElementById("receive_child_support").required = false;
        }
    </script>
@endsection