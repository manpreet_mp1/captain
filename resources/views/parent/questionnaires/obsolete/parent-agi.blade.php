@extends('themes.metronics.layout.fullwith-parent-questionnaires')
@section('pageTitle', 'Adjusted Gross Income')
@section('content')
    {!! Form::model($data['request'], [
           'method'=>'POST',
           'id'=> 'assessment-form',
           'class'=> 'kt-margin-t-30',
           'name'=>'assessment-form'
        ]) !!}
    <div class="row">
        <div class="col-md-12">
            <p class="lead text-brand text-primary"> We have your stated household income, but unfortunately
                the colleges don’t just take your word for it. </p>
            <p class="lead text-brand text-primary"> They will require that to see what you
                @if($ST_PARENT->is_parent_two === 1)and {{$ST_USER_TWO->first_name}} @endif actually reported to the IRS as your Adjusted Gross Income (AGI) so they can
                match it up with the IRS. </p>
            <p class="lead text-brand text-primary"> In other words, the colleges want to know the difference
                between what the household actually earned and the total annual amount that was deducted from your
                income or paycheck. </p>
            <p class="lead text-brand text-primary"> Regardless how you filed your tax return, all that is
                needed here is the total amount of AGI that your household reported to the IRS. </p>
            <p class="lead text-brand text-primary"> Look at line 37 on your 1040, or line 21 of your 1040A, or
                line 4 of your 1040EZ to get your AGI numbers. </p>
            <p class="lead text-brand text-primary"> If you don’t have access to last year’s tax returns or you
                have not filed, merely estimate what you think your total household Adjusted Gross Income is by rounding
                to the nearest $1,000. </p>
            <h2 class="font-weight-light kt-margin-b-20">
                What is the household Adjusted Gross Income (AGI)?
                <sup><span class="error font-s12 text-danger">*Required</span></sup>
            </h2>
        </div>
        <div class="col-md-6">
            {!! Form::text("family[adjusted_gross_income]", $ST_FAMILY->adjusted_gross_income, [
                    'class'=>'form-control form-control-lg text-left',
                    'id' => 'adjusted_gross_income',
                    'required' => 'false',
                    "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"
                 ]) !!}
        </div>
        <div class="col-md-6">
        </div>
    </div>
    @include('elements.parent.questionnaires.common._continue_button')
    {!! Form::close() !!}
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $("#adjusted_gross_income").inputmask();
        });
    </script>
@endsection