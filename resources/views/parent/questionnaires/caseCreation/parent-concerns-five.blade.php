@extends('themes.metronics.layout.fullwith-parent-case-creation')
@section('pageTitle', ' Feedback')
@section('content')
    {!! Form::model($data['request'], [
           'method'=>'POST',
           'id'=> 'assessment-form',
           'class'=> 'kt-margin-t-30',
           'name'=>'assessment-form'
        ]) !!}
    @include('parent.questionnaires.caseCreation._assessment_step')
    <div class="row">
        <div class="col-md-12">
            <p class="lead text-brand text-primary">
                On a scale from 1 - 10, with 10 being your highest concern.
            </p>
            <p class="lead text-brand text-primary font-weight-normal">
                Read this statement and rank its level of importance …
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @include('elements.forms._input_feedback', [
               'title' => "I feel like I don't have a clear plan to completely fund college for my kids.",
               'required' => "required",
               'textLeft' => "Not At All Concerned",
               'textRight' => "Extremely Concerned",
               'column' => 'family[parent_opinion_no_clear_plan]',
           ])
        </div>
    </div>
    @include('elements.parent.questionnaires.common._continue_button')
    {!! Form::close() !!}
@endsection