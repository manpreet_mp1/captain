@extends('themes.metronics.layout.fullwith-parent-case-creation')
@section('pageTitle', ' Feedback')
@section('content')
    {!! Form::model($data['request'], [
           'method'=>'POST',
           'id'=> 'assessment-form',
           'class'=> 'kt-margin-t-30',
           'name'=>'assessment-form'
        ]) !!}
    @include('parent.questionnaires.caseCreation._assessment_step')
    <div class="row">
        <div class="col-md-12">
            <p class="lead text-brand text-primary">
                Ok {{ $ST_USER->first_name }}, in this section we are going to ask your opinion on
                the most common financial concerns that families have pertaining to college.
            </p>
            <p class="lead text-brand text-primary">
                They are on a scale from 1 - 10, with 10 being your highest concern.
            </p>
            <p class="lead text-brand text-primary font-weight-normal">
                Read this statement and rank its level of importance …
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @include('elements.forms._input_feedback', [
               'title' => "I am concerned that I won't be able to stretch our household income or savings to pay for college.",
               'required' => "required",
               'textLeft' => "Not At All Concerned",
               'textRight' => "Extremely Concerned",
               'column' => 'family[parent_opinion_stretch_household_income]',
           ])
        </div>
    </div>
    @include('elements.parent.questionnaires.common._continue_button')
    {!! Form::close() !!}
@endsection