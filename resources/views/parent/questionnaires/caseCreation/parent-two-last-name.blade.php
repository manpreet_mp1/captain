@extends('themes.metronics.layout.fullwith-parent-case-creation')
@section('pageTitle', 'Spouse Information')
@section('content')
    {!! Form::model($data['request'], [
           'method'=>'POST',
           'class'=> 'kt-margin-t-30',
           'id'=> 'assessment-form',
           'name'=>'assessment-form'
        ]) !!}
    @include('parent.questionnaires.caseCreation._assessment_step')
    <div class="row">
        <div class="col-md-6">
            <h2 class="font-weight-light kt-margin-b-20">
                What is your spouse’s last name?<sup><span class="error font-s12 text-danger">*Required</span></sup>
            </h2>
            {!! Form::text('parentTwo[last_name]', null, ['class'=>'form-control form-control-lg', 'required' => 'true']) !!}
        </div>
    </div>
    @include('elements.parent.questionnaires.common._continue_button')
    {!! Form::close() !!}
@endsection