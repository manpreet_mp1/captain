@extends('themes.metronics.layout.fullwith-parent-case-creation')
@section('pageTitle', ' Assets Details')
@section('content')
    {!! Form::model($data['request'], [
           'method'=>'POST',
           'id'=> 'assessment-form',
           'class'=> 'kt-margin-t-30',
           'name'=>'assessment-form'
        ]) !!}
    @include('parent.questionnaires.caseCreation._assessment_step')
    <div class="row">
        <div class="col-md-12">
            <!-- @todo - Move this to DB -->
            @include('elements.forms._input_image', [
                'title' => 'Which types of assets do you have in your household? Select all that apply.',
                'type' => "checkbox",
                'required' => 'true',
                'column' => 'parent_type_of_asset',
                'modelData' => $ST_FAMILY->parent_type_of_asset,
                'values' => [
                    ['label' => 'Checking, Savings, CDs, and Money Market Accounts.', 'value' => '1', 'image' => 'images/forms/CheckinSavingsAsset.png'],
                    ['label' => '529s, Coverdells, <br/>and Educational IRAs', 'value' => '2', 'image' => 'images/forms/529CoverdellsAsset.png'],
                    ['label' => 'Stocks, <br/>Bonds, <br/>and Mutual Funds', 'value' => '3', 'image' => 'images/forms/StocksBondsAsset.png'],
                    ['label' => '401k, 403b, IRA, Roth IRA, or Keough Accounts', 'value' => '4', 'image' => 'images/forms/401k403bAsset.png'],
                    ['label' => 'Real<br>Estate<br>&nbsp;', 'value' => '5', 'image' => 'images/forms/RealEstateAsset.png'],
                ]
            ])
        </div>
    </div>
    <div id="parent_asset_bank_checking_div">
        @include('elements.parent.questionnaires.common._divider')
        <div class="row">
            <div class="col-md-12">
                <h2 class="font-weight-light kt-margin-b-20">
                    Please estimate the amount you have in banks, including checking, savings, CDs, and
                    money market accounts.
                    <sup><span class="error font-s12 text-danger">*Required</span></sup>
                </h2>
            </div>
            <div class="col-md-6">
                {!! Form::text('parent_asset_bank_checking', null, [
                    'class'=>'form-control form-control-lg text-left',
                    'id' => 'parent_asset_bank_checking',
                    'required' => 'false',
                    "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"
                 ]) !!}
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
    <div id="parent_asset_college_savings_div">
        @include('elements.parent.questionnaires.common._divider')
        <div class="row" id="parent_asset_college_savings_div">
            <div class="col-md-12">
                <h2 class="font-weight-light kt-margin-b-20">
                    What is the total estimated amount in your college savings accounts, including 529s,
                    Coverdells, and Educational IRAs?
                    <sup><span class="error font-s12 text-danger">*Required</span></sup>
                </h2>
            </div>
            <div class="col-md-6">
                {!! Form::text('parent_asset_college_savings', null, [
                    'class'=>'form-control form-control-lg text-left',
                    'id' => 'parent_asset_college_savings',
                    'required' => 'false',
                    "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"
                 ]) !!}
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
    <div id="parent_asset_investments_accounts_div">
        @include('elements.parent.questionnaires.common._divider')
        <div class="row" id="parent_asset_investments_accounts_div">
            <div class="col-md-12">
                <h2 class="font-weight-light kt-margin-b-20">
                    NOT including retirement accounts, how much do you have in investments accounts such as
                    stocks, bonds, and mutual funds?
                    To be clear, do NOT include any money you have in a 401k, IRA, or other retirement
                    accounts. Please give the combined total for the parents for this question.
                    <sup><span class="error font-s12 text-danger">*Required</span></sup>
                </h2>
            </div>
            <div class="col-md-6">
                {!! Form::text('parent_asset_investments_accounts', null, [
                    'class'=>'form-control form-control-lg text-left',
                    'id' => 'parent_asset_investments_accounts',
                    'required' => 'false',
                    "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"
                 ]) !!}
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
    <div id="parent_asset_retirement_accounts_div">
        @include('elements.parent.questionnaires.common._divider')
        <div class="row" id="">
            <div class="col-md-12">
                <h2 class="font-weight-light kt-margin-b-20">
                    What is the estimated combined total amount in your retirement accounts, include all
                    401k, 403b, IRA, Roth IRA, and Keogh accounts. Make sure this is the combined total for
                    the parents for this question.
                    <sup><span class="error font-s12 text-danger">*Required</span></sup>
                </h2>
            </div>
            <div class="col-md-6">
                {!! Form::text('parent_asset_retirement_accounts', null, [
                    'class'=>'form-control form-control-lg text-left',
                    'id' => 'parent_asset_retirement_accounts',
                    'required' => 'false',
                    "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"
                 ]) !!}
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
    @include('elements.parent.questionnaires.common._continue_button')
    {!! Form::close() !!}
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            showHideDiv();
            $("input.parent_type_of_asset").click(function () {
                showHideDiv();
            });
        });

        function showHideDiv() {
            hideAllAndMakeNotRequired();
            // Get All the checked values for checkbox
            let selectedAsset = [];
            $('.parent_type_of_asset:checked').each(function () {
                selectedAsset.push($(this).val());
            });
            console.log(selectedAsset);
            if (selectedAsset.includes("1")) {
                $("#parent_asset_bank_checking_div").show();
                $("#parent_asset_bank_checking").inputmask();
                document.getElementById("parent_asset_bank_checking").required = true;
            }
            if (selectedAsset.includes("2")) {
                $("#parent_asset_college_savings_div").show();
                $("#parent_asset_college_savings").inputmask();
                document.getElementById("parent_asset_college_savings").required = true;
            }
            if (selectedAsset.includes("3")) {
                $("#parent_asset_investments_accounts_div").show();
                $("#parent_asset_investments_accounts").inputmask();
                document.getElementById("parent_asset_investments_accounts").required = true;
            }
            if (selectedAsset.includes("4")) {
                $("#parent_asset_retirement_accounts_div").show();
                $("#parent_asset_retirement_accounts").inputmask();
                document.getElementById("parent_asset_retirement_accounts").required = true;
            }
        }

        function hideAllAndMakeNotRequired() {
            $("#parent_asset_bank_checking_div").hide();
            $("#parent_asset_college_savings_div").hide();
            $("#parent_asset_investments_accounts_div").hide();
            $("#parent_asset_retirement_accounts_div").hide();
            document.getElementById("parent_asset_bank_checking").required = false;
            document.getElementById("parent_asset_college_savings").required = false;
            document.getElementById("parent_asset_investments_accounts").required = false;
            document.getElementById("parent_asset_retirement_accounts").required = false;
        }
    </script>
@endsection