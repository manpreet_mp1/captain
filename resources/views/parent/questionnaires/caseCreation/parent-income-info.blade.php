@extends('themes.metronics.layout.fullwith-parent-case-creation')
@section('pageTitle', ' Income Details')
@section('content')
    {!! Form::model($data['request'], [
           'method'=>'POST',
           'class'=> 'kt-margin-t-30',
           'id'=> 'assessment-form',
           'name'=>'assessment-form'
        ]) !!}
    @include('parent.questionnaires.caseCreation._assessment_step')

    <div class="row">
        <div class="col-md-12">
            <p class="lead text-brand text-primary">
                {{ $ST_USER->first_name }}, enter the amount that closest to your actual income. Be
                sure to include all wages, salaries, and self- employment income from all sources you
                report on your tax return.
            </p>
            <h2 class="font-weight-light kt-margin-b-20">
                What is your annual income from work?
                <sup><span class="error font-s12 text-danger">*Required</span></sup>
            </h2>
        </div>
        <div class="col-md-6">
            {!! Form::text('parentOne[income_work_dollar]', null, [
                'class'=>'form-control form-control-lg text-left',
                'required' => 'true',
                'id' => 'income_work_dollar',
                "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"
             ]) !!}
        </div>
        <div class="col-md-6">
        </div>
    </div>
    @if($ST_PARENT->is_parent_two === 1)
        @include('elements.parent.questionnaires.common._divider')
        <div class="row">
            <div class="col-md-12">
                <p class="lead text-brand text-primary">
                    Enter the amount that best matches {{ $ST_USER_TWO->first_name }}'s
                    income. Be sure to include all wages,
                    salaries, and self- employment income from all sources that are reported to the IRS.
                </p>
                <p class="lead text-brand text-primary">
                    Only include {{ $ST_USER_TWO->first_name }}'s income. You already
                    provided your income.
                </p>
                <h2 class="font-weight-light kt-margin-b-20">
                    Enter the total amount of annual earnings from work
                    for {{ $ST_USER_TWO->first_name }}?
                    <sup><span class="error font-s12 text-danger">*Required</span></sup>
                </h2>
            </div>
            <div class="col-md-6">
                {!! Form::text("parentTwo[income_work_dollar]", null, [
                'class'=>'form-control form-control-lg text-left',
                'required' => 'true',
                'id' => 'parent_two_income_work_dollar',
                "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"
             ]) !!}
            </div>
            <div class="col-md-6">
            </div>
        </div>
    @endif
    <!--
    include('elements.parent.questionnaires.common._divider')
    <div class="row">
        <div class="col-md-12">
            <p class="lead text-brand text-primary">
                Check all that apply.
            </p>
        php
            $parentTwoText = $ST_PARENT->is_parent_two === 1 ? " or {$ST_USER_TWO->first_name} " : "";
            $question = "Do you {$parentTwoText}have any of these additional sources of income?";
        endphp
            include('elements.forms._input_image', [
                'title' => $question,
                'type' => "checkbox",
                'required' => 'true',
                'column' => 'family[parent_additional_income_sources]',
                'modelData' => $ST_FAMILY->parent_additional_income_sources,
                'values' => $data['additionalIncomeSources']
            ])
        </div>
    </div>
    -->
    @include('elements.parent.questionnaires.common._continue_button')
    {!! Form::close() !!}
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $("#income_work_dollar").inputmask();
            $("#parent_two_income_work_dollar").inputmask();
        });
    </script>
@endsection