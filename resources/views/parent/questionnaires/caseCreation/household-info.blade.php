@extends('themes.metronics.layout.fullwith-parent-case-creation')
@section('pageTitle', ' Household Information')
@section('content')
    {!! Form::model($data['request'], [
           'method'=>'POST',
           'class'=> 'kt-margin-t-30',
           'id'=> 'assessment-form',
           'name'=>'assessment-form'
        ]) !!}
    @include('parent.questionnaires.caseCreation._assessment_step')
    <div class="row">
        <div class="col-md-12">
            <p class="lead text-brand text-primary">The colleges need to know how many children
                you @if($ST_PARENT->is_parent_two === 1)
                    and {{ $ST_USER_TWO->first_name }}@endif have in your
                household.</p>
            <p class="lead text-brand text-primary">You will need to include all children, step-children
                and adult children, that reside in
                your household and derive more than 50% of their support from
                you @if($ST_PARENT->is_parent_two === 1)
                    and {{ $ST_USER_TWO->first_name }}@endif.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h2 class="font-weight-light kt-margin-b-20">
                How many children, stepchildren, or adult children are currently living in your
                household and derive more than 50% of their support from you
                @if($ST_PARENT->is_parent_two === 1)
                    and {{ $ST_USER_TWO->first_name }}@endif ?
                <sup><span class="error font-s12 text-danger">*Required</span></sup>
            </h2>
        </div>
        <div class="col-md-6">
            {!! Form::select("family[household_dependent_children_num]", $data['dropDownNumberTillFive'], null, ['class'=>'form-control form-control-lg', 'required' => 'true']) !!}
        </div>
        <div class="col-md-6">
        </div>
    </div>
    @include('elements.parent.questionnaires.common._continue_button')
    {!! Form::close() !!}
@endsection