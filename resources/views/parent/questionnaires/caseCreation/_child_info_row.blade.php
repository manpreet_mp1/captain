<td>
    {!! Form::text("child[$sequence][first_name]", null, ['class'=>'form-control', 'required' => 'true']) !!}
</td>
<td>
    {!! Form::select("child[$sequence][grade_level_id]", $data['gradeLevel'], null, ['class'=>'form-control', 'required' => 'true']) !!}
</td>
<td>
    {!! Form::select("child[$sequence][relationship_parent_one_id]", $data['relationshipToParent'], null, ['class'=>'form-control', 'required' => 'true']) !!}
</td>
@if($ST_PARENT->is_parent_two === 1)
    <td>
        {!! Form::select("child[$sequence][relationship_parent_two_id]", $data['relationshipToParent'], null, ['class'=>'form-control', 'required' => 'true']) !!}
    </td>
@endif