<div class="row kt-margin-t-50 kt-margin-b-30">
    <div class="col-md-6">
        {!! Form::model($data['request'], [
           'method'=>'POST',
           'class'=>'',
           'id'=> 'assessment-form',
           'name'=>'assessment-form'
        ]) !!}
        @include('parent.questionnaires.caseCreation._assessment_step')
        {!! Form::button('START', [
            'name' => 'start',
            'id' => 'start',
            'class'=> 'btn btn-brand btn-elevate-air font-s18 kt-padding-l-40 kt-padding-r-40',
            'type'=>'submit',
            'value' => 'START'
        ]) !!}
        {!! Form::close() !!}
    </div>
</div>