<p class="text-center kt-margin-b-30">
    <img src="{{ config('app.cdn') }}images/logo-ncsa.png" alt="NCSA" class="img-fluid">
</p>
<div class="kt-divider">
    <span></span>
    <span></span>
</div>
<h2 class="text-center kt-margin-t-50 kt-margin-b-30 text-uppercase">Welcome NSCA MVP Member!</h2>
<h3 class="font-weight-light text-center kt-margin-b-30">{{ $ST_USER->first_name }}, completing this financial
    assessment is the first step in developing your personalized game plan to pay for your child's college.</h3>
<div class="row">
    <div class="col-md-8">
        <p class="font-weight-light font-s18">Upon completion you will be redirected to an online calendar where you will be
            able to schedule your initial call with a SMARTTRACK® College Funding Advisor. There is NO COST to you. Our
            personalized SMARTTRACK® College Funding Consultation is provided as part of your child's MVP status with
            NCSA.</p>
        <p class="font-weight-light font-s18">This secure online assessment asks some basic questions that take about 5 minutes
            to complete.</p>
        <p class="font-weight-light font-s18">{{ $ST_USER->first_name }}, as a part of your NCSA MVP membership,
            SMARTTRACK® College Funding will be assisting you with how to best pay for college, how to pay less for
            college, and how to keep your retirement safe in the process.</p>
    </div>
    <div class="col-md-4">
        <img src="{{ config('app.cdn') }}images/financial-assessment.png" alt="Financial Assessment" class="img-fluid">
    </div>
</div>
@include("parent.questionnaires.caseCreation.landing._form")


