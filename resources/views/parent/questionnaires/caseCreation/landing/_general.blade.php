<h1 class="text-center kt-margin-b-30">{{ $ST_USER->first_name }}, completing this financial
    assessment is the first step in developing your personalized game plan to pay for your child's college.</h1>
<div class="row">
    <div class="col-md-8">
        <p class="font-weight-light font-s18">Upon completion you will be redirected to an online calendar where you
            will be
            able to schedule your initial call with a SMARTTRACK® College Funding Advisor.</p>
        <p class="font-weight-light font-s18">This secure online assessment asks some basic questions that take about 5
            minutes
            to complete.</p>
        <p class="font-weight-light font-s18">Once you have completed our secure online assessment and have chosen a
            convenient time for your initial call, your SMARTTRACK® College Funding Advisor will review and analyze your
            answers and then prepare an initial college funding game plan that is custom tailored for you and your
            student.</p>
    </div>
    <div class="col-md-4">
        <img src="{{ config('app.cdn') }}images/financial-assessment.png" alt="Financial Assessment" class="img-fluid">
    </div>
</div>
@include("parent.questionnaires.caseCreation.landing._form")