@extends('themes.metronics.layout.fullwith-parent-case-creation')
@section('pageTitle', ' Assets Details')
@section('content')
    {!! Form::model($data['request'], [
           'method'=>'POST',
           'id'=> 'assessment-form',
           'class'=> 'kt-margin-t-30',
           'name'=>'assessment-form'
        ]) !!}
    @include('parent.questionnaires.caseCreation._assessment_step')
    <div class="row">
        <div class="col-md-12">
            @php
                $parentTwoText = $ST_PARENT->is_parent_two === 1 ? " or {$ST_USER_TWO->first_name} " : "";
                $question = "Do you {$parentTwoText}have any non-retirement assets like checking/savings, stocks, bonds, and/or mutual funds?";
            @endphp
            @include('elements.forms._input_image', [
                'title' => $question,
                'type' => "radio",
                'required' => 'true',
                'column' => 'family[is_parent_asset_non_retirement]',
                'class' => 'is_parent_asset_non_retirement',
                'modelData' => $ST_FAMILY->is_parent_asset_non_retirement,
                'values' => [
                    ['label' => 'Yes', 'value' => '1', 'image' => 'images/forms/yes.png'],
                    ['label' => 'No', 'value' => '0', 'image' => 'images/forms/no.png'],
                ]
            ])
        </div>
    </div>
    <div id="asset_details">
        @include('elements.parent.questionnaires.common._divider')
        <div class="row">
            <div class="col-md-12">
                <h2 class="font-weight-light kt-margin-b-20">
                    What is the total amount of all your non-retirement accounts?
                    <sup><span class="error font-s12 text-danger">*Required</span></sup>
                </h2>
            </div>
            <div class="col-md-6">
                {!! Form::text("family[parent_asset_non_retirement_dollar_total]", null, [
                    'class'=>'form-control form-control-lg text-left',
                    'id' => 'parent_asset_non_retirement_dollar',
                    'required' => 'false',
                    "data-inputmask" => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"
                 ]) !!}
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
    @include('elements.parent.questionnaires.common._continue_button')
    {!! Form::close() !!}
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            showHideDiv();
            $("input.is_parent_asset_non_retirement").click(function () {
                showHideDiv();
            });
        });

        function showHideDiv() {
            hideAllAndMakeNotRequired();
            // Get All the checked values for checkbox
            let selectedAsset = [];
            $('.is_parent_asset_non_retirement:checked').each(function () {
                selectedAsset.push($(this).val());
            });
            if (selectedAsset.includes("1")) {
                $("#asset_details").show();
                $("#parent_asset_non_retirement_dollar").inputmask();
                document.getElementById("parent_asset_non_retirement_dollar").required = true;
            }
        }

        function hideAllAndMakeNotRequired() {
            $("#asset_details").hide();
            document.getElementById("parent_asset_non_retirement_dollar").required = false;
        }
    </script>
@endsection