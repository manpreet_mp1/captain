@extends("themes.metronics.layout.fullwith-parent-default")
@include('elements.parent.familyInfo.students.index', [
    'editStudentRouteName' => 'parent.familyInfo.students.edit',
    'addStudentRouteName' => 'parent.familyInfo.students.add',
    'deleteStudentRouteName' => 'parent.familyInfo.students.delete',
    'additionalParam' => [
     ]
])
