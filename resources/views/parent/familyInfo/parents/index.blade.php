@extends("themes.metronics.layout.fullwith-parent-default")
@include('elements.parent.familyInfo.parents.index', [
    'editParentRouteName' => 'parent.familyInfo.parents.edit',
    'addParentRouteName' => 'parent.familyInfo.parents.add',
    'additionalParam' => [
     ]
])
