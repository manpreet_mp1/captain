/**
 * Additional Packages
 */
/**
 * Inputmask is a javascript library which creates an input mask.
 * Inputmask can run against vanilla javascript, jQuery and jqlite.
 * https://www.npmjs.com/package/inputmask
 */
window.inputmask = require('inputmask');
/**
 * This script gives you the zone info key representing your device's time zone setting.
 * https://www.npmjs.com/package/jstimezonedetect
 */
window.jstz = require('jstimezonedetect');
/**
 * Lightweight customizable cross-browser jQuery datepicker, built with es5 and css-flexbox.
 * Works in all modern desktop and mobile browsers (tested on Android 4.4+ and iOS8+).
 */
// window.datepicker = require('air-datepicker');
// (function ($) { $.fn.datepicker.language['en'] = {
//     days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
//     daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
//     daysMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
//     months: ['January','February','March','April','May','June', 'July','August','September','October','November','December'],
//     monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
//     today: 'Today',
//     clear: 'Clear',
//     dateFormat: 'mm/dd/yyyy',
//     timeFormat: 'hh:ii aa',
//     firstDay: 0
// }; })(jQuery);
