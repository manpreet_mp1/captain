import lodash from 'lodash';
import axios from 'axios'
import numeral from 'numeral';
import _ from "underscore";

export default {
    // Declare data
    data: {
        appointment: {
            availableDates: [],
            availableTimes: [],
            appointmentTypes: [],
            appointmentTypeID: null,
            timezone: null,
            yearMonth: null, // simple month in YYYY-MM format
            selectedDate: null, // date selected by calendar
            firstDay: 1, // first day of the calendar
            selectedTime: null, // time selected by user
            bookedAppointment: {}, // after appointment is booked
            advisor: {},
            clientAppointment: {}, // parsed valued by backend
        },
        loading: {
            form: false,
            appointmentTypes: true,
            calendar: true,
            time: true,
        },
        visibility: {
            appointmentTypes: false,
            form: false,
            result: false,
            calendar: false,
            time: false,
            button: false,
        }
    },
    methods: {
        // @todo - will be needed
        getAppointmentTypes() {
        },
        async getF1AppointmentId() {
            let response = await axios.get('/api/acuity/getF1AppointmentId');
            return response.data.id;
        },
        timezoneChanged() {
            this.getAvailableDates();
        },
        getAvailableDates() {
            this.visibility.calendar = true;
            this.loading.calendar = true;
            this.visibility.time = false; // hide time
            this.visibility.button = false; // hide button
            axios.get('/api/acuity/availability/dates', {params: this.getAvailableDatesParam()})
                .then(response => {
                    this.appointment.availableDates = response.data.dates;
                    let dates = [];
                    dates.push({
                        start: 0,
                        end: 0
                    });
                    this.appointment.availableDates.forEach(function (entry) {
                        dates.push({
                            start: entry,
                            end: entry
                        });
                    });
                    this.appointment.availableDates = dates;
                    this.loading.calendar = false;
                });
        },
        getAvailableDatesParam() {
            return {
                month: this.appointment.yearMonth,
                timezone: this.appointment.timezone,
                appointmentTypeID: this.appointment.appointmentTypeID,
            };
        },
        dateSelected(val) {
            this.visibility.time = true;
            this.loading.time = true;
            this.getAvailableTimes(val);
        },
        getAvailableTimes(date) {
            return axios.get('/api/acuity/availability/times', {params: this.getAvailableTimesParam(date)})
                .then(response => {
                    this.appointment.availableTimes = response.data.times;
                    this.loading.time = false;
                });
        },
        getAvailableTimesParam(date) {
            return {
                date: date,
                timezone: this.appointment.timezone,
                appointmentTypeID: this.appointment.appointmentTypeID,
            };
        },
        timeSelected() {
            this.visibility.button = true;
        },
        bookAppointment() {
            this.loading.form = true;
            axios.post('/api/acuity/appointment', this.getBookAppointmentData())
                .then(response => {
                    this.appointment.bookedAppointment = response.data.appointment;
                    this.appointment.clientAppointment = response.data.clientAppointment;
                    this.performAfterAppointmentBooked();
                });
        },
        getBookAppointmentData() {
            return {
                date: this.appointment.selectedDate.toISOString().substring(0, 10),
                timezone: this.appointment.timezone,
                appointmentTypeID: this.appointment.appointmentTypeID,
                time: this.appointment.selectedTime,
                familyId: window.familyId,
            };
        },
    }
};
