import VCalendar from 'v-calendar';
import AppointmentMixin from "./mixins/AppointmentMixin";
import axios from "axios";

new Vue({
    el: '#app',
    mixins: [AppointmentMixin],
    components: {
        VCalendar
    },
    // call various methods and assign data whenever the vue app is loaded
    mounted() {
        this.setInitialValues();
    },
    // Declare data
    data: {
    },
    // Various method definitions
    methods: {
        // Set the initial values
        async setInitialValues() {
            if (!this.appointment.timezone) {
                this.appointment.timezone = jstz.determine().name();
            }
            this.appointment.appointmentTypeID = await this.getF1AppointmentId();
            this.visibility.form = true;
            this.visibility.calendar = true;
        },
        setDatePicker(page) {
            if (!this.appointment.timezone || !this.appointment.appointmentTypeID) {
                this.setInitialValues();
            }
            this.appointment.yearMonth = page.year + "-" + page.month;
            this.getAvailableDates();
        },
        performAfterAppointmentBooked() {
            // assign family to advisor
            axios.post('/api/family/assignAdvisor', {familyId: window.familyId, calendarId: this.appointment.bookedAppointment.calendarID})
                .then(response => {
                    this.appointment.advisor = response.data.advisor;
                    // disable form
                    this.loading.form = false;
                    this.visibility.form = false;
                    this.visibility.result = true;
                });
        },
    },
});
