$(document).ready(function () {
    $(".dollar").inputmask();

    function extractNumber(str) {
        let output = Number(parseInt(str.replace(/[^0-9]/g, '')));
        if (!output) {
            return 0;
        }
        return output;
    }

    function setSum(sumClass) {
        let sum = 0;
        $("." + sumClass).each(function () {
            sum += extractNumber($(this).val());
        });
        $('#total_' + sumClass).val(sum);
    }

    // update array here
    let sumArray = [
        // Income - info
        'income_work_dollar',
        'income_adjusted_gross_dollar',
        // Income - tax
        'income_tax_dollar',
        // Income - additional
        'income_additional_tax_credit_dollar',
        'income_additional_child_support_paid_dollar',
        'income_additional_work_study_dollar',
        'income_additional_grant_scholarship_dollar',
        'income_additional_combat_pay_dollar',
        'income_additional_cooperative_education_dollar',
        // Income - untaxed,
        'income_untaxed_tax_deferred_dollar',
        'income_untaxed_ira_deduction_dollar',
        'income_untaxed_child_support_received_dollar',
        'income_untaxed_tax_exempt_dollar',
        'income_untaxed_ira_distribution_dollar',
        'income_untaxed_pension_portion_dollar',
        'income_untaxed_paid_to_military_dollar',
        'income_untaxed_veteran_benefit_dollar',
        'income_untaxed_other_dollar',
        // Asset - education
        'asset_education_student_sibling_dollar',
        'asset_education_five_two_nine_dollar',
        'asset_education_coverdell_dollar',
        // Asset - non-retirement
        'asset_non_retirement_checking_savings_dollar',
        'asset_non_retirement_certificate_of_deposit_dollar',
        'asset_non_retirement_t_bills_dollar',
        'asset_non_retirement_money_market_dollar',
        'asset_non_retirement_mutual_dollar',
        'asset_non_retirement_stock_dollar',
        'asset_non_retirement_bond_dollar',
        'asset_non_retirement_trust_dollar',
        'asset_non_retirement_other_securities_dollar',
        'asset_non_retirement_annuities_dollar',
        'asset_non_retirement_other_investments_dollar',
        // Asset - retirement
        'asset_retirement_defined_benefit_dollar',
        'asset_retirement_four_o_one_dollar',
        'asset_retirement_ira_dollar',
        'asset_retirement_roth_ira_dollar',
        'asset_retirement_previous_employer_dollar',
        'asset_retirement_other_dollar',
    ];

    $.each(sumArray, function (index, sumClass) {
        setSum(sumClass);
    });

    // Income - info
    $('.income_work_dollar').keyup(function () {
        setSum('income_work_dollar');
    });
    $('.income_adjusted_gross_dollar').keyup(function () {
        setSum('income_adjusted_gross_dollar');
    });
    // Income - tax
    $('.income_tax_dollar').keyup(function () {
        setSum('income_tax_dollar');
    });
    // Income - additional
    $('.income_additional_tax_credit_dollar').keyup(function () {
        setSum('income_additional_tax_credit_dollar');
    });
    $('.income_additional_child_support_paid_dollar').keyup(function () {
        setSum('income_additional_child_support_paid_dollar');
    });
    $('.income_additional_work_study_dollar').keyup(function () {
        setSum('income_additional_work_study_dollar');
    });
    $('.income_additional_grant_scholarship_dollar').keyup(function () {
        setSum('income_additional_grant_scholarship_dollar');
    });
    $('.income_additional_combat_pay_dollar').keyup(function () {
        setSum('income_additional_combat_pay_dollar');
    });
    $('.income_additional_cooperative_education_dollar').keyup(function () {
        setSum('income_additional_cooperative_education_dollar');
    });
    // Income - untaxed
    $('.income_untaxed_tax_deferred_dollar').keyup(function () {
        setSum('income_untaxed_tax_deferred_dollar');
    });
    $('.income_untaxed_ira_deduction_dollar').keyup(function () {
        setSum('income_untaxed_ira_deduction_dollar');
    });
    $('.income_untaxed_child_support_received_dollar').keyup(function () {
        setSum('income_untaxed_child_support_received_dollar');
    });
    $('.income_untaxed_tax_exempt_dollar').keyup(function () {
        setSum('income_untaxed_tax_exempt_dollar');
    });
    $('.income_untaxed_ira_distribution_dollar').keyup(function () {
        setSum('income_untaxed_ira_distribution_dollar');
    });
    $('.income_untaxed_pension_portion_dollar').keyup(function () {
        setSum('income_untaxed_pension_portion_dollar');
    });
    $('.income_untaxed_paid_to_military_dollar').keyup(function () {
        setSum('income_untaxed_paid_to_military_dollar');
    });
    $('.income_untaxed_veteran_benefit_dollar').keyup(function () {
        setSum('income_untaxed_veteran_benefit_dollar');
    });
    $('.income_untaxed_other_dollar').keyup(function () {
        setSum('income_untaxed_other_dollar');
    });
    // Asset - education
    $('.asset_education_student_sibling_dollar').keyup(function () {
        setSum('asset_education_student_sibling_dollar');
    });
    $('.asset_education_five_two_nine_dollar').keyup(function () {
        setSum('asset_education_five_two_nine_dollar');
    });
    $('.asset_education_coverdell_dollar').keyup(function () {
        setSum('asset_education_coverdell_dollar');
    });
    // Asset - non-retirement
    $('.asset_non_retirement_checking_savings_dollar').keyup(function () {
        setSum('asset_non_retirement_checking_savings_dollar');
    });
    $('.asset_non_retirement_certificate_of_deposit_dollar').keyup(function () {
        setSum('asset_non_retirement_certificate_of_deposit_dollar');
    });
    $('.asset_non_retirement_t_bills_dollar').keyup(function () {
        setSum('asset_non_retirement_t_bills_dollar');
    });
    $('.asset_non_retirement_money_market_dollar').keyup(function () {
        setSum('asset_non_retirement_money_market_dollar');
    });
    $('.asset_non_retirement_mutual_dollar').keyup(function () {
        setSum('asset_non_retirement_mutual_dollar');
    });
    $('.asset_non_retirement_stock_dollar').keyup(function () {
        setSum('asset_non_retirement_stock_dollar');
    });
    $('.asset_non_retirement_bond_dollar').keyup(function () {
        setSum('asset_non_retirement_bond_dollar');
    });
    $('.asset_non_retirement_trust_dollar').keyup(function () {
        setSum('asset_non_retirement_trust_dollar');
    });
    $('.asset_non_retirement_other_securities_dollar').keyup(function () {
        setSum('asset_non_retirement_other_securities_dollar');
    });
    $('.asset_non_retirement_annuities_dollar').keyup(function () {
        setSum('asset_non_retirement_annuities_dollar');
    });
    $('.asset_non_retirement_other_investments_dollar').keyup(function () {
        setSum('asset_non_retirement_other_investments_dollar');
    });
    // Asset - retirement
    $('.asset_retirement_defined_benefit_dollar').keyup(function () {
        setSum('asset_retirement_defined_benefit_dollar');
    });
    $('.asset_retirement_four_o_one_dollar').keyup(function () {
        setSum('asset_retirement_four_o_one_dollar');
    });
    $('.asset_retirement_ira_dollar').keyup(function () {
        setSum('asset_retirement_ira_dollar');
    });
    $('.asset_retirement_roth_ira_dollar').keyup(function () {
        setSum('asset_retirement_roth_ira_dollar');
    });
    $('.asset_retirement_previous_employer_dollar').keyup(function () {
        setSum('asset_retirement_previous_employer_dollar');
    });
    $('.asset_retirement_other_dollar').keyup(function () {
        setSum('asset_retirement_other_dollar');
    });
});