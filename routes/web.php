<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

// Scripts Route
Route::group(['namespace' => 'Scripts', 'prefix' => 'scripts'], function () {
    Route::get('financialCoachAssessmentRedirect', 'FinancialCoachAssessmentRedirectController@index')->name('scripts.financialCoachAssessmentRedirect');
    Route::get('financialCoachAssessmentResponse', 'FinancialCoachAssessmentResponseController@index')->name('scripts.financialCoachAssessmentResponse');
    Route::get('financialCoachAssessmentResult', 'FinancialCoachAssessmentResponseController@result')->middleware('auth.basic');
});
Route::group(['namespace' => 'Webhooks', 'prefix' => 'webhooks'], function () {
    Route::get('acuityUpdateAppointmentById', 'AcuityUpdateAppointmentByIdController@index');
    Route::post('acuityUpdateAppointmentById', 'AcuityUpdateAppointmentByIdController@index');
    Route::post('financialCoachAssessmentSaveResponse', 'FinancialCoachAssessmentSaveResponseController@index');
});

// Basic Auth Routes
Route::group(['namespace' => 'Auth', 'middleware' => 'guest'], function () {
    Route::get('/', 'LoginController@index')->name('login');
    Route::post('/', 'LoginController@authenticate');
//    Route::get('/forgot-password', 'ForgotPasswordController@index')->name('forgotPassword');
//    Route::post('/forgot-password', 'ForgotPasswordController@forgotPassword');
//    Route::get('/reset-password', 'ResetPasswordController@index')->name('resetPassword');
//    Route::post('/reset-password', 'ResetPasswordController@reset');
});

// General Logged In Routes
Route::group(['middleware' => 'auth'], function () {
    Route::get('/logout', 'Auth\LogoutController@index')->name('logout');
    Route::get('/dashboard', 'Auth\RedirectToCorrectDashboardController@index')->name('dashboard');

    Route::group(['middleware' => 'role.any', 'namespace' => 'Resources'], function () {
        Route::get('scholarships/merit', 'MeritScholarshipsController@index')->name('scholarships.merit');
        Route::get('scholarships/merit/{ipeds}', 'MeritScholarshipsController@show')->name('scholarships.merit.show');
        Route::group(['middleware' => 'role.advisor'], function () {
        });
        Route::group(['middleware' => 'role.advisor'], function () {
//            Route::get('client-strategies/strategies', 'ClientStrategiesController@strategies')->name('advisor.case.clientStrategies.strategies');
//            Route::get('client-strategies/strategies/{slug}', 'ClientStrategiesController@strategiesShow')->name('advisor.case.clientStrategies.strategies.show');
//            Route::get('client-strategies/tax-talking-points', 'ClientStrategiesController@taxTalkingPoints')->name('advisor.case.clientStrategies.taxTalkingPoints');
//            Route::get('client-strategies/tax-talking-points/{slug}', 'ClientStrategiesController@taxTalkingPointsShow')->name('advisor.case.clientStrategies.taxTalkingPoints.show');
        });
        Route::get('colleges/test', 'CollegesController@test'); // @todo - remove me
    });
    Route::group(['prefix' => 'premium', 'namespace' => 'Premium'], function () {
        Route::get('/smart-steps', 'SmartStepsPremiumController@index')->name('premium.smartSteps.index');
    });
});

// Parent Routes
Route::group(['namespace' => 'Parent', 'prefix' => 'parent'], function () {
    Route::group(['middleware' => 'guest'], function () {
        Route::get('/register', 'ParentRegisterController@index')->name('parent.register');
        Route::post('/register', 'ParentRegisterController@register');
    });
    Route::group(['middleware' => ['auth', 'role.parent']], function () {
        Route::get('/dashboard', 'ParentDashboardController@index')->name('parent.dashboard');
        Route::get('/efc', 'ParentEfcCalculatorController@index')->name('parent.efc.result');
        Route::get('/scenarios', 'ParentScenarioController@index')->name('parent.scenarios');
        Route::get('/scenarios/{slug}', 'ParentScenarioController@show')->name('parent.scenarios.show');

        // Subscription
        Route::group(['prefix' => 'subscriptions',], function () {
            Route::get('/', 'ParentSubscriptionController@index')->name('parent.subscriptions');
            Route::get('/{slug}', 'ParentSubscriptionController@checkout')->name('parent.subscriptions.checkout');
            Route::post('/{slug}', 'ParentSubscriptionController@coupon')->name('parent.subscriptions.coupon');
            Route::post('/{slug}/subscribe', 'ParentSubscriptionController@subscribe')->name('parent.subscriptions.subscribe');
            Route::get('/{slug}/thank-you', 'ParentSubscriptionController@thankYou')->name('parent.subscriptions.thankYou');
        });

        // Family info
        Route::group(['prefix' => 'family-info',], function () {
            Route::get('/parents', 'ParentFamilyInfoController@parent')->name('parent.familyInfo.parents');
            Route::get('/parents/add', 'ParentFamilyInfoController@parentAdd')->name('parent.familyInfo.parents.add');
            Route::post('/parents/add', 'ParentFamilyInfoController@parentStore')->name('parent.familyInfo.parents.store');
            Route::get('/parents/{slug}', 'ParentFamilyInfoController@parentEdit')->name('parent.familyInfo.parents.edit');
            Route::post('/parents/{slug}', 'ParentFamilyInfoController@parentUpdate')->name('parent.familyInfo.parents.update');
            // students
            Route::get('/students', 'ParentFamilyInfoController@student')->name('parent.familyInfo.students');
            Route::get('/students/add', 'ParentFamilyInfoController@studentAdd')->name('parent.familyInfo.students.add');
            Route::post('/students/add', 'ParentFamilyInfoController@studentStore')->name('parent.familyInfo.students.store');
            Route::get('/students/{slug}', 'ParentFamilyInfoController@studentEdit')->name('parent.familyInfo.students.edit');
            Route::post('/students/{slug}', 'ParentFamilyInfoController@studentUpdate')->name('parent.familyInfo.students.update');
            Route::get('/students/{slug}/delete', 'ParentFamilyInfoController@studentDelete')->name('parent.familyInfo.students.delete');
            // Income info
            Route::get('/income-info', 'ParentFamilyInfoController@incomeInfo')->name('parent.familyInfo.incomeInfo');
            Route::post('/income-info', 'ParentFamilyInfoController@incomeInfoUpdate')->name('parent.familyInfo.incomeInfo.update');
            Route::get('/income-tax', 'ParentFamilyInfoController@incomeTax')->name('parent.familyInfo.incomeTax');
            Route::post('/income-tax', 'ParentFamilyInfoController@incomeTaxUpdate')->name('parent.familyInfo.incomeTax.update');
            Route::get('/income-additional', 'ParentFamilyInfoController@incomeAdditional')->name('parent.familyInfo.incomeAdditional');
            Route::post('/income-additional', 'ParentFamilyInfoController@incomeAdditionalUpdate')->name('parent.familyInfo.incomeAdditional.update');
            Route::get('/income-untaxed', 'ParentFamilyInfoController@incomeUntaxed')->name('parent.familyInfo.incomeUntaxed');
            Route::post('/income-untaxed', 'ParentFamilyInfoController@incomeUntaxedUpdate')->name('parent.familyInfo.incomeUntaxed.update');
            // Assets Info
            Route::get('/assets-education', 'ParentFamilyInfoController@assetsEducation')->name('parent.familyInfo.assetsEducation');
            Route::post('/assets-education', 'ParentFamilyInfoController@assetsEducationUpdate')->name('parent.familyInfo.assetsEducation.update');
            Route::get('/assets-non-retirement', 'ParentFamilyInfoController@assetsNonRetirement')->name('parent.familyInfo.assetsNonRetirement');
            Route::post('/assets-non-retirement', 'ParentFamilyInfoController@assetsNonRetirementUpdate')->name('parent.familyInfo.assetsNonRetirement.update');
            Route::get('/assets-retirement', 'ParentFamilyInfoController@assetsRetirement')->name('parent.familyInfo.assetsRetirement');
            Route::post('/assets-retirement', 'ParentFamilyInfoController@assetsRetirementUpdate')->name('parent.familyInfo.assetsRetirement.update');
            // Real-estate
            Route::get('/real-estate', 'ParentFamilyInfoController@realEstate')->name('parent.familyInfo.realEstate');
            Route::post('/real-estate', 'ParentFamilyInfoController@realEstateUpdate')->name('parent.familyInfo.realEstate.update');
            // business
            Route::get('/business', 'ParentFamilyInfoController@business')->name('parent.familyInfo.business');
            Route::post('/business', 'ParentFamilyInfoController@businessUpdate')->name('parent.familyInfo.business.update');
            // insurance
            Route::get('/insurance', 'ParentFamilyInfoController@insurance')->name('parent.familyInfo.insurance');
            Route::post('/insurance', 'ParentFamilyInfoController@insuranceUpdate')->name('parent.familyInfo.insurance.update');
        });

        // Resources
        Route::get('colleges', 'ParentCollegeController@index')->name('parent.colleges');
        Route::get('colleges/{slug}', 'ParentCollegeController@show')->name('parent.colleges.show');
        Route::post('colleges/{slug}/save', 'ParentCollegeController@save')->name('parent.colleges.save');
        // @todo - remove generosity routes
//        Route::get('colleges/{slug}/generosity', 'ParentCollegeController@generosity');
//        Route::get('colleges/{slug}/generosity/image', 'ParentCollegeController@generosityImage');
        Route::get('majors', 'ParentMajorController@index')->name('parent.majors');
        Route::get('majors/heading/{heading}', 'ParentMajorController@heading')->name('parent.majors.heading');
        Route::get('majors/{slug}', 'ParentMajorController@show')->name('parent.majors.show');
        Route::post('majors/{slug}/save', 'ParentMajorController@save')->name('parent.majors.save');
        Route::get('scholarships/merit', 'ParentMeritScholarshipsController@index')->name('parent.scholarships.merit');
        Route::get('scholarships/merit/{ipeds}', 'ParentMeritScholarshipsController@show')->name('parent.scholarships.merit.show');

        // Premium
        Route::get('messages', 'ParentMessageController@index')->name('parent.messages');

        // Questionnaires
        Route::group(['prefix' => 'questionnaires',], function () {
            Route::get('/case-creation/{slug}', 'ParentCaseCreationQuestionnaireController@index')->name('parent.questionnaires.caseCreation');
            Route::post('/case-creation/{slug}', 'ParentCaseCreationQuestionnaireController@store')->name('parent.questionnaires.caseCreation.store');
            Route::get('/efc/{slug}', 'ParentEfcQuestionnaireController@index')->name('parent.questionnaires.efc');
            Route::post('/efc/{slug}', 'ParentEfcQuestionnaireController@store')->name('parent.questionnaires.efc.store');
        });
    });
});

// Advisor Routes
Route::group(['namespace' => 'Advisor', 'prefix' => 'advisor', 'middleware' => ['auth', 'role.advisor']], function () {
    Route::get('/dashboard', 'AdvisorDashboardController@index')->name('advisor.dashboard');
    Route::get('/cases', 'AdvisorCaseController@index')->name('advisor.cases');
    Route::group(['prefix' => 'case/{caseSlug}', 'middleware' => 'advisorLoadedCase'], function () {
        Route::get('/', 'AdvisorCaseController@show')->name('advisor.cases.show');
        // subscription
        Route::group(['prefix' => 'subscriptions',], function () {
            Route::get('/', 'AdvisorCaseSubscriptionController@index')->name('advisor.case.subscriptions');
            Route::get('/fixed/{slug}', 'AdvisorCaseSubscriptionController@checkoutFixed')->name('advisor.case.subscriptions.checkoutFixed');
            Route::get('/payment-plan/{slug}', 'AdvisorCaseSubscriptionController@checkoutPaymentPlan')->name('advisor.case.subscriptions.checkoutPaymentPlan');
            Route::post('/fixed/{slug}', 'AdvisorCaseSubscriptionController@coupon')->name('advisor.case.subscriptions.coupon');
            Route::post('/payment-plan/{slug}', 'AdvisorCaseSubscriptionController@coupon');
            Route::post('/fixed/{slug}/subscribe', 'AdvisorCaseSubscriptionController@subscribeFixed')->name('advisor.case.subscriptions.subscribeFixed');
            Route::post('/payment-plan/{slug}/subscribe', 'AdvisorCaseSubscriptionController@subscribePaymentPlan')->name('advisor.case.subscriptions.subscribePaymentPlan');
            Route::get('/fixed/{slug}/thank-you', 'AdvisorCaseSubscriptionController@thankYouFixed')->name('advisor.case.subscriptions.thankYouFixed');
            Route::get('/payment-plan/{slug}/thank-you', 'AdvisorCaseSubscriptionController@thankYouPaymentPlan')->name('advisor.case.subscriptions.thankYouPaymentPlan');
        });
        // Family info
        Route::group(['prefix' => 'family-info',], function () {
            Route::get('/parents', 'AdvisorFamilyInfoController@parent')->name('advisor.case.familyInfo.parents');
            Route::get('/parents/add', 'AdvisorFamilyInfoController@parentAdd')->name('advisor.case.familyInfo.parents.add');
            Route::post('/parents/add', 'AdvisorFamilyInfoController@parentStore')->name('advisor.case.familyInfo.parents.store');
            Route::get('/parents/{slug}', 'AdvisorFamilyInfoController@parentEdit')->name('advisor.case.familyInfo.parents.edit');
            Route::post('/parents/{slug}', 'AdvisorFamilyInfoController@parentUpdate')->name('advisor.case.familyInfo.parents.update');
            // students
            Route::get('/students', 'AdvisorFamilyInfoController@student')->name('advisor.case.familyInfo.students');
            Route::get('/students/add', 'AdvisorFamilyInfoController@studentAdd')->name('advisor.case.familyInfo.students.add');
            Route::post('/students/add', 'AdvisorFamilyInfoController@studentStore')->name('advisor.case.familyInfo.students.store');
            Route::get('/students/{slug}', 'AdvisorFamilyInfoController@studentEdit')->name('advisor.case.familyInfo.students.edit');
            Route::post('/students/{slug}', 'AdvisorFamilyInfoController@studentUpdate')->name('advisor.case.familyInfo.students.update');
            Route::get('/students/{slug}/delete', 'AdvisorFamilyInfoController@studentDelete')->name('advisor.case.familyInfo.students.delete');
            // Income info
            Route::get('/income-info', 'AdvisorFamilyInfoController@incomeInfo')->name('advisor.case.familyInfo.incomeInfo');
            Route::post('/income-info', 'AdvisorFamilyInfoController@incomeInfoUpdate')->name('advisor.case.familyInfo.incomeInfo.update');
            Route::get('/income-tax', 'AdvisorFamilyInfoController@incomeTax')->name('advisor.case.familyInfo.incomeTax');
            Route::post('/income-tax', 'AdvisorFamilyInfoController@incomeTaxUpdate')->name('advisor.case.familyInfo.incomeTax.update');
            Route::get('/income-additional', 'AdvisorFamilyInfoController@incomeAdditional')->name('advisor.case.familyInfo.incomeAdditional');
            Route::post('/income-additional', 'AdvisorFamilyInfoController@incomeAdditionalUpdate')->name('advisor.case.familyInfo.incomeAdditional.update');
            Route::get('/income-untaxed', 'AdvisorFamilyInfoController@incomeUntaxed')->name('advisor.case.familyInfo.incomeUntaxed');
            Route::post('/income-untaxed', 'AdvisorFamilyInfoController@incomeUntaxedUpdate')->name('advisor.case.familyInfo.incomeUntaxed.update');
            // Assets Info
            Route::get('/assets-education', 'AdvisorFamilyInfoController@assetsEducation')->name('advisor.case.familyInfo.assetsEducation');
            Route::post('/assets-education', 'AdvisorFamilyInfoController@assetsEducationUpdate')->name('advisor.case.familyInfo.assetsEducation.update');
            Route::get('/assets-non-retirement', 'AdvisorFamilyInfoController@assetsNonRetirement')->name('advisor.case.familyInfo.assetsNonRetirement');
            Route::post('/assets-non-retirement', 'AdvisorFamilyInfoController@assetsNonRetirementUpdate')->name('advisor.case.familyInfo.assetsNonRetirement.update');
            Route::get('/assets-retirement', 'AdvisorFamilyInfoController@assetsRetirement')->name('advisor.case.familyInfo.assetsRetirement');
            Route::post('/assets-retirement', 'AdvisorFamilyInfoController@assetsRetirementUpdate')->name('advisor.case.familyInfo.assetsRetirement.update');
            // Real-estate
            Route::get('/real-estate', 'AdvisorFamilyInfoController@realEstate')->name('advisor.case.familyInfo.realEstate');
            Route::post('/real-estate', 'AdvisorFamilyInfoController@realEstateUpdate')->name('advisor.case.familyInfo.realEstate.update');
            // business
            Route::get('/business', 'AdvisorFamilyInfoController@business')->name('advisor.case.familyInfo.business');
            Route::post('/business', 'AdvisorFamilyInfoController@businessUpdate')->name('advisor.case.familyInfo.business.update');
            // insurance
            Route::get('/insurance', 'AdvisorFamilyInfoController@insurance')->name('advisor.case.familyInfo.insurance');
            Route::post('/insurance', 'AdvisorFamilyInfoController@insuranceUpdate')->name('advisor.case.familyInfo.insurance.update');
        });
        // Questionnaires
        Route::get('/efc', 'AdvisorEfcCalculatorController@index')->name('advisor.case.efc.result');
        Route::group(['prefix' => 'questionnaires',], function () {
            Route::get('/efc/{slug}', 'AdvisorEfcQuestionnaireController@index')->name('advisor.case.questionnaires.efc');
            Route::post('/efc/{slug}', 'AdvisorEfcQuestionnaireController@store')->name('advisor.case.questionnaires.efc.store');
        });
        Route::get('scenarios', 'AdvisorScenariosController@index')->name('advisor.case.scenarios');
        Route::get('scenarios/{slug}', 'AdvisorScenariosController@show')->name('advisor.case.scenarios.show');
        // Quick Plan
        Route::group(['prefix' => 'quick-plan',], function () {
            Route::get('/', 'AdvisorQuickPlanController@index')->name('advisor.case.quickPlan');
            Route::post('/', 'AdvisorQuickPlanController@store')->name('advisor.case.quickPlan.store');
            Route::get('/{slug}', 'AdvisorQuickPlanController@show')->name('advisor.case.quickPlan.show');
            Route::post('/{slug}', 'AdvisorQuickPlanController@update')->name('advisor.case.quickPlan.update');
        });
        Route::get('client-strategies/strategies', 'AdvisorClientStrategiesController@strategies')->name('advisor.case.clientStrategies.strategies');
        Route::get('client-strategies/strategies/{slug}', 'AdvisorClientStrategiesController@strategiesShow')->name('advisor.case.clientStrategies.strategies.show');
        Route::get('client-strategies/tax-talking-points', 'AdvisorClientStrategiesController@taxTalkingPoints')->name('advisor.case.clientStrategies.taxTalkingPoints');
        Route::get('client-strategies/tax-talking-points/{slug}', 'AdvisorClientStrategiesController@taxTalkingPointsShow')->name('advisor.case.clientStrategies.taxTalkingPoints.show');
        Route::get('scholarships/merit', 'AdvisorMeritScholarshipsController@index')->name('advisor.case.scholarships.merit');
        Route::get('scholarships/merit/{ipeds}', 'AdvisorMeritScholarshipsController@show')->name('advisor.case.scholarships.merit.show');
        Route::get('colleges', 'AdvisorCollegeController@index')->name('advisor.case.colleges');
        Route::get('colleges/{slug}', 'AdvisorCollegeController@show')->name('advisor.case.colleges.show');
        Route::post('colleges/{slug}/save', 'AdvisorCollegeController@save')->name('advisor.case.colleges.save');
        Route::get('five-two-nine', 'AdvisorFiveTwoNineController@index')->name('advisor.case.fiveTwoNine');
        Route::get('five-two-nine/common-questions', 'AdvisorFiveTwoNineController@commonQuestions')->name('advisor.case.fiveTwoNine.commonQuestions');
        Route::get('five-two-nine/compare', 'AdvisorFiveTwoNineController@compare')->name('advisor.case.fiveTwoNine.compare');
        Route::get('five-two-nine/{slug}', 'AdvisorFiveTwoNineController@show')->name('advisor.case.fiveTwoNine.show');
        Route::get('majors', 'AdvisorMajorController@index')->name('advisor.case.majors');
        Route::get('majors/heading/{heading}', 'AdvisorMajorController@heading')->name('advisor.case.majors.heading');
        Route::get('majors/{slug}', 'AdvisorMajorController@show')->name('advisor.case.majors.show');
        Route::post('majors/{slug}/save', 'AdvisorMajorController@save')->name('advisor.case.majors.save');
        Route::get('client-strategies/form-1040', 'AdvisorFormController@index')->name('advisor.form-1040');
    });
});
