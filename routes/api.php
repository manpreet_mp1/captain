<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function () {
    Route::get('/lite/colleges', 'LiteController@colleges');
    Route::get('/acuity/getF1AppointmentId', 'AcuityController@f1AppointmentId');
    Route::get('/acuity/availability/dates', 'AcuityController@availabilityDates');
    Route::get('/acuity/availability/times', 'AcuityController@availabilityTimes');
    Route::post('/acuity/appointment', 'AcuityController@appointment');
    Route::post('/family/assignAdvisor', 'AcuityController@assignAdvisor');
});
