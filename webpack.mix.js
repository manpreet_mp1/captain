let mix = require('laravel-mix');

    /*
    |--------------------------------------------------------------------------
    | Metronics Release - Metronic v6.0.4 - 24 June, 2019
    |--------------------------------------------------------------------------
    - Update the follow files
    -- resources/assets/metronics/v6.0.4/default/src/assets/sass/theme/_config.scss
    --- $kt-state-colors->brand->base
    --- $kt-state-colors->success->base
    --- $kt-font-families->regular
    --- $kt-font-families->heading
    --- $kt-font-size->size->desktop
    --- $kt-font-size->size->tablet
    --- $kt-font-size->size->mobile
    --- $kt-font-color->text
    --- $kt-base-colors->label
    -- resources/assets/metronics/v6.0.4/default/src/assets/sass/theme/demos/demo1/_config.scss
    --- comment out the color so that it uses global colors
    - Add node_modules to gitignore
    - Update ./package.json so that command npm run devAll and npm run prodAll points to correct version of metronics
    - Add correct files below for laravel
    */
mix
    .styles([
        'resources/assets/metronics/v6.0.4/default/dist/assets/vendors/global/vendors.bundle.css',
        'resources/assets/metronics/v6.0.4/default/dist/assets/vendors/custom/datatables/datatables.bundle.css',
    ], 'public/css/metronics-vendors.bundle.css')
    .styles([
        'resources/assets/metronics/v6.0.4/default/dist/assets/css/demo1/style.bundle.css',
        'resources/assets/metronics/v6.0.4/default/dist/assets/css/demo1/skins/aside/dark.css',
        'resources/assets/metronics/v6.0.4/default/dist/assets/css/demo1/skins/brand/dark.css',
        'resources/assets/metronics/v6.0.4/default/dist/assets/css/demo1/skins/header/base/light.css',
        'resources/assets/metronics/v6.0.4/default/dist/assets/css/demo1/skins/header/menu/light.css',
        // add pages from here
        'resources/assets/metronics/v6.0.4/default/dist/assets/css/demo1/pages/pricing/pricing-2.css',
        'resources/assets/metronics/v6.0.4/default/dist/assets/css/demo1/pages/wizard/wizard-2.css',
        'resources/assets/metronics/v6.0.4/default/dist/assets/css/demo1/pages/login/login-6.css',
    ], 'public/css/default.css')

    .scripts([
        'resources/assets/js/KTAppOptions.js', // custom
        'resources/assets/js/TimeZones.js', // custom
        'resources/assets/metronics/v6.0.4/default/dist/assets/vendors/global/vendors.bundle.js',
        'resources/assets/metronics/v6.0.4/default/dist/assets/js/demo1/scripts.bundle.js',
        'resources/assets/metronics/v6.0.4/default/dist/assets/vendors/custom/datatables/datatables.bundle.js',
        'resources/assets/js/sumField.js', // custom
    ], 'public/js/main.js')

    .copyDirectory('resources/assets/metronics/v6.0.4/default/dist/assets/media', 'public/media')

    /*
     |--------------------------------------------------------------------------
     | vendor assets
     |--------------------------------------------------------------------------
     */
    .styles([
        'resources/assets/common/css/font-poppins.css',
        'resources/assets/common/css/font-libre-franklin.css',
        'resources/assets/common/css/font-oswald.css',
        'resources/assets/common/css/font-open-sans.css',
        'resources/assets/common/css/font-lato.css',
        'resources/assets/common/css/icon-fontawesome-520.css',
        'resources/assets/common/css/icon-flaticon.css',
        'resources/assets/common/css/blueprintone.css',
        'resources/assets/common/css/blueprinttwo.css',
        'resources/assets/common/css/icon-lineawesome.css',
        'resources/assets/common/css/margin-padding.css',
        'resources/assets/common/css/font-size.css',
        'resources/assets/common/css/air-datepicker.css',
        'resources/assets/common/css/custom.css',
    ], 'public/css/vendors.css')

    .js('resources/assets/common/js/vendor.js', 'public/js/vendors.js')

    /*
     |--------------------------------------------------------------------------
     | App js
     |--------------------------------------------------------------------------
     */
    .js('resources/assets/js/app.js', 'public/js/app.js')
    .js('resources/assets/js/vue/acuityF1Appointment.js', 'public/js/acuityF1Appointment.js')

    .options({
        processCssUrls: false
    })

    .version();

/*
|--------------------------------------------------------------------------
| Metronics Release - Metronic v6 - 22 March, 2019
|--------------------------------------------------------------------------
vendors.bundle.css
    - Download this file from metronic demo (internet) using source code
style-3.css AND default.css
    style.scss
        - Add combination for skins
    _config.scss
        - $kt-state-colors->brand->base
        - $kt-state-colors->success->base
    ../../../framework/_config.scss
        - $kt-font-size->size->desktop
        - $kt-font-size->size->tablet
        - $kt-font-size->size->mobile
        - $kt-font-families->regular
        - $kt-font-families->heading
        - kt-font-color->text
        - kt-base-colors->label->4
login-v6.css
    - Include demo config update path
vendors.bundle.js
    - Download this file from metronic demo (internet) using source code
global.config.js
    - This is copied from the html as we need this to avoid js errors, need to update colors here
*/
// /** v6 */
// .styles([
//     'resources/assets/metronics/v6/metronics-vendors.bundle.css',
//     'resources/assets/metronics/v6/classic/assets/vendors/custom/datatables/datatables.bundle.css',
// ], 'public/css/metronics-vendors.bundle.css')
// .sass('resources/assets/metronics/v6/default/src/theme/demo/demo3/sass/style.scss', 'public/css/style-3.css')
// .sass('resources/assets/metronics/v6/default/src/theme/demo/default/sass/style.scss', 'public/css/default.css')
// .sass('resources/assets/metronics/v6/default/src/theme/app/custom/login/login-v6.scss', 'public/css/login-v6.css')
// .sass('resources/assets/metronics/v6/default/src/theme/app/custom/wizard/wizard-v2.scss', 'public/css/wizard-v2.default.css')
// .scripts([
//     'resources/assets/metronics/v6/global.config.js',
//     'resources/assets/metronics/v6/vendors.bundle.js',
//     'resources/assets/metronics/v6/classic/assets/demo/demo3/base/scripts.bundle.js',
//     'resources/assets/metronics/v6/classic/assets/app/bundle/app.bundle.js',
//     'resources/assets/metronics/v6/classic/assets/vendors/custom/datatables/datatables.bundle.js',
//     'resources/assets/js/sumField.js',
// ], 'public/js/main.js')
