<?php

use App\Models\Core\User;
use App\Models\Dropdown\State;
use App\Models\Family\Family;
use App\Models\Family\FamilyChild;
use App\Models\Family\FamilyParent;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Family::class, function (Faker $faker) {
    return [
        'advisor_id' => function () {
            return factory(User::class)->create()->id;
        },
        'name' => $faker->lastName,
        // Income - info
        //  'parent_income_work_dollar_total'  => $faker->numberBetween(100000, 200000),
        'parent_income_adjusted_gross_dollar' => $faker->numberBetween(10000, 20000),
        //  'parent_income_adjusted_gross_dollar_total' => $faker->numberBetween(100000, 200000),
        // Income - tax
        'parent_income_tax_dollar' => $faker->numberBetween(500, 2000),
        //  'parent_income_tax_dollar_total' => $faker->numberBetween(10000, 20000),
        // Income - additional
        'parent_income_additional_tax_credit_dollar' => $faker->numberBetween(100, 200),
        // 'parent_income_additional_tax_credit_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_income_additional_child_support_paid_dollar' => $faker->numberBetween(100, 200),
        // 'parent_income_additional_child_support_paid_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_income_additional_work_study_dollar' => $faker->numberBetween(100, 200),
        // 'parent_income_additional_work_study_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_income_additional_grant_scholarship_dollar' => $faker->numberBetween(100, 200),
        // 'parent_income_additional_grant_scholarship_dollar_total' => $faker->numberBetween(100, 200),
        'parent_income_additional_combat_pay_dollar' => $faker->numberBetween(100, 200),
        // 'parent_income_additional_combat_pay_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_income_additional_cooperative_education_dollar' => $faker->numberBetween(100, 200),
        // 'parent_income_additional_cooperative_education_dollar_total' => $faker->numberBetween(100, 200),
        // Income - untaxed
        'parent_income_untaxed_tax_deferred_dollar' => $faker->numberBetween(1000, 2000),
        // 'parent_income_untaxed_tax_deferred_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_income_untaxed_ira_deduction_dollar' => $faker->numberBetween(1000, 2000),
        // 'parent_income_untaxed_ira_deduction_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_income_untaxed_child_support_received_dollar' => $faker->numberBetween(1000, 2000),
        // 'parent_income_untaxed_child_support_received_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_income_untaxed_tax_exempt_dollar' => $faker->numberBetween(1000, 2000),
        // 'parent_income_untaxed_tax_exempt_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_income_untaxed_ira_distribution_dollar' => $faker->numberBetween(1000, 2000),
        //'parent_income_untaxed_ira_distribution_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_income_untaxed_pension_portion_dollar' => $faker->numberBetween(1000, 2000),
        // 'parent_income_untaxed_pension_portion_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_income_untaxed_paid_to_military_dollar' => $faker->numberBetween(1000, 2000),
        // 'parent_income_untaxed_paid_to_military_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_income_untaxed_veteran_benefit_dollar' => $faker->numberBetween(1000, 2000),
        // 'parent_income_untaxed_veteran_benefit_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_income_untaxed_other_dollar' => $faker->numberBetween(1000, 2000),
        // 'parent_income_untaxed_other_dollar_total' => $faker->numberBetween(10000, 20000),
        // Asset - education
        'parent_asset_education_student_sibling_dollar' => $faker->numberBetween(1000, 2000),
        // 'parent_asset_education_student_sibling_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_asset_education_five_two_nine_dollar' => $faker->numberBetween(1000, 2000),
        // 'parent_asset_education_five_two_nine_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_asset_education_coverdell_dollar' => $faker->numberBetween(1000, 2000),
        // 'parent_asset_education_coverdell_dollar_total' => $faker->numberBetween(10000, 20000),
        // Asset - non-retirement
        'parent_asset_non_retirement_checking_savings_dollar' => $faker->numberBetween(10000, 20000),
        // 'parent_asset_non_retirement_checking_savings_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_asset_non_retirement_certificate_of_deposit_dollar' => $faker->numberBetween(1000, 2000),
        // 'parent_asset_non_retirement_certificate_of_deposit_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_asset_non_retirement_t_bills_dollar' => $faker->numberBetween(1000, 2000),
        // 'parent_asset_non_retirement_t_bills_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_asset_non_retirement_money_market_dollar' => $faker->numberBetween(1000, 2000),
        // 'parent_asset_non_retirement_money_market_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_asset_non_retirement_mutual_dollar' => $faker->numberBetween(1000, 2000),
        // 'parent_asset_non_retirement_mutual_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_asset_non_retirement_stock_dollar' => $faker->numberBetween(1000, 2000),
        // 'parent_asset_non_retirement_stock_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_asset_non_retirement_bond_dollar' => $faker->numberBetween(1000, 2000),
        // 'parent_asset_non_retirement_bond_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_asset_non_retirement_trust_dollar' => $faker->numberBetween(1000, 2000),
        // 'parent_asset_non_retirement_trust_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_asset_non_retirement_other_securities_dollar' => $faker->numberBetween(1000, 2000),
        // 'parent_asset_non_retirement_other_securities_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_asset_non_retirement_annuities_dollar' => $faker->numberBetween(1000, 2000),
        // 'parent_asset_non_retirement_annuities_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_asset_non_retirement_other_investments_dollar' => $faker->numberBetween(1000, 2000),
        // 'parent_asset_non_retirement_other_investments_dollar_total' => $faker->numberBetween(10000, 20000),
        // Asset - retirement
        'parent_asset_retirement_defined_benefit_dollar' => $faker->numberBetween(1000, 2000),
        // 'parent_asset_retirement_defined_benefit_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_asset_retirement_four_o_one_dollar' => $faker->numberBetween(10000, 20000),
        // 'parent_asset_retirement_four_o_one_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_asset_retirement_ira_dollar' => $faker->numberBetween(1000, 2000),
        // 'parent_asset_retirement_ira_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_asset_retirement_roth_ira_dollar' => $faker->numberBetween(1000, 2000),
        // 'parent_asset_retirement_roth_ira_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_asset_retirement_previous_employer_dollar' => $faker->numberBetween(1000, 2000),
        // 'parent_asset_retirement_previous_employer_dollar_total' => $faker->numberBetween(10000, 20000),
        'parent_asset_retirement_other_dollar' => $faker->numberBetween(1000, 2000),
        // 'parent_asset_retirement_other_dollar_total' => $faker->numberBetween(10000, 20000),
        // Real-estate
        'parent_asset_real_estate_options' => ["1", "2", "3", "4"],
        'parent_asset_real_estate_primary_purchase_dollar' => $faker->numberBetween(10000, 20000),
        'parent_asset_real_estate_primary_value_dollar' => $faker->numberBetween(10000, 20000),
        'parent_asset_real_estate_primary_owned_dollar' => $faker->numberBetween(10000, 20000),
        'parent_asset_real_estate_primary_interest_percent' => $faker->numberBetween(1, 5),
        'parent_asset_real_estate_primary_payment_dollar' => $faker->numberBetween(10000, 20000),
        'parent_asset_real_estate_other_purchase_dollar' => $faker->numberBetween(10000, 20000),
        'parent_asset_real_estate_other_value_dollar' => $faker->numberBetween(10000, 20000),
        'parent_asset_real_estate_other_owned_dollar' => $faker->numberBetween(10000, 20000),
        'parent_asset_real_estate_other_interest_percent' => $faker->numberBetween(1, 5),
        'parent_asset_real_estate_other_payment_dollar' => $faker->numberBetween(10000, 20000),
        // Business farm
        'parent_business_value_dollar' => $faker->numberBetween(10000, 20000),
        'parent_farm_value_dollar' => $faker->numberBetween(10000, 20000),
        // case creation
        'household_dependent_children_num' => $faker->numberBetween(2, 5),
        'is_parent_asset_non_retirement' => 1,
        'parent_asset_non_retirement_dollar_total' => $faker->numberBetween(10000, 200000),
        'is_parent_asset_education' => 1,
        'parent_asset_education_dollar_total' => $faker->numberBetween(10000, 200000),
        'is_parent_asset_retirement_accounts' => 1,
        'parent_asset_retirement_dollar_total' => $faker->numberBetween(10000, 200000),
        'parent_opinion_stretch_household_income' => $faker->numberBetween(1, 10),
        'parent_opinion_jeopardize_retirement' => $faker->numberBetween(1, 10),
        'parent_opinion_ways_to_borrow' => $faker->numberBetween(1, 10),
        'parent_opinion_cashing_investments' => $faker->numberBetween(1, 10),
        'parent_opinion_no_clear_plan' => $faker->numberBetween(1, 10),
        'parent_opinion_qualify_for_financial_aid' => $faker->numberBetween(1, 10),
        // efc
        'efc_school_year' => "2019 - 2020",
//        'child_support_options' => $faker->randomElements(["1", "2"], $faker->numberBetween(1, 2)),
//        'pay_child_support' => $faker->numberBetween(10000, 20000),
//        'receive_child_support' => $faker->numberBetween(10000, 20000),
//        'adjusted_gross_income' => $faker->numberBetween(100000, 200000),
//        'income_tax' => $faker->numberBetween(10000, 20000),
//        'is_own_business' => $faker->numberBetween(0, 1),
//        'net_worth_business_farm' => $faker->numberBetween(50000, 200000),
//        'is_other_family_members' => $faker->numberBetween(0, 1),
//        'number_of_other_family_members' => $faker->numberBetween(1, 4),

        'is_completed_assessment' => 1,
        'assessment_current_step_num' => 1,

        'subscription' => 'BUNDLE',

        'source' => $faker->word,
    ];
});

$factory->define(User::class, function (Faker $faker) {
    return [
        'family_id' => function () {
            return factory(Family::class)->create()->id;
        },
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'mobile' => $faker->numberBetween(1000000000, 9999999999),
        'role' => $faker->randomElement(['PARENT', 'ADVISOR', 'STUDENT']),
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'tmp_password' => 'secret',
        'is_account' => 1,
    ];
});

$factory->define(FamilyParent::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(User::class)->create()->id;
        },
        'family_id' => function () {
            return factory(Family::class)->create()->id;
        },
        'role_id' => $faker->numberBetween(1, 5),
        'marital_status_id' => $faker->numberBetween(1, 5),
        'employment_status_id' => $faker->numberBetween(1, 7),
        'state_id' => function () {
            return State::inRandomOrder()->first()->id;
        },
        'birth_date' => $faker->dateTimeBetween('1975-01-01 00:00:00', now()) ,
        // Income - info
        'income_work_dollar' => $faker->numberBetween(76000, 90000),
        'income_adjusted_gross_dollar' => $faker->numberBetween(50000, 75000),
        // Income - tax
        'income_tax_dollar' => $faker->numberBetween(1000, 2000),
        // Income - additional
        'income_additional_tax_credit_dollar' => $faker->numberBetween(500, 1000),
        'income_additional_child_support_paid_dollar' => $faker->numberBetween(500, 1000),
        'income_additional_work_study_dollar' => $faker->numberBetween(500, 1000),
        'income_additional_grant_scholarship_dollar' => $faker->numberBetween(500, 1000),
        'income_additional_combat_pay_dollar' => $faker->numberBetween(500, 1000),
        'income_additional_cooperative_education_dollar' => $faker->numberBetween(500, 1000),
        // Income - untaxed
        'income_untaxed_tax_deferred_dollar' => $faker->numberBetween(500, 1000),
        'income_untaxed_ira_deduction_dollar' => $faker->numberBetween(500, 1000),
        'income_untaxed_child_support_received_dollar' => $faker->numberBetween(500, 1000),
        'income_untaxed_tax_exempt_dollar' => $faker->numberBetween(500, 1000),
        'income_untaxed_ira_distribution_dollar' => $faker->numberBetween(500, 1000),
        'income_untaxed_pension_portion_dollar' => $faker->numberBetween(500, 1000),
        'income_untaxed_paid_to_military_dollar' => $faker->numberBetween(500, 1000),
        'income_untaxed_veteran_benefit_dollar' => $faker->numberBetween(500, 1000),
        'income_untaxed_other_dollar' => $faker->numberBetween(500, 1000),
        // Asset education
        'asset_education_student_sibling_dollar' => $faker->numberBetween(500, 1000),
        'asset_education_five_two_nine_dollar' => $faker->numberBetween(500, 1000),
        'asset_education_coverdell_dollar' => $faker->numberBetween(500, 1000),
        // Asset - non-retirement
        'asset_non_retirement_checking_savings_dollar' => $faker->numberBetween(5000, 10000),
        'asset_non_retirement_certificate_of_deposit_dollar' => $faker->numberBetween(1000, 2000),
        'asset_non_retirement_t_bills_dollar' => $faker->numberBetween(1000, 2000),
        'asset_non_retirement_money_market_dollar' => $faker->numberBetween(1000, 2000),
        'asset_non_retirement_mutual_dollar' => $faker->numberBetween(1000, 2000),
        'asset_non_retirement_stock_dollar' => $faker->numberBetween(1000, 2000),
        'asset_non_retirement_bond_dollar' => $faker->numberBetween(1000, 2000),
        'asset_non_retirement_trust_dollar' => $faker->numberBetween(1000, 2000),
        'asset_non_retirement_other_securities_dollar' => $faker->numberBetween(1000, 2000),
        'asset_non_retirement_annuities_dollar' => $faker->numberBetween(1000, 2000),
        'asset_non_retirement_other_investments_dollar' => $faker->numberBetween(1000, 2000),
        // Asset - retirement
        'asset_retirement_defined_benefit_dollar' => $faker->numberBetween(1000, 2000),
        'asset_retirement_four_o_one_dollar' => $faker->numberBetween(150000, 175000),
        'asset_retirement_ira_dollar' => $faker->numberBetween(1000, 2000),
        'asset_retirement_roth_ira_dollar' => $faker->numberBetween(1000, 2000),
        'asset_retirement_previous_employer_dollar' => $faker->numberBetween(1000, 2000),
        'asset_retirement_other_dollar' => $faker->numberBetween(1000, 2000),
        // Insurance
        'insurance_permanent_cash_value_dollar' => $faker->numberBetween(25000, 30000),
    ];
});

$factory->define(FamilyChild::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(User::class)->create()->id;
        },
        'family_id' => function () {
            return factory(Family::class)->create()->id;
        },
        'grade_level_id' => $faker->numberBetween(1, 23),
        'relationship_parent_one_id' => $faker->numberBetween(1, 8),
        'relationship_parent_two_id' => $faker->numberBetween(1, 8),
        // Income - info
        'income_work_dollar' => $faker->numberBetween(3000, 4000),
        'income_adjusted_gross_dollar' => $faker->numberBetween(3000, 4000),
        // Income - tax
        'income_tax_dollar' => $faker->numberBetween(300, 400),
        // Income - additional
        'income_additional_tax_credit_dollar' => $faker->numberBetween(100, 150),
        'income_additional_child_support_paid_dollar' => $faker->numberBetween(100, 150),
        'income_additional_work_study_dollar' => $faker->numberBetween(100, 150),
        'income_additional_grant_scholarship_dollar' => $faker->numberBetween(100, 150),
        'income_additional_combat_pay_dollar' => $faker->numberBetween(100, 150),
        'income_additional_cooperative_education_dollar' => $faker->numberBetween(100, 150),
        // Income - untaxed
        'income_untaxed_tax_deferred_dollar' => $faker->numberBetween(50, 100),
        'income_untaxed_ira_deduction_dollar' => $faker->numberBetween(50, 100),
        'income_untaxed_child_support_received_dollar' => $faker->numberBetween(50, 100),
        'income_untaxed_tax_exempt_dollar' => $faker->numberBetween(50, 100),
        'income_untaxed_ira_distribution_dollar' => $faker->numberBetween(50, 100),
        'income_untaxed_pension_portion_dollar' => $faker->numberBetween(50, 100),
        'income_untaxed_paid_to_military_dollar' => $faker->numberBetween(50, 100),
        'income_untaxed_veteran_benefit_dollar' => $faker->numberBetween(50, 100),
        'income_untaxed_other_dollar' => $faker->numberBetween(50, 100),
        'income_untaxed_student_behalf_dollar' => $faker->numberBetween(50, 100),
        // Asset - non retirement
        'asset_non_retirement_checking_savings_dollar' => $faker->numberBetween(1000, 20000),
        'asset_non_retirement_ugma_utma_dollar' => $faker->numberBetween(100, 200),
        'asset_non_retirement_certificate_of_deposit_dollar' => $faker->numberBetween(100, 200),
        'asset_non_retirement_t_bills_dollar' => $faker->numberBetween(100, 200),
        'asset_non_retirement_money_market_dollar' => $faker->numberBetween(100, 200),
        'asset_non_retirement_mutual_dollar' => $faker->numberBetween(100, 200),
        'asset_non_retirement_stock_dollar' => $faker->numberBetween(100, 200),
        'asset_non_retirement_bond_dollar' => $faker->numberBetween(100, 200),
        'asset_non_retirement_trust_dollar' => $faker->numberBetween(100, 200),
        'asset_non_retirement_other_investments_dollar' => $faker->numberBetween(100, 200),

        'is_in_college' => 1,
    ];
});
