<?php

use App\Models\Core\User;
use App\Repositories\Misc\DataSeederRepository;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->createAdvisorUsers();

        // for local env, call these seeders
        if (!in_array(strtoupper(config('app.env')), ['PROD', 'PRODUCTION', "LIVE"])) {
            (new DataSeederRepository())->createFamily(
                $numberOfParents = 2,
                $numberOfChildren = 5,
                $completedAssessment = 1,
                $completedEfc = 0,
                $advisorId = null,
                $subscription = null
            );
            // $this->createAdvisorData();
        }
    }

    /**
     * Create advisor needed
     */
    private function createAdvisorUsers()
    {
        factory(User::class)->create([
            'first_name' => 'Test',
            'last_name' => 'Advisor 1',
            'email' => 'testadvisor1@smarttrackcollegefunding.com',
            'mobile' => null,
            'family_id' => null,
            'role' => 'ADVISOR',
            'acuity_calendar_id' => 3134195,
        ]);
        factory(User::class)->create([
            'first_name' => 'Test',
            'last_name' => 'Advisor 2',
            'email' => 'testadvisor2@smarttrackcollegefunding.com',
            'mobile' => null,
            'family_id' => null,
            'role' => 'ADVISOR',
            'acuity_calendar_id' => 3134199,
        ]);
        factory(User::class)->create([
            'first_name' => 'Test',
            'last_name' => 'Advisor 3',
            'email' => 'testadvisor3@smarttrackcollegefunding.com',
            'mobile' => null,
            'family_id' => null,
            'role' => 'ADVISOR',
            'acuity_calendar_id' => 3134202,
        ]);
    }

    /**
     * Create dummy advisor data
     */
    private function createAdvisorData()
    {
        $advisors = factory(User::class, 1)->create([
            'family_id' => null,
            'role' => 'ADVISOR'
        ]);

        // for each advisor create 11 cases
        foreach ($advisors as $advisor) {
            for ($i=0; $i < 11; $i++) {
                (new DataSeederRepository())->createFamily(
                    $numberOfParents = 2,
                    $numberOfChildren = 4,
                    $completedAssessment = 1,
                    $completedEfc = 0,
                    $advisorId = $advisor->id,
                    $subscription = 'BUNDLE'
                );
            }
        }
    }
}
