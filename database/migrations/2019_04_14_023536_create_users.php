<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('family_id')->nullable();
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('slug');
            $table->string('email')->nullable();
            $table->unsignedBigInteger('mobile')->nullable();
            $table->string('role');
            $table->string('password');
            $table->string('tmp_password');
            $table->boolean('is_account')->default(0);
            $table->unsignedBigInteger('acuity_calendar_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('family_id')->references('id')->on('families');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
