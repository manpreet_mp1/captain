<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInputValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('input_values', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type')->index();
            $table->string('label');
            $table->string('value')->index();
            $table->string('image')->nullable();
            $table->unsignedInteger('order_seq');
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['type', 'value', 'deleted_at']);
        });

        Artisan::call('DataPopulate:InputValuesTableCommand');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('input_values');
    }
}
