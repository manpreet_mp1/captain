<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormBlades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_blades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type');
            $table->string('blade_name');
            $table->string('menu_name')->nullable();
            $table->string('menu_sub_text')->nullable();
            $table->string('icon_class')->nullable();
            $table->boolean('is_visible_in_menu')->default(1);
            $table->string('active_class_blade_name')->nullable();
            $table->unsignedInteger('order_seq');
            $table->timestamps();
            $table->softDeletes();
        });
        Artisan::call('DataPopulate:FormBladeCommand');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_blades');
    }
}
