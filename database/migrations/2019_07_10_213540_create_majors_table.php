<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMajorsTable extends Migration
{
    /**
     * The database schema.
     *
     * @var Schema
     */
    protected $schema;

    /**
     * Create a new migration instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->schema = Schema::connection(config('database.data'));
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('database.runOtherDbMigration')) {
            ini_set('memory_limit', '-1');
            DB::connection('ipeds')->select("ALTER TABLE C2017_A ADD INDEX (CIPCODE);");
            DB::connection('ipeds')->statement("ALTER TABLE C2017_A CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci");

            $this->schema->create('majors', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('family')->index();
                $table->string('code')->index();
                $table->string('type')->index();
                $table->string('title');
                $table->string('slug')->index()->nullable();
                $table->text('definition');
                $table->unsignedInteger('associate_count')->nullable();
                $table->unsignedInteger('associate_percent')->nullable();
                $table->unsignedInteger('bachelor_count')->nullable();
                $table->unsignedInteger('bachelor_percent')->nullable();
                $table->unsignedInteger('certificate_count')->nullable();
                $table->unsignedInteger('certificate_percent')->nullable();
                $table->unsignedInteger('doctor_count')->nullable();
                $table->unsignedInteger('doctor_percent')->nullable();
                $table->unsignedInteger('master_count')->nullable();
                $table->unsignedInteger('master_percent')->nullable();
                $table->unsignedInteger('other_count')->nullable();
                $table->unsignedInteger('other_percent')->nullable();
                $table->unsignedInteger('male_count')->nullable();
                $table->unsignedInteger('male_percent')->nullable();
                $table->unsignedInteger('female_count')->nullable();
                $table->unsignedInteger('female_percent')->nullable();
                $table->unsignedInteger('white_count')->nullable();
                $table->unsignedInteger('white_percent')->nullable();
                $table->unsignedInteger('indian_count')->nullable();
                $table->unsignedInteger('indian_percent')->nullable();
                $table->unsignedInteger('asian_count')->nullable();
                $table->unsignedInteger('asian_percent')->nullable();
                $table->unsignedInteger('black_count')->nullable();
                $table->unsignedInteger('black_percent')->nullable();
                $table->unsignedInteger('hispanic_count')->nullable();
                $table->unsignedInteger('hispanic_percent')->nullable();
                $table->unsignedInteger('native_count')->nullable();
                $table->unsignedInteger('native_percent')->nullable();
                $table->unsignedInteger('two_or_more_count')->nullable();
                $table->unsignedInteger('two_or_more_percent')->nullable();
                $table->unsignedInteger('non_resident_count')->nullable();
                $table->unsignedInteger('non_resident_percent')->nullable();
                $table->unsignedInteger('unknown_count')->nullable();
                $table->unsignedInteger('unknown_percent')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });

            $this->schema->create('major_colleges', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('code')->index();
                $table->unsignedInteger('unitid')->index();
                $table->string('name');
                $table->string('slug');
                $table->string('size_of_college');
                $table->unsignedInteger('degree_count')->nullable();
                $table->unsignedInteger('associate_count')->nullable();
                $table->unsignedInteger('bachelor_count')->nullable();
                $table->unsignedInteger('certificate_count')->nullable();
                $table->unsignedInteger('doctor_count')->nullable();
                $table->unsignedInteger('master_count')->nullable();
                $table->unsignedInteger('other_count')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });

            Artisan::call('DataManipulate:EtlJobMajorsTableCommand');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (config('database.runOtherDbMigration')) {
            $this->schema->dropIfExists('majors');
            $this->schema->dropIfExists('major_colleges');
        }
    }
}
