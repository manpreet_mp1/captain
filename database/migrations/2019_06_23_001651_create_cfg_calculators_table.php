<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCfgCalculatorsTable extends Migration
{

    /**
     * The database schema.
     *
     * @var Schema
     */
    protected $schema;

    /**
     * Create a new migration instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->schema = Schema::connection(config('database.data'));
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('database.runOtherDbMigration')) {
            ini_set('memory_limit', '-1');
            $dataDbName = config('database.connections.data.database');
            $wpDBName = config('database.connections.corcel.database');
            $ipedsDbName = config('database.connections.ipeds.database');
            DB::connection('mysql')->select("CREATE DATABASE {$dataDbName};");
            DB::connection('mysql')->select("CREATE DATABASE {$wpDBName};");
            DB::connection('mysql')->select("CREATE DATABASE {$ipedsDbName};");

            $this->schema->create('cfg_calculators', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('key')->index();
                $table->decimal('value', 20, 4);
                $table->string('description');
                $table->string('type');
                $table->string('calculators');
                $table->timestamps();
                $table->softDeletes();
            });

            Artisan::call('DataPopulate:CfgCalculatorsCommand');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (config('database.runOtherDbMigration')) {
            $this->schema->dropIfExists('cfg_calculators');
        }
    }
}
