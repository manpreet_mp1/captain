<?php
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFiveTwoNineTables extends Migration
{
    /**
     * The database schema.
     *
     * @var Schema
     */
    protected $schema;

    /**
     * Create a new migration instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->schema = Schema::connection(config('database.data'));
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('database.runOtherDbMigration')) {
            ini_set('memory_limit', '-1');
            $this->schema->create('five_two_nine_basics', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('plan_id')->index();
                $table->unsignedBigInteger('state_id')->index()->nullable();
                $table->string('name')->nullable();
                $table->string('slug')->nullable();
                $table->string('type_1')->nullable();
                $table->string('type_2')->nullable();
                $table->string('type_3_key')->nullable();
                $table->string('type_3_name')->nullable();
                $table->string('state_name')->nullable();
                $table->string('website')->nullable();
                $table->string('phone')->nullable();
                $table->string('residency')->nullable();
                $table->text('residency_details')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            $this->schema->create('five_two_nine_contacts', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('plan_id')->index();
                $table->string('name')->nullable();
                $table->string('address_line_1')->nullable();
                $table->string('address_line_2')->nullable();
                $table->string('city_state_zip')->nullable();
                $table->string('phone')->nullable();
                $table->string('toll_free')->nullable();
                $table->string('fax')->nullable();
                $table->string('email')->nullable();
                $table->string('home_url')->nullable();
                $table->string('offering_material_url', 500)->nullable();
                $table->string('enrollment_material_url', 500)->nullable();
                $table->string('online_account_access_url', 500)->nullable();
                $table->string('general_performance_url', 500)->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            $this->schema->create('five_two_nine_investments', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('plan_id')->index();
                $table->unsignedBigInteger('option_id')->index();
                $table->string('name')->nullable();
                $table->string('option_type', 500)->nullable();
                $table->string('sub_option', 500)->nullable();
                $table->string('manager', 500)->nullable();
                $table->string('residency_requirements', 500)->nullable();
                $table->string('enrollment_fee', 500)->nullable();
                $table->string('minimum_initial_contribution', 500)->nullable();
                $table->string('minimum_subsequent_contribution', 500)->nullable();
                $table->string('maximum_total_contribution', 500)->nullable();
                $table->string('performance_url', 500)->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            $this->schema->create('five_two_nine_investment_first_details', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('plan_id')->index();
                $table->unsignedBigInteger('option_id')->index();
                $table->string('sub_option', 500)->nullable();
                $table->string('fees_structure', 500)->nullable();
                $table->string('heading_name', 500)->nullable();
                $table->string('heading_value', 500)->nullable();
                $table->string('estimated_underlying_fund_expenses', 500)->nullable();
                $table->string('program_manager_fee', 500)->nullable();
                $table->string('state_fee', 500)->nullable();
                $table->string('misc_fees', 500)->nullable();
                $table->string('annual_distribution_fee', 500)->nullable();
                $table->string('total_annual_asset_based_fees', 500)->nullable();
                $table->string('maximum_initial_sales_charge', 500)->nullable();
                $table->string('annual_account_maintenance_fee', 500)->nullable();
                $table->text('content_before')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            $this->schema->create('five_two_nine_investment_second_details', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('plan_id')->index();
                $table->unsignedBigInteger('option_id')->index();
                $table->string('sub_option', 500)->nullable();
                $table->string('redemption', 500)->nullable();
                $table->string('fees_structure', 500)->nullable();
                $table->string('heading_name', 500)->nullable();
                $table->string('heading_value', 500)->nullable();
                $table->string('1_year', 500)->nullable();
                $table->string('3_year', 500)->nullable();
                $table->string('5_year', 500)->nullable();
                $table->string('10_year', 500)->nullable();
                $table->text('content_after')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            $this->schema->create('five_two_nine_managements', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('plan_id')->index();
                $table->string('name', 500)->nullable();
                $table->string('type', 500)->nullable();
                $table->string('type_key', 500)->nullable();
                $table->string('manager_name', 500)->nullable();
                $table->string('contract_termination_date', 500)->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            $this->schema->create('five_two_nine_benefits', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('plan_id')->index();
                $table->string('name', 500)->nullable();
                $table->text('benefit_heading')->nullable();
                $table->text('benefit_details')->nullable();
                $table->text('additional_information')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            $this->schema->create('five_two_nine_performances', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('plan_id')->index();
                $table->unsignedBigInteger('option_id')->index();
                $table->string('sub_option', 500)->nullable();
                $table->string('heading_name_1', 500)->nullable();
                $table->string('class_name', 500)->nullable();
                $table->string('1_year', 500)->nullable();
                $table->string('3_year', 500)->nullable();
                $table->string('5_year', 500)->nullable();
                $table->string('10_year', 500)->nullable();
                $table->string('since_inception', 500)->nullable();
                $table->string('inception_date', 500)->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            $this->schema->create('five_two_nine_prepaids', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('plan_id')->index();
                $table->string('name', 500)->nullable();
                $table->text('requirements_for_participation')->nullable();
                $table->text('requirements_for_participation_url')->nullable();
                $table->text('enrollment_period')->nullable();
                $table->text('enrollment_period_url')->nullable();
                $table->text('form_of_state_backing')->nullable();
                $table->text('form_of_state_backing_url')->nullable();
                $table->text('tuition_plans_and_payment_options')->nullable();
                $table->text('tuition_plans_url')->nullable();
                $table->text('tuition_benefits_information')->nullable();
                $table->text('tuition_benefits_url')->nullable();
                $table->text('refunds')->nullable();
                $table->text('refunds_url')->nullable();
                $table->text('additional_information')->nullable();
                $table->text('additional_information_url')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            $this->schema->create('five_two_nine_state_descriptions', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('state_id');
                $table->string('name', 500)->nullable();
                $table->string('abbr', 500)->nullable();
                $table->text('description')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            $this->schema->create('five_two_nine_dropdown_types', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('value', 500)->nullable()->index();
                $table->string('label', 500)->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            $this->schema->create('five_two_nine_masters', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('plan_id')->index();
                $table->unsignedBigInteger('state_id')->index();
                $table->string('name', 500)->nullable();
                $table->string('slug')->nullable();
                $table->string('type_1')->nullable();
                $table->string('type_2')->nullable();
                $table->string('type_3_key')->nullable();
                $table->string('type_3_name')->nullable();
                $table->string('state_abbr')->nullable();
                $table->string('state_name')->nullable();
                $table->string('website')->nullable();
                $table->string('plan_phone')->nullable();
                $table->string('address_line_1')->nullable();
                $table->string('address_line_2')->nullable();
                $table->string('city_state_zip')->nullable();
                $table->string('phone')->nullable();
                $table->string('toll_free')->nullable();
                $table->string('fax')->nullable();
                $table->string('email')->nullable();
                $table->string('home_url')->nullable();
                $table->string('offering_material_url', 500)->nullable();
                $table->string('enrollment_material_url', 500)->nullable();
                $table->string('online_account_access_url', 500)->nullable();
                $table->string('general_performance_url', 500)->nullable();
                $table->string('residency')->nullable();
                $table->text('residency_details')->nullable();
                $table->text('pm_name')->nullable();
                $table->text('im_name')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });

            Artisan::call('DataPopulate:FiveTwoNineTablesCommand');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (config('database.runOtherDbMigration')) {
            $this->schema->dropIfExists('five_two_nine_basics');
            $this->schema->dropIfExists('five_two_nine_contacts');
            $this->schema->dropIfExists('five_two_nine_investments');
            $this->schema->dropIfExists('five_two_nine_investment_first_details');
            $this->schema->dropIfExists('five_two_nine_investment_second_details');
            $this->schema->dropIfExists('five_two_nine_managements');
            $this->schema->dropIfExists('five_two_nine_benefits');
            $this->schema->dropIfExists('five_two_nine_performances');
            $this->schema->dropIfExists('five_two_nine_prepaids');
            $this->schema->dropIfExists('five_two_nine_state_descriptions');
            $this->schema->dropIfExists('five_two_nine_dropdown_types');
            $this->schema->dropIfExists('five_two_nine_masters');
        }
    }
}
