<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScenariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scenarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            // basic info
            $table->unsignedBigInteger('family_id')->index();
            $table->unsignedBigInteger('family_child_id')->index();
            $table->string('name');
            $table->boolean('is_shared_with_family')->default(0)->index();
            $table->string('slug')->unique();
            $table->text('summary');
            // college list
            $table->unsignedBigInteger('college_year')->default(1);
            $table->unsignedBigInteger('college_id_1')->default(166027);
            $table->unsignedBigInteger('college_id_2')->default(0);
            $table->unsignedBigInteger('college_id_3')->default(0);
            $table->unsignedBigInteger('college_id_4')->default(0);
            // student fields
            $table->unsignedInteger('student_income_adjusted_gross_dollar')->default(0);
            $table->unsignedInteger('student_income_work_dollar')->default(0);
            $table->unsignedInteger('student_income_tax_dollar')->default(0);
            $table->unsignedInteger('student_income_untaxed_dollar_total')->default(0);
            $table->unsignedInteger('student_income_additional_dollar_total')->default(0);
            $table->unsignedInteger('student_asset_non_retirement_checking_savings_dollar')->default(0);
            $table->unsignedInteger('student_asset_non_retirement_ugma_utma_dollar')->default(0);
            $table->unsignedInteger('student_asset_non_retirement_certificate_of_deposit_dollar')->default(0);
            $table->unsignedInteger('student_asset_non_retirement_t_bills_dollar')->default(0);
            $table->unsignedInteger('student_asset_non_retirement_money_market_dollar')->default(0);
            $table->unsignedInteger('student_asset_non_retirement_mutual_dollar')->default(0);
            $table->unsignedInteger('student_asset_non_retirement_stock_dollar')->default(0);
            $table->unsignedInteger('student_asset_non_retirement_bond_dollar')->default(0);
            $table->unsignedInteger('student_asset_non_retirement_trust_dollar')->default(0);
            $table->unsignedInteger('student_asset_non_retirement_other_investments_dollar')->default(0);
            // parent fields
            $table->unsignedInteger('parent_income_adjusted_gross_dollar_total')->default(0);
            $table->unsignedInteger('parent_1_income_work_dollar')->default(0);
            $table->unsignedInteger('parent_2_income_work_dollar')->default(0);
            $table->unsignedInteger('parent_income_tax_dollar_total')->default(0);
            $table->unsignedInteger('parent_income_untaxed_dollar_total')->default(0);
            $table->unsignedInteger('parent_income_additional_dollar_total')->default(0);
            $table->unsignedInteger('parent_asset_non_retirement_checking_savings_dollar_total')->default(0);
            $table->unsignedInteger('parent_asset_non_retirement_certificate_of_deposit_dollar_total')->default(0);
            $table->unsignedInteger('parent_asset_non_retirement_t_bills_dollar_total')->default(0);
            $table->unsignedInteger('parent_asset_non_retirement_money_market_dollar_total')->default(0);
            $table->unsignedInteger('parent_asset_non_retirement_mutual_dollar_total')->default(0);
            $table->unsignedInteger('parent_asset_non_retirement_stock_dollar_total')->default(0);
            $table->unsignedInteger('parent_asset_non_retirement_bond_dollar_total')->default(0);
            $table->unsignedInteger('parent_asset_non_retirement_trust_dollar_total')->default(0);
            $table->unsignedInteger('parent_asset_non_retirement_other_securities_dollar_total')->default(0);
            $table->unsignedInteger('parent_asset_non_retirement_annuities_dollar_total')->default(0);
            $table->unsignedInteger('parent_asset_non_retirement_other_investments_dollar_total')->default(0);
            $table->unsignedInteger('parent_asset_education_student_sibling_dollar_total')->default(0);
            $table->unsignedInteger('parent_asset_education_five_two_nine_dollar_total')->default(0);
            $table->unsignedInteger('parent_asset_education_coverdell_dollar_total')->default(0);
            $table->unsignedInteger('parent_asset_retirement_defined_benefit_dollar_total')->default(0);
            $table->unsignedInteger('parent_asset_retirement_four_o_one_dollar_total')->default(0);
            $table->unsignedInteger('parent_asset_retirement_ira_dollar_total')->default(0);
            $table->unsignedInteger('parent_asset_retirement_roth_ira_dollar_total')->default(0);
            $table->unsignedInteger('parent_asset_retirement_other_dollar_total')->default(0);
            $table->unsignedInteger('parent_insurance_permanent_cash_value_dollar_total')->default(0);
            $table->unsignedInteger('parent_farm_net_worth_dollar')->default(0);
            $table->unsignedInteger('parent_business_net_worth_dollar')->default(0);
            $table->unsignedInteger('parent_asset_real_estate_primary_net_worth_dollar')->default(0);
            $table->unsignedInteger('parent_asset_real_estate_other_net_worth_dollar')->default(0);
            // saving strategies, planning
            $table->unsignedInteger('savings_tax_planning_min')->default(0);
            $table->unsignedInteger('savings_tax_planning_max')->default(0);
            $table->unsignedInteger('savings_assets_management_min')->default(0);
            $table->unsignedInteger('savings_assets_management_max')->default(0);
            $table->unsignedInteger('savings_borrowing_loan_min')->default(0);
            $table->unsignedInteger('savings_borrowing_loan_max')->default(0);
            $table->unsignedInteger('savings_cashflow_improvement_min')->default(0);
            $table->unsignedInteger('savings_cashflow_improvement_max')->default(0);
            $table->unsignedInteger('savings_other_min')->default(0);
            $table->unsignedInteger('savings_other_max')->default(0);
            $table->unsignedInteger('savings_outside_scholarship_min')->default(0);
            $table->unsignedInteger('savings_outside_scholarship_max')->default(0);
            $table->unsignedInteger('savings_other_financial_other_min')->default(0);
            $table->unsignedInteger('savings_other_financial_other_max')->default(0);
            $table->unsignedInteger('planning_retirement_age')->default(65);
            $table->decimal('planning_investment_rate', 4, 2)->default(5);
            // planned contribution
            $table->unsignedInteger('contributions_parent_annual_income_dollar')->default(0);
            $table->unsignedInteger('contributions_parent_assets_education_dollar')->default(0);
            $table->unsignedInteger('contributions_parent_assets_non_retirement_dollar')->default(0);
            $table->unsignedInteger('contributions_parent_assets_retirement_dollar')->default(0);
            $table->unsignedInteger('contributions_parent_life_insurance_dollar')->default(0);
            $table->unsignedInteger('contributions_parent_business_farm_dollar')->default(0);
            $table->unsignedInteger('contributions_parent_real_estate_dollar')->default(0);
            $table->unsignedInteger('contributions_parent_plus_private_loans_dollar')->default(0);
            $table->unsignedInteger('contributions_parent_other_contribution_dollar')->default(0);
            $table->unsignedInteger('contributions_student_annual_income_dollar')->default(0);
            $table->unsignedInteger('contributions_student_total_asset_dollar')->default(0);
            $table->unsignedInteger('contributions_student_loans_dollar')->default(0);
            $table->unsignedInteger('contributions_student_other_contribution_dollar')->default(0);
            // html comments
            $table->text('comments_parent_income')->nullable();
            $table->text('comments_parent_assets_education')->nullable();
            $table->text('comments_parent_assets_non_retirement')->nullable();
            $table->text('comments_parent_assets_retirement')->nullable();
            $table->text('comments_parent_assets_insurance')->nullable();
            $table->text('comments_parent_assets_business_farm')->nullable();
            $table->text('comments_parent_assets_real_estate')->nullable();
            $table->text('comments_student_income')->nullable();
            $table->text('comments_student_asset')->nullable();
            $table->text('comments_planned_parent_contribution')->nullable();
            $table->text('comments_planned_student_contribution')->nullable();
            $table->text('comments_savings_financial')->nullable();
            $table->text('comments_savings_other')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scenarios');
    }
}
