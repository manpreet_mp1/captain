<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFamiliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('families', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('advisor_id')->nullable()->index();
            $table->string('name');
            $table->string('slug');
            // Income - Info
            $table->unsignedInteger('parent_income_work_dollar_total')->nullable();
            $table->unsignedInteger('parent_income_adjusted_gross_dollar')->nullable();
            $table->unsignedInteger('parent_income_adjusted_gross_dollar_total')->nullable();
            $table->boolean('is_income_info_breakdown_completed')->default(0);
            // Income - Tax
            $table->unsignedInteger('parent_income_tax_dollar')->nullable();
            $table->unsignedInteger('parent_income_tax_dollar_total')->nullable();
            $table->boolean('is_income_tax_breakdown_completed')->default(0);
            // Income - Additional
            $table->unsignedInteger('parent_income_additional_tax_credit_dollar')->nullable();
            $table->unsignedInteger('parent_income_additional_tax_credit_dollar_total')->nullable();
            $table->unsignedInteger('parent_income_additional_child_support_paid_dollar')->nullable();
            $table->unsignedInteger('parent_income_additional_child_support_paid_dollar_total')->nullable();
            $table->unsignedInteger('parent_income_additional_work_study_dollar')->nullable();
            $table->unsignedInteger('parent_income_additional_work_study_dollar_total')->nullable();
            $table->unsignedInteger('parent_income_additional_grant_scholarship_dollar')->nullable();
            $table->unsignedInteger('parent_income_additional_grant_scholarship_dollar_total')->nullable();
            $table->unsignedInteger('parent_income_additional_combat_pay_dollar')->nullable();
            $table->unsignedInteger('parent_income_additional_combat_pay_dollar_total')->nullable();
            $table->unsignedInteger('parent_income_additional_cooperative_education_dollar')->nullable();
            $table->unsignedInteger('parent_income_additional_cooperative_education_dollar_total')->nullable();
            $table->boolean('is_income_additional_breakdown_completed')->default(0);
            // Income - untaxed
            $table->unsignedInteger('parent_income_untaxed_tax_deferred_dollar')->nullable();
            $table->unsignedInteger('parent_income_untaxed_tax_deferred_dollar_total')->nullable();
            $table->unsignedInteger('parent_income_untaxed_ira_deduction_dollar')->nullable();
            $table->unsignedInteger('parent_income_untaxed_ira_deduction_dollar_total')->nullable();
            $table->unsignedInteger('parent_income_untaxed_child_support_received_dollar')->nullable();
            $table->unsignedInteger('parent_income_untaxed_child_support_received_dollar_total')->nullable();
            $table->unsignedInteger('parent_income_untaxed_tax_exempt_dollar')->nullable();
            $table->unsignedInteger('parent_income_untaxed_tax_exempt_dollar_total')->nullable();
            $table->unsignedInteger('parent_income_untaxed_ira_distribution_dollar')->nullable();
            $table->unsignedInteger('parent_income_untaxed_ira_distribution_dollar_total')->nullable();
            $table->unsignedInteger('parent_income_untaxed_pension_portion_dollar')->nullable();
            $table->unsignedInteger('parent_income_untaxed_pension_portion_dollar_total')->nullable();
            $table->unsignedInteger('parent_income_untaxed_paid_to_military_dollar')->nullable();
            $table->unsignedInteger('parent_income_untaxed_paid_to_military_dollar_total')->nullable();
            $table->unsignedInteger('parent_income_untaxed_veteran_benefit_dollar')->nullable();
            $table->unsignedInteger('parent_income_untaxed_veteran_benefit_dollar_total')->nullable();
            $table->unsignedInteger('parent_income_untaxed_other_dollar')->nullable();
            $table->unsignedInteger('parent_income_untaxed_other_dollar_total')->nullable();
            $table->boolean('is_income_untaxed_breakdown_completed')->default(0);
            // Asset - education
            $table->unsignedInteger('parent_asset_education_student_sibling_dollar')->nullable();
            $table->unsignedInteger('parent_asset_education_student_sibling_dollar_total')->nullable();
            $table->unsignedInteger('parent_asset_education_five_two_nine_dollar')->nullable();
            $table->unsignedInteger('parent_asset_education_five_two_nine_dollar_total')->nullable();
            $table->unsignedInteger('parent_asset_education_coverdell_dollar')->nullable();
            $table->unsignedInteger('parent_asset_education_coverdell_dollar_total')->nullable();
            $table->boolean('is_assets_eduction_breakdown_completed')->default(0);
            // Asset - non retirement
            $table->unsignedInteger('parent_asset_non_retirement_checking_savings_dollar')->nullable();
            $table->unsignedInteger('parent_asset_non_retirement_checking_savings_dollar_total')->nullable();
            $table->unsignedInteger('parent_asset_non_retirement_certificate_of_deposit_dollar')->nullable();
            $table->unsignedInteger('parent_asset_non_retirement_certificate_of_deposit_dollar_total')->nullable();
            $table->unsignedInteger('parent_asset_non_retirement_t_bills_dollar')->nullable();
            $table->unsignedInteger('parent_asset_non_retirement_t_bills_dollar_total')->nullable();
            $table->unsignedInteger('parent_asset_non_retirement_money_market_dollar')->nullable();
            $table->unsignedInteger('parent_asset_non_retirement_money_market_dollar_total')->nullable();
            $table->unsignedInteger('parent_asset_non_retirement_mutual_dollar')->nullable();
            $table->unsignedInteger('parent_asset_non_retirement_mutual_dollar_total')->nullable();
            $table->unsignedInteger('parent_asset_non_retirement_stock_dollar')->nullable();
            $table->unsignedInteger('parent_asset_non_retirement_stock_dollar_total')->nullable();
            $table->unsignedInteger('parent_asset_non_retirement_bond_dollar')->nullable();
            $table->unsignedInteger('parent_asset_non_retirement_bond_dollar_total')->nullable();
            $table->unsignedInteger('parent_asset_non_retirement_trust_dollar')->nullable();
            $table->unsignedInteger('parent_asset_non_retirement_trust_dollar_total')->nullable();
            $table->unsignedInteger('parent_asset_non_retirement_other_securities_dollar')->nullable();
            $table->unsignedInteger('parent_asset_non_retirement_other_securities_dollar_total')->nullable();
            $table->unsignedInteger('parent_asset_non_retirement_annuities_dollar')->nullable();
            $table->unsignedInteger('parent_asset_non_retirement_annuities_dollar_total')->nullable();
            $table->unsignedInteger('parent_asset_non_retirement_other_investments_dollar')->nullable();
            $table->unsignedInteger('parent_asset_non_retirement_other_investments_dollar_total')->nullable();
            $table->boolean('is_assets_non_retirement_breakdown_completed')->default(0);
            // Asset - retirement
            $table->unsignedInteger('parent_asset_retirement_defined_benefit_dollar')->nullable();
            $table->unsignedInteger('parent_asset_retirement_defined_benefit_dollar_total')->nullable();
            $table->unsignedInteger('parent_asset_retirement_four_o_one_dollar')->nullable();
            $table->unsignedInteger('parent_asset_retirement_four_o_one_dollar_total')->nullable();
            $table->unsignedInteger('parent_asset_retirement_ira_dollar')->nullable();
            $table->unsignedInteger('parent_asset_retirement_ira_dollar_total')->nullable();
            $table->unsignedInteger('parent_asset_retirement_roth_ira_dollar')->nullable();
            $table->unsignedInteger('parent_asset_retirement_roth_ira_dollar_total')->nullable();
            $table->unsignedInteger('parent_asset_retirement_previous_employer_dollar')->nullable();
            $table->unsignedInteger('parent_asset_retirement_previous_employer_dollar_total')->nullable();
            $table->unsignedInteger('parent_asset_retirement_other_dollar')->nullable();
            $table->unsignedInteger('parent_asset_retirement_other_dollar_total')->nullable();
            $table->boolean('is_assets_retirement_breakdown_completed')->default(0);
            // Real-estate
            $table->string('parent_asset_real_estate_options')->nullable();
            $table->unsignedInteger('parent_asset_real_estate_primary_purchase_dollar')->nullable();
            $table->unsignedInteger('parent_asset_real_estate_primary_value_dollar')->nullable();
            $table->unsignedInteger('parent_asset_real_estate_primary_owned_dollar')->nullable();
            $table->double('parent_asset_real_estate_primary_interest_percent')->nullable();
            $table->unsignedInteger('parent_asset_real_estate_primary_payment_dollar')->nullable();
            $table->unsignedInteger('parent_asset_real_estate_other_purchase_dollar')->nullable();
            $table->unsignedInteger('parent_asset_real_estate_other_value_dollar')->nullable();
            $table->unsignedInteger('parent_asset_real_estate_other_owned_dollar')->nullable();
            $table->double('parent_asset_real_estate_other_interest_percent')->nullable();
            $table->unsignedInteger('parent_asset_real_estate_other_payment_dollar')->nullable();
            $table->boolean('is_real_estate_breakdown_completed')->default(0);
            // Business-farm
            $table->unsignedInteger('parent_business_value_dollar')->nullable();
            $table->unsignedInteger('parent_farm_value_dollar')->nullable();
            $table->boolean('is_business_breakdown_completed')->default(0);
            // Insurance
            $table->boolean('is_insurance_breakdown_completed')->default(0);
            // case creation
            $table->boolean('is_completed_assessment')->default(0);
            $table->unsignedInteger('assessment_current_step_num')->default(1);
            // number of children
            $table->unsignedInteger('household_dependent_children_num')->default(1);
            // case creation - asset
            $table->boolean('is_parent_asset_non_retirement')->default(0);
            $table->unsignedInteger('parent_asset_non_retirement_dollar_total')->nullable();
            $table->boolean('is_parent_asset_education')->default(0);
            $table->unsignedInteger('parent_asset_education_dollar_total')->nullable();
            $table->boolean('is_parent_asset_retirement_accounts')->default(0);
            $table->unsignedInteger('parent_asset_retirement_dollar_total')->nullable();
            // case creation - opinion scale
            $table->unsignedInteger('parent_opinion_stretch_household_income')->nullable();
            $table->unsignedInteger('parent_opinion_jeopardize_retirement')->nullable();
            $table->unsignedInteger('parent_opinion_ways_to_borrow')->nullable();
            $table->unsignedInteger('parent_opinion_cashing_investments')->nullable();
            $table->unsignedInteger('parent_opinion_no_clear_plan')->nullable();
            $table->unsignedInteger('parent_opinion_qualify_for_financial_aid')->nullable();

            // efc specific
            $table->string('efc_school_year')->nullable();
            $table->boolean('is_other_family_members')->default(0);
            $table->unsignedInteger('other_family_members_num')->nullable();
            $table->boolean('is_completed_efc')->default(0);

            $table->string('source')->nullable();

            $table->string('subscription')->nullable();
            $table->string('coupon_code')->nullable();
            $table->string('payment_customer_id')->nullable();
            $table->boolean('is_on_payment_plan')->default(0);
            $table->boolean('is_payment_declined')->default(0);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('families');
    }
}
