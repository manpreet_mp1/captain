<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEfcTables extends Migration
{
    /**
     * The database schema.
     *
     * @var Schema
     */
    protected $schema;

    /**
     * Create a new migration instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->schema = Schema::connection(config('database.data'));
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('database.runOtherDbMigration')) {
            ini_set('memory_limit', '-1');
            $this->schema->create('efc_table_a1', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('state_id');
                $table->string('state_name');
                $table->string('state_abbr');
                $table->decimal('state_tax_allowance_14', 20, 4);
                $table->decimal('state_tax_allowance_15', 20, 4);
                $table->timestamps();
                $table->softDeletes();
            });

            $this->schema->create('efc_table_a2', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('income_work')->index();
                $table->string('description');
                $table->decimal('min_value', 20, 4)->index();
                $table->decimal('max_value', 20, 4)->index();
                $table->decimal('percent', 20, 4);
                $table->decimal('fixed', 20, 4);
                $table->string('exceed');
                $table->timestamps();
                $table->softDeletes();
            });

            $this->schema->create('efc_table_a3', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedSmallInteger('num_household')->index();
                $table->unsignedSmallInteger('num_student_college')->index();
                $table->decimal('value', 20, 4);
                $table->timestamps();
                $table->softDeletes();
            });

            $this->schema->create('efc_table_a35', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('key')->index();
                $table->string('description');
                $table->decimal('percent', 20, 4);
                $table->decimal('fixed', 20, 4);
                $table->timestamps();
                $table->softDeletes();
            });

            $this->schema->create('efc_table_a4', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('description');
                $table->decimal('min_value', 20, 4)->index();
                $table->decimal('max_value', 20, 4)->index();
                $table->decimal('percent', 20, 4);
                $table->decimal('fixed', 20, 4);
                $table->string('exceed')->index();
                $table->timestamps();
                $table->softDeletes();
            });

            $this->schema->create('efc_table_a5', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('age_older_parent')->index();
                $table->decimal('two_parent_allowance', 20, 4);
                $table->decimal('one_parent_allowance', 20, 4);
                $table->timestamps();
                $table->softDeletes();
            });

            $this->schema->create('efc_table_a6', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('description');
                $table->decimal('min_value', 20, 4)->index();
                $table->decimal('max_value', 20, 4)->index();
                $table->decimal('percent', 20, 4);
                $table->decimal('fixed', 20, 4);
                $table->string('exceed')->index();
                $table->timestamps();
                $table->softDeletes();
            });

            $this->schema->create('efc_table_a7', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('state_id')->index();
                $table->string('state_name');
                $table->string('state_abbr');
                $table->decimal('percent', 20, 4);
                $table->timestamps();
                $table->softDeletes();
            });

            Artisan::call('DataPopulate:EfcTableCommand');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (config('database.runOtherDbMigration')) {
            $this->schema->dropIfExists('efc_table_a1');
            $this->schema->dropIfExists('efc_table_a2');
            $this->schema->dropIfExists('efc_table_a3');
            $this->schema->dropIfExists('efc_table_a35');
            $this->schema->dropIfExists('efc_table_a4');
            $this->schema->dropIfExists('efc_table_a5');
            $this->schema->dropIfExists('efc_table_a6');
            $this->schema->dropIfExists('efc_table_a7');
        }
    }
}
