<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFamilyParentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('family_parents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('family_id');
            $table->unsignedInteger('role_id')->nullable()->index();
            $table->unsignedInteger('marital_status_id')->nullable()->index();
            $table->unsignedInteger('employment_status_id')->nullable()->index();
            $table->unsignedInteger('state_id')->nullable()->index();
            $table->date('birth_date')->nullable();
            // Income - Info
            $table->unsignedInteger('income_work_dollar')->nullable();
            $table->unsignedInteger('income_adjusted_gross_dollar')->nullable();
            // Income - Tax
            $table->unsignedInteger('income_tax_dollar')->nullable();
            // Income - Additional
            $table->unsignedInteger('income_additional_tax_credit_dollar')->nullable();
            $table->unsignedInteger('income_additional_child_support_paid_dollar')->nullable();
            $table->unsignedInteger('income_additional_work_study_dollar')->nullable();
            $table->unsignedInteger('income_additional_grant_scholarship_dollar')->nullable();
            $table->unsignedInteger('income_additional_combat_pay_dollar')->nullable();
            $table->unsignedInteger('income_additional_cooperative_education_dollar')->nullable();
            // Income - untaxed
            $table->unsignedInteger('income_untaxed_tax_deferred_dollar')->nullable();
            $table->unsignedInteger('income_untaxed_ira_deduction_dollar')->nullable();
            $table->unsignedInteger('income_untaxed_child_support_received_dollar')->nullable();
            $table->unsignedInteger('income_untaxed_tax_exempt_dollar')->nullable();
            $table->unsignedInteger('income_untaxed_ira_distribution_dollar')->nullable();
            $table->unsignedInteger('income_untaxed_pension_portion_dollar')->nullable();
            $table->unsignedInteger('income_untaxed_paid_to_military_dollar')->nullable();
            $table->unsignedInteger('income_untaxed_veteran_benefit_dollar')->nullable();
            $table->unsignedInteger('income_untaxed_other_dollar')->nullable();
            // Asset - education
            $table->unsignedInteger('asset_education_student_sibling_dollar')->nullable();
            $table->unsignedInteger('asset_education_five_two_nine_dollar')->nullable();
            $table->unsignedInteger('asset_education_coverdell_dollar')->nullable();
            // Asset - non-retirement
            $table->unsignedInteger('asset_non_retirement_checking_savings_dollar')->nullable();
            $table->unsignedInteger('asset_non_retirement_certificate_of_deposit_dollar')->nullable();
            $table->unsignedInteger('asset_non_retirement_t_bills_dollar')->nullable();
            $table->unsignedInteger('asset_non_retirement_money_market_dollar')->nullable();
            $table->unsignedInteger('asset_non_retirement_mutual_dollar')->nullable();
            $table->unsignedInteger('asset_non_retirement_stock_dollar')->nullable();
            $table->unsignedInteger('asset_non_retirement_bond_dollar')->nullable();
            $table->unsignedInteger('asset_non_retirement_trust_dollar')->nullable();
            $table->unsignedInteger('asset_non_retirement_other_securities_dollar')->nullable();
            $table->unsignedInteger('asset_non_retirement_annuities_dollar')->nullable();
            $table->unsignedInteger('asset_non_retirement_other_investments_dollar')->nullable();
            // Asset - retirement
            $table->unsignedInteger('asset_retirement_defined_benefit_dollar')->nullable();
            $table->unsignedInteger('asset_retirement_four_o_one_dollar')->nullable();
            $table->unsignedInteger('asset_retirement_ira_dollar')->nullable();
            $table->unsignedInteger('asset_retirement_roth_ira_dollar')->nullable();
            $table->unsignedInteger('asset_retirement_previous_employer_dollar')->nullable();
            $table->unsignedInteger('asset_retirement_other_dollar')->nullable();
            // Insurance
            $table->unsignedInteger('insurance_permanent_cash_value_dollar')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('family_id')->references('id')->on('families');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('family_parents');
    }
}
