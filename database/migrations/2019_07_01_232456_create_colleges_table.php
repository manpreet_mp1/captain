<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollegesTable extends Migration
{
    /**
     * The database schema.
     *
     * @var Schema
     */
    protected $schema;

    /**
     * Create a new migration instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->schema = Schema::connection(config('database.data'));
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('database.runOtherDbMigration')) {
            ini_set('memory_limit', '-1');
            $this->schema->create('college_treasuries', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('unitid')->index();
                $table->unsignedInteger('mn_earn_wne_p6')->nullable();
                $table->unsignedInteger('md_earn_wne_p6')->nullable();
                $table->unsignedInteger('mn_earn_wne_p10')->nullable();
                $table->unsignedInteger('md_earn_wne_p10')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });

            $this->schema->create('custom_ron_colleges', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('unitid')->index();
                $table->string('instnm')->index();
                $table->string('webaddr')->nullable();
                $table->string('stabbr')->nullable()->index();
                $table->string('city')->nullable()->index();
                $table->double('need_met', 20, 4)->default(0);
                $table->double('gift_aid', 20, 4)->default(0);
                $table->double('self_help', 20, 4)->default(0);
                $table->boolean('is_college_im')->default(0);
                $table->double('avg_graduation_year', 20, 4)->default(4);
                $table->double('inflation', 20, 4)->default(0.0400);
                $table->double('inflation_year_1', 20, 4)->nullable();
                $table->double('inflation_year_2', 20, 4)->nullable();
                $table->double('inflation_year_3', 20, 4)->nullable();
                $table->double('inflation_year_4', 20, 4)->nullable();
                $table->double('inflation_year_5', 20, 4)->nullable();
                $table->string('merit_link')->nullable();
                $table->string('common_data_link')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });

            $this->schema->create('colleges', function (Blueprint $table) {
                // Basic details
                $table->unsignedBigInteger('unitid')->primary();
                $table->string('name', 250)->index();
                $table->string('slug', 250)->nullable()->unique();
                $table->string('institute_type', 10);
                $table->string('address')->index();
                $table->string('city', 50)->index();
                $table->string('state_abbr')->index();
                $table->string('zip')->index();
                $table->string('inst_nm_city', 400)->index()->comment("institute name and city combined");
                $table->string('web_addr')->nullable();
                $table->string('net_price_addr')->nullable()->comment('Net price calculator web address');
                $table->string('locale')->nullable()->comment('locale - City, Rural, Suburb, Town');
                $table->string('inst_size')->nullable()->comment('Size of the college');
                $table->string('cal_sys')->nullable()->comment('Academic calendar system');
                $table->string('opeid')->comment('Federal School Code or OPEID');

                // Admissions Statistics
                $table->unsignedSmallInteger('ug_application_fee')->nullable()->comment('Undergraduate application fee');
                $table->unsignedInteger('applicants_total')->nullable()->comment('Applicants total');
                $table->unsignedInteger('admissions_total')->nullable()->comment('Admissions total');
                $table->double('percent_admitted', 20, 4)->nullable()->comment('Percent admitted - total, Acceptance Rate');
                $table->unsignedInteger('enrolled_total')->nullable()->comment('Enrolled total');
                $table->double('admissions_yield', 20, 4)->nullable()->comment('Admissions yield - full time');
                $table->unsignedInteger('undergraduate_enrollment')->nullable()->comment('Undergraduate enrollment');

                $table->unsignedSmallInteger('act_comp_25')->nullable()->comment('ACT Composite 25th percentile score');
                $table->unsignedSmallInteger('act_comp_75')->nullable()->comment('ACT Composite 75th percentile score');
                $table->unsignedSmallInteger('act_eng_25')->nullable()->comment('ACT English 25th percentile score');
                $table->unsignedSmallInteger('act_eng_75')->nullable()->comment('ACT English 75th percentile score');
                $table->unsignedSmallInteger('act_math_25')->nullable()->comment('ACT Math 25th percentile score');
                $table->unsignedSmallInteger('act_math_75')->nullable()->comment('ACT Math 75th percentile score');
                $table->unsignedSmallInteger('act_submitted_total')->nullable()->comment('Number of first-time degree/certificate-seeking students submitting ACT scores');
                $table->unsignedSmallInteger('act_submitted_percent')->nullable()->comment('Percent of first-time degree/certificate-seeking students submitting ACT scores');

                $table->unsignedSmallInteger('sat_read_25')->nullable()->comment('SAT Critical Reading 25th percentile score');
                $table->unsignedSmallInteger('sat_read_75')->nullable()->comment('SAT Critical Reading 75th percentile score');
                $table->unsignedSmallInteger('sat_math_25')->nullable()->comment('SAT Math 25th percentile score');
                $table->unsignedSmallInteger('sat_math_75')->nullable()->comment('SAT Math 75th percentile score');
                $table->unsignedSmallInteger('sat_submitted_total')->nullable()->comment('Number of first-time degree/certificate-seeking students submitting SAT scores');
                $table->unsignedSmallInteger('sat_submitted_percent')->nullable()->comment('Percent of first-time degree/certificate-seeking students submitting SAT scores');

                // Admissions Requirements
                $table->string('admission_req_gpa')->nullable()->comment('Secondary school GPA');
                $table->string('admission_req_rank')->nullable()->comment('Secondary school rank');
                $table->string('admission_req_record')->nullable()->comment('Secondary school record');
                $table->string('admission_req_comp_prep_prog')->nullable()->comment('Completion of college-preparatory program');
                $table->string('admission_req_rec')->nullable()->comment('Recommendations');
                $table->string('admission_req_comp')->nullable()->comment('Formal demonstration of competencies');
                $table->string('admission_req_test_score')->nullable()->comment('Admission test scores');
                $table->string('admission_req_toefl')->nullable()->comment('TOEFL (Test of English as a Foreign Language');
                $table->string('admission_req_other_test')->nullable()->comment('Other Test (Wonderlic, WISC-III, etc.)');

                // Cost
                $table->unsignedInteger('original_in_state_cost')->nullable()->comment('Total price for in-state students living on campus/off campus/with parents if earlier is null');
                $table->unsignedInteger('original_out_state_cost')->nullable()->comment('Total price for out-state-district students living on campus/off campus/with parents if earlier is null');
                $table->unsignedInteger('original_private_cost')->nullable()->comment('Total price for private-state students living on campus/off campus/with parents if earlier is null');

                $table->unsignedInteger('on_camp_room_board')->nullable()->comment('On campus, room and board');
                $table->unsignedInteger('on_camp_other')->nullable()->comment('On campus, other expenses');
                $table->unsignedInteger('off_camp_not_family_room_board')->nullable()->comment('Off campus (not with family), room and board');
                $table->unsignedInteger('off_camp_not_family_other')->nullable()->comment('Off campus (not with family), other expenses');
                $table->unsignedInteger('off_camp_with_family_other')->nullable()->comment('Off campus (with family), other expenses');
                $table->unsignedInteger('books_supplies')->nullable()->comment('Books and supplies');

                $table->tinyInteger('tuition_guaranteed_plan')->nullable()->comment('Tuition guaranteed plan');
                $table->tinyInteger('prepaid_tuition_plan')->nullable()->comment('Prepaid tuition plan');
                $table->tinyInteger('tuition_payment_plan')->nullable()->comment('Tuition payment plan');
                $table->tinyInteger('other_alternative_tuition_plan')->nullable()->comment('Other alternative tuition plan');

                // Net price
                $table->unsignedInteger('net_price')->nullable()->comment('Average net price-students receiving grant or scholarship aid, paying the in-state or in-district tuition rate');

                // Net Price by Household Income
                $table->integer('net_price_0_30k')->nullable()->comment('Average net price (income 0-30,000)-students awarded Title IV federal financial aid');
                $table->integer('net_price_30k_48k')->nullable()->comment('Average net price (income 30,001-48,000)-students awarded Title IV federal financial aid');
                $table->integer('net_price_48k_75k')->nullable()->comment('Average net price (income 48,001-75,000)-students awarded Title IV federal financial aid');
                $table->integer('net_price_75k_110k')->nullable()->comment('Average net price (income 75,001-110,000)-students awarded Title IV federal financial aid');
                $table->integer('net_price_110k_plus')->nullable()->comment('Average net price (income over 110,000)-students awarded Title IV federal financial aid');
                $table->integer('net_price_75k_plus')->nullable()->comment('Custom: Average net price income more than 75001');

                // financial aid
                $table->unsignedInteger('full_first_ug_any_aid_total')->nullable()->comment('Number of full-time first-time undergraduates awarded federal, state, local or institutional grant aid');
                $table->unsignedInteger('full_first_ug_any_aid_percent')->nullable()->comment('Percent of full-time first-time undergraduates awarded federal, state, local or institutional grant aid');
                $table->unsignedInteger('full_first_ug_any_aid_amount')->nullable()->comment('Average amount of federal, state, local or institutional grant aid awarded');
                $table->unsignedInteger('full_first_ug_fed_aid_total')->nullable()->comment('Number of full-time first-time undergraduates awarded federal grant aid');
                $table->unsignedInteger('full_first_ug_fed_aid_percent')->nullable()->comment('Percent of full-time first-time undergraduates awarded federal grant aid');
                $table->unsignedInteger('full_first_ug_fed_aid_amount')->nullable()->comment('Average amount of federal grant aid awarded to full-time first-time undergraduates');
                $table->unsignedInteger('full_first_ug_state_aid_total')->nullable()->comment('Number of full-time first-time undergraduates awarded state/local grant aid');
                $table->unsignedInteger('full_first_ug_state_aid_percent')->nullable()->comment('Percent of full-time first-time undergraduates awarded state/local grant aid');
                $table->unsignedInteger('full_first_ug_state_aid_amount')->nullable()->comment('Average amount of state/local grant aid awarded to full-time first-time undergraduates');
                $table->unsignedInteger('full_first_ug_inst_aid_total')->nullable()->comment('Number of full-time first-time undergraduates awarded  institutional grant aid');
                $table->unsignedInteger('full_first_ug_inst_aid_percent')->nullable()->comment('Percent of full-time first-time undergraduates awarded institutional grant aid');
                $table->unsignedInteger('full_first_ug_inst_aid_amount')->nullable()->comment('Average amount of institutional grant aid awarded to full-time first-time undergraduates');
                $table->unsignedInteger('full_first_ug_pell_grant_total')->nullable()->comment('Number of undergraduate students awarded Pell grants');
                $table->unsignedInteger('full_first_ug_pell_grant_percent')->nullable()->comment('Percent of full-time first-time undergraduates awarded Pell grants');
                $table->unsignedInteger('full_first_ug_pell_grant_amount')->nullable()->comment('Average amount of Pell grant aid awarded to full-time first-time undergraduates');
                $table->unsignedInteger('full_first_ug_student_loan_total')->nullable()->comment('Number of full-time first-time undergraduates awarded student loans');
                $table->unsignedInteger('full_first_ug_student_loan_percent')->nullable()->comment('Percent of full-time first-time undergraduates awarded student loans');
                $table->unsignedInteger('full_first_ug_student_loan_amount')->nullable()->comment('Average amount of student loans awarded to full-time first-time undergraduates');

                // Academic statistics
                $table->unsignedSmallInteger('graduation_rate_total')->nullable()->comment('Graduation rate, total cohort');
                $table->unsignedSmallInteger('full_time_retention_rate')->nullable()->comment('Full-time retention rate');
                $table->unsignedSmallInteger('part_time_retention_rate')->nullable()->comment('Part-time retention rate');
                $table->unsignedSmallInteger('student_to_faculty')->nullable()->comment('Student-to-faculty ratio');

                $table->tinyInteger('weekend_eve_college')->nullable()->comment('Weekend/evening college');
                $table->tinyInteger('teacher_certification')->nullable()->comment('Teacher certification (below the postsecondary level)');
                $table->tinyInteger('distance_education')->nullable()->comment('Distance education courses offered');
                $table->tinyInteger('study_abroad')->nullable()->comment('Study abroad');

                // majors
                $table->string('major_1')->nullable()->comment('CIP code of largest program')->index();
                $table->string('major_2')->nullable()->comment('CIP code of 2nd largest program')->index();
                $table->string('major_3')->nullable()->comment('CIP code of 3rd largest program')->index();
                $table->string('major_4')->nullable()->comment('CIP code of 4th largest program')->index();
                $table->string('major_5')->nullable()->comment('CIP code of 5th largest program')->index();

                // student body
                $table->unsignedSmallInteger('ug_men_percent')->nullable()->comment('Percent of undergraduate enrollment that are men (100 - women)');
                $table->unsignedSmallInteger('ug_women_percent')->nullable()->comment('Percent of undergraduate enrollment that are women');

                $table->unsignedSmallInteger('ug_in_state_percent')->nullable()->comment('Percent of first-time undergraduates - in-state');
                $table->unsignedSmallInteger('ug_out_state_percent')->nullable()->comment('Percent of first-time undergraduates - out-of-state');
                $table->unsignedSmallInteger('ug_foreign_percent')->nullable()->comment('Percent of first-time undergraduates - foreign countries');
                $table->unsignedSmallInteger('ug_unknown_percent')->nullable()->comment('Percent of first-time undergraduates - residence unknown');

                $table->unsignedSmallInteger('ug_30k_total')->nullable()->comment('Number in income level (0-30,000)');
                $table->unsignedSmallInteger('ug_30k_48k_total')->nullable()->comment('Number in income level (30,001-48,000)');
                $table->unsignedSmallInteger('ug_48k_75k_total')->nullable()->comment('Number in income level (48,001-75,000)');
                $table->unsignedSmallInteger('ug_75k_110k_total')->nullable()->comment('Number in income level (75,001-110,000)');

                $table->unsignedSmallInteger('ug_african_percent')->nullable()->comment('Percent of undergraduate enrollment that are Black or African American');
                $table->unsignedSmallInteger('ug_asian_percent')->nullable()->comment('Percent of undergraduate enrollment that are Asian');
                $table->unsignedSmallInteger('ug_hispanic_percent')->nullable()->comment('Percent of undergraduate enrollment that are Hispanic/Latino');
                $table->unsignedSmallInteger('ug_non_resident_percent')->nullable()->comment('Percent of undergraduate enrollment that are Nonresident Alien');
                $table->unsignedSmallInteger('ug_multiracial_percent')->nullable()->comment('Percent of undergraduate enrollment that are two or more races');
                $table->unsignedSmallInteger('ug_native_percent')->nullable()->comment('Percent of undergraduate enrollment that are American Indian or Alaska Native');
                $table->unsignedSmallInteger('ug_pacific_percent')->nullable()->comment('Percent of undergraduate enrollment that are Native Hawaiian or Other Pacific Islander');
                $table->unsignedSmallInteger('ug_unknown_race_percent')->nullable()->comment('Percent of undergraduate enrollment that are Race/ethnicity unknown');
                $table->unsignedSmallInteger('ug_white_percent')->nullable()->comment('Percent of undergraduate enrollment that are White');

                // QP Formula
                $table->unsignedSmallInteger('year')->comment('Which year IPEDS Data do we have, this is only related to price');
                $table->unsignedSmallInteger('data_year_difference')->comment('Year in IPEDS Data and current year');

                $table->double('avg_graduation_year', 20, 4)->default(4);

                $table->double('need_met', 20, 4)->default(0);
                $table->double('gift_aid', 20, 4)->default(0);
                $table->double('self_help', 20, 4)->default(0);

                $table->double('inflation', 20, 4)->default(0.0400);
                $table->double('inflation_year_1', 20, 4)->nullable();
                $table->double('inflation_year_2', 20, 4)->nullable();
                $table->double('inflation_year_3', 20, 4)->nullable();
                $table->double('inflation_year_4', 20, 4)->nullable();
                $table->double('inflation_year_5', 20, 4)->nullable();

                $table->unsignedSmallInteger('year_1');
                $table->unsignedSmallInteger('year_2');
                $table->unsignedSmallInteger('year_3');
                $table->unsignedSmallInteger('year_4');
                $table->unsignedSmallInteger('year_5');

                $table->unsignedInteger('private_cost_year_1')->nullable();
                $table->unsignedInteger('private_cost_year_2')->nullable();
                $table->unsignedInteger('private_cost_year_3')->nullable();
                $table->unsignedInteger('private_cost_year_4')->nullable();
                $table->unsignedInteger('private_cost_year_5')->nullable();

                $table->unsignedInteger('instate_cost_year_1')->nullable();
                $table->unsignedInteger('instate_cost_year_2')->nullable();
                $table->unsignedInteger('instate_cost_year_3')->nullable();
                $table->unsignedInteger('instate_cost_year_4')->nullable();
                $table->unsignedInteger('instate_cost_year_5')->nullable();

                $table->unsignedInteger('out_state_cost_year_1')->nullable();
                $table->unsignedInteger('out_state_cost_year_2')->nullable();
                $table->unsignedInteger('out_state_cost_year_3')->nullable();
                $table->unsignedInteger('out_state_cost_year_4')->nullable();
                $table->unsignedInteger('out_state_cost_year_5')->nullable();

                // earnings
                $table->unsignedInteger('md_earn_wne_p10')->nullable()->comment('Median earnings of students working and not enrolled 10 years after entry');
                $table->unsignedInteger('mn_earn_wne_p10')->nullable()->comment('Mean earnings of students working and not enrolled 10 years after entry');
                $table->unsignedInteger('md_earn_wne_p6')->nullable()->comment('Median earnings of students working and not enrolled 6 years after entry');
                $table->unsignedInteger('mn_earn_wne_p6')->nullable()->comment('Mean earnings of students working and not enrolled 6 years after entry');
                $table->unsignedInteger('avg_earn_wne_p6')->nullable()->comment('Average earnings of students working and not enrolled 6 years after entry');
                $table->unsignedInteger('avg_earn_wne_p10')->nullable()->comment('Average earnings of students working and not enrolled 10 years after entry');

                // flags
                $table->boolean('is_college_im')->default(0)->comment('Does colleges uses IM or FM Methodology');
                $table->boolean('is_private')->default(1)->comment('Is college a private college');

                $table->timestamps();
                $table->softDeletes();
            });

            Artisan::call('DataPopulate:CollegeScorecardTableCommand');
            Artisan::call('DataManipulate:AddIndexToIpedsTableCommand');
            Artisan::call('DataPopulate:CustomRonCollegesTableCommand');
            Artisan::call('DataManipulate:EtlJobCollegesTableCommand');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (config('database.runOtherDbMigration')) {
            $this->schema->dropIfExists('colleges');
            $this->schema->dropIfExists('college_treasuries');
            $this->schema->dropIfExists('custom_ron_colleges');
        }
    }
}
