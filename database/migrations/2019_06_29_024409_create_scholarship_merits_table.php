<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScholarshipMeritsTable extends Migration
{
    /**
     * The database schema.
     *
     * @var Schema
     */
    protected $schema;

    /**
     * Create a new migration instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->schema = Schema::connection(config('database.data'));
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('database.runOtherDbMigration')) {
            ini_set('memory_limit', '-1');
            $this->schema->create('scholarship_merits', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('ipeds')->index();
                $table->string('school_name')->index();
                $table->string('state_abbr')->index();
                $table->string('name');
                $table->string('type', 100)->index();
                $table->string('amount');
                $table->string('required_sat', 10);
                $table->string('required_act', 10);
                $table->string('required_gpa', 10);
                $table->string('school_type', 50);
                $table->string('acceptance_rate', 10);
                $table->string('scholarship_url', 500);
                $table->string('tuition');
                $table->string('eligibility_for_public_schools', 50);
                $table->string('international_students', 10);
                $table->string('mid_50_act', 500);
                $table->string('mid_50_sat', 500);
                $table->string('us_news_ranking', 50);
                $table->longText('scholarship_description');
                $table->timestamps();
                $table->softDeletes();
            });

            Artisan::call('DataPopulate:ScholarshipMeritTableCommand');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (config('database.runOtherDbMigration')) {
            $this->schema->dropIfExists('scholarship_merits');
        }
    }
}
