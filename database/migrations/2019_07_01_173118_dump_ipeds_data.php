<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Artisan;

class DumpIpedsData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('database.runOtherDbMigration')) {
            ini_set('memory_limit', '-1');
            Artisan::call('DataPopulate:DumpIpedsDataFromSqlFileCommand');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
