<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollegeGpasTable extends Migration
{
    /**
     * The database schema.
     *
     * @var Schema
     */
    protected $schema;

    /**
     * Create a new migration instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->schema = Schema::connection(config('database.data'));
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('database.runOtherDbMigration')) {
            ini_set('memory_limit', '-1');
            $this->schema->create('college_gpas', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedInteger('unitid')->index()->nullable();
                $table->string('school')->index();
                $table->string('state')->index();
                $table->string('sat25')->nullable();
                $table->string('sat75')->nullable();
                $table->string('act25')->nullable();
                $table->string('act75')->nullable();
                $table->string('gpa')->nullable();
                $table->string('acceptance_rate')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });

            Artisan::call('DataPopulate:CollegeGpaCommand');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (config('database.runOtherDbMigration')) {
            $this->schema->dropIfExists('college_gpas');
        }
    }
}
