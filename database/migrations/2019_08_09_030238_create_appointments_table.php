<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('family_id');
            $table->unsignedBigInteger('acuity_calendar_id')->index();
            $table->string('appointment_date');
            $table->string('appointment_start_time');
            $table->string('appointment_end_time');
            $table->string('appointment_datetime');
            $table->string('client_timezone');
            $table->string('duration_minutes');
            $table->unsignedTinyInteger('is_canceled');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('family_id')->references('id')->on('families');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
